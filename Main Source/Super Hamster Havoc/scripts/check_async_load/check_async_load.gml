/// @function check_async_load(async_id)
/// @description Takes the id from buffer_save_async and checks if it was loaded
/// @param async_id The Async ID being checked for

var _async_id = argument0;

//
if ds_map_find_value(async_load, "id") == _async_id
   {
	if ds_map_find_value(async_load, "status") == false
      {
      internal_debug_console_message("Status for an async load attempt with the id " + string(_async_id) + " is FAILED");
	  return false;
      }
	
	if ds_map_find_value(async_load, "status") == true
      {
      internal_debug_console_message("Status for an async load attempt with the id " + string(_async_id) + " is SUCCESS");
	  return true;
      }
   }
