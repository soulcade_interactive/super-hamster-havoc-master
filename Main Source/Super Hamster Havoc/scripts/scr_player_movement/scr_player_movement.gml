gamepad_id = gamepad[argument0];
camera = argument0;
#region
var horizontal_axis = gamepad_axis_value(gamepad_id,gp_axislh);
var vertical_axis = gamepad_axis_value(gamepad_id,gp_axislv);
var left_joystick = gamepad_button_check(gamepad_id,gp_stickl);
#endregion
//Convert Input to Movement Variables
#region
var horizontal_movement = (horizontal_axis);
var vertical_movement = (vertical_axis);
var sprint = (1 + left_joystick);
#endregion
//Delta Time the movement (So it's the intended speed on all devices)
#region
if knockback_distance > 0
	{
	knockback_distance = lerp(knockback_distance,0,0.25)
	var knockback_x = lengthdir_x(knockback_distance,knockback_dir);
	var knockback_y = lengthdir_y(knockback_distance,knockback_dir);
	}
	
else
	{
	var knockback_x = 0;
	var knockback_y = 0;	
	}
hsp = ((horizontal_movement + knockback_x) * movement_speed * sprint) * game_delta_time;
vsp = ((vertical_movement + knockback_y) * movement_speed * sprint) * game_delta_time;
#endregion
// Horizontal and Vertical Movement Implementation
#region
//Horizontal Collisions and Movement
if (place_meeting(x+hsp,y,obj_solid_object_parent))
	{
	//Increment till collisions are Pixel Perfect
	while (!place_meeting(x+sign(hsp),y,obj_solid_object_parent))
		{
		x += sign(hsp)	
		}
	//Set speed to zero upon flush collision
	hsp = 0;
	}
x += hsp;

//Vertical Collisions and Movement
if (place_meeting(x,y+vsp,obj_solid_object_parent))
	{
	//Increment till collisions are Pixel Perfect
	while (!place_meeting(x,y+sign(vsp),obj_solid_object_parent))
		{
		y += sign(vsp)	
		}
	//Set speed to zero upon flush collision
	vsp = 0;
	}
y += vsp;
#endregion