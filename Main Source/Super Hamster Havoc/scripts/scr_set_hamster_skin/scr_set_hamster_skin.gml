
var skin = argument0;
enum cell_type
    {
    ground,
    wall,
    void
    }
    
enum hamster_skin 
	{
	black,
	white,
	browngray,
	brownwhite
	}
switch(skin)
	{
		
	case hamster_skin.black:
	body_idle = spr_hamster_body_idle_black;
	body_run = spr_hamster_body_run_black;
	body_run_side = spr_hamster_body_run_side_black;
	body_run_back = spr_hamster_body_run_back_black;
	head = spr_hamster_head_black;
	
	corpse_head = spr_hamster_corpse_head_black;
	corpse_torso = spr_hamster_corpse_torso_black;
	corpse_limb = spr_hamster_corpse_limb_black;
	corpse_paw = spr_hamster_corpse_paw;
	break;
	
	case hamster_skin.white:
	body_idle = spr_hamster_body_idle_white;
	body_run = spr_hamster_body_run_white;
	body_run_side = spr_hamster_body_run_side_white;
	body_run_back = spr_hamster_body_run_back_white;
	head = spr_hamster_head_white;
	
	corpse_head = spr_hamster_corpse_head_white;
	corpse_torso = spr_hamster_corpse_torso_white;
	corpse_limb = spr_hamster_corpse_limb_white;
	corpse_paw = spr_hamster_corpse_paw;
	break;
	
	case hamster_skin.browngray:
	body_idle = spr_hamster_body_idle_brown_and_gray;
	body_run = spr_hamster_body_run_brown_and_gray;
	body_run_side = spr_hamster_body_run_side_brown_and_gray;
	body_run_back = spr_hamster_body_run_back_brown_and_gray;
	head = spr_hamster_head_brown_and_gray;
	
	corpse_head = spr_hamster_corspe_head_brown_and_gray;
	corpse_torso = spr_hamster_corpse_torso_brown_and_gray;
	corpse_limb = spr_hamster_corpse_limb_brown_and_gray;
	corpse_paw = spr_hamster_corpse_paw;
	break;
	
	case hamster_skin.brownwhite:
	body_idle = spr_hamster_body_idle_brown_and_white;
	body_run = spr_hamster_body_run_brown_and_white;
	body_run_side = spr_hamster_body_run_side_brown_and_white;
	body_run_back = spr_hamster_body_run_back_brown_and_white;
	head = spr_hamster_head_brown_and_white;
	
	corpse_head = spr_hamster_corpse_head_brown_and_white;
	corpse_torso = spr_hamster_corpse_torso_brown_and_white;
	corpse_limb = spr_hamster_corpse_limb_brown_and_white;
	corpse_paw = spr_hamster_corpse_paw;
	break;
	}