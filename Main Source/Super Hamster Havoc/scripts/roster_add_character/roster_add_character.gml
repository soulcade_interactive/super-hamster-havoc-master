/// @function roster_add_character(character_id,display_name,row)
/// @description Adds character to the roster for player_selection (Returns a ds_map id)
/// @param character_id ID of the character per Marco's naming convention
/// @param display_name Name of the character
/// @param costume_amount Ammount of costumes
/// @param character_type The type of template the character follows
/// @param character_perks A list of the perks of the character
/// @param row Assigned row of character roster

//Get arguments
var character_id = argument0;
var character_display_name = argument1;
var costume_amount = argument2;
var character_type = argument3;
var character_perks = argument4;
var row = argument5;

var character_data = ds_map_create();

//Add data to ds_map
ds_map_add(character_data,"character_id",character_id);
ds_map_add(character_data,"display_name",character_display_name);
ds_map_add(character_data,"skin_amount",costume_amount);
ds_map_add(character_data,"character_type",character_type);
ds_map_add(character_data,"character_perks",character_perks);

var character_icon = asset_get_index("spr_" + character_id + "_character_icon");
if character_icon == -1
	{
	ds_map_add(character_data,"character_icon", asset_get_index("spr_" + character_id + "_0_head_r_running"));
	}
else
	{
	ds_map_add(character_data,"character_icon", asset_get_index("spr_" + character_id + "_character_icon"));
	}

//Add to character roster
ds_list_add(character_roster[row-1],character_data);
