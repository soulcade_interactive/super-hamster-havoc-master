/// @param Val The value being rounded
/// @param RoundTo The value who's multiple is being rounded to

var rounding = argument0;
var roundto = argument1;

var returnval = roundto * floor(rounding/roundto);
return returnval;