//Get Gamepad ID
var gamepad_id = gamepad[argument0];
var camera = argument0;


//Get Variables
#region
//Get Gun Directions
var axis_h = gamepad_axis_value(gamepad_id,gp_axisrh);
var axis_v = gamepad_axis_value(gamepad_id,gp_axisrv);
var axis_hm = gamepad_axis_value(gamepad_id,gp_axislh);
var axis_vm = gamepad_axis_value(gamepad_id,gp_axislv);

if ((axis_h == 0) && (axis_v == 0))
	{
	gun_direction = point_direction(0,0,axis_hm,axis_vm);
	}
else
	{
	gun_direction = point_direction(0,0,axis_h,axis_v);
	}

//Calculate Gun Barrel postioning
var gun_barrel_x = gun_x + lengthdir_x(4,gun_direction);
var gun_barrel_y = gun_y + lengthdir_y(0,gun_direction);

var gun_eject_x = gun_x + lengthdir_x(-14,gun_direction);
var gun_eject_y = gun_y + lengthdir_y(-14,gun_direction);
//Increment Gun Firerate Variables 
pistol_delay -= 1;
shotgun_delay -= 1;
sniper_delay -= 1;
assault_rifle_delay -= 1;
rocket_launcher_delay -= 1;
#endregion

//Fire Automatic Weapons
#region
if gamepad_button_check(gamepad_id,gp_shoulderrb)
	{
	switch(item_equipped)
		{
		
		case weapon.assault_rifle:
		if assault_rifle_delay < 0
			{
			assault_rifle_delay = 5;
			gamepad_set_vibration(gamepad_id,0.5,0.5);
			alarm[0] = room_speed/6;
			cameras[camera].screen_shake = 5;
			var knockback = 32;
			gun_x += lengthdir_x(knockback,gun_direction);
			gun_y += lengthdir_y(knockback,gun_direction);
			audio_play_sound(snd_shoot_ak,0,false);
			var shell = instance_create_layer(gun_eject_x,gun_eject_y,"Instances",obj_bullet_shell)
				shell.image_speed = 0;
				shell.image_index = 0;
				var variaty = irandom_range(-35,35);
				shell.hsp = lengthdir_x(6,90 + variaty)
				shell.vsp = lengthdir_y(6,90+ variaty)
			with(instance_create_layer(gun_barrel_x,gun_barrel_y,"Instances",obj_bullet))
				{
				direction = other.gun_direction + random_range(-5,5);
				image_angle = direction;
				speed = 15;
				gamepad_id = other.player_id;
				origin = other.id;
				damage = 5;
				}
			}
		break;
		
		}
	}
#endregion
	
//Fire Semi-Automatic Weapons
#region
	if gamepad_button_check_pressed(gamepad_id,gp_shoulderrb)
	{
	switch(item_equipped)
		{
		//Hands
		case weapon.hands:
		break;
		
		//Fire Pistol
		case weapon.pistol:
			gamepad_set_vibration(gamepad_id,0.5,0.5);
			alarm[0] = room_speed/6;
			cameras[camera].screen_shake = 5;
			var knockback = 8;
			gun_x += lengthdir_x(knockback,gun_direction);
			gun_y += lengthdir_y(knockback,gun_direction);
			audio_play_sound(snd_pistol_single_fire,0,false);
			var shell = instance_create_layer(gun_eject_x,gun_eject_y,"Instances",obj_bullet_shell)
				shell.image_speed = 0;
				shell.image_index = 0;
				var variaty = irandom_range(-35,35);
				shell.hsp = lengthdir_x(6,90 + variaty)
				shell.vsp = lengthdir_y(6,90+ variaty)
			with(instance_create_layer(gun_barrel_x,gun_barrel_y,"Instances",obj_bullet))
				{
				direction = other.gun_direction;
				image_angle = direction;
				speed = 30;
				gamepad_id = other.player_id;
				origin = other.id;
				damage = 5;
				}
		break;
		
		//Fire Shotgun
		case weapon.shotgun:
			if shotgun_delay < 0
			{
			shotgun_delay = 25;
			gamepad_set_vibration(gamepad_id,0.75,0.75);
			alarm[0] = room_speed/6;
			cameras[camera].screen_shake = 20;
			var knockback = 96;
			gun_x += lengthdir_x(knockback,gun_direction);
			gun_y += lengthdir_y(knockback,gun_direction);
			audio_play_sound(snd_shoot_shotgun,0,false);
			var shell = instance_create_layer(gun_eject_x,gun_eject_y,"Instances",obj_bullet_shell)
				shell.image_speed = 0;
				shell.image_index = 2;
				var variaty = irandom_range(-35,35);
				shell.hsp = lengthdir_x(6,90 + variaty)
				shell.vsp = lengthdir_y(6,90+ variaty)
			with(instance_create_layer(gun_barrel_x,gun_barrel_y,"Instances",obj_bullet))
				{
				direction = other.gun_direction;
				image_angle = direction;
				speed = 15;
				gamepad_id = other.player_id;
				origin = other.id;
				damage = 10;
				}
				
			with(instance_create_layer(gun_barrel_x,gun_barrel_y,"Instances",obj_bullet))
				{
				direction = other.gun_direction+15;
				image_angle = direction;
				speed = 15;
				gamepad_id = other.player_id;
				origin = other.id;
				damage = 10;
				}
				
			with(instance_create_layer(gun_barrel_x,gun_barrel_y,"Instances",obj_bullet))
				{
				direction = other.gun_direction-15;
				image_angle = direction;
				speed = 15;
				gamepad_id = other.player_id;
				origin = other.id;
				damage = 10;
				}
			}
		break;
		
		//Fire Sniper 
		case weapon.sniper:
			gamepad_set_vibration(gamepad_id,1,1);
			alarm[0] = room_speed/6;
			cameras[camera].screen_shake = 15;
			var knockback = 16;
			gun_x += lengthdir_x(knockback,gun_direction);
			gun_y += lengthdir_y(knockback,gun_direction);
			audio_play_sound(snd_sniper_single_fire,0,false);
			var shell = instance_create_layer(gun_eject_x,gun_eject_y,"Instances",obj_bullet_shell)
				shell.image_speed = 0;
				shell.image_index = 1;
				shell.hsp = lengthdir_x(knockback/3,gun_direction - 90 + irandom_range(-15,15))
				shell.vsp = lengthdir_y(knockback/3,gun_direction - 90+ irandom_range(-15,15))
			with(instance_create_layer(gun_barrel_x,gun_barrel_y,"Instances",obj_bullet))
				{
				direction = other.gun_direction;
				image_angle = direction;
				speed = 25;
				gamepad_id = other.player_id;
				origin = other.id;
				damage = 15;
				}
		break;
		
		//Fire Rocket Launcher
		case weapon.rocket_launcher:
		if rocket_launcher_delay < 0
		{
			rocket_launcher_delay = 25;
			gamepad_set_vibration(gamepad_id,1,1);
			alarm[0] = room_speed/6;
			cameras[camera].screen_shake = 15;
			var knockback = 16;
			gun_x += lengthdir_x(knockback,gun_direction);
			gun_y += lengthdir_y(knockback,gun_direction);
			audio_play_sound(snd_shoot_boom,0,false);
			with(instance_create_layer(gun_barrel_x,gun_barrel_y,"Instances",obj_rocket))
				{
				direction = other.gun_direction;
				image_angle = direction;
				speed = 25;
				gamepad_id = other.player_id;
				origin = other.id;
				damage = 35;
				}
		}
		break;
		}
	}
#endregion

avaliable_gun = instance_place(x,y,obj_gun)
if avaliable_gun != noone
	{
	if gamepad_button_check_pressed(gamepad[player_id],gp_face3)
		{
		//Drops current gun Player Already has a gun
		if item_equipped != 0
				{
				var drop = instance_create_layer(x,y,"Instances",obj_gun);
				drop.gun_id = item_equipped;
				}
		//Actually picks up gun
		item_equipped = avaliable_gun.gun_id;
		instance_destroy(avaliable_gun);
		}
	}
	