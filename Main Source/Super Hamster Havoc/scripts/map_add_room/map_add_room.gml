/// @function map_add_room(name,flavor_text,room_id);
/// @description Registers a new projectile weapon
/// @param name The name of the new level
/// @param flavor_text The flavor text for the level
/// @param room_id The room_id for this level

var _name = argument0;
var _flavor_text = argument1;
var _room_id = argument2;

var map_add = ds_map_create();
ds_map_add(map_add,"name", _name);
ds_map_add(map_add,"flavor_text", _flavor_text);
ds_map_add(map_add,"room_id", _room_id);
ds_map_add(map_add,"map_type", "native");
ds_list_add(maps,json_encode(map_add));
ds_map_destroy(map_add);