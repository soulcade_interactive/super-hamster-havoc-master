/// @function register_projectile_weapon(name,flavor_text,sprite_index,image_index,gun_type,gun_firerate,firerate_value,gun_projectile,damage);
/// @description Registers a new projectile weapon
/// @param name The name of the new projectile weapon
/// @param flavor_text The flavor text for the projectile weapon
/// @param sprite_index The sprite for the projectile weapon
/// @param image_index The image index for the projectile weapon
/// @param gun_type The enumerate for the type of weapon this is
/// @param gun_firerate The enumerate for the fire rate
/// @param firerate The numerical value for the firerate
/// @param gun_projectile The enumerate for the type of projectile
/// @param damage The ammount of damage a single round does
/// @param akimbo Whether or not the weapon can be held akimbo

//Get Arguments
var _name = argument0;
var _flavor_text = argument1;
var _sprite_index = argument2;
var _image_index = argument3;
var _gun_type = argument4;
var _gun_firerate = argument5;
var _firerate = argument6;
var _gun_projectile = argument7;
var _damage = argument8;
var _akimbo = argument9;
var _sound = argument10;

//Create a DS Map for storing the gun data
var gun_data = ds_map_create();

//Add the information to the map
ds_map_add(gun_data,"name",_name);
ds_map_add(gun_data,"flavor_text",_flavor_text);
ds_map_add(gun_data,"sprite_index", _sprite_index);
ds_map_add(gun_data,"image_index",_image_index);
ds_map_add(gun_data,"gun_type",_gun_type);
ds_map_add(gun_data,"gun_firerate_type",_gun_firerate);
ds_map_add(gun_data,"gun_firerate_amount",_firerate);
ds_map_add(gun_data,"gun_projectile",_gun_projectile);
ds_map_add(gun_data,"damage",_damage);
ds_map_add(gun_data,"akimbo",_akimbo);
ds_map_add(gun_data,"sound",_sound);
//Add the map to the list of guns
internal_debug_console_message(_name + " has been registered")
var return_val = json_encode(gun_data);
ds_map_destroy(gun_data);

return return_val;



