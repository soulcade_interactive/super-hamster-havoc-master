/// @function add_gamemode(name,flavor_text);
/// @description Registers a new projectile weapon
/// @param name The name of the new gamemode
/// @param flavor_text The flavor text for the gamemode
/// @param min_players The minimum amount of players required of the gamemode
/// @param teams Whether or not the gamemode is team based

var _name = argument0;
var _flavor_text = argument1;
var _min_players = argument2;
var _teams = argument3;

var mode = ds_map_create();
ds_map_add(mode,"name", _name);
ds_map_add(mode,"flavor_text", _flavor_text);
ds_map_add(mode,"min_players", _min_players);
ds_map_add(mode,"teams", _teams);
ds_list_add(gamemode,json_encode(mode));
ds_map_destroy(mode);