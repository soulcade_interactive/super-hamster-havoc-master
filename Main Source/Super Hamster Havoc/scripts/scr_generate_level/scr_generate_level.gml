if (live_call(argument0)) return live_result;
/*
//Randomize the world seed
randomize();

//Resize the room
var room_size = (cell_size/cell_size) * (cell_size*100);
room_width = room_size;
room_height = room_size;

//Setup grid variables
var width = room_width div cell_size;
var height = room_height div cell_size;

//create an empthy grid for procedural generation
grid = ds_grid_create(width,height);
ds_grid_set_region(grid,0,0,width-1,height-1,cell_type.void);

//Create the controller
var cx = width div 2; //Controller X Position
var cy = height div 2; // Controller Y Position
var cdir = irandom(3); // Controller Direction Facing
var odds = argument0; // Odds of Controller Direction Changing


var spawn_increment = 0
globalvar spawn_x,spawn_y;
//Create the Level
repeat(1000)
    {
	spawn_increment += 1;
    //Place a Floor Tiles
    ds_grid_set(grid,cx,cy,cell_type.ground);
    
    // Move the controller
    
        // Checks chance of the Controller changing direction
        if (irandom(odds) == odds)
            {
            cdir = irandom(3);
            }
        
        // Moves the Controller
        var xdir = lengthdir_x(1,cdir*90);
        var ydir = lengthdir_y(1,cdir*90);
        
        cx += xdir;
        cy += ydir;
        
        cx = clamp(cx,1,width-2)
        cy = clamp(cy,1,height-2)
		
		if spawn_increment == 0 || 50 || 100 || 150 || 200 || 250 ||  300 || 350 || 400 || 450 || 500 || 550 || 600 || 650 || 700 || 750 || 800 || 850 || 900 || 950|| 1000
			{
			spawn_x[spawn_increment/50] = cx * cell_size;
			spawn_y[spawn_increment/50] = cy * cell_size;
			}
    }
    // Place the Wall Tiles 
    for (var yy = 1; yy < height - 1; yy++)
        {
        for (xx = 1; xx < width - 1; xx++)
            {
            if (grid[# xx, yy] == cell_type.ground)
                {
                //Check Left
                if (grid[# xx-1, yy] == cell_type.void)
                    {
                    grid[# xx-1, yy] = cell_type.wall;
					instance_create((xx-1)*cell_size,yy*cell_size,obj_wall);

                    }
                //Check Right 
                if (grid[# xx+1, yy] == cell_type.void)
                    {
                    grid[# xx+1, yy] = cell_type.wall;
					instance_create((xx+1)*cell_size,yy*cell_size,obj_wall);
                    }
                //Check Up
                if (grid[# xx, yy-1] == cell_type.void)
                    {
                    grid[# xx, yy-1] = cell_type.wall;
					instance_create(xx*cell_size,(yy-1)*cell_size,obj_wall);

                    }
                //Check Down
                if (grid[# xx, yy+1] == cell_type.void)
                    {
                    grid[# xx, yy+1] = cell_type.wall;
					instance_create(xx*cell_size,(yy+1)*cell_size,obj_wall);

                    }
                }
            }
        }
        
//Actually Generate Game Level (All the previous code was prep)
for (var yy = 0; yy < height; yy++)
    {
    for (var xx = 0; xx < width; xx++)
        {
        //Place Floor
        if (grid[# xx, yy] == cell_type.ground)
            {
            tile_add(choose(generation_floor,generation_floor,generation_floor_alt),0,0,cell_size,cell_size,xx*cell_size,yy*cell_size,0)
			if spawn_increment == 0 || 100 || 200 || 300 || 400 || 500 || 600 || 700 || 800 || 900 || 1000
				{
				spawn_x[spawn_increment/100] = xx * cell_size;
				spawn_y[spawn_increment/100] = yy * cell_size;
				}
}
			
		//Place Ceiling
        if (grid[# xx, yy] == cell_type.void)
            {
            tile_add(bg_ceiling,0,0,cell_size,cell_size,xx*cell_size,yy*cell_size,-room_height)
            }
			
        //Place Wall
        if (grid[# xx, yy] == cell_type.wall)
            {
            //tile_add(bg_ceiling,0,0,cell_size,cell_size,xx*cell_size,(yy-1)*cell_size,-room_height)
            }
        }
    }
    
//Place wall Tiles
// Get tile sizes
var tw = cell_size;
var th = cell_size;

// Add the tiles
for (var yy = 0; yy < height; yy++) {
    for (var xx = 0; xx < width; xx++) {
        if (grid[# xx, yy] == cell_type.ground) {
            // Get the tile's x and y
            var tx = xx*tw;
            var ty = yy*th;
            
            var right = grid[# (xx+1), yy] != cell_type.ground;
            var left = grid[# (xx-1), yy] != cell_type.ground;
            var top = grid[# xx, (yy-1)] != cell_type.ground;
            var bottom = grid[# xx, (yy+1)] != cell_type.ground;
            
            var top_right = grid[# (xx+1), (yy-1)] != cell_type.ground;
            var top_left = grid[# (xx-1), (yy-1)] != cell_type.ground;
            var bottom_right = grid[# (xx+1), (yy+1)] != cell_type.ground;
            var bottom_left = grid[# (xx-1), (yy+1)] != cell_type.ground;
            
            if (right) {
                if (bottom) {
                    tile_add(generation_tileset, tw*4, th*1, tw, th, tx+tw, ty, -ty);
                } else if (top) {
                    if (top_right) {
                        tile_add(generation_tileset, tw*4, th*0, tw, th, tx+tw, ty-th, -ty-th);
                    } else {
                        tile_add(generation_tileset, tw*3, th*0, tw, th, tx, ty-th, -ty-th);
                    }
                    tile_add(generation_tileset, tw*0, th*1, tw, th, tx+tw, ty, -ty);
                } else {
                    tile_add(generation_tileset, tw*0, th*1, tw, th, tx+tw, ty, -ty);
                }
            }
            
            if (left) {
                if (bottom) {
                    tile_add(generation_tileset, tw*3, th*1, tw, th, tx-tw, ty, -ty);
                } else if (top) {
                    if (top_left) {
                        tile_add(generation_tileset, tw*3, th*0, tw, th, tx-tw, ty-th, -ty-th);
                    } else {
                        tile_add(generation_tileset, tw*4, th*0, tw, th, tx, ty-th, -ty-th);
                    }
                    tile_add(generation_tileset, tw*2, th*1, tw, th, tx-tw, ty, -ty);
                } else {
                    tile_add(generation_tileset, tw*2, th*1, tw, th, tx-tw, ty, -ty);
                }
            }
            
            if (top) {
                if (!top_right) {
                    tile_add(generation_tileset, tw*2, th*2, tw, th, tx, ty-th, -ty);
                } else if (!top_left) {
                    tile_add(generation_tileset, tw*0, th*2, tw, th, tx, ty-th, -ty);
                } else {
                    tile_add(generation_tileset, tw*1, th*2, tw, th, tx, ty-th, -ty);
                }
            }
            
            if (bottom) {
                if (!bottom_right) {
                    tile_add(generation_tileset, tw*2, th*0, tw, th, tx, ty, -ty-tw);
                } else if (!bottom_left) {
                    tile_add(generation_tileset, tw*0, th*0, tw, th, tx, ty, -ty-tw);
                } else {
                    tile_add(generation_tileset, tw*1, th*0, tw, th, tx, ty, -ty-tw);
                }
            }
        }
    }
}
