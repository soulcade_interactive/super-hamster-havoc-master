/// @function ds_map_to_json_file(ds_map,file)
/// @description Convert a ds_map into a json and save it to a file.
/// @param ds_map ID of the ds_map
/// @param file Name of File

//Get Arguments
var ds_map = argument0;
var file = argument1;

if (ds_exists(ds_map, ds_type_map))
	{
	//Convert ds_map to JSON
	var json_data = json_encode(ds_map);
	internal_debug_console_message("ds_map was converted to a json string with the following output:" + json_data);

	//Convert json to buffer
	var json_buffer = json_string_to_buffer(json_data);


	//Save File
	buffer_save(json_buffer,file);
	buffer_delete(json_buffer);
	return true;
	}

else
	{
	return false;	
	}