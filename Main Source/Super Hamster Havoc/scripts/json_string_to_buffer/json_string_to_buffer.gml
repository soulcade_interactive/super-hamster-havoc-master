/// @function json_string_to_buffer
/// @description Takes a JSON string and puts it in a buffer
/// @param json The JSON string

//Get Arguments
var _json_string = argument0;

//Create Properly sized buffer and adds json string to it
var _json_buffer = buffer_create(string_byte_length(_json_string) + 1, buffer_fixed, 1);
buffer_write(_json_buffer, buffer_string, _json_string);

//Debug Output
internal_debug_console_message("json string was written to a buffer (" + string(_json_buffer) + "), with a total size of " + string(buffer_get_size(_json_buffer)) + " bytes.");

return _json_buffer;