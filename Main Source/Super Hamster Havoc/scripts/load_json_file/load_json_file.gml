/// @function load_json_file
/// @description Takes a JSON file, loads it, and returns it as a map
/// @param file The JSON file

var _file = argument0;

if file_exists(_file)
	{
	var _buffer = buffer_load(_file);
	var _string = buffer_read(_buffer,buffer_string);
	buffer_delete(_buffer);

	var _json_to_ds_map = json_decode(_string);
	return _json_to_ds_map;
	}
	
else
	{
	return -1;	
	}