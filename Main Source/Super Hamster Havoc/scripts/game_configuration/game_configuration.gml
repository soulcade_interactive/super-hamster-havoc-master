globalvar game_config;
game_config = os_get_config();

//Default Controller UI Configuration (Xbox One Input)
globalvar ui_a_button,ui_b_button,ui_x_button,ui_y_button,ui_dpad,ui_lb,ui_rb,ui_lstick,ui_rstick,ui_start,ui_select;

//Action Buttons
ui_a_button = spr_xboxone_a;
ui_b_button = spr_xboxone_b;
ui_x_button = spr_xboxone_x;
ui_y_button = spr_xboxone_y;

//Dpad
ui_dpad = spr_xboxone_dpad;

//Bumpers
ui_lb = spr_xboxone_lb;
ui_rb = spr_xboxone_rb;

//Triggers
ui_lb = spr_xboxone_lb;
ui_rb = spr_xboxone_rb;

//Joysticks
ui_lstick = spr_xboxone_lstick;
ui_rstick = spr_xboxone_rstick;

//Start & Select
ui_start = spr_xboxone_start;
ui_select = spr_xboxone_select;

switch(game_config)
	{
	case "Desktop":
	//Initialize Discord Rich Presence
	#region
	rousr_dissonance_create("467861661261234176");
	rousr_dissonance_handler_on_ready(scr_on_ready, noone);
	rousr_dissonance_handler_on_disconnected(scr_on_disconnect, noone);
	rousr_dissonance_handler_on_error(scr_on_error, noone);
	rousr_dissonance_handler_on_join(scr_on_join_game, noone);
	rousr_dissonance_handler_on_spectate(scr_on_spectate_game, noone);
	rousr_dissonance_handler_on_join_request(scr_on_join_request, noone);
	rousr_dissonance_set_details("Getting my controllers ready...");
	rousr_dissonance_set_state("About to Play");
	rousr_dissonance_set_party(1, 8);
	#endregion
	break;
	
	//STEAM CONFIGURATION
	case "Steam":
	//Initialize Discord Rich Presence
	#region
	rousr_dissonance_create("467861661261234176");
	rousr_dissonance_handler_on_ready(scr_on_ready, noone);
	rousr_dissonance_handler_on_disconnected(scr_on_disconnect, noone);
	rousr_dissonance_handler_on_error(scr_on_error, noone);
	rousr_dissonance_handler_on_join(scr_on_join_game, noone);
	rousr_dissonance_handler_on_spectate(scr_on_spectate_game, noone);
	rousr_dissonance_handler_on_join_request(scr_on_join_request, noone);
	rousr_dissonance_set_details("Getting my controllers ready...");
	rousr_dissonance_set_state("About to Play");
	rousr_dissonance_set_party(1, 8);
	#endregion
	break;
	
	//DISCORD CONFIGURATION
	case "Discord":
	//Initialize Discord Rich Presence
	#region
	rousr_dissonance_create("467861661261234176");
	rousr_dissonance_handler_on_ready(scr_on_ready, noone);
	rousr_dissonance_handler_on_disconnected(scr_on_disconnect, noone);
	rousr_dissonance_handler_on_error(scr_on_error, noone);
	rousr_dissonance_handler_on_join(scr_on_join_game, noone);
	rousr_dissonance_handler_on_spectate(scr_on_spectate_game, noone);
	rousr_dissonance_handler_on_join_request(scr_on_join_request, noone);
	rousr_dissonance_set_details("Getting my controllers ready...");
	rousr_dissonance_set_state("About to Play");
	rousr_dissonance_set_party(1, 8);
	#endregion
	break;
	
	//ITCH.IO STORE CONFIGURATION
	case "Itchio":
	//Initialize Discord Rich Presence
	#region
	rousr_dissonance_create("467861661261234176");
	rousr_dissonance_handler_on_ready(scr_on_ready, noone);
	rousr_dissonance_handler_on_disconnected(scr_on_disconnect, noone);
	rousr_dissonance_handler_on_error(scr_on_error, noone);
	rousr_dissonance_handler_on_join(scr_on_join_game, noone);
	rousr_dissonance_handler_on_spectate(scr_on_spectate_game, noone);
	rousr_dissonance_handler_on_join_request(scr_on_join_request, noone);
	rousr_dissonance_set_details("Getting my controllers ready...");
	rousr_dissonance_set_state("About to Play");
	rousr_dissonance_set_party(1, 8);
	#endregion
	break;
	
	//KARTRIDGE STORE CONFIGURATION
	case "Kartridge":
	//Initialize Discord Rich Presence
	#region
	rousr_dissonance_create("467861661261234176");
	rousr_dissonance_handler_on_ready(scr_on_ready, noone);
	rousr_dissonance_handler_on_disconnected(scr_on_disconnect, noone);
	rousr_dissonance_handler_on_error(scr_on_error, noone);
	rousr_dissonance_handler_on_join(scr_on_join_game, noone);
	rousr_dissonance_handler_on_spectate(scr_on_spectate_game, noone);
	rousr_dissonance_handler_on_join_request(scr_on_join_request, noone);
	rousr_dissonance_set_details("Getting my controllers ready...");
	rousr_dissonance_set_state("About to Play");
	rousr_dissonance_set_party(1, 8);
	#endregion
	break;
	
	// XBOX ONE CONFIGURATION
	case "XboxOne":
	#region
	
	#endregion
	break;
	
	// NINTENDO SWITCH CONFIGURATION
	case "NintendoSwitch":
	#region
	#region
		//Action Buttons
		ui_a_button = spr_switch_a;
		ui_b_button = spr_switch_b;
		ui_x_button = spr_switch_x;
		ui_y_button = spr_switch_y;

		//Dpad
		ui_dpad = spr_switch_dpad;

		//Bumpers
		ui_lb = spr_switch_lb;
		ui_rb = spr_switch_rb;

		//Triggers
		ui_lb = spr_switch_lb;
		ui_rb = spr_switch_rb;

		//Joysticks
		ui_lstick = spr_switch_lstick;
		ui_rstick = spr_switch_rstick;

		//Start & Select
		ui_start = spr_switch_plus;
		ui_select = spr_switch_minus;
		#endregion
	#endregion
	break;
	
	// PLAYSTATION 4 CONFIGURATION
	case "Playstation4":
	#region
	
	//Playstation 4 UI
	#region
		//Action Buttons
		ui_a_button = spr_ps4_cross;
		ui_b_button = spr_ps4_circle;
		ui_x_button = spr_ps4_square;
		ui_y_button = spr_ps4_triangle;

		//Dpad
		ui_dpad = spr_ps4_dpad;

		//Bumpers
		ui_lb = spr_ps4_lb;
		ui_rb = spr_ps4_rb;

		//Triggers
		ui_lb = spr_ps4_lb;
		ui_rb = spr_ps4_rb;

		//Joysticks
		ui_lstick = spr_ps4_lstick;
		ui_rstick = spr_ps4_rstick;

		//Start & Select
		ui_start = spr_ps4_options;
		ui_select = spr_ps4_share;
		#endregion
	#endregion
	break;
	
	case "default":
	default:
	break;
	}