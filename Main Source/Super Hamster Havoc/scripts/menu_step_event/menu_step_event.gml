if (live_call()) return live_result;

switch(menu_current_state)
	{
	//***************************************************************** MAIN MENU *****************************************************************
	case menu_state.main:
	//Navigation sounds
	if _left[0]
	or _right[0]
	or _right_pressed_alt[0]
	or _left_pressed_alt[0]
		{
		audio_play_sound(snd_navigation_sound,1,false);
		}
	
	//Menu Transition
	if _a_button[0]
		{
		gamepad_set_vibration(gamepad[0],0.5,0.5);
		alarm[0] = room_speed/6;
		switch(main_menu_select)
			{
			case 0:
			audio_play_sound(snd_selection_sound,1,false);
			internal_debug_console_message("Menu switched to menu_state.settings_main");
			transition_menu_state(menu_state.settings_main);
			break;
			
			case 1:
			internal_debug_console_message("Menu switched to menu_state.horde_mode_player_select");
			audio_play_sound(snd_back_sound,1,false);
			//transition_menu_state(menu_state.horde_mode_player_select);
			break;
			
			case 2:
			audio_play_sound(snd_selection_sound,1,false);
			internal_debug_console_message("Menu switched to menu_state.warfare_settings");
			transition_menu_state(menu_state.warfare_settings);
			break;
			
			case 3:
			audio_play_sound(snd_back_sound,1,false);
			//internal_debug_console_message("Event triggered for Create A Hamster Menu Slot");
			break;
			
			case 4:
			audio_play_sound(snd_back_sound,1,false);
			internal_debug_console_message("Menu switched to menu_state.forge_mode_select");
			//transition_menu_state(menu_state.forge_mode_select);
			break;
			}
		}
	
	break;
	
	//***************************************************************** WARFARE SUBMENUS *****************************************************************
	/***** WARFARE GAMEMODE SELECT*****/
	case menu_state.warfare_settings:
	#region
	var gamemode_amount = ds_list_size(gamemode);
	
	
	//Move Map Selection
	gamepad_set_axis_deadzone(gamepad[0],0.95)
	gamemode_select += (-_up[0] + _down[0]);
	gamemode_select += (-_up_pressed_alt[0] + _down_pressed_alt[0]);
	if _up[0]
	or _down[0]
	or _up_pressed_alt[0]
	or _down_pressed_alt[0]
		{
		audio_play_sound(snd_navigation_sound,1,false);
		}
	gamemode_select = clamp(gamemode_select,0,gamemode_amount-1);
	//Select gamemode
	if (_a_button[0])
		{
		var gamemode_data = json_decode(gamemode[| gamemode_select]);
		var gamemode_player_min =  real(gamemode_data[? "min_players"]);
		ds_map_destroy(gamemode_data);
		
		if gp_amount >= gamemode_player_min
			{
			audio_play_sound(snd_selection_sound,1,false);
			gamepad_set_vibration(gamepad[0],0.5,0.5);
			alarm[0] = room_speed/6;
			transition_menu_state(menu_state.warfare_player_select);
			}
		
		else
			{
			audio_play_sound(snd_back_sound,1,false);
			gamepad_set_vibration(gamepad[0],0.5,0.5);
			alarm[0] = room_speed/6;
			}
		
		}
	//Back Button
	if _b_button[0]
		{
		audio_play_sound(snd_back_sound,1,false);
		internal_debug_console_message("Menu switched to menu_state.warfare_map_select");
		transition_menu_state(menu_state.main);
		}
	#endregion
	break;
	//**** WARFARE MODE PLAYER SELECT****
	case menu_state.warfare_player_select:
	#region
	//Move Player Select Cursor

	//Confirm whether or not all players are locked in
	var players_ready = 0; 
	for(var i = 0; i < gp_amount; i++)
		{
		if player_character_id[i] != noone
			{
			players_ready++;	
			}
		}
		
	if players_ready == gp_amount
	//&& gp_amount > 1
		{
			players_are_ready = true;
		}
		
	if players_ready != gp_amount
		{
		players_are_ready = false;
		}
	//Character Select Panels & Cursors
	for(i=0;i<=gp_amount-1;i++)
		{
		if player_character_id[i] == noone
		{
		player_cursor_x[i] += gamepad_axis_value(gamepad[i],gp_axislh) * 12;
		player_cursor_y[i] += gamepad_axis_value(gamepad[i],gp_axislv) * 12;
		}
		player_cursor_x[i] = clamp(player_cursor_x[i],0+32,1920-32);
		player_cursor_y[i] = clamp(player_cursor_y[i],0+32,1080-32)
		}
		
	
	//Assigns Chacter
			for(var i=0; i<array_length_1d(character_roster); i++)
				{
				if !ds_list_empty(character_roster[i])
					{
					var roster_row_ammount = ds_list_size(character_roster[i]);
					var x_offset = 960 - (72 * roster_row_ammount)
					var y_offset = 128;
					for(var c=0; c<roster_row_ammount; c++)
						{
						//Draw indicator if cursor over character
						for(g=0;g<=gp_amount-1;g++)
							{
							if point_in_rectangle(player_cursor_x[g],player_cursor_y[g],x_offset + (128*c),(128 * i) + y_offset,x_offset + (128*c) + 128,128 + (128 * i) + y_offset)
							&& _a_button[g]
								{
								audio_play_sound(snd_selection_sound,1,false);
								gamepad_set_vibration(gamepad[g],0.5,0.5);
								alarm[0] = room_speed/6;
								player_character_id[g] = ds_map_find_value(ds_list_find_value(character_roster[i],c),"character_id")
								player_character_data[g] = json_encode(ds_list_find_value(character_roster[i],c));
								}
							}
						}
					}
				}
				
	//Back Button
	if _b_button[0] && (player_character_id[0] == noone)
		{
		audio_play_sound(snd_back_sound,1,false);
		internal_debug_console_message("Menu switched to menu_state.main");
		transition_menu_state(menu_state.warfare_settings);
		}
		
	for(var g=0; g<= gp_amount; g++)
		{
		if _b_button[g] && (player_character_id[g] != noone)
			{
			player_character_id[g] = noone;
			player_character_data[g] = noone;
			}
		}
	//Continue to level select if all characters are locked in 
	if players_are_ready == true
	//&& gp_amount > 1
		{
		for(var i = 0; i < gp_amount; i++)
			{
			if _start[i]
				{
				audio_play_sound(snd_start_sound,1,false);
				for(var g = 0; g < gp_amount; g++)
					{
					gamepad_set_vibration(gamepad[g],0.5,0.5);
					}
				alarm[0] = room_speed/6;
				internal_debug_console_message("Players advanced to level selection")
				transition_menu_state(menu_state.warfare_map_select);
				}
			}
		}
	break;
	#endregion
	
	//**** WARFARE MAP SELECTION ****
	
	case menu_state.warfare_map_select:
	#region
	//Clamp the Map Selection
	var map_amount = ds_list_size(maps);
	
	
	//Move Map Selection
	gamepad_set_axis_deadzone(gamepad[0],0.95)
	map_select += (-_up[0] + _down[0]);
	map_select += (-_up_pressed_alt[0] + _down_pressed_alt[0]);
	if _up[0]
	or _down[0]
	or _up_pressed_alt[0]
	or _down_pressed_alt[0]
		{
		audio_play_sound(snd_navigation_sound,1,false);
		}
	map_select = clamp(map_select,0,map_amount-1);
	//Select Map
	if (_a_button[0])
		{
		audio_play_sound(snd_selection_sound,1,false);
		gamepad_set_vibration(gamepad[0],0.5,0.5);
		if map_select != map_amount
			{
			var level_data = json_decode(maps[| map_select]);
			var level_goto =  real(level_data[? "room_id"]);
			ds_map_destroy(level_data);
			gamepad_set_vibration(gamepad[0],0,0);
			room_goto(level_goto);
			}
		else
			{
			var random_map = irandom_range(0,map_amount)	
			var level_data = json_decode(maps[| random_map]);
			var level_goto =  real(level_data[? "room_id"]);
			ds_map_destroy(level_data);
			gamepad_set_vibration(gamepad[0],0,0);
			room_goto(level_goto);
			}
		}
	//Back Button
	if _b_button[0]
		{
		audio_play_sound(snd_back_sound,1,false);
		internal_debug_console_message("Menu switched to menu_state.warfare_map_select");
		transition_menu_state(menu_state.warfare_player_select);
		}
	#endregion
	break;
	
	
	//**** HORDE MODE PLAYER SELECT ****
	case menu_state.horde_mode_player_select:
	//Back Button
	if _b_button[0]
		{
		audio_play_sound(snd_back_sound,1,false);
		internal_debug_console_message("Menu switched to menu_state.main");
		transition_menu_state(menu_state.main);
		}
	break;
	
	//***************************************************************** HORDE MODE SUBMENUS *****************************************************************
	//**** HORDE MODE SETTINGS ****
 	case menu_state.horde_mode_settings:
	//Back Button
	if _b_button[0]
		{
		audio_play_sound(snd_back_sound,1,false);
		internal_debug_console_message("Menu switched to menu_state.horde_mode_player_select");
		transition_menu_state(menu_state.horde_mode_player_select);
		}
	break;
	//**** HORDE MODE MAP SELECT ****
	case menu_state.horde_mode_map_select:
	//Back Button
	if _b_button[0]
		{
		audio_play_sound(snd_back_sound,1,false);
		internal_debug_console_message("Menu switched to menu_state.horde_mode_map_select");
		transition_menu_state(menu_state.horde_mode_map_select);
		}
	break;
	
	//***************************************************************** SETTINGS SUBMENUS *****************************************************************
	//**** SETTINGS ****
	case menu_state.settings_main:
	
	
	//Input Code
	
	//Navigation (^ and v on the dpad)
	var items = array_length_1d(settings_text) -1;
	var navigation = (-_up[0]-_up_pressed_alt[0]) + (_down[0] + _down_pressed_alt[0]);
	if navigation != 0	
		{
		audio_play_sound(snd_navigation_sound,1,false);	
		}
	main_settings_select -= navigation;
	if main_settings_select < 0
		{
		main_settings_select = items;
		}
		
	if main_settings_select > items
		{
		main_settings_select = 0;	
		}
	//Select Button (A)
	if _a_button[0]
		{
		audio_play_sound(snd_selection_sound,1,false);
		gamepad_set_vibration(gamepad[0],0.5,0.5);
		alarm[0] = room_speed/6;
		switch(main_settings_select)
			{
			//Back Button
			case 0:
			internal_debug_console_message("Menu switched to menu_state.main");
			transition_menu_state(menu_state.main);
			break;
			
			//Reset all Settings
			case 1:
			internal_debug_console_message("Reset all Settings");
			break;
			
			//Controller Settings
			case 2:
			internal_debug_console_message("Menu switched to menu_state.settings_controller");
			transition_menu_state(menu_state.settings_controller);
			break;
			
			//Audio Settings
			case 3:
			internal_debug_console_message("Menu switched to menu_state.main");
			transition_menu_state(menu_state.settings_audio);
			break;
			
			//Graphics Settings
			case 4:
			internal_debug_console_message("Menu switched to menu_state.settings_video");
			transition_menu_state(menu_state.settings_video);
			
			break; 
			}
		}
	//Back Button (B)
	if _b_button[0]
		{
		audio_play_sound(snd_back_sound,1,false);
		internal_debug_console_message("Menu switched to menu_state.main");
		transition_menu_state(menu_state.main);
		}
	break;
	
					//**** SETTINGS - VIDEO ****
					case menu_state.settings_video:
					//Back Button
					if _b_button[0]
						{
						audio_play_sound(snd_back_sound,1,false);
						internal_debug_console_message("Menu switched to menu_state.settings_main");
						transition_menu_state(menu_state.settings_main);
						}
					break;
	
					//**** SETTINGS - CONTROLLER ****
					case menu_state.settings_controller:
					//Back Button
					if _b_button[0]
						{
						audio_play_sound(snd_back_sound,1,false);
						internal_debug_console_message("Menu switched to menu_state.settings_main");
						transition_menu_state(menu_state.settings_main);
						}
					break;
	
					//**** SETTINGS - MEMORY ****
					case menu_state.settings_memory:
					//Back Button
					if _b_button[0]
						{
						audio_play_sound(snd_back_sound,1,false);
						internal_debug_console_message("Menu switched to menu_state.settings_main");
						transition_menu_state(menu_state.settings_main);
						}
					break;
	
	//***************************************************************** FORGE MODE CREATE OR VIEW MAPS *****************************************************************
	case menu_state.forge_mode_select:
	//Back Button
	if _b_button[0]
		{
		audio_play_sound(snd_back_sound,1,false);
		internal_debug_console_message("Menu switched to menu_state.main");
		transition_menu_state(menu_state.main);
		}
	break;
	
	
	default:
	break;
	}
	
//***************************************************************** Menu Transition Fade in & Fade Out System *****************************************************************
if menu_transition == true
&& menu_transition_target != noone
	{
		switch(current_transition_state)
			{
			//Fade In
			case transition_state.fade_in:
				menu_transition_alpha = lerp(menu_transition_alpha,1,0.4);
				
				if menu_transition_alpha == 1
					{
					menu_current_state = menu_transition_target;
					
					current_transition_state = transition_state.fade_out;
					}
			break;
			
			//Fade Out
			case transition_state.fade_out:
			menu_transition_alpha = lerp(menu_transition_alpha,0,0.25);
				
				if menu_transition_alpha == 0
					{
					menu_transition = false;
					menu_transition_target = noone;
					}
			break
			}
	}