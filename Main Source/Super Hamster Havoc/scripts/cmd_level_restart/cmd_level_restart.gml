// @description Reset the game
// @param arguments an array containing any arguments needed by the script
var arguments = argument0;
room_restart();
