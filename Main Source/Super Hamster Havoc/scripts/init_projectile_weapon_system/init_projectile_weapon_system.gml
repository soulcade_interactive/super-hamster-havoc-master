///Initializes the system for registering projectile weapons
if (live_call()) return live_result;
//Create a global scope list for storing gun information
globalvar projectile_weapon;

//Create Enumerates for different gun types
enum gun_type
	{
	pistol,
	rifle,
	shotgun,
	assault_rifle,
	sniper,
	rocket_launcher,
	lazer,
	bow,
	special
	}
	
//Create Enumerates for diiferent firerates types
enum gun_fire_rate_type
	{
	full_auto,
	semi_auto,
	single_shot,
	pump_action
	}
	
//Create Enumerates for different projectiles
enum gun_projectile
	{
	regular_bullet,
	rifle_bullet,
	slug,
	arrow,
	explosive_arrows,
	rpg
	}

