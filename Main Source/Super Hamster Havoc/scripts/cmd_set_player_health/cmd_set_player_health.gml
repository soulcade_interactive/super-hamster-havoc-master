// @description Sets the current weapon of a player
// @param arguments an array containing any arguments needed by the script
var arguments = argument0;

if is_undefined(players[arguments[0]])
	{
	log += chr(13) + "Player with the ID "+ string(arguments[0]) +" has not been found."
	}

else
	{
	var target_player = players[arguments[0]].id;
	var target_hp = arguments[1];
	
	target_player.hp = target_hp;
	}

