/// @function show_debug_message(string)
/// @description Send a string to both the GMS2 Debug Console and the In-Game Debug Console
/// @param string

var _string = string(argument0);

if instance_exists(obj_debug_console)
	{
		with(obj_debug_console)
			{
			log += chr(13) + _string;
			}
	}

show_debug_message(_string);