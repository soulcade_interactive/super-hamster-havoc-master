/// @function number_to_bool(num)
/// @description Converts a number to a bool
/// @param integer Number to convert

var number = argument0;

switch(abs(round(clamp(number,0,1))))
	{
	case 0:
	return false;
	break;
	
	case 1:
	return true;
	break;
	}