/// @function init_camera(camera_amount,camera_id)
/// @description Convert a ds_map into a json and save it to a file.
/// @param camera_amount The amount of cameras that will be displayed on the screen
/// @param camera_id The ID of the camera that is being created

//Get Arguments
var camera_amount = argument0;
var camera_id = argument1;

//Enable the use of views
view_enabled = true;

//Get the width and height based on amount of players
#region
switch(camera_amount)
	{
	case 1:
	var camera_w = 1920;
	var camera_h = 1080;
	break;
	
	case 2:
	var camera_w = 960;
	var camera_h = 1080;
	break;
	
	case 3:
	var camera_w = 960;
	var camera_h = 540;
	break;
	
	case 4:
	var camera_w = 960;
	var camera_h = 540;
	break;
	
	case 5:
	var camera_w = 960;
	var camera_h = 360;
	break;
	
	case 6:
	var camera_w = 960;
	var camera_h = 360;
	break;
	
	case 7:
	var camera_w = 640;
	var camera_h = 360;
	break;
	
	case 8:
	var camera_w = 640;
	var camera_h = 360;
	break;
	
	default:
	var camera_w = 1920;
	var camera_h = 1080;
	break;
	}
#endregion

//Setup Camera and Views

//Make view "i" visible
view_set_visible(camera_id, true);
	
//Set the port bounds of view "i" to the assigned dimensions
view_set_wport(camera_id, camera_w);
view_set_hport(camera_id, camera_h);
	
//Build a camera suited for 2D
var _camera = camera_create_view(0, 0, camera_w, camera_h, 0, -1, -1, -1, -1, -1);
	
//Set view "i" to use the camera "camera"
view_set_camera(camera_id, _camera);

//Set the camera to the position of it's player
camera_set_view_pos(view_camera[camera_id], players[camera_id].x, players[camera_id].y);

/*
camera = camera_create();
camera_set_dimensions();
var vm = matrix_build_lookat(x,y,-10,x,y,1,0,1,0);
var pm = matrix_build_projection_ortho(camera_width,camera_height,1,10000);
			
camera_set_view_mat(camera,vm);
camera_set_proj_mat(camera,pm);

player_id = argument0;
follow = players[argument0].id;

//Enable the use of views
view_enabled = true;

//Make view 0 visible
view_set_visible(0, true);

//Set the port bounds of view 0 to 640x480
view_set_wport(argument0, camera_width);
view_set_hport(argument0, camera_height);
view_camera[argument0] = camera;