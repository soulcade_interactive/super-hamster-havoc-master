/// @description gamepad_anybutton_pressed(gamepad_id) Checks in any button on a given controller is pressed
/// @param gamepad_id

//Gets Gamepad ID
var gp_id = argument0;

//Checks to see in any button on the gamepad is pressed and returns whether of not it has

//If True (This region is a long if/or list of all the gamepad inputs)
#region
if gamepad_button_check(gp_id, gp_face1)
or gamepad_button_check(gp_id, gp_face2)
or gamepad_button_check(gp_id, gp_face3)
or gamepad_button_check(gp_id, gp_face4)
or gamepad_button_check(gp_id, gp_padu)
or gamepad_button_check(gp_id, gp_padd)
or gamepad_button_check(gp_id, gp_padl)
or gamepad_button_check(gp_id, gp_padr)
or gamepad_button_check(gp_id, gp_shoulderl)
or gamepad_button_check(gp_id, gp_shoulderr)
or gamepad_button_check(gp_id, gp_shoulderlb)
or gamepad_button_check(gp_id, gp_shoulderrb)
or gamepad_button_check(gp_id, gp_shoulderl)
or gamepad_button_check(gp_id, gp_start)
or gamepad_button_check(gp_id, gp_select)
or gamepad_button_check(gp_id, gp_stickl)
or gamepad_button_check(gp_id, gp_stickr)
	{
	return true;	
	}
#endregion
//If not true, then it's false

else
	{
	return false;	
	}