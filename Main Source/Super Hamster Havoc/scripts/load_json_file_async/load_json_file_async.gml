/// @function load_json_file_
/// @description Takes a JSON file, loads it, and returns it as a map
/// @param file The JSON file

var _file = argument0;

if file_exists(_file)
	{
	//Get size of file
	internal_debug_console_message("Confirmed that " + _file + "exists");
	var _file_temp = file_bin_open(_file, 0);
	var _size = file_bin_size(_file_temp);
	file_bin_close(_file_temp);
	internal_debug_console_message("Got file size of " + string(_size) + "bytes for " + _file);
	
	//Create the buffer that the data will be loaded into, and begin asyc loading of data
	var _temp_load_buffer = buffer_create(_size, buffer_fixed, 1);
	internal_debug_console_message("Created Buffer for storing the loaded data with an id of " + string(_temp_load_buffer) + "and a size of " + string(buffer_get_size(_temp_load_buffer)));
	var _settings_load_callback_id = buffer_load_async(_temp_load_buffer, _file, 0, _size);
	internal_debug_console_message("Started async loading of " + string(_file) + " to buffer " + string(_temp_load_buffer));
	//Create a ds_map with the buffer id and the callback id for the async loading
	var ds_map_return = ds_map_create();
	ds_map_add(ds_map_return,"buffer_id",_temp_load_buffer);
	ds_map_add(ds_map_return,"settings_load_callback_id",_settings_load_callback_id);
	
	return ds_map_return;
	}
	
else
	{
	internal_debug_console_message("File did not exist at " + string(_file));
	return -1;	
	}