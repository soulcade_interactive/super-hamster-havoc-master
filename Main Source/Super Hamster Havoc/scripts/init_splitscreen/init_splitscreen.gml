/// @function init_splitscreen()
/// @description Create a camera and bind it to a view
/// @param Amount The amount of viewports to set the positions of

var view_amount = argument0;

switch(view_amount)
	{
	case 1:
	view_xport[0] = 0;
	view_yport[0] = 0;
	break;
	
	case 2:
	view_xport[0] = 0;
	view_yport[0] = 0;
	
	view_xport[1] = 960;
	view_yport[1] = 0;
	break;
	
	case 3:
	view_xport[0] = 0;
	view_yport[0] = 0;
	
	view_xport[1] = 960;
	view_yport[1] = 0;
	
	view_xport[2] = (1920/2) - (view_wport[2]/2);
	view_yport[2] = 540;
	break;
	
	case 4:
	view_xport[0] = 0;
	view_yport[0] = 0;
	
	view_xport[1] = 960;
	view_yport[1] = 0;
	
	view_xport[2] = 0;
	view_yport[2] = 540;
	
	view_xport[3] = 960;
	view_yport[3] = 540;
	break;
	
	case 5:
	view_xport[0] = 0;
	view_yport[0] = 0;
	
	view_xport[1] = 960;
	view_yport[1] = 0;
	
	view_xport[2] = 0;
	view_yport[2] = 360;
	
	view_xport[3] = 960;
	view_yport[3] = 360;
	
	view_xport[4] = (1920/2) - (view_wport[2]/2);
	view_yport[4] = 720;
	break;
	
	case 6:
	view_xport[0] = 0;
	view_yport[0] = 0;
	
	view_xport[1] = 960;
	view_yport[1] = 0;
	
	view_xport[2] = 0;
	view_yport[2] = 360;
	
	view_xport[3] = 960;
	view_yport[3] = 360;
	
	view_xport[4] = 0;
	view_yport[4] = 720;
	
	view_xport[5] = 960;
	view_yport[5] = 720;
	break;
	
	case 7:
	cw = 1920/3;
	ch = 1080/3;
	view_xport[0] = 960-cw;
	view_yport[0] = 0;
	
	view_xport[1] = 960;
	view_yport[1] = 0;
	
	view_xport[2] = 0;
	view_yport[2] = ch;
	
	view_xport[3] = cw;
	view_yport[3] = ch;
	
	view_xport[4] = cw*2;
	view_yport[4] = ch;
	
	view_xport[5] = 960-cw;
	view_yport[5] = 720;
	
	view_xport[6] = 960;
	view_yport[6] = 720;
	break;
	
	case 8:
	cw = 1920/3;
	ch = 1080/3;
	view_xport[0] = 0;
	view_yport[0] = 0;
	
	view_xport[1] = cw;
	view_yport[1] = 0;
	
	view_xport[2] = cw*2;
	view_yport[2] = 0;
	
	view_xport[3] = 0;
	view_yport[3] = ch;
	
	view_xport[4] = cw*2;
	view_yport[4] = ch;
	
	view_xport[5] = 0;
	view_yport[5] = 720;
	
	view_xport[6] = cw;
	view_yport[6] = 720;
	
	view_xport[7] = cw*2;
	view_yport[7] = 720;
	break;
	}