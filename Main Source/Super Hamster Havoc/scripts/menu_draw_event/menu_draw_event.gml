 if (live_call()) return live_result;

switch(menu_current_state)
	{
	case menu_state.main:
	#region
	//Define Draw Settings
	var gwidth = 1920;
	var gheight = 1080;
	var center_x = gwidth/2;
	var center_y = gheight/2;
	var start_x = (gwidth/5);
	start_y = lerp(start_y,gheight * 0.40,0.1);
	logo_y = lerp(logo_y,190,0.35);
	
	//Set Offset Positions for the Menu
	var _xstart = 960;
	_ystart = lerp(_ystart,540,0.35);
	menu_angle = 200;
	
	//Set Panel Colors
	var color_red = make_color_rgb(114,0,8);
	var color_red_dark = make_color_rgb(90,0,17);

	//Get Input
	if ((_right_alt[0] + _left_alt[0] + _up_alt[0] +_down_alt[0]) > 0)
		{
		var d = _right_pressed_alt[0] - _left_pressed_alt[0];
		}
	else
		{
		var d = gamepad_button_check_pressed(gamepad[0],gp_padr) - gamepad_button_check_pressed(gamepad[0],gp_padl);
		}
	
	//Rotate Menu
	if (d != 0) {
	    main_menu_select = (main_menu_select + d) % count;
	    if (main_menu_select < 0) main_menu_select += count;
	}
	rot += angle_difference(main_menu_select/(count)*menu_angle+90, rot) * 0.2;
	
	//Sort the items based on expected Y:
	for (var i = 0; i < count; i += 1) {
	    var d = -i/(count)*menu_angle + rot;
	    var dy = -lengthdir_y(25, d);
	    ds_priority_add(pri, i, dy);
	}
	// draw the items:
	repeat (count) {
	    var i = ds_priority_delete_min(pri);
	    var d = -i/(count) * menu_angle + rot;
	    var dx = lengthdir_x(480, d); //lengthdir_x(200, d);
	    var dy = -lengthdir_y(30, d);
	    var r = 140 + dy;
	    if i == main_menu_select{draw_set_color(make_color_rgb(114,0,8));}
	    else{draw_set_color(make_color_rgb(33+(dy/3),33+(dy/3),33+(dy/2)));}
	    draw_rectangle(_xstart + dx-r*1.45,_ystart -r,_xstart + dx+r*1.45,_ystart +r,false);
		draw_set_color(c_black);
		draw_set_font(fnt_bebas_kai_24_pt);
		draw_set_halign(fa_center);
		draw_text(_xstart + dx, _ystart + dy + 60 + 2, text[i])
		draw_set_color(c_white);
		draw_text(_xstart + dx, _ystart + dy + 60, text[i])
		draw_sprite_ext(image[i],6,_xstart + dx, _ystart + dy - 48,0.6,0.6,0,c_white,1)
	}


	//Draw Flavor Text
	draw_set_alpha(0.85);
	draw_set_color(c_black);
	draw_rectangle(0,1080*0.825,1920,1080,false);
	draw_set_alpha(1);

	draw_set_color(c_white);
	draw_set_font(fnt_bebas_kai_24_pt);
	draw_set_halign(fa_center);
	draw_text(960,1080*0.855, flavor_text[main_menu_select])
	draw_set_font(fnt_bebas_kai_18_pt);
	draw_text(960,1080*0.9, flavor_sub_text[main_menu_select])

	
	start_x = 320;
	for(var i = 0; i < gp_amount; i++)
		{
			
		var controller_start_adjustment = (((gp_amount-1) * 64))/2
		var controller_x_pos = (960 - controller_start_adjustment) + (i * 64)
		draw_sprite(spr_controller_icon,4,controller_x_pos, 1080 - 32);
		//draw_sprite(spr_controller_icon,gamepad[i],960 + (64 * i), 1080 - 32); 
			
		if gamepad[i] != noone
			{
			if gamepad_anybutton_pressed(gamepad[i])
				{
				switch(i)
					{
					case 0:
					var indicator_color = c_red;
					break;
				
					case 1:
					var indicator_color = c_blue;
					break;
				
					case 2:
					var indicator_color = c_yellow;
					break;
				
					case 3:
					var indicator_color = c_green;
					break;
					
					case 4:
					var indicator_color = c_orange;
					break;
					
					case 5:
					var indicator_color = c_purple;
					break;
					
					case 6:
					var indicator_color = c_white;
					break;
					
					case 7:
					var indicator_color = c_dkgray;
					break;
					}
				draw_sprite_ext(spr_controller_icon,5,controller_x_pos, 1080 - 32,1,1,0,indicator_color,1);
				}
				
			
			}}
	
	//Draw Logo
	draw_sprite_ext(spr_game_wordmark,0,960,logo_y,2,2,0,c_white,1);
	
	draw_set_font(fnt_bebas_kai_24_pt);
	draw_set_halign(fa_center);
	draw_set_color(c_black);
	draw_text(center_x,782,"Press        to select");
	draw_set_color(c_white);
	draw_text(center_x,780,"Press        to select");
	image_speed = 0.25;
	draw_sprite_ext(ui_a_button,image_index,center_x - 18,800,0.5,0.5,0,c_white,1);
	draw_sprite_ext(ui_a_button,image_index,center_x - 18,794 + (sin(current_time/50) * 2),0.5,0.5,0,c_white,1);
	#endregion
	break;
	
//Draw Character Selection Screen
	case menu_state.warfare_player_select:
	#region
	var center_x = 1920/2;
	player_select_width = 1920;
	draw_set_color(c_black);
	draw_set_alpha(0.75);
	
	//Character Select Panels & Cursors
	var character_panel_width = (1664/(gp_amount));
	var character_panel_start_y = 740;
	var panel_start_offset = 128;
	var panel_margin = 24;
	
	draw_set_alpha(0.95);
	draw_rectangle(0,128,1920,128 + 128*4,false)
	draw_set_alpha(0.75);
		
	var char_panel_margin = 64;
	draw_set_alpha(0.8);
	draw_set_color(c_black);
	draw_rectangle(char_panel_margin,640,1920-char_panel_margin,1080,false);
	var panel_amount = gp_amount;
	panel_amount = clamp(panel_amount,4,8);
	var character_panel_width = (1920 - (char_panel_margin * 2))/panel_amount;
		
		for(var i=0; i<panel_amount; i++)
			{
			draw_set_alpha(0.75);
			draw_set_color(merge_color(c_black,get_player_color(i),0.5));
			var xx1 = char_panel_margin + (character_panel_width * i);
			var xx2 = xx1 + character_panel_width;
			var yy1 = 640;
			var yy2 = 1080;
			
			draw_rectangle(xx1,yy1,xx2,yy2,false);
			gpu_set_blendmode(bm_add);
			draw_set_alpha(0.3);
			draw_rectangle_color(xx1,yy1,xx2,yy2,c_black,c_black,c_white,c_white,false);
			draw_set_alpha(0.75);
			gpu_set_blendmode(bm_normal);
			draw_set_color(merge_color(c_dkgray,get_player_color(i),0.45));
			draw_set_alpha(1);
			draw_rectangle(xx1,yy1,xx2,yy1 + 48,false);
			draw_set_color(merge_color(c_black,get_player_color(i),0.5));
			draw_rectangle(xx1,yy1-2,xx2,yy1 + 48,true);
			draw_set_color(c_white);
			draw_set_font(fnt_bebas_kai_16_pt);
			draw_text((xx1+xx2)/2,yy1+14,"Player " + string(i + 1));
			
			if gamepad[i] == noone
				{
				draw_set_halign(fa_center);
				draw_set_valign(fa_center);
				var join_prompt_x = (xx1+xx2)/2;
				var join_prompt_y = (yy1+yy2)/2;
				draw_text(join_prompt_x,join_prompt_y,"Press \n\n\n\n to join game");
				draw_sprite_ext(ui_start,1,join_prompt_x,join_prompt_y+6,0.5,0.5,0,-1,1);
				draw_sprite_ext(ui_start,1,join_prompt_x,join_prompt_y + sin(current_time/75)*4,0.5,0.5,0,-1,1);
				draw_set_halign(fa_center);
				draw_set_valign(fa_top);
				}
			else
				{
				draw_set_halign(fa_center);
				draw_set_valign(fa_center);
				var character_prompt_x = (xx1+xx2)/2;
				var character_prompt_y = (yy1+yy2)/2;
				var offset_x = 48;
				if player_character_id[i] == noone
				{
				draw_set_color(c_white);
				draw_set_font(fnt_bebas_kai_18_pt);
				draw_text(character_prompt_x,character_prompt_y,"Pick a Character\n\n +");
				draw_sprite_ext(ui_a_button,1,character_prompt_x+offset_x,character_prompt_y+18+6,0.5,0.5,0,-1,1);
				draw_sprite_ext(ui_a_button,1,character_prompt_x+offset_x,character_prompt_y +18 + sin(current_time/75)*4,0.5,0.5,0,-1,1);
				//draw_sprite_ext(ui_lstick,1,character_prompt_x-offset_x,character_prompt_y+24+6,0.5,0.5,0,-1,1);
				draw_set_color(c_dkgray)
				draw_circle(character_prompt_x-offset_x,character_prompt_y+24+6,18,false);
				draw_set_alpha(0.25);
				draw_set_color(c_black)
				draw_circle(character_prompt_x-offset_x,character_prompt_y+24+6,16,false);
				draw_set_alpha(0.5);
				draw_circle(character_prompt_x-offset_x,character_prompt_y+24+6,12,false);
				draw_set_alpha(1);
				draw_sprite_ext(ui_lstick,1,character_prompt_x-offset_x + cos(current_time/75)*4,character_prompt_y+18+6 - sin(current_time/75)*4,0.4,0.4,0,-1,1);
				}
				if player_character_id[i] != noone
					{
					var character = player_character_id[i]
					var character_data = json_decode(player_character_data[i]);
					var player_type = real(character_data[? "character_type"]);
					var player_perks = character_data[? "character_perks"];
					var player_name = character_data[? "display_name"];
					ds_map_destroy(character_data);
					
					draw_set_color(merge_color(c_black,get_player_color(i),0.5));
					draw_rectangle(xx1+8,yy1+320,xx2-8,yy2,false);
					draw_set_color(merge_color(c_black,get_player_color(i),0.35));
					draw_rectangle(xx1+8,yy1+320,xx2-8,yy2-82,false);
					draw_set_alpha(0.5);
					draw_set_color(merge_color(c_black,get_player_color(i),0.4));
					draw_rectangle(xx1+8,yy1+320,xx2-8,yy2,true);
					draw_set_alpha(1);
					draw_set_color(c_white)
					//player_perks = "Testing";
					draw_set_valign(fa_top);
					draw_text(character_prompt_x,yy1+330,player_name);
					draw_set_halign(fa_left);
					draw_set_font(fnt_bebas_kai_16_pt);
					draw_text_ext(xx1+16,yy1+366,player_perks,24,character_panel_width-20);
					draw_set_valign(fa_center);
					draw_set_valign(fa_center);
					var head = 0;
					var torso = 0;
					var legs = 0;
					image_speed = 0.5
					
					player_scale = 2;
				
					switch(player_type)
						{
						case character_type.basic:
						draw_sprite_ext(spr_hamster_hand,0,character_prompt_x+12,character_prompt_y+35  + (sin(current_time/40)*5),player_scale,player_scale,0,c_white,1);
						draw_sprite_ext(asset_get_index("spr_" + character + "_0_head_r_running"),image_index,character_prompt_x,character_prompt_y,player_scale,player_scale,0,c_white,1);
						draw_sprite_ext(asset_get_index("spr_" + character + "_0_torso_r_running"),image_index,character_prompt_x,character_prompt_y,player_scale,player_scale,0,c_white,1);
						draw_sprite_ext(asset_get_index("spr_" + character + "_0_legs_r_running"),image_index,character_prompt_x,character_prompt_y,player_scale,player_scale,0,c_white,1);
						draw_sprite_ext(spr_hamster_hand,0,character_prompt_x-14,character_prompt_y+35  + (sin(current_time/50)*5),player_scale,player_scale,0,c_white,1);
						break;
					
						case character_type.buff:
						draw_sprite_ext(spr_buff_hamster_hand,0,character_prompt_x+36,character_prompt_y+12 + (sin(current_time/30)*2),player_scale,player_scale,(sin(current_time/40)*25),c_white,1);
						draw_sprite_ext(asset_get_index("spr_" + character + "_0_head_r_running"),image_index,character_prompt_x,character_prompt_y,player_scale,player_scale,0,c_white,1);
						draw_sprite_ext(asset_get_index("spr_" + character + "_0_torso_r_running"),image_index,character_prompt_x,character_prompt_y,player_scale,player_scale,0,c_white,1);
						draw_sprite_ext(asset_get_index("spr_" + character + "_0_legs_r_running"),image_index,character_prompt_x,character_prompt_y,player_scale,player_scale,0,c_white,1);
						draw_sprite_ext(spr_buff_hamster_hand,0,character_prompt_x-42,character_prompt_y+12 + (sin(current_time/30)*5),player_scale,player_scale,(sin(current_time/50)*25),c_white,1);
						break;
						}
					}	
					
				draw_set_halign(fa_center);
				draw_set_valign(fa_top);
				}
			}
		//Draw Character Select
			for(var i=0; i<array_length_1d(character_roster); i++)
				{
				if !ds_list_empty(character_roster[i])
					{
					var roster_row_ammount = ds_list_size(character_roster[i]);
					var x_offset = 960 - (72 * roster_row_ammount)
					var y_offset = 128;
					var bk_color_1 = make_color_rgb(37,20,0);
					var bk_color_2 = make_color_rgb(50,40,0)
					for(var c=0; c<roster_row_ammount; c++)
						{
						draw_set_alpha(0.7);
						draw_set_color(c_black);
						draw_rectangle(x_offset + (128*c),(128 * i) + y_offset,x_offset + (128*c) + 128,128 + (128 * i) + y_offset,true);
						draw_rectangle_color(x_offset + (128*c) + 1,(128 * i) + y_offset + 1,x_offset + (128*c) + 128 - 1,128 + (128 * i) + y_offset - 1,bk_color_1,bk_color_1,bk_color_2,bk_color_2,false);
						//Draw indicator if cursor over character
						for(g=0;g<=gp_amount-1;g++)
							{
							if point_in_rectangle(player_cursor_x[g],player_cursor_y[g],x_offset + (128*c),(128 * i) + y_offset,x_offset + (128*c) + 128,128 + (128 * i) + y_offset)
								{
								draw_rectangle(x_offset + (128*c),(128 * i) + y_offset,x_offset + (128*c) + 128,128 + (128 * i) + y_offset,false);
								}
							}
						draw_set_alpha(0.9);
						draw_sprite_ext(ds_map_find_value(ds_list_find_value(character_roster[i],c),"character_icon"),0,x_offset + (128*c)+64, (128 * i)+64+y_offset,0.99,1,0,c_white,1);
						draw_rectangle(x_offset + (128*c),(128 * i) + y_offset + 104,x_offset + (128*c) + 128,128 + (128 * i) + y_offset,false);
						draw_set_color(c_white);
						draw_set_alpha(1);
						draw_set_font(fnt_bebas_kai_10_pt);
						draw_set_halign(fa_center)
						draw_text(((x_offset + (128*c)) + (x_offset + (128*c) + 128))/2,110 + (128 * i) + y_offset,ds_map_find_value(ds_list_find_value(character_roster[i],c),"display_name"))
						}
					}
				}
				
		for(i=0;i<=gp_amount-1;i++)
		{
			if player_character_id[i] == noone
			{
			draw_sprite_ext(spr_player_cursor,0,player_cursor_x[i],player_cursor_y[i],1,1,0,get_player_color(i),1);
			}
			
			else
			{
			draw_set_alpha(1)
			draw_set_color(c_black);
			draw_circle(player_cursor_x[i]+30,player_cursor_y[i]+30,20,false);
			draw_circle(player_cursor_x[i]+26,player_cursor_y[i]+26,20,true);
			draw_set_color(get_player_color(i));
			draw_circle(player_cursor_x[i]+26,player_cursor_y[i]+26,20,false);	
			}
			draw_set_color(c_black);
			draw_set_font(fnt_8bitwonder_16pt);
			draw_text(player_cursor_x[i]+32,player_cursor_y[i]+16,"P" + string(i+1))
			draw_set_color(c_white);
			draw_text(player_cursor_x[i]+30,player_cursor_y[i]+14,"P" + string(i+1))
		}
		draw_set_font(fnt_bebas_kai_36_pt)
		draw_set_color(c_black)
		draw_text(960,48,"Select Your Character");
		draw_set_color(c_white)
		draw_text(960,44,"Select Your Character");
		
		//Draw Indicator if all players are ready
			if players_are_ready == true
				{
				draw_set_color(c_black);
				draw_set_alpha(0.6);
				draw_rectangle(0,0,1920,1080,false);
				draw_set_alpha(0.95);
				draw_rectangle(0,1080/3,1920,(1080/3) * 2,false);
				draw_text(1920/2,1080/2 - 128,"Are You Ready?");
				draw_set_alpha(1);
				draw_set_color(c_white);
				draw_set_font(fnt_bebas_kai_96_pt);
				draw_text(1920/2,1080/2 - 132,"Are You Ready?");
				draw_set_font(fnt_bebas_kai_36_pt);
				draw_text(1920/2,1080/2 + 64,"Any Player press          to continue!");
				draw_sprite_ext(ui_start,1,1920/2 + 38,1080/2 + 72 + 18 + 6,0.65,0.65,0,-1,1);
				draw_sprite_ext(ui_start,1,1920/2 + 38,1080/2 + 72 + 18 + sin(current_time/75)*4,0.65,0.65,0,-1,1);
				}
	#endregion
	break;
	
	case menu_state.warfare_settings:
	#region
	var gamemode_amount = ds_list_size(gamemode);
	draw_set_halign(fa_center);
	draw_set_valign(fa_top);
	draw_set_font(fnt_bebas_kai_36_pt)
	draw_set_color(c_black)
	draw_text(960,48,"Select a Gamemode");
	draw_set_color(c_white)
	draw_text(960,44,"Select a Gamemode");
	
	draw_set_color(c_black);
	draw_set_alpha(0.5)
	draw_rectangle(64,128,1920-64,1080-120,false);
	draw_rectangle(1920 * 0.6,128,1920-64,1080-120,false);
	for (i=0; i < gamemode_amount; i++)
		{
		draw_set_color(c_black);
		draw_set_alpha(0.75)
		if i == gamemode_select
			{
			draw_set_color(c_maroon)	
			}
			
		else
			{
			draw_set_color(c_black)
			}
		draw_rectangle(64,128 + (64*i),1920 * 0.6,128 + 64 + (64*i),false);
		var map_data = json_decode(gamemode[| i]);
		draw_set_halign(fa_left);
		draw_set_color(c_white);
		draw_set_alpha(1);
		draw_set_font(fnt_bebas_kai_24_pt)
		draw_text(96,128+16+(64*i),map_data[? "name"])
		ds_map_destroy(map_data);
		}
		
		var game_data = json_decode(gamemode[| gamemode_select]);
		var game_flavor_text = game_data[? "flavor_text"];
		var player_min = real(game_data[? "min_players"]);
		draw_set_halign(fa_left);
		draw_set_color(c_white);
		draw_set_alpha(1);
		draw_set_font(fnt_bebas_kai_24_pt)
		draw_text_ext(1920*0.6 + 64,540 + 64,game_flavor_text,48,600);
		draw_text_ext(1920*0.6 + 64,540 + 256,"Players Needed",48,600);
		draw_set_halign(fa_left);
		
		for(i=0; i < player_min; i++)
			{
			gpu_set_fog(true,c_red,0,0);
			draw_sprite_ext(spr_controller_icon,4,1920*0.6 + 128 + (i * 64),540 + 256 + 96,1,1,0,c_gray,0.75)	
			gpu_set_fog(false,c_white,0,0);
			}
			
		for(i=0; i < gp_amount; i++)
			{
			gpu_set_fog(true,c_green,0,0);
			draw_sprite_ext(spr_controller_icon,4,1920*0.6 + 128 + (i * 64),540 + 256 + 96,1,1,0,c_gray,0.75)	
			gpu_set_fog(false,c_white,0,0);	
			}
		ds_map_destroy(game_data);
		
		draw_set_color(c_black);
		draw_set_alpha(0.85);
		draw_rectangle(32,960,1920-32,1040,false);
		draw_set_color(c_white);
		draw_set_alpha(1);
		draw_sprite_ext(ui_a_button,0,128,1004,0.5,0.5,0,-1,1);
		draw_sprite_ext(ui_a_button,0,128,998 + sin(current_time/50),0.5,0.5,0,-1,1);
		draw_text(156,984,"Select Gamemode");
		draw_sprite_ext(ui_dpad,1,420,1000,0.5,0.5,0,-1,1);
		draw_sprite_ext(ui_dpad,3,420,1000,0.5,0.5,0,-1,(sin(current_time/250)));
		draw_text(460,984,"Navigate Selection");
		draw_sprite_ext(ui_start,1,1675,1004,0.5,0.5,0,-1,1);
		draw_sprite_ext(ui_start,1,1675,998 + sin(current_time/50),0.5,0.5,0,-1,1);
		draw_text(1440,984,"New Players Press          to Join");
		
		
	#endregion
	break;
	
	case menu_state.warfare_map_select:
	#region
	var map_amount = ds_list_size(maps);
	draw_set_halign(fa_center);
	draw_set_valign(fa_top);
	draw_set_font(fnt_bebas_kai_36_pt)
	draw_set_color(c_black)
	draw_text(960,48,"Select a Map");
	draw_set_color(c_white)
	draw_text(960,44,"Select a Map");
	
	draw_set_color(c_black);
	draw_set_alpha(0.5)
	draw_rectangle(64,128,1920-64,1080-120,false);
	draw_rectangle(1920 * 0.6,128,1920-64,1080-120,false);
	for (i=0; i < map_amount; i++)
		{
		draw_set_color(c_black);
		draw_set_alpha(0.75)
		if i == map_select
			{
			draw_set_color(c_maroon)	
			}
			
		else
			{
			draw_set_color(c_black)
			}
		draw_rectangle(64,128 + (64*i),1920 * 0.6,128 + 64 + (64*i),false);
		var map_data = json_decode(maps[| i]);
		draw_set_halign(fa_left);
		draw_set_color(c_white);
		draw_set_alpha(1);
		draw_set_font(fnt_bebas_kai_24_pt)
		draw_text(96,128+16+(64*i),map_data[? "name"])
		ds_map_destroy(map_data);
		}
		
		var map_data = json_decode(maps[| map_select]);
		draw_set_halign(fa_left);
		draw_set_color(c_white);
		draw_set_alpha(1);
		draw_set_font(fnt_bebas_kai_24_pt)
		draw_text(1920*0.6 + 64,540 + 64,map_data[? "flavor_text"]);
		ds_map_destroy(map_data);
		
		draw_set_color(c_black);
		draw_set_alpha(0.85);
		draw_rectangle(32,960,1920-32,1040,false);
		draw_set_color(c_white);
		draw_set_alpha(1);
		draw_sprite_ext(ui_a_button,0,128,1004,0.5,0.5,0,-1,1);
		draw_sprite_ext(ui_a_button,0,128,998 + sin(current_time/50),0.5,0.5,0,-1,1);
		draw_text(156,984,"Select Map");
		draw_sprite_ext(ui_dpad,1,420,1000,0.5,0.5,0,-1,1);
		draw_sprite_ext(ui_dpad,3,420,1000,0.5,0.5,0,-1,(sin(current_time/250)));
		draw_text(460,984,"Navigate Selection");
		//draw_sprite_ext(ui_start,1,1675,1004,0.5,0.5,0,-1,1);
		//draw_sprite_ext(ui_start,1,1675,998 + sin(current_time/50),0.5,0.5,0,-1,1);
		//draw_text(1440,984,"New Players Press          to Join");
	#endregion
	break;
	
	case menu_state.horde_mode_player_select:
	#region
	
	#endregion
	break;
	
	case menu_state.horde_mode_settings:
	#region

	#endregion
	break;
	
	case menu_state.horde_mode_map_select:
	#region
	
	#endregion
	break;
	
	case menu_state.settings_main:
	#region
	//Define Draw Settings
	var gwidth = 1920;
	var gheight = 1080;
	var center_x = gwidth/2;
	var center_y = gheight/2;
	var start_x = (gwidth/5);
	start_y = lerp(start_y,gheight * 0.40,0.1);
	logo_y = lerp(logo_y,190,0.35);
	//Draw Settings
	
	
	//Draw Flavor Text
	draw_set_alpha(0.85);
	draw_set_color(c_black);
	draw_rectangle(0,1080*0.825,1920,1080,false);
	draw_set_alpha(1);

	draw_set_color(c_white);
	draw_set_font(fnt_bebas_kai_24_pt);
	draw_set_halign(fa_center);
	draw_text(960,1080*0.855, settings_flavor_text[main_settings_select])
	draw_set_font(fnt_bebas_kai_18_pt);
	draw_text(960,1080*0.9, settings_flavor_sub_text[main_settings_select])
	
	//Draw Settings Menu
	var settings_item_amount = array_length_1d(settings_text);
	var button_spread = 84;
	var menu_y_pos = 300;
	for(var i=0; i < settings_item_amount; i += 1;)
		{
		draw_set_color(make_color_rgb(33,33,33));
		if i == main_settings_select
			{
			draw_set_color(make_color_rgb(114,0,8));
			}
		draw_rectangle(center_x - 320,(1080 - menu_y_pos) - (i * button_spread) - 24,center_x + 320,(1080 - menu_y_pos) - (i * button_spread) + 42,false)
		
		draw_set_color(c_white);
		draw_set_halign(fa_center);
		draw_text(center_x,(1080 - menu_y_pos) - (i * button_spread),settings_text[i]);	
		draw_text(32,32,string(settings_item_amount));
		}
	//Draw Controllers, indicating who is connected
	start_x = 320;
	for(var i = 0; i < 7; i++)
		{
		if gamepad[i] != noone
			{
			if gamepad_anybutton_pressed(gamepad[i])
				{
				draw_sprite_ext(spr_controller_icon,5,960 + (64 * i), 1080 - 32,1,1,0,get_player_color(i),1);
				}
	
			draw_sprite(spr_controller_icon,4,960 + (64 * i), 1080 - 32);
			draw_sprite(spr_controller_icon,gamepad[i],960 + (64 * i), 1080 - 32);
			}}
	
	//Draw Logo
	draw_sprite_ext(spr_game_wordmark,0,960,logo_y,2,2,0,c_white,1);
	#endregion
	break;
	
	case menu_state.settings_video:
	#region
	//Define Draw Settings
	var gwidth = 1920;
	var gheight = 1080;
	var center_x = gwidth/2;
	var center_y = gheight/2;
	var start_x = (gwidth/5);
	start_y = lerp(start_y,gheight * 0.40,0.1);
	logo_y = lerp(logo_y,190,0.35);
	
	//Draw Logo
	draw_sprite_ext(spr_game_wordmark,0,960,logo_y,2,2,0,c_white,1);
	#endregion
	break;
	
	case menu_state.settings_controller:
	#region
	//Define Draw Settings
	var gwidth = 1920;
	var gheight = 1080;
	var center_x = gwidth/2;
	var center_y = gheight/2;
	var start_x = (gwidth/5);
	start_y = lerp(start_y,gheight * 0.40,0.1);
	logo_y = lerp(logo_y,190,0.35);
	
	//Draw Logo
	draw_sprite_ext(spr_game_wordmark,0,960,logo_y,2,2,0,c_white,1);
	#endregion
	break;
	
	case menu_state.settings_audio:
	#region
	//Define Draw Settings
	var gwidth = 1920;
	var gheight = 1080;
	var center_x = gwidth/2;
	var center_y = gheight/2;
	var start_x = (gwidth/5);
	start_y = lerp(start_y,gheight * 0.40,0.1);
	logo_y = lerp(logo_y,190,0.35);
	
	//Draw Logo
	draw_sprite_ext(spr_game_wordmark,0,960,logo_y,2,2,0,c_white,1);
	
	var master_volume_y = 480;
	draw_set_alpha(0.75);
	draw_set_color(c_white);
	draw_rectangle(1920/2 - 420,master_volume_y-32,1920/2 + 420,master_volume_y+96,false);
	draw_set_color(c_white);
	draw_set_alpha(1);
	draw_rectangle(1920/2 - 320,master_volume_y,1920/2 + 320,master_volume_y+32,false);
	draw_healthbar(1920/2 - 320,master_volume_y,1920/2 + 320,master_volume_y+32,setting_master_channel * 100,c_dkgray,merge_color(c_black,c_red,0.7),merge_color(c_black,c_green,0.9),0,true,false);
	var slider_x = 640 + (640 * setting_master_channel);
	draw_set_color(merge_color(c_black,c_white,0.3));
	draw_set_alpha(0.5);
	draw_roundrect(slider_x-17,master_volume_y-12,slider_x+17,master_volume_y+46,false);
	draw_set_color(c_white);
	draw_set_alpha(1);
	draw_roundrect(slider_x-16,master_volume_y-12,slider_x+16,master_volume_y+44,false);
	draw_set_color(merge_color(c_black,c_white,0.275));
	draw_set_font(fnt_bebas_kai_16_pt);
	draw_text(1920/2,master_volume_y+64,"Master Volume");
	
	master_volume_y = 640;
	draw_set_alpha(0.75);
	draw_set_color(c_white);
	draw_rectangle(1920/2 - 420,master_volume_y-32,1920/2 + 420,master_volume_y+96,false);
	draw_set_color(c_white);
	draw_set_alpha(1);
	draw_rectangle(1920/2 - 320,master_volume_y,1920/2 + 320,master_volume_y+32,false);
	draw_healthbar(1920/2 - 320,master_volume_y,1920/2 + 320,master_volume_y+32,setting_music_channel * 100,c_dkgray,merge_color(c_black,c_red,0.7),merge_color(c_black,c_green,0.9),0,true,false);
	var slider_x = 640 + (640 * setting_music_channel);
	draw_set_color(merge_color(c_black,c_white,0.3));
	draw_set_alpha(0.5);
	draw_roundrect(slider_x-17,master_volume_y-12,slider_x+17,master_volume_y+46,false);
	draw_set_color(c_white);
	draw_set_alpha(1);
	draw_roundrect(slider_x-16,master_volume_y-12,slider_x+16,master_volume_y+44,false);
	draw_set_color(merge_color(c_black,c_white,0.275));
	draw_set_font(fnt_bebas_kai_16_pt);
	draw_text(1920/2,master_volume_y+64,"Music Volume");
	
	master_volume_y = 800;
	draw_set_alpha(0.75);
	draw_set_color(c_white);
	draw_rectangle(1920/2 - 420,master_volume_y-32,1920/2 + 420,master_volume_y+96,false);
	draw_set_color(c_white);
	draw_set_alpha(1);
	draw_rectangle(1920/2 - 320,master_volume_y,1920/2 + 320,master_volume_y+32,false);
	draw_healthbar(1920/2 - 320,master_volume_y,1920/2 + 320,master_volume_y+32,setting_sfx_channel * 100,c_dkgray,merge_color(c_black,c_red,0.7),merge_color(c_black,c_green,0.9),0,true,false);
	var slider_x = 640 + (640 * setting_sfx_channel);
	draw_set_color(merge_color(c_black,c_white,0.3));
	draw_set_alpha(0.5);
	draw_roundrect(slider_x-17,master_volume_y-12,slider_x+17,master_volume_y+46,false);
	draw_set_color(c_white);
	draw_set_alpha(1);
	draw_roundrect(slider_x-16,master_volume_y-12,slider_x+16,master_volume_y+44,false);
	draw_set_color(merge_color(c_black,c_white,0.275));
	draw_set_font(fnt_bebas_kai_16_pt);
	draw_text(1920/2,master_volume_y+64,"SFX Volume");
	#endregion
	break;
	
	case menu_state.settings_memory:
	#region
	
	#endregion
	break;
	
	case menu_state.forge_mode_select:
	#region
	
	#endregion
	break;
	
	
	default:
	#region
	
	#endregion
	break;
	}