{
    "id": "86572e6c-236d-40b8-8dd7-9a891d40a08c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_blood",
    "eventList": [
        {
            "id": "4dc4f3ef-6848-469a-ae10-b95ed6c4f18c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "86572e6c-236d-40b8-8dd7-9a891d40a08c"
        },
        {
            "id": "cb37d541-1d2c-4b90-8820-3f2d84e62297",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "86572e6c-236d-40b8-8dd7-9a891d40a08c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "77b298e4-c7a2-452c-850b-abf01cc060b0",
    "visible": true
}