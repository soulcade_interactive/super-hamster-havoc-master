{
    "id": "9717ef9c-f63e-479e-93c8-54dd1e90a0e3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_debug_console",
    "eventList": [
        {
            "id": "f3781013-c5c0-489b-a4c1-99c5dec6a826",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9717ef9c-f63e-479e-93c8-54dd1e90a0e3"
        },
        {
            "id": "0424cbad-d51e-435c-b31e-04b137230f56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9717ef9c-f63e-479e-93c8-54dd1e90a0e3"
        },
        {
            "id": "e1ca1e9f-af32-42d4-92c3-02de6fadc7d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 8,
            "m_owner": "9717ef9c-f63e-479e-93c8-54dd1e90a0e3"
        },
        {
            "id": "0b5e6230-7137-4beb-b1b6-278d5f260b23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "9717ef9c-f63e-479e-93c8-54dd1e90a0e3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}