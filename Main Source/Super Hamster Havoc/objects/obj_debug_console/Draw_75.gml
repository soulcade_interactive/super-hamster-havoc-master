/// @description Draw console overlay
if (live_call()) return live_result;
var console_width = 1920;
var console_height = 360;
draw_set_font(fnt_roboto_12pt);
if(self.has_focus){
	// Draw the console background
	draw_set_alpha(_log_background_alpha);
	draw_set_color(_log_background_color);		
	draw_rectangle(0, 0, console_width, console_height, false);	
	draw_set_halign(fa_left);
	// Draw console log, aka previous commands
	draw_set_alpha(_log_text_alpha);
	draw_set_color(_log_text_color);	
    draw_set_valign(fa_bottom);
    draw_text(10, console_height, log);
	// Draw current command background
	draw_set_alpha(_command_background_alpha);
	draw_set_color(_command_background_color);	
    draw_set_valign(fa_top);
	draw_rectangle(0, console_height, 
		console_width, (console_height) + 20, false);
	draw_set_alpha(_command_text_alpha);
	draw_set_color(_command_text_color);	
    draw_text(10, console_height, "$ " + keyboard_string);	 
	
	/*
	var gp_amount = 0;
	for(var i = 0; i < controller_limit; i++)
		{
		if gamepad[i] != noone
			{
			gp_amount += 1;
			}
		}
		
	var startx = 1920 * 0.5;
	var starty = 64;
	
	for (var i = 0; i < gp_amount; i++)
		{
		xx = startx + (96 * i);
		yy = starty;
		draw_rectangle(xx,yy,xx+64,yy+32,true);	
		
		}
	*/
	draw_set_color(c_white);
	draw_set_alpha(1);
	draw_text(1920-64,32,"FPS: " + string(fps));
}
