/// @description Insert description here
// You can write your code in this editor

if place_meeting(x,y,obj_solid_object_parent)
	{
	instance_destroy();
	}
depth = -y

var hit = instance_place(x,y,obj_hamster);

if hit != noone
&& hit != origin
	{
	hit.hp -= damage;
	hit.hitflash = damage;
	hit.knockback_dir = direction;
	hit.knockback_distance = damage/6;
	cameras[hit.player_id].screen_shake = damage * 5;
	hit.impact_direction = direction;
	
	//Create Blood Particle
	blood_particle = hit.blood_particle
	var dir = direction - 180;
	part_type_shape(blood_particle,pt_shape_square);
	part_type_size(blood_particle,0.03,0.05,0,0);
	part_type_scale(blood_particle,1,1);
	part_type_color2(blood_particle,c_red,c_maroon);
	part_type_speed(blood_particle,3,7,0,0);
	part_type_direction(blood_particle,dir-35,dir+35,0,0)
	part_type_life(blood_particle, room_speed * 0.25, room_speed * 0.5);
	part_particles_create(global.particle_system, x, y, blood_particle, 25);
	
	instance_destroy();
	}

var hit2 = instance_place(x,y,obj_limb_parent);

if hit2 != noone
	{
	hit2.hsp = hspeed * 0.2;
	hit2.vsp = vspeed * 0.2;
	
	blood_particle = hit2.blood_particle
	var dir = direction - 180;
	part_type_shape(blood_particle,pt_shape_square);
	part_type_size(blood_particle,0.03,0.05,0,0);
	part_type_scale(blood_particle,1,1);
	part_type_color2(blood_particle,c_red,c_maroon);
	part_type_speed(blood_particle,2,5,0,0);
	part_type_direction(blood_particle,dir-35,dir+35,0,0)
	part_type_life(blood_particle, room_speed * 0.25, room_speed * 0.5);
	part_particles_create(global.particle_system, x, y, blood_particle, 25);
	
	instance_destroy();
	}