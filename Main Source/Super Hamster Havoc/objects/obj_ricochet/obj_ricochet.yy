{
    "id": "e863b4b2-7ce9-413d-94cb-26c2e2a11fce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ricochet",
    "eventList": [
        {
            "id": "b392c0c7-17bd-49f7-87c1-83f8d24c71cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "e863b4b2-7ce9-413d-94cb-26c2e2a11fce"
        },
        {
            "id": "19f2f074-e233-474f-8cab-f70c702f2fd1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e863b4b2-7ce9-413d-94cb-26c2e2a11fce"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c0df1b8c-d321-46dc-82a5-b342f651fe2a",
    "visible": true
}