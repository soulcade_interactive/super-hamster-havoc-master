{
    "id": "acbf5fc7-f0d8-4490-8b93-047376468c68",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "rousrDissonance",
    "eventList": [
        {
            "id": "6befd869-c898-4951-9089-cc193346c0c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "acbf5fc7-f0d8-4490-8b93-047376468c68"
        },
        {
            "id": "d8d6acc0-abc8-495c-894c-bb02abea2a58",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "acbf5fc7-f0d8-4490-8b93-047376468c68"
        },
        {
            "id": "1ce28caa-611a-4ee5-98de-5956114bf4a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "acbf5fc7-f0d8-4490-8b93-047376468c68"
        },
        {
            "id": "d45f447d-9d7d-4b58-8526-0edc05f544b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 70,
            "eventtype": 7,
            "m_owner": "acbf5fc7-f0d8-4490-8b93-047376468c68"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}