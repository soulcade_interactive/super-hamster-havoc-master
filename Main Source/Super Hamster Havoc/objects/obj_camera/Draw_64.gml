/// @description Insert description here
// You can write your code in this editor
if (live_call()) return live_result;

draw_set_halign(fa_center);
draw_set_font(fnt_small);
if (follow != noone)
	{
		
		var xx = view_xport[player_id];
		var yy = view_yport[player_id];
		var cw = view_wport[player_id];
		var ch = view_hport[player_id];
		var cx_center = (xx + (cw/2));
		var cy_center = (yy + (ch/2))
		
		if hitflash > 0
			{
			hitflash--;
			draw_set_alpha(1 - (1/hitflash));
			draw_set_color(c_white);
			draw_rectangle(xx,yy,xx+cw,yy+ch,false);
			draw_set_alpha(1);
			draw_set_color(c_white);
			}
		var hp = follow.hp;
		if hp != -1
		&& hp > 0
			{
			if game_alive == true
				{
				var highlight1 = c_white;
				var highlight2 = c_gray;
				draw_hp = lerp(draw_hp,hp,0.25)
				draw_set_color(c_black);
				draw_set_alpha(0.85)
				draw_rectangle(cx_center - (cw * 0.4), yy + (ch * 0.063), cx_center + (cw * 0.403), yy + (ch * 0.092),false);
				draw_set_alpha(1);
				draw_healthbar(cx_center - (cw * 0.4), yy + (ch * 0.065), cx_center + (cw * 0.4), yy + (ch * 0.09), (draw_hp/100)*100,c_gray,c_red,c_green,0,true,false);
				draw_set_alpha(0.5)
				draw_rectangle(cx_center - (cw * 0.4), yy + (ch * 0.075), cx_center + (cw * 0.4), yy + (ch * 0.092),false);
				
				player_character = player_character_id[player_id]
				
				player_head = asset_get_index("spr_" + player_character + "_character_icon");
				draw_sprite_ext(player_head,0,cx_center - (cw * 0.4), yy + (ch * 0.07),4/5,4/5,0,c_white,1);
				if players[player_id].item_equipped != -1 
					{
					var gun_data = json_decode(projectile_weapon[players[player_id].item_equipped]);
					var _sprite_index = real(gun_data[? "sprite_index"]);
					var _image_index = real(gun_data[? "image_index"]);
					ds_map_destroy(gun_data);
					draw_sprite_ext(_sprite_index,_image_index,cx_center - (cw * 0.320), yy + (ch * 0.105),2,2,0,c_white,1);
					}
			
				draw_set_alpha(1)
				draw_set_color(c_black);
				draw_text(cx_center - (cw * 0.4), yy + (ch * 0.1050),"P" + string(player_id+1));
				draw_set_color(c_white);
				draw_text(cx_center - (cw * 0.4), yy + (ch * 0.1),"P" + string(player_id+1));
				if players[player_id].avaliable_gun != noone
					{
					if instance_exists(players[player_id].avaliable_gun)
						{
						var gun = players[player_id].avaliable_gun;
						var gun_index = gun.gun_id;
						draw_set_color(c_black);
						draw_text(cx_center,cy_center + (ch/4),"Press X to pick up")
						draw_set_color(c_white);
						draw_text(cx_center,cy_center + (ch/4)-4,"Press X to pick up")
						draw_sprite_ext(gun.sprite_index,gun.image_index,cx_center,cy_center + (ch/4)+72,2,2,0,c_white,1);
						}
					}
				//draw_text(cx_center - (cw * 0.4), yy + (ch * 0.1)+16,string(player_score[player_id]));
				}
			}
			
		if hp == -1
			{
			draw_sprite_ext(spr_controls,0,cx_center+16,yy,0.75,0.75,0,c_white,1);
			draw_sprite_ext(spr_player_button_a,image_index,cx_center,yy + (ch * 0.85),2,2,0,c_white,1);
			draw_set_color(c_black)
			draw_text(cx_center, yy + (ch * 0.91)+2, "Press A to drop at the location");
			draw_set_color(c_white)
			draw_text(cx_center, yy + (ch * 0.91), "Press A to drop at the location");
			}
			
		if hp <= 0
		&& hp != -1
			{
			if game_alive == true
				{
				draw_set_alpha(0.75);
				draw_set_color(c_black);
				draw_rectangle(xx,yy,xx+cw,yy+ch,false);
			
				draw_set_alpha(1);
				draw_set_color(c_black)
				draw_text(cx_center, cy_center, "P" + string(player_id+1) + " is now dead");
				draw_set_color(c_white)
				draw_text(cx_center, cy_center, "P" + string(player_id+1) + " is now dead");
				}
			}

	}	

	else
		{
		if game_alive == true
			{
			var xx = view_xport[player_id];
			var yy = view_yport[player_id];
			var cw = view_wport[player_id];
			var ch = view_hport[player_id];
			var cx_center = (xx + (cw/2));
			var cy_center = (yy + (ch/2))
		
			draw_set_alpha(0.75);
			draw_set_color(c_black);
			draw_rectangle(xx,yy,xx+cw,yy+ch,false);
			
			draw_set_alpha(1);
			draw_set_color(c_black)
			draw_text(cx_center, cy_center, "Player " + string(player_id+1) + " is now dead");
			draw_set_color(c_white)
			draw_text(cx_center, cy_center, "Player " + string(player_id+1) + " is now dead");
			}
		}