/// @description Insert description here
// You can write your code in this editor

var aim_x_offset = 0;
var aim_y_offset = 0;
var camera_height = camera_get_view_height(view_camera[player_id]);
var camera_width = camera_get_view_width(view_camera[player_id])
var offset_amount = camera_get_view_height(view_camera[player_id])/3;

if follow != noone
	{
		if gamepad_button_check(gamepad[player_id], gp_shoulderlb)
			{
			var joystick_dir = players[player_id].gun_direction;
	
			aim_x_offset = lerp(aim_x_offset,lengthdir_x(offset_amount,joystick_dir),0.5);
			aim_y_offset = lerp(aim_y_offset,lengthdir_y(offset_amount,joystick_dir),0.5);
			}

		if (follow != noone)
		{
			xTo = follow.x - (camera_width/2) + aim_x_offset;
			yTo = follow.y - (camera_height/2) + aim_y_offset;
		}
	}
	
x = round(lerp(x,xTo,0.05));
y = round(lerp(y,yTo,0.05));

var apply_shake = random_range(-screen_shake,screen_shake)
camera_set_view_pos(view_camera[player_id], round(x + apply_shake), round(y + apply_shake));
screen_shake = lerp(screen_shake,0,0.1)
