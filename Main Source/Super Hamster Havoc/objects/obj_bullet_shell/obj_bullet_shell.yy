{
    "id": "12919a4e-c081-4904-b465-af0ee3a86c22",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet_shell",
    "eventList": [
        {
            "id": "3b55149c-1f48-4ba6-9a1d-b883b8be9710",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "12919a4e-c081-4904-b465-af0ee3a86c22"
        },
        {
            "id": "40c52dab-5970-4e26-8451-18e36f6ee4a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "12919a4e-c081-4904-b465-af0ee3a86c22"
        },
        {
            "id": "5d86f4a8-9649-4d55-b97c-2a4b7210e8fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "12919a4e-c081-4904-b465-af0ee3a86c22"
        },
        {
            "id": "a567ce28-7274-43c0-8782-57cbd1f65d98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "12919a4e-c081-4904-b465-af0ee3a86c22"
        }
    ],
    "maskSpriteId": "0ff6eba4-a601-4ffd-a9a7-be3a3b2c7039",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0ff6eba4-a601-4ffd-a9a7-be3a3b2c7039",
    "visible": true
}