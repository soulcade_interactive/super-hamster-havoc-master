/// @description Insert description here
// You can write your code in this editor

if static == false
	{
	vsp += grav;

	//Horizontal Collisions and Movement
	if (place_meeting(x+hsp,y,obj_solid_object_parent))
		{
		//Increment till collisions are Pixel Perfect
		while (!place_meeting(x+sign(hsp),y,obj_solid_object_parent))
			{
			x += sign(hsp)	
			}
		//Set speed to zero upon flush collision
		hsp = 0;
		}
	x += hsp;

	//Vertical Collisions and Movement
	if (place_meeting(x,y+vsp,obj_solid_object_parent))
		{
		//Increment till collisions are Pixel Perfect
		while (!place_meeting(x,y+sign(vsp),obj_solid_object_parent))
			{
			y += sign(vsp)	
			}
		//Set speed to zero upon flush collision
		vsp = 0;
		}
	y += vsp;
	
	image_angle += 15;
	
	if y > shell_landing_y
	{
	static = true;	
	}
	
	/*
	var source = instance_nearest(x,y,obj_hamster)
	if source != noone
	{
	if point_distance(x,y,source.x,source.y) > dist	
		{
		static = true;	
		}
	}
	*/
	}

depth = -y+8;