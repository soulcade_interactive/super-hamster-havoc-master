/// @description Runs Loop to get input for all controllers
for(i=0; i<8; i++)
	{
	_left[i] = gamepad_button_check_pressed(gamepad[i],gp_padl);
	_right[i] = gamepad_button_check_pressed(gamepad[i],gp_padr);
	_up[i] = gamepad_button_check_pressed(gamepad[i],gp_padu);
	_down[i] = gamepad_button_check_pressed(gamepad[i],gp_padd);
	_a_button[i] = gamepad_button_check_pressed(gamepad[i],gp_face1);
	_b_button[i] = gamepad_button_check_pressed(gamepad[i],gp_face2);
	_start[i] = gamepad_button_check_pressed(gamepad[i],gp_start);
	
	//Dpad input system
	_left_pressed_alt[i]  = !_left_alt[i]  &&  (gamepad_axis_value(0, gp_axislh) < 0)
	_right_pressed_alt[i] = !_right_alt[i] &&  (gamepad_axis_value(0, gp_axislh) > 0)
	_up_pressed_alt[i]    = !_up_alt[i]    &&  (gamepad_axis_value(0, gp_axislv) < 0)
	_down_pressed_alt[i]  = !_down_alt[i]  &&  (gamepad_axis_value(0, gp_axislv) > 0)

	_left_alt[i]  = (gamepad_axis_value(0, gp_axislh) < 0);
	_right_alt[i] = (gamepad_axis_value(0, gp_axislh) > 0);
	_up_alt[i]    = (gamepad_axis_value(0, gp_axislv) < 0);
	_down_alt[i]  = (gamepad_axis_value(0, gp_axislv) > 0);
	}