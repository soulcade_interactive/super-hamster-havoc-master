/// @description Insert description here
// You can write your code in this editor
if (live_call()) return live_result;

switch(player_amount)
	{
	case 2:
	draw_line_width_color(960,0,960,1080,3,c_black,c_black);
	var width = room_width div cell_size;
	var height = room_height div cell_size;
	map_x = 1920/2 - (72);
	map_y = 1080 * 0.7;
	scale = 0.025
	draw_set_alpha(0.7)
	draw_set_color(c_black);
	draw_rectangle(map_x,map_y,map_x + room_width * scale,map_y + room_height * scale,false)
	
	
	with(obj_hamster)
		{
		player_character = player_character_id[player_id]
		
		player_data = json_decode(player_character_data[player_id]);
		var player_thumbnail = player_data[? "character_icon"];
		ds_map_destroy(player_data);
		player_head = asset_get_index("spr_" + player_character + "_0_head_r_running");
		draw_sprite_ext(player_head,0,other.map_x + (x * other.scale),other.map_y + (y * other.scale),0.5,0.5,0,c_white,1);
		}
	break;
	
	case 3:
	draw_line_width_color(960,0,960,540,3,c_black,c_black);
	draw_line_width_color(0,540,1920,540,3,c_black,c_black);
	
	var width = room_width div cell_size;
	var height = room_height div cell_size;
	map_x = 72;
	map_y = 1080 * 0.6;
	scale = 0.05
	draw_set_alpha(0.7)
	draw_set_color(c_black);
	draw_rectangle(map_x,map_y,map_x + room_width * scale,map_y + room_height * scale,false)
	
	
	with(obj_hamster)
		{
		player_character = player_character_id[player_id]
		player_head = asset_get_index("spr_" + player_character + "_0_head_r_running");
		draw_sprite_ext(player_head,0,other.map_x + (x * other.scale),other.map_y + (y * other.scale),0.5,0.5,0,c_white,1);
		}
	break;
	
	case 4:
	draw_line_width_color(0,540,1920,540,3,c_black,c_black);
	draw_line_width_color(960,0,960,1080,3,c_black,c_black);
	
		var width = room_width div cell_size;
	var height = room_height div cell_size;
	map_x = 1920/2 - 64;
	map_y = 1080/2 - 72;
	scale = 0.02
	draw_set_alpha(0.7)
	draw_set_color(c_black);
	draw_rectangle(map_x,map_y,map_x + room_width * scale,map_y + room_height * scale,false)
	
	
	with(obj_hamster)
		{
		player_character = player_character_id[player_id]
		player_head = asset_get_index("spr_" + player_character + "_0_head_r_running");
		draw_sprite_ext(player_head,0,other.map_x + (x * other.scale),other.map_y + (y * other.scale),0.5,0.5,0,c_white,1);
		}
	break;
	}
draw_set_alpha(1);
if game_alive == false
	{
	var width = 1920;
	var height = 1080;
	draw_set_color(c_white);
	draw_set_alpha(1);
	
	draw_set_color(c_black);
	draw_set_alpha(0.75);
	draw_rectangle(0,0,width,height,false);
	draw_sprite_ext(spr_podium,image_index,width/2,height * 0.4,3,4,0,c_white,1);
	if instance_exists(obj_hamster)
		{
		player_character = player_character_id[obj_hamster.player_id]
		player_head = asset_get_index("spr_" + player_character + "_0_head_r_running");
		player_torso = asset_get_index("spr_" + player_character + "_0_torso_r_running");
		player_legs = asset_get_index("spr_" + player_character + "_0_legs_r_running");
		draw_sprite_ext(player_legs,0,width/2,height * 0.365,4,4,0,c_white,1);
		draw_sprite_ext(player_torso,0,width/2,height * 0.365,4,4,0,c_white,1);
		draw_sprite_ext(player_head,0,width/2,height * 0.365,4,4,0,c_white,1);
		draw_sprite_ext(spr_weapons,player_alive.item_equipped,width/2,height * 0.425,3,4,0,c_white,1);
		draw_set_font(fnt_8bitwonder_18pt);
		draw_set_halign(fa_center);
		draw_text(width/2,height * 0.58 + 3,"Player " + string(player_alive.player_id + 1) + " is the victor");
		}
	draw_set_font(fnt_8bitwonder_12pt);
	draw_text(width/2,height * 0.62 + 3,"A New round will start in " + string(round(alarm[0]/room_speed)));
	draw_set_alpha(1);
	draw_set_color(c_white);
	draw_set_font(fnt_8bitwonder_18pt);
	draw_text(width/2,height * 0.58,"Player " + string(player_alive.player_id + 1) + " is the victor");
	draw_set_font(fnt_8bitwonder_12pt);
	draw_text(width/2,height * 0.62,"A New round will start in " + string(round(alarm[0]/room_speed)));
	
	draw_set_font(fnt_8bitwonder_10pt);
	draw_sprite_ext(spr_player_button_a,image_index,width/2,height * 0.67,2,2,0,c_white,1);
	draw_set_color(c_black);
	draw_set_alpha(0.75);
	draw_text(width/2,height * 0.7,"Or Press A to restart now");
	draw_set_color(c_white);
	draw_set_alpha(1);
	draw_text(width/2,height * 0.7 + 3,"Or Press A to restart now");
	
	image_speed = 0.5
	
	}

if (paused)
	{
	var indicator_color = c_white
				switch(pausing_player)
					{
					case 0:
					var indicator_color = c_red;
					break;
				
					case 1:
					var indicator_color = c_blue;
					break;
				
					case 2:
					var indicator_color = c_yellow;
					break;
				
					case 3:
					var indicator_color = c_green;
					break;
					
					case 4:
					var indicator_color = c_orange;
					break;
					
					case 5:
					var indicator_color = c_purple;
					break;
					
					case 6:
					var indicator_color = c_white;
					break;
					
					case 7:
					var indicator_color = c_dkgray;
					break;
					}
					
	draw_set_alpha(0.85);
	draw_set_color(c_black);
	draw_rectangle(0,0,1920,1080,false);
	
	draw_set_font(fnt_bebas_kai_36_pt);
	draw_set_color(c_white);
	draw_set_alpha(1);
	draw_text(1920/2,180,"Game Paused by Player")
	
	draw_set_alpha(0.5);
	draw_set_color(indicator_color);
	draw_rectangle(0,330 + (128 * pause_menu_select),1920,360 + 64 + (128 * pause_menu_select),false);
	draw_set_alpha(1);
	draw_set_color(c_white);
	
	var menu_size = array_length_1d(pause_menu);
	
	draw_set_font(fnt_bebas_kai_24_pt);
	for(i=0; i < menu_size; i++)
		{
		draw_text(1920/2,360 + (128 * i),pause_menu[i])	
		}
		
	
	}
