{
    "id": "97ff6c36-a13e-47c7-b8d8-c411e64a7f75",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_game_controller",
    "eventList": [
        {
            "id": "ebeac42a-bef5-4e03-bfab-3517db8c0cc7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "97ff6c36-a13e-47c7-b8d8-c411e64a7f75"
        },
        {
            "id": "ee93035b-ef4d-4c32-af8e-d1154093ddea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "97ff6c36-a13e-47c7-b8d8-c411e64a7f75"
        },
        {
            "id": "fbbf73db-e7b6-4c69-94f4-0d0957337871",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 8,
            "m_owner": "97ff6c36-a13e-47c7-b8d8-c411e64a7f75"
        },
        {
            "id": "3b11c1c3-70a4-4b4c-a4fa-9720afa8c8c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "97ff6c36-a13e-47c7-b8d8-c411e64a7f75"
        },
        {
            "id": "3bb2132a-2b3a-4bb0-9908-bc6f0a9d4121",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "97ff6c36-a13e-47c7-b8d8-c411e64a7f75"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}