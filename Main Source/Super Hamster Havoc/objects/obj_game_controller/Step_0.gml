if (live_call()) return live_result;
if keyboard_check_pressed(vk_escape)
    {
    room_goto(rm_menu);
    }

game_delta_time = update_delta_time();

switch(gamemode_select)
	{
	case 0:
	var players_alive = instance_number(obj_hamster);

	if players_alive == 1
	&& game_alive == true
		{
		player_alive = instance_nearest(0,0,obj_hamster);
		scr_set_hamster_skin(player_alive.player_skin);
		game_alive = false;	
		alarm[0] = room_speed * 10;
		player_score[player_alive.player_id] += 1;
		}
		
	if instance_exists(obj_gun)
	with (obj_gun)
		{
		if gun_id == 6
		gun_id = random_range(0,5);	
		}
	break;
	
	case 1:
	var players_alive = instance_number(obj_hamster);

	if players_alive == 1
	&& game_alive == true
		{
		player_alive = instance_nearest(0,0,obj_hamster);
		scr_set_hamster_skin(player_alive.player_skin);
		game_alive = false;	
		alarm[0] = room_speed * 10;
		player_score[player_alive.player_id] += 1;
		}
		
	if instance_exists(obj_gun)
	with (obj_gun)
		{
		if gun_id != 6
		gun_id = 6;	
		}
	break;
	
	case 2:
	break;
	}

	
if game_alive == false
	{
	if gamepad_button_check_pressed(gamepad[player_alive.player_id],gp_start)
	or gamepad_button_check_pressed(gamepad[player_alive.player_id],gp_face1)
		{
		room_restart();	
		}
	}
	
//Tracks Amount of Gamepads
var gp_amount = 0;
for(var i = 0; i < controller_limit; i++)
	{
	if gamepad[i] != noone
		{
		gp_amount += 1;
		}
	}

for (i=0; i<gp_amount; i++)
	{
	if gamepad_button_check_pressed(gamepad[i],gp_start)
		{
		switch(paused)
			{
			case true:
			paused = false;
			audio_play_sound(snd_back_sound,1,false);
			audio_resume_sound(snd_menu);
			audio_stop_sound(snd_pause_music);
			break;
			
			case false:
			audio_play_sound(snd_start_sound,1,false);
			paused = true;
			pausing_player = i
			audio_pause_sound(snd_menu)
			audio_play_sound(snd_pause_music,1,true);
			break;
			}
		}
	}
	
if paused	
	{
	var _accept = gamepad_button_check_pressed(gamepad[pausing_player],gp_face1);	
	var _reject = gamepad_button_check_pressed(gamepad[pausing_player],gp_face2);
	
	pause_menu_select += (-_up[pausing_player] + _down[pausing_player]);
	pause_menu_select += (-_up_pressed_alt[pausing_player] + _down_pressed_alt[pausing_player]);
	pause_menu_select = clamp(pause_menu_select,0,array_length_1d(pause_menu) - 1)
	
	if _reject
		{
		audio_play_sound(snd_back_sound,1,false);
		paused = false;
		}
		
	if _up[pausing_player]
	or _down[pausing_player]
	or _up_pressed_alt[pausing_player]
	or _down_pressed_alt[pausing_player]
		{
		audio_play_sound(snd_navigation_sound,1,false);
		}
		
	if _accept
		{
		audio_play_sound(snd_selection_sound,1,false);
		switch(pause_menu_select)	
			{
			case 0:
			paused = false;
			audio_resume_sound(snd_menu);
			audio_stop_sound(snd_pause_music);
			break;
			
			case 1:
			audio_resume_sound(snd_menu);
			audio_stop_sound(snd_pause_music);
			room_restart();
			break;
			
			case 2:
			audio_resume_sound(snd_menu);
			audio_stop_sound(snd_pause_music);
			menu_current_state = 0;
			room_goto(rm_menu);
			break;
			}
		}
	}