//Variables for procedural generation system



/*
cell_size = 64;


//Run the level generation script
generation_tileset = spr_tileset;
generation_floor = spr_floor;
generation_floor_alt = spr_floor_alt;
scr_generate_level(3);

//Spawn Players and cameras

globalvar players,cameras;
 
for (var i = 0; i < player_amount; i++)
	{
	var spawn_point = i*5; spawn_point += irandom_range(-3,3); spawn_point = clamp(spawn_point,0,20);
	//players[i] = instance_create_layer(spawn_x[spawn_point],spawn_y[spawn_point],"Instances",obj_spawner);
	players[i] = instance_create_layer(spawn_x[spawn_point]+16,spawn_y[spawn_point]+16,"Instances",obj_hamster);
	cameras[i] = instance_create_layer(players[i].x,players[i].y,"Instances",obj_camera);
	
	players[i].player_id = i;
	cameras[i].player_id = i;
	players[i].my_camera = cameras[i];
	cameras[i].follow = players[i].id;
	
	with(cameras[i])
		{
		init_camera(player_amount,i)	
		}
		
	gamepad_set_axis_deadzone(i,0.1);
	}
	
init_splitscreen(player_amount);
//Spawn Weapons
for (var i = 0; i < 20; i++)
	{
	var spawn_point = i
	instance_create_layer(spawn_x[spawn_point],spawn_y[spawn_point],"Instances",obj_gun);
	}
	
//Create Particle System
global.particle_system = part_system_create_layer("Particles", true);
display_set_gui_size(1920,1080);

globalvar game_delta_time;
game_delta_time = update_delta_time();

globalvar game_alive;
game_alive = true;

//Check for edge cases in generation
for (var i = 0; i < player_amount; i++)
	{
	
	if place_meeting(round_multiple_ceiling(players[i].x,64),round_multiple_ceiling(players[i].y,64),obj_wall)
		{
		room_restart();
		}
	if place_meeting(round_multiple_floor(players[i].x,64),round_multiple_floor(players[i].y,64),obj_wall)
		{
		room_restart();
		}
	if place_meeting(players[i].x,players[i].y,obj_wall)
		{
		room_restart();
		}
	}
*/
//Spawn Players and cameras
var player_spawn_points = ds_list_create();
with obj_player_spawner
	{
	var spawn_point = ds_map_create();
	ds_map_add(spawn_point,"x", x);
	ds_map_add(spawn_point,"y", y);
	ds_list_add(player_spawn_points,json_encode(spawn_point));
	ds_map_destroy(spawn_point);
	}
ds_list_shuffle(player_spawn_points);
globalvar players,cameras;
 
for (var i = 0; i < player_amount; i++)
	{
	var spawn_data = json_decode(player_spawn_points[| i ]);
	var xx = real(spawn_data[? "x"]);
	var yy = real(spawn_data[? "y"]);
	
	players[i] = instance_create_layer(xx,yy,"Instances",obj_hamster);
	cameras[i] = instance_create_layer(players[i].x,players[i].y,"Instances",obj_camera);
	
	players[i].player_id = i;
	cameras[i].player_id = i;
	players[i].my_camera = cameras[i];
	cameras[i].follow = players[i].id;
	
	with(cameras[i])
		{
		init_camera(player_amount,i)	
		}
		
	gamepad_set_axis_deadzone(i,0.1);
	}
	
init_splitscreen(player_amount);

//Create Particle System
global.particle_system = part_system_create_layer("Particles", true);
display_set_gui_size(1920,1080);

globalvar game_delta_time;
game_delta_time = update_delta_time();

globalvar game_alive;
game_alive = true;
globalvar game_ready;
game_ready = false;

usize = shader_get_uniform(shr_blur,"size");//uniform for width, height, radius
cell_size = 64;

//Reset Main Menu
menu_current_state = 0;

//Paused Varaibles
globalvar paused, pausing_player;
paused = false;
pausing_player = -1;
pause_menu_select = 0;

pause_menu[0] = "Continue";
pause_menu[1] = "Restart Match";
pause_menu[2] = "Return to Main Menu";

// Runs Loop to get input for all controllers
for(i=0; i<8; i++)
	{
	_up[i] = gamepad_button_check_pressed(i,gp_padu);
	_down[i] = gamepad_button_check_pressed(i,gp_padd);
	_left[i] = gamepad_button_check_pressed(i,gp_padl);
	_right[i] = gamepad_button_check_pressed(i,gp_padr);
	_left_alt[i] = false;
	_right_alt[i] = false;
	_up_alt[i] = false;
	_down_alt[i] = false;
	_left_pressed_alt[i] = false;
	_right_pressed_alt[i] = false;
	_up_pressed_alt[i] = false;
	_down_pressed_alt[i] = false;
	_a_button[i] = gamepad_button_check_pressed(i,gp_face1);
	_b_button[i] = gamepad_button_check_pressed(i,gp_face2);
	_start[i] = gamepad_button_check_pressed(i,gp_start);
	}