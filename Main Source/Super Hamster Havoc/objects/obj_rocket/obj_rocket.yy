{
    "id": "74f4e817-75c3-460b-930f-d18dfc9e8cab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rocket",
    "eventList": [
        {
            "id": "ed8e2639-46b2-4e52-be65-6ebcd2788a9e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "74f4e817-75c3-460b-930f-d18dfc9e8cab"
        },
        {
            "id": "dd1b1f57-6b7e-4006-a2d3-c25e473fed3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "74f4e817-75c3-460b-930f-d18dfc9e8cab"
        },
        {
            "id": "48b29a5b-0ad5-440e-8e56-f92478444854",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "74f4e817-75c3-460b-930f-d18dfc9e8cab"
        },
        {
            "id": "cf007591-e1d3-485a-8895-0d4faccb3690",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "74f4e817-75c3-460b-930f-d18dfc9e8cab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dea7d15a-5017-4ad5-b97d-44483cbe9ece",
    "visible": true
}