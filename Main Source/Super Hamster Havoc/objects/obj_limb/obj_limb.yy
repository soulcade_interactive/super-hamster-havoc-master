{
    "id": "6d9b6397-83fd-4062-afdf-147d19718bc5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_limb",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2b6a2388-0c05-4d25-adae-2d84df7efe35",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c9d19a41-8277-4152-afcd-dec9bca9ac57",
    "visible": true
}