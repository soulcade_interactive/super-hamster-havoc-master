/// @description Insert description here
// You can write your code in this editor
if (live_call()) return live_result;
var hit = instance_place(x,y,obj_hamster);

if hit != noone
	{
	hit.hp -= damage/room_speed;
	hit.hitflash = damage;
	hit.knockback_dir = point_direction(x,y,hit.x,hit.y);
	hit.knockback_distance = 96/room_speed;
	cameras[hit.player_id].screen_shake = 15;
	hit.impact_direction = direction;
	}