{
    "id": "12cd3d4d-19b6-428f-b05d-2ec6a0e9acfe",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_explosion",
    "eventList": [
        {
            "id": "a6e2cf57-98e0-4344-9a6c-1520ef9b317e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "12cd3d4d-19b6-428f-b05d-2ec6a0e9acfe"
        },
        {
            "id": "caba4c59-e82d-4493-ab00-c15b5a9a4ce5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "12cd3d4d-19b6-428f-b05d-2ec6a0e9acfe"
        },
        {
            "id": "0312894f-a23e-42cb-94a2-93197d8c98ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "12cd3d4d-19b6-428f-b05d-2ec6a0e9acfe"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ff688443-1d3c-48eb-8165-bcfccded85ba",
    "visible": true
}