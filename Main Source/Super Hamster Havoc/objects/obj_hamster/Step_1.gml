/// @description Input
// You can write your code in this editor
if (live_call()) return live_result;
//Get Gamepad Input

//Aim Down Sight (Left Trigger)
_ads = gamepad_button_check(gamepad[player_id],gp_shoulderlb);

//Attack (Right Trigger)
_shoot_auto = gamepad_button_check(gamepad[player_id],gp_shoulderrb); //Automatic
_shoot = gamepad_button_check_pressed(gamepad[player_id],gp_shoulderrb); //Semi-Automatic

//Special 1 (Left Bumper)
_bumper1 = gamepad_button_check(gamepad[player_id],gp_shoulderl);
_bumper1_pressed = gamepad_button_check_pressed(gamepad[player_id],gp_shoulderl);

//Grenade/Special 2 (Right Bumper)
_bumper2 = gamepad_button_check(gamepad[player_id],gp_shoulderr);
_bumper2_pressed = gamepad_button_check_pressed(gamepad[player_id],gp_shoulderr);

//Aiming (Right Thumbstick)
_aim_hor_axis = gamepad_axis_value(gamepad[player_id],gp_axisrh);
_aim_ver_axis = gamepad_axis_value(gamepad[player_id],gp_axisrv);
_aim_dir = point_direction(0,0,_aim_hor_axis,_aim_ver_axis);
_aim_pressed = gamepad_button_check(gamepad[player_id],gp_stickr);

//Movement (Left Thumbstick)
_move_hor_axis = gamepad_axis_value(gamepad[player_id],gp_axislh);
_move_ver_axis = gamepad_axis_value(gamepad[player_id],gp_axislv);
_move_dir = point_direction(0,0,_move_hor_axis,_move_ver_axis);
_move_pressed = gamepad_button_check(gamepad[player_id],gp_stickl);

//Actions (A B X Y)
_action1 = gamepad_button_check_pressed(gamepad[player_id],gp_face1);
_action2 = gamepad_button_check_pressed(gamepad[player_id],gp_face2);
_action3 = gamepad_button_check_pressed(gamepad[player_id],gp_face3);
_action4 = gamepad_button_check_pressed(gamepad[player_id],gp_face4);

//Meta Actions (Start & Select)
_start = gamepad_button_check_pressed(gamepad[player_id],gp_start);
_select = gamepad_button_check_pressed(gamepad[player_id],gp_select);