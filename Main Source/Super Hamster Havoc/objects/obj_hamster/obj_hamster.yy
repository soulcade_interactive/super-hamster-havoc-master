{
    "id": "0eba12f1-7de8-4539-86eb-b8382447a567",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_hamster",
    "eventList": [
        {
            "id": "0dc873c9-7c89-4cd7-a81e-ded14cadc00b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0eba12f1-7de8-4539-86eb-b8382447a567"
        },
        {
            "id": "ceec4820-8422-4f92-8f17-da0a02a5922b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0eba12f1-7de8-4539-86eb-b8382447a567"
        },
        {
            "id": "53b0230f-75ed-4f64-baf5-bba219ea3bed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0eba12f1-7de8-4539-86eb-b8382447a567"
        },
        {
            "id": "9340fb9f-f658-4e88-abd1-e69ec2cf3a37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0eba12f1-7de8-4539-86eb-b8382447a567"
        },
        {
            "id": "d1e94e45-f125-4ed3-ae8c-fc243982e155",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "0eba12f1-7de8-4539-86eb-b8382447a567"
        },
        {
            "id": "d3c0dbce-e62e-4d8e-bc44-575a8c0e5d17",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "0eba12f1-7de8-4539-86eb-b8382447a567"
        },
        {
            "id": "89767526-493f-4dc4-9334-0f81f66b4ba9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "0eba12f1-7de8-4539-86eb-b8382447a567"
        }
    ],
    "maskSpriteId": "5aea9789-92b2-426a-95b4-012fee3d5a22",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "19528ffc-4a5f-4ded-8a29-d55acbf241de",
    "visible": true
}