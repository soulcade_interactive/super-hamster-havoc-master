/// @description Insert description here
// You can write your code in this editor
if (live_call()) return live_result;
if hp > 0
{
//Get Player Information
var player_data = json_decode(player_character_data[player_id]);
var player_type = real(player_data[? "character_type"]);
ds_map_destroy(player_data);
//Get Gun Information
var gun_data = json_decode(projectile_weapon[item_equipped]);
var _sprite_id = gun_data[? "sprite_index"];
var _image_id = gun_data[? "image_index"];
var _gun_type = gun_data[? "gun_type"];
var _akimbo = gun_data[? "akimbo"];
ds_map_destroy(gun_data);


//Get Player Information
var player_character = player_character_id[player_id];
var player_head = asset_get_index("spr_" + player_character + "_0_head_" + angle_to_letter(gun_direction) + "_running");
var player_torso = asset_get_index("spr_" + player_character + "_0_torso_" + angle_to_letter(gun_direction) + "_running");
var player_legs = asset_get_index("spr_" + player_character + "_0_legs_" + angle_to_letter(_move_dir) + "_running");

//Add Hitflash shader if active
if (hitflash > 0)
	{
	hitflash--;
	shader_set(shr_hitflash);
	}
//Set Gun Y-Scale
if gun_direction < 90
or gun_direction > 270
	{
	gun_yscale = 1;
	}
else
	{
	gun_yscale = -1;
	}
	
//gun_handle_x = clamp(gun_handle_x,x-16,x+16);
//Draw Gun if Facing Up
if angle_to_letter(gun_direction) == "u"
	{
	#region
	switch(player_type)
		{
		case character_type.basic:
			switch(_gun_type)
				{
				case gun_type.pistol:
				draw_sprite_ext(_sprite_id,_image_id,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_hamster_hand,0,gun_handle_x,gun_handle_y,1,gun_yscale,gun_direction,c_white,1);
				
				break;
	
				case gun_type.rifle:
				draw_sprite_ext(_sprite_id,_image_id,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_hamster_hand,0,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				break;
	
				case gun_type.shotgun:
				draw_sprite_ext(_sprite_id,_image_id,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_hamster_hand,0,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_hamster_hand,0,gun_handle_x,gun_handle_y,1,gun_yscale,gun_direction,c_white,1);
				break;
	
				case gun_type.assault_rifle:
				draw_sprite_ext(_sprite_id,_image_id,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_hamster_hand,0,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_hamster_hand,0,gun_handle_x,gun_handle_y,1,gun_yscale,gun_direction,c_white,1);
				if (gun_delay > 0){draw_sprite_ext(spr_muzzle_flash,current_time,gun_muzzle_x,gun_muzzle_y,2,2,gun_direction,c_white,0.75);}
				break;
	
				case gun_type.sniper:
				draw_sprite_ext(_sprite_id,_image_id,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_hamster_hand,0,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_hamster_hand,0,gun_handle_x,gun_handle_y,1,gun_yscale,gun_direction,c_white,1);
				break;
	
				case gun_type.rocket_launcher:
				draw_sprite_ext(_sprite_id,_image_id,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_hamster_hand,0,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_hamster_hand,0,gun_handle_x,gun_handle_y,1,gun_yscale,gun_direction,c_white,1);
				break;
				}
		
		break;
		
		case character_type.buff:
		var hand_angle = point_direction(x+20,y+4,gun_muzzle_x,gun_muzzle_y);
			switch(_gun_type)
				{
				case gun_type.pistol:
				draw_sprite_ext(spr_buff_hamster_hand,0,x+20,y+4,1.25,gun_yscale*1.25,gun_direction,c_white,1);
				draw_sprite_ext(_sprite_id,_image_id,x-20 + lengthdir_x(24,gun_direction),y + 4 + lengthdir_y(24,gun_direction),1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_buff_hamster_hand,0,x-20,y+4,1.25,gun_yscale*1.25,gun_direction,c_white,1);
				if (gun_delay > 0){draw_sprite_ext(spr_muzzle_flash,current_time,gun_muzzle_x,gun_muzzle_y,2,2,gun_direction,c_white,1);}
				break;
	
				case gun_type.rifle:
				draw_sprite_ext(spr_buff_hamster_hand,0,x+20,y+4,1.25,gun_yscale*1.25,hand_angle,c_white,1);
				draw_sprite_ext(_sprite_id,_image_id,x-20 + lengthdir_x(24,gun_direction),y + 4 + lengthdir_y(24,gun_direction),1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_buff_hamster_hand,0,x-20,y+4,1.25,gun_yscale*1.25,gun_direction,c_white,1);
				break;
	
				case gun_type.shotgun:
				draw_sprite_ext(spr_buff_hamster_hand,0,x+20,y+4,1.25,gun_yscale*1.25,hand_angle,c_white,1);
				draw_sprite_ext(_sprite_id,_image_id,x-20 + lengthdir_x(24,gun_direction),y + 4 + lengthdir_y(24,gun_direction),1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_buff_hamster_hand,0,x-20,y+4,1.25,gun_yscale*1.25,gun_direction,c_white,1);
				break;
	
				case gun_type.assault_rifle:
				draw_sprite_ext(spr_buff_hamster_hand,0,x+20,y+4,1.25,gun_yscale*1.25,hand_angle,c_white,1);
				draw_sprite_ext(_sprite_id,_image_id,x-20 + lengthdir_x(24,gun_direction),y + 4 + lengthdir_y(24,gun_direction),1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_buff_hamster_hand,0,x-20,y+4,1.25,gun_yscale*1.25,gun_direction,c_white,1);
				if (gun_delay > 0){draw_sprite_ext(spr_muzzle_flash,current_time,gun_muzzle_x,gun_muzzle_y,2,2,gun_direction,c_white,0.75);}
				break;
	
				case gun_type.sniper:
				draw_sprite_ext(spr_buff_hamster_hand,0,x+20,y+4,1.25,gun_yscale*1.25,hand_angle,c_white,1);
				draw_sprite_ext(_sprite_id,_image_id,x-20 + lengthdir_x(24,gun_direction),y + 4 + lengthdir_y(24,gun_direction),1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_buff_hamster_hand,0,x-20,y+4,1.25,gun_yscale*1.25,gun_direction,c_white,1);
				break;
	
				case gun_type.rocket_launcher:
				draw_sprite_ext(spr_buff_hamster_hand,0,x+20,y+4,1.25,gun_yscale*1.25,hand_angle,c_white,1);
				draw_sprite_ext(_sprite_id,_image_id,x-20 + lengthdir_x(24,gun_direction),y + 4 + lengthdir_y(24,gun_direction),1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_buff_hamster_hand,0,x-20,y+4,1.25,gun_yscale*1.25,gun_direction,c_white,1);
				break;
				}
		break;
		}
	#endregion
	}
//Set Player Animation Speed
if (_move_hor_axis + _move_ver_axis) != 0 { image_speed = sp/4;	}
else { image_speed = 0; image_index = 0;}

draw_sprite_ext(player_head,image_index,x,y,1,1,0,c_white,1);
draw_sprite_ext(player_torso,image_index,x,y,1,1,0,c_white,1);
draw_sprite_ext(player_legs,image_index,x,y,1,1,0,c_white,1);
shader_reset();
	
//Draw Gun if Facing Anywhere Else
if angle_to_letter(gun_direction) != "u"
	{
	#region
	switch(player_type)
		{
		case character_type.basic:
			switch(_gun_type)
				{
				case gun_type.pistol:
				draw_sprite_ext(_sprite_id,_image_id,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_hamster_hand,0,gun_handle_x,gun_handle_y,1,gun_yscale,gun_direction,c_white,1);
				
				break;
	
				case gun_type.rifle:
				draw_sprite_ext(_sprite_id,_image_id,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_hamster_hand,0,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				break;
	
				case gun_type.shotgun:
				draw_sprite_ext(_sprite_id,_image_id,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_hamster_hand,0,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_hamster_hand,0,gun_handle_x,gun_handle_y,1,gun_yscale,gun_direction,c_white,1);
				break;
	
				case gun_type.assault_rifle:
				draw_sprite_ext(_sprite_id,_image_id,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_hamster_hand,0,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_hamster_hand,0,gun_handle_x,gun_handle_y,1,gun_yscale,gun_direction,c_white,1);
				if (gun_delay > 0){draw_sprite_ext(spr_muzzle_flash,current_time,gun_muzzle_x,gun_muzzle_y,2,2,gun_direction,c_white,0.75);}
				break;
	
				case gun_type.sniper:
				draw_sprite_ext(_sprite_id,_image_id,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_hamster_hand,0,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_hamster_hand,0,gun_handle_x,gun_handle_y,1,gun_yscale,gun_direction,c_white,1);
				break;
	
				case gun_type.rocket_launcher:
				draw_sprite_ext(_sprite_id,_image_id,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_hamster_hand,0,gun_x,gun_y,1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_hamster_hand,0,gun_handle_x,gun_handle_y,1,gun_yscale,gun_direction,c_white,1);
				break;
				}
		
		break;
		
		case character_type.buff:
		var hand_angle = point_direction(x+20,y+4,gun_muzzle_x,gun_muzzle_y);
			switch(_gun_type)
				{
				case gun_type.pistol:
				draw_sprite_ext(spr_buff_hamster_hand,0,x+20,y+4,1.25,gun_yscale*1.25,gun_direction,c_white,1);
				draw_sprite_ext(_sprite_id,_image_id,x-20 + lengthdir_x(24,gun_direction),y + 4 + lengthdir_y(24,gun_direction),1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_buff_hamster_hand,0,x-20,y+4,1.25,gun_yscale*1.25,gun_direction,c_white,1);
				if (gun_delay > 0){draw_sprite_ext(spr_muzzle_flash,current_time,gun_muzzle_x,gun_muzzle_y,2,2,gun_direction,c_white,1);}
				break;
	
				case gun_type.rifle:
				draw_sprite_ext(spr_buff_hamster_hand,0,x+20,y+4,1.25,gun_yscale*1.25,hand_angle,c_white,1);
				draw_sprite_ext(_sprite_id,_image_id,x-20 + lengthdir_x(24,gun_direction),y + 4 + lengthdir_y(24,gun_direction),1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_buff_hamster_hand,0,x-20,y+4,1.25,gun_yscale*1.25,gun_direction,c_white,1);
				break;
	
				case gun_type.shotgun:
				draw_sprite_ext(spr_buff_hamster_hand,0,x+20,y+4,1.25,gun_yscale*1.25,hand_angle,c_white,1);
				draw_sprite_ext(_sprite_id,_image_id,x-20 + lengthdir_x(24,gun_direction),y + 4 + lengthdir_y(24,gun_direction),1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_buff_hamster_hand,0,x-20,y+4,1.25,gun_yscale*1.25,gun_direction,c_white,1);
				break;
	
				case gun_type.assault_rifle:
				draw_sprite_ext(spr_buff_hamster_hand,0,x+20,y+4,1.25,gun_yscale*1.25,hand_angle,c_white,1);
				draw_sprite_ext(_sprite_id,_image_id,x-20 + lengthdir_x(24,gun_direction),y + 4 + lengthdir_y(24,gun_direction),1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_buff_hamster_hand,0,x-20,y+4,1.25,gun_yscale*1.25,gun_direction,c_white,1);
				if (gun_delay > 0){draw_sprite_ext(spr_muzzle_flash,current_time,gun_muzzle_x,gun_muzzle_y,2,2,gun_direction,c_white,0.75);}
				break;
	
				case gun_type.sniper:
				draw_sprite_ext(spr_buff_hamster_hand,0,x+20,y+4,1.25,gun_yscale*1.25,hand_angle,c_white,1);
				draw_sprite_ext(_sprite_id,_image_id,x-20 + lengthdir_x(24,gun_direction),y + 4 + lengthdir_y(24,gun_direction),1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_buff_hamster_hand,0,x-20,y+4,1.25,gun_yscale*1.25,gun_direction,c_white,1);
				break;
	
				case gun_type.rocket_launcher:
				draw_sprite_ext(spr_buff_hamster_hand,0,x+20,y+4,1.25,gun_yscale*1.25,hand_angle,c_white,1);
				draw_sprite_ext(_sprite_id,_image_id,x-20 + lengthdir_x(24,gun_direction),y + 4 + lengthdir_y(24,gun_direction),1,gun_yscale,gun_direction,c_white,1);
				draw_sprite_ext(spr_buff_hamster_hand,0,x-20,y+4,1.25,gun_yscale*1.25,gun_direction,c_white,1);
				break;
				}
		break;
		}
	#endregion
	}
}

gpu_set_blendenable(false);
gpu_set_colorwriteenable(false,false,false,true);

draw_set_alpha(0);
var x1 = x - 72;
var y1 = y - 72;
var x2 = x + 72;
var y2 = y + 72;
draw_rectangle(x1,y1,x2,y2,false);
draw_set_alpha(1);

gpu_set_colorwriteenable(true,true,true,true);
gpu_set_blendenable(true);