/// @description Setup Player Variables

	//Assign Player Skin selection to player 
	player_skin = hamster_skin.brownwhite;
	scr_set_hamster_skin(player_skin);
	
	//Create all gameplay variables
	#region
	hsp = 0;
	vsp = 0;
	sp = 0;
	movement_speed = 400;//orginal 350
	gun_direction = 0;
	impact_direction = 0;
	blood_particle = part_type_create();
	gun_x = x;
	gun_y = y;
	gun_handle_x = x;
	gun_handle_y = y;
	
	hitflash = 0;
	knockback_dir = 0;
	knockback_distance = 0;
	enum weapon
		{
		hands,
		pistol,
		shotgun,
		sniper,
		assault_rifle,
		rocket_launcher
		}
	
	item_equipped = weapon.hands;
	avaliable_gun = noone;
	gun_delay = 0;

	hp = 100;
	alive = true;
	
	//Variables for Controller/Action Input
	_ads = 0;
	_shoot_auto = 0;
	_shoot = 0;
	_special1 = 0;
	_special2 = 0;
	_aim_hor_axis = 0;
	_aim_ver_axis = 0;
	_aim_dir = 0;
	_aim_pressed = 0;
	_move_hor_axis = 0;
	_move_ver_axis = 0;
	_move_dir = 0;
	_move_pressed = 0;
	_action1 = 0;
	_action2 = 0;
	_action3 = 0;
	_action4 = 0;
	_start = 0;
	_select = 0;
	#endregion
	
	gun_xscale = 0;
	gun_yscale = 0;
	
	usize = shader_get_uniform(shr_blur,"size");//uniform for width, height, radius
	