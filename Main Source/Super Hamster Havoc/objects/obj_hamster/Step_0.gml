/// @description Insert description here
// You can write your code in this editor
if (live_call()) return live_result;

depth = -y - 16;
if hp > 0
&& paused == false
{
if gamepad_is_connected(gamepad[player_id])
		{
		gamepad_set_axis_deadzone(gamepad[player_id],0.5)
		//Player Movement
		#region
		//Convert Input to Movement Variables
		var horizontal_movement = (_move_hor_axis);
		var vertical_movement = (_move_ver_axis);
		var sprint = (2 * _bumper1);

		//Delta Time the movement (So it's the intended speed on all devices)
		if knockback_distance > 0
			{
			knockback_distance = lerp(knockback_distance,0,0.25)
			var knockback_x = lengthdir_x(knockback_distance,knockback_dir);
			var knockback_y = lengthdir_y(knockback_distance,knockback_dir);
			}
	
		else
			{
			var knockback_x = 0;
			var knockback_y = 0;	
			}

		// Horizontal and Vertical Movement Implementation
		hsp = ((horizontal_movement + knockback_x) * (movement_speed + sprint)) * game_delta_time;
		vsp = ((vertical_movement + knockback_y) * (movement_speed + sprint)) * game_delta_time;
		
		//Horizontal Collisions and Movement
		if (place_meeting(x+hsp,y,obj_solid_object_parent))
			{
			//Increment till collisions are Pixel Perfect
			while (!place_meeting(x+sign(hsp),y,obj_solid_object_parent))
				{
				x += sign(hsp)	
				}
			//Set speed to zero upon flush collision
			hsp = 0;
			}
		x += hsp;

		//Vertical Collisions and Movement
		if (place_meeting(x,y+vsp,obj_solid_object_parent))
			{
			//Increment till collisions are Pixel Perfect
			while (!place_meeting(x,y+sign(vsp),obj_solid_object_parent))
				{
				y += sign(vsp)	
				}
			//Set speed to zero upon flush collision
			vsp = 0;
			}
		y += vsp;
		#endregion
		//Player Combat
		#region
		//Get Gun Directions
		if (_aim_hor_axis + _aim_ver_axis) != 0
			{
			gun_direction = _aim_dir;	
			}
		else 
			{
			if (_move_hor_axis + _move_ver_axis) != 0
				{
				gun_direction = _move_dir;	
				}
			}
		//Get Player Information
		var player_data = json_decode(player_character_data[player_id]);
		var player_type = real(player_data[? "character_type"]);
		ds_map_destroy(player_data);
		//Increment Gun Firerate Variables 
		gun_delay -= 1;
		gun_delay = clamp(gun_delay,-1,gun_delay+1);
		//Get Gun Information
		//item_equipped = 2;
		var gun_data = json_decode(projectile_weapon[item_equipped]);
		var _gun_type = gun_data[? "gun_type"];
		var _gun_firerate = gun_data[? "gun_firerate_type"];
		var _firerate = gun_data[? "gun_firerate_amount"];
		var _gun_projectile_type = real(gun_data[? "gun_projectile"]);
		switch(_gun_projectile_type)
			{
			case gun_projectile.regular_bullet:
			_gun_projectile = obj_bullet;
			_shell_sprite = spr_bullet_shell;
			_shell_image = 0;
			break;
			case gun_projectile.rifle_bullet:
			_gun_projectile = obj_bullet;
			_shell_sprite = spr_bullet_shell;
			_shell_image = 0;
			break;
			case gun_projectile.slug:
			_gun_projectile = obj_bullet;
			_shell_sprite = spr_bullet_shell;
			_shell_image = 2;
			break;
			case gun_projectile.arrow:
			_gun_projectile = obj_bullet;
			_shell_sprite = spr_bullet_shell;
			_shell_image = 0;
			break;
			case gun_projectile.explosive_arrows:
			_gun_projectile = obj_bullet;
			_shell_sprite = spr_bullet_shell;
			_shell_image = 0;
			break;
			case gun_projectile.rpg:
			_gun_projectile = obj_rocket;
			_shell_sprite = -1;
			_shell_image = -1;
			break;
			
			default:
			_gun_projectile = obj_bullet;
			_shell_sprite = spr_bullet_shell;
			_shell_image = 0;
			}
		var _damage = gun_data[? "damage"];
		var _akimbo = gun_data[? "akimbo"];
		var _sound = gun_data[? "sound"];
		ds_map_destroy(gun_data);
		
		var _handgun_offset = 20;
		var _rifle_offset = 24;
		var _shotgun_offset = 14;
		var _assault_rifle_offset = 14;
		var _sniper_offset = 26;
		var _rocket_launcher_offset = 16;
		
		switch(player_type)
		{
		case character_type.basic:
			{
			switch(_gun_type)
				{
				case gun_type.pistol:
				gun_x = x + lengthdir_x((_handgun_offset - gun_delay),gun_direction);
				gun_y = y + 16 + lengthdir_y((_handgun_offset - gun_delay),gun_direction);
				gun_handle_x = x + lengthdir_x((_handgun_offset - 12 - gun_delay),gun_direction);
				gun_handle_y = y + 16 + lengthdir_y((_handgun_offset - 12 - gun_delay),gun_direction);
				gun_muzzle_x = x + lengthdir_x((_handgun_offset - gun_delay) * 1.25,gun_direction);
				gun_muzzle_y = y + 16 + lengthdir_y((_handgun_offset - gun_delay) * 1.25,gun_direction);
				break;
	
				case gun_type.rifle:
				gun_x = x + lengthdir_x(_rifle_offset - gun_delay,gun_direction);
				gun_y = y + 16 + lengthdir_y(_rifle_offset - gun_delay,gun_direction);
				gun_handle_x = x + lengthdir_x(-(_assault_rifle_offset/10) - (gun_delay*2),gun_direction);
				gun_handle_y = y + 16 + lengthdir_y(-(_assault_rifle_offset/10) - (gun_delay*2),gun_direction);
				gun_muzzle_x = x + lengthdir_x(_rifle_offset - gun_delay * 1.25,gun_direction);
				gun_muzzle_y = y + 16 + lengthdir_y(_rifle_offset - gun_delay * 1.25,gun_direction);
				break;
	
				case gun_type.shotgun:
				gun_x = x + lengthdir_x(_shotgun_offset - gun_delay,gun_direction);
				gun_y = y + 16 + lengthdir_y(_shotgun_offset - gun_delay,gun_direction);
				gun_handle_x = x + lengthdir_x(-(_assault_rifle_offset/10) - (gun_delay),gun_direction);
				gun_handle_y = y + 16 + lengthdir_y(-(_assault_rifle_offset/10) - (gun_delay),gun_direction);
				gun_muzzle_x = x + lengthdir_x(_shotgun_offset * 2.5 - gun_delay,gun_direction);
				gun_muzzle_y = y + 16 + lengthdir_y(_shotgun_offset * 2.5 - gun_delay,gun_direction);
				break;
	
				case gun_type.assault_rifle:
				gun_x = x + lengthdir_x(_assault_rifle_offset - (gun_delay*2),gun_direction);
				gun_y = y + 16 + lengthdir_y(_assault_rifle_offset - (gun_delay*2),gun_direction);
				gun_handle_x = x + lengthdir_x(-(_assault_rifle_offset/10) - (gun_delay*2),gun_direction);
				gun_handle_y = y + 16 + lengthdir_y(-(_assault_rifle_offset/10) - (gun_delay*2),gun_direction);
				gun_muzzle_x = x + lengthdir_x(_assault_rifle_offset*2.5 - (gun_delay*2),gun_direction);
				gun_muzzle_y = y + 16 + lengthdir_y(_assault_rifle_offset*2.5 - (gun_delay*2),gun_direction);
				break;
	
				case gun_type.sniper:
				gun_x = x + lengthdir_x(_sniper_offset - gun_delay,gun_direction);
				gun_y = y + 16 + lengthdir_y(_sniper_offset - gun_delay,gun_direction);
				gun_handle_x = x + lengthdir_x(-(_assault_rifle_offset/10) - (gun_delay*2),gun_direction);
				gun_handle_y = y + 16 + lengthdir_y(-(_assault_rifle_offset/10) - (gun_delay*2),gun_direction);
				gun_muzzle_x =  x + lengthdir_x(_sniper_offset - gun_delay * 1.25,gun_direction);
				gun_muzzle_y = y + 16 + lengthdir_y(_sniper_offset - gun_delay * 1.25,gun_direction);
				break;
	
				case gun_type.rocket_launcher:
				gun_x = x + lengthdir_x(_rocket_launcher_offset - gun_delay,gun_direction);
				gun_y = y + 16 + lengthdir_y(_rocket_launcher_offset - gun_delay,gun_direction);
				gun_handle_x = x + lengthdir_x(-(_assault_rifle_offset/10) - (gun_delay),gun_direction);
				gun_handle_y = y + 16 + lengthdir_y(-(_assault_rifle_offset/10) - (gun_delay),gun_direction);
				gun_muzzle_x = x + lengthdir_x(_rocket_launcher_offset*2.5 - gun_delay,gun_direction);
				gun_muzzle_y = y + 16 + lengthdir_y(_rocket_launcher_offset*2.5 - gun_delay,gun_direction);
				break;
				}
			}
			break;
			
			case character_type.buff:
			{
			switch(_gun_type)
			
				{
				case gun_type.pistol:
				gun_x = x - 20 + lengthdir_x(gun_delay,gun_direction);
				gun_y = y + 4 + lengthdir_y(gun_delay,gun_direction);
				gun_handle_x = x - 20 + lengthdir_x(64,gun_direction);
				gun_handle_y = y + 4 + lengthdir_y(64,gun_direction);
				gun_muzzle_x = x - 20 + lengthdir_x(gun_delay + _handgun_offset*2,gun_direction);
				gun_muzzle_y = y + 4 + lengthdir_y(gun_delay + _handgun_offset*2,gun_direction);
				break;
	
				case gun_type.rifle:
				gun_x = x + lengthdir_x(_rifle_offset - gun_delay,gun_direction);
				gun_y = y + 16 + lengthdir_y(_rifle_offset - gun_delay,gun_direction);
				gun_muzzle_x = x - 20 + lengthdir_x(gun_delay + _rifle_offset*2,gun_direction);
				gun_muzzle_y = y + 4 + lengthdir_y(gun_delay + _rifle_offset*2,gun_direction);
				break;
	
				case gun_type.shotgun:
				gun_x = x + lengthdir_x(_shotgun_offset - gun_delay,gun_direction);
				gun_y = y + 16 + lengthdir_y(_shotgun_offset - gun_delay,gun_direction);
				gun_muzzle_x = x - 20 + lengthdir_x(-gun_delay + _shotgun_offset*3,gun_direction);
				gun_muzzle_y = y + 4 + lengthdir_y(-gun_delay + _shotgun_offset*3,gun_direction);
				break;
	
				case gun_type.assault_rifle:
				gun_x = x + lengthdir_x(_assault_rifle_offset - (gun_delay*2),gun_direction);
				gun_y = y + 16 + lengthdir_y(_assault_rifle_offset - (gun_delay*2),gun_direction);
				gun_handle_x = x + lengthdir_x(-(_assault_rifle_offset/10) - (gun_delay*2),gun_direction);
				gun_handle_y = y + lengthdir_y(-(_assault_rifle_offset/10) - (gun_delay*2),gun_direction);
				gun_muzzle_x = x - 20 + lengthdir_x(gun_delay + _assault_rifle_offset*3.5,gun_direction);
				gun_muzzle_y = y + 2 + lengthdir_y(gun_delay + _assault_rifle_offset*3.5,gun_direction);
				break;
	
				case gun_type.sniper:
				gun_x = x + lengthdir_x(_sniper_offset - gun_delay,gun_direction);
				gun_y = y + 16 + lengthdir_y(_sniper_offset - gun_delay,gun_direction);
				gun_muzzle_x = x - 20 + lengthdir_x(gun_delay + _sniper_offset*2,gun_direction);
				gun_muzzle_y = y + 4 + lengthdir_y(gun_delay + _sniper_offset*2,gun_direction);
				break;
	
				case gun_type.rocket_launcher:
				gun_x = x + lengthdir_x(_rocket_launcher_offset - gun_delay,gun_direction);
				gun_y = y + 16 + lengthdir_y(_rocket_launcher_offset - gun_delay,gun_direction);
				gun_muzzle_x = x - 20 + lengthdir_x(gun_delay + _rocket_launcher_offset*3,gun_direction);
				gun_muzzle_y = y + 4 + lengthdir_y(gun_delay + _rocket_launcher_offset*3,gun_direction);
				break;
				}
			}
			break;
			}
		var _player_object_id = id;
		//Calculate Gun Barrel postioning
		var gun_barrel_x = x + gun_x + lengthdir_x(4,gun_direction);
		var gun_barrel_y = y + gun_y + lengthdir_y(4,gun_direction);

		var gun_eject_x = x + lengthdir_x(-14,gun_direction);
		var gun_eject_y = y + lengthdir_y(-14,gun_direction);
		
		//Fire Automatic Weapons
		switch(_gun_firerate)
			{
			case gun_fire_rate_type.semi_auto:
			case gun_fire_rate_type.single_shot:
			case gun_fire_rate_type.pump_action:
			if (_shoot)
				{
				if gun_delay < 0
					{
					gun_delay = _firerate;
					gamepad_set_vibration(gamepad[player_id],0.5,0.5);
					alarm[0] = room_speed/6;
					cameras[player_id].screen_shake = 5;
					var knockback = 32;
					gun_x += lengthdir_x(knockback,gun_direction);
					gun_y += lengthdir_y(knockback,gun_direction);
					audio_play_sound(_sound,0.5,false);
					
					if (_shell_image != -1) && (_shell_sprite != -1)
					{
					var shell = instance_create_layer(gun_eject_x,gun_eject_y,"Instances",obj_bullet_shell)
						shell.image_speed = 0;
						shell.sprite_index = _shell_sprite;
						shell.image_index = _shell_image;
					
						var variaty = irandom_range(-35,35);
						shell.hsp = lengthdir_x(6,90 + variaty)
						shell.vsp = lengthdir_y(6,90+ variaty)
					}
					switch(_gun_type)
						{
						case gun_type.pistol:
						case gun_type.rifle:
						case gun_type.assault_rifle:
						case gun_type.sniper:
						case gun_type.rocket_launcher:
						case gun_type.bow:
						
						with(instance_create_layer(gun_muzzle_x,gun_muzzle_y,"Instances",real(_gun_projectile)))
							{
							direction = other.gun_direction + random_range(-5,5);
							image_angle = direction;
							speed = 25 + other.sp;
							gamepad_id = other.player_id;
							origin = _player_object_id;
							damage = _damage;
							}
						
						break;
						
						case gun_type.shotgun:
						with(instance_create_layer(gun_muzzle_x,gun_muzzle_y,"Instances",real(_gun_projectile)))
							{
							direction = other.gun_direction;
							image_angle = direction;
							speed = 15 + other.sp;
							gamepad_id = other.player_id;
							origin = _player_object_id;
							damage = _damage;
							}
				
						with(instance_create_layer(gun_muzzle_x,gun_muzzle_y,"Instances",real(_gun_projectile)))
							{
							direction = other.gun_direction+15;
							image_angle = direction;
							speed = 15 + other.sp;
							gamepad_id = other.player_id;
							origin = _player_object_id;
							damage = _damage;
							}
				
						with(instance_create_layer(gun_muzzle_x,gun_muzzle_y,"Instances",real(_gun_projectile)))
							{
							direction = other.gun_direction-15;
							image_angle = direction;
							speed = 15 + other.sp;
							gamepad_id = other.player_id;
							origin = _player_object_id;
							damage = _damage;
							}
							
						with(instance_create_layer(gun_muzzle_x,gun_muzzle_y,"Instances",real(_gun_projectile)))
							{
							direction = other.gun_direction+5;
							image_angle = direction;
							speed = 15 + other.sp;
							gamepad_id = other.player_id;
							origin = _player_object_id;
							damage = _damage;
							}
				
						with(instance_create_layer(gun_muzzle_x,gun_muzzle_y,"Instances",real(_gun_projectile)))
							{
							direction = other.gun_direction-5;
							image_angle = direction;
							speed = 15 + other.sp;
							gamepad_id = other.player_id;
							origin = _player_object_id;
							damage = _damage;
							}
						break;
						}
					
					}
				}
			break;
			
			case gun_fire_rate_type.full_auto:
			if (_shoot_auto)
				{
				if gun_delay < 0
					{
					gun_delay = _firerate;
					gamepad_set_vibration(gamepad[player_id],0.5,0.5);
					alarm[0] = room_speed/6;
					cameras[player_id].screen_shake = 5;
					var knockback = 32;
					gun_x += lengthdir_x(knockback,gun_direction);
					gun_y += lengthdir_y(knockback,gun_direction);
					audio_play_sound(_sound,0.5,false);
					if (_shell_image != -1) && (_shell_sprite != -1)
					{
					var shell = instance_create_layer(gun_eject_x,gun_eject_y,"Instances",obj_bullet_shell)
						shell.image_speed = 0;
						shell.image_index = 0;
						var variaty = irandom_range(-35,35);
						shell.hsp = lengthdir_x(6,90 + variaty)
						shell.vsp = lengthdir_y(6,90+ variaty)
					}
					switch(_gun_type)
						{

						case gun_type.pistol:
						case gun_type.rifle:
						case gun_type.assault_rifle:
						case gun_type.sniper:
						case gun_type.rocket_launcher:
						case gun_type.bow:
						
						with(instance_create_layer(gun_muzzle_x,gun_muzzle_y,"Instances",real(_gun_projectile)))
							{
							direction = other.gun_direction + random_range(-5,5);
							image_angle = direction;
							speed = 15 + other.sp;
							gamepad_id = other.player_id;
							origin = _player_object_id;
							damage = _damage;
							}
						
						break;
						
						case gun_type.shotgun:
						with(instance_create_layer(gun_muzzle_x,gun_muzzle_y,"Instances",obj_bullet))
							{
							direction = other.gun_direction;
							image_angle = direction;
							speed = 15 + other.sp;
							origin = _player_object_id;
							damage = 10;
							}
				
						with(instance_create_layer(gun_muzzle_x,gun_muzzle_y,"Instances",obj_bullet))
							{
							direction = other.gun_direction+15;
							image_angle = direction;
							speed = 15 + other.sp;
							origin = _player_object_id;
							damage = 10;
							}
				
						with(instance_create_layer(gun_muzzle_x,gun_muzzle_y,"Instances",obj_bullet))
							{
							direction = other.gun_direction-15;
							image_angle = direction;
							speed = 15 + other.sp;
							gamepad_id = other.player_id;
							origin = _player_object_id;
							damage = 10;
							}
						break;
						}
					
					}
				}
			break;
			}



		avaliable_gun = instance_place(x,y,obj_gun)
		if avaliable_gun != noone
			{
			if _action3
				{
				//Drops current gun Player Already has a gun
				if item_equipped != -1
						{
						var drop = instance_create_layer(x,y,"Instances",obj_gun);
						drop.gun_id = item_equipped;
						drop.hsp = lengthdir_x(16,gun_direction);
						drop.vsp = lengthdir_y(16,gun_direction);
						}
				//Actually picks up gun
				item_equipped = avaliable_gun.gun_id;
				instance_destroy(avaliable_gun);
				}
			}
		}
}
#endregion

sp = sqrt((hsp*hsp) + (vsp*vsp));

//Player Death
#region
if hp <= 0
&& alive == true
	{
	//Drops current gun 
				if item_equipped != -1
						{
						var drop = instance_create_layer(x,y,"Instances",obj_gun);
						drop.gun_id = item_equipped;
						drop.hsp = lengthdir_x(25,gun_direction);
						drop.vsp = lengthdir_y(25,gun_direction);
						}
	alive = false;
	var limbs;
	instance_create_layer(x,y,"Instances",obj_blood);
	limbs[0] = instance_create_layer(x,y,"Instances",obj_head_limb);
		limbs[0].sprite_index = corpse_head;
	limbs[1] = instance_create_layer(x,y,"Instances",obj_chest_limb);
		limbs[1].sprite_index = corpse_torso;
	limbs[2] = instance_create_layer(x,y,"Instances",obj_limb);
		limbs[2].sprite_index = corpse_limb;
	limbs[3] = instance_create_layer(x,y,"Instances",obj_limb);
		limbs[3].sprite_index = corpse_limb;
	limbs[4] = instance_create_layer(x,y,"Instances",obj_limb);
		limbs[4].sprite_index = corpse_paw;
	limbs[5] = instance_create_layer(x,y,"Instances",obj_limb);
		limbs[5].sprite_index = corpse_paw;
	
	for(i = 0; i < 6; i++)
		{
		var velocity = irandom_range(3,9);
		var dispercment = irandom_range(-25,25) 
		limbs[i].hsp = lengthdir_x(velocity,impact_direction + dispercment);
		limbs[i].vsp = lengthdir_y(velocity,impact_direction + dispercment);
		}	
	my_camera.follow = noone;
	instance_destroy();
	}
#endregion