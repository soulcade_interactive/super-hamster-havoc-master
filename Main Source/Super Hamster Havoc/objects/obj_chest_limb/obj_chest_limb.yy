{
    "id": "06ef79d3-d2c1-48f6-927a-26d3e21b1330",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_chest_limb",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2b6a2388-0c05-4d25-adae-2d84df7efe35",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9b1ff14c-01bd-42a9-9fc6-906999c4a5c1",
    "visible": true
}