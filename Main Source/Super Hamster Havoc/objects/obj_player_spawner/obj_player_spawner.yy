{
    "id": "bd6683f1-d6bc-45ff-a856-9326488ab54f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_spawner",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "4bc57f59-0690-4bac-a7d6-fcb68aeaf251",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": true,
            "rangeMax": 7,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "player_spawn_id",
            "varType": 1
        }
    ],
    "solid": false,
    "spriteId": "fa0bbbb1-a841-456c-8c22-8f5ee06493c6",
    "visible": false
}