{
    "id": "33634c94-b392-4387-9e8a-14a6330a1256",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gmlive",
    "eventList": [
        {
            "id": "6e5c0ac4-1860-411c-b8f4-c5db61a47b47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "33634c94-b392-4387-9e8a-14a6330a1256"
        },
        {
            "id": "dbf2c095-f9d4-463d-aec3-33743ecec611",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "33634c94-b392-4387-9e8a-14a6330a1256"
        },
        {
            "id": "199cf3f3-ac06-49fd-8c14-4fb24395ad56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 62,
            "eventtype": 7,
            "m_owner": "33634c94-b392-4387-9e8a-14a6330a1256"
        },
        {
            "id": "757ca6e6-bc21-4265-bd84-066381e9371f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "33634c94-b392-4387-9e8a-14a6330a1256"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}