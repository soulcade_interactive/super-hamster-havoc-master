{
    "id": "7c79fda4-3ab9-4fbb-b246-deea48f2448b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_water",
    "eventList": [
        {
            "id": "814e97aa-770a-43a4-8276-7d7b2fd4210b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7c79fda4-3ab9-4fbb-b246-deea48f2448b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f5718b43-b724-440c-8035-e1c0a047fac2",
    "visible": true
}