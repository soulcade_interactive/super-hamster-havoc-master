{
    "id": "eafdcf69-ebad-48f4-931d-5c446e01c91f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gun",
    "eventList": [
        {
            "id": "2eebdddc-ea2c-4014-bf66-db60e4389f19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "eafdcf69-ebad-48f4-931d-5c446e01c91f"
        },
        {
            "id": "2a471d99-0fcd-41f9-bf99-a209d5d520d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "eafdcf69-ebad-48f4-931d-5c446e01c91f"
        },
        {
            "id": "8a13a65c-6060-4a5e-920b-266b5a034fc4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "eafdcf69-ebad-48f4-931d-5c446e01c91f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "a14c4182-44b9-47fd-9d0c-3dd39cba5bd2",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": [
                
            ],
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "-1",
            "varName": "gun_id_override",
            "varType": 1
        }
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}