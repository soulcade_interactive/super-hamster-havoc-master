/// @description Insert description here
// You can write your code in this editor
if (live_call()) return live_result;
var gun_data = json_decode(projectile_weapon[gun_id]);
name = gun_data[? "name"];
sprite_index = real(gun_data[? "sprite_index"]);
image_index = real(gun_data[? "image_index"]);
ds_map_destroy(gun_data);

//Horizontal Collisions and Movement
		if (place_meeting(x+hsp,y,obj_solid_object_parent))
			{
			//Increment till collisions are Pixel Perfect
			while (!place_meeting(x+sign(hsp),y,obj_solid_object_parent))
				{
				x += sign(hsp)	
				}
			//Set speed to zero upon flush collision
			hsp = 0;
			}
		x += hsp;

		//Vertical Collisions and Movement
		if (place_meeting(x,y+vsp,obj_solid_object_parent))
			{
			//Increment till collisions are Pixel Perfect
			while (!place_meeting(x,y+sign(vsp),obj_solid_object_parent))
				{
				y += sign(vsp)	
				}
			//Set speed to zero upon flush collision
			vsp = 0;
			}
		y += vsp;
		
hsp = lerp(hsp,0,0.25);
vsp = lerp(vsp,0,0.25);