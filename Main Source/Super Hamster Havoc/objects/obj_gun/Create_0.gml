/// @description Insert description here
// You can write your code in this editor
/*
gun_id = irandom_range(0,ds_list_size(projectile_weapon));
gun_info = json_decode(projectile_weapon[| gun_id]);
sprite_index = ds_map_read(gun_info,"sprite_index");
image_index = ds_map_read(gun_info,"image_index");
image_angle = choose(0,25,45,-25,-45);
depth = -y-1;
*/
randomize();
image_speed = 0;
gun_id = irandom_range(0,array_length_1d(projectile_weapon)-1);

if gun_id_override != -1
	{
	gun_id = gun_id_override;	
	}
var gun_data = json_decode(projectile_weapon[gun_id]);
name = gun_data[? "name"];
sprite_index = real(gun_data[? "sprite_index"]);
image_index = real(gun_data[? "image_index"]);
ds_map_destroy(gun_data);


image_angle = choose(0,25,45,-25,-45);
depth = -y-1;

hsp = 0;
vsp = 0;