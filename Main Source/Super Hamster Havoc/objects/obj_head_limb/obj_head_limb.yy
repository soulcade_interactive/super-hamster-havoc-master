{
    "id": "a0f4cbdb-8680-4f79-9c76-14ff7ac1f0d7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_head_limb",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2b6a2388-0c05-4d25-adae-2d84df7efe35",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5cf84e15-4850-4c19-a2ac-01c22acfe195",
    "visible": true
}