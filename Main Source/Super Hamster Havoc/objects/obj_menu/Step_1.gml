/// @description Tracks Gamepads
if (live_call()) return live_result;

//Checks for new controllers if in the apropriate menus
#region
switch(menu_current_state)
{
	case menu_state.main:
	case menu_state.warfare_settings:
	case menu_state.warfare_player_select:
	for (var i = 0; i < 12; i++)
	{
		//If input is recieved on an unassigned controller
	     if gamepad_anybutton_pressed(i)
		 && !(gamepad_is_assigned(i))
	    {  
		  //Find the next open slot
	      for (var g = 0; g < controller_limit; g++)
		  {
			if gamepad[g] == noone
				{
				//Assign the new controller to that slot
				gamepad[g] = i;
				exit;
				}
		  }	
	    }
	}
	break;
	
	default:
	break;
}
#endregion

//Tracks Amount of Gamepads
#region
gp_amount = 0;
for(var i = 0; i < controller_limit; i++)
	{
	if gamepad[i] != noone
		{
		gp_amount += 1;
		}
	}
player_amount = gp_amount;
#endregion

// Gets input for all the controllers
#region
for(i=0; i<8; i++)
	{
	//Gets Regular Button input
	_left[i] = gamepad_button_check_pressed(gamepad[i],gp_padl);
	_right[i] = gamepad_button_check_pressed(gamepad[i],gp_padr);
	_up[i] = gamepad_button_check_pressed(gamepad[i],gp_padu);
	_down[i] = gamepad_button_check_pressed(gamepad[i],gp_padd);
	_a_button[i] = gamepad_button_check_pressed(gamepad[i],gp_face1);
	_b_button[i] = gamepad_button_check_pressed(gamepad[i],gp_face2);
	_start[i] = gamepad_button_check_pressed(gamepad[i],gp_start);
	
	//Simulates "pressed" input system for joysticks 
	_left_pressed_alt[i]  = !_left_alt[i]  &&  (gamepad_axis_value(0, gp_axislh) < 0)
	_right_pressed_alt[i] = !_right_alt[i] &&  (gamepad_axis_value(0, gp_axislh) > 0)
	_up_pressed_alt[i]    = !_up_alt[i]    &&  (gamepad_axis_value(0, gp_axislv) < 0)
	_down_pressed_alt[i]  = !_down_alt[i]  &&  (gamepad_axis_value(0, gp_axislv) > 0)
	
	//Gets joystick input
	_left_alt[i]  = (gamepad_axis_value(0, gp_axislh) < 0);
	_right_alt[i] = (gamepad_axis_value(0, gp_axislh) > 0);
	_up_alt[i]    = (gamepad_axis_value(0, gp_axislv) < 0);
	_down_alt[i]  = (gamepad_axis_value(0, gp_axislv) > 0);
	}
#endregion