/// @description Loading/Saving Settings
// You can write your code in this editor

if (check_async_save(settings_save_check))
	{
	show_debug_message( "Check if the file that was saved exists to the OS: " + string(file_exists("default/settings.json")))
	//Once Saved, load back in that same data
	var settings_save_data = load_json_file_async("default/settings.json");
	settings_load_callback_id = -1;
	if settings_save_data != -1
		{
		load_buffer = ds_map_find_value(settings_save_data,"buffer_id");
		settings_load_callback_id = ds_map_find_value(settings_save_data,"settings_load_callback_id");
		}
	}
	


if (settings_load_callback_id != -1)
	{
	if (check_async_load(settings_load_callback_id))
		{
		var settings_json = buffer_read(load_buffer,buffer_string)
		internal_debug_console_message("The contents of the file are as follows: " + settings_json);
		internal_debug_console_message("This file is located at: " + filename_path("default/settings.json"));
		
		var settings_map = json_decode(settings_json);
		
		setting_anti_aliasing = settings_map[? "setting_anti_aliasing"];
		setting_particles = settings_map[? "setting_particles"];
		setting_screenshake = settings_map[? "setting_screenshake"];
		setting_music_channel = settings_map[? "setting_music_channel"];
		setting_sfx_channel = settings_map[? "setting_sfx_channel"];
		setting_controller_vibrate = settings_map[? "setting_controller_vibrate"];
		}
	}
	
if audio_group_is_loaded(audio_music)
	{
	audio_play_sound(snd_menu,1,true);	
	}