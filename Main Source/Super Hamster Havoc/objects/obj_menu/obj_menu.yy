{
    "id": "63c1ccdf-c209-4b69-aba9-cdf49735c591",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_menu",
    "eventList": [
        {
            "id": "6a8b356f-1349-4e03-898f-07cc6e97387b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "63c1ccdf-c209-4b69-aba9-cdf49735c591"
        },
        {
            "id": "e710231d-4946-4b27-8ba9-444afed97d86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "63c1ccdf-c209-4b69-aba9-cdf49735c591"
        },
        {
            "id": "85c89f95-1ffb-4cfd-8c88-c45af8c70d01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "63c1ccdf-c209-4b69-aba9-cdf49735c591"
        },
        {
            "id": "51b5aa32-9bd5-4afb-8153-12f52bedd4eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "63c1ccdf-c209-4b69-aba9-cdf49735c591"
        },
        {
            "id": "7ecc04c9-f266-468a-9b44-6486e3b8ea27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 72,
            "eventtype": 7,
            "m_owner": "63c1ccdf-c209-4b69-aba9-cdf49735c591"
        },
        {
            "id": "cb3bca78-3500-480b-8ae4-943a2bf7c542",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "63c1ccdf-c209-4b69-aba9-cdf49735c591"
        },
        {
            "id": "697f50ec-209d-441d-9565-b8f57808d600",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "63c1ccdf-c209-4b69-aba9-cdf49735c591"
        },
        {
            "id": "faae119b-772b-47f6-899c-ed3bf60e73b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "63c1ccdf-c209-4b69-aba9-cdf49735c591"
        },
        {
            "id": "3461d7b1-bf34-480b-89ac-86f19efca875",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 74,
            "eventtype": 7,
            "m_owner": "63c1ccdf-c209-4b69-aba9-cdf49735c591"
        },
        {
            "id": "ef8b3094-0453-426c-8b55-4155af248d23",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "63c1ccdf-c209-4b69-aba9-cdf49735c591"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}