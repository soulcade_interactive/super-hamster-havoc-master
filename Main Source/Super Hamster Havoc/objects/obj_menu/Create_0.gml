/// @description Insert description here

gp_amount = 0;

//Main Menu Variables
#region
//Menu Text for Main Menu
text[4] = "Forge Mode";
text[3] = "Create a Hamster";
text[2] = "Hamster Warfare";
text[1] = "Horde Mode";
text[0] = "Settings";

image[4] = spr_main_menu_locked;
image[3] = spr_main_menu_locked;
image[2] = spr_main_menu_image;
image[1] = spr_main_menu_locked;
image[0] = spr_main_menu_settings;

flavor_text[4] = "Design and download maps to play with your friends";
flavor_text[3] = "Create the hamster fighters of tommorow";
flavor_text[2] = "Establish the stakes and battle it out.";
flavor_text[1] = "Fight to survive the perpetual rodent warfare";
flavor_text[0] = "Customize various aspects of the game";

flavor_sub_text[4] = "Map Sharing is currently in Beta";
flavor_sub_text[3] = "Customize the looks, clothing, and perks of your band of hamsters";
flavor_sub_text[2] = "Requires at least 2 players, Goes up to 8 players";
flavor_sub_text[1] = "Play Solo or up to 4 Players";
flavor_sub_text[0] = "Change Video, Audio, Controller, and memory settings";

// Variables for Main Menu
main_menu_select = 2;
count = array_length_1d(text);
rot = 0;
menu_angle = 135;
pri = ds_priority_create();
_ystart = 2400;

start_y = 1080;
info_y = 1080;
logo_y = 1080;

#endregion

//Settings Variables
#region
//Menu Text for Main Settings Menu
settings_text[4] = "Graphics";
settings_text[3] = "Audio";
settings_text[2] = "Controller";
settings_text[1] = "Reset all Settings";
settings_text[0] = "Back";

settings_flavor_text[4] = "Adjust Graphical Settings";
settings_flavor_text[3] = "Adjust Audio Settings";
settings_flavor_text[2] = "Adjust Controller Settings";
settings_flavor_text[1] = "Reset all Settings";
settings_flavor_text[0] = "Return to the main menu";

settings_flavor_sub_text[4] = "Adjust settings like anti-aliasing, particle effects, screenshake, etc";
settings_flavor_sub_text[3] = "Adjust the levels of all audio channels";
settings_flavor_sub_text[2] = "Adjust Controller mappings and vibration";
settings_flavor_sub_text[1] = "Set all settings to the out of box default";
settings_flavor_sub_text[0] = "";

main_settings_select = 0;
#endregion

//Character Selection Variables
#region
//Character Roster

//Create Character Roster
character_roster[0] = ds_list_create();
character_roster[1] = ds_list_create();
character_roster[2] = ds_list_create();
character_roster[3] = ds_list_create();

//Add Characters
roster_add_character("guy","Hamilton Wheeler",2,character_type.basic,"- Average Joe\n- Has a cool hat",1);
roster_add_character("girl","Aura Cheeks",1,character_type.basic,"",1);
roster_add_character("buff","Ron `Tuff` Haydigger",1,character_type.buff,"- 2 guns are better than 1\n- Can really take a punch",1);
roster_add_character("nerd","Harry Hampton the 33rd",1,character_type.basic,"- Runs fast\n- Can't really take a punch",1);

//Map Selection Variables
map_select = 0;
//Player Selection
globalvar player_character_id;
globalvar player_character_data;
for (var i=0;i<8;i++)
	{
	player_cursor_x[i] = 960;
	player_cursor_y[i] = 540;
	player_character_id[i] = noone;
	player_character_data[i] = noone;
	}
	
players_are_ready = false;
	

#endregion

//Menu System Variables
#region
	//Variables for Menu State Machine
	globalvar menu_current_state;
	menu_transition = false;
	current_transition_state = transition_state.fade_in;
	menu_transition_alpha = 0;
	menu_transition_target = noone;
	menu_current_state = 0;
	//Enumerates for Menu State Machine
	enum menu_state
		{
		main,
		warfare_player_select,
		warfare_settings,
		warfare_map_select,
		horde_mode_player_select,
		horde_mode_settings,
		horde_mode_map_select,
		settings_main,
		settings_video,
		settings_controller,
		settings_memory,
		settings_audio,
		forge_mode_select
		}
	
		enum transition_state
			{
			fade_in,
			fade_out,
			}
			
		enum character_type
			{
			basic,
			buff
			}
#endregion

// Runs Loop to get input for all controllers
for(i=0; i<8; i++)
	{
	_up[i] = gamepad_button_check_pressed(i,gp_padu);
	_down[i] = gamepad_button_check_pressed(i,gp_padd);
	_left[i] = gamepad_button_check_pressed(i,gp_padl);
	_right[i] = gamepad_button_check_pressed(i,gp_padr);
	_left_alt[i] = false;
	_right_alt[i] = false;
	_up_alt[i] = false;
	_down_alt[i] = false;
	_left_pressed_alt[i] = false;
	_right_pressed_alt[i] = false;
	_up_pressed_alt[i] = false;
	_down_pressed_alt[i] = false;
	_a_button[i] = gamepad_button_check_pressed(i,gp_face1);
	_b_button[i] = gamepad_button_check_pressed(i,gp_face2);
	_start[i] = gamepad_button_check_pressed(i,gp_start);
	}
	

audio_play_sound(snd_menu,1,true);

