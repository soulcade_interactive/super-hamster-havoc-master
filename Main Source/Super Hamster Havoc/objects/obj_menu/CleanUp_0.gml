/// @description Destroy All Data Structures

//Character Roster
	//Delete all the DS Lists 
	for (var i = 0; i < array_length_1d(character_roster); i++)
		{
			ds_list_destroy(character_roster[i])
		}

	//Clear the Array as well by setting the entire variable to zero
	character_roster = 0;
	
//Map List
ds_list_destroy(maps);

/*
//Clear Player Data
for (var i=0;i<8;i++)
	{
	if ds_exists(player_character_data[i],ds_type_map)
		{
		ds_map_destroy(player_character_data[i])
		}
	else
		{
		player_character_data[i] = 0;	
		}
	
	}
*/