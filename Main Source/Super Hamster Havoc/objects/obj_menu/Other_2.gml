/// @description Initialize the menu
if (live_call()) return live_result;
game_configuration();
globalvar steam_gml_version;
steam_gml_version = 0;
//Create a debug log
#region
instance_create_layer(0,0,"Instances",obj_debug_console);
internal_debug_console_message("The Game has started");
internal_debug_console_message("Testing communication with the internal debug log");
internal_debug_console_message("The Game Install Directory is: " + filename_path(string(working_directory)));
//internal_debug_console_message("The Program directory is: " + string(program_directory));
#endregion

//Gamepad System
#region
//Controller Variables
globalvar gamepad,gamepad_rumble,player_score,controller_limit;
controller_limit = 8;
for(var i = 0; i < 12; i++)
	{
	gamepad[i] = noone;	
	gamepad_rumble[i] = 0;
	player_score[i] = 0;
	gamepad_set_axis_deadzone(i,0.25);
	}
globalvar player_amount;
player_amount = 0;
#endregion



//Initialize Settings
globalvar setting_anti_aliasing, setting_particles, setting_screenshake, setting_master_channel, setting_music_channel, setting_sfx_channel, setting_controller_vibrate;
setting_anti_aliasing = true;
setting_particles = true;
setting_screenshake = true;
setting_master_channel = 1;
setting_music_channel = 1;
setting_sfx_channel = 1;
setting_controller_vibrate = true;

//Create a save file for settings if it doesn't exist, or loads a prexisting one
#region
settings_save_check = -1;
if !file_exists("default/settings.json")
	{
	internal_debug_console_message("default/settings.json was not found, so it is being created");
	var temp_settings_ds_map = ds_map_create();
	ds_map_add(temp_settings_ds_map,"setting_anti_aliasing", setting_anti_aliasing);
	ds_map_add(temp_settings_ds_map,"setting_particles", setting_particles);
	ds_map_add(temp_settings_ds_map,"setting_screenshake", setting_screenshake);
	ds_map_add(temp_settings_ds_map,"setting_music_channel", setting_music_channel);
	ds_map_add(temp_settings_ds_map,"setting_sfx_channel", setting_sfx_channel);
	ds_map_add(temp_settings_ds_map,"setting_controller_vibrate", setting_controller_vibrate);

	settings_save_check = ds_map_to_json_file_async(temp_settings_ds_map, "settings.json");
	ds_map_destroy(temp_settings_ds_map);
	
	
	}
else
	{
	//If Saved, load back in that same data
	internal_debug_console_message("default/settings.json was found, so it is being loaded");
	var settings_save_data = load_json_file_async("default/settings.json");
	settings_load_callback_id = -1;
	if settings_save_data != -1
		{
		load_buffer = ds_map_find_value(settings_save_data,"buffer_id");
		settings_load_callback_id = ds_map_find_value(settings_save_data,"settings_load_callback_id");
		}
	}
#endregion


//Load Audio
#region
audio_group_load(audio_music);
audio_group_load(audio_sound_effects);
#endregion

//Load Guns
#region
//Initialize projectile weapon system
init_projectile_weapon_system();

projectile_weapon[0] = register_projectile_weapon("M9", "The most generic of handguns.", spr_weapons1,1,gun_type.pistol,gun_fire_rate_type.semi_auto,5,gun_projectile.regular_bullet,10,true,snd_pistol_single_fire);
projectile_weapon[1] = register_projectile_weapon("Model 870", "One Bang per pump.", spr_weapons1,2,gun_type.shotgun,gun_fire_rate_type.pump_action,25,gun_projectile.slug,10,false,snd_shoot_shotgun);
projectile_weapon[2] = register_projectile_weapon("AK47", "A gas powered pew pew machine", spr_weapons1,4,gun_type.assault_rifle,gun_fire_rate_type.full_auto,5,gun_projectile.rifle_bullet,5,false,snd_shoot_ak);
projectile_weapon[3] = register_projectile_weapon(".50 Cal Sniper Rifle", "The long distance pew pew rifle", spr_weapons1,3,gun_type.sniper,gun_fire_rate_type.semi_auto,15,gun_projectile.rifle_bullet,30,false,snd_sniper_single_fire);
projectile_weapon[4] = register_projectile_weapon("AT4", "The Tube of Noobs", spr_weapons1,5,gun_type.rocket_launcher,gun_fire_rate_type.semi_auto,50,gun_projectile.rpg,20,false,snd_rocket_launcher_shoot);
projectile_weapon[5] = register_projectile_weapon("Revolver", "The yee-haw pistol.", spr_weapons1,6,gun_type.pistol,gun_fire_rate_type.semi_auto,10,gun_projectile.regular_bullet,20,true,snd_revolver_single_fire);
projectile_weapon[6] = register_projectile_weapon("The TRBO", "For when one rocket is not enough", spr_weapons1,7,gun_type.rocket_launcher,gun_fire_rate_type.full_auto,5,gun_projectile.rpg,3,false,snd_rocket_launcher_shoot);

for(i=0; i<array_length_1d(projectile_weapon); i++)
	{
	internal_debug_console_message(projectile_weapon[i]);
	}
#endregion

//Load Maps
#region
globalvar maps;
maps = ds_list_create();
map_select = 0;

//Add Grassland
map_add_room("Grassland Test Level", "This is a level to test the level loading system", room_level_grassland);
//Add Grassland
map_add_room("Old Forest", "A secluded forest riddled with landing balls form long ago.", room_level_oldforest);
//Add Random Level Selection
map_add_room("Random", "Picks a Random Level", room_level_grassland);
#endregion

//Create Gamemodes
globalvar gamemode,gamemode_select;
gamemode = ds_list_create();
gamemode_select = 0;

add_gamemode("Last Man Standing", "Every person has one life, and the last one standing is the victor.", 2, false);
add_gamemode("Rocket Royale","Every Person has One life, every weapon is a heat seeking, fully automatic rocket launcher",2,false);
//add_gamemode("5 Minute Deathmatch", "Free for all with a 5 minute time limit. The player with the larget rapsheet wins", 2, false);
add_gamemode("Debug Gamemode", "A Gamemode with for testing maps in a singleplayer setting", 1, false);