{
    "id": "fe05c09d-b289-452a-b662-1fcb63587192",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "tileset_grassland",
    "auto_tile_sets": [
        {
            "id": "2fb57273-e49b-4b3c-aa30-551cc529bc24",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "grassland_stage_walls",
            "tiles": [
                16,
                24,
                23,
                11,
                14,
                20,
                0,
                10,
                13,
                0,
                22,
                12,
                31,
                30,
                32,
                15
            ]
        },
        {
            "id": "8595e307-cb6a-4047-8fcc-83b1d197a750",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "grassland_shadows",
            "tiles": [
                103,
                122,
                120,
                121,
                102,
                112,
                0,
                113,
                100,
                0,
                110,
                114,
                101,
                123,
                124,
                0
            ]
        },
        {
            "id": "d660da70-d655-4566-882d-9ed75c4eca46",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "grassland_treetops",
            "tiles": [
                73,
                92,
                90,
                91,
                72,
                82,
                68,
                83,
                70,
                69,
                80,
                84,
                71,
                93,
                94,
                0
            ]
        },
        {
            "id": "4b81c412-ce49-4336-9ab1-29abcb1e6635",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "grassland_grass",
            "tiles": [
                45,
                62,
                60,
                61,
                42,
                52,
                44,
                53,
                40,
                43,
                50,
                54,
                41,
                63,
                64,
                0
            ]
        }
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 16,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "c289e40c-bdd5-4851-a3b7-e26f2996ac8b",
    "sprite_no_export": false,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
            24,
            25,
            26,
            27,
            28,
            29,
            30,
            31,
            32,
            33,
            34,
            35,
            36,
            37,
            38,
            39,
            40,
            41,
            42,
            43,
            44,
            45,
            46,
            47,
            48,
            49,
            50,
            51,
            52,
            53,
            54,
            55,
            56,
            57,
            58,
            59,
            60,
            61,
            62,
            63,
            64,
            65,
            66,
            67,
            68,
            69,
            70,
            71,
            72,
            73,
            74,
            75,
            76,
            77,
            78,
            79,
            80,
            81,
            82,
            83,
            84,
            85,
            86,
            87,
            88,
            89,
            90,
            91,
            92,
            93,
            94,
            95,
            96,
            97,
            98,
            99,
            100,
            101,
            102,
            103,
            104,
            105,
            106,
            107,
            108,
            109,
            110,
            111,
            112,
            113,
            114,
            115,
            116,
            117,
            118,
            119,
            120,
            121,
            122,
            123,
            124,
            125,
            126,
            127,
            128,
            129,
            130,
            131,
            132,
            133,
            134,
            135,
            136,
            137,
            138,
            139,
            140,
            141,
            142,
            143,
            144,
            145,
            146,
            147,
            148,
            149,
            150,
            151,
            152,
            153,
            154,
            155,
            156,
            157,
            158,
            159,
            160,
            161,
            162,
            163,
            164,
            165,
            166,
            167,
            168,
            169,
            170,
            171,
            172,
            173,
            174,
            175,
            176,
            177,
            178,
            179,
            180,
            181,
            182,
            183,
            184,
            185,
            186,
            187,
            188,
            189,
            190,
            191,
            192,
            193,
            194,
            195,
            196,
            197,
            198,
            199,
            200,
            201,
            202,
            203,
            204,
            205,
            206,
            207,
            208,
            209,
            210,
            211,
            212,
            213,
            214,
            215,
            216,
            217,
            218,
            219,
            220,
            221,
            222,
            223,
            224,
            225,
            226,
            227,
            228,
            229,
            230,
            231,
            232,
            233,
            234,
            235,
            236,
            237,
            238,
            239,
            240,
            241,
            242,
            243,
            244,
            245,
            246,
            247,
            248,
            249
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 12,
    "tile_count": 250,
    "tileheight": 64,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 64,
    "tilexoff": 0,
    "tileyoff": 0
}