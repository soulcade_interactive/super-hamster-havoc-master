{
    "id": "efb46cfc-f7a4-4da4-8083-eb606afcd3dc",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_bebas_kai_36_pt",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bebas Kai",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a26832ed-ff42-4cc6-af93-75ce231d6431",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 58,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "967eb2bc-4003-4e45-9edb-a60c1beac649",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 58,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 296,
                "y": 242
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "025942c4-8ed4-4131-bb3c-9ed4c60b4138",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 58,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 305,
                "y": 242
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "097f5fb4-ee75-4ea9-87f2-a0bd2bc4d7ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 58,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 321,
                "y": 242
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "61472b47-f8da-4050-800b-e9737dfd783e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 344,
                "y": 242
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "197fb4a5-cc5e-4785-bd31-eb1d929093a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 58,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 363,
                "y": 242
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "584b2b2d-08ae-4ad0-a965-c58e38160622",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 58,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 391,
                "y": 242
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "2fa15a06-cbdf-4a07-9478-f0826efed519",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 58,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 415,
                "y": 242
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "d7263111-ccfe-47dd-9bc3-0f93d5e2e2e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 58,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 423,
                "y": 242
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "30fadc66-6795-4012-b5ca-b7bd7e8353c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 58,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 437,
                "y": 242
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "52d4cfcc-7e15-4559-b8fe-d6e2ab18edef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 58,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 470,
                "y": 242
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "2319d203-0f10-454d-83ab-23828222f13b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 58,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 137,
                "y": 302
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c05dfe22-5733-4208-9971-f1ed5f70fb3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 58,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 489,
                "y": 242
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "861e6be4-1af9-4d24-840e-b1f74ccaba45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 58,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 302
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "13632da0-11c1-43b3-b07b-e1e47bf6a7ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 58,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 16,
                "y": 302
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "5746a326-14ba-4876-8546-b2e1192720de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 58,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 25,
                "y": 302
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "003a17fa-2600-4fee-a437-c85a4cbb2ab4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 45,
                "y": 302
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d514311e-7b56-473d-b5f0-2def550ef431",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 58,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 64,
                "y": 302
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "80b335ae-5095-4571-b33e-6ee05ba6d73a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 58,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 79,
                "y": 302
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "ea0aa978-4953-4c35-8962-b997a992c799",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 58,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 97,
                "y": 302
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "9a9f9855-77fa-40e8-bfb0-2278f23e8c62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 116,
                "y": 302
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b5ba4adc-ed50-4a65-8787-c5c800771034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 58,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 277,
                "y": 242
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "013c8797-fd30-46d6-a420-8b0b5d6bf340",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 58,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 451,
                "y": 242
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9cd32f55-352d-4470-bd3f-ae022ce2341a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 58,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 258,
                "y": 242
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "80f478d1-a04b-44ee-9dd6-399c1c64d078",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 58,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 24,
                "y": 242
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "6f711f9e-30ee-455e-88ad-52d195e8a4aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 58,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 340,
                "y": 182
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "7cf11907-cdd3-48e8-a626-ba7dea10388a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 58,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 359,
                "y": 182
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "dd3de2e7-9438-4aa7-a332-1aa212329140",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 58,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 368,
                "y": 182
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "0ff7e656-aaa0-4938-89fd-faf3a923aa66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 58,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 377,
                "y": 182
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "bd2f7761-f798-42b0-8cdf-e6ec58ccbb96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 58,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 399,
                "y": 182
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fe9edf50-6690-4361-816b-8ae86a1f3520",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 58,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 421,
                "y": 182
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "fbd795aa-1c52-439b-adb8-aafd2b51e8b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 58,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 443,
                "y": 182
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8df4ce95-c680-4b38-86b6-1e1d6e0b8e03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 58,
                "offset": 1,
                "shift": 35,
                "w": 34,
                "x": 461,
                "y": 182
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "214a0152-1f3a-4c1e-b378-5f9c6ea2e8e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 2,
                "y": 242
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "4c4e018d-5c72-4e5b-87c3-c06d09122c42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 58,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 44,
                "y": 242
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ed853508-88e1-4534-8445-1ba58823ea67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 58,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 215,
                "y": 242
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "0f44f5f9-5dd0-4ecf-b2e4-5e5346dccb57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 63,
                "y": 242
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "df40f15d-7ccb-4e10-8b1d-f664f8029877",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 58,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 82,
                "y": 242
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "a532bdb2-3131-4ba7-a9f8-80f4b8e5657c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 58,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 100,
                "y": 242
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1ba529ff-6749-48c7-aaf6-6d9aa19b4b65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 58,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 117,
                "y": 242
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "8e2f07de-b9ae-42dc-bfa6-b0ac3a068050",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 58,
                "offset": 2,
                "shift": 22,
                "w": 17,
                "x": 136,
                "y": 242
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ca497bc1-1fc5-486a-879d-08976291f1ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 58,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 155,
                "y": 242
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "abb546e7-b560-4919-a308-b615da20d0e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 58,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 163,
                "y": 242
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "f6094309-3654-43f2-902c-dd2ee7408861",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 177,
                "y": 242
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d83f5a6b-8b43-414c-9558-ab965fee1bba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 58,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 198,
                "y": 242
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "4e412702-99a2-4663-aa67-f6f303ebcc5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 58,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 233,
                "y": 242
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "24d91de7-7a7d-4d48-b75f-2a964abf8306",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 58,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 182,
                "y": 302
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "c949944d-e7f3-46ed-a6c5-1bbb7a11f456",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 133,
                "y": 362
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e452644a-0547-4985-b4db-8504cccc2b1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 58,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 202,
                "y": 302
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "adaa8421-df10-410e-8b59-b60490065e61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 189,
                "y": 362
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "31fe4b29-71f3-4666-babb-4992b80404d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 209,
                "y": 362
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "23de8334-f932-41e5-981c-7eb493e5162d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 58,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 229,
                "y": 362
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "fa79ed61-6f97-4794-a761-2c0c76d758d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 58,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 247,
                "y": 362
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "4517e4c0-4168-4442-a9ae-cbba80e4df13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 266,
                "y": 362
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "b5ef1aaf-6e05-4aa0-8de2-b470b159e04e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 285,
                "y": 362
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a75121a1-1d04-4ba7-8e4f-5ad682e73a49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 58,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 306,
                "y": 362
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "5164b47d-6402-4f3b-a834-f47486e25120",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 336,
                "y": 362
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c6574faa-faee-41e5-8c58-29a7a1f24d18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 58,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 358,
                "y": 362
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b05d6efb-9f2a-4677-989a-759716ae652c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 58,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 397,
                "y": 362
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "a50aa51b-d2d0-48de-8a45-4f8db473b959",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 58,
                "offset": 3,
                "shift": 14,
                "w": 10,
                "x": 80,
                "y": 422
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "63f6ddae-e3b8-4f25-a6cb-a7d1e461db1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 58,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 415,
                "y": 362
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "b68d9f3b-3127-494b-a0a6-6f51375143ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 58,
                "offset": 1,
                "shift": 14,
                "w": 10,
                "x": 435,
                "y": 362
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "12d57477-5752-44c4-87bd-fc1c2fd3a081",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 58,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 447,
                "y": 362
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "4631a6ed-90f7-468a-9fa7-39b1e3439a39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 58,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 469,
                "y": 362
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "99a15a7c-aa27-4588-a927-0f89fa8aea3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 58,
                "offset": 0,
                "shift": 12,
                "w": 9,
                "x": 493,
                "y": 362
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "41801c01-e26c-461d-88fa-e1c6b515a2a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 2,
                "y": 422
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a7182e21-de26-4494-bb17-0256b8d0c16e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 58,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 24,
                "y": 422
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "90c6a4ae-3eb2-4e1e-985a-a62f09f35ab2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 58,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 43,
                "y": 422
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b62c2274-9f15-4305-8fce-4833553f61e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 61,
                "y": 422
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "996fc801-5550-4091-b131-b35178660a61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 58,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 171,
                "y": 362
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "1dacbbc8-5d12-4203-b9df-1a0ec164d058",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 58,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 380,
                "y": 362
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f90a0c7b-465a-4f96-934c-04780535fe56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 58,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 152,
                "y": 362
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "328f5e04-159f-4535-b1a8-2dadb1b5da88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 58,
                "offset": 2,
                "shift": 22,
                "w": 17,
                "x": 384,
                "y": 302
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "7031bcd0-e482-4828-8f17-18ebc6973b38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 58,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 221,
                "y": 302
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "f9826da0-d2e5-481f-ac2d-114957562519",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 58,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 229,
                "y": 302
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3bacf854-13ca-4465-8574-387b1db0fa41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 19,
                "x": 243,
                "y": 302
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "edb44610-587d-44c1-af94-8034a124b5a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 58,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 264,
                "y": 302
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "8caac11a-ffa0-4dda-87a8-632c054388f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 58,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 281,
                "y": 302
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "34e3ed59-5ee8-4395-bca3-86b27e13aa3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 58,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 306,
                "y": 302
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "5a1eaddc-10bd-48a1-8daa-2f0d35af1873",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 326,
                "y": 302
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "2eec11f3-0e11-4aa9-b610-9f866747c062",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 58,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 345,
                "y": 302
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6060eff8-e5c4-42e9-9135-6c2d422a6d79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 364,
                "y": 302
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "9d5bb0b3-c82f-467b-8bb9-71b528b510f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 403,
                "y": 302
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "73c0f699-08d1-4a4c-a7d4-5fb280a7184b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 58,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 115,
                "y": 362
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "16dcd1a7-234a-4cef-bf48-2e2bd78e9ca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 58,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 423,
                "y": 302
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d71c26ba-ee0c-46b9-950a-bcc928948d31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 442,
                "y": 302
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "f51eaa24-b659-4f39-ba37-7b4d8ef92d15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 461,
                "y": 302
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "24d4e282-62b2-4678-9768-2b4f5092790c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 58,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 2,
                "y": 362
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "ba1eb6ac-e039-4aed-b3f9-68134adb40d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 32,
                "y": 362
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "09b53604-7dab-4548-b490-19ff7031c5f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 58,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 54,
                "y": 362
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c4d13d98-e8c0-4db8-b3fc-7005651f6e97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 58,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 76,
                "y": 362
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3611571a-09d6-4e85-a838-cecb8dfda613",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 58,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 94,
                "y": 362
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "28c7cf8b-5c57-4cd9-94f3-160abf2d0987",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 58,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 108,
                "y": 362
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "81cf22d2-9a8f-4b76-b07a-f440c94e76b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 58,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 326,
                "y": 182
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "2ffa7de0-bf60-4fba-af4e-9e998302906f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 58,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 159,
                "y": 302
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "0bacff09-febc-4c24-9432-e4429a00b4b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 58,
                "offset": 0,
                "shift": 9,
                "w": 0,
                "x": 324,
                "y": 182
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "bb2c9cc7-235f-44f5-bfb0-6eebd6e12bc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 58,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 360,
                "y": 62
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "863a0cda-72fc-44d0-89c6-642329484a9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 482,
                "y": 2
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "da09a3bd-e214-4495-9091-7aef416fd27b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 58,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "ca6342d5-8416-4424-9abe-d809201f7fd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 58,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 22,
                "y": 62
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "1d39a86b-fba0-485c-8e50-0f497d9028d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 58,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 44,
                "y": 62
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "c8d42340-3ea8-4f86-8e88-037f810ccf48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 58,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 66,
                "y": 62
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "45465b0f-9090-4e83-b60e-c3ea97e9949e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 58,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 73,
                "y": 62
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "cf455b92-8302-40c9-8071-a319de7bbe46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 58,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 92,
                "y": 62
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "9a33d08d-af67-4478-8a41-11dd2c405656",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 58,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 106,
                "y": 62
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "c54aaa6a-a6ae-4741-ba9d-a890f8718aac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 58,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 142,
                "y": 62
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "326b8349-629d-446b-8967-c4e40087d3ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 58,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 164,
                "y": 62
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "ef67af17-defe-46db-b4fb-9b99e70ced86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 58,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 338,
                "y": 62
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "baa26c63-5feb-4dce-941e-7076ba3320fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 58,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 184,
                "y": 62
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "602689b9-f4c8-49af-b981-20e7330fb0f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 58,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 198,
                "y": 62
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "ca9cccac-70fc-42f0-a8a0-19fcbe724a53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 58,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 234,
                "y": 62
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "1df6949c-3789-41ad-8c65-2300f472e211",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 58,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 246,
                "y": 62
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "53e636e7-12f5-49f6-8ea6-0fb1b0024385",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 58,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 259,
                "y": 62
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "073cdc9a-70ad-4198-a42c-d54e6218ea37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 58,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 281,
                "y": 62
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "9993bfd7-530e-4a25-9557-f40d45a68d34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 58,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 294,
                "y": 62
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "284d8825-bd19-4922-a41e-0affe0aa6072",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 58,
                "offset": 3,
                "shift": 12,
                "w": 9,
                "x": 308,
                "y": 62
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "a9d18a63-0cf0-475e-ada0-1200c8bfd8a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 319,
                "y": 62
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "37f2d169-ae8b-4927-b409-551aed84ce89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 58,
                "offset": 1,
                "shift": 24,
                "w": 20,
                "x": 460,
                "y": 2
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "750fcc4a-9509-44f4-a862-ef0817fb1a22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 58,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 155,
                "y": 62
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "ba84665e-f152-4edf-9340-154b0f3bb40a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 58,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 448,
                "y": 2
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "0f8538c8-f51d-4558-84d9-931fb41ccbab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 58,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "e4aecac1-c0ae-4b21-95b1-0a2ea8f0bf17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 58,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "4a1ed59b-5b87-49af-8bbe-39dd4b1cf9b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "2c0e8fac-344c-4541-bf69-c33cc588d0ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 58,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "d6da0e4e-6f02-4fc8-ad90-6a482638855c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 58,
                "offset": 0,
                "shift": 27,
                "w": 26,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "08c7142b-b1e1-44ff-bb55-2a5d9cf077fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 58,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "a9f97914-daf7-4572-8dea-9ca52e60d006",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 58,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "df4f9109-55a8-4bd2-b07b-0cccae1c52ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "2446e916-704a-4a2d-8c81-eec9ca6e0489",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "0edb27dd-94b5-45ba-8918-3654d51a0df6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "78cbe6af-3d09-410c-9468-c922924b5b41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "2ba5c93a-8d03-435c-b293-b48195460268",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 411,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "644ecb48-c5f8-439a-b613-d0fd6a3f79ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 247,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "656a9fe4-32ec-4a19-88f2-66a14d5232e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 58,
                "offset": 0,
                "shift": 29,
                "w": 28,
                "x": 269,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "275fba1c-286a-4256-80a0-a18532b03cf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 58,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 299,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "34774228-2142-4895-9ec2-5344a43c8c3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 58,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 317,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "0b48ec7c-53f6-4372-a8f4-8fd690ee0ac0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 58,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 335,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "f2323e92-6653-4022-adca-bd58900cf5eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 58,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 353,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "165c561f-55d6-4f0b-964c-c95536126536",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 58,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 371,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "3aa28e8a-69e0-462b-b078-ce0f297d551b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 58,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 389,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "36643b24-f789-4327-a56b-10b9cbc49572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 58,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 400,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "a422c3a9-3898-4c5c-822c-37ab1497a2ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 58,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 433,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "02054755-8ec9-42d5-9e0e-82345ec652f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 58,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 369,
                "y": 62
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "f10733b1-1964-44c4-9281-4518130bef6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 58,
                "offset": 0,
                "shift": 21,
                "w": 19,
                "x": 296,
                "y": 122
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "01d5e2de-0190-4138-b23a-3a42405e95f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 58,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 384,
                "y": 62
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "7bb9ee8f-6b16-4c6f-afa3-e7b71aaf5007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 365,
                "y": 122
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "2c0a6317-8112-4335-8620-013b43c6719c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 384,
                "y": 122
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "c6d10948-4645-419f-9f2d-d6721fc82ac1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 403,
                "y": 122
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "1bf72489-7cf1-44f5-a039-f68ba56267d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 422,
                "y": 122
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "1667feff-9138-4e4f-8b17-850e4b673244",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 441,
                "y": 122
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "9f1da0ce-628a-4ef1-b4ec-5f4628a26e7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 58,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 460,
                "y": 122
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "97882658-9ff9-413d-87ca-bde4750ddcc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 58,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 481,
                "y": 122
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "cfa831e1-964d-4ecd-9c61-87330d6fffc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 2,
                "y": 182
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "bc556591-c295-465c-8844-2fc1dca40e33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 21,
                "y": 182
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "c674ac00-0c0b-469d-ba34-a6ed40648fd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 58,
                "y": 182
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "9eab97aa-45cc-42c4-9c48-83881910bc99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 286,
                "y": 182
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "310b39fb-1fa1-4a92-ae94-1ac2e5748255",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 58,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 77,
                "y": 182
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "f4a5ba18-b65f-43cc-9e39-b43b269ae2f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 58,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 99,
                "y": 182
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "fc1b7f72-ba26-4678-bcf1-52f7abde6389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 58,
                "offset": 1,
                "shift": 34,
                "w": 34,
                "x": 118,
                "y": 182
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "08b470f3-8c41-421a-adaa-d2c977a1a516",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 154,
                "y": 182
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "1a7ca3cf-04d6-4f8b-8a33-1af092709343",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 176,
                "y": 182
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "e7014844-6abf-4dd2-8c31-845016ec2f4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 198,
                "y": 182
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "c2d008a6-6f4c-4a40-82bd-1a211c9141e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 220,
                "y": 182
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "05160527-764d-4629-bcb1-bdb68ce8ae60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 242,
                "y": 182
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "a0898bcb-7f5e-44af-89f1-f8b57d05375d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 58,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 264,
                "y": 182
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "b5fd4687-f837-4535-b078-a74eb8b3438a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 58,
                "offset": 0,
                "shift": 29,
                "w": 28,
                "x": 335,
                "y": 122
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "16bb4ef9-0e05-4d47-8876-9e8e07876879",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 58,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 40,
                "y": 182
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "83a837d8-608b-4323-953c-dbd539c25b50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 58,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 317,
                "y": 122
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "3e1a4459-810a-449d-9750-16e5515447a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 58,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 62,
                "y": 122
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "a5c9f5b6-daea-413e-844d-17279d716315",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 58,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 404,
                "y": 62
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "cd05e5f3-524c-4fae-9819-4221b118d1c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 58,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 422,
                "y": 62
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "39032a45-0235-4832-933c-6a723a9266cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 58,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 440,
                "y": 62
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "ef9756ac-ba10-41da-9c45-153604dd15e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 58,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 451,
                "y": 62
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "e12f63b5-128a-4e1e-a177-eaa0cc2b3c14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 58,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 462,
                "y": 62
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "b8add836-eef4-4807-a28d-70fad2ea446e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 58,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 477,
                "y": 62
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "0e04f5de-c538-4190-b8b7-d06c074a463f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 58,
                "offset": 0,
                "shift": 21,
                "w": 19,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "c2279257-56f7-4594-88a6-e680eaaa99e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 58,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 23,
                "y": 122
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "50394c71-582a-4044-816f-dec80a4a831f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 43,
                "y": 122
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "6bb10af0-47b9-4e9d-8f89-b450887cba67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 80,
                "y": 122
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "d5b9e446-2bca-41d1-91d0-98d911074c49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 277,
                "y": 122
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "83b85642-3ebe-4d38-8348-978c8bd88082",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 99,
                "y": 122
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "25bb270d-fa20-4ad4-b3aa-acfc1afcd63a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 118,
                "y": 122
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "f664e5aa-6643-4d97-9e93-aa503c65864c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 58,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 137,
                "y": 122
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "dc1b9465-fdb3-4ae2-ba65-5ab1caeabe41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 58,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 159,
                "y": 122
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "43e0cc76-4268-4ea8-8946-20189652fcde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 179,
                "y": 122
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "243057e7-ea93-4d34-bcd6-e8dabb83d3cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 198,
                "y": 122
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "19feb436-4eac-4189-bee1-b310f3437ac8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 217,
                "y": 122
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "69588999-dfdf-4864-87cb-20f1a0750546",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 58,
                "offset": 2,
                "shift": 21,
                "w": 17,
                "x": 236,
                "y": 122
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "e33e8dbd-4f2f-4d53-b6f1-b393448058d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 58,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 255,
                "y": 122
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "3460c959-b1dd-4f96-9b1d-90cbe40034e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 58,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 305,
                "y": 182
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "d9c0b05a-d5cc-4276-8788-44534b42697e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 58,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 92,
                "y": 422
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 36,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}