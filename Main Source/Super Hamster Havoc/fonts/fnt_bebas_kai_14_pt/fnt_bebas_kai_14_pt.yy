{
    "id": "6aa7635a-7840-4e8d-9a8a-b0d32a6c7ff3",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_bebas_kai_14_pt",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bebas Kai",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a1f8eda2-ae8a-4226-ac9b-a608cdb88531",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a976bff9-245b-4666-b35e-6d7d014d1128",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 113,
                "y": 98
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "cd14daa9-8a2a-418c-a0a6-3201dea92208",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 118,
                "y": 98
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "255394cb-9cf7-4a6d-a1f9-6c1ce3b8fe51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 127,
                "y": 98
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "bf5f42d8-0120-46d1-8d7b-d4dce6abc627",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 138,
                "y": 98
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "4bc4b92c-49b9-415a-8093-09ab7f1369e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 148,
                "y": 98
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "3e19f513-d183-4ce8-b680-4b89b485cf33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 161,
                "y": 98
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "61c81da4-b1c9-4854-8e54-3e56b41fdfdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 172,
                "y": 98
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f5ca884d-a08f-4ad3-8d50-6197ea517006",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 178,
                "y": 98
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "32bdca6e-a4c2-4cd9-a72e-63383a9c7504",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 186,
                "y": 98
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a086a010-1014-40e0-855d-738650f6592c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 204,
                "y": 98
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "ee907a8d-8fb2-4cb2-808d-d2c28e1220cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 38,
                "y": 122
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "48ac4b35-75a1-4ae4-9073-bfb1645e4cba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 213,
                "y": 98
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "82817c40-8a6f-497b-916e-8a50c1de0f2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 218,
                "y": 98
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "61561445-087b-4168-b2bf-2f1435af93a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 225,
                "y": 98
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "edfdb2ca-20e0-4e8c-a433-8802f1893879",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 230,
                "y": 98
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "95f4e035-8d59-47e2-857d-24cc899630b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 239,
                "y": 98
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "32e13c7e-0057-47ff-bb9c-2610e12f81bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "596460c9-1c4a-41cd-b8f5-3faa71c76d41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 10,
                "y": 122
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "62d75c67-d318-4308-b267-4cc580404cb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 19,
                "y": 122
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7d667803-1a1c-42a7-8f58-2c3933216274",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 28,
                "y": 122
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "f3a89a2e-23f7-428a-b220-959e525e3dd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 104,
                "y": 98
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "90fc2bbf-5a6d-40b7-b2d2-914fb86c5bbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 194,
                "y": 98
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f1b17045-da06-4a79-825e-299ce1151316",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 95,
                "y": 98
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "65e9074d-6c81-4d00-a8cf-de00a22a155f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 230,
                "y": 74
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d1c61b65-bad5-4d2b-87e8-20c7d6ca6785",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 145,
                "y": 74
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "2b022d72-fd5a-4753-9651-a4fa4250ffeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 155,
                "y": 74
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "31332ecd-43c4-4ee0-ab5d-9b6eacdfb156",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 160,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "208bb0ff-75d1-42c9-ab92-d5277b25d612",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 165,
                "y": 74
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "71af6a84-561e-448b-a419-8abdb76836fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 175,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "9dc204b0-905a-4634-8a0d-51be5692f231",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 185,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "6b67298a-fbec-4b8a-b5bd-ffbacf8a37de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 195,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "ad755de4-a366-409a-bf17-333bdb4dbee2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 204,
                "y": 74
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "264d1c27-851d-453e-99de-e4f918731e15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 220,
                "y": 74
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "5294531f-73c2-4660-896d-07a3d756220d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 240,
                "y": 74
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "90cc842e-7f73-4568-9dc5-add77eec68a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 75,
                "y": 98
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "02aa8941-862f-4cdb-a125-226e29f03307",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "9050f35d-8fbc-4a92-baf9-e1a08f7cbbd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 11,
                "y": 98
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "1ae979e1-9c71-4483-8ae6-150921eaa4cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 19,
                "y": 98
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "fadb34c3-69e9-4878-850f-106d99131d46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 27,
                "y": 98
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "b207564c-a1cc-4cba-bccc-e47facfb8274",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 37,
                "y": 98
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f0b4e150-8c5a-4029-bc25-2fd8f4c8aa8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 46,
                "y": 98
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f3d9bbdb-603e-438e-822b-59ee5db73ff2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 51,
                "y": 98
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "60ee34bf-3fb9-4f54-9d45-0f62cc233fa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 58,
                "y": 98
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "7180c4ef-df28-4cd0-a283-226866fa6fbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 67,
                "y": 98
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6c36c861-9250-486f-9217-c099737323c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 84,
                "y": 98
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "87d99206-f733-41d6-82be-e26d3438ae59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 58,
                "y": 122
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "eb4be533-d353-442b-928a-84db32d598c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 11,
                "y": 146
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f7e3020c-6bd3-4f00-abd1-544738b38d6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 67,
                "y": 122
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "263870da-21c2-49f5-8256-453f8b33637d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 39,
                "y": 146
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e3caaefe-90d6-4fab-bd71-6acc4b01ffb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 49,
                "y": 146
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e71a12f3-f890-4116-ba62-bcd5d16f8ed2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 58,
                "y": 146
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "9bdf7288-3ce1-4415-b083-c89656321596",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 67,
                "y": 146
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "31869bb2-df97-4542-902e-caa45573d024",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 76,
                "y": 146
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ce08aba8-269d-4e2b-8b25-92aa730a9c9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 86,
                "y": 146
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "2d53f5d4-18f1-4e28-b078-10c43eb894b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 96,
                "y": 146
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e77f0017-a966-4cbe-8bd6-290552aaeded",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 109,
                "y": 146
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "0a0379b4-91e6-4972-9561-6e50ffa119aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 119,
                "y": 146
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "814e201a-8bc7-4233-97c8-78fa6631b4fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 138,
                "y": 146
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ad63b5a7-cdd2-4f67-ae65-8aa326fa4ba6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 226,
                "y": 146
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "9f113033-694d-42a0-b4a6-5940ba56ea6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 147,
                "y": 146
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "fae7d5dc-5662-48ef-b969-fcdc27f8f18d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 156,
                "y": 146
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "86a4a30b-dc64-477c-a38b-f5af2fd42c9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 162,
                "y": 146
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "5353a66b-033d-41b9-af61-48b97dcb5809",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 172,
                "y": 146
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "2dd7894d-e0b1-4e8c-b2ea-d5eff3f6af54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 183,
                "y": 146
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8c628169-8277-467a-9f0a-eab8804ff31d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 189,
                "y": 146
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f28fa1a4-0ea2-4cfb-8c10-a3b11a8ac920",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 199,
                "y": 146
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "6bf2bdd3-fe5c-4676-bb6f-719c1b570533",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 208,
                "y": 146
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f5591cc4-c1df-4ddc-9c37-4709ff88b20e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 217,
                "y": 146
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8fbf8d28-c430-44ec-9224-8c2a72618d65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 31,
                "y": 146
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "6227d725-dea1-42c9-83b9-dc4df47297cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 130,
                "y": 146
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e201a37a-5967-4a05-ba5a-7f2b9a083738",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 21,
                "y": 146
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b497c35a-8d8d-4dae-9152-8920df229752",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 154,
                "y": 122
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "fc230b1b-8c9b-4e97-b5ff-67a5bfff9c91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 76,
                "y": 122
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1716dbf5-7b8e-4b44-9f54-aae2ca78248d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 81,
                "y": 122
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "245f376f-660a-4f49-9cea-4e7e6a6da8ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 88,
                "y": 122
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "7539e898-6885-4bb5-9fbb-b623d1f90e02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 97,
                "y": 122
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "9ce414a6-8325-44b2-af63-50d49e560ca8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 105,
                "y": 122
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "ab7f1e25-b94c-44c6-a228-f6f6f028e6c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 116,
                "y": 122
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b1ae1e0e-66a9-40d7-9971-490c3c94eed4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 125,
                "y": 122
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "2df5079d-6598-4eb1-968e-138ccbfde5e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 135,
                "y": 122
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6c22ca79-7069-4d3b-8be4-2d8d8dcada1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 144,
                "y": 122
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "57137979-b501-42ac-a51c-0501991c7445",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 163,
                "y": 122
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9acc6ab2-c802-4299-b0e8-46558daccf48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "0f9080e1-0a03-4657-8268-949cb9dcc679",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 172,
                "y": 122
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "e6c1d777-1a60-43bc-a49a-3210dcf6c331",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 181,
                "y": 122
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d3261821-5d89-4e26-a9ca-a60676d11da7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 191,
                "y": 122
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "2e1be5bf-7644-4c55-8153-c44de9b77746",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 201,
                "y": 122
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "be19ed76-534f-4737-adb7-9a7b6b688dc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 214,
                "y": 122
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f7db8860-9e6b-431f-912b-d29084054ac0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 224,
                "y": 122
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "794df341-9eec-4083-bffe-d7d7f153a124",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 235,
                "y": 122
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b57cd1b9-eefb-4234-94a9-245d7a68f641",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 244,
                "y": 122
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "bde37ca1-ba26-4631-8dac-2fbfea3b040e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 251,
                "y": 122
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "fd7256f5-bfb1-4712-a382-8fdeed4cca97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 138,
                "y": 74
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "21c6e4ac-a41a-4553-adba-55f266022b20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 48,
                "y": 122
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "6fda2315-32ff-49aa-88a0-4c98885f41ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 0,
                "x": 136,
                "y": 74
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "7a539080-9d44-42b9-84d8-9c6b82219863",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 165,
                "y": 26
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "96c06fcd-f7d8-459c-a3e9-098a5f7efa2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "f0778ee7-0e86-4e90-997d-167f17b5d69b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "44830719-d913-417f-928b-dfaf8897ba93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "36156fc8-42b4-4ed7-9404-cbc60f98418d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 12,
                "y": 26
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "2866d917-97e1-4d20-bba4-bcd7b4b154ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 22,
                "y": 26
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "8bf62bbc-360b-422f-bff2-a34bfa5e2792",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 26,
                "y": 26
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "af3e993b-32fa-47ce-b546-4c918257b23e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 35,
                "y": 26
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "a3340f3f-5023-4960-82b3-954873871837",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 42,
                "y": 26
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "2009e725-828f-4cf3-815e-6d3a728e7dcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 58,
                "y": 26
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "8a4ef80f-d8e5-4f80-b316-b681603f3bab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 70,
                "y": 26
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "3dc1b9d9-7597-4ed5-83fd-8e93ab2d1d1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 155,
                "y": 26
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "1c43391a-15ff-4c77-a1d7-8470ec2037db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 79,
                "y": 26
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "bd64ebf8-92bc-41ad-94c0-286a201c3e2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 22,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 86,
                "y": 26
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "7d910c8b-985a-417f-957b-c5a9817e05ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 102,
                "y": 26
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "2a2a1c96-dabd-485e-986d-fd3bda4e342c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 109,
                "y": 26
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "242c3b80-d60e-44de-89f6-5d111bf7973b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 116,
                "y": 26
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "13169616-2510-4bbd-994c-5191fb79d4b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 126,
                "y": 26
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "3c9407e4-be6a-4301-a248-01b26fac28b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 133,
                "y": 26
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "3da80d2c-4863-4e3a-97b9-9a866e43d254",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 22,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 140,
                "y": 26
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "fcd65c14-5a58-438a-b9f4-5bb4ea9ef3ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 146,
                "y": 26
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "e180358e-0667-41d6-819c-27474e96c7f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "68086616-9271-4c0a-98e0-b897902ec16e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 65,
                "y": 26
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "f0defea4-87a2-4e82-9030-d199cb4a5e4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "6f06bad5-5e3f-4549-a43a-c6f6aec9404c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "b121958f-5850-4d21-bc7e-73bec4af84ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "d4aa0558-1e38-4c8c-ae05-4be0b20158ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "53d9a41a-48cd-461a-adf4-6f29b9675ec7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "b6cf321e-f64f-4549-a5a2-f929d903d552",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 22,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "cde18a3c-eda8-48e5-b620-f55748cf597e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "43eccd94-2022-4662-a4cc-884ec84ad529",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "7dc337cf-0c2e-4d65-820f-a0f57754dd13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "bb43bf21-5b05-4b4b-b7a1-1a5e0e38f719",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "2ae314aa-1092-4d43-9e2d-a00a3793a83c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "05a3c587-2583-48da-9206-8ab3fc802884",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "345cbb75-f07c-47a6-9f71-83ed8272cb34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "23e39508-299a-41c9-92de-df9445ab70fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "cc3aed1b-e3f2-430b-8c7c-eec24ae73b25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "79c5c464-a583-455d-8003-5b6fc2f10fa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 139,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "ab160f37-cfb3-4c45-b4c7-9ad460a4210c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "972915a5-6bb7-4faf-a3af-140c0441e901",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "575beedf-5662-4a19-845c-72d8e7c4a515",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "849c0063-15d0-4075-b7e6-95a47ca96b6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "45909eba-a2c9-4b23-80dc-66cd0691f3c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 22,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "2f9de2bc-cc3e-4902-963c-b5b7c12d9423",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "168dc949-77f5-4265-9092-f86a9a581eb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 22,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "e4514daf-5b5b-4c12-aeaa-535ef28d5d5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 22,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 170,
                "y": 26
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "6f88922c-ea21-4353-8a55-9bd89be94b28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 131,
                "y": 50
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "ded571df-8579-4099-9870-fc1a926b6de7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 178,
                "y": 26
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "378d8c88-6dbc-4169-8684-473bff9d05c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 162,
                "y": 50
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "1a76d859-5d28-4ebb-b238-1318891ef05f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 172,
                "y": 50
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "8eb68913-f229-47af-9ec4-abf538d64b67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 182,
                "y": 50
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "1a44fc42-0741-46b4-85e0-28cf9260b231",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 192,
                "y": 50
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "915a7c40-4058-4e6d-a571-3d51d4e6f936",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 202,
                "y": 50
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "77c7f223-fa9f-4339-bd95-8ed497aa9e50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 212,
                "y": 50
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "cc26dc29-a6f9-47f8-afeb-1ba800c860df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 222,
                "y": 50
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "729defaa-bcfd-4465-a53c-e6a789aba946",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 232,
                "y": 50
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "54334e30-22ab-4662-98d4-853082f22aa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 242,
                "y": 50
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "ccf57e66-44ab-4747-bc45-691890bb8e43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 11,
                "y": 74
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "74bc835c-61d6-4274-b6c4-52aad8757e80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 117,
                "y": 74
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "c6eb0ca7-d869-4a21-a6c4-a972be3bb710",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 22,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 21,
                "y": 74
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "9b3a7045-8f3e-4f5d-8d2e-b623da80a9eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 32,
                "y": 74
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "6a747d76-7811-4981-a2d0-73e9adf65685",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 22,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 41,
                "y": 74
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "8e4eccf9-baf8-4485-a3da-d2ef85269ada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 57,
                "y": 74
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "cc499343-c489-4027-bd6f-64ab82df39e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 67,
                "y": 74
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "22960239-d244-4a8b-97db-4e5485ba0006",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 77,
                "y": 74
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "de0cd13c-4cb0-4dfd-b735-a46a4d982da2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 87,
                "y": 74
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "088a80ba-8907-4fee-9928-35cf6a135a98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 97,
                "y": 74
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "07c87b61-7c74-41ec-8d43-40c652e74dd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 107,
                "y": 74
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "048bed72-21d9-4c6e-93a9-e57192a8dbfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 149,
                "y": 50
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "2662d9ad-0f5a-406b-b0e6-e9256aa2bcbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "91b5c982-da36-45f8-8809-f41d44deb1d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 141,
                "y": 50
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "fe4f8e4c-ff9b-451b-90b5-8946a0e3cd82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 12,
                "y": 50
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "1c6c92da-f540-49c6-acb5-462cad58bce8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 187,
                "y": 26
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "f18a6e01-e768-4374-ad07-a2a3380b73b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 22,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 195,
                "y": 26
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "8df9ef48-50f7-4763-bda0-fff43d6b7572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 22,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 203,
                "y": 26
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "0fbba785-b6c2-483b-9fff-83de6eb91f6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 22,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 210,
                "y": 26
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "d1be9eea-6760-429b-91f7-7b730aa67d60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 22,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 216,
                "y": 26
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "79da17e0-06b2-4716-871e-67e9293eb278",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 22,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 224,
                "y": 26
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "ff653988-0690-48e2-9e83-a8d87e58489c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 232,
                "y": 26
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "b7bd9d24-81ef-4f5f-8da2-33b08d21e27a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 22,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 242,
                "y": 26
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "c559ae45-2b97-4a90-b863-dae406f175ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "2759c0fc-ba8f-460c-864e-b0c3ed777b88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 20,
                "y": 50
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "c354b24e-8102-4fa4-9ce9-e7baa3e7d6df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 121,
                "y": 50
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "504be90d-0f0d-4a19-b288-35b38ed82c8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 30,
                "y": 50
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "12c62ecd-870b-4b35-8e59-a9a61adb16c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 40,
                "y": 50
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "871a9602-0b6e-427d-b85f-56053e9e3f33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 50,
                "y": 50
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "fae81ca2-4950-4bd0-a550-d2dd35f86539",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 60,
                "y": 50
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "0d289bf3-aea5-4d5a-aa83-4e262b3445c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 70,
                "y": 50
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "444b45a5-3a1e-4f7e-9cdc-365fc59b9fd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 80,
                "y": 50
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "d79c52a9-244d-44be-848e-b4bb305101d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 90,
                "y": 50
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "d15a8d63-5fd8-425a-b122-8a2ea096f21b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 22,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 100,
                "y": 50
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "cb1b9df8-67da-4c9a-9aab-27f106a1e0bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 22,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 110,
                "y": 50
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "08b8a6c3-0792-4768-8a99-ad03a28cd9f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 22,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 127,
                "y": 74
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "d137e39a-40fd-4472-9748-92a0bb2053b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 22,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 232,
                "y": 146
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}