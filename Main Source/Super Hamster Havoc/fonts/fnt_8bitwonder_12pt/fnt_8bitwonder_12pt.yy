{
    "id": "419823f0-b1e2-4479-a197-f13984b2ae59",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_8bitwonder_12pt",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "8BIT WONDER",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "820e5dab-b5c0-4f9e-8ccf-fabf7efc906e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 110,
                "y": 74
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ba04db4c-2ad3-4110-8ca6-e250807405b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 16,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 88,
                "y": 92
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "3cfc7db3-3ad6-4b5c-b292-14728e4d3161",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 16,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 37,
                "y": 92
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "4273bae4-d857-4f6b-8a83-b03d461e0e72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "d8d82ee2-1ef4-4f86-b637-46c371a61e2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 130,
                "y": 74
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "cf9c0ff0-baa1-4219-bf33-df439fc98791",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 75,
                "y": 74
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "8f21d1e8-e295-4731-97ad-ac6435770333",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 63,
                "y": 74
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e94786c3-9e2c-427a-b4f2-f33141e78545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 16,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 78,
                "y": 92
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "7310e3f9-1ba2-42f6-8855-595a8acb3721",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 140,
                "y": 74
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "bb5e4ca7-252c-4f19-9b63-9d25cd6f24b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 150,
                "y": 74
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "adf96e1d-d82d-4d5e-96b1-0158f2845926",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 38
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "bfbef156-5c36-47f7-b2f9-8ce9166361c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 160,
                "y": 74
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "1eeb36b1-27bc-4e60-b279-eda375618dbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 16,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 61,
                "y": 92
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "89cfd203-579b-4483-9836-f61f657dea5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 16,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 45,
                "y": 92
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "704ef7e7-848d-47a1-b0e9-04541f1791b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 16,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 73,
                "y": 92
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "cf2a891f-5c76-444c-9972-e57e0a0f53be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 16,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 20,
                "y": 92
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "003fc372-3cac-467c-b631-37f87fbbe8ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 56
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0acad6aa-a8b7-4809-81d9-6b553734587d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 16,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 170,
                "y": 74
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "487d34aa-6c3d-4f7e-8913-8f178109b689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 82,
                "y": 56
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "130f0bce-54e7-4386-b38e-494d3ced8f42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 74
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3ae7174b-2666-404f-bd8f-35d3ae974c56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "6565c4cb-33c6-47a7-a82c-f1d5c7c10da1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 226,
                "y": 56
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "2b6c199f-3aa3-43ed-838b-160aa94a808a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 210,
                "y": 56
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "60ab0bf9-c053-44fa-8aaf-131fe5c60098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 56
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "f06291a9-4785-4664-a86e-88d185a0c954",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 74
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "70ec92d5-876d-4597-9978-60d08c918815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 56
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "000a6bf2-f5a8-46a9-b0b1-e03ceac446ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 16,
                "offset": 4,
                "shift": 10,
                "w": 3,
                "x": 83,
                "y": 92
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "89cbdfcd-0283-4f25-adbf-024aa35533d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 16,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 93,
                "y": 92
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "dc393e19-f349-43eb-9681-0f09bfc0a197",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 180,
                "y": 74
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "019e552f-7201-4d9f-b693-2973b31c763f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 190,
                "y": 74
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "498467fa-3fa2-471b-b264-bdc25059374d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 230,
                "y": 74
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "2ec5a359-02d9-4a4d-bd56-2b456f7fe6f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 210,
                "y": 74
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8aa69cdf-603f-4cc7-9967-33119af45e8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 87,
                "y": 74
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e388a6e7-29e2-4297-b6eb-255a150b7f91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 38
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "398836fc-4998-483a-821d-a69a727cc89e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 82,
                "y": 20
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "aa2e164b-fd36-4902-a28a-505239daa488",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 20
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "c1350b18-1b77-4c9e-ba62-74f6342dd6f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 20
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "83aee024-09b6-499b-b95d-3041b4ed1af3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "296988ad-1d39-405c-a07d-0283c29f395e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0bc29c44-5f05-46e7-abb2-5767eeffec7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "0e61bdcf-8de0-461b-9b73-c20e8e816f57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 20
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e7272cc8-c502-4891-bd1f-d7f3c7528693",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 29,
                "y": 92
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b516de66-72bc-4853-bb96-aa6a42fe2920",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "c0b70b1b-987b-4bf6-9367-bfd9d7075698",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "23dadfed-4e3f-4d60-aad9-8a7e58135186",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "876902b2-19d6-4cae-9b09-7bb73379bdda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 16,
                "offset": 0,
                "shift": 24,
                "w": 22,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d5bb1617-01a6-4ed0-8191-436ac8fa6e27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 20
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "53cdb08f-aa3f-4a23-b91a-235fade82d74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "dcd7a49e-2630-4ee6-8d88-89605a814d1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 20
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "008b37f3-5c77-45ca-91e0-9289fdb428ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 98,
                "y": 20
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ec0d623c-60a6-46eb-bf17-c559e381215d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 114,
                "y": 20
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "be7580a3-a584-4f3a-a5cd-a7da60a493ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 20
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "3cba2ef8-4d5b-4489-987f-2ba50b58a3c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 146,
                "y": 20
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "938f544c-ff90-4d6d-8170-813cb88c11b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 20
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "bf47b502-bdbe-4fd2-9f07-acfa372740a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 20
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "3e50dded-f64c-4a88-b1e9-7be8946d481f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 16,
                "offset": 0,
                "shift": 24,
                "w": 22,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a5c9bf67-d1bd-4187-b385-995cd48799e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 194,
                "y": 20
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "43049e7a-40fd-426c-a66a-229b300534ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 210,
                "y": 20
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "bee40c1d-9b19-4269-8f70-686cd1073d38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 226,
                "y": 20
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "04bb1e71-350d-42aa-927b-d50f965ad0b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 220,
                "y": 74
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "366c480f-793c-4a94-b88a-f6fd9df40cde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 16,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 11,
                "y": 92
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "b4ce535c-eb75-4eb1-9a0a-845d2ff1a5c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 120,
                "y": 74
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "57883592-331b-4504-ac12-148eacbf2914",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 99,
                "y": 74
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "1b226cf1-8af6-46ff-aa2d-4bceac95d1e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 50,
                "y": 74
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "fa1d9397-ee5c-408e-81d2-71e32fd6b5fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 16,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 67,
                "y": 92
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "05240e91-0a81-42f3-8f30-811c31ce15a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 82,
                "y": 38
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "cef22094-34c9-4cde-8c14-986aace50720",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 56
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e1a92c9c-8533-4e53-8647-8ca180dca24b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 38
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ee3631c6-de63-46fe-add6-50145d68d79f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 38
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "11ce1750-937a-43ac-b9aa-61ce22507d6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "85e1c70e-023d-4734-b962-fc198b2c1fcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 38
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "fbaa10c8-7686-412c-b6bd-37154a196d52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "8df123f0-0663-4546-9b1e-a569a60fd91d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 98,
                "y": 38
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e8c06d61-211c-410f-8532-427cbf6dedab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 53,
                "y": 92
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e0aace79-1536-4911-8a1d-4f5da4209f75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 98,
                "y": 56
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "cc27f37c-44d6-4b4c-bca1-67e9bb007906",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 114,
                "y": 56
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "9146d3da-87e5-4112-aa35-dfa01fd2fe00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 56
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "0e47b27a-8806-47ca-901f-3c7fa179799f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 16,
                "offset": 0,
                "shift": 24,
                "w": 22,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "04c31933-d015-4060-9e74-81fd3d8087f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 56
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "12d55b51-4742-4e9a-98a7-d41e7e43f3bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 146,
                "y": 56
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "0d84355e-4622-4085-9a35-c590e64b71bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 56
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d3820023-2029-439e-a3f4-adf5d4e96a5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 18,
                "y": 56
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "afdacabc-b061-4b24-a631-646d8be09098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b5fc70c0-65ee-4bad-95de-ecb70106fb4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 226,
                "y": 38
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "5fef26c8-10a9-4863-970a-c8af8e58551e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 210,
                "y": 38
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "c8f6572c-0b99-4356-9c3b-0c85645f7158",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 178,
                "y": 38
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "38a27e8a-c359-4775-8e25-cdbe61eff639",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 162,
                "y": 38
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "5b42b87f-5bb5-496a-ac21-b5c5f7cdf8ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 16,
                "offset": 0,
                "shift": 24,
                "w": 22,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "6b769ed6-d8b7-4ca8-b798-2f338cdf5a1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 146,
                "y": 38
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c9b42207-3e29-4c6a-a177-26c81b83987b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 38
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "98ff0444-c66f-4051-b291-ef2d7e975205",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 16,
                "offset": 0,
                "shift": 16,
                "w": 14,
                "x": 114,
                "y": 38
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "ad7ad1d2-073a-4ef7-a57b-33ea1007efa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 16,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 2,
                "y": 92
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ada0dd0b-7e60-41cf-9af4-1a67dcfa7668",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 16,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 98,
                "y": 92
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "87a6b556-d0e0-4dec-b2b3-d7a40ecfc144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 16,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 240,
                "y": 74
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d8a8bbb7-871a-4fd1-894d-1257f20a4a2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 16,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 200,
                "y": 74
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Nominal",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}