{
    "id": "4f74ae85-a0f4-496f-92d9-58f54ed65f2b",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_arcadepix_14pt",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arcadepix",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "55ab44ab-1a69-457f-b9df-18b59866cfff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 146,
                "y": 44
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ccaca454-d2d7-4f9e-85e1-06df48f15481",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 200,
                "y": 86
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "ac338710-4b00-4a13-ae26-f57b16d11a22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2f33a264-8f15-4ba9-b157-e4aacd6282c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 92,
                "y": 86
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "acde401f-fa8c-4e37-9870-9c01d5f33355",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 128,
                "y": 86
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "f86a24bf-b10b-497e-81c4-546c4378dacc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9ae8537c-0f18-4f0f-864f-a9acefe2272a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 65
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "a3cb3e69-d319-4102-bbe6-47cf16d062df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 8,
                "y": 107
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "049e5a08-7490-4c61-ad41-986b7855f09f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 232,
                "y": 86
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4af98bb4-a1a0-46b9-95e9-c1b945c8a31f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 224,
                "y": 86
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "45718f58-22dd-4132-9735-92de734dfba0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 116,
                "y": 86
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "de137c96-8cd3-4892-a432-8c9c27bf60c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 140,
                "y": 86
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "4a9b6af2-d695-4b94-8c26-0aaf0c98ee32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 208,
                "y": 86
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "82aa7b70-fb27-4df2-b569-0390800d3c67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 104,
                "y": 86
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "bfdc3ebe-0fbf-4c7b-9fe9-09d312e69538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 248,
                "y": 86
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "8ceea802-977e-42b4-b31a-f766495d3eed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 43,
                "y": 86
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "07a43184-669b-4877-bec6-62adc217dbd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 44
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "2902a4ec-ecdd-483e-8785-9a44b89ce3c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 170,
                "y": 65
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "af4ffdeb-a50c-421d-b6d9-8d5b02445e33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 226,
                "y": 23
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "61a4ee9d-29db-4cbf-aa36-07602987ea06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 34,
                "y": 44
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "003dd62c-2cb4-4a48-91ab-67bb92ab3ebb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 130,
                "y": 44
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "0826a686-e348-4952-9c48-d4ffb1b246fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 114,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "eb56833f-9e89-4717-a228-4e50ff331ae5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 98,
                "y": 44
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "10b5757f-259d-491f-9a67-35fa37799f71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 82,
                "y": 44
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4579a2a9-4cba-4dd9-9e6c-a2728376eae7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 66,
                "y": 44
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8e621630-6a38-416d-abfb-5683c0c02f27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 50,
                "y": 44
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "addb8fe9-debf-4a4c-9a85-e3b30cd7c419",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 2,
                "y": 107
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "2c66751c-4f6b-4cad-84d5-a81e7d2192ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 192,
                "y": 86
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "dfd66feb-e720-45df-97c3-a636c1b4b78e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 152,
                "y": 86
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "dfb4a0ed-55d9-4065-8e76-f88bb57cc5f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 80,
                "y": 86
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "cabb6bae-f526-4c84-a5f9-91eec0e9e0b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 172,
                "y": 86
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "c6842cd3-177c-4342-ab7b-913cdc8ee247",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 194,
                "y": 23
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "bca5643f-a05e-4082-8b58-6df38f30b171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c18ffb82-e54d-455e-8d61-14476808b470",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "73f03102-d3c3-4a70-95ab-c9ca19ba2ce5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fb26c4a7-6d48-4b17-8a1d-3861f6e3e61b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "21774dae-258d-4890-a3fa-124b6021cfd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "029a586f-c0c5-4026-956f-0ee0e501b58b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "00462799-4232-4d33-aa76-96f51d3fc4d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "a7a7491f-250f-4332-9768-d2ded2c3bd98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "019626cd-2d78-4c5f-8bd3-2edba266c2f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "13051fd3-ac60-423c-9932-24d5b63c8ff9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 156,
                "y": 65
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "da4489ed-fe9e-40c7-a27f-9e413ee634ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b12f6f5c-b4da-4106-b577-71de8da031be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 179,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "34f035f6-54df-4069-9383-3007302fdf15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "0d5aba63-cb6e-40fa-9c8d-f4afe54edfb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "144d5db0-08c9-4b74-9f4f-f79d730c419d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d2daf78d-5d75-4805-be9a-6070999f8321",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b52a3372-7167-4ebc-9970-16a380d28b8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 178,
                "y": 23
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d6066898-14fa-4d23-b036-8c510ff65c51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "53d7ac04-3a8b-45c8-978a-ac0b53766b1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 23
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "fad2410e-2e92-4f76-9fca-8b8c136efcfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 34,
                "y": 23
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "ba552e37-438a-4407-9035-0b28f6c3aacd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 184,
                "y": 65
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "79ad7997-5df2-4604-a137-46c1675c7721",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 50,
                "y": 23
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "00561b1d-11f4-45a1-b9dc-3a8d285b7984",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 66,
                "y": 23
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "80c467d7-e714-4ed3-a183-8637f5248615",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 82,
                "y": 23
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "d8b45bc0-b453-442c-8da7-708d82bbf8b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 98,
                "y": 23
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "06482f03-f5d8-41c9-901f-73ea3dd70ad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 65
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "92f3ebbe-04f3-4efc-ac4f-e9e385f6e38a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 114,
                "y": 23
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "16ccd33a-e70a-4339-9e43-1246336749f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 16,
                "y": 107
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d1845332-d164-4edc-b77b-64a83782073d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 30,
                "y": 86
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "63dbf1e9-88d7-4d4c-be07-de6da1a504f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 22,
                "y": 107
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "9b31d935-af4c-4311-8b98-6bce96d41dc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 189,
                "y": 44
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "44f0f2b6-aa04-4102-8676-c2ba017e3828",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 130,
                "y": 23
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "08f8ec70-9a83-4b62-a842-223b17ebfac4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 182,
                "y": 86
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "63e411d5-b1a6-47dd-b5bc-61e47e8171d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 58,
                "y": 65
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "7ea12cea-864d-452a-a8a0-a0f05d504835",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 203,
                "y": 44
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "118fa44b-ac4b-4ee8-9a81-d8d848186895",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 65
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "19d38780-3e82-40b5-a86e-53683a69522c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "21091a7d-d3ab-4e36-b8dc-61c288b9653d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 198,
                "y": 65
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "05c5fa78-8138-4ef0-8840-6b9fa608e305",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 68,
                "y": 86
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "461daecd-c562-4d1b-b5c6-fc910c4e9f3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 175,
                "y": 44
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "44ef683a-3223-4ffb-8887-b17052175f69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 217,
                "y": 44
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "332accb4-3aad-4935-95f8-a21f2230ed2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 240,
                "y": 86
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "57bb8756-6845-45b0-b2f3-2f6ded78de27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 162,
                "y": 86
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "f3ea67ba-7838-4663-bbfb-b8c1949a54f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 231,
                "y": 44
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d62444c4-a23b-4b18-bcfc-3a05ab469d03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 216,
                "y": 86
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "654a696b-101c-47c8-8c8f-f1665cd39de8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 146,
                "y": 23
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "103b928b-e8e4-4ab9-b8c9-0b657115ddb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 161,
                "y": 44
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "a7aa1083-3f91-4445-a765-f0d30a2983c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 72,
                "y": 65
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "ca2b0d1f-54bb-4f15-9bb6-961035f3d32b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 100,
                "y": 65
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "dadbd984-ff4c-4f11-b38d-c45eb9e937e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 86
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ad536a92-c2d6-4293-b2d7-a6ae99d06fe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 86,
                "y": 65
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "f5d85698-b8ea-4c9b-83b8-a7dd71337622",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 240,
                "y": 65
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "5fbc1dfa-db6a-4225-92b3-73d7e5e96553",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 56,
                "y": 86
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "eb2627d4-8ce8-421c-bf84-5c1bfc66c9a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 226,
                "y": 65
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a35507ab-07a7-403c-bea2-b80fde03d2f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 212,
                "y": 65
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "a0d2fa03-a8b9-43ed-bdf3-38a95e236805",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 162,
                "y": 23
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4eca4285-2cd7-44cd-9a81-42c4ef72bc87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 142,
                "y": 65
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f965b19c-ee9d-4c02-bba9-802ce49486e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 128,
                "y": 65
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "35034f05-3af7-4777-bf8b-b018a1928fee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 114,
                "y": 65
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b6ed7f91-934a-4d27-aece-21f10abf00fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 18,
                "y": 107
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ebdd5417-3f33-4fc0-ae8a-44a52eeeaeb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 20,
                "y": 107
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "7cacabe4-2447-44f3-a9b1-393857611fef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 14,
                "y": 107
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "ec3baef8-9a16-44e1-aaaf-342a5e620a11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 210,
                "y": 23
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "f379ada1-7f85-4c77-8abe-01175b1b1860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 84
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}