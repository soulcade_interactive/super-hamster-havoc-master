{
    "id": "a8328d4f-bd62-42fd-bd04-eb14e9b83ecd",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_arcadepix_16pt",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arcadepix",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "1cbdf94f-5b00-45b5-9695-397f18f91a95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "f8586ed3-bc8b-4e05-8452-c9d66d49373f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 44,
                "y": 117
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "48afdd5b-5d88-4dd3-9369-5a3b48249600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 107,
                "y": 71
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "806adfb7-f538-432b-84f4-07ff1dafba01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 146,
                "y": 94
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "89865266-9652-498d-a601-6aa06b08392d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 198,
                "y": 94
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "76c962bb-ae88-471d-8d37-01731b3901da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 36,
                "y": 48
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5a45ddbd-97c1-428b-afa2-dd2bdca575d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 92,
                "y": 71
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "238de47d-1718-4274-88e8-d5923e8bc9bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 112,
                "y": 117
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5eb489d0-8f79-4468-a554-d13f5979d4f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 71,
                "y": 117
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "d2d616fa-858f-4964-924e-379e40b23a4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 62,
                "y": 117
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "3f743c47-1a99-47a0-971e-62a1c84eec90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 185,
                "y": 94
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "cbfb5d7f-19f0-4462-b885-78edf83f06d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 211,
                "y": 94
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "cf5f7d43-23a4-4250-a2eb-8a953232ad7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 89,
                "y": 117
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "bbf99987-e53a-495d-bed2-59ac1a0499d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 172,
                "y": 94
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "eed248f2-ffaa-47d1-9416-00227e59d47e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 98,
                "y": 117
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "ab2a78b4-0992-4d45-b2da-d9676898666b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 159,
                "y": 94
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "49eb53f0-a09e-4cac-b1ad-18287af154e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 53,
                "y": 48
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "50f03717-f87b-47b6-8979-8b2d664290bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 94
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "5b8cf885-7aa8-4138-a9e0-2ff04cd2fa22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 138,
                "y": 48
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "0cba4d93-9319-4647-9c62-9be1b00865f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 70,
                "y": 48
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "cac5cdcf-d1dd-4963-b99d-83c1f5073f5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 87,
                "y": 48
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "3f0ea406-d26e-4ad4-aef0-da52b17176d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 104,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "be8798b2-b1f7-4070-9b5d-25cba80ff0b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 121,
                "y": 48
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e823bb97-260d-4389-9967-ac259277efde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 189,
                "y": 48
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4ccb847c-8978-465d-8503-fa655dbc0c92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 155,
                "y": 48
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "9e7c9587-6462-4515-82bc-8597b98f6d72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 172,
                "y": 48
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "06c39756-0722-428d-b182-750bb2faf07e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 105,
                "y": 117
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e0b31e69-bda6-4295-813b-bc7e0c2de26e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 35,
                "y": 117
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "c52ea80c-32de-4432-890a-98712e459193",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 117
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5f36760a-b737-4f5d-b9f3-bef507a6e383",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 133,
                "y": 94
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "8e9eb746-5c83-4a3d-b966-eb2675ed409f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 237,
                "y": 94
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "197518e1-09cf-4ed8-b8a6-e0aef9d8a3b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 223,
                "y": 25
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "fb34fce9-7e40-479c-a299-cb452ca8fe12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "8d284820-b166-4a83-9ffb-25f94c380931",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "1589cc21-216a-4ab3-b863-1551964dbb2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "48264316-4429-43bc-8b73-ed61b72b06fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "3d676e8b-6f1b-4e52-b208-aa97c14ccf4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c370307e-3274-44c8-9df0-6869c6e31ad0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c36ff1f2-69cb-43d4-85ca-a1431a3a2edf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "accb7bf8-333c-4db2-9260-447dd90272ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "eb8795ec-5ca5-494d-824c-9301d0d6fbf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 158,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "6a305216-a23a-4a2d-88b7-01cae4851e11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 17,
                "y": 94
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "515d0b49-550a-4987-9ac9-39884b0b838e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1621da2c-4780-4d08-8b26-7d1329223f61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "9d90122b-b310-4a75-b414-e9ba28df71d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "24de30b6-537a-4ef4-be55-850b35f9e81e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7fea53eb-8225-4365-8ac9-53907a5e141d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "483c568e-ac9d-441a-b0de-55a040a88c58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ddedd038-5efc-4ddd-abe7-302cddce65d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 206,
                "y": 25
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b60e5d5f-99b3-436f-9cfb-fc0c824a933f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 19,
                "y": 25
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "01ec3970-3019-4db1-94b3-f606e218be30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 36,
                "y": 25
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "cebfa85a-d79f-4042-a4a0-d606ce67a00e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 53,
                "y": 25
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e82683ff-7792-467d-a933-9945f31b202f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 122,
                "y": 71
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "4e2f39e3-93d7-45bb-b986-3c8127b97687",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 70,
                "y": 25
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a2aab60f-a740-464b-bb6a-83853b4d4335",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 87,
                "y": 25
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "7c823dd7-2bc2-4435-9b0a-27e8d56f2634",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 104,
                "y": 25
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "303e2bfa-c8ed-4e49-b679-1362030c1149",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 121,
                "y": 25
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "86f3bb69-9f50-4d3d-976c-c349c00047a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 62,
                "y": 71
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "cf409434-d45a-4445-b39f-4aae0db7e079",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 138,
                "y": 25
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6123c48f-1e14-43ac-9f70-dcc189dc7a55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 121,
                "y": 117
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "5c71aa3a-5f35-4ff1-a892-29c9100b9201",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 224,
                "y": 94
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "01984ec6-a1d3-4d52-a462-b8057f2691ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 127,
                "y": 117
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "c76e5afa-d13b-403a-8f5f-5100b298d865",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 236,
                "y": 48
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ddacf0b4-41b5-40a2-bfec-1a28143eb82f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 155,
                "y": 25
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "870b7542-90ab-4db1-82de-30d0f2d41e3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 24,
                "y": 117
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "919b97ed-abab-4cf9-ad82-c1bf6b59326e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 77,
                "y": 71
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "04aad5d6-c268-47f5-8569-7a231f954fe2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 182,
                "y": 71
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "b76467bc-083f-45f4-ab26-cec72e150370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 92,
                "y": 94
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "fb27f7a0-c33d-43ef-9b7c-b2c5eff25a59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 17,
                "y": 71
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "873c03f5-779a-4b45-9848-b5b5ec401cd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 206,
                "y": 48
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "6f971ab9-eccd-4110-b9d6-d153e89f2258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 120,
                "y": 94
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9806d3d6-978c-43b0-b621-7abb97af71c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "abd62a17-b240-47e0-be66-a32e62dd93dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 32,
                "y": 71
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "df11c5e5-7f2d-4de6-884b-936320b2dcd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 53,
                "y": 117
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "6a886670-ab27-49b1-9525-487d40099c21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 117
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ed26d7aa-9246-419e-b272-aa5875fe6251",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 47,
                "y": 71
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "749927c4-c0b4-4357-b0c9-8948e88da2ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 80,
                "y": 117
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "ae8a92e0-57e5-4fa8-82c5-a09e6ff089fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 172,
                "y": 25
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "d3a59c49-d3e4-42a5-876f-4a7968f0c28e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 221,
                "y": 48
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3b843e87-1443-4cb4-a9be-e07490917441",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 77,
                "y": 94
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "296bf2a3-74f0-447d-be91-3ec59bf6f234",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 152,
                "y": 71
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6f3e0be1-fa83-4b83-8aaf-5d215af2e7c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 167,
                "y": 71
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f89100de-9dc2-4d41-bc8d-5b50ef72673e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 137,
                "y": 71
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c2ff80e9-4721-44b6-97e5-24459d5db7b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 197,
                "y": 71
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "bed7b05f-898f-4be5-afd3-9697a583181b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 107,
                "y": 94
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "11ce1e47-b651-4382-ac0f-1566864b41fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 212,
                "y": 71
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "8d9dcf6e-14ae-4c62-9ed1-63b22dee001d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 227,
                "y": 71
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "20e00a08-98e3-479d-936c-11ec8dd36c83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 189,
                "y": 25
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "bda199b9-73f1-4631-b6ae-fb1df7f50994",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 32,
                "y": 94
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "39358a9d-0d04-4241-a849-bd431e4e311f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 47,
                "y": 94
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2e06bda8-8f99-4f47-ac74-f24ea211ef00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 62,
                "y": 94
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "12f39d93-9363-43ca-b47a-1c3d25f90893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 123,
                "y": 117
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "2ce21f92-80fc-469c-becc-4b4f0aae5b9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 125,
                "y": 117
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "1c3ff4f6-9476-4ba3-8631-de3d7381bb26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 119,
                "y": 117
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c559b0fa-4de5-4a8a-aa3f-61c5d1c28a1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 19,
                "y": 48
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "2afc9437-df90-47e6-9731-77b4c59a9e8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 84
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}