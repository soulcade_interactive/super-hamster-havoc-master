{
    "id": "20bed115-fbdd-4d85-aa21-6c6903908b24",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_8bitwonder_16pt",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "8BIT WONDER",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "89525775-7082-4c8c-ba82-eaa9482f1d93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 123,
                "y": 117
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "9a88d856-fba7-4a49-ac21-fb16a8460012",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 166,
                "y": 140
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "774c6d95-fb27-4bcb-9651-32e04af34e82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 3,
                "shift": 13,
                "w": 8,
                "x": 125,
                "y": 140
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "6cf8cedf-e41d-45e2-8e70-a08a76c5f005",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e94740fd-43af-4ac8-a63f-bf875339d897",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 15,
                "y": 140
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "7d1121ad-bfbd-42c2-b166-443291b2837a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 93,
                "y": 117
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "f4c388d3-4734-46ec-9412-348e77568e62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 78,
                "y": 117
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "460262f6-d87d-4ea0-a9ec-2f88c6c6a778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 161,
                "y": 140
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "3438a790-2fc9-4547-a8e8-5f2246dcaa48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 149,
                "y": 117
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "a06fc3cf-1026-4776-9900-d191c63d8cbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 162,
                "y": 117
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "c5e08b87-010f-48ec-a720-56dc83752591",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 82,
                "y": 71
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "74261c8a-2773-4549-9ada-1fd824588934",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 175,
                "y": 117
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "0f791ba1-f149-4ce5-9d0d-424ca5ddc146",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 5,
                "shift": 13,
                "w": 5,
                "x": 142,
                "y": 140
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ee341fea-444e-4c74-b1a5-a0cb998de9f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 3,
                "shift": 13,
                "w": 8,
                "x": 95,
                "y": 140
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0fff5355-4226-41d0-a5fa-a095494d53e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 5,
                "shift": 13,
                "w": 4,
                "x": 155,
                "y": 140
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "38027e0c-f733-4bb2-9d8c-10ca949eaa05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 84,
                "y": 140
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "f4914ca6-73b5-44ab-9841-b63acadd4b8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "ffaa62da-ebac-4420-988c-8cb073e7f543",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": -2,
                "shift": 11,
                "w": 10,
                "x": 27,
                "y": 140
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "c6b991eb-5f7d-45b6-be88-5197a3bef545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 22,
                "y": 94
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b8a8f596-6bb6-4613-8b20-2c7324fd966c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 22,
                "y": 117
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "32ae67e2-0c50-4400-b968-e9aaacf445ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 42,
                "y": 94
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "912362a7-95e4-4452-b466-43c4c9363920",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 62,
                "y": 94
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6ed6f3fc-8c61-4517-bcf5-6e20f1ecf376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 82,
                "y": 94
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "da2271e9-c074-48ba-8186-df663719f6ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 102,
                "y": 94
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2960c4ea-3bf7-488d-b71d-8c0b9cc5884e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 122,
                "y": 94
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "0d1c8521-6d68-4aec-89cd-57c5980553c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 142,
                "y": 94
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a16c2223-a907-4842-8f50-e008f54a155f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 176,
                "y": 140
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "abf311f4-0856-4262-a9c6-3cac44b15fc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 4,
                "shift": 13,
                "w": 4,
                "x": 149,
                "y": 140
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "9b89fcd6-bd8f-432c-8649-b37c94eaa8d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 188,
                "y": 117
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "60a5da92-37c5-4240-9952-8c4df00e23bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 201,
                "y": 117
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3967352b-e0c8-415a-a804-2cfdfb4d0062",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 214,
                "y": 117
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "2a42fe92-8294-4036-b3b2-a1b82b95e0f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 39,
                "y": 140
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "35f65e3c-f66d-4bca-ab4b-fe2544cfa773",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 108,
                "y": 117
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "21677cb8-1b91-4fb1-913b-c727c49a00c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 202,
                "y": 71
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "3e37210a-f3a9-402f-b539-bf8222c831d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 202,
                "y": 48
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "5f55eae6-2e25-42ed-8184-fb0fdbc47e29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 162,
                "y": 25
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "ecfc8846-6c8e-4ca5-9bd9-e0c1766f9597",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 82,
                "y": 25
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d9934fe6-7316-4c27-98d9-1131e0ced03a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 62,
                "y": 25
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "38576776-daf5-4749-9787-d98a3cff41de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 42,
                "y": 25
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "3f900d25-8465-4ee7-b23e-a132327ca1c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 22,
                "y": 25
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "afdd4a0d-64c1-494c-a461-c0de131c04a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "bba554b3-ffbb-4bf6-b5bb-69d09d527c64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 115,
                "y": 140
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "c7d98bc8-99d0-4e8e-8c3f-6df5c2f00f2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "cbb72771-11f1-4a98-81bc-f468d892ac1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "33ae0d19-1555-47c0-b41b-820c746ad610",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "34740f28-760e-4ff5-89bd-965f35276fc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 0,
                "shift": 32,
                "w": 29,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "68e55b4a-010f-40a1-b131-cb7a712f75ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "3a97ae12-af1b-4f52-8c5e-e18ef2942c76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d080acd4-8e43-4dda-a32a-dba5073f269f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 182,
                "y": 48
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e68f3efe-717f-49bd-8412-3c26135d7f5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 142,
                "y": 25
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "25ff0ec0-7407-4752-bdd5-6a393f769a3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 182,
                "y": 25
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "1af8a60c-0cc2-4693-a77c-6f2c6be105ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 202,
                "y": 25
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "708f975b-8e32-44b0-be0b-3d66f7c742e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 222,
                "y": 25
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "4faba9d6-41fc-4f29-becd-20162d411d5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "17707849-b70b-40fd-8640-d62474e2489e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 22,
                "y": 48
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "328e89ec-297e-49f2-a57c-4a0a9ca17e0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 0,
                "shift": 32,
                "w": 29,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "cef267cb-d5e0-405b-859a-7b77b48101f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 42,
                "y": 48
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "73b8fa62-a2a7-4ce8-bfad-036d99fc42fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 62,
                "y": 48
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "47d9e0ae-ef90-4816-b966-43bc199fd6f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 82,
                "y": 48
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "cc5258f8-2130-4bc5-b8df-299dcee6dac5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 240,
                "y": 117
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "4b456d3e-6096-41d3-95f4-ce427f19261f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 51,
                "y": 140
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "99e86271-3b5b-4728-9499-7f2ef8420607",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 140
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "2d1b1344-6783-4e17-b93d-0e3a0d40a183",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 136,
                "y": 117
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "00841270-d026-4431-bbf7-25c7aef1385a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 62,
                "y": 117
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5c7f840c-3a3b-4392-a26c-f803b52df91d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 4,
                "shift": 13,
                "w": 5,
                "x": 135,
                "y": 140
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "3ffd416b-2759-4afd-aebb-8b00d68f4c0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 222,
                "y": 48
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "2dd2518d-494b-45e7-81fc-16860a616614",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 162,
                "y": 48
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "c22850c5-202f-4d9b-b70d-ff6250f9c776",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 142,
                "y": 48
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "0b50ef7e-5365-45c9-97e1-9dcbc06507a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 122,
                "y": 48
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "fab14ce0-b685-4f9d-8faf-b77f275cf02c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 102,
                "y": 48
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "173b7831-eda7-464b-ba7f-279ac243494f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 122,
                "y": 25
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "22e03d56-2039-4856-8836-cfba062a99f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 102,
                "y": 25
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c3b008e2-ec77-4b75-917f-d6dce4c4a405",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 2,
                "y": 117
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "03b1a402-c830-46c8-b5a1-05c5a6d05a04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 105,
                "y": 140
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "5ab01267-805b-4885-a11b-1df312c523fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 222,
                "y": 94
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "7952c380-7973-4a96-9ed8-9aa84cd66d66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 202,
                "y": 94
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "181afde3-85dd-4491-94ce-315d34581428",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 182,
                "y": 94
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "47b24d7f-e8e7-4ae4-b952-099b966a66fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 0,
                "shift": 32,
                "w": 29,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3d54ae17-fa82-4512-8d5c-b86c9aada5da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 162,
                "y": 94
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ff0f94b2-c391-43ff-86c4-f187eb6f78b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 222,
                "y": 71
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "2ede6ccc-7dfb-412e-a228-44240f7e2c81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 182,
                "y": 71
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "cddbca57-84ea-4c04-8425-a64ea1bfd3a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 162,
                "y": 71
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "bdf80e44-e8f1-42c1-8c0f-f4ebf1a2129c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 142,
                "y": 71
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "3440904f-0c72-4190-8a12-f551705b7a7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 122,
                "y": 71
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b9692833-3fd1-46fc-8b2c-27ebdae449dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 102,
                "y": 71
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "786c77a4-075c-438d-9bdb-708053beaef4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 62,
                "y": 71
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "54dd7031-e867-48a6-a5d8-fc5d20ab94a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 42,
                "y": 71
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "5f67c5ae-32da-4b81-a7f9-c100f965ca13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 0,
                "shift": 32,
                "w": 29,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4a01602d-872a-41f4-98d4-804e50d6edb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 42,
                "y": 117
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "fea4f73a-f205-41a8-b025-5f255cc300c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 2,
                "y": 94
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a0ed5178-37fd-4efd-9559-c7058c7a558e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 22,
                "y": 71
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "a332adfc-d157-47db-90a2-01f553154b92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 73,
                "y": 140
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "cfc751a0-d86a-4218-ae04-d5e66e50f4c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 6,
                "shift": 13,
                "w": 3,
                "x": 171,
                "y": 140
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "05bb6edd-2b30-48f0-806a-0c9dc7ef3f21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 62,
                "y": 140
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "31a3bff7-4ed4-4988-9865-4631219c3d76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 227,
                "y": 117
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 16,
    "styleName": "Nominal",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}