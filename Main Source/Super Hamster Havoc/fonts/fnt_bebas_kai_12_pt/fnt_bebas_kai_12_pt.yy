{
    "id": "70693131-c1d2-4306-adab-aa68a630d243",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_bebas_kai_12_pt",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bebas Kai",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f2d370c1-0fca-4399-b72c-e80b316df06e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "0e6f440a-9bf6-471f-9e36-d723cd27767f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 18,
                "y": 86
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "4308a4c9-7ffd-45f3-8df9-225efcad720b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 23,
                "y": 86
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "489da9a7-9938-42ee-aab6-0d6d1b52a7d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 31,
                "y": 86
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "6e4d56d1-030b-43b0-9d74-cc73c8a763ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 41,
                "y": 86
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c8b09a5e-7504-4533-8c06-3d0577d46ad8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 50,
                "y": 86
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "58088b2b-63cf-4676-ad04-84d313bbe04f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 61,
                "y": 86
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "590fd309-d58e-4090-9891-39b57832d654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 71,
                "y": 86
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "92ac25b4-3b65-4860-afd4-5358a2dfcdbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 76,
                "y": 86
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "6f8213ef-3118-4cc5-a5b1-6cff17b745df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 83,
                "y": 86
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "31c5b6d9-3b4b-4e58-93be-e0b6596147ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 99,
                "y": 86
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "baedc6b9-6ebb-457d-b3ea-bce5fa4e2a0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 173,
                "y": 86
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "3ff2431b-80d7-4a27-9bc2-734aa6a9b0b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 107,
                "y": 86
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "f09fd43c-73c9-4182-8994-8522bf86b3cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 112,
                "y": 86
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2c40425a-93d2-4932-b1e0-0b07f9886e26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 119,
                "y": 86
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "466b5e08-0739-4449-94ff-88a483045f07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 124,
                "y": 86
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "cd01d8ba-eedd-41db-b31b-8c8e087b7c63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 132,
                "y": 86
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "dff0e31f-4ff0-44c2-833f-07460df4574b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 141,
                "y": 86
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "5ce9aaaa-02da-47f5-b029-585d19e86fdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 148,
                "y": 86
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "bb42b9c6-67d0-4444-9bd1-02972c8e68c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 156,
                "y": 86
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "194a15d1-85dd-4d6b-a049-5455fb2e9c23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 164,
                "y": 86
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b8e1e97e-9460-40d1-afe9-82dc5a836a70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 86
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1bc5c967-0e61-40a4-ae4f-9cf783b1ef25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 90,
                "y": 86
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "2a8c9c26-1295-4d60-b1b7-eb50845d50f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2159152f-7718-4a5b-a543-c4674bafed11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 142,
                "y": 65
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "c3b5f898-c5a8-4164-a9f3-982bdd515d2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 65,
                "y": 65
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "d3328a7f-1c07-4033-96e4-2e4c8800cf5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 74,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "6725e0b3-b416-489f-91bb-0f13168cf43f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 79,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a5a6902f-16eb-48c1-92a8-dcc7e7abe69c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 84,
                "y": 65
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "7664ae15-c9c5-45c8-ae25-d34f1f354a26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 93,
                "y": 65
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "abe3fe7b-9b7e-4de1-9362-3ce889c858dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 102,
                "y": 65
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "94a35aff-ef75-49f8-b570-3a3ac2cfab79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 111,
                "y": 65
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "06924b9c-8322-4689-8194-0d60bc947eb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 119,
                "y": 65
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "6343f8de-e17d-4f43-80d9-e91ca111a706",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 133,
                "y": 65
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "70c7633e-fca2-473a-b4aa-3abd0195bd4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 151,
                "y": 65
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "400ff7b5-fd5d-4a4f-970e-dd367a505d86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 231,
                "y": 65
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "88b0f15e-16e1-4d26-9c4d-7d466e1c2a38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 160,
                "y": 65
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "f77b450d-1cbc-4627-8e70-d3733793d43c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 169,
                "y": 65
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "63afbf36-4e4b-4b9c-a1dc-e49b2f3f963e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 177,
                "y": 65
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1e7d27c4-51bc-4c87-9866-35bd5a7b4c4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 185,
                "y": 65
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "c755a82d-02ea-435f-9f70-57af67020409",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 194,
                "y": 65
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f6b9a9f2-74e3-410e-9d97-8e9626b6d08f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 203,
                "y": 65
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "0c115f2c-bffc-4220-af27-14496e66428a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 208,
                "y": 65
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "220f0b2d-007d-4f0a-bd4c-686085164822",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 214,
                "y": 65
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "af534801-25b8-4fb0-8b6b-aefc9b5f4071",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 223,
                "y": 65
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6d789c14-707b-4a60-9b90-e1946f339a77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 239,
                "y": 65
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "2c67e297-2fa1-4ab5-bfd3-570023d78973",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 191,
                "y": 86
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "061577ad-8210-4513-9487-dc8da068dac5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 140,
                "y": 107
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "120dfbe3-106b-4658-837d-63f729b5fec3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 200,
                "y": 86
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "222b6e69-5c56-4237-8887-93c157da850a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 166,
                "y": 107
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "7f06af97-84d8-4805-9c4d-5702885c7cdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 175,
                "y": 107
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8a9939dd-edf3-4a74-9f98-32e09c89ea78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 184,
                "y": 107
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "0ff23050-78f2-44ef-bbe8-865e2a038469",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 192,
                "y": 107
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c1133ccb-a333-410a-bcfc-cc769a2f265b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 200,
                "y": 107
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "6ca98c71-02d4-40c2-921f-632419378efb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 209,
                "y": 107
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1b3eba37-12cf-4e7e-bd5c-d7ceeb0eb53a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 218,
                "y": 107
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "997287fd-7c0e-4508-809f-71938bdcbc93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 230,
                "y": 107
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "595937cf-0710-4346-9953-11505040f0b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 239,
                "y": 107
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "78f27e14-d9be-4dc9-8511-eb85441d36c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 128
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "9bcc28cc-23ac-4a56-ab1c-6824c1edb35a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 91,
                "y": 128
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "eaca2c61-9f88-49f3-9e9d-97add1c76f31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 128
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "2ef4f9cc-326b-4263-8484-5d4ae6112cc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 26,
                "y": 128
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e0ddb253-6d4d-4bf2-8653-bce60c4a07dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 32,
                "y": 128
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "3e98596c-2044-4b18-a169-322e6df8011d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 41,
                "y": 128
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "cc1bf305-cf7a-4503-b06c-04cf0588f8b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 51,
                "y": 128
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "915f81c4-5fd0-46b6-a7a7-87be3abf66bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 56,
                "y": 128
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "cefa407a-338d-4563-a27e-daa7363827d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 65,
                "y": 128
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "1ade6d0c-ba80-40a8-a4a5-7965fc97e0dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 74,
                "y": 128
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "a7642335-794a-4697-bd27-9838b9f3d2cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 82,
                "y": 128
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ad00d1d8-bf6d-4ba0-9940-ac538ce0c62e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 158,
                "y": 107
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "64b411f1-4015-4f7b-bb65-743412c18165",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 128
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "30d85d7b-db0d-480c-a7cb-d8cb889995bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 149,
                "y": 107
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "06ad3218-b5a8-47a9-aeb4-cdd41ccd85ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 38,
                "y": 107
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a065fb45-5e45-4335-8c72-3b34df1c41c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 209,
                "y": 86
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "601a8084-26d3-4389-af99-ee7b10090fc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 214,
                "y": 86
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5077e6d3-2a13-43a0-a0f7-29638185f5e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 220,
                "y": 86
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "86687ed7-d6ca-4f28-bc08-c46f5fe72a37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 229,
                "y": 86
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "dd431bc0-09f9-4e57-a49d-ec350e06028e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 237,
                "y": 86
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "5fdaa6e4-e8f4-485f-b4b2-533a3cda6b77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 107
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ad76812c-ab6d-4c01-a840-86f791284e5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 107
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "8bf6e399-efdb-4dc9-81ef-7e34edde6302",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 107
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "dd818e01-180e-4fe3-b4e9-27799f8438cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 107
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "81cfd270-f516-4061-9cf4-f8ff8e27a425",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 47,
                "y": 107
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "3af8af69-6120-4ecb-8131-54548f1aab6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 132,
                "y": 107
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8e7e1806-57de-4e10-891f-76b52d62f758",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 56,
                "y": 107
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "12b1a136-3f23-4011-8926-e3e290ec1eed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 64,
                "y": 107
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c4427e49-9c32-4f09-9fc9-be72629e4afa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 73,
                "y": 107
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d669d505-7c00-457e-9b01-eb9f65ceed93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 82,
                "y": 107
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "467912b2-d92c-441c-a89c-05b5900be71e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 94,
                "y": 107
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "ed418cb0-042f-4ba1-91f8-d1ee51d6a012",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 103,
                "y": 107
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d7338371-70f0-4aa6-9126-9a99735178da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 113,
                "y": 107
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "4fe20348-6df7-4561-99a5-c45884334757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 121,
                "y": 107
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "05e7e063-ec2e-41f8-a817-60a733ab2280",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 128,
                "y": 107
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "dc2950aa-bbf3-410b-9bc5-3ece37d222ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 58,
                "y": 65
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a87fd0f9-3334-472e-bf4a-127d0614afb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 182,
                "y": 86
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "7217603e-3c53-4e19-a0d4-8606e0de10bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 0,
                "x": 56,
                "y": 65
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "7112d25c-456a-4474-b091-04c2efb296a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 127,
                "y": 23
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "c2d0ea7a-d76d-4f23-a2d5-49ce94c2537f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "7dc02304-2f04-4449-814e-3f94273afc79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "d6bb78ed-dc8d-47af-aa0d-b2a0feaadd20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "971fa88a-0ff5-4415-ae87-09b2d04c5419",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "5b9c6b33-92f2-4d46-a7d5-b2026be04fa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 245,
                "y": 2
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "f7f08a60-d193-4309-b4dc-b6c4e51af4c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "3e328d57-4fd3-495e-8dc5-682b2d61da06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 10,
                "y": 23
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "97a9b8d4-d98b-4aba-ae0e-1d65b58e4987",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 16,
                "y": 23
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "1d4bc53d-b22f-4230-96f7-2132e018b25a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 30,
                "y": 23
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "90a5dbac-af4a-4c87-ba74-ac3a9ad62ca2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 41,
                "y": 23
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "3c549d71-2e91-4591-8cd0-7199457b6a95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 118,
                "y": 23
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "454888d2-1420-49f2-a99e-3590271237a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 50,
                "y": 23
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "47e7053f-9970-4e40-9e75-df1f0b5e1c6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 57,
                "y": 23
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "585af50d-c246-4c20-b471-35c36f48cfe4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 71,
                "y": 23
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "9b9e54c1-9aca-4ff4-8286-f28e0e95da96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 77,
                "y": 23
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "0f6afcca-6bc0-4b45-9881-d1e259975ede",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 83,
                "y": 23
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "fb0d69f4-5f46-4768-af36-22a9a57a8f22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 92,
                "y": 23
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "cdc11a9e-900f-4fea-9d08-3ea6f4c889e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 98,
                "y": 23
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "24c5ecbd-27c3-40be-b9d3-fd5cdba4c540",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 104,
                "y": 23
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "a7b80280-caac-42b5-a667-aec19898816a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 109,
                "y": 23
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "c99c216d-ab4d-46f6-af7b-a9a182c02857",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "4bf13fdf-3c3a-45c2-9201-ab5a5be7d354",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 36,
                "y": 23
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "00720093-1200-4769-befb-8712cf2d8285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "d4119b58-c79b-4544-ab0e-e4da71de4029",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "8bbf6a20-0840-4df4-b356-9101703f3a39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "d09069c5-54aa-4d49-aac7-e9437ee0c0a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "45e62138-4406-4cce-820d-e3dd33a823dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "07e40108-cf97-4bf9-89dd-023f6d02a0d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "0c9226d9-9297-41b0-954b-7dca13f8f6b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "3aa9d296-5844-4371-83cc-ab46cce0e3fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "e0afb390-1d77-488b-9309-2015558cc8d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "185f7019-eb14-4fb5-9530-1caabca21dc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "f26eda7f-8bd5-4ece-a16b-88ada5fdc04d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "250fa39b-33bb-41f5-b6e4-c69c36729af8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "c86664c0-3154-425d-9c1e-148bed9cb833",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "61749b28-7529-413c-9d76-d8373a138c6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "2d312c55-3de1-43f1-a535-c3f5e1acc1e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "e9c2069b-7147-485f-bd38-f7b706a30aa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "2e8f29e0-2dfb-4a58-b93c-3eac2f1ff476",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 134,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "63f9eb44-ba75-45b4-a4c6-04a2d30eab02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "e3fafd04-6953-4448-a0d8-4723b3ac4204",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "92767993-d4b8-4181-b6d0-45ad9a9111d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 158,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "a874c119-79c4-4458-b907-db4198c6b2ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 19,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "fddaedf1-0623-4c7b-8777-c7e8eacbb0ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "f0364be3-96fd-4b42-95b6-6f2ce8802778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 19,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "bac4f188-5347-44c2-90af-04805461ea81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 19,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 132,
                "y": 23
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "4bf4b284-d0c8-4d28-80f3-c1d2c2c6397c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 75,
                "y": 44
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "df5e8aee-b1b0-442e-aa76-4557a257736b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 139,
                "y": 23
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "0f776ae7-e284-413f-84bd-78406158a8b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 104,
                "y": 44
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "7d0b321a-356e-4f20-8d20-650ff3f06714",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 113,
                "y": 44
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "c249488d-b16a-486e-8cc1-305d443716cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 122,
                "y": 44
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "8b186d27-b40a-44eb-81e8-8f499cd94e78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 131,
                "y": 44
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "e533437a-20d3-403f-82c2-629c96db13b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 140,
                "y": 44
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "257ba7df-5dc8-449c-832d-1416edc9356f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 149,
                "y": 44
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "1d5b0471-6692-4f17-9194-503805d9e465",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 158,
                "y": 44
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "470e9166-1b56-4ff4-97f4-314a08fe3023",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 167,
                "y": 44
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "eea49f6a-5573-4a18-976b-f459a6367187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 176,
                "y": 44
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "44fe9f46-0109-44a5-aa34-db756c2afc19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 193,
                "y": 44
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "6614a95c-8216-4e4c-b55f-f949ad8f8af9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 38,
                "y": 65
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "d959f4a3-1a1d-475b-ba5e-a884c75259b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 19,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 202,
                "y": 44
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "0edc10fb-fab7-4afc-87fe-e84d948c96a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 212,
                "y": 44
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "31b83231-f25a-4e85-9256-0bfaeb361d79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 221,
                "y": 44
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "dad9d1e7-4a6c-4d4a-b9df-fc80a9b22c64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 235,
                "y": 44
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "fa257978-8ba8-4d0e-8816-0f1f2f1fe0ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 244,
                "y": 44
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "48722d6d-c8fd-42a8-b408-13970dd0de5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "dbc605fc-33e5-4374-adc7-b5a06d542ded",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 65
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "d038a19d-f6ba-498c-b751-601888bf52a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 65
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "cc3d6087-089d-4c97-a4c4-cb294d41c1f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 65
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "e79b26ba-1389-4c1d-9b74-5baff29fc79c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 92,
                "y": 44
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "798d3bce-f0bd-4e7f-84a7-5c14f65895ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 185,
                "y": 44
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "eaf84978-6432-47d0-bb85-bda59fc20d22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 84,
                "y": 44
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "c91b3643-68d1-4ec3-9eb2-25f8b0bb5eb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 217,
                "y": 23
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "637b8dd6-3b7b-4c27-ba96-c91439df8514",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 148,
                "y": 23
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "2024bdcd-fc3b-4a85-94c0-6eeab2d0ff90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 156,
                "y": 23
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "bd078e4c-14e1-4e5b-a53f-6db0ee055645",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 19,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 164,
                "y": 23
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "ea567864-5493-4f98-bf07-56abcc82a849",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 170,
                "y": 23
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "025cfbcd-f758-4100-9a58-a39c2896487a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 19,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 176,
                "y": 23
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "9a28a94c-ac41-4a5c-b879-e95ebf996d05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 19,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 183,
                "y": 23
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "a31f5a48-7a55-4386-9755-89a4439c5161",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 190,
                "y": 23
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "449de5ac-d169-49d7-af4c-fc7cea5ea881",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 199,
                "y": 23
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "448cc90e-5936-47e5-9165-84ca4d5b5709",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 208,
                "y": 23
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "76d0584f-a367-47b0-944d-0cf0966f9bcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 225,
                "y": 23
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "9647a9de-3a67-496f-b46f-473fbd5b7e69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 66,
                "y": 44
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "cc1d47ee-9b19-4c7e-82e5-612e41bd7e12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 234,
                "y": 23
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "c0087be3-37bf-40f3-9e70-ad09bda6e56d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 243,
                "y": 23
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "1368b13d-3141-476f-8ea5-08bc10658dfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "0d544956-b651-4181-9bfb-3d7b9203edc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 44
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "21e34949-7818-45d2-afd6-f914fbf11310",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 44
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "463c7dbf-d1e7-4f74-b534-3bad9ca88b14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 44
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "a4829476-c735-4d50-b73c-feecd968c03b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 38,
                "y": 44
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "ba40fc63-f4ef-48dc-a0e2-b65a4fd799c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 47,
                "y": 44
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "a282d637-8d63-460d-8af2-7c595bb1f118",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 19,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 56,
                "y": 44
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "a9f0a4bf-0363-4fb0-be6d-1db795ee62e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 47,
                "y": 65
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "fde97b50-92e6-40db-9e1c-4506f2c5bae4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 19,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 97,
                "y": 128
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}