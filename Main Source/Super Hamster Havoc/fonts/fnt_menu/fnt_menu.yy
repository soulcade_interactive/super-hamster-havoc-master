{
    "id": "1f966c5b-c99f-499c-b5de-584580418bc7",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_menu",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "8BIT WONDER",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "255bae37-989e-4a7a-b5d0-5513e5243fa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 457,
                "y": 104
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "385931de-4611-42af-b1a1-45c8e0984855",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 32,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 332,
                "y": 138
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "1b2742ea-2f3c-4bdb-9e6b-3bc9c076ce77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 32,
                "offset": 4,
                "shift": 19,
                "w": 11,
                "x": 233,
                "y": 138
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "e6b0ccfd-c587-47e5-9835-06396cff196f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "b406df03-c3db-46ea-9e6e-0334cbf4a1b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 32,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 38,
                "y": 138
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "7b420997-ecae-4a9e-b545-5c40f04e2ee9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 32,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 395,
                "y": 104
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "d9caa34e-baa2-44f1-a543-5f40711e36da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 32,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 373,
                "y": 104
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "12eed47e-0455-47a6-a5dd-59d8201e0f47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 32,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 318,
                "y": 138
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "13d28fc2-1330-4063-a093-9e78d62c611d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 138
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "2226620a-0ff8-4916-a0b9-6250fa6c6eab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 20,
                "y": 138
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "67e1a09f-28f9-4da0-8333-9abfd11ce956",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 234,
                "y": 70
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "1f18140b-eff4-4c2c-b083-4028372f3098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 32,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 55,
                "y": 138
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "5599834b-8390-499e-9ce7-83a893023e08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 32,
                "offset": 6,
                "shift": 19,
                "w": 7,
                "x": 294,
                "y": 138
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "8731bf4d-c609-4cf4-87f0-f346a984c1a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 32,
                "offset": 4,
                "shift": 19,
                "w": 11,
                "x": 259,
                "y": 138
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "a3616703-e7c8-4ecf-a706-3b172e3458d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 32,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 311,
                "y": 138
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "22076513-461f-45d3-b104-314bbfc358fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 32,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 173,
                "y": 138
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "10a65481-7abc-4c6d-bb59-248e3175b9b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 408,
                "y": 70
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c5325692-7f2d-4b3e-96c4-fa5f03881473",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 32,
                "offset": -3,
                "shift": 16,
                "w": 14,
                "x": 157,
                "y": 138
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "875bbfd5-156d-43c5-9a1b-ba701d6793a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 466,
                "y": 70
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "26841178-baa5-4274-bc9d-febf1febdd68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 292,
                "y": 104
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "6068b1d4-1668-4742-bc3f-cd7858b26ef9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 263,
                "y": 104
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "0da6000e-cc68-420c-bde2-851edf045af2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 234,
                "y": 104
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "14373bf0-c370-4fc1-84f6-f209a509a61a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 205,
                "y": 104
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "4a8b1c16-81fa-4dbf-b7f3-a84bd9461f3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 176,
                "y": 104
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9b392480-5da3-4218-b2ae-bddea17f3358",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 321,
                "y": 104
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8b6e15a9-fce3-43dd-b7aa-203a9bb88287",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 118,
                "y": 104
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "df92c17a-c9c4-4ab4-900e-574796907770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 32,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 325,
                "y": 138
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "82488ecf-7542-4412-b537-d87695f8578d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 32,
                "offset": 6,
                "shift": 19,
                "w": 6,
                "x": 303,
                "y": 138
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5756c167-1b9c-4c8d-8295-de1d7caf6c24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 32,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 72,
                "y": 138
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "54b1c871-df8f-4927-bbb6-04ab733e1a55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 32,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 89,
                "y": 138
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f94e508d-24e0-47ad-978c-18c12df73a02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 32,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 123,
                "y": 138
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a05a9dbd-bcf8-48b9-9916-e6d2fbe884a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 32,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 140,
                "y": 138
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "7c825b35-c0b3-477f-a869-6ed241018733",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 32,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 417,
                "y": 104
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ab2a4a0b-674e-4c32-b39c-0090d9d7a010",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "15f9e805-5aea-451f-a622-3c0ad6b976de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 89,
                "y": 36
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "577a546c-ffb8-4339-a9a9-175c3e1747ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 477,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "1864fda4-3eaa-47ae-bb12-1d144800c194",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 448,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "77a44ce7-0279-4169-8adb-7a805cd43676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 419,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "f1d12060-f220-47de-8cc0-58ac131a43c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 390,
                "y": 2
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "6198a4ff-d175-49a9-b554-2a0ddec87c1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 361,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d2a6a3a8-bfda-4fa8-a46b-1fca158cdf96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "31267bf6-aec5-4904-9654-43c9ab86cf92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 11,
                "x": 246,
                "y": 138
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a034b614-543f-47d9-83cf-62d363601d87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 303,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "2cd4e2a7-37d9-4c39-bb63-b389bddc6d1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 274,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "518e63db-6a89-4e33-acbb-a74aea65879e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 245,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "c6c836d4-1262-40bf-9128-c952f81f3cbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 32,
                "offset": 0,
                "shift": 48,
                "w": 43,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "3fcdbd67-a00d-48f9-bad1-0fffe590f6a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 31,
                "y": 36
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "562c0d6f-fda2-4dc6-989d-2df4a39f684f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "4ac1263b-8aad-49fa-8deb-f8d654ab71fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 60,
                "y": 36
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a7435e49-5370-45a3-a470-d13c5a2fe539",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 118,
                "y": 36
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e9626709-260c-40e6-8126-a1afca85b5cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 147,
                "y": 36
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "77826e6d-34dc-4e4d-9a75-ef780b9ad9f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 176,
                "y": 36
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "5a2c579f-fe24-4870-8a09-891e37973303",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 205,
                "y": 36
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "28493d12-0c60-4d6d-9aaf-0201504fa421",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 234,
                "y": 36
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "28fdacbe-82af-430e-a41a-45773439079f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 263,
                "y": 36
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "68f8c0a9-6d28-43ae-848a-579f322f7342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 32,
                "offset": 0,
                "shift": 48,
                "w": 43,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "32d0da0f-1fb2-45ec-8e2b-9c6c9ffec15e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 292,
                "y": 36
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "4060e7f8-5b56-4441-9dee-c29057e1b974",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 321,
                "y": 36
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "3b776d22-fb3a-4cdc-9f86-8a27a9cc0b08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 350,
                "y": 36
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "0e0d74a2-c407-4c83-bf6e-c098180076b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 493,
                "y": 104
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "da004aff-00fc-4789-8b90-fcf7685a32dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 32,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 203,
                "y": 138
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "34033c3a-f399-42dc-9be9-07556848ea49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 475,
                "y": 104
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "e88788c0-c5d8-40c7-80b9-183b26813d70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 32,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 438,
                "y": 104
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "4f224e59-6cbe-4fa1-96db-b257819d7d60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 32,
                "offset": -1,
                "shift": 19,
                "w": 21,
                "x": 350,
                "y": 104
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "66bc2aae-4213-4f87-9b72-050aa8c33617",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 32,
                "offset": 6,
                "shift": 19,
                "w": 7,
                "x": 285,
                "y": 138
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d5e0a70b-7a39-48ea-a78d-4aea354a45a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 31,
                "y": 70
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "658c258e-f470-45df-abb3-4c99aca8dd64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 379,
                "y": 70
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7569c4d4-b993-4a56-9641-a3650f8ba2b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 437,
                "y": 36
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "61b0e3ed-bebb-450d-80cd-93c48cbceb65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 408,
                "y": 36
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "56ca206f-fde7-45e1-b555-2413779732e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 379,
                "y": 36
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "202d2a1b-4cdc-48c9-afbe-1163e2738319",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 466,
                "y": 36
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ac2a47f7-13c0-4f0c-b413-c3a03ef567ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 332,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f7b2a129-1050-404f-996d-01b745441a4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 60,
                "y": 70
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "333f8191-c500-4e13-a68c-3fc1a98e8bed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 11,
                "x": 272,
                "y": 138
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "f797ea12-b521-4cbd-af3b-27e1c828839a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "6ee0e39d-702f-4751-9fbd-4c385f0345a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 31,
                "y": 104
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2d2e2310-d705-438f-853f-2468a28ca3d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 60,
                "y": 104
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "daebe4d6-ce8f-449a-a51f-88e94bae970d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 32,
                "offset": 0,
                "shift": 48,
                "w": 43,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "87ed9fc4-a9f7-49a9-a31f-9990775d125c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 147,
                "y": 104
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "a8e9a761-3356-41fb-af87-01dae2281e98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 89,
                "y": 104
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "bbf85dde-7c18-492f-9be8-8410f8f88d9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 437,
                "y": 70
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a6fa530e-9567-45e5-b4b1-8d303cb45cfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 350,
                "y": 70
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "5ee3c3e5-445e-4582-b8a1-e9008ab4546f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 321,
                "y": 70
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "4878a1da-f5a6-47c5-b051-869374c17b1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 292,
                "y": 70
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "15010d76-7037-432e-be8d-b4ee498b7a08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 263,
                "y": 70
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "c9d3e9e7-d233-4f9f-9a09-e66ba4cb0399",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 205,
                "y": 70
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3d631ee7-d2f3-4f9b-ba3e-529ca27415f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 176,
                "y": 70
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "4e6f9c07-37c0-420f-a179-362433a821fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 32,
                "offset": 0,
                "shift": 48,
                "w": 43,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "64d8d677-fb0b-4949-90da-f45146392561",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 147,
                "y": 70
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "cc11f2ab-e056-4ba0-9bf2-55e03b3c2daf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 118,
                "y": 70
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "0a74d2ad-6171-4575-8b33-05adbd51761d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 89,
                "y": 70
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "a4b03c08-707d-4818-a36b-0e19e9940cfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 32,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 188,
                "y": 138
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "c6d237f7-4d6c-4589-a0ec-1c078044d6ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 32,
                "offset": 8,
                "shift": 19,
                "w": 3,
                "x": 339,
                "y": 138
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "9e7c4f9b-28a6-444a-9f1c-389e88790d4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 32,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 218,
                "y": 138
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "4c22e067-781e-42bc-845e-45eb03f1fabd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 32,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 106,
                "y": 138
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Nominal",
    "textureGroupId": "00000000-0000-0000-0000-000000000000"
}