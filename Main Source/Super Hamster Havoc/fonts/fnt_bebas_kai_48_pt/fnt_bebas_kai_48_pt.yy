{
    "id": "b9c99dfe-7193-4a86-ad8c-4c514c25c9c7",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_bebas_kai_48_pt",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bebas Kai",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "3d41d0c6-8d56-4940-b0cf-8641413becca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 77,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "f7c20f05-d4a8-4ef4-89c6-240afe0f97b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 77,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 877,
                "y": 160
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "97fb6e77-8006-4951-b26a-322fa6123a5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 77,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 887,
                "y": 160
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a8f150e4-b58b-4010-b8ec-fad8c94d6923",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 77,
                "offset": 1,
                "shift": 31,
                "w": 29,
                "x": 907,
                "y": 160
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f4cebcd4-d0ae-477c-9142-68f3e6ca9f8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 77,
                "offset": 3,
                "shift": 28,
                "w": 22,
                "x": 938,
                "y": 160
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "9c8e476c-baf1-461d-894f-c6b232afcfa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 77,
                "offset": 1,
                "shift": 37,
                "w": 35,
                "x": 962,
                "y": 160
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c436c905-8fa1-4aed-b41d-295bf6f88f3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 77,
                "offset": 2,
                "shift": 31,
                "w": 29,
                "x": 2,
                "y": 239
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8fcb2f09-03ee-48db-b149-90d942d9da40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 77,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 33,
                "y": 239
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "0b3e1bd6-aa15-4f5f-af24-f5f9579c1ac6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 77,
                "offset": 3,
                "shift": 19,
                "w": 15,
                "x": 43,
                "y": 239
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "2ee91e8b-4c88-4664-9abe-c862f6521e4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 77,
                "offset": 1,
                "shift": 19,
                "w": 15,
                "x": 60,
                "y": 239
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "6ce495e8-c1cc-4e36-aafa-f4a6e089cf49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 77,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 102,
                "y": 239
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "5de00eeb-257a-4faa-8cfe-a643908d0afc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 77,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 307,
                "y": 239
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "1449bba1-9b57-4cdc-b8e5-9a5d1afb0498",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 77,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 125,
                "y": 239
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "bfd42044-2e9d-4096-a0ea-61de7b6812b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 77,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 135,
                "y": 239
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "cd309436-9183-4629-9a97-2fa8e68f414f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 77,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 152,
                "y": 239
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "2d310272-e518-419f-b9e9-fd51e69e6998",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 77,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 162,
                "y": 239
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "7de2fc30-5ddf-4058-963e-44cc7f79232d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 77,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 186,
                "y": 239
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "956d10bf-7fcd-4e7d-a4f7-c3a0220a619f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 77,
                "offset": 1,
                "shift": 22,
                "w": 17,
                "x": 211,
                "y": 239
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "a5bb7b9c-350a-4baa-8caa-ab6243ab3f93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 77,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 230,
                "y": 239
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a81cbe63-aae7-468f-8a4d-d4422bd33daa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 77,
                "offset": 1,
                "shift": 26,
                "w": 23,
                "x": 254,
                "y": 239
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "d40df533-aaf1-493e-bc2e-a9625abe7157",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 77,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 279,
                "y": 239
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "42063e51-75c3-42fb-95ee-da75b7e9647c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 77,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 853,
                "y": 160
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "f2218092-4c73-4905-9e80-25137a283dc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 77,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 77,
                "y": 239
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "dac127ca-d225-42ff-9b4e-abeb8cc837b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 77,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 829,
                "y": 160
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "11242f21-5940-4b85-ade2-3fe51b9f4e91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 77,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 529,
                "y": 160
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8e385673-3e73-4ee2-b804-49c799e92972",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 77,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 303,
                "y": 160
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "3cd75a5e-bedc-4ae1-b0a0-96f2d1632464",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 77,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 328,
                "y": 160
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "89308417-9b94-4313-b94f-9e0ec477bf96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 77,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 338,
                "y": 160
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "782a84d7-27ce-40d0-9bc8-8408683292f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 77,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 348,
                "y": 160
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "357c912b-fb0a-4734-9ebe-ec9a939bbd45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 77,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 376,
                "y": 160
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "a0b907f7-1fac-4b39-898c-8163809d55cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 77,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 404,
                "y": 160
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "863f079b-3dc1-441a-8c4f-11172f0b56a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 77,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 432,
                "y": 160
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "1de3beec-7217-42ea-94df-9d193799ecf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 77,
                "offset": 1,
                "shift": 47,
                "w": 45,
                "x": 455,
                "y": 160
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d72f360f-84d7-4022-a7e7-ca163893f3b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 77,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 502,
                "y": 160
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "9986b1ab-a4c7-4ae6-9c2d-75e903cde2a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 77,
                "offset": 3,
                "shift": 27,
                "w": 22,
                "x": 555,
                "y": 160
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "32bba32d-0cac-4dd6-b5b4-4ce790affac8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 77,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 773,
                "y": 160
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "98f42d05-875b-494b-aab7-10027514f417",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 77,
                "offset": 3,
                "shift": 28,
                "w": 22,
                "x": 579,
                "y": 160
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "fcb78233-f449-44e8-8771-48590620eee3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 77,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 603,
                "y": 160
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "6a6237be-12bc-4338-9a20-b12715b5a762",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 77,
                "offset": 3,
                "shift": 23,
                "w": 20,
                "x": 625,
                "y": 160
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "2e94eefe-205b-449b-b249-8225a12ef10f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 77,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 647,
                "y": 160
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f6bf3ffc-f03c-4555-8108-835760f7583a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 77,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 672,
                "y": 160
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "51831a96-2ee1-46ab-ae38-af1f50771d3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 77,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 697,
                "y": 160
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "380c1785-c970-4397-8ebc-5f6aff88ae93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 77,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 707,
                "y": 160
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "bb44fdc4-51b8-45ec-86fe-375b31cb8f56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 77,
                "offset": 3,
                "shift": 28,
                "w": 25,
                "x": 725,
                "y": 160
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c3a016fe-5101-4c9c-9db1-bfa7e83f0bb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 77,
                "offset": 3,
                "shift": 22,
                "w": 19,
                "x": 752,
                "y": 160
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d0151eb1-4758-430b-9a83-a035ec2483c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 77,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 797,
                "y": 160
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "9f2d3de1-828e-4109-bea9-308b31470cb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 77,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 365,
                "y": 239
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "7410d19a-3245-4aef-9a9f-a140e5e2192e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 77,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 917,
                "y": 239
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f4a6339c-109f-4d98-8a41-94f02054d554",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 77,
                "offset": 3,
                "shift": 26,
                "w": 22,
                "x": 390,
                "y": 239
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e4a8e0d8-8c28-4129-a85e-23fa6f04a3b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 77,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 989,
                "y": 239
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9d5dce30-f92c-463b-bd32-a0c485303038",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 77,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 2,
                "y": 318
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2cd3e6d9-1711-4ea4-8e46-2d83cc634e3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 77,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 28,
                "y": 318
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "56baed9e-2f22-4a82-87bc-385877d5dd50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 77,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 52,
                "y": 318
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "8f1c23a5-f9ec-4606-bbf6-d81294a272c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 77,
                "offset": 3,
                "shift": 28,
                "w": 22,
                "x": 76,
                "y": 318
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a505cb54-b140-4c9f-be30-66af0ab3f43b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 77,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 100,
                "y": 318
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "7b248b49-258f-49ba-a8c9-db90e7ffabc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 77,
                "offset": 1,
                "shift": 38,
                "w": 36,
                "x": 127,
                "y": 318
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "45762fba-5547-4afe-87b2-a891d33f42c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 77,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 165,
                "y": 318
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f23db723-3300-4373-bfff-9e743330b856",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 77,
                "offset": -1,
                "shift": 24,
                "w": 26,
                "x": 193,
                "y": 318
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4307ce06-636b-469e-a0cb-3987a91b87a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 77,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 243,
                "y": 318
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "bc984f88-dc86-444c-88fb-0bc9fffd68cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 77,
                "offset": 4,
                "shift": 19,
                "w": 13,
                "x": 477,
                "y": 318
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "7efd10fb-eabe-4e1a-811d-66f1c9752abf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 77,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 266,
                "y": 318
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "600d39c7-c32b-4ad6-b063-7c814b174a2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 77,
                "offset": 2,
                "shift": 19,
                "w": 13,
                "x": 290,
                "y": 318
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a3ac7b0a-8c0c-4905-b02a-d6fce1020e89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 77,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 305,
                "y": 318
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "46eccc21-94bd-482b-923f-02203208708c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 77,
                "offset": 0,
                "shift": 28,
                "w": 29,
                "x": 333,
                "y": 318
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "95f36ffc-7e03-422b-91f1-77f82570c926",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 77,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 364,
                "y": 318
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8bdd7cb5-a815-4eb9-9a78-8826e25016a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 77,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 378,
                "y": 318
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "3b13de40-f9a0-46b1-8211-804d760b1aa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 77,
                "offset": 3,
                "shift": 27,
                "w": 22,
                "x": 405,
                "y": 318
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ff273255-6c61-47c0-a771-186f003624f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 77,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 429,
                "y": 318
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "1e6a7d63-ac30-4672-a0c4-2f5eb3b616eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 77,
                "offset": 3,
                "shift": 28,
                "w": 22,
                "x": 453,
                "y": 318
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "c1ed14f9-e210-495f-aa0d-e0e902ebd6aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 77,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 967,
                "y": 239
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "81a24187-fbf8-487b-9154-71b767ee49b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 77,
                "offset": 3,
                "shift": 23,
                "w": 20,
                "x": 221,
                "y": 318
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "a35d453b-8ada-4049-a451-dab6702294a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 77,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 942,
                "y": 239
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "9a90b7ae-cf67-48d5-b314-ff07d666d1fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 77,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 623,
                "y": 239
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e5a3708f-64ea-4df8-9a1f-c9eeef24749f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 77,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 414,
                "y": 239
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "372cc7d9-9e1f-44fe-b14c-dce879a22447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 77,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 424,
                "y": 239
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "086ae601-3e8b-429f-be37-a12f4dab17aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 77,
                "offset": 3,
                "shift": 28,
                "w": 25,
                "x": 442,
                "y": 239
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "49a74f26-9c35-4437-89b2-95dd0cc7eced",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 77,
                "offset": 3,
                "shift": 22,
                "w": 19,
                "x": 469,
                "y": 239
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "3f80d870-c61d-4d86-b7f0-b4c066163124",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 77,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 490,
                "y": 239
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "2ec23851-4a46-419d-8569-8c1452b5f66f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 77,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 522,
                "y": 239
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "a8348ce7-f411-4b36-bc62-096d20ef8075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 77,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 547,
                "y": 239
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "bbca8785-5178-445c-986e-57c6b6ecd36e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 77,
                "offset": 3,
                "shift": 26,
                "w": 22,
                "x": 572,
                "y": 239
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "091844d8-9cf7-44eb-aa9b-5cec1e71cc12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 77,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 596,
                "y": 239
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "32bbd3c8-b546-4244-997d-0319fb6f4cda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 77,
                "offset": 3,
                "shift": 27,
                "w": 24,
                "x": 648,
                "y": 239
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "4608e2bb-7394-4f18-8cde-8f1d6bfdbc01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 77,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 893,
                "y": 239
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a5149ea2-b226-4586-bc47-ebbc0d763418",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 77,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 674,
                "y": 239
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "eedd6fc5-fb78-4bc7-bf2b-cebb65d73d91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 77,
                "offset": 3,
                "shift": 28,
                "w": 22,
                "x": 698,
                "y": 239
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a342a9ed-de3e-4da0-abf1-c8767866f806",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 77,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 722,
                "y": 239
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "4fdc7121-dd47-4329-a035-1e0b402edb69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 77,
                "offset": 1,
                "shift": 38,
                "w": 36,
                "x": 749,
                "y": 239
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d15c29ef-846c-4dcc-915e-d6aeaf594866",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 77,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 787,
                "y": 239
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "b221de76-11a8-45be-9fdb-c248cb967727",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 77,
                "offset": -1,
                "shift": 24,
                "w": 26,
                "x": 815,
                "y": 239
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "9d5ba43e-a3c3-41c0-906e-f13591f22f10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 77,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 843,
                "y": 239
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "f61b5cbc-3280-41a1-ad3c-3c6a7a497d2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 77,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 866,
                "y": 239
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f2d9cc5c-3e38-45cc-a038-814d8764a496",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 77,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 884,
                "y": 239
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "02fe55b8-7a8e-4e53-9f5f-a332a889673b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 77,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 285,
                "y": 160
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "3838148d-cd5f-406a-ad34-666366fccf78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 77,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 335,
                "y": 239
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "2e0f1dfb-01f6-4e83-9082-310601af2adc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 77,
                "offset": 0,
                "shift": 12,
                "w": 0,
                "x": 283,
                "y": 160
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "ee3350cd-3af4-49dc-b889-1260114840cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 77,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 84,
                "y": 81
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "81f4a3d0-9b58-4020-89b3-bfe553273900",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 77,
                "offset": 3,
                "shift": 28,
                "w": 22,
                "x": 609,
                "y": 2
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "81a9c451-2b63-4a66-b706-6f601ff0fe65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 77,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 633,
                "y": 2
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "b058736e-3eaa-43e8-9632-1eb05caf90b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 77,
                "offset": 0,
                "shift": 28,
                "w": 27,
                "x": 659,
                "y": 2
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "5117a7ac-721b-4f39-9156-46493f04907b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 77,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 688,
                "y": 2
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "f049001e-3b37-44e4-9b17-530cea9e376d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 77,
                "offset": 4,
                "shift": 15,
                "w": 7,
                "x": 716,
                "y": 2
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "2035bbb6-f8a8-4062-b801-e7ac9706dd4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 77,
                "offset": 1,
                "shift": 26,
                "w": 23,
                "x": 725,
                "y": 2
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "61c8f69d-f303-4a0d-955f-6e66cf5c0988",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 77,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 750,
                "y": 2
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "a1238990-84e5-45b4-9f7e-3042c1fe7485",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 77,
                "offset": 1,
                "shift": 47,
                "w": 45,
                "x": 768,
                "y": 2
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "f1446ccd-ee78-40b4-841f-873c06d0ee35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 77,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 815,
                "y": 2
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "9089110b-71b1-43c1-a9ea-73ae182c4b85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 77,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 841,
                "y": 2
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "aa336ddb-018a-4f5e-9927-efefb65d460f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 77,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 56,
                "y": 81
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "b032859c-87d4-489b-9787-8962f8dc8ad3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 77,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 867,
                "y": 2
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "e5142928-b9df-4e73-aae1-ce0a1cb4a46a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 77,
                "offset": 1,
                "shift": 47,
                "w": 45,
                "x": 884,
                "y": 2
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "a44802bd-87fd-4fe5-a180-764baae956c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 77,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 931,
                "y": 2
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "e8eeb75d-81a1-4a2d-84c0-1026942cd803",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 77,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 947,
                "y": 2
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "59ea5b52-2933-4989-9000-95baadc990b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 77,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 964,
                "y": 2
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "9b33643b-53b3-4d19-ba74-6b5d1b5fe25a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 77,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 992,
                "y": 2
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "1af082cf-22c5-4c24-a920-3fef5d6cf920",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 77,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 81
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "329188fd-1865-4c0f-b3ba-ab726807d2b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 77,
                "offset": 4,
                "shift": 16,
                "w": 12,
                "x": 18,
                "y": 81
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "fc476bc8-66a4-48b2-8c7e-54de3c198842",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 77,
                "offset": 3,
                "shift": 28,
                "w": 22,
                "x": 32,
                "y": 81
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "0ec61aa3-ad25-4ce5-b4d2-8c87c16a3cd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 77,
                "offset": 2,
                "shift": 32,
                "w": 26,
                "x": 581,
                "y": 2
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "f38b93b4-45e4-460a-b8e9-ef9446e3c53b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 77,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 831,
                "y": 2
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "1f551c2e-7b97-4992-bdfc-b074ef6fc0d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 77,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 565,
                "y": 2
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "2d836cce-d197-4d91-96b7-ca4def18c8e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 77,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 273,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "cfe48729-877b-419b-8e95-51214056531a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 77,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "16132d05-ee51-41bb-b1fb-e7a00fb317bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 77,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "b3407ca8-a80f-4403-9bd4-3d5d6f6a9be1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 77,
                "offset": 0,
                "shift": 34,
                "w": 34,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "4dda9188-823b-4557-88c3-1896c6a45daf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 77,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "decc9948-e6e7-455d-98b7-e9d1c824dc6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 77,
                "offset": 1,
                "shift": 37,
                "w": 36,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "0283d621-3f36-4d3a-a792-05e5209acaac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 77,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "cccdc311-2e95-4c57-982b-e5b01db586fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 77,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "7ccbf5cf-442c-495b-ad24-dde8b6824a08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 77,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "aee29460-4eb8-4ea4-864d-82b8c91e1db5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 77,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 246,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "9281f347-e609-4aea-ae93-d9dfc6ce1088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 77,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 286,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "8c9ae543-8282-487f-bcc3-09acc6d0dd3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 77,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 520,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "796c922f-a635-4819-a0b0-0e93207740c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 77,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 313,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "4b79ef46-6473-46e0-a137-3511e3b3cffa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 77,
                "offset": 0,
                "shift": 39,
                "w": 38,
                "x": 340,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "b793c425-aca7-4e27-85b2-766c1a0e1dca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 77,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 380,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "5c7044a3-72fc-47ed-ac50-77b91b9de28b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 77,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 404,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "34c02a36-92ae-49ac-965e-a2c1905fc130",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 77,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 426,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "08bc1ba1-5013-424e-b3e2-a5eadb5d05de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 77,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 448,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "c92a3ef8-44bc-4ed0-bc1d-aa03c7382d15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 77,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 470,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "cd3b250c-739d-4ca9-a4c3-e717e5034a6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 77,
                "offset": -1,
                "shift": 14,
                "w": 12,
                "x": 492,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "706f78dc-f171-47da-ba6b-c81b79754d2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 77,
                "offset": 3,
                "shift": 14,
                "w": 12,
                "x": 506,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "fa16f382-4f43-4739-90ad-6f04197557d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 77,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 547,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "0e737ef1-8771-4178-b6b6-86c62a8c3b79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 77,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 94,
                "y": 81
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "a02fc08e-b601-4f29-982a-8834efba7eea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 77,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 622,
                "y": 81
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "0ec91b51-989b-4321-9ee3-243ef179d233",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 77,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 112,
                "y": 81
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "11c0b05e-f8a6-4ab1-b3fd-c92f66f99f75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 77,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 711,
                "y": 81
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "5fd4b4d0-ad0b-46f6-a5f9-fc8d684c3356",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 77,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 736,
                "y": 81
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "41310a0c-18d9-4263-bb36-4ca71e57cecf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 77,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 761,
                "y": 81
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "a78e2d9d-5eaf-4ed2-a5e6-9b839304d707",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 77,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 786,
                "y": 81
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "2dd1e7a0-482c-4b32-a796-346b7f0838c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 77,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 811,
                "y": 81
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "3c5f489a-bd2a-438d-894d-8b0d17fc4149",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 77,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 836,
                "y": 81
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "3e4cdf83-dd60-41ea-9dd5-30355f8e9d3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 77,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 863,
                "y": 81
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "506ff960-8293-42ff-9d03-6b0915e3b3c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 77,
                "offset": 3,
                "shift": 28,
                "w": 22,
                "x": 889,
                "y": 81
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "fd7435b3-ed3c-4fe1-ba7b-4b8c6ef6c5b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 77,
                "offset": 3,
                "shift": 28,
                "w": 22,
                "x": 913,
                "y": 81
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "35bced51-199a-4e49-ba84-d5d3ced4df2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 77,
                "offset": 3,
                "shift": 28,
                "w": 22,
                "x": 961,
                "y": 81
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "f68c409a-1055-4178-ba46-7e76cfcb7f7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 77,
                "offset": 3,
                "shift": 28,
                "w": 22,
                "x": 235,
                "y": 160
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "88f9fb57-2d6b-4735-b508-79490c82b2d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 77,
                "offset": -1,
                "shift": 24,
                "w": 26,
                "x": 985,
                "y": 81
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "19b80a23-2ee1-4353-bcdb-4755b5b3b9a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 77,
                "offset": 3,
                "shift": 26,
                "w": 22,
                "x": 2,
                "y": 160
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "df714ba5-4cb8-45dd-ab28-aa83fc7bd500",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 77,
                "offset": 1,
                "shift": 46,
                "w": 45,
                "x": 26,
                "y": 160
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "f56fcc7e-1024-4e44-b7a2-203df368d444",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 77,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 73,
                "y": 160
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "afd1c58f-a9ac-4c7a-bdc2-40682d4e1c81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 77,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 100,
                "y": 160
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "d59b5374-3b73-460d-a47a-3534e622dca5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 77,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 127,
                "y": 160
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "7450bd54-94cc-481a-a7ec-1371c7b32d13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 77,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 154,
                "y": 160
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "14bfae82-33d2-412b-a520-1886b418bf89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 77,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 181,
                "y": 160
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "d39ab940-46ef-444b-90c2-caec5bb5e35d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 77,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 208,
                "y": 160
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "0f41976c-ceba-4df0-a0e2-a666d241eb77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 77,
                "offset": 0,
                "shift": 39,
                "w": 38,
                "x": 671,
                "y": 81
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "b071d87f-0f33-4797-9261-259e0fb61161",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 77,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 937,
                "y": 81
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "b2aad034-817a-4b0a-ae07-46d316c0beb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 77,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 649,
                "y": 81
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "ee21f07b-676e-47be-86e1-e58696b25452",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 77,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 322,
                "y": 81
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "b83ed767-e60a-4180-89af-6ab5681831c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 77,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 137,
                "y": 81
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "00501389-6f9b-472a-9f8f-cfd70cddd154",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 77,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 159,
                "y": 81
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "95d6513c-cff0-485c-8b90-4dddc832f79e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 77,
                "offset": -1,
                "shift": 14,
                "w": 12,
                "x": 181,
                "y": 81
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "16df8599-c164-452b-9461-9e64edbc7864",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 77,
                "offset": 3,
                "shift": 14,
                "w": 12,
                "x": 195,
                "y": 81
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "6e538bda-5e55-4119-89f0-a05a078209d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 77,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 209,
                "y": 81
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "279350c3-cbfd-4101-86e2-887dc29cef7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 77,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 227,
                "y": 81
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "cbb45105-d3c3-4e81-b7d9-dbef797b0f1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 77,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 245,
                "y": 81
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "ef783dbf-4d46-49dc-9a0f-49b24c6a579a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 77,
                "offset": 3,
                "shift": 29,
                "w": 23,
                "x": 272,
                "y": 81
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "296bcf4a-858b-49bb-ad05-3576fc7e7e1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 77,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 297,
                "y": 81
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "a0e2e83d-10ba-4703-8b71-b880398cc586",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 77,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 344,
                "y": 81
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "24cfaa68-61a2-45a8-a220-de3a0d68b9c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 77,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 597,
                "y": 81
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "0c464c86-427b-4077-b9fd-33a835268c59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 77,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 369,
                "y": 81
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "75187522-2578-43b2-a463-d3df45300f65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 77,
                "offset": 2,
                "shift": 28,
                "w": 23,
                "x": 394,
                "y": 81
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "fb53e3ee-c4c6-4634-89c0-cd632283be84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 77,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 419,
                "y": 81
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "d3c7a73a-66f7-4219-9ec0-ab18deb250db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 77,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 447,
                "y": 81
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "5b4e39ee-d0de-4733-bb1e-5807d1a7ee51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 77,
                "offset": 3,
                "shift": 28,
                "w": 22,
                "x": 473,
                "y": 81
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "357013c9-e35d-4150-9b71-4071c888da31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 77,
                "offset": 3,
                "shift": 28,
                "w": 22,
                "x": 497,
                "y": 81
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "ea2562ca-d1af-499b-9b41-8d97684080a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 77,
                "offset": 3,
                "shift": 28,
                "w": 22,
                "x": 521,
                "y": 81
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "e976eeae-f4d5-471f-b815-06bbdb983078",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 77,
                "offset": 3,
                "shift": 28,
                "w": 22,
                "x": 545,
                "y": 81
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "4a7ad390-e798-40f6-8d58-a56913678047",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 77,
                "offset": -1,
                "shift": 24,
                "w": 26,
                "x": 569,
                "y": 81
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "f2cf5e94-47c5-43e3-a60c-47811b192ac3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 77,
                "offset": 3,
                "shift": 26,
                "w": 22,
                "x": 259,
                "y": 160
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "6951fa2d-7d89-4b2e-b76a-4a94e8afbb1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 77,
                "offset": -1,
                "shift": 24,
                "w": 26,
                "x": 492,
                "y": 318
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 48,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}