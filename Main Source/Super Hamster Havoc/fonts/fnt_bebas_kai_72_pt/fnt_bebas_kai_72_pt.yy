{
    "id": "56d053fb-5eea-459d-8c1b-d4916f8cead7",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_bebas_kai_72_pt",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bebas Kai",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "df362a2d-15d9-4439-b079-4964a6f8afe8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 115,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "88daebd6-cbe0-4fad-ad88-1a728a91a9c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 115,
                "offset": 5,
                "shift": 22,
                "w": 12,
                "x": 219,
                "y": 470
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a4cfcfeb-1033-4753-adaa-c83a34c970c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 115,
                "offset": 4,
                "shift": 36,
                "w": 27,
                "x": 233,
                "y": 470
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "43a38535-7504-48eb-8bd5-b4037aa2a25b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 115,
                "offset": 2,
                "shift": 47,
                "w": 42,
                "x": 262,
                "y": 470
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "232195f0-d0f5-4859-b410-b5c02edbdee1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 115,
                "offset": 4,
                "shift": 41,
                "w": 33,
                "x": 306,
                "y": 470
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "a6586441-e9bf-4a5a-8319-0d67d1a313a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 115,
                "offset": 2,
                "shift": 56,
                "w": 51,
                "x": 341,
                "y": 470
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "f0ca7a39-4e1a-4a8f-84f8-73873d21087f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 115,
                "offset": 3,
                "shift": 46,
                "w": 43,
                "x": 394,
                "y": 470
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "aa54b18d-6c8b-4088-93ce-3ee5c7647f3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 115,
                "offset": 4,
                "shift": 20,
                "w": 12,
                "x": 439,
                "y": 470
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b000aa90-1d60-4a77-ad40-31eced8a1019",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 115,
                "offset": 4,
                "shift": 29,
                "w": 23,
                "x": 453,
                "y": 470
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "0541c7a3-807f-4049-afb2-c38e7a9eff45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 115,
                "offset": 2,
                "shift": 29,
                "w": 23,
                "x": 478,
                "y": 470
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f4fa9665-f49e-4417-80d0-a4be5dcc61cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 115,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 539,
                "y": 470
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "84232936-ab91-4085-805a-da695de0999d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 115,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 833,
                "y": 470
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "2fa94eea-790a-4be9-9699-cc3b00fb99c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 115,
                "offset": 5,
                "shift": 22,
                "w": 12,
                "x": 573,
                "y": 470
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c4218e60-2062-4c35-98ac-224802ea1cae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 115,
                "offset": 3,
                "shift": 28,
                "w": 22,
                "x": 587,
                "y": 470
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7396f3d6-6390-4ec8-a597-0119b1041f7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 115,
                "offset": 5,
                "shift": 22,
                "w": 12,
                "x": 611,
                "y": 470
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "744a4a26-b623-4c78-bade-5456fa0168b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 115,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 625,
                "y": 470
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ce1d5b16-a5e3-4380-9348-3930c2592b06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 115,
                "offset": 4,
                "shift": 42,
                "w": 34,
                "x": 661,
                "y": 470
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b612b902-2f5f-4631-a51c-c967fb15e658",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 115,
                "offset": 2,
                "shift": 32,
                "w": 25,
                "x": 697,
                "y": 470
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e18f0a9d-8764-4dae-9960-e36d8f19ba6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 115,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 724,
                "y": 470
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c7e83bc9-1b21-4d92-8a91-f3b8a2474060",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 115,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 758,
                "y": 470
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "70007171-ab73-4f28-90f4-c47a06a4d2f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 115,
                "offset": 1,
                "shift": 39,
                "w": 37,
                "x": 794,
                "y": 470
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "3e434f6b-4b8f-464a-95fc-1e76ef8e5cf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 115,
                "offset": 3,
                "shift": 38,
                "w": 33,
                "x": 184,
                "y": 470
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1ef09cc9-073a-49bc-ae33-3d41de05813c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 115,
                "offset": 4,
                "shift": 41,
                "w": 34,
                "x": 503,
                "y": 470
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8b825736-eebf-4b69-9956-cb545218898a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 115,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 150,
                "y": 470
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "edd1bb30-badf-479f-a202-9e99ee0448ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 115,
                "offset": 2,
                "shift": 40,
                "w": 35,
                "x": 737,
                "y": 353
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "725ef2ae-0350-408e-8cbb-9d4e5eec5920",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 115,
                "offset": 3,
                "shift": 41,
                "w": 34,
                "x": 408,
                "y": 353
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1ec45c64-2b8c-456c-b541-ee4c37733d17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 115,
                "offset": 5,
                "shift": 22,
                "w": 12,
                "x": 444,
                "y": 353
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "9ee6a08c-36b4-4f9f-b64b-79431427d5d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 115,
                "offset": 5,
                "shift": 22,
                "w": 12,
                "x": 458,
                "y": 353
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "c37d30b7-fb89-42ed-a067-f9456d5f8485",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 115,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 472,
                "y": 353
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "1ba14261-5ca2-4050-89a3-a241b8867f8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 115,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 513,
                "y": 353
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "b8ecd3cc-a7a5-4b83-a4a9-bbea5f599847",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 115,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 554,
                "y": 353
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "5aef5389-1183-4071-80b6-d4eaa2710b98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 115,
                "offset": 2,
                "shift": 35,
                "w": 31,
                "x": 595,
                "y": 353
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6cd580eb-1c9e-4ec7-98d2-4495ccf90d0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 115,
                "offset": 2,
                "shift": 71,
                "w": 67,
                "x": 628,
                "y": 353
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "718a80aa-eb53-4a20-9de4-64e96309962b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 115,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 697,
                "y": 353
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "dc6bbbce-33c3-4161-a74d-4f3c39a94641",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 115,
                "offset": 5,
                "shift": 40,
                "w": 33,
                "x": 774,
                "y": 353
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "3548e152-1598-4dc3-b5da-f09760b0598c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 115,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 70,
                "y": 470
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "3556642e-7447-4067-bab6-a12d1e88c1ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 115,
                "offset": 5,
                "shift": 41,
                "w": 32,
                "x": 809,
                "y": 353
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "9d43e026-a7ee-4f96-a5e8-1be66b5dbd12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 115,
                "offset": 5,
                "shift": 37,
                "w": 30,
                "x": 843,
                "y": 353
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "6c3df9d6-c6b7-418f-b70a-d1266e890a17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 115,
                "offset": 5,
                "shift": 35,
                "w": 29,
                "x": 875,
                "y": 353
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "cb13cee3-d24e-4186-ab5e-ada143de4c28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 115,
                "offset": 4,
                "shift": 40,
                "w": 33,
                "x": 906,
                "y": 353
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "e5243465-3efc-446a-b2e0-f654f38871c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 115,
                "offset": 5,
                "shift": 43,
                "w": 33,
                "x": 941,
                "y": 353
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "70c8ad7d-54ae-4fb3-86fa-a407add6ac3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 115,
                "offset": 5,
                "shift": 21,
                "w": 11,
                "x": 976,
                "y": 353
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5a8cb853-c327-4698-9aa8-4c6b50a95f62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 115,
                "offset": 1,
                "shift": 29,
                "w": 23,
                "x": 989,
                "y": 353
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "a11b24db-4b83-4858-a507-96b764ab32f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 115,
                "offset": 5,
                "shift": 41,
                "w": 36,
                "x": 2,
                "y": 470
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "31b88568-5587-4b31-9598-1b291c7baf37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 115,
                "offset": 5,
                "shift": 34,
                "w": 28,
                "x": 40,
                "y": 470
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9d1ac7b3-da94-4265-927c-866f8367f356",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 115,
                "offset": 5,
                "shift": 54,
                "w": 44,
                "x": 104,
                "y": 470
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "38d97130-1e28-446a-99ba-e079480ae796",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 115,
                "offset": 5,
                "shift": 44,
                "w": 34,
                "x": 917,
                "y": 470
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a8c95b24-9d29-4da9-89ee-3b7966ccffb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 115,
                "offset": 4,
                "shift": 42,
                "w": 34,
                "x": 709,
                "y": 587
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "428b44c9-a82c-447e-8712-e9acb491f60f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 115,
                "offset": 5,
                "shift": 39,
                "w": 33,
                "x": 953,
                "y": 470
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8ad1d9a4-1cc4-4f6f-8163-9cf8e604625b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 115,
                "offset": 4,
                "shift": 42,
                "w": 36,
                "x": 812,
                "y": 587
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4f1d0fb5-3af2-446e-87e3-e178d7174132",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 115,
                "offset": 5,
                "shift": 41,
                "w": 35,
                "x": 850,
                "y": 587
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "484d6a98-8be0-4295-86bb-bda1dace88c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 115,
                "offset": 2,
                "shift": 34,
                "w": 32,
                "x": 887,
                "y": 587
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "36434981-1cd2-4e2c-a687-f002cdf0c773",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 115,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 921,
                "y": 587
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "94ba5afa-96ae-42a4-85b0-a9b44303651b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 115,
                "offset": 4,
                "shift": 41,
                "w": 33,
                "x": 955,
                "y": 587
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ab83d04e-27e4-43f5-b613-3f6a6a505d9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 115,
                "offset": 1,
                "shift": 39,
                "w": 37,
                "x": 2,
                "y": 704
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "148548ce-0594-4fa3-9504-d94f9586cded",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 115,
                "offset": 1,
                "shift": 57,
                "w": 55,
                "x": 41,
                "y": 704
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "1437e951-7e35-49c6-97bb-513a62f4a006",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 115,
                "offset": 0,
                "shift": 39,
                "w": 39,
                "x": 98,
                "y": 704
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "141ec0eb-a668-421f-9743-958bd3ece0f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 115,
                "offset": -2,
                "shift": 36,
                "w": 40,
                "x": 139,
                "y": 704
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "662ca354-8673-485a-b0f6-10aa368e8869",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 115,
                "offset": 1,
                "shift": 34,
                "w": 31,
                "x": 212,
                "y": 704
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "73aed8c9-6d01-4176-ae6b-1182beb3094a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 115,
                "offset": 6,
                "shift": 29,
                "w": 19,
                "x": 549,
                "y": 704
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "9bec8d9c-90ed-43bf-be41-685d7fc1803e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 115,
                "offset": 1,
                "shift": 36,
                "w": 34,
                "x": 245,
                "y": 704
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "afdad181-62c7-44a2-bb60-89b4b5b71401",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 115,
                "offset": 4,
                "shift": 29,
                "w": 19,
                "x": 281,
                "y": 704
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "347c159c-e74d-4d5a-a5f5-d6aa30d6a55b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 115,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 302,
                "y": 704
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "1a36bdd4-9c2f-486b-b6e5-93455cc2a3a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 115,
                "offset": 0,
                "shift": 42,
                "w": 43,
                "x": 343,
                "y": 704
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f2b3edb7-2f13-454f-9f34-b6b02c0bda88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 115,
                "offset": 1,
                "shift": 24,
                "w": 16,
                "x": 388,
                "y": 704
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8ed87d44-6c6a-4524-9058-0496afb517bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 115,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 406,
                "y": 704
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "60ddc529-f3b3-4300-bc26-380a2bd50c83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 115,
                "offset": 5,
                "shift": 40,
                "w": 33,
                "x": 446,
                "y": 704
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "c47c30f2-c9de-432b-bab2-93e25997914d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 115,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 481,
                "y": 704
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e4198d1e-7fc7-49ab-a3a6-37b666c51474",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 115,
                "offset": 5,
                "shift": 41,
                "w": 32,
                "x": 515,
                "y": 704
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ca391aa9-cb09-4f17-980d-54b25bcb25a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 115,
                "offset": 5,
                "shift": 37,
                "w": 30,
                "x": 780,
                "y": 587
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "de00ab59-74f0-4db3-b251-6cecfa8ce675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 115,
                "offset": 5,
                "shift": 35,
                "w": 29,
                "x": 181,
                "y": 704
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "cd8581e5-b03e-42fe-bc2f-d2eedbad7c46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 115,
                "offset": 4,
                "shift": 40,
                "w": 33,
                "x": 745,
                "y": 587
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "9ebecc9e-6082-4676-9592-7beab1326479",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 115,
                "offset": 5,
                "shift": 43,
                "w": 33,
                "x": 286,
                "y": 587
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "66e0c938-d7e9-4a48-ba78-87ab749b6bc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 115,
                "offset": 5,
                "shift": 21,
                "w": 11,
                "x": 988,
                "y": 470
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "433501aa-5c2a-4c6f-8f99-8794529c4367",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 115,
                "offset": 1,
                "shift": 29,
                "w": 23,
                "x": 2,
                "y": 587
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e5f22ef9-0668-4c17-a6be-ba22c6d8f71b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 115,
                "offset": 5,
                "shift": 41,
                "w": 36,
                "x": 27,
                "y": 587
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "6279299e-1d2d-4aa9-9482-95aada375548",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 115,
                "offset": 5,
                "shift": 34,
                "w": 28,
                "x": 65,
                "y": 587
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "bfc1295c-c258-49b1-84af-274a632fe6f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 115,
                "offset": 5,
                "shift": 54,
                "w": 44,
                "x": 95,
                "y": 587
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "ff553829-b809-4997-b449-1a4f0f338660",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 115,
                "offset": 5,
                "shift": 44,
                "w": 34,
                "x": 141,
                "y": 587
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "51c1a960-4e5d-4fc6-84d9-b1362cf50002",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 115,
                "offset": 4,
                "shift": 42,
                "w": 34,
                "x": 177,
                "y": 587
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "d103b488-72e0-47e5-bae0-423e5df04bf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 115,
                "offset": 5,
                "shift": 39,
                "w": 33,
                "x": 213,
                "y": 587
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "24af545c-702e-4f71-bc29-6bde034d27f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 115,
                "offset": 4,
                "shift": 42,
                "w": 36,
                "x": 248,
                "y": 587
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "4bac01d9-7b05-4bf6-97a3-5ac95a586249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 115,
                "offset": 5,
                "shift": 41,
                "w": 35,
                "x": 321,
                "y": 587
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "49b93ccc-f7b8-4e14-a629-7a7c2ca4c657",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 115,
                "offset": 2,
                "shift": 34,
                "w": 32,
                "x": 675,
                "y": 587
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "5de7c712-af57-4fe5-9193-a7c04d9e9459",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 115,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 358,
                "y": 587
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "09f7cba4-2cf5-4087-97c3-134104868a93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 115,
                "offset": 4,
                "shift": 41,
                "w": 33,
                "x": 392,
                "y": 587
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c89154c1-e7cf-48b1-8088-beba50a243f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 115,
                "offset": 1,
                "shift": 39,
                "w": 37,
                "x": 427,
                "y": 587
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "45ddfce2-120b-49ed-85de-b73ed8f78096",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 115,
                "offset": 1,
                "shift": 57,
                "w": 55,
                "x": 466,
                "y": 587
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "008571a6-7c49-4d24-aa18-a9d96691c5b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 115,
                "offset": 0,
                "shift": 39,
                "w": 39,
                "x": 523,
                "y": 587
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "3f8431a0-18c2-44c3-bf0c-d84e3aaf3e78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 115,
                "offset": -2,
                "shift": 36,
                "w": 40,
                "x": 564,
                "y": 587
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "34aa416d-69e3-48d2-9db0-728e895a991c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 115,
                "offset": 1,
                "shift": 34,
                "w": 31,
                "x": 606,
                "y": 587
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "06fad066-fe49-4c10-8c7a-d053a812be9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 115,
                "offset": 2,
                "shift": 29,
                "w": 23,
                "x": 639,
                "y": 587
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "7afe564d-5f5f-430f-a6c2-21abdd499fd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 115,
                "offset": 7,
                "shift": 23,
                "w": 9,
                "x": 664,
                "y": 587
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "af79ab78-7d70-4df1-8d33-1209d3980555",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 115,
                "offset": 4,
                "shift": 29,
                "w": 23,
                "x": 383,
                "y": 353
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "10d990e4-394e-4751-9449-6b80e990a403",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 115,
                "offset": 0,
                "shift": 41,
                "w": 41,
                "x": 874,
                "y": 470
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "8698460b-9ef4-45a2-b4e8-b45a9c1c9708",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 115,
                "offset": 0,
                "shift": 18,
                "w": 0,
                "x": 381,
                "y": 353
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "f00d155f-099c-4b95-a5b1-5e319f5b2245",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 115,
                "offset": 5,
                "shift": 22,
                "w": 12,
                "x": 580,
                "y": 119
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "696dcac7-5f4f-4491-8333-15fc2b05e8dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 115,
                "offset": 5,
                "shift": 41,
                "w": 32,
                "x": 879,
                "y": 2
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "33a8645c-c146-42d0-84da-c97f39fc308e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 115,
                "offset": 3,
                "shift": 41,
                "w": 35,
                "x": 913,
                "y": 2
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "3617ba57-442c-423e-86b6-fb97b5b9c1fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 115,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 950,
                "y": 2
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "a5654104-3a32-44a3-be51-1638398fc1e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 115,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 2,
                "y": 119
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "7851a286-95d2-44d1-a92e-291f3b43a5c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 115,
                "offset": 7,
                "shift": 23,
                "w": 9,
                "x": 43,
                "y": 119
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "2ec6e47c-38e8-49f2-834f-e0191b692c03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 115,
                "offset": 2,
                "shift": 39,
                "w": 34,
                "x": 54,
                "y": 119
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "9f27622e-77a8-4646-b27e-ac9e7df571d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 115,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 90,
                "y": 119
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "ffa8b21b-4fd5-495f-ae54-4b7a86d8d509",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 115,
                "offset": 2,
                "shift": 71,
                "w": 67,
                "x": 116,
                "y": 119
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "96718389-b4a0-4bd5-948e-5022590660d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 115,
                "offset": 1,
                "shift": 25,
                "w": 21,
                "x": 185,
                "y": 119
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "04f190b6-f91e-4e57-a33a-974b5dfdb92b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 115,
                "offset": 2,
                "shift": 42,
                "w": 36,
                "x": 222,
                "y": 119
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "e7efcfd5-9500-43a1-8b84-56e948fae9f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 115,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 539,
                "y": 119
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "5b8b896c-dcee-4a76-9f02-004757f3bf91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 115,
                "offset": 3,
                "shift": 28,
                "w": 22,
                "x": 260,
                "y": 119
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "a9015571-1aa7-404d-b93e-a1f4caf84bd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 115,
                "offset": 2,
                "shift": 71,
                "w": 67,
                "x": 284,
                "y": 119
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "70f56a16-de57-42e2-adac-a907ac8ecc05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 115,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 353,
                "y": 119
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "b4b05e53-05ba-4a82-befc-5b20fae80ff4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 115,
                "offset": 2,
                "shift": 27,
                "w": 22,
                "x": 375,
                "y": 119
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "a3f00bb7-98ec-4e9e-a1bd-bce5a337bde6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 115,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 399,
                "y": 119
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "1d74548b-e2bc-4154-9c2f-82c8f608a0c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 115,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 440,
                "y": 119
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "18bebce9-5821-4411-9fcf-420e0af4a57d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 115,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 463,
                "y": 119
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "93a853bc-d36a-4f62-8adf-dba52ce6aca2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 115,
                "offset": 7,
                "shift": 24,
                "w": 16,
                "x": 487,
                "y": 119
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "01150e05-d2ce-4925-a74c-723884f77fb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 115,
                "offset": 5,
                "shift": 42,
                "w": 32,
                "x": 505,
                "y": 119
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "0b8cbcd4-b601-4ce2-a0c2-791ebb3877a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 115,
                "offset": 3,
                "shift": 48,
                "w": 39,
                "x": 838,
                "y": 2
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "8557dc92-0b17-4e6f-95cf-529b5eb0cd96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 115,
                "offset": 5,
                "shift": 22,
                "w": 12,
                "x": 208,
                "y": 119
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "7f5830ff-c6b0-41f1-9300-753b6c9ad012",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 115,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 816,
                "y": 2
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "7744f422-6837-41a4-a783-1e2b9178daf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 115,
                "offset": 1,
                "shift": 19,
                "w": 15,
                "x": 395,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "027c9722-1256-421a-9181-1f55ec7891ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 115,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "75aef480-d339-4162-88d3-c1681f6d15b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 115,
                "offset": 4,
                "shift": 42,
                "w": 35,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "cc046b83-8402-4df1-b208-ebb862a19f3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 115,
                "offset": 1,
                "shift": 51,
                "w": 49,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "2ad54302-a846-4f32-98dd-3567b9dd38c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 115,
                "offset": 1,
                "shift": 53,
                "w": 51,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "e6315d93-b29f-4294-b1e7-c3343b534462",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 115,
                "offset": 1,
                "shift": 56,
                "w": 54,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "73fb250c-ee2e-4170-8153-8c702b314b4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 115,
                "offset": 2,
                "shift": 35,
                "w": 31,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "75b66e4f-d6a4-4ffb-9573-3726ec6ea66c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 115,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 275,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "b59e31d4-4007-489f-bf55-42fc6eb87857",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 115,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 315,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "e81fc5a7-a550-48fe-a660-9fec7cd3d516",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 115,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 355,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "a11ce5cb-8873-4ed9-b385-97d7038304a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 115,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 412,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "2f0c4f30-d3c1-4d28-8b80-668e6969d96d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 115,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 749,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "9d470928-bf76-4c9d-8c26-b984e5efe21c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 115,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 452,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "2f63978d-9a29-4d2f-8004-a17cb34aa05f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 115,
                "offset": 1,
                "shift": 59,
                "w": 55,
                "x": 492,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "7f4d1e0d-b817-43ff-9609-213de22f52f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 115,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 549,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "fa952948-8705-417b-830c-8f4e8e87956d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 115,
                "offset": 5,
                "shift": 37,
                "w": 30,
                "x": 583,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "dabd0719-b51a-4d6d-86f8-e8195fccc880",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 115,
                "offset": 5,
                "shift": 37,
                "w": 30,
                "x": 615,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "00887111-c83a-4044-891f-2cedca594c74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 115,
                "offset": 5,
                "shift": 37,
                "w": 30,
                "x": 647,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "8097aeaa-0a74-428c-85d5-1250e4781dd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 115,
                "offset": 5,
                "shift": 37,
                "w": 30,
                "x": 679,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "88c656b1-81a8-4edb-85c9-479e9d15f6cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 115,
                "offset": -1,
                "shift": 21,
                "w": 17,
                "x": 711,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "0dabd005-d9d8-476f-80b9-36e66db246a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 115,
                "offset": 5,
                "shift": 21,
                "w": 17,
                "x": 730,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "47ff06b5-c8a2-4f04-a35c-0414741771ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 115,
                "offset": -2,
                "shift": 21,
                "w": 25,
                "x": 789,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "d092f37e-e9c8-4353-a392-d51065248561",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 115,
                "offset": -1,
                "shift": 21,
                "w": 24,
                "x": 594,
                "y": 119
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "c987f75d-03bb-4796-8fb9-3732497a8dda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 115,
                "offset": 1,
                "shift": 42,
                "w": 37,
                "x": 370,
                "y": 236
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "88bb4158-9999-42e2-b8ab-7aea38f35600",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 115,
                "offset": 5,
                "shift": 44,
                "w": 34,
                "x": 620,
                "y": 119
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "7e3908b9-4569-407f-b489-d08996a70c91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 115,
                "offset": 4,
                "shift": 42,
                "w": 34,
                "x": 498,
                "y": 236
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "3aa555d6-95d3-4e59-a173-9e2c5bc2894d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 115,
                "offset": 4,
                "shift": 42,
                "w": 34,
                "x": 534,
                "y": 236
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "989e33db-fb9d-4f17-963b-8ef1e4856568",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 115,
                "offset": 4,
                "shift": 42,
                "w": 34,
                "x": 570,
                "y": 236
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "1e1d6d29-14b8-4c65-993b-fd0f8ca8ebca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 115,
                "offset": 4,
                "shift": 42,
                "w": 34,
                "x": 606,
                "y": 236
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "84f467ed-4c09-48a1-aae0-459429e1a908",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 115,
                "offset": 4,
                "shift": 42,
                "w": 34,
                "x": 642,
                "y": 236
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "66c15144-0647-45e5-99c7-5ae8ca02e5ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 115,
                "offset": 2,
                "shift": 41,
                "w": 37,
                "x": 678,
                "y": 236
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "9aa09f91-9f41-4ff3-bda9-018a47680c0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 115,
                "offset": 3,
                "shift": 42,
                "w": 35,
                "x": 717,
                "y": 236
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "4aa6da92-c55d-45ac-904c-8a06307b9d42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 115,
                "offset": 4,
                "shift": 41,
                "w": 33,
                "x": 754,
                "y": 236
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "22a87fbe-ba49-460f-b438-0cecd9c00d38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 115,
                "offset": 4,
                "shift": 41,
                "w": 33,
                "x": 789,
                "y": 236
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "75d79caa-21d1-40f9-ab01-6dcf50f70dd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 115,
                "offset": 4,
                "shift": 41,
                "w": 33,
                "x": 858,
                "y": 236
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "7f4e8b33-0988-4b91-88ad-8c91af5ac03c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 115,
                "offset": 4,
                "shift": 41,
                "w": 33,
                "x": 311,
                "y": 353
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "4185a956-326f-4e35-ba11-1c17ec62f310",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 115,
                "offset": -2,
                "shift": 36,
                "w": 40,
                "x": 893,
                "y": 236
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "15a5b19d-9f32-45f4-a9a2-a8719055c2a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 115,
                "offset": 5,
                "shift": 39,
                "w": 33,
                "x": 935,
                "y": 236
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "0e907a13-bb45-43fd-8d67-0c0362c7871e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 115,
                "offset": 2,
                "shift": 69,
                "w": 67,
                "x": 2,
                "y": 353
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "dbdcc65b-b143-4746-9ad4-f0fae7fe7de3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 115,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 71,
                "y": 353
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "b59410ed-c975-4896-8712-ebb9cb4cf1b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 115,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 111,
                "y": 353
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "85cd9823-b0ec-4a0f-8531-b3e36c05f24d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 115,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 151,
                "y": 353
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "a4a75cb8-a13f-4346-a5cc-b60a5c224e34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 115,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 191,
                "y": 353
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "de542e48-2287-41e7-bf20-936e831461a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 115,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 231,
                "y": 353
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "3b42697d-8642-452b-8d03-efeb85d3571a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 115,
                "offset": 1,
                "shift": 40,
                "w": 38,
                "x": 271,
                "y": 353
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "a5e92783-9daa-4cde-abe1-d12a1ac5a2ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 115,
                "offset": 1,
                "shift": 59,
                "w": 55,
                "x": 441,
                "y": 236
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "b7d3fe3a-002c-49e7-b473-fb23cabe56d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 115,
                "offset": 4,
                "shift": 39,
                "w": 32,
                "x": 824,
                "y": 236
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "5be7246d-96ab-4914-b146-c5a0c144622e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 115,
                "offset": 5,
                "shift": 37,
                "w": 30,
                "x": 409,
                "y": 236
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "f3d4ef49-5825-4695-9b44-86dcf117fefc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 115,
                "offset": 5,
                "shift": 37,
                "w": 30,
                "x": 922,
                "y": 119
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "a5ca80ca-19d0-4222-a95b-d99280274f0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 115,
                "offset": 5,
                "shift": 37,
                "w": 30,
                "x": 656,
                "y": 119
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "bd75e037-fbe4-4487-98f6-4523b54d931c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 115,
                "offset": 5,
                "shift": 37,
                "w": 30,
                "x": 688,
                "y": 119
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "244fb619-1c9c-4dba-ae59-1c25c8718cfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 115,
                "offset": -1,
                "shift": 21,
                "w": 17,
                "x": 720,
                "y": 119
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "ad8046e1-0e9a-4426-9e70-eab0d9fff7cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 115,
                "offset": 5,
                "shift": 21,
                "w": 17,
                "x": 739,
                "y": 119
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "d46e920b-b3ff-4ea5-87d3-7f99e9dd3fc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 115,
                "offset": -2,
                "shift": 21,
                "w": 25,
                "x": 758,
                "y": 119
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "38ebfb9d-be2b-4d13-9c43-8eb6279294a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 115,
                "offset": -1,
                "shift": 21,
                "w": 24,
                "x": 785,
                "y": 119
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "3be83b60-ea76-4bed-b18a-191764b8c49a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 115,
                "offset": 1,
                "shift": 42,
                "w": 37,
                "x": 811,
                "y": 119
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "ad40f82b-93d7-430f-83fa-7020a2bfb829",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 115,
                "offset": 5,
                "shift": 44,
                "w": 34,
                "x": 850,
                "y": 119
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "f7550648-f9a1-4ecf-8150-f0f5ee4065fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 115,
                "offset": 4,
                "shift": 42,
                "w": 34,
                "x": 886,
                "y": 119
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "780f78a6-f0cb-4c0c-946e-aef5e404a833",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 115,
                "offset": 4,
                "shift": 42,
                "w": 34,
                "x": 954,
                "y": 119
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "ad43f658-67dc-43d1-934b-c3c56da8643a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 115,
                "offset": 4,
                "shift": 42,
                "w": 34,
                "x": 334,
                "y": 236
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "fa7dd9c6-1c9a-4de2-b043-3dbae8499752",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 115,
                "offset": 4,
                "shift": 42,
                "w": 34,
                "x": 2,
                "y": 236
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "682a30b1-9a82-4826-9bc1-20bd8ed739d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 115,
                "offset": 4,
                "shift": 42,
                "w": 34,
                "x": 38,
                "y": 236
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "c740bed2-4fa3-4e86-9dd2-5f2655b28465",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 115,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 74,
                "y": 236
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "a4e1bd46-db09-4ff5-9481-472457b5fbc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 115,
                "offset": 3,
                "shift": 42,
                "w": 35,
                "x": 115,
                "y": 236
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "4c927364-07ba-44ec-b60e-6bfc7eee0fc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 115,
                "offset": 4,
                "shift": 41,
                "w": 33,
                "x": 152,
                "y": 236
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "028d0205-ece3-467c-9266-eed2e014c3fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 115,
                "offset": 4,
                "shift": 41,
                "w": 33,
                "x": 187,
                "y": 236
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "96b706df-26c8-4b0a-b69e-3f0b310b3400",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 115,
                "offset": 4,
                "shift": 41,
                "w": 33,
                "x": 222,
                "y": 236
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "8020253a-1d6d-40e9-818b-01410df41e7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 115,
                "offset": 4,
                "shift": 41,
                "w": 33,
                "x": 257,
                "y": 236
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "dd47e95c-35dd-4079-8179-28929033539d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 115,
                "offset": -2,
                "shift": 36,
                "w": 40,
                "x": 292,
                "y": 236
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "b4fa6765-dc4e-4b15-ad78-a7f2c93983ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 115,
                "offset": 5,
                "shift": 39,
                "w": 33,
                "x": 346,
                "y": 353
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "a351cd6d-619b-4d29-bb20-2794d762d25f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 115,
                "offset": -2,
                "shift": 36,
                "w": 40,
                "x": 570,
                "y": 704
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 72,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}