{
    "id": "8cf8ac5a-7236-42aa-b7ea-26d3bb7cad96",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_roboto_12pt",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Roboto",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c39eac5e-c3ea-43b9-b0de-641e32be6fb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 143,
                "y": 65
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2cbbfdfa-5696-44b3-aa5d-a9346b67cc78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 159,
                "y": 65
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "6a7f386b-fa47-4188-ad42-fafaaa5feb8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 111,
                "y": 65
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "31156f95-1e8d-4599-88d4-300edfdee4dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 14,
                "y": 23
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "4e96cdbe-d895-4cfc-8220-c0d1d9f9a612",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 137,
                "y": 23
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "cd741bca-8a88-421a-bc55-dca7e3b93b79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 76,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "bdfb6899-2e69-4e43-8fdc-39b470a1fe6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 26,
                "y": 23
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "65849bc7-f840-4df7-b80f-b26093612119",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 164,
                "y": 65
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "72ae23f1-addc-4e6e-be0b-561b79201740",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 118,
                "y": 65
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f0c9c3e4-539a-4ba1-a21a-b4b31a341772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 90,
                "y": 65
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a2e04b9f-bdd9-4b91-917d-688680694620",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 65
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "f982dd36-f68c-4c9f-abf2-753e82d4a14a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 181,
                "y": 23
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "922750f6-1f45-47c7-bd60-bfd719edab53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 169,
                "y": 65
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "95499545-8de9-4fdf-9a23-e0d3c8a3ca53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 83,
                "y": 65
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "b6c7e3b7-b545-4345-bef6-eb1c61387816",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 179,
                "y": 65
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "40d2cb90-9c12-432b-8b2d-86942081228d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 236,
                "y": 44
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "538ead72-eb45-42c2-a3e3-0895f5a3c73a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 44
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "17f0f84d-5b63-4f01-acd3-e244f8a375d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 76,
                "y": 65
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f14b8300-f4d3-42c1-bd63-7650ca7c967d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c5c7b1be-a94c-4dbe-8a13-bcd83b1acba2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 126,
                "y": 23
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "d9d9c7d2-5570-45fc-b872-fc4f6dd92106",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 192,
                "y": 23
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "829d7c80-2333-419e-aeb9-07f2ea53406c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 66,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "f3b26f4c-6870-441c-ba2f-6abc3c87d552",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 203,
                "y": 23
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "467d8e69-d097-4df9-beb2-012593601af3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 214,
                "y": 23
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "947ef035-619b-4d51-8c07-13294a72b249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 225,
                "y": 23
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "5476983f-38dc-44be-8e52-1880c0dbbd1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 236,
                "y": 23
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "37ee2534-39a7-4e12-9aeb-3a9bbf724abf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 154,
                "y": 65
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "0d5ae418-ac60-47a6-a950-2752b532645c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 125,
                "y": 65
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "31d2c587-aa51-4684-a7f0-979415fdba60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 136,
                "y": 44
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "71551d4b-ad17-4270-992f-ae4f9456d3fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 20,
                "y": 65
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ba9cd30e-e475-4d9e-ac6e-fa4d64cce965",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 245,
                "y": 44
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "fb0d91b4-76ab-49fb-968e-1ef29739ad1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 216,
                "y": 44
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "3e9721e6-0e99-4ade-b06f-fae4b8b3f1a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b470e302-168d-4e64-8d33-c7ab6a2013c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "3a4e8de0-5b3a-4a4f-b7b7-9ad778a1a899",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 35,
                "y": 44
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "6eaae96f-5b36-4fd8-b8cf-295efc34c2a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "9068e85c-4c3d-4d6b-a338-776f2b527b32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 24,
                "y": 44
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6b8789b1-c022-47ae-8c14-ec6fe4b8b4f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 46,
                "y": 44
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ec8b342f-5022-4788-bcab-9a8ca5c2556c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 166,
                "y": 44
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "82713a1c-f7ab-44a8-8182-d54abd2a9076",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4348565f-4d08-4b89-bbd8-dd73b07a7e07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "4985fe3a-5b86-48de-b5e0-57145abac2bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 174,
                "y": 65
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "3bbfc612-2886-4525-9941-673448fe7b35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 196,
                "y": 44
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "cfd62108-6b74-45b8-8d5a-b5edabf6e54b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a3267d1e-35cd-427d-a4ee-325e97c0bf54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 206,
                "y": 44
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "857b26b8-9359-4e59-8c4b-1c40c0e7ba48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "960cef86-7e4f-4c5a-b3b6-17472be338d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 165,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "8686638e-8b6c-4ebb-b165-3698743167a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "0e816529-45fa-47c3-9eac-fb5a6f41f274",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 170,
                "y": 23
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c3d0124e-dde2-4d98-98d1-fb44e747d19a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "685a61a7-a56c-4000-8c7f-b86a4487f0e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 159,
                "y": 23
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "a3fdc091-2338-4bb3-a9a8-7fb06822a997",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f3f58ee8-6fe6-4eac-826d-c4173efecedd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "10932b75-b7b1-4b5d-8441-6dcf8e62b058",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 148,
                "y": 23
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "302adc5f-0cfa-4e6d-9e09-b61ff5ab83d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 116,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "f1511926-a381-4fd4-87a2-44198cf65111",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 18,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ee0c5308-674f-41a0-a122-8b206825afbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "42b5616f-c25f-4575-a76b-a8f88e17354f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ec77d374-b284-4a51-925e-36f7b2523d28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 115,
                "y": 23
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "09b14215-d0de-46ac-8311-c5e3710446da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 137,
                "y": 65
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "65635fce-75a9-4aee-aedb-37033ac4903e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 65
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "acceefbb-7f2c-4a97-8083-6062638808b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 131,
                "y": 65
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "2f736126-45ff-4b45-9922-e2de22c89911",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "23939fd9-81b1-4db4-8bb9-d13a8bd67c19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 76,
                "y": 44
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "22678b97-5ed4-45cb-8251-f807580d12c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 69,
                "y": 65
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "d7e5a563-a63b-41d5-b85a-98efa4778b24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 104,
                "y": 23
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "896accec-9786-490b-9c5d-26f8259fcb3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 126,
                "y": 44
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "854955a3-ca18-465a-b740-d8be70859f1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 86,
                "y": 44
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ab0e5c53-4de6-4e9e-bae5-2990685830e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 93,
                "y": 23
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "37765908-7802-4b6b-aea6-af78b9381502",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 96,
                "y": 44
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a60b3a05-8c98-48f0-a046-4829e4f8a9aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 38,
                "y": 65
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c1194f90-3719-4835-8af3-dc760628921e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 82,
                "y": 23
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "3b8a6fed-69f0-4c0b-8700-db0ab3328a95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 71,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "0aaeb726-197b-4003-a4fe-a01c6d3b0504",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 149,
                "y": 65
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "a36dd9fd-4249-4bbd-ba72-e58ad1a94a29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 97,
                "y": 65
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3b4b6f16-29b5-42c4-9c50-0acbd9f006bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 116,
                "y": 44
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "7a8130b5-7017-4c70-9c97-64dea46e304a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 184,
                "y": 65
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c102dea7-2d77-4ff0-9d92-e45fcc5025bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "60b75da8-613b-4e4a-a3b4-83090cbd7725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 60,
                "y": 23
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "bb0bcfc5-83e4-4b34-95fb-2cd9738b337d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 49,
                "y": 23
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e3f764ff-43bf-4cd7-964a-e2211a966b04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 56,
                "y": 44
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "8cd8bbdd-9a12-49cd-beb2-711815b52926",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 156,
                "y": 44
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "23d65fc6-d408-4dfe-9418-8505ee366770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 104,
                "y": 65
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b91ae1c1-a30e-4ccc-8285-d9f6607f141d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 106,
                "y": 44
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "90466f7e-24b1-4d20-b5b7-451f974bc249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 54,
                "y": 65
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "99bd602c-f21d-46f1-bc8c-3114b9d4cdf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 38,
                "y": 23
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "8168e974-3bcc-48d0-b15b-0840dab3e615",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 146,
                "y": 44
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "78370005-3d21-411a-96e6-1e246f7a0243",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "15d80ec1-45b9-4609-9027-8093cb5bb0c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 176,
                "y": 44
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2823f7fe-7560-437d-b5bd-f07819d1f113",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 226,
                "y": 44
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "58e51802-50c1-49cd-b919-c86e3b6a5979",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 186,
                "y": 44
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "0362570e-5c1b-412d-9523-02265ccf2c6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 46,
                "y": 65
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "4a250746-7fa8-458e-abd1-074164b5b7b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 189,
                "y": 65
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "58cc8061-365b-44cf-88e0-54c3c26ea67b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 62,
                "y": 65
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7caf0ac1-acf1-470b-900e-cf018906f5c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 23
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "489e811b-f2fa-4d29-a723-2e0e647430ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 65
        },
        {
            "id": "992a8e7b-0ced-423f-8326-fa8eb2644f65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 192
        },
        {
            "id": "b3fab352-6ec6-4ddf-91d6-3f2495ccca2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 193
        },
        {
            "id": "3d6fec38-dd7a-4b3f-b527-400739b196e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 194
        },
        {
            "id": "2e952c04-b908-4a6f-922b-fe2128a71382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 195
        },
        {
            "id": "f88efd5e-5df9-4279-ab93-2d3e3a95224b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 196
        },
        {
            "id": "3006d339-5376-49ab-8b2a-ff200b94363d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 197
        },
        {
            "id": "bd123fa9-8f19-482b-9693-03533cae0bc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 916
        },
        {
            "id": "c29d2187-ba46-42f6-b0be-68422c4f85e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 8710
        },
        {
            "id": "d302495d-2ad5-4ea9-b5e2-f61be3237c01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 65
        },
        {
            "id": "89b81fc7-903a-44a6-ade2-51fc43399200",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 192
        },
        {
            "id": "608684dc-f093-4817-a1fd-bb80e51c96eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 193
        },
        {
            "id": "a091ea7f-c954-4854-9699-447b2ecd6d4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 194
        },
        {
            "id": "7abf4e6e-0843-4e07-85c4-715c5c1cb23b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 195
        },
        {
            "id": "aa30dd0d-7497-46ea-8277-378ead8d43f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 196
        },
        {
            "id": "458db387-51cb-48b9-b9cc-455781f016d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 197
        },
        {
            "id": "07d3ff29-0dcc-4e7e-8d93-dd756ff9bbbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 916
        },
        {
            "id": "f0037fe7-dfeb-416b-881e-f955e9dc2f79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 8710
        },
        {
            "id": "dedc021a-c5a7-44d1-8019-e71831fde501",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 34
        },
        {
            "id": "33387718-1d6e-486f-abbf-1cb0ef604f2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 39
        },
        {
            "id": "ae8b146f-c0a2-41f0-aa36-991ec7260d5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "af9d70d9-819d-419f-81e2-12964335213d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 376
        },
        {
            "id": "90806049-af26-443d-b80e-0d686335594a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8216
        },
        {
            "id": "a5f466bb-894a-4d9d-91b5-0f5b267cf669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "e41e2a85-bb0a-4e8d-86a1-5da003c9e36d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8220
        },
        {
            "id": "d4033759-2f44-48a3-9c8e-ed869aeebc05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "a16b2d7d-1e0f-4273-8404-f26e587fae04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 44
        },
        {
            "id": "c9cedf5e-cde9-4d01-b51e-1b719f614083",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 46
        },
        {
            "id": "606fba56-3b53-405d-a1eb-cc3a6b73fc18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 58
        },
        {
            "id": "7659c92d-4df4-482d-8774-78d1259eaa3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 8218
        },
        {
            "id": "c6096d85-ba6b-479b-92f2-6b5cb249db43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 8222
        },
        {
            "id": "9048beb2-f812-451e-96bb-2b4418b6a305",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 8230
        },
        {
            "id": "6f66c8db-f06b-4ffa-bd8d-c2743c34cb8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "1afb2e7f-b4aa-4fe9-b8be-abfab2045396",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "f71185dd-35c2-4135-aca1-1f0080be8eeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 58
        },
        {
            "id": "b1ba74e2-d272-44c1-8087-5ae6ee9d4b08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8218
        },
        {
            "id": "61377008-5888-4ef3-90ae-1e07d3ff0e4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8222
        },
        {
            "id": "59aa49b1-7f6e-43da-adc6-5e317f93cfd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8230
        },
        {
            "id": "f6d920cf-98c4-4135-bfdb-107a2a6fc5a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 45
        },
        {
            "id": "49789781-1956-487e-aa63-40bd1c519ab5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8211
        },
        {
            "id": "5a408dc2-093f-47be-b327-b23bbcbdbfba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 8212
        },
        {
            "id": "394c3423-4612-4f34-b01e-cab4e5d91105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 34
        },
        {
            "id": "2ac184dd-85f9-4347-8332-8e52cde16559",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 39
        },
        {
            "id": "af3350c0-7d10-49ce-bc38-0a99eba0fbbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "5a29112a-5bd4-4d93-90ca-e39df0683094",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "8c19240b-2f6b-4501-96a8-e8767eff8d17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "3b7e7f7f-87c5-4f41-a9fe-a659fa0e3557",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "24e626d7-37ad-47d4-813c-e199335e8d4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "1eb34140-942e-4bc5-b05c-95dc76a263b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 255
        },
        {
            "id": "62e16413-a4ee-4623-b1ca-a08d503082c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 376
        },
        {
            "id": "8258bc7a-0ae3-4ba1-8463-c5bcb6312e37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8216
        },
        {
            "id": "65a97480-53ed-4b18-8057-8c1afee0eb05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "1d169783-821b-41e7-bd23-e8f3757bead6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8220
        },
        {
            "id": "a6d892ba-ea48-4a84-8dd6-f7cdaf6c1658",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        },
        {
            "id": "8d4d7d7a-6274-48cc-aabf-34b65fcc5452",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 44
        },
        {
            "id": "5e5192a2-ca85-41e9-ab49-184fd623a5b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "e8f83655-5288-4da4-a292-799f08ddd64e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 58
        },
        {
            "id": "d3dba1d8-d796-4794-9515-258339c24c26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8218
        },
        {
            "id": "e65248a2-58e2-49e3-ab6f-19876bb9b5b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8222
        },
        {
            "id": "d4b828d6-3167-4f0f-ab73-51ffd8b3b1c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8230
        },
        {
            "id": "178dbca7-c0dd-4ab8-a159-026ce9274b72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "a57d154a-144b-496b-860d-987348ffaf2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "caa89a3f-eee8-408b-bfb7-9f7fdef021e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 58
        },
        {
            "id": "dab81249-7454-49b1-847b-b1e23dadad52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "d07d2545-09d2-41cd-ad7a-086e522af55a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 192
        },
        {
            "id": "6589bdb3-18ff-4514-8e6f-fb0dacbc571b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 193
        },
        {
            "id": "7966ed26-fb65-40f0-87f2-804527909f6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 194
        },
        {
            "id": "4765e456-9a51-474c-853b-ac2fd8a296de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 195
        },
        {
            "id": "b05e7a5e-d766-45d7-94db-f0492018ef0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 196
        },
        {
            "id": "1f5eaf16-665d-42fc-add3-1de58572d7ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 197
        },
        {
            "id": "08b21783-e25b-4752-87d1-538845d7b461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 198
        },
        {
            "id": "b8d5ebbe-e2e9-4a1a-b51a-629f984835e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 916
        },
        {
            "id": "8c4e864e-fddb-4767-ac53-0fb2c6984ffc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 8218
        },
        {
            "id": "9234bbe0-fa50-4edb-97c9-21e59cafd9b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 8222
        },
        {
            "id": "61ea101e-5a76-42eb-8b99-532e3de35f49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 8230
        },
        {
            "id": "7609450a-b81e-46e1-8fdc-6e7c1d83b862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 8710
        },
        {
            "id": "ccca25a3-c1b0-434f-8df1-121f8ad738f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "ec0c35e3-00a0-4bc5-bfc5-ab7ecc9139e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "00ff75b0-f025-4a40-84d4-824f179c9a63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "4f4f4267-fbae-45e9-8473-ba3fbbafca73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "27506983-7b45-4222-beae-deef223372d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "85e0e152-4e0f-46cb-8b72-8f794403ba71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 171
        },
        {
            "id": "fa62d595-9c5f-402e-af50-f0ac23aa8f81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 187
        },
        {
            "id": "18bc1e87-78bc-4e9a-a4f9-ef3769cafb00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 192
        },
        {
            "id": "a14bd363-4773-4c2b-8af2-a4b687681080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 193
        },
        {
            "id": "8fae2304-a7db-4956-b7ec-05b5e19eed14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 194
        },
        {
            "id": "ca12cd3d-6161-48a9-b849-619a36f441e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 195
        },
        {
            "id": "1dab9175-3ba0-4a23-a480-daef9b4222f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 196
        },
        {
            "id": "868162e3-93ff-47ac-9dc1-11351029f1df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 197
        },
        {
            "id": "88127bb1-f4a5-41a8-88e7-cc052695cc5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 198
        },
        {
            "id": "4a426885-f605-4fd7-a116-aa38a09f4d5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 916
        },
        {
            "id": "a168e479-6631-487e-aefb-604420448844",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 960
        },
        {
            "id": "af67c5ff-e8b7-4a31-8154-f60cec70657e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8211
        },
        {
            "id": "e698860c-48b9-49cf-a5b7-fcd58f3bf5ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8212
        },
        {
            "id": "d9b97250-0a5f-47a0-95ec-3b4118a8d5fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8218
        },
        {
            "id": "3ff2ea61-2635-4807-8d38-05e9809dd4f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8222
        },
        {
            "id": "74017a67-683b-4470-ae10-4012b3e2d9ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8230
        },
        {
            "id": "e4e2fdb4-29fd-4a13-bbd2-fc92dd9c95f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8710
        },
        {
            "id": "545e2ed9-63e7-43cd-ada4-e52a5bead447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "d8eff69d-797e-475b-a2f2-8c3b878d74cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "f9d05b1f-c818-40f7-9d4f-d883238f5997",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "12884765-077b-4d3f-812c-c2323a75fccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "81151eaf-17e7-4ca4-9d0b-068941d32117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8211
        },
        {
            "id": "83ba0d27-b35d-49dd-8853-e6bb7a0c1f55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8212
        },
        {
            "id": "6840454d-ea57-4649-b79f-15cef7099764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8218
        },
        {
            "id": "63c01b6a-7fe6-44c2-a92d-9f43e0c12af0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8222
        },
        {
            "id": "bbe1b722-3b1f-41d2-9eb9-f0fa17f52d91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8230
        },
        {
            "id": "ee4d7ae7-6463-4426-8c05-737b8c797803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "147c076a-d808-4db9-ae4b-4cd5033b11d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "9041020b-32cc-4e77-912b-70ef050af209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "5f1e9f2f-d4b7-4878-ba79-cd3b90eb6e77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8218
        },
        {
            "id": "312e3d78-d751-4501-99fc-8fdeadb97049",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8222
        },
        {
            "id": "4e7f65f9-27d5-46f3-948e-dc3295134c92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8230
        },
        {
            "id": "187183b9-c0ec-49e4-8b76-8f292bdf00c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 45
        },
        {
            "id": "de19845c-33e3-48c2-91e5-975e92e55dd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8211
        },
        {
            "id": "297dd76f-dc67-431b-8858-8d12ecc54486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8212
        },
        {
            "id": "2126a28d-a518-44d5-b232-c762d887857d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "bb8147e0-ba70-46e1-b839-f68be6fceba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "ce5f963e-60e1-4c27-a89e-099565792223",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "116c7a76-776c-42a9-a42e-3a65a4b3dbb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "55562d0a-ee3e-446e-9fea-3be030cd1516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8211
        },
        {
            "id": "ceb62386-77ed-40be-b03b-56f7d636940e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8212
        },
        {
            "id": "ab47be5c-c7f0-4d4f-af34-94f1e8c26369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8218
        },
        {
            "id": "eb7c1efa-5966-49f6-a6e0-0d9728c2d2bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8222
        },
        {
            "id": "143d246d-491e-4ddf-91ff-c0fb7350f188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8230
        },
        {
            "id": "4269c857-06e7-4e94-8560-eec550c3613b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "abd98052-cec3-45ec-ab4e-6ec479702bcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "00d2871f-3ca1-493c-9bba-a20a5506589b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 58
        },
        {
            "id": "9a6ed5e9-5309-4bd0-9fb4-352cc5563072",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 8218
        },
        {
            "id": "a62ed20b-0079-4cf6-9776-b46575045047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 8222
        },
        {
            "id": "15ba9d41-a147-4d8f-ace6-63cb1fb9dce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 8230
        },
        {
            "id": "40582218-da71-4de2-8d32-f50a8cc33fb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "f263c0d9-98a9-48c7-8151-735f6959e9d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "41ecb137-b5ae-4bb6-b739-fa6fa992113b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 58
        },
        {
            "id": "145d293e-8394-4bfe-b02f-74eeaf6f7cfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 8218
        },
        {
            "id": "86fcb1d8-2488-4474-b7e5-931cf7d1bfb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 8222
        },
        {
            "id": "f99525e7-400e-4a0b-8079-f54131fa442d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 8230
        },
        {
            "id": "0828d160-247c-4bc2-a1cc-d4997b4154d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "9875153f-1121-40b1-87e7-1778e53e8338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "54fe0c6a-b7b0-4720-8519-ebc84e5bbdcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 58
        },
        {
            "id": "f1e9a8ec-9e81-49a2-a16f-31462700c7a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8218
        },
        {
            "id": "27b45854-b3c3-4470-a699-91b228c634b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8222
        },
        {
            "id": "e92aaeed-d660-4438-b87d-9555f56995b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8230
        },
        {
            "id": "7931e5b4-8cd1-4a38-bbbb-ef28db456c91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "8ee52751-08e4-42f9-88e8-8cf6f9fe7ad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "92df087a-ff44-4571-b9b0-ddf8b277f057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 58
        },
        {
            "id": "310527cd-cdc1-4358-8acd-e3945b0637df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 8218
        },
        {
            "id": "8741baaf-ffa7-4090-bb3f-92c4ea909d32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 8222
        },
        {
            "id": "c24644a5-bf3c-431f-93c1-00788830a49f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 8230
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}