{
    "id": "1ef66a9a-1e28-4915-abbe-3372aaad9cd7",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_bebas_kai_24_pt",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bebas Kai",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "0aecc96a-6bf1-4794-85db-978eb0bccd01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 38,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "31b17f52-0c4f-4562-9680-28167b3368d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 38,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 144,
                "y": 122
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "35495bfd-a839-480c-9d80-8456c63b15c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 151,
                "y": 122
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a98aef09-b538-406d-a2a2-25849abab41b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 38,
                "offset": 0,
                "shift": 16,
                "w": 15,
                "x": 163,
                "y": 122
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "5e7b4650-bb34-4a93-af66-a03ee61e0a26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 180,
                "y": 122
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2e279f39-1023-483d-8779-f3feae1555c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 38,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 194,
                "y": 122
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "86eb7151-1bbb-4c9b-bd82-b3f4cedf8927",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 15,
                "x": 214,
                "y": 122
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f37b7657-4a68-4421-a7c6-b8b55f083f4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 38,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 231,
                "y": 122
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "7e696b76-2620-482b-a559-dbe1a91ed87b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 38,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 238,
                "y": 122
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "08705b64-ad32-4063-ae61-6767229feb65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 38,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 248,
                "y": 122
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e04cd36c-0862-46dd-9d85-64c100b9e2a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 272,
                "y": 122
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "404ec48e-c4c4-49be-9915-b3f872de481a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 38,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 391,
                "y": 122
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "e2471c72-51ac-4e84-97ed-e815b6874ef9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 38,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 285,
                "y": 122
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b8aa324a-a6ce-4544-9450-75c8bcaed7f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 38,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 292,
                "y": 122
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "168e5006-4696-4f0c-9c04-e59438e810fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 38,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 302,
                "y": 122
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6685079d-8f56-4c88-8cfb-e57056fd2e20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 38,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 309,
                "y": 122
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "64f3c8b3-4eb6-44af-93ca-80b1c8aead32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 323,
                "y": 122
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "eb320e11-9721-4345-9394-702946cd0058",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 337,
                "y": 122
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "6dbc5026-aecb-49d8-8d3e-edefd4a0452a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 38,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 348,
                "y": 122
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "fb6a8fa3-ce1e-4ae2-bd3e-851f98d87083",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 362,
                "y": 122
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "01e9018d-fd43-4653-b1d3-009053a3faf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 376,
                "y": 122
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e997fa74-f976-44dc-8201-b1bf7e98c2f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 131,
                "y": 122
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "20747d6c-d637-4353-9b3d-747bf29ec011",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 258,
                "y": 122
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8c552eb9-2102-4656-b787-eaa8caa3f744",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 118,
                "y": 122
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4b3f8793-820d-492c-a574-85e82eb32d03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 443,
                "y": 82
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "14ec15ec-a868-4672-8874-c1af594973eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 314,
                "y": 82
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "6392863d-af30-485f-9593-6ea468bf1ed5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 38,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 328,
                "y": 82
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "847da108-f98b-4aa4-8ee6-05907b72160d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 38,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 335,
                "y": 82
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "46121cf8-f079-4de0-b06b-afc846885b1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 38,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 342,
                "y": 82
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c59846cf-3c8f-4b49-a166-a81490599047",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 38,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 358,
                "y": 82
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6c8d6443-d2c4-4609-91be-3b48e508ab80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 38,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 374,
                "y": 82
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "22b32d5b-67b4-40d9-a5df-66f92a80d3c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 38,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 390,
                "y": 82
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d8f98269-169a-4d13-9b14-7f7aaffed8fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 38,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 403,
                "y": 82
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9d71e70a-7c2f-4b4e-b83c-48745cbb50c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 428,
                "y": 82
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "32478b09-f2bd-45f2-b9db-f5edbcf81113",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 458,
                "y": 82
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "9de50323-c066-4914-aa6a-a83af953c028",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 87,
                "y": 122
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "fa18a041-07d5-4c33-804c-d2fd00ea94e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 472,
                "y": 82
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "947534e3-052a-4466-9214-ab734e6d5182",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 486,
                "y": 82
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "0646ca2d-0948-457c-865a-24d70701b518",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 122
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "294dd801-d5c5-4036-8343-cf04308ce37d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 15,
                "y": 122
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "da7c0817-1281-4f8d-af25-8f4f982fbb4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 29,
                "y": 122
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "b2dc529a-e0de-4719-bab6-f5d7ea89c212",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 38,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 43,
                "y": 122
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "372b3a55-0a05-4b9f-9cc6-14f0eb22a5a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 38,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 50,
                "y": 122
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "3aec4cde-e965-4973-98ae-16f30061ff7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 60,
                "y": 122
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "376a8f9a-1e3c-48da-8f37-08123ae89ba1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 38,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 75,
                "y": 122
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "282c26e7-ab9c-4df9-9027-8f7cf62c9aaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 100,
                "y": 122
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a923c569-4985-4841-8e08-4dc16daccba0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 423,
                "y": 122
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "427d364a-d716-46ad-89d0-b710ea9ba874",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 244,
                "y": 162
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "bd5383b2-75bb-4070-b7b1-4d15ce9402da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 437,
                "y": 122
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "5bf84fd6-0058-4e60-8930-260145991345",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 285,
                "y": 162
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "cc54bd8a-15dd-4ff8-8f1f-313c9cb86a4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 300,
                "y": 162
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "83992ed4-c7ef-45b9-aa39-7d1e8af844fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 315,
                "y": 162
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "3c9ac7fc-6216-48da-8f8b-a2e71cd05fa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 329,
                "y": 162
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "af23a159-78f8-411c-ad8f-25948c9bb5eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 342,
                "y": 162
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "bf9ae725-bf34-4085-a552-a29d1ee08f03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 356,
                "y": 162
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "cfa3e2cd-48cd-45db-9512-0d47edcfe94d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 38,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 371,
                "y": 162
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "22e139f1-9303-4210-8355-d7b69bde0e6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 392,
                "y": 162
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "90c12225-a227-4572-9766-a8603c08c8df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 38,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 407,
                "y": 162
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "19228737-ed1a-4e8d-9e55-5e37dfa52f0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 436,
                "y": 162
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "cb366461-9919-4d49-8cc7-08151eca2139",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 38,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 66,
                "y": 202
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "a314d1f8-63bd-4549-9b21-7ff4e66e41a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 38,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 449,
                "y": 162
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "57cb2f99-d887-4b92-a526-8a07165c8e86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 38,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 463,
                "y": 162
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "593e596b-bc13-48cd-822d-ee8b278093a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 38,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 472,
                "y": 162
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "23d693ea-de9e-4876-a4e5-c50e09d81a70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 38,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 488,
                "y": 162
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ae2f4d3d-fac4-4556-b081-69c2fa6f075c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 38,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 202
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8f6835c3-d4d0-467e-8a9c-14449a5539fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 10,
                "y": 202
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "4ef2daf8-b407-439b-a73f-8963918b1f13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 25,
                "y": 202
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2ac8b60a-35dd-4083-86ab-14fe72078454",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 39,
                "y": 202
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "a2567386-c214-439c-a652-f4260675a540",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 52,
                "y": 202
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "042ba2f4-c1cc-4401-83ad-ce4a7549aee9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 272,
                "y": 162
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "104a4c5c-f7d3-406f-8df6-f3ba9a74378e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 423,
                "y": 162
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f1ed3e4a-5cf4-4075-9cdb-29426a2851b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 258,
                "y": 162
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "41f54494-19ec-4d7e-999c-38d439956905",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 77,
                "y": 162
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4eb93273-921b-43a7-bcc0-01a2dca29405",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 38,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 451,
                "y": 122
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "82dcf12c-406b-4a84-896d-2f5c003c903b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 38,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 458,
                "y": 122
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a3bc7a60-1790-427d-923f-1262b6c092b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 468,
                "y": 122
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d0e9481c-9a1f-4b9c-9221-ef250cf1b4fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 38,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 483,
                "y": 122
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a0fd9d90-8feb-491c-96bd-ca39848577fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 38,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 2,
                "y": 162
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a3130aaf-95ae-4617-86f9-b3f949a3bc66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 20,
                "y": 162
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "997a8fbb-1420-456f-83fa-debafb8aad45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 34,
                "y": 162
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "4a8c9f35-9bbf-4357-9698-f3fa8bda2923",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 48,
                "y": 162
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "453f930d-c971-4c76-9ccc-09416d435479",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 62,
                "y": 162
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e7bb1f33-043c-462a-8d5b-d229830ad633",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 91,
                "y": 162
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c7fc89c0-459e-4a75-8741-fb9a6d9be48a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 230,
                "y": 162
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "0fc4daee-7d4f-48f5-a970-7452dab99476",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 106,
                "y": 162
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "872db1b1-d1ff-4be4-8db3-7b3ff5d4143a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 119,
                "y": 162
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0f9151ea-b0ee-4a47-8f57-a09a08d783fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 133,
                "y": 162
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "0f2dfe50-c5ef-4bed-b668-fd14a6541baa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 38,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 148,
                "y": 162
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f61b4dc9-b355-4c05-a5c8-bc1753445f80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 169,
                "y": 162
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "3a8c9194-3d9d-438c-8e3f-f70159313e97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 38,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 184,
                "y": 162
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f48d1929-1c50-44e1-9040-c4414811d930",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 200,
                "y": 162
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "a205e4df-08a0-4784-a6be-a75ce6beda36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 38,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 213,
                "y": 162
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "98f120eb-0dc2-4cea-9a1e-d1c8c82d5094",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 38,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 224,
                "y": 162
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "668aa112-151b-45b0-8b4e-6ae6af7fd81d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 38,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 303,
                "y": 82
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c428a5af-bd58-4c4d-831f-693babde0e17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 38,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 407,
                "y": 122
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "e5c8e2d4-52e8-47c3-8fb9-13e87dbd499e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 38,
                "offset": 0,
                "shift": 6,
                "w": 0,
                "x": 301,
                "y": 82
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "c24a1a90-d70f-41d4-90ef-aa967939fa94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 38,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 121,
                "y": 42
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "928e86b2-a9e7-4d35-aa33-26295f82c1a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 344,
                "y": 2
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "fcf36ae7-047b-4365-8350-51273a488e0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 358,
                "y": 2
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "f39ca472-00c9-429b-b8e8-06bc2b6b8725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 38,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 372,
                "y": 2
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "60412f83-860b-4540-8895-58cc3a35cbc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 38,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 388,
                "y": 2
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "a99b0310-d043-4765-9f87-7050e113d167",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 38,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 404,
                "y": 2
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "3b7b628c-0b96-46e9-8c10-2b8cf07baae0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 410,
                "y": 2
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "95b15c51-db5b-4970-8af2-d5a1432e0d89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 38,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 424,
                "y": 2
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "647110db-35cb-4f32-a68a-e0693d97a29f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 38,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 434,
                "y": 2
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "b9cb8d0b-77cb-402c-a6a9-40b84686934b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 38,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 459,
                "y": 2
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "631fbb50-470d-4a01-8048-736f84109f9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 476,
                "y": 2
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "a9cd7f48-18c0-4cb0-85ee-916f0fcccdca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 38,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 105,
                "y": 42
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "c252c57c-b277-4183-8871-2ec948c8b2ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 38,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 490,
                "y": 2
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "4975469c-db7b-4817-8e09-f2dcc8572544",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 38,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "d347c9d1-64fe-43f7-b2f4-da98a3736bee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 38,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 27,
                "y": 42
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "c5d9d5b1-b70d-48b3-a167-ab81b9666904",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 38,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 37,
                "y": 42
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "7af6e1b9-9cf9-4433-bef1-a3afc139656b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 38,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 47,
                "y": 42
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "8da3ea35-e24d-47c6-9a09-f94ab2cfbb1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 38,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 63,
                "y": 42
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "b19402d4-7460-4e59-93ba-aa0da529b79f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 38,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 73,
                "y": 42
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "483dba6b-093a-42cd-b139-3685271ba580",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 38,
                "offset": 2,
                "shift": 8,
                "w": 6,
                "x": 83,
                "y": 42
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "669bdfec-f3da-4e96-95d4-519fe87f6d3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 91,
                "y": 42
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "9a75d460-031d-4d2f-953b-7be7fabab3e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 38,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 329,
                "y": 2
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "47a07478-9f18-4b12-932f-2c6adc9a3921",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 38,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 469,
                "y": 2
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "3cabda4d-65dd-4e6b-bf54-38fe348b5d32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 38,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 319,
                "y": 2
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "bcfc9b80-3545-44ac-8cad-1b544e1af69c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 38,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "8288972d-fdcd-43e9-adc8-88807ec5aa9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 38,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "abb092a1-aa84-4d77-a0e6-d0900223e334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "9301c0eb-02ca-4f26-8dc5-4c6465ef8965",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "b7e0e4e6-d993-4e75-a7e8-3241e4414b64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "c2aa713f-3f6b-4bc7-addc-30a3295965a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 38,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "e568ff98-7ad8-4fd2-8fd7-469e31b1e921",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "13acf1f4-97b1-4fdf-aa5f-844c241570f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "8ad53af3-70f9-493e-9ec1-b43a4c7db739",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "ebe31413-dded-465e-9aad-09e20ff8d1aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "47d79920-6729-4f87-b877-3564ba269771",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "cd729b4d-1f8a-4ed5-a18b-c92275355cf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 293,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "b1e56e88-bfff-4961-9af2-6e751e41a7c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "9f03a1c5-82f4-4bd2-90ce-3f6acce09f23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 38,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "70819354-de1b-4cdb-8750-7bbc97e8ceab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "29fd63a4-7287-420d-b96e-959440949af5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "a2eb145e-24df-467b-8322-9f2725a6425a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "7fb4d7b8-224f-4bba-b1fc-bff70c51b214",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 249,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "32de3e7e-1020-4c2a-9bcc-9ff1f47482df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 262,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "a185a300-9ca3-43e7-b10f-e1f4202336bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 38,
                "offset": -1,
                "shift": 7,
                "w": 7,
                "x": 275,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "1c66c7ff-99ea-470a-96dc-903f4f318702",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 38,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 284,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "993b214f-821f-45b7-8dab-b766a373d102",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 38,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 308,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "c9ad8c2c-6f3e-457b-91ff-96fba5e6fc03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 38,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 128,
                "y": 42
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "eaa92ace-4115-4841-9c5f-13f6ad961a9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 38,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 433,
                "y": 42
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "1261b929-27ab-4cae-9344-81521427af53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 139,
                "y": 42
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "6e5d9b6a-7e13-4e0d-8507-53b9870db6bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 482,
                "y": 42
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "5f050da2-8d62-4807-9b0f-714819f5b945",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 496,
                "y": 42
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "6924a7f3-9d0d-4fb4-8f6c-cde6662babe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "ba426caf-392c-4f65-881a-c8513bdf0e68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 82
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "7daaf7aa-9945-4040-bfc5-abdc1fe075bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 30,
                "y": 82
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "0eb6fcf4-0fde-42c2-935c-05bf946e0a59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 38,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 44,
                "y": 82
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "912137b9-e102-4e6b-8672-098cc5798338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 59,
                "y": 82
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "43f97851-f379-4cda-9259-c2c6eaa82cfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 73,
                "y": 82
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "dbed7157-e55a-4f49-b5a0-5d919c6106b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 87,
                "y": 82
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "d9e922c5-3dd0-41cd-ad31-1f83714359d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 114,
                "y": 82
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "bca2924a-8adc-431e-9ca9-1183170e92cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 273,
                "y": 82
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "8730542f-0097-4d1a-83f1-1b28812eab4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 38,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 128,
                "y": 82
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "337cfd1b-b66d-480e-aca0-f6bb2179ecaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 144,
                "y": 82
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "112e0038-e347-4456-b202-bf58fd077867",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 38,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 158,
                "y": 82
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "e6a89a45-dab1-415c-896a-51c6e21e6044",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 183,
                "y": 82
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "d97ed215-8352-4103-afe0-33008adb5f45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 198,
                "y": 82
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "ec256dc5-fe85-4bac-ae66-b0a3362ca4f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 213,
                "y": 82
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "c0c27402-117f-4497-b7e6-bfff0fd9a43e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 228,
                "y": 82
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "75a87a7b-b33b-4e11-93d2-684d860982af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 243,
                "y": 82
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "47f76ef5-b736-4a45-9942-fcdd1e09da2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 38,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 258,
                "y": 82
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "f5aa5489-9577-4502-9987-0c57705754eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 38,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 461,
                "y": 42
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "f2dfcdd5-a67a-4de5-a601-6a34c86392c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 101,
                "y": 82
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "f43dcb5d-efac-4137-9f77-dc4640b96b2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 448,
                "y": 42
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "ece4b2f8-0936-4b53-a023-9bac7c7b5ae7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 262,
                "y": 42
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "ae07f99d-ae8e-404d-9e4c-7317532853c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 153,
                "y": 42
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "edc99518-5caf-470f-97a0-b55744b49f43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 38,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 166,
                "y": 42
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "e03ae8ce-3507-443b-ab2f-e5dba7c6abd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 38,
                "offset": -1,
                "shift": 7,
                "w": 7,
                "x": 179,
                "y": 42
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "964e5e58-fc10-4610-9183-ba46c19bb860",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 38,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 188,
                "y": 42
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "99bec2c0-7c0b-4e22-9559-832752d8f278",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 38,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 197,
                "y": 42
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "5025da1e-3eda-418a-bd5e-e13569cc5658",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 38,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 208,
                "y": 42
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "6b9f283b-7fc7-4498-bd56-5e719894612b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 38,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 219,
                "y": 42
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "39341c13-712e-4565-a19a-4b05653ce233",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 38,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 234,
                "y": 42
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "8c039f88-7632-4494-abef-5de45c78cc6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 248,
                "y": 42
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "f12bf6f9-238e-4815-bb97-3540aa41c725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 275,
                "y": 42
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "91b6bc8f-4487-442c-8e2f-81d6653c367a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 419,
                "y": 42
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "3f93e879-7076-4e44-9c69-8963b077a3e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 289,
                "y": 42
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "76f3dd5a-30c7-4a3c-b431-33e6e65681aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 303,
                "y": 42
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "dfba82ec-6543-42e8-9e69-55510ba9b0c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 38,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 317,
                "y": 42
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "649bdbb3-b82d-48f5-8c2e-a75b0e6011a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 333,
                "y": 42
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "6d44a900-c511-4f3b-b3b6-3ddc92bdb809",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 347,
                "y": 42
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "4b0cf55a-f47a-44c3-aa3f-727eef948c65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 361,
                "y": 42
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "8261a22c-17cd-42d9-bdca-534529d33be7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 375,
                "y": 42
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "13682fc9-67c0-4b4b-af9b-790cf8a39276",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 38,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 389,
                "y": 42
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "6e135a98-11ac-4b9d-8ef1-e7276ff57708",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 38,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 403,
                "y": 42
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "0184bb2d-e7f5-47c5-9f7a-cb7628f07983",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 287,
                "y": 82
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "814094f6-3ad3-4d82-9ee3-a9ba864acd60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 38,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 75,
                "y": 202
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}