{
    "id": "552d8ad5-0abc-46b4-955d-fde197a720b2",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_arcadepix_24pt",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arcadepix",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b1f88999-056d-4e44-9e54-25cb930157ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 477,
                "y": 36
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ffe7999b-d56e-4a9a-8fa2-2565bc33fe65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 32,
                "offset": 0,
                "shift": 13,
                "w": 10,
                "x": 322,
                "y": 104
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "c788f690-45e5-420d-a3e4-68c3a4bd0be9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 134,
                "y": 70
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ff6b038c-43b6-450e-ac77-aa667b697808",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 32,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 160,
                "y": 104
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "77b6f934-2a2b-4dfb-8649-431212dcf075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 32,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 214,
                "y": 104
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e2eb1193-5932-463c-a1a0-b6726c6cadcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 252,
                "y": 36
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b1c13add-d179-410c-8a00-4744466e3846",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 200,
                "y": 70
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "40e4b090-936f-4c99-8197-f06dc44a8102",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 32,
                "offset": 0,
                "shift": 10,
                "w": 7,
                "x": 412,
                "y": 104
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "d9ab252a-0116-4c07-a974-25212bf4104b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 32,
                "offset": 0,
                "shift": 13,
                "w": 10,
                "x": 370,
                "y": 104
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "2b98d6b0-9685-4db1-9554-7433cd5dcff2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 32,
                "offset": 0,
                "shift": 13,
                "w": 10,
                "x": 358,
                "y": 104
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "475ce12b-b1b3-4578-9ca0-b749f61a64d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 32,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 196,
                "y": 104
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e08a0ba3-5316-4799-9234-4977e9b91663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 32,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 232,
                "y": 104
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "729adbce-3053-4bcd-8364-a8ac6a22bb3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 32,
                "offset": 0,
                "shift": 13,
                "w": 10,
                "x": 334,
                "y": 104
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "61f340e5-c8bf-481e-8b45-e005bada462f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 32,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 178,
                "y": 104
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "54840c45-3fff-40ee-9652-bef296a6bccd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 32,
                "offset": 0,
                "shift": 10,
                "w": 7,
                "x": 394,
                "y": 104
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "605cc7b6-071c-479c-a450-5dbd97b868bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 32,
                "offset": 3,
                "shift": 22,
                "w": 17,
                "x": 87,
                "y": 104
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2b29ef62-9762-4f31-bf00-806ea8179cb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 277,
                "y": 36
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e4d499a0-6cbf-4b46-93be-3ca298a1b475",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 398,
                "y": 70
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d0a121af-2b9b-4769-bda6-3aa66a031db6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 227,
                "y": 36
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "6109d728-f95f-43ab-8605-c81fe99d038d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 302,
                "y": 36
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "fc440fa0-5c43-4520-98d3-83f8d215ccde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 452,
                "y": 36
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "f98fd13a-c0a1-4364-9576-8b1da0c63dab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 427,
                "y": 36
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "6382db0e-9f3c-4f70-94d7-e1e1c868b04e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 402,
                "y": 36
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "dc3e63b5-af86-4982-bfee-1f2b37702118",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 377,
                "y": 36
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "7ea8f52c-b4f9-41dc-bccf-6d15b5a80dfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 352,
                "y": 36
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "417eed73-f817-43f9-bf21-c8cba7299098",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 327,
                "y": 36
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1eb2e2da-71d1-4bf1-ad5d-bca8a470ae02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 32,
                "offset": 0,
                "shift": 10,
                "w": 7,
                "x": 403,
                "y": 104
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "50a1f2d2-7cc7-466a-a760-fafa6e86e704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 32,
                "offset": 0,
                "shift": 13,
                "w": 10,
                "x": 310,
                "y": 104
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a4f44991-cfaa-4890-9ce5-f827815c5bbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 13,
                "x": 250,
                "y": 104
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d1c374ca-caff-4a93-ba23-6af04570ef87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 32,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 142,
                "y": 104
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "c450fdf7-edbe-4ec3-85ff-d261e0ef21f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 13,
                "x": 280,
                "y": 104
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "2b8eb342-31bc-44dc-b263-b04a4fa5fa49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 177,
                "y": 36
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "70352c48-6b6a-40e1-9a21-0358f6f08939",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 32,
                "offset": 0,
                "shift": 29,
                "w": 26,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "372d24d4-dc33-45f7-a861-ec08533b5d9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "f3612a5f-cb32-49a4-aa9c-03b7966fd01e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "77f7da57-eec7-4e38-904a-9ff1c144a985",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "3ab8038f-3988-4117-a737-4969c41107ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6818ba6a-c542-449d-8363-d15387a27d9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "fbb83aa1-b2d8-4c67-b7b6-ec4104b1a915",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "d521b824-3f6a-4278-a24e-4515d2ed6a09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 205,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "faca213b-e4a1-4424-b0e5-de2ac05e2c36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "7ce9d8f1-e302-4e35-8780-af3454c74ca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 376,
                "y": 70
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "4b7d3c73-31a3-4271-8b47-07b28cf2ea42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 255,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "0790cfd0-1e05-4da0-a151-0b75347e48ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 280,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "dbe5ef5f-a7ae-49ed-8af6-b362e8161738",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 305,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d0f896bc-5d0f-4e08-8ad5-36f02224e97f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 330,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7984ae5d-db12-4235-99c9-aeb8478164c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 355,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "127638c5-2949-4bab-b4bb-40d21f36a250",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2fd6a63f-bce4-4a7a-b99f-838a4046f18e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 152,
                "y": 36
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2ea941ca-13f4-42ef-8c7e-05fb73ca57d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 380,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "81ccde8c-60ab-4bad-949f-9b563ba4bb33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 405,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "0a8bc66f-4b22-49f9-9e05-456f23ae5974",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 430,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "3accd23b-7a9c-4f43-b442-50a778944b13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 420,
                "y": 70
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "70a147ad-4a89-4676-be54-0bf5666bc0c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 455,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5d0e0f18-64aa-40cc-b126-eefd1f318958",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 480,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "884aa114-835a-4fc7-b4a5-999f60a0dc00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "52699f07-e3db-49a7-a316-f33711b829ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 27,
                "y": 36
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "95688006-a131-499d-a673-aebf941f0144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 156,
                "y": 70
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "97fd398a-6d8c-4ea9-b1e2-31b90e8042f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 52,
                "y": 36
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "43498148-a50a-42b7-94f9-af9f6127b609",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 32,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 423,
                "y": 104
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "243eea0b-9eac-4032-b85f-db4692bf4d2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 32,
                "offset": 3,
                "shift": 22,
                "w": 17,
                "x": 68,
                "y": 104
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1e3255ee-5062-4d40-86d7-93df03e1babd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 32,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 429,
                "y": 104
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "44f21890-0c5b-46a6-b56c-38b3eebf924f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 46,
                "y": 70
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "323d7d43-8f34-45e1-a5a9-1990f9c9807b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 23,
                "x": 77,
                "y": 36
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "fad8a9eb-cbde-4419-827b-d2a4707411fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 13,
                "x": 295,
                "y": 104
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "bfe017ba-aed6-412c-86b9-5edc688783b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 222,
                "y": 70
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "3561bd3d-bb23-42b8-a554-2740cc408a3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 68,
                "y": 70
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "c6b0d63c-90ed-4233-ae92-93acdcc928f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 178,
                "y": 70
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "148fc5cb-0839-4d09-b10d-9806ad0678d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 24,
                "y": 104
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "2b91ec20-096a-4f74-8125-543092ef1d65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 442,
                "y": 70
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a14e3405-5184-4560-9be0-b81b984a3aec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 32,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 124,
                "y": 104
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "cda9304c-5969-4c2d-9984-ba054c9b009c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 24,
                "y": 70
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "042bed3e-8006-4c28-9749-6b33023b5273",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 90,
                "y": 70
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "7d1547be-5cad-4efb-936c-5fa332f7a2da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 32,
                "offset": 0,
                "shift": 13,
                "w": 10,
                "x": 382,
                "y": 104
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "35dbdad9-595b-4d7f-8801-0becc14db1f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 13,
                "x": 265,
                "y": 104
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "9f11d0c9-c6be-4703-ae4a-bfaa970c488f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 112,
                "y": 70
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "45f59770-2ce8-4810-8803-1fa4bd0e520b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 32,
                "offset": 0,
                "shift": 13,
                "w": 10,
                "x": 346,
                "y": 104
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "be6a842e-fa1a-466f-84ba-407379e22638",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 102,
                "y": 36
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "9fc42646-b3f5-45b3-84fd-22d3b216fbde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "2b129afa-9388-49c3-a482-fbe64fea53ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 244,
                "y": 70
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "ed96608f-ab9b-4b23-8400-5675031f5b19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 288,
                "y": 70
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "30ff81cf-0d9e-45aa-b94f-805e22022ad1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 46,
                "y": 104
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0ba38dfe-c2d6-490e-9756-8fc44f6d94ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 266,
                "y": 70
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c8c7b230-db49-49e0-b399-740b91cd6d2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "df3a5fa5-edda-4b4a-a749-0652fd1d7fa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 32,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 106,
                "y": 104
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "1176e0ca-a9d0-4e54-809b-27e647afb8dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 486,
                "y": 70
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "edb456ff-01ef-42e7-90e2-51b67fc10808",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 464,
                "y": 70
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "241afa61-e996-4ea6-afa9-21b2d7f84d88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 127,
                "y": 36
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "91af4519-97b4-4005-8158-7b564be61caf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 354,
                "y": 70
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "4f78b036-d12d-4f79-bb67-a8d0f03827ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 332,
                "y": 70
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "80b762e7-b88c-40d2-829c-47dad8ced227",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 32,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 310,
                "y": 70
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "6cef87d9-8ecc-4d0f-afc9-92a444ab82bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 32,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 425,
                "y": 104
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "72ee5a55-21ba-45e6-9e6d-4f8262939c50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 32,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 427,
                "y": 104
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "122db48e-db76-412f-8628-7ca7f632a4b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 32,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 421,
                "y": 104
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "61ea46e8-d7b4-458f-a843-844657c611f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 32,
                "offset": 0,
                "shift": 26,
                "w": 23,
                "x": 202,
                "y": 36
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "5faa86df-ef53-4104-8fc3-71482eca8ff8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 84
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}