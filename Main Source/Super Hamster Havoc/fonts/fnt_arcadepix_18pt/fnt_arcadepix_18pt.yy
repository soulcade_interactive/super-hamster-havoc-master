{
    "id": "307d481e-69a3-43f2-8b91-4fa248b47804",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_arcadepix_18pt",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arcadepix",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "7e195188-5afc-43b8-b296-6ff24f1a3448",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "c7b8717f-341e-49a6-ad35-d1615919ef6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 172,
                "y": 132
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "0c852bc8-b921-452b-8880-4a9ba04bfb90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 140,
                "y": 80
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "14a1ba81-3128-434f-90d5-c64134de8167",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 44,
                "y": 132
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "0ca79a3a-dcad-4f16-af38-3b48fb53064e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 86,
                "y": 132
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "57c6f546-d93e-47e5-94f4-e28ab4c5c3d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 78,
                "y": 54
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e2935dd7-10f1-48b5-a24c-b21e22f8ac97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 191,
                "y": 80
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "473f2da7-822d-4122-b2ec-628f095ee237",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 24,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 246,
                "y": 132
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "80968564-8da5-4860-82b4-7049f3f5b812",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 212,
                "y": 132
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "80386730-9773-40d4-835a-e3532c337ded",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 202,
                "y": 132
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "cdc7df83-e328-4fe8-9e00-9e102ff66bf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 72,
                "y": 132
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e1a36c7a-efe7-4ef6-ad5f-3c44f3cd2c77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 100,
                "y": 132
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "7afadf4a-1ad6-43ba-8348-f1485cae84e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 182,
                "y": 132
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "737d4c7e-ad82-4a9b-945d-57c5e67aac06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 58,
                "y": 132
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "799238e1-c845-41f5-a3c2-0a4be58d990b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 24,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 232,
                "y": 132
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0c4d0b94-aac0-47c6-abfe-ae7344a22cbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 24,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 238,
                "y": 106
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "8937625c-66f0-4d91-9919-c897f1863b88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 97,
                "y": 54
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6c727fe1-ed61-4e28-832f-ba6dd55edab7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 87,
                "y": 106
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1d27414b-377b-4e46-b751-3c432bbbadf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 192,
                "y": 54
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "43a4ef53-fb8b-4c44-a219-c1772be08865",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 116,
                "y": 54
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "78fb5438-ab9d-4942-ba01-15efd52d94d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 135,
                "y": 54
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c4159ace-fcaf-4406-980c-29a1c8c334c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 154,
                "y": 54
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c5066c24-60cb-4506-b9cb-934dba72bb2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 173,
                "y": 54
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "120bdf7a-d3a9-40ca-852e-945338ebbd43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2d604c07-676f-4a20-999c-82281dda2361",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 211,
                "y": 54
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "30eb1607-2be0-4232-a8a2-f3ca78411406",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 230,
                "y": 54
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "3b9037cd-e355-46cc-bfe6-12f08fae2d2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 24,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 239,
                "y": 132
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "7f25aaf3-2adf-4c19-b49e-49b86ff06293",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 162,
                "y": 132
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5498b1ef-ad0a-4955-a1eb-ecd3e323f8ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 114,
                "y": 132
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5765581e-cd6d-4810-9d65-d75ecb49c799",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 30,
                "y": 132
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "83d5f5ef-ffbd-466a-8c94-7af2af7799f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 138,
                "y": 132
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d8b8f6e0-7255-4dc9-812a-08454e2742aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 21,
                "y": 54
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "9ec0ee26-1888-4e9e-bb71-3aed74bf1824",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 24,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d4c9c4a0-9980-49d0-95cd-e96220e86af4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "afab474a-7484-49d9-9885-fc0dd9692de4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "9f9d1d54-9299-48d0-a5de-dad058b81ada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "cb71d34b-096a-49bd-a827-d541faca5161",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a5974770-ffad-40b0-819d-946d664eff67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b52fdff0-88be-4ee7-9cf3-de6c45e1191c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0ba1266f-a7f2-4781-8a78-cf28ca2e8e1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "cf385f24-8a7a-412d-8b04-7265d23c3de5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 176,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "30ba7a39-fcd3-41d1-8c51-834cb35db2cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 70,
                "y": 106
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "7cd3355b-4c3d-4bbd-96da-4bf9601a92e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "f979814e-57e9-4c37-9358-51c9572dd74d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e5323e33-be0f-4de4-9fc1-b650355eed2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "4b816aa9-35b7-413f-ac82-30887c648295",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "269b2e34-2f15-4061-8132-8f2817ed84c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 21,
                "y": 28
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "49150810-21c2-4b69-8283-87ea10334224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 40,
                "y": 54
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "6399dd40-b2ad-4f04-a4b3-6da9efb5be76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 2,
                "y": 54
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a99fddcf-ce6a-43f0-964e-571942996ad7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 40,
                "y": 28
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9778728e-74dc-418b-86bc-e877663fc6c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 59,
                "y": 28
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "175a3574-f0e6-4031-b9bd-ed71246f43f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 78,
                "y": 28
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "8f7d04e4-620a-44f5-b43f-998f5a5ba5f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 104,
                "y": 106
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c5b05389-63af-4bb4-9b5f-dd5dc2859ed8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 97,
                "y": 28
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4064cb4a-d5a0-4f1c-bf96-3f0af293e17e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 116,
                "y": 28
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "581acb67-47d1-419d-acd6-f8c82baee6f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 135,
                "y": 28
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "bc5d9cb7-90e2-4b94-bac1-b841c9284e22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 154,
                "y": 28
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "2a26a429-9da6-47c8-bddc-62ef7feb63dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 157,
                "y": 80
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f6cfc207-2a3a-4f63-9a9c-4d85f3aa7f2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 173,
                "y": 28
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "044ec1b2-f205-41b7-bd0c-396871cb6a0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 24,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 2,
                "y": 158
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f8eafddc-407b-4804-86ad-99bcef61c674",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 24,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 223,
                "y": 106
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "19223f5f-c696-47f9-b54e-3281ba9d3aaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 24,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 8,
                "y": 158
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "7a7d8d3f-3aee-4cbc-9979-d66d61b54a94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 72,
                "y": 80
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "850deddc-7add-4ff7-aae5-508631933327",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 192,
                "y": 28
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9c9ed4bd-8577-42fd-b337-a567f2a9127b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 150,
                "y": 132
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "47fb5a31-9678-4155-b0b4-8e9ce8baecc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 21,
                "y": 80
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "477ba246-40ee-4f70-9a59-dcfce5771011",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 89,
                "y": 80
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "fb0f78bd-e335-4e6e-b9dc-6dca120ee69f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 174,
                "y": 80
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "58687a79-a544-45ad-b35a-ed0419aeb292",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 189,
                "y": 106
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "56a2bd2e-f5ab-4760-a467-bd7654da27c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 121,
                "y": 106
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a3ef682c-63cd-4719-82a7-28f4bbef6aa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 132
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "6e20dd4a-ad6e-4906-8b4c-f0b65ed0b229",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 55,
                "y": 80
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "457ba885-4711-495b-83bb-67b42727524f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 106,
                "y": 80
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "95dc691d-094f-41fe-871d-c2e631453903",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 222,
                "y": 132
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "95a0e176-4151-42fa-8958-3a848edc8a6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 126,
                "y": 132
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "707002d3-8ab0-4f68-a242-6b08002c457c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 123,
                "y": 80
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "68e5e38c-e48f-48c3-911f-8784bbd21880",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 24,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 192,
                "y": 132
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "868343e0-2160-4515-b3c0-30b391ffc500",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 211,
                "y": 28
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "19301f66-1b71-4d19-b17b-9c2f9c9b70f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 38,
                "y": 80
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f5215f9c-e032-46a7-bc3e-a46342ff33ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 208,
                "y": 80
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6a26206e-db91-4a45-b52d-55a2285ce5e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 2,
                "y": 106
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "9859c22f-2b35-4dd0-baf7-59b494135fe8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 206,
                "y": 106
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c2d2efc8-eeec-42ca-b8da-05d72694d3e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 225,
                "y": 80
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8de13876-fcf7-4d0c-ad56-84388bf6dd99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 172,
                "y": 106
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3dc6d5b4-2056-46c6-8678-cbb46dad2960",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 2,
                "y": 132
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "91cc8c3d-c8a9-426d-884d-ff21b2dc6c79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 155,
                "y": 106
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "8e7ee976-411e-4b98-abad-306eb9de6be6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 138,
                "y": 106
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "b16d6956-23b2-4665-a73c-6da2939b7102",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 230,
                "y": 28
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "9841d94d-d78f-4f94-945d-2d210c78c71f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 53,
                "y": 106
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "22b88dbd-d201-4841-8db5-30dfb686f116",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 36,
                "y": 106
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ef4d9328-7305-473b-b7d0-f7d3e34c9e0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 24,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 19,
                "y": 106
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "5c90c9c6-b0a0-416c-8d1a-6d1e71c59742",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 24,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 4,
                "y": 158
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "68402d1e-c4e0-4c38-a47c-170c182832eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 24,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 6,
                "y": 158
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2e3bf1ca-5d8f-44ed-a565-08f11c45ccf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 24,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 253,
                "y": 132
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "bf3978f1-1187-4938-b447-3d5cbcb3a2ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 24,
                "offset": 0,
                "shift": 19,
                "w": 17,
                "x": 59,
                "y": 54
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "2285c5e2-0abd-4aec-978c-0d012c95016a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 84
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 18,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}