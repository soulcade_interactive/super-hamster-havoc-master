{
    "id": "22472f2a-877c-4c6d-a082-ce5fb505c46f",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_8bitwonder_24pt",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "8BIT WONDER",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4dfb784d-6464-4312-af6d-72dd69d14e9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 457,
                "y": 104
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "dc0eb590-ac54-457e-b2f4-f697a1160312",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 32,
                "offset": 8,
                "shift": 19,
                "w": 5,
                "x": 332,
                "y": 138
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "37369853-8236-4a5d-8bd5-37c3182d04c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 32,
                "offset": 4,
                "shift": 19,
                "w": 11,
                "x": 233,
                "y": 138
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "39f724f6-2d57-4c5a-8874-b45767897858",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "9912eda1-23f6-47eb-8039-c74b0e22e33b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 32,
                "offset": 3,
                "shift": 19,
                "w": 15,
                "x": 38,
                "y": 138
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "6f383310-2579-41d3-b8b7-91a787fdeaa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 32,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 395,
                "y": 104
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ceb36eff-be8b-4743-b53e-90e7136e0577",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 32,
                "offset": 1,
                "shift": 19,
                "w": 20,
                "x": 373,
                "y": 104
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "efe298d8-f51e-4f3e-98d6-b05b968a01c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 32,
                "offset": 8,
                "shift": 19,
                "w": 5,
                "x": 318,
                "y": 138
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "aed38b07-b98f-48a0-b11e-893531987cff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 138
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "90f0ae15-aaae-417d-aacb-027527e1c12d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 20,
                "y": 138
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "2355b7a6-1daf-4a47-9ea1-ff99d120b595",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 234,
                "y": 70
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "08432a90-6d66-47ca-b98c-5cc659a22ce6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 32,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 55,
                "y": 138
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "935dd45d-aae7-42fd-a3b4-fc7f5fca50a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 32,
                "offset": 7,
                "shift": 19,
                "w": 7,
                "x": 294,
                "y": 138
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "247b4135-3372-4fd7-b7be-f6b878006f90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 32,
                "offset": 4,
                "shift": 19,
                "w": 11,
                "x": 259,
                "y": 138
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f0e47db3-c8be-4117-936a-61f2fe3aed9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 32,
                "offset": 7,
                "shift": 19,
                "w": 5,
                "x": 311,
                "y": 138
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "ed52cee9-9d7c-44b1-af94-da497fd011e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 32,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 173,
                "y": 138
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "cfe336cd-e035-4408-8b30-3ecd524dc765",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 408,
                "y": 70
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "d381f3e1-4822-47cf-a11d-00a147f7ac11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 32,
                "offset": -3,
                "shift": 16,
                "w": 14,
                "x": 157,
                "y": 138
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "14c213fd-88a1-4140-9421-fe1220009b7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 466,
                "y": 70
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b19ce4ea-b184-4c10-b542-f84f06df507d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 292,
                "y": 104
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "349bbdae-7cbd-43b2-90ba-d0151c0c1c47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 263,
                "y": 104
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "a241bc7c-0ba1-4314-be56-047b4300edef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 234,
                "y": 104
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "05465ba6-0a29-48cd-9feb-72734056a25b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 205,
                "y": 104
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "1b91b2cd-6bf1-418d-a0ab-42cefcd85e6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 176,
                "y": 104
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "e9809236-721d-4a65-80d0-5eaaa45c4e12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 321,
                "y": 104
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a1d73fc2-23cc-4213-b70f-cb7ff50936cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 118,
                "y": 104
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "8e180b5f-5b60-4427-b10e-70cbbb7e4669",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 32,
                "offset": 8,
                "shift": 19,
                "w": 5,
                "x": 325,
                "y": 138
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "6a4f3503-a495-45b2-a866-eec0266a1f2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 32,
                "offset": 6,
                "shift": 19,
                "w": 6,
                "x": 303,
                "y": 138
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "c6dea444-c5da-4585-9197-1a64ec3485d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 32,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 72,
                "y": 138
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "2340b01a-3093-460f-8285-8760c74bfaba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 32,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 89,
                "y": 138
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "284d4127-ebd6-449e-a8e3-7e1fe5a0434d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 32,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 123,
                "y": 138
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "6a6a70d3-de3b-4c52-b3db-80d152c43c17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 32,
                "offset": 3,
                "shift": 19,
                "w": 15,
                "x": 140,
                "y": 138
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "ad6a2b3f-99b2-49b1-a3f8-86273f961b32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 32,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 417,
                "y": 104
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "9e11fdcb-cd29-4d7b-9b38-983284074b61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "3ee0ec2a-9b28-43fa-a1b1-386b2f4fe876",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 89,
                "y": 36
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "48f77c05-03d3-4f40-ad9d-37e642af02e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 477,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "66ee6a27-a952-4035-bdef-38bc5f192109",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 448,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b05f8958-d4ab-4b9f-833e-0848624c7893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 419,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "7d4f4617-9306-463d-95f5-b9c3b7a1356c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 390,
                "y": 2
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e67415a5-c11a-4744-85d7-7ae17a5dd75f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 361,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "81bb3d21-dfb0-47a6-94b0-9c27d56c8d59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1ddbd063-8fcf-47e2-9c8b-3c9f1ea4e134",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 11,
                "x": 246,
                "y": 138
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "cb8b2742-9254-4932-8ef8-da0e47382459",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 303,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "709fb849-e2b0-46d8-ab64-bcc71472102c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 274,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b410228e-4ab5-4c52-940a-508eb9b23b75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 245,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a222e75a-b5fb-46ef-9750-02aafbfe40c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 32,
                "offset": 0,
                "shift": 48,
                "w": 43,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "0c10097c-4601-4599-9232-880d1fdab8bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 31,
                "y": 36
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "9a6ce80b-4676-4969-9516-8d3ea902121d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "64b11b4a-69ef-4061-ab0e-d304bedea562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 60,
                "y": 36
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "c6ec901b-00b0-4f4d-aef8-7dcaee649867",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 118,
                "y": 36
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4468280c-1af3-41ce-9947-b75de917c783",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 147,
                "y": 36
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "3061994d-cac2-4a74-ab77-f643cf0db5cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 176,
                "y": 36
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f9bd0ae7-efc2-45bb-b11e-513822c8f703",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 205,
                "y": 36
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "fc5822e9-8360-471f-a51d-83983c73c365",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 234,
                "y": 36
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "dd6bd3c9-fa78-42c1-b691-8f546e0a4aa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 263,
                "y": 36
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "82d8a2ac-b881-4e9c-945e-fbe990f06648",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 32,
                "offset": 0,
                "shift": 48,
                "w": 43,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "09b89250-d784-42d5-b7c6-8bd8bbb2c740",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 292,
                "y": 36
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b7194ca8-f30a-4bb6-baad-9ddf79e56f97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 321,
                "y": 36
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "d73069a9-cdf3-4cf3-8662-92eb36b022d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 350,
                "y": 36
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c321b620-628b-4e86-80dc-6fe4c4fbac9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 493,
                "y": 104
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "8cfc531e-8b84-430e-9046-a06afa693314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 32,
                "offset": 3,
                "shift": 19,
                "w": 13,
                "x": 203,
                "y": 138
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "adcc68fe-280c-47fc-a7ad-c99652103d46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 475,
                "y": 104
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "4bcf5cec-6436-4df1-b67c-64d3c8094b9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 32,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 438,
                "y": 104
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "7b17f58c-38fb-4a46-bb81-b56b7ea7d187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 32,
                "offset": 0,
                "shift": 19,
                "w": 21,
                "x": 350,
                "y": 104
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7a252067-d560-4389-8e49-bc87e46da9fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 32,
                "offset": 7,
                "shift": 19,
                "w": 7,
                "x": 285,
                "y": 138
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "e46c7c79-d1ae-4661-ac1a-80c41543c0e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 31,
                "y": 70
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b1b85ed5-2c5c-4fb7-947b-8eb854dab0d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 379,
                "y": 70
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "04c35ed4-9065-4dd1-b46b-b4fa90ccacb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 437,
                "y": 36
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "9ee55ecd-6891-4a63-b361-482b3e6ff6dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 408,
                "y": 36
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "37a6fe50-6169-42bf-b7f8-34bab2468f58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 379,
                "y": 36
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "18528843-5fe3-481d-96b7-c40a89c9b667",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 466,
                "y": 36
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3a7aaa6e-45f2-4ef5-b0d9-7a8f5035c024",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 332,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "ea08ab79-e604-4e6e-9bfe-109939e64d1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 60,
                "y": 70
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "97234e5d-0c1f-4287-8f0a-4fbc179841d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 32,
                "offset": 0,
                "shift": 16,
                "w": 11,
                "x": 272,
                "y": 138
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "b22574db-ec83-4aa4-85c3-3c8e9f3a8f99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "3a3a70f2-f5c4-4e21-9882-da47ca1744fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 31,
                "y": 104
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e3a3dc34-82c0-4f7b-8933-0af874542d82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 60,
                "y": 104
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a6c6aecd-f4fd-4d2e-b3d8-7c8fb6fd26ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 32,
                "offset": 0,
                "shift": 48,
                "w": 43,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a3b5564f-c123-4ee8-96b9-aa65bad1a85d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 147,
                "y": 104
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b78e8f9f-3d7d-49c7-9312-47e48a1218ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 89,
                "y": 104
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "4d12cb9c-fd27-461e-a3b1-77ca3df6f8a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 437,
                "y": 70
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "023658ea-ad6e-4385-a13f-a7044de1ed44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 350,
                "y": 70
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b75bf0ff-3d53-4552-8bf9-465b20b302db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 321,
                "y": 70
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "5c844a57-856a-4343-8a41-a3f92094bfde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 292,
                "y": 70
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "39de234f-aa80-4f01-a597-141f6291b90a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 263,
                "y": 70
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "dccd2762-37cd-49ca-a2f3-ca672dd3d77d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 205,
                "y": 70
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "e3c11363-38c4-4a24-b487-6a976568d46f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 176,
                "y": 70
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "dcb3c47c-0e3e-43a3-95af-86b2966de6f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 32,
                "offset": 0,
                "shift": 48,
                "w": 43,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "f118f7c9-7bea-4665-8e35-1029ad38aa35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 147,
                "y": 70
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "bd8ba8b2-1ebb-4bba-b391-7279dec6aa89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 118,
                "y": 70
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "6a386831-e359-4c9a-a6a7-75aebbd006e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 32,
                "offset": 0,
                "shift": 32,
                "w": 27,
                "x": 89,
                "y": 70
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b2f38e61-806a-4587-97df-25e31cde7a69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 32,
                "offset": 4,
                "shift": 19,
                "w": 13,
                "x": 188,
                "y": 138
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "049d1ccd-a7e7-4b27-b2c4-01fd1cff3dc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 32,
                "offset": 8,
                "shift": 19,
                "w": 3,
                "x": 339,
                "y": 138
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "55648ccf-4749-43e4-9beb-50b9a95089e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 32,
                "offset": 4,
                "shift": 19,
                "w": 13,
                "x": 218,
                "y": 138
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a7d692c5-4ff3-48cd-8863-6597e6d32347",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 32,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 106,
                "y": 138
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 24,
    "styleName": "Nominal",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}