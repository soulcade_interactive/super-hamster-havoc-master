{
    "id": "89019849-9c42-4415-abd1-75cadd7f3df2",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_large",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "Bebas",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "51d0157b-728f-4809-8f7e-a8138cfd6131",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 65,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 286,
                "y": 391
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "23ec1c5e-3103-40d2-9745-ccbc68ea58a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 65,
                "offset": 2,
                "shift": 15,
                "w": 10,
                "x": 215,
                "y": 391
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5b16f89b-0af1-4087-a013-80a78e343c31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 65,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 450,
                "y": 315
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "e53495e5-ba78-4a51-97a6-f47e0433ea22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 65,
                "offset": 2,
                "shift": 52,
                "w": 48,
                "x": 64,
                "y": 11
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "9fa1af3e-df8a-406a-9f08-9b6672aa46cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 65,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 130,
                "y": 163
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "9d06d050-b77f-4e0f-8383-43e4f55eb81a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 65,
                "offset": 2,
                "shift": 45,
                "w": 41,
                "x": 300,
                "y": 11
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "0aebfaf9-ebb7-4a29-9ada-841d56b0946f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 65,
                "offset": 2,
                "shift": 40,
                "w": 35,
                "x": 386,
                "y": 11
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d15626cd-1a9f-48d4-9d66-5212903bded4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 65,
                "offset": 2,
                "shift": 14,
                "w": 9,
                "x": 275,
                "y": 391
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "28f7b371-3c6a-4124-8e58-072e6bc0b37d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 65,
                "offset": 2,
                "shift": 23,
                "w": 18,
                "x": 68,
                "y": 391
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f05939c5-c40e-4b1f-8154-77ebdcf454ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 65,
                "offset": 2,
                "shift": 23,
                "w": 18,
                "x": 88,
                "y": 391
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "cf834e4f-0d0b-4594-9ff7-ffc344c39fbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 65,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 402,
                "y": 315
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "371c5fcd-1f11-45ff-9aa0-2f335ed0d16b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 65,
                "offset": 2,
                "shift": 35,
                "w": 31,
                "x": 478,
                "y": 87
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "2f32a874-351f-4c0f-b61c-17bb8dbb20a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 65,
                "offset": 2,
                "shift": 15,
                "w": 10,
                "x": 251,
                "y": 391
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "cc571976-aeb0-4363-baf6-d36b4c46384e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 65,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 120,
                "y": 315
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "67f1792f-9e94-47c5-8916-64edf53e0125",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 65,
                "offset": 2,
                "shift": 15,
                "w": 10,
                "x": 191,
                "y": 391
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "2a4ef61a-2935-4388-a777-bb12cebbd999",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 65,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 210,
                "y": 87
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "cfa43aa2-8259-4cbf-8acf-3f1157130d09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 65,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 250,
                "y": 239
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "fdb81a2c-b35c-4f88-81a5-c711f176c772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 65,
                "offset": 2,
                "shift": 22,
                "w": 17,
                "x": 108,
                "y": 391
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "939db5a2-b4bf-4cad-9214-65cf3f247c2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 65,
                "offset": 2,
                "shift": 34,
                "w": 29,
                "x": 95,
                "y": 239
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a4f92e3f-34a8-49a2-82bb-f0d6b390d6ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 65,
                "offset": 2,
                "shift": 34,
                "w": 29,
                "x": 126,
                "y": 239
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0168bac3-9c1b-4ad3-9c9b-0d5571ecc107",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 65,
                "offset": 2,
                "shift": 35,
                "w": 31,
                "x": 412,
                "y": 87
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ab6bc58c-93ab-44a1-9a5a-d737b9635a05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 65,
                "offset": 2,
                "shift": 33,
                "w": 28,
                "x": 343,
                "y": 239
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "3b302b16-65af-45c5-8bc1-d1cad25221c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 65,
                "offset": 2,
                "shift": 32,
                "w": 27,
                "x": 207,
                "y": 315
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d17d0027-da7f-449d-b93d-7f2b5f587985",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 65,
                "offset": 2,
                "shift": 32,
                "w": 27,
                "x": 178,
                "y": 315
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "42b2390f-2a5d-4584-af23-11795d0cfe98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 65,
                "offset": 2,
                "shift": 34,
                "w": 29,
                "x": 219,
                "y": 239
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "36717664-d860-4e9c-9a24-1d83ef7c3447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 65,
                "offset": 2,
                "shift": 32,
                "w": 27,
                "x": 149,
                "y": 315
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "46516e28-f6d2-435b-a166-a17963f7ad84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 65,
                "offset": 2,
                "shift": 15,
                "w": 10,
                "x": 179,
                "y": 391
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "37131b20-6a55-4ad8-bccc-0c9457d5de97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 65,
                "offset": 2,
                "shift": 15,
                "w": 10,
                "x": 203,
                "y": 391
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "c4820b60-4f50-4a36-aac5-3c66d5bd543d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 65,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 290,
                "y": 163
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b0d33278-2d8e-4751-90f0-12b1e553efbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 65,
                "offset": 2,
                "shift": 35,
                "w": 31,
                "x": 346,
                "y": 87
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "afe9dc85-f92e-45ef-a89b-3ac8ff80882f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 65,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 226,
                "y": 163
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "651a17b1-485b-491d-bbc0-834b2564ded2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 65,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 281,
                "y": 239
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d4a13bfd-4af8-4ad5-83a9-ac27c0334a0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 65,
                "offset": 2,
                "shift": 64,
                "w": 60,
                "x": 2,
                "y": 11
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "ed63081f-945e-4c2e-aada-5519c9b20e27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 65,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 176,
                "y": 87
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "9e7b49a1-21c5-4ffd-8a71-9676b72c5f44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 65,
                "offset": 2,
                "shift": 35,
                "w": 30,
                "x": 162,
                "y": 163
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "2b6b5d34-82ff-4942-a213-26759f44f77c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 65,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 312,
                "y": 239
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "9496b82f-04e1-450f-aa76-41f23d8c5b5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 65,
                "offset": 2,
                "shift": 33,
                "w": 28,
                "x": 32,
                "y": 315
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d8279d43-b199-4856-b3b1-79e5651c3c84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 65,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 320,
                "y": 315
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "bcb78f44-6fbc-43dc-966b-e58f059252d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 65,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 236,
                "y": 315
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "083287e3-7f8f-467b-90bc-e2cc64c145ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 65,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 188,
                "y": 239
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "9131f528-84cb-4071-a85d-7cebb4ac03ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 65,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 157,
                "y": 239
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "9b4f0420-6166-41f6-aa5d-ef69412c0f57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 65,
                "offset": 2,
                "shift": 15,
                "w": 10,
                "x": 227,
                "y": 391
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "a159c6b1-2e4f-4c43-abff-715fb4580451",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 65,
                "offset": 2,
                "shift": 25,
                "w": 20,
                "x": 472,
                "y": 315
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "3000d507-0add-412b-ab74-423d9c07a34a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 65,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 312,
                "y": 87
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c38d8b7c-d992-4db5-8896-01533003287d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 65,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 348,
                "y": 315
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "7bc4eaf9-2d7d-4805-a1e5-134e12b45a87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 65,
                "offset": 2,
                "shift": 46,
                "w": 41,
                "x": 257,
                "y": 11
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "5041bfc7-9ca3-4a93-a0f7-4acfcde4adeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 65,
                "offset": 2,
                "shift": 36,
                "w": 31,
                "x": 379,
                "y": 87
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a35013d5-7c4e-4980-adbd-8913657a7789",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 65,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 64,
                "y": 239
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "21d9640e-5096-4e04-910e-14a250d33c2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 65,
                "offset": 2,
                "shift": 33,
                "w": 28,
                "x": 463,
                "y": 239
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "eaebd568-3984-4518-be8d-967e023d21b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 65,
                "offset": 2,
                "shift": 35,
                "w": 30,
                "x": 34,
                "y": 163
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "99c69859-53b9-434b-adaa-b70008207060",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 65,
                "offset": 2,
                "shift": 35,
                "w": 30,
                "x": 66,
                "y": 163
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "08eefc77-ad60-4dc6-834e-6f48fd6f4fff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 65,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 98,
                "y": 163
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "bd92b3d6-b985-4a2d-bc15-4d7adc2da419",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 65,
                "offset": 2,
                "shift": 33,
                "w": 28,
                "x": 2,
                "y": 315
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "f65d476d-328a-4432-87da-0afd9843e46f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 65,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 416,
                "y": 163
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "b35ca011-3cd0-4010-b370-f389980ada7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 65,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 423,
                "y": 11
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "95f6d6d1-5ab1-4a79-b7b5-015b1167665c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 65,
                "offset": 2,
                "shift": 50,
                "w": 46,
                "x": 162,
                "y": 11
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "931aecc3-b4f3-4407-8d0c-22f50f0b596c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 65,
                "offset": 2,
                "shift": 38,
                "w": 33,
                "x": 72,
                "y": 87
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c9eba74b-6f16-4de0-80bc-e53b8e1a6a74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 65,
                "offset": 2,
                "shift": 38,
                "w": 33,
                "x": 107,
                "y": 87
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b08fee1b-8815-4152-b0ba-e1a07dc2c439",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 65,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 62,
                "y": 315
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "483e28fd-f86d-4605-aee7-1a851248a341",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 65,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 127,
                "y": 391
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c36c512d-63a4-4703-9fb0-876df849932a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 65,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 278,
                "y": 87
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "35ef491c-4a2c-4120-98fa-8dca35f091d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 65,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 145,
                "y": 391
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "4d405d08-c771-46de-84f8-b78431016fe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 65,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 24,
                "y": 391
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "2e4b111c-610f-40b1-86c3-cdb53515a7dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 65,
                "offset": 2,
                "shift": 50,
                "w": 45,
                "x": 210,
                "y": 11
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "088a5dcc-ad08-48fd-bf57-b46c1152a160",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 65,
                "offset": 2,
                "shift": 19,
                "w": 14,
                "x": 163,
                "y": 391
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f672fe57-1a6e-4e5c-a9f6-d4ec8f13f37f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 65,
                "offset": 2,
                "shift": 37,
                "w": 32,
                "x": 244,
                "y": 87
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "782f7d44-989a-4ce0-922e-2e3e2d45b6e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 65,
                "offset": 2,
                "shift": 35,
                "w": 30,
                "x": 2,
                "y": 163
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ab1a9b1d-d328-44c1-897f-01c5b60833fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 65,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 354,
                "y": 163
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "a4ca0f08-f31c-41c8-b8c7-e794165325ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 65,
                "offset": 2,
                "shift": 33,
                "w": 28,
                "x": 373,
                "y": 239
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "365771a2-7597-4359-8823-b41dec640a47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 65,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 292,
                "y": 315
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "971281de-fc66-4151-b7cd-762a5984034c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 65,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 264,
                "y": 315
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f318079f-2b97-4d71-87fb-4e4f02277ba0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 65,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 33,
                "y": 239
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "86828992-225d-4ae8-80b8-776dc444485e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 65,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 478,
                "y": 163
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "edb69dc1-6af8-47f9-af1f-8c187c9854a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 65,
                "offset": 2,
                "shift": 15,
                "w": 10,
                "x": 263,
                "y": 391
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "9e63597b-3de0-4081-ae48-e9fdab1fe7e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 65,
                "offset": 2,
                "shift": 25,
                "w": 20,
                "x": 46,
                "y": 391
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "9a264281-7a4e-4651-b0e3-477fb5ce8718",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 65,
                "offset": 2,
                "shift": 36,
                "w": 32,
                "x": 142,
                "y": 87
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e65b80de-0a37-4345-88e8-8f8c465fcfdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 65,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 375,
                "y": 315
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f1c880b2-b51a-435a-a440-44fd282f8fb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 65,
                "offset": 2,
                "shift": 46,
                "w": 41,
                "x": 343,
                "y": 11
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "dd677c81-569d-4d6a-ad60-1bd38267daab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 65,
                "offset": 2,
                "shift": 36,
                "w": 31,
                "x": 445,
                "y": 87
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "78b4543b-a6b7-484a-beb2-16e12a457f87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 65,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 447,
                "y": 163
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "b77e8d77-4905-4ff2-885d-7f5fda019408",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 65,
                "offset": 2,
                "shift": 33,
                "w": 28,
                "x": 403,
                "y": 239
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a3ccd047-c27e-4b3c-b77b-2df0dee7d759",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 65,
                "offset": 2,
                "shift": 35,
                "w": 30,
                "x": 258,
                "y": 163
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "410814eb-52c1-4886-b4a7-f15071d87d40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 65,
                "offset": 2,
                "shift": 35,
                "w": 30,
                "x": 322,
                "y": 163
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8ff6b338-c2b5-4abd-8200-f98dd36244a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 65,
                "offset": 2,
                "shift": 34,
                "w": 30,
                "x": 194,
                "y": 163
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "549707b4-0f44-437c-9f00-913d2af19845",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 65,
                "offset": 2,
                "shift": 33,
                "w": 28,
                "x": 433,
                "y": 239
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "5efb1af9-d44b-417f-a328-03c2fc5e4c38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 65,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 385,
                "y": 163
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "7e30bb5c-c48a-4637-8b95-780a18624a74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 65,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 459,
                "y": 11
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "3171a28e-a33d-469e-bc23-82618634df51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 65,
                "offset": 2,
                "shift": 50,
                "w": 46,
                "x": 114,
                "y": 11
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "da90006c-9353-4299-921b-e03509606697",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 65,
                "offset": 2,
                "shift": 38,
                "w": 33,
                "x": 37,
                "y": 87
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "569ee692-8586-4726-92cc-57a1f116bf45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 65,
                "offset": 2,
                "shift": 38,
                "w": 33,
                "x": 2,
                "y": 87
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "625edad9-56a0-41dc-8b4b-fb0a38fe61e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 65,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 91,
                "y": 315
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "fcbd24cc-1f93-42b7-ae91-8b49ab1e505c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 65,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 428,
                "y": 315
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "cd0a1827-fc81-4136-9531-752695aed519",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 65,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 239,
                "y": 391
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "4954c651-eb55-4aa2-bc44-9ee144f2220e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 65,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 2,
                "y": 391
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "822d688c-1823-4332-9e32-10e20ce86d79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 65,
                "offset": 2,
                "shift": 34,
                "w": 29,
                "x": 2,
                "y": 239
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "57c9143a-6e2a-4f31-86bc-ee4deed7a754",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 65,
                "offset": 0,
                "shift": 23,
                "w": 0,
                "x": 294,
                "y": 391
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "d8de393c-2a27-4fd8-8fe7-dccaf0b85403",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 48
        },
        {
            "id": "8a6fff72-da64-44c9-aeff-344c6b8496c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 49
        },
        {
            "id": "d5d768c0-6afc-44d5-ab73-6954400edf13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 50
        },
        {
            "id": "9ceb0a09-f0ec-43c7-ba37-8fc2f3c0d692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 53
        },
        {
            "id": "3df11914-0820-4161-bf31-46b1381b1c82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 54
        },
        {
            "id": "bb84cc89-c7d6-4e00-9ca9-e990fef3e906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 56
        },
        {
            "id": "3be0aa21-0b32-4f0b-a374-b30c0d8aad4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 57
        },
        {
            "id": "330a4fa0-7370-4369-b1f7-503e60309351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 66
        },
        {
            "id": "6df3b4f7-18fe-4b75-83eb-3c14ee5e2ebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 67
        },
        {
            "id": "882aed41-a286-4431-8a2f-5a311829f46e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 68
        },
        {
            "id": "5b5b8a60-fb30-459a-b3af-b921931a0b12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 69
        },
        {
            "id": "f4070f3f-e408-4f02-89a2-8fc4794757a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 70
        },
        {
            "id": "8cfff57c-e251-4209-a720-32c9530d2ecd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 71
        },
        {
            "id": "aa36284b-7908-4355-a800-a6c616b8efcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 72
        },
        {
            "id": "f1028142-74d8-4718-8821-21e9e2884097",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 73
        },
        {
            "id": "3c4b6fb0-7041-4b95-a872-1088746e10c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 75
        },
        {
            "id": "23899bfa-f63e-42d3-b5a9-482b24ff4d16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 76
        },
        {
            "id": "77c63323-cd03-4c5a-89e6-c0ead1cc117b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 77
        },
        {
            "id": "53a375e2-0639-4a93-b442-56100805ca94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 78
        },
        {
            "id": "342df1d9-11c1-494c-a183-51f858c6d5a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 79
        },
        {
            "id": "60dfa650-501f-4f16-90af-229cce23f8de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 80
        },
        {
            "id": "155168bd-a57d-470d-9b33-0e3612e2eedf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 81
        },
        {
            "id": "8335d389-bf81-40db-bb55-8ec919a1291b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 82
        },
        {
            "id": "57e7b5ae-ec32-4310-9a27-5950f2d784d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 83
        },
        {
            "id": "a270cf8e-d473-4143-8496-17040d15a8c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 85
        },
        {
            "id": "7b21210b-9fdc-4e65-b1ed-39ed46ed3af0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 88
        },
        {
            "id": "5956678a-b0d7-41ac-b28f-59dc1d6bdf0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 89
        },
        {
            "id": "56dbb94e-b924-4805-847a-e846e40d153d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 98
        },
        {
            "id": "11d83918-52d6-4173-932e-98f76196c823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 99
        },
        {
            "id": "237ddc1b-98c4-4d8c-bbac-4c6aab263fe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 100
        },
        {
            "id": "24edae1d-664b-49c7-ae40-9f4a0affbc00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 101
        },
        {
            "id": "35e7e938-e2e2-4776-aee1-5c282f89cc20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 102
        },
        {
            "id": "c5faa091-65c0-4d0a-b10b-017bfd8bdc07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 103
        },
        {
            "id": "4a836c1e-f904-44e6-9530-0483b2685abf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 104
        },
        {
            "id": "8080201a-ddce-4f96-9b24-3c84c39253da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 105
        },
        {
            "id": "e9d8de22-53c2-4a5e-a9b4-3c1bc6864e56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 107
        },
        {
            "id": "ff38ee73-0ea6-4730-9512-f595740b16e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 108
        },
        {
            "id": "ffbd0fab-ad1d-4ea7-acb8-e200ca368e4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 109
        },
        {
            "id": "4f969262-4989-47c5-8640-2878e7fd1128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 110
        },
        {
            "id": "d1bb9163-b8e4-4d44-a512-1ff0893e3566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 111
        },
        {
            "id": "d76c9234-0df7-48bc-bedb-b2efba16f70e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 112
        },
        {
            "id": "4514b6c5-fb0c-40d8-8c1b-158cbf7310dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 113
        },
        {
            "id": "47032e4d-b1ca-41aa-874e-c007695224f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 114
        },
        {
            "id": "231902b0-c59b-4a8a-9870-1b11ef367d25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 48,
            "second": 115
        },
        {
            "id": "b884f811-9e4e-4662-88ac-08e219684fcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 48,
            "second": 117
        },
        {
            "id": "53228588-2bf4-4e88-8a32-00809feed92b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 120
        },
        {
            "id": "0378b467-a6d8-447e-9d18-72f3f820053b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 121
        },
        {
            "id": "f966017f-c95d-4462-ae76-3326038405bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 48
        },
        {
            "id": "df82941c-2322-4c20-9c84-8370af7a6dc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 49
        },
        {
            "id": "94dfce27-7ed3-4a96-be21-bf3aa1e41711",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 50
        },
        {
            "id": "1e838ea3-5714-453c-a16f-573bdce92942",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 51
        },
        {
            "id": "8d42f76c-09e9-479c-9c0f-d4b01e7ae838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 52
        },
        {
            "id": "7b475beb-130a-440f-8f46-947426d1c7c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 53
        },
        {
            "id": "0a4c6717-44d8-44c1-9e32-51f8633e67fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 54
        },
        {
            "id": "4cf799aa-62b9-4653-8940-639ec338f6f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 56
        },
        {
            "id": "3edbcfe7-2f30-4ae0-b432-7443f728d527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 57
        },
        {
            "id": "98c3cb47-f9ba-4ead-b99e-9023eeecd991",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 65
        },
        {
            "id": "b159837a-e7e4-40d2-8d2c-44dc614c1768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 66
        },
        {
            "id": "f192681c-13b7-4079-be05-3e8f62efab2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 67
        },
        {
            "id": "fe955fab-44e6-4422-811a-fc45158a66ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 68
        },
        {
            "id": "a00fb7b8-583f-4e69-9619-ab9fbf66a665",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 69
        },
        {
            "id": "45e5ee51-1aee-4907-89b7-cc6c02257700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 70
        },
        {
            "id": "eb2f084b-f0f7-4e49-9039-7ae1e2709457",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 71
        },
        {
            "id": "a143a5db-3533-49f3-9787-0a0e3b3d39de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 72
        },
        {
            "id": "8ff6295f-c38b-4c57-a018-ad86c5b9c5ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 73
        },
        {
            "id": "83b3f401-0528-42e3-a537-1521de0cfea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 75
        },
        {
            "id": "fcd49ea3-6f3a-46f1-830f-e9a61a695d84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 76
        },
        {
            "id": "44d86da1-a73f-49b4-8124-6eff634e924a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 77
        },
        {
            "id": "836275e1-2214-446f-bae1-7bdb1434b029",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 78
        },
        {
            "id": "c9223f0d-2bac-4018-b699-f86d667960be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 79
        },
        {
            "id": "ada57110-8356-42e6-af06-3d5239ff1e1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 80
        },
        {
            "id": "442f92e0-3a52-4448-93ea-7c9f367e52b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 81
        },
        {
            "id": "031058a5-3f17-4a39-9183-210b789d5f90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 82
        },
        {
            "id": "c9cb688e-db1f-4ada-bf8e-7693028c2456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 83
        },
        {
            "id": "f293d6a5-48b3-4230-aab4-10e84933111b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 84
        },
        {
            "id": "77bf138a-d5aa-4262-80de-50f41aaaf91a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 85
        },
        {
            "id": "6dcb95a0-90c0-46e5-8908-093830e8df5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 86
        },
        {
            "id": "6452a96c-7013-46f2-a868-b3b3fdc6885c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 87
        },
        {
            "id": "b325ff0f-d451-4862-a691-5839bf7fee36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 88
        },
        {
            "id": "3bfbad2a-552c-4fca-bf28-adaa1d47cc02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 89
        },
        {
            "id": "277fda0d-1d19-41c5-a6cb-7bb42f3cc35e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 90
        },
        {
            "id": "803ca8f6-9c2d-4a9e-a317-a6a50dbeb4d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 97
        },
        {
            "id": "03f1518e-b4dc-43fa-b2df-eaec25bb9098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 98
        },
        {
            "id": "acd60e68-f325-42b4-945c-42e927e8f48f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 99
        },
        {
            "id": "c06f8694-7be6-48e1-8048-dc5d233ed2f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 100
        },
        {
            "id": "23b63919-df82-46d4-9406-88b9d288a030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 101
        },
        {
            "id": "8f1233ad-621f-4766-b93a-c3f1d8ac70cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 102
        },
        {
            "id": "05ddb679-97c2-42f6-9a1f-9e95ad6aa38f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 103
        },
        {
            "id": "1e700b44-7ac5-4d18-913c-6179de27406e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 104
        },
        {
            "id": "42b821bb-d158-46a5-b602-f9602ead2f48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 105
        },
        {
            "id": "34b57642-0b2f-4bcb-bef1-f717b33974fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 107
        },
        {
            "id": "d3393b27-72c9-4c6b-8db8-d9cbcf5506db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 108
        },
        {
            "id": "fc43b265-9e0f-4d64-95db-fc8ccebf2ace",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 109
        },
        {
            "id": "7c83aa1f-0fe9-47f1-8869-c6500770fd63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 110
        },
        {
            "id": "19c34fd7-0e7e-474d-9655-94b4ff31bf07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 111
        },
        {
            "id": "b6e87e54-a7a2-47b2-a85d-07935a6a4126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 112
        },
        {
            "id": "a2013448-9b2e-4594-b242-bc9693e39146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 113
        },
        {
            "id": "302a01a1-f66d-430b-81f9-3226727603c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 114
        },
        {
            "id": "7a64579a-19a2-4dbe-bd19-bc12d15e225c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 115
        },
        {
            "id": "89048daa-9b7d-4566-9e4f-5024a2a39f6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 116
        },
        {
            "id": "6ef765dd-da5c-4989-9ebf-5b679de9071b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 49,
            "second": 117
        },
        {
            "id": "b1ce65cc-e29f-4ee6-a047-a7c635727fcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 118
        },
        {
            "id": "9661333d-1c97-4c28-85cc-ad52b7410755",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 119
        },
        {
            "id": "ae2d3764-bdd7-4a56-9d87-e3ef74911a8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 120
        },
        {
            "id": "2400f10c-c141-4eb1-a7aa-d27ef1c549fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 121
        },
        {
            "id": "7e1d24c1-dd16-4086-9da1-2f06c0a493ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 49,
            "second": 122
        },
        {
            "id": "167e1145-5718-42a6-8ac9-e8bede5f8b70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 48
        },
        {
            "id": "a9a366e3-ee84-4abf-a5e7-5fa03fc80eca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 50,
            "second": 52
        },
        {
            "id": "4b98a440-e435-490d-9fff-78f839c47ab0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 54
        },
        {
            "id": "a80d2885-c2eb-41e8-9f01-ea8d95e2be0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 57
        },
        {
            "id": "66803ed7-a1a2-43bd-9b68-b3c403815857",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 66
        },
        {
            "id": "285bdf33-cce2-4250-9672-2fb5ba8bc896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 67
        },
        {
            "id": "4f68999d-dbbd-4317-8701-c0cc76695d28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 68
        },
        {
            "id": "c36fa21e-8f46-4472-b321-253cf5587c88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 69
        },
        {
            "id": "706919ff-3f46-4833-a790-fbc7d9e7094a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 70
        },
        {
            "id": "8c0a700f-3277-4c3c-9a11-3359a80f3eef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 71
        },
        {
            "id": "c7c7fc1a-ff6a-467b-be8a-80d878d981c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 72
        },
        {
            "id": "92b59692-b336-403d-b995-645ad1b0ce46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 73
        },
        {
            "id": "b6d29ec6-951e-4174-a232-09ff6ef35c09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 75
        },
        {
            "id": "eb4a5fd9-1f7e-4d4b-836d-c2ef96f19e48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 76
        },
        {
            "id": "f58f24aa-7a35-4bbe-a37d-7fea6e388d41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 77
        },
        {
            "id": "61faf086-1215-462b-9923-21e409a35b20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 78
        },
        {
            "id": "58db7ff0-ef7b-472f-9c30-d402509c526e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 79
        },
        {
            "id": "d39e73fb-7e93-4690-89fb-c6bb049fd55b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 80
        },
        {
            "id": "a46190b3-9fab-4e80-ad71-062015acc719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 81
        },
        {
            "id": "1b24b931-a857-4d34-a82b-7982579475a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 82
        },
        {
            "id": "aeb60a6a-24f2-4b47-94ef-1dba400b44b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 85
        },
        {
            "id": "940eaa30-c3d1-4fa4-a6be-c8649df82429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 86
        },
        {
            "id": "79d42a39-44e0-4840-85f4-c0455e2d42e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 87
        },
        {
            "id": "0966986f-8567-4c25-9aa4-b18bd7788e31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 50,
            "second": 89
        },
        {
            "id": "6f81a4af-ca0d-4158-b63f-0058dfa4205f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 98
        },
        {
            "id": "2cecc80c-30b9-48c4-b5b2-d0740a342e1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 99
        },
        {
            "id": "4b8f15c0-e4a7-4c04-9521-2098fac3351a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 100
        },
        {
            "id": "f85905c9-1e73-4717-af3d-d7494ac85db1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 101
        },
        {
            "id": "224cba9a-b94c-4fcf-b194-fba998bfc3e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 102
        },
        {
            "id": "0dc03220-3601-433a-a3ce-b1e5d10ccc46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 103
        },
        {
            "id": "a0d426af-f96e-4dba-aa33-ac0c6b054d1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 104
        },
        {
            "id": "382971f5-1508-4f57-ab22-dcc4b53818b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 105
        },
        {
            "id": "c4bd13b1-f158-4cfa-9e66-a5e46467aefc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 107
        },
        {
            "id": "872ca999-71f1-4b8d-956a-ce0647ceaeec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 108
        },
        {
            "id": "c14628d2-f8c1-42b7-8a79-95ddaa799d5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 109
        },
        {
            "id": "265a8ac4-600d-4d28-a316-b383d573f5c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 110
        },
        {
            "id": "1026908b-026a-4abd-b744-8d8ede7dafe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 111
        },
        {
            "id": "cc300c3e-ff50-4fe6-9b58-db8403be68da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 112
        },
        {
            "id": "1babda78-c114-4b01-bc74-d075a7042bf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 113
        },
        {
            "id": "8d7c5b19-6944-464b-bfc6-14964e2f52de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 114
        },
        {
            "id": "3723479c-c20c-456b-8f50-4ceb12a21371",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 50,
            "second": 117
        },
        {
            "id": "9eee7b0d-c86f-43ca-8c1c-d97598067273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 118
        },
        {
            "id": "84acdadf-5977-47f4-9232-3e72f8d8c7ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 119
        },
        {
            "id": "4f16af81-4c42-48b2-b7e4-625d60249864",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 50,
            "second": 121
        },
        {
            "id": "cfa4eaa8-9a30-4dbb-8a29-cb1bc8c62daf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 48
        },
        {
            "id": "58c3fe4b-2579-4edb-ae68-2766eefe2b36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 53
        },
        {
            "id": "00f3cfe0-6901-46a3-85a1-998b57571c01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 54
        },
        {
            "id": "4dbf789d-7698-49fe-a202-d498e6e73dc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 56
        },
        {
            "id": "df297bd4-4a84-4850-a3ca-29f35c4778ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 57
        },
        {
            "id": "30edfb45-6aee-4fd4-b274-519b28fe1a4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 66
        },
        {
            "id": "f0d34595-e768-4e72-9acd-7550df28b911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 67
        },
        {
            "id": "e0e1b0cd-0f65-482b-a8e8-61a0dbd667f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 68
        },
        {
            "id": "26be57ca-c93d-4ed0-be0c-930d7bc88e22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 69
        },
        {
            "id": "ff554891-2acd-44a8-b999-dde86bd46e31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 70
        },
        {
            "id": "edb174c1-c9b9-48fa-89f2-304324ecdcda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 71
        },
        {
            "id": "2850923b-7ac2-48e0-863e-3a66edcefc39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 72
        },
        {
            "id": "03130a4a-7d27-4f31-a47f-8a57565a7e46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 73
        },
        {
            "id": "ae7c4721-4546-4230-adc8-a7b963e0af87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 74
        },
        {
            "id": "d37b5317-f6aa-4bd4-9f04-2b5fe883a613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 75
        },
        {
            "id": "56c8bba9-bba2-48f2-bf6a-cee921196de3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 76
        },
        {
            "id": "cf1c8211-301e-4522-8664-2b7214a176c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 77
        },
        {
            "id": "298bbb68-aafe-45b2-86e6-7ee827a48ff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 78
        },
        {
            "id": "fbd3aa4c-9c63-408d-92e5-f90b5287e109",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 79
        },
        {
            "id": "ac458a96-f3e2-4eb7-bf51-1b5bc21bc536",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 80
        },
        {
            "id": "67474209-a967-491e-91eb-62e1ee117a2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 81
        },
        {
            "id": "ca5047a5-aadc-41e0-b8ad-122b0d0916e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 82
        },
        {
            "id": "3ca1cd98-234b-4a7d-92e2-d5c7da48b5a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 83
        },
        {
            "id": "efc7c1a9-5f9e-40d0-9f19-457fa5fbb433",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 85
        },
        {
            "id": "ff4cb881-fd6c-45d7-9fac-fe8fe2be893d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 86
        },
        {
            "id": "d54dc84d-e8dd-4aef-a7d5-a4aa251c3c95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 87
        },
        {
            "id": "233123f6-49e6-422e-b42b-030daed41e29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 88
        },
        {
            "id": "e6d214af-186d-4bbd-893e-870393d55609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 89
        },
        {
            "id": "05d6d899-922a-45d4-bea0-13d3c922f2fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 98
        },
        {
            "id": "d5c7d325-cb72-4244-b805-8024f375d195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 99
        },
        {
            "id": "7abca618-a492-44bb-8fb5-87c5d32b7bfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 100
        },
        {
            "id": "add3da65-5356-457d-b6df-6bc056d7633e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 101
        },
        {
            "id": "e9a189c6-7a66-4a5a-8ade-49890bab0c4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 102
        },
        {
            "id": "8303598b-ad48-4710-9cb9-90ea9b5e6485",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 103
        },
        {
            "id": "0723a38f-8abb-477c-89ad-d1f1cd781904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 104
        },
        {
            "id": "a68c9d91-19e8-4064-9459-ff8c60a6333b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 105
        },
        {
            "id": "d334acf4-9408-4c4c-b158-ad78965a1ed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 106
        },
        {
            "id": "b1e9ae89-1244-4d34-bb31-bbf13312ff19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 107
        },
        {
            "id": "4a79a135-c487-4515-8a11-be9e59a6e99f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 108
        },
        {
            "id": "edc583f8-d26f-443a-a716-604326fb8100",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 109
        },
        {
            "id": "bbecbe23-e5ce-48c7-bd26-68b27bacf93f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 110
        },
        {
            "id": "a71a4f72-700d-4ef5-a741-67221c7401d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 111
        },
        {
            "id": "f353d2c8-a763-482b-89c4-1dd593da0234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 112
        },
        {
            "id": "eccdd248-c6fd-4b1c-8519-d1174ed9e2f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 113
        },
        {
            "id": "b160a510-59ec-45f6-bca3-deda916711c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 114
        },
        {
            "id": "305fa07a-8331-4a7d-be43-f17238a377d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 51,
            "second": 115
        },
        {
            "id": "92c534d6-1f6c-48d5-be26-dff141efdce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 51,
            "second": 117
        },
        {
            "id": "b8396505-c2a0-457a-9ec8-b3cd9b621001",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 118
        },
        {
            "id": "b18244d1-bcd9-40e3-91d6-6cf20f34ad99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 119
        },
        {
            "id": "0e8cf476-b54f-44de-bcd9-160a254dbf32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 120
        },
        {
            "id": "7011e9a8-e83d-4991-86c2-dae6e8dff04c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 121
        },
        {
            "id": "7e5a43fd-e294-44c4-887f-d5317208d6fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 48
        },
        {
            "id": "105797b6-d474-4b0d-86da-177e4becdd59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 49
        },
        {
            "id": "49d8e64b-7a0c-4657-b8f9-3d7d65c17796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 50
        },
        {
            "id": "09093443-c47b-42e5-b046-61182941277c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 53
        },
        {
            "id": "8543c79f-0d5b-4c20-92a4-ce52003d8920",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 54
        },
        {
            "id": "ba9e2d1e-d751-47e5-890f-769dafc8d016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 52,
            "second": 55
        },
        {
            "id": "62958a48-b627-4627-a517-527edb62be99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 56
        },
        {
            "id": "f1f2c8e7-a194-4295-b08a-b27aff79382c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 57
        },
        {
            "id": "fb3df42c-8ecd-47f1-944f-f3446b1c9c52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 65
        },
        {
            "id": "737c3ce3-d820-45cf-8079-3047018d27cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 66
        },
        {
            "id": "186c7480-8e27-4475-bafe-9d3d45e7d9c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 67
        },
        {
            "id": "76c22c30-2842-4f0d-93f0-54523f7c1cb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 68
        },
        {
            "id": "f4a14192-1f97-4513-9446-3e569dcbc046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 69
        },
        {
            "id": "492c9edd-dd95-4a53-8e58-e961498ad0a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 70
        },
        {
            "id": "35c3e737-6cb3-49c1-b3c8-78f9beca1c5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 71
        },
        {
            "id": "4bfae280-dc5c-4823-aa32-dcfb3feef598",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 72
        },
        {
            "id": "a72b071e-ae6e-4581-baa0-dbd345fff235",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 73
        },
        {
            "id": "dd05028d-0af6-4786-be61-704ea0e6a3ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 52,
            "second": 74
        },
        {
            "id": "72be4611-e056-480c-88c1-cdbf9bf2f9c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 75
        },
        {
            "id": "2755cd30-756a-4707-824c-40f379d9cb1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 76
        },
        {
            "id": "d7eb9c71-6a20-4393-ae25-009f984e1c6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 77
        },
        {
            "id": "1b2bfb48-6266-494d-90a8-9248c0c13bb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 78
        },
        {
            "id": "80124017-6bbe-4916-857f-5dabccce5172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 79
        },
        {
            "id": "ffe5c392-9211-438b-9b84-2f3c4688a5e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 80
        },
        {
            "id": "a3b7375f-1433-48ac-8efa-39a10b85db3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 81
        },
        {
            "id": "3bdc0491-1982-4bcb-9c03-88461a0701e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 82
        },
        {
            "id": "6cc24eff-c496-4d63-ac71-57c0bd3eb97d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 84
        },
        {
            "id": "3959160e-4387-40a7-b018-82350fe31b46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 85
        },
        {
            "id": "8a2f139e-49d2-4521-b2aa-4b1415234ef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 86
        },
        {
            "id": "0696b293-153a-4c16-93e1-7ef4ba87f2a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 87
        },
        {
            "id": "d5adee11-2375-4aa0-8faf-ce3d5a88ccc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 88
        },
        {
            "id": "9bcc980b-cb2e-4c6f-b9de-b8bcf4e895e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 89
        },
        {
            "id": "bd0d669a-ac1c-4083-ab0b-9b9cbf0e3c33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 97
        },
        {
            "id": "04a621c4-c307-485a-bef7-efcedf4b14ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 98
        },
        {
            "id": "baa599c4-cb59-414f-91d8-ea126d6d5409",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 99
        },
        {
            "id": "5a64ba4c-2c8b-4513-a271-4f08ab69e1f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 100
        },
        {
            "id": "1b7d9adc-b375-45c6-9309-6d54052aebfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 101
        },
        {
            "id": "dd097af8-e2f6-4f53-b77e-323ddeb9fafd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 102
        },
        {
            "id": "a7cf09f3-0a8d-482b-aa23-ed019df0630f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 103
        },
        {
            "id": "9fcd0c60-77ef-4d10-99c5-5112a7fe0ddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 104
        },
        {
            "id": "bd8e2af2-60fa-4163-9c8e-27bc0fb42e2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 105
        },
        {
            "id": "699882c8-fff1-40f9-98bd-62f70632c445",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 52,
            "second": 106
        },
        {
            "id": "71b49c10-2cde-44e1-a5f4-dd05133d8aac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 107
        },
        {
            "id": "5ce6c346-3495-4e59-88b5-7514222cd681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 108
        },
        {
            "id": "4d996e56-81ee-45a5-a3b5-c9a31a66367b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 109
        },
        {
            "id": "87ade4ba-c6bb-4a42-87f1-7856eeeb3a22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 110
        },
        {
            "id": "aa5b3aa6-41bf-4323-b8bf-4d9118c4785a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 111
        },
        {
            "id": "305df7c4-f960-4c1a-9677-1378859e4760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 112
        },
        {
            "id": "7173e5ef-4d37-448b-a2f6-b3d697c1bbf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 113
        },
        {
            "id": "cca46687-98ec-4b64-b9dc-da99c9ad47bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 114
        },
        {
            "id": "bfb72378-db4f-42ec-9a14-08b90efb9fbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 116
        },
        {
            "id": "9e5564e1-da61-42f7-a26c-1cc650d2a4c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 117
        },
        {
            "id": "b03b6e7d-bb58-41b1-90a5-ac958081ef93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 118
        },
        {
            "id": "c6051d8c-b322-448b-9117-c595756b9f40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 119
        },
        {
            "id": "dae0690f-ef11-4531-8625-fb1605c98f3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 120
        },
        {
            "id": "be603f08-f6d7-438b-a4c4-60d8097bcec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 121
        },
        {
            "id": "c7aac53b-58ce-4e4d-999a-2901f15b45e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 48
        },
        {
            "id": "0f88bddf-ba54-4bef-933d-3f60cd424122",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 53
        },
        {
            "id": "ffabd831-2905-4132-be35-ba030a71beb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 54
        },
        {
            "id": "2549ffa5-c870-4358-bd60-1bd5f80cd24b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 56
        },
        {
            "id": "e428fe2e-c7c4-46ae-91c8-35d1dd0255d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 57
        },
        {
            "id": "3bd49796-89e3-4264-aea1-3c2c4ec55667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 66
        },
        {
            "id": "0102e520-4a3b-430a-a82e-f8d46dfd383b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 67
        },
        {
            "id": "6cd3f5d1-2704-48e3-ba53-1a0472ca4e0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 68
        },
        {
            "id": "b09ce8c2-98b4-4707-be79-ee68f19ccaa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 69
        },
        {
            "id": "ec98ae04-f5f6-4a36-b2c7-cd782e3c4722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 70
        },
        {
            "id": "330313a5-8b51-45b6-959d-f82c9bb2a2ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 71
        },
        {
            "id": "e7837bb3-1b03-44b9-922f-48e41bb32850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 72
        },
        {
            "id": "8a7044b0-4889-4de8-b148-1ca1afe90ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 73
        },
        {
            "id": "5976698c-fdc7-4fb9-b7cc-33afebe0aec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 74
        },
        {
            "id": "303ac0af-ca47-4af0-948c-80c70d20d9bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 75
        },
        {
            "id": "e55d0200-f9c5-4921-a046-f6a71dcb4585",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 76
        },
        {
            "id": "6ca4f4dc-5770-4ce8-bfb2-db11d7ee6201",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 77
        },
        {
            "id": "2a2b2f73-c400-456a-a989-b880e9eadd86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 78
        },
        {
            "id": "e95c57d7-3673-4140-b45b-77b764fc78f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 79
        },
        {
            "id": "cbad0d92-dd14-4503-8833-d957f47bc740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 80
        },
        {
            "id": "2039b4cd-a32a-4cf1-9b65-32b92f104fd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 81
        },
        {
            "id": "8062d6de-3a99-4224-a14d-231c95f61269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 82
        },
        {
            "id": "6cfefbbc-54d6-4c05-bebc-d352c5e5d309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 85
        },
        {
            "id": "63befc2f-aa18-43e8-9f3d-a6ffea0b8d0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 98
        },
        {
            "id": "88604d48-785a-48bf-8f9a-2b8ab9e16be5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 99
        },
        {
            "id": "596bf913-96fe-4329-8c0c-62b0f629f99f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 100
        },
        {
            "id": "8f073d0a-10f5-40ae-b0d7-362330762642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 101
        },
        {
            "id": "a71ba62c-403b-4195-ab97-1e7fba576f3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 102
        },
        {
            "id": "7ee41a60-b97a-4bda-b301-579a8013dbc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 103
        },
        {
            "id": "1141d8c9-4e08-44d7-87fe-566f38c16b94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 104
        },
        {
            "id": "915c1e65-4fd1-4beb-8405-6e18e4ef436e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 105
        },
        {
            "id": "bda24195-457a-4cef-af98-7dad03a13dbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 106
        },
        {
            "id": "74d4a0a1-1e07-42d6-95d1-38962c0c16fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 107
        },
        {
            "id": "6ea0dd63-dd62-4df3-8f2b-fa893a3a7633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 108
        },
        {
            "id": "47bcd996-d133-4f9e-bcb4-e4a7508e8828",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 109
        },
        {
            "id": "9e167e33-711e-4c35-a8cc-e62abb8d1a39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 110
        },
        {
            "id": "2ee9fdf1-792d-444a-b168-cab62855a903",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 111
        },
        {
            "id": "78cea903-7cf1-4146-b597-d69311d00ead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 112
        },
        {
            "id": "4b8a71f8-293e-4e6e-8629-55119df81f35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 113
        },
        {
            "id": "8b0eecbe-446f-4174-a582-a0606bcbacbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 114
        },
        {
            "id": "d74fd708-ad72-4b65-b0f9-d368084fd1f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 53,
            "second": 117
        },
        {
            "id": "f47947f5-b1ce-42a5-aaec-f227623c5a01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 48
        },
        {
            "id": "ad786065-a8cb-46f5-8302-51c435dbc948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 53
        },
        {
            "id": "a0a3d79f-132a-4ae4-bbc8-bd35aba47b0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 54
        },
        {
            "id": "c89eaec8-96ba-4294-9d8c-56884deea8bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 56
        },
        {
            "id": "630f9a6e-37e6-45c1-a037-39d42a09995b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 57
        },
        {
            "id": "2ee4b703-dfd1-4f91-a71c-fe6a6793c9b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 66
        },
        {
            "id": "bdd639f4-8987-47db-8b47-bfd84733cfad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 67
        },
        {
            "id": "c47ced63-189b-441c-a805-e507c76a194e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 68
        },
        {
            "id": "3ac20ba7-c545-4387-b1d7-b9597070bc01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 69
        },
        {
            "id": "7116acb7-5db5-4cb7-b83a-0d473d57a94a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 70
        },
        {
            "id": "a7cced78-f409-46c8-9448-97ba2986564d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 71
        },
        {
            "id": "69f30c7a-e0ee-46dc-9edd-ba9007455869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 72
        },
        {
            "id": "8656c5bc-4c4e-4114-81e3-03cc1f7f474f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 73
        },
        {
            "id": "07882bf3-cb0f-497c-9f95-cfa0c01d1716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 74
        },
        {
            "id": "47c1973b-00e2-4fed-a25e-205ea074eb10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 75
        },
        {
            "id": "07944922-e303-4378-8ed6-f86e7f8373ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 76
        },
        {
            "id": "ce1dd696-e424-4340-8215-6f160ddf8ca7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 77
        },
        {
            "id": "1b39737c-0339-491b-82ad-ae1edcb2cd38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 78
        },
        {
            "id": "78b9d32f-7da6-41e2-8103-e88ab4b3649a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 79
        },
        {
            "id": "6d76de76-462d-41dd-8884-775514b0a325",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 80
        },
        {
            "id": "3f6872af-9e03-4174-ab67-6f81ce57ef1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 81
        },
        {
            "id": "bcc226ca-de98-4740-a83a-1f745e7f8256",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 82
        },
        {
            "id": "0281cad5-72c4-4122-b50c-9ce386d6a6cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 83
        },
        {
            "id": "c590a684-c6dd-49c2-a56b-2738ae39dba8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 85
        },
        {
            "id": "47a5a30f-7e36-4c8b-9bff-850bca974638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 88
        },
        {
            "id": "55adaa05-86fa-46e8-899d-78c84e8fdbe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 89
        },
        {
            "id": "fc15ded4-7f07-418b-b286-726fcba104b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 98
        },
        {
            "id": "81d5c8d9-9d41-4868-aded-8c2112d1baec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 99
        },
        {
            "id": "3ad03be6-6f6b-4138-b3dc-42e3789eb023",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 100
        },
        {
            "id": "4d01f364-4fe7-4ce2-ac54-2c6a85d17dd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 101
        },
        {
            "id": "b03e0b91-79e8-420b-888e-6e7a344dc58d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 102
        },
        {
            "id": "488be0a3-16b6-4789-b9ef-92e3d9a8746b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 103
        },
        {
            "id": "c41db9ae-e679-4ec4-ab1a-23db442468a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 104
        },
        {
            "id": "d83006e4-a3cd-48c9-bd82-03ed5083f94b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 105
        },
        {
            "id": "885df1a0-77de-40ff-998f-122fb56fac92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 106
        },
        {
            "id": "9e9d96b1-634e-4452-9875-a376996cd562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 107
        },
        {
            "id": "f86bc298-cab8-4a4b-8796-40c2be270d41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 108
        },
        {
            "id": "8942214a-c10b-4318-8e9a-1db54a49140a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 109
        },
        {
            "id": "944ce899-ed85-4c0f-8ec0-bdd080cfe34b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 110
        },
        {
            "id": "c2199d17-2928-4ae6-a5b2-bec2b6678687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 111
        },
        {
            "id": "0016a36f-0d5f-4f68-8daa-4352cc9e4400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 112
        },
        {
            "id": "3b086a01-50e5-4776-a5a6-ccb454f36878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 113
        },
        {
            "id": "a137bb81-7ff3-452a-a770-447607ff2246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 114
        },
        {
            "id": "50d44798-2451-4764-8998-e759e0858dfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 115
        },
        {
            "id": "9c73a4c4-f5ea-46df-83ae-f097964de37c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 54,
            "second": 117
        },
        {
            "id": "0dabbda4-c530-4b82-91ac-6a454165ca16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 120
        },
        {
            "id": "8439128d-68f7-477e-97d7-3a264d4ad1a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 121
        },
        {
            "id": "4fa2d02e-c268-4f8b-bf09-1cbeba68fa3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 51
        },
        {
            "id": "114d5490-9739-413f-a034-7c9e8d55d568",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 52
        },
        {
            "id": "11799657-611d-4de8-a267-e8a93380c522",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 55,
            "second": 65
        },
        {
            "id": "63b74a74-8d3b-4ecc-8b30-eb8f25e94530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 66
        },
        {
            "id": "92dbe2bd-3aa7-4312-b7b7-12b182f1cb9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 68
        },
        {
            "id": "ba60a8da-59b9-497c-9bcf-c3b983a9fce6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 69
        },
        {
            "id": "2f9ff872-3cbd-447b-ac1d-7a5a810f55a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 70
        },
        {
            "id": "0ac6c9e1-7dbd-426d-a42a-b1274d5a8d3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 72
        },
        {
            "id": "65b62979-033e-48aa-a343-9a55ef6e78a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 73
        },
        {
            "id": "0e5c5e5c-0774-49a1-8a8e-50d40e6cf3af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 55,
            "second": 74
        },
        {
            "id": "948b8f52-7074-4143-83ce-3e9ad2d4732c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 75
        },
        {
            "id": "54da313e-47be-4620-b0c7-b958d3e76675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 76
        },
        {
            "id": "2d477365-e847-4f07-9898-a1e9a6905411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 77
        },
        {
            "id": "62d8b29b-930f-48ec-bbaa-7d23201898ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 78
        },
        {
            "id": "d0472b25-c6c7-4c31-8923-590455cf2ec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 80
        },
        {
            "id": "39f0ebf4-c02f-4aa3-a31d-f9762a24bd6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 82
        },
        {
            "id": "46574777-a37f-43ba-80ec-94243b4a0d96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 85
        },
        {
            "id": "06d76059-1c83-49dd-ab53-f46bd48192dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 55,
            "second": 97
        },
        {
            "id": "a20f0efa-431f-4530-a4f1-5ee06ff1a0f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 98
        },
        {
            "id": "6d59d709-5108-4ea2-91dc-9c196d6e7d0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 100
        },
        {
            "id": "6d23abda-2d8e-4cde-9c87-cda45e40cdb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 101
        },
        {
            "id": "ed10a05b-6569-4ea4-9518-20e5756a21d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 102
        },
        {
            "id": "d11764ed-d78b-42ac-aefc-877b8a6030aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 104
        },
        {
            "id": "f683c040-48c2-41fb-ac00-b35054a906d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 105
        },
        {
            "id": "5b19e370-5ec0-46c0-a9f6-852832a28f45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 55,
            "second": 106
        },
        {
            "id": "a00e57d9-e833-4f2b-a2a9-541a7520adbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 107
        },
        {
            "id": "28f07362-a57f-4323-9b22-f8478e27c908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 108
        },
        {
            "id": "80735726-c60c-449c-97ad-c7e89eb9dbab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 109
        },
        {
            "id": "116ab58a-e0b9-4eba-bcf1-6bc0af7cbdc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 110
        },
        {
            "id": "1ccb98f5-73de-4c56-962e-54ebc26d354e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 112
        },
        {
            "id": "c9e52cb6-5f1f-476b-a7c2-5cb7d20f5852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 114
        },
        {
            "id": "f9dd0b84-6ecb-4294-aba1-836d18ff6f9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 117
        },
        {
            "id": "29b0b63f-1b8d-414a-88f6-53a066671e51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 48
        },
        {
            "id": "1d128bfd-6001-4529-851d-416e5778a8a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 53
        },
        {
            "id": "f09a0e75-4114-4f61-92b9-e213722fa865",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 54
        },
        {
            "id": "8449afd3-3bcd-4b9d-a858-c5e871f30f24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 56
        },
        {
            "id": "7d48e442-46ed-4218-920e-a791185bea8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 57
        },
        {
            "id": "51c6f3b6-3135-4eaf-903d-7dcd3b5aa93a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 66
        },
        {
            "id": "9cc5d8fc-5e30-4362-82d8-97fb70c5d8d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 67
        },
        {
            "id": "61e49d26-5f9b-4a76-a885-fbd5084b0f45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 68
        },
        {
            "id": "6e7a63ae-68a7-43d9-9836-80bfc99d8f95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 69
        },
        {
            "id": "0058408b-b819-4914-b3bd-79b0e263eb64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 70
        },
        {
            "id": "d58337df-ed6f-47ae-a83f-3e9cc865b933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 71
        },
        {
            "id": "60c2052e-35bc-436e-ae66-ef7562c87bb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 72
        },
        {
            "id": "a39be5fc-b2d6-4029-854f-8e907a56d6ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 73
        },
        {
            "id": "47c7dc32-8741-4160-a96b-ba156df5da92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 74
        },
        {
            "id": "db751178-b458-4309-a7cf-7c638f5886a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 75
        },
        {
            "id": "e287bc11-a4d3-450a-bc7a-3590dce2b8c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 76
        },
        {
            "id": "2c953c8c-c9f7-4a9f-a6e9-bc0bb554ad16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 77
        },
        {
            "id": "1ac6fe4d-4338-48bc-8bb2-ca601216e8f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 78
        },
        {
            "id": "ee1cf0fc-ae5c-4af4-a830-07be412cfa0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 79
        },
        {
            "id": "8ff442b3-e372-408e-bb0c-5bff73226bcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 80
        },
        {
            "id": "6c140fa7-219b-4695-8d67-22f3676b330d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 81
        },
        {
            "id": "236a9c63-2569-4032-9866-7b5a02af9d92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 82
        },
        {
            "id": "6f03d4f9-b889-4625-b2a1-bf0c177c1f86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 83
        },
        {
            "id": "2080c6c1-46ad-4cf8-bf9e-0c0931c49a9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 85
        },
        {
            "id": "f8b602b5-a012-4ba9-9e0b-25e93d9e58dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 86
        },
        {
            "id": "68b185a4-89c9-44a9-8fb5-ebdcf355f8d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 87
        },
        {
            "id": "b291af75-0218-4ca5-a589-bb5907257e0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 88
        },
        {
            "id": "37de33d6-75c9-4426-897d-cfde136c17dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 89
        },
        {
            "id": "610e4522-8a18-4e77-8cae-eb8a9622cbad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 98
        },
        {
            "id": "199d8661-7fee-4519-8e56-4969fc58e093",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 99
        },
        {
            "id": "3971981e-0ede-4a15-ae4d-94bed545f174",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 100
        },
        {
            "id": "7728fb53-6111-42ba-b125-8fc5a0353c25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 101
        },
        {
            "id": "e3fed504-e655-4701-a9a3-2d7e2331ae6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 102
        },
        {
            "id": "9645429b-db75-4ff5-899f-26b0fd7fd289",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 103
        },
        {
            "id": "a1c52acc-70dd-459c-9a41-72bd962e6707",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 104
        },
        {
            "id": "946c752e-f0b1-4384-b52c-a8b239ca39e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 105
        },
        {
            "id": "611b7faf-d3ae-4c2b-8f41-d814f0358ef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 106
        },
        {
            "id": "01685f01-3c3d-4be7-8db7-6065db3b0640",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 107
        },
        {
            "id": "e8d7d17b-01d8-4527-bd86-0540d2e6973b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 108
        },
        {
            "id": "fa123409-3539-4940-90c9-317535854c25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 109
        },
        {
            "id": "8150a81a-2154-4234-b8eb-ec5535730b3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 110
        },
        {
            "id": "749f63c2-d0e2-4f44-91c0-3670072e1a44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 111
        },
        {
            "id": "ed014fdd-0447-46b2-8228-57de1d7e31ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 112
        },
        {
            "id": "02c3fb85-850a-4448-8440-a6344521d75a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 113
        },
        {
            "id": "23f7043c-4673-493a-842c-90162c8aca4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 114
        },
        {
            "id": "b1d0b7b4-a6de-4000-97d2-2f04a55bdade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 56,
            "second": 115
        },
        {
            "id": "e9cc989f-32d5-4062-982f-4bac825faaba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 56,
            "second": 117
        },
        {
            "id": "e5ee00c9-4f40-4903-87aa-15cd917289c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 118
        },
        {
            "id": "2b772740-049f-4263-b083-7fcd21b17182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 119
        },
        {
            "id": "249430b9-547d-4ea3-9fb8-cba663e10933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 120
        },
        {
            "id": "b63eb69f-48b1-4385-8d07-44730b41b50d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 121
        },
        {
            "id": "da7cfec1-cebb-4b08-88c3-b90b1a0b6bb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 48
        },
        {
            "id": "df2c19dc-8870-4307-90e6-b2f54979a9ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 49
        },
        {
            "id": "c61f1038-fc02-4296-8d46-dd0faef5a2ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 50
        },
        {
            "id": "95540301-86b7-42fb-a187-ca0d9ddf2e63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 53
        },
        {
            "id": "3969739a-ed22-422d-8209-e135f6dedbce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 54
        },
        {
            "id": "e4f752d2-bc02-4f41-abee-854ed575838a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 56
        },
        {
            "id": "3ee55867-4554-4c60-8bb2-d4f85824c4ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 57
        },
        {
            "id": "a7783293-3602-49c4-b617-41fc5dcad9e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 66
        },
        {
            "id": "6037b1a1-0f77-4ed1-8c2c-e0d5b18a5c58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 67
        },
        {
            "id": "27030de5-a2e3-4825-b8f6-256dde761f08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 68
        },
        {
            "id": "60d7bf74-f389-46ad-8a49-e155743bc00c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 69
        },
        {
            "id": "cbbde550-df2e-4be7-8ff0-205e41baa248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 70
        },
        {
            "id": "b8443244-a73c-4a53-9316-5cbc21f157d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 71
        },
        {
            "id": "886fd351-6bce-4a69-87dd-2e4cf84e40cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 72
        },
        {
            "id": "1e8099e3-f71b-411e-b55c-8bb839e78ffc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 73
        },
        {
            "id": "b6af2593-5526-4a53-b12b-bc6bdcfa5c59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 75
        },
        {
            "id": "d5fd5882-71a9-4d6a-a092-871d304bfda3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 76
        },
        {
            "id": "d59e743b-6f9a-404f-bcf4-cba76c586fb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 77
        },
        {
            "id": "3e576b9b-e575-47fb-96c2-86e5bb5690d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 78
        },
        {
            "id": "5f3f7962-7cf2-4f28-a3f0-6830c311f081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 79
        },
        {
            "id": "773ba86c-acee-4b8f-96f3-1fae5fbc68d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 80
        },
        {
            "id": "2f8a291d-255a-4d6a-9500-f3ef2cfbfad7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 81
        },
        {
            "id": "bb225790-128a-4700-a0cd-6fa67609e95a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 82
        },
        {
            "id": "6e0c40f7-41ae-4b2e-8054-e8c543ec99ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 83
        },
        {
            "id": "2c1c8754-5275-40de-9b41-a0944da57b1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 85
        },
        {
            "id": "68b4c1d0-798d-40e3-b545-d98d8282abca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 88
        },
        {
            "id": "a90a87c0-c498-4de9-a89d-e84c3d05251e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 89
        },
        {
            "id": "73381917-101a-4323-ae94-5fbd3ca65b66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 98
        },
        {
            "id": "55eadec1-4c9d-4fd3-94a6-c96b3cacbf49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 99
        },
        {
            "id": "1039cedc-82ec-4758-85c5-ad2f9384f8b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 100
        },
        {
            "id": "a950aa9c-c6e0-49b3-b2c0-b6c6c8487937",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 101
        },
        {
            "id": "62c42947-e244-4e16-84d3-c94d017c162c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 102
        },
        {
            "id": "5f683c53-af41-430d-aecb-30cd80b0676d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 103
        },
        {
            "id": "cdd2b9f0-6ef0-4b7e-a8d9-5869d8a3de9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 104
        },
        {
            "id": "6615a092-005a-4e5b-bcb4-a50facbe45b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 105
        },
        {
            "id": "cc7a9eff-5128-455c-965c-ab33a81d5224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 107
        },
        {
            "id": "201349c0-57e2-4d2f-a4df-43c0a7c84bfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 108
        },
        {
            "id": "30b357ea-e2e4-4fcc-a002-a8712bcb4af1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 109
        },
        {
            "id": "8383b6f2-6247-4cf0-8f04-d975b851b8fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 110
        },
        {
            "id": "a61a6b1f-7610-4fa2-b5c9-cef5a62cdd2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 111
        },
        {
            "id": "4ba8cf71-d158-498c-9a0f-24460be1df07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 112
        },
        {
            "id": "59fe369a-5582-475a-b397-e62a0b3f0cb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 113
        },
        {
            "id": "2e09e79c-8dab-43af-bd46-57cc8b72e038",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 114
        },
        {
            "id": "27b42632-c7c3-4324-ad12-66eb47b4f676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 57,
            "second": 115
        },
        {
            "id": "6861c86c-8986-439b-8e08-2ac5a859ae16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 57,
            "second": 117
        },
        {
            "id": "8f4e28e8-b96a-4a8f-8e7e-2e24b88a793f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 120
        },
        {
            "id": "46e1a6f1-7ac8-471c-ae62-ca0d5d6ea0ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 121
        },
        {
            "id": "d76666aa-38d5-450d-b903-cb564f976c9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 49
        },
        {
            "id": "f7feb4f8-f878-46d0-aa47-827f7e10091f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 51
        },
        {
            "id": "14c86b83-16fb-40be-8500-5f94280d4c26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 52
        },
        {
            "id": "adea43d8-b811-4651-b77f-9a96605fcebf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 55
        },
        {
            "id": "9d77b13f-2a11-4474-9758-b05d9b046411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 66
        },
        {
            "id": "e2782974-10c0-4ad8-96a7-8102973a5b7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 68
        },
        {
            "id": "8356d5fd-002d-4b50-93bf-d204e3d72175",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 69
        },
        {
            "id": "00bc7f33-1c96-49e4-885e-07d57332005e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 70
        },
        {
            "id": "5aaba5ff-769f-4131-9de3-3593fe97460d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 72
        },
        {
            "id": "16e74c9f-bd86-449d-97b8-3ced63f67517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 73
        },
        {
            "id": "669048aa-3893-4fdb-a4fd-c8c072592729",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 75
        },
        {
            "id": "55822262-bf46-483b-8327-076c0646824b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 76
        },
        {
            "id": "61a705d0-73a6-4c99-8a62-59001ce5914a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 77
        },
        {
            "id": "624c2971-17da-4df2-8769-669ab30caafd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 78
        },
        {
            "id": "1c0eda69-e25a-4739-9c93-f0d1e735692f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 80
        },
        {
            "id": "6d346904-8233-409a-91d1-cbac9032e0df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 82
        },
        {
            "id": "ac663c52-1a96-4817-af74-16656044a428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 83
        },
        {
            "id": "cd8c0ad7-3fb0-42e6-b8d8-cf885b4bebcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 65,
            "second": 84
        },
        {
            "id": "50b42117-5bca-4121-b389-d9e893b5345b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 65,
            "second": 86
        },
        {
            "id": "9a803a4e-fc6d-4513-8048-a1cbe9730927",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 65,
            "second": 87
        },
        {
            "id": "d01c87c4-458a-428a-a577-870216f6a324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 65,
            "second": 89
        },
        {
            "id": "d584b722-2e0f-4a54-a679-d498d1d3e420",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 98
        },
        {
            "id": "6922629d-299d-4ed4-9542-7f5493523312",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 100
        },
        {
            "id": "a7e0df0d-e787-4a7b-b737-61f82b8423c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 101
        },
        {
            "id": "3a487d15-d9b4-4861-ae27-da224f117e95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 102
        },
        {
            "id": "76aa45dd-9dcf-4c27-be2f-df5bacc15f62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 104
        },
        {
            "id": "a1aff9e7-3044-4973-b66c-d005fe069324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 105
        },
        {
            "id": "7fe46b07-008e-4477-a580-65ebaaa0aaff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 107
        },
        {
            "id": "8ff65756-d645-42dc-9097-49bd58205c67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 108
        },
        {
            "id": "d6ee5f6c-1bd5-4393-b07f-e7bf3a49f57f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 109
        },
        {
            "id": "3ff3385b-0fbd-4879-a0c1-8cfee3791a2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 110
        },
        {
            "id": "381afc23-8785-427b-8a28-018be290f308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 112
        },
        {
            "id": "988f1a69-05c2-4951-8aa1-48995a3935bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 114
        },
        {
            "id": "40886d5b-acb3-4ccf-850b-a5bd429ce27c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 115
        },
        {
            "id": "635ee5a3-0f50-4b74-8ba1-d34cecb289cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 65,
            "second": 116
        },
        {
            "id": "2772e072-bd56-463e-827e-2ceb9c67c920",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 65,
            "second": 118
        },
        {
            "id": "b9ed4c00-d4cd-45ca-bb11-f201291b73ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 65,
            "second": 119
        },
        {
            "id": "56def99b-a9ce-40b2-906d-1968f2baece8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 65,
            "second": 121
        },
        {
            "id": "6fa48b39-dfbf-4604-9e6c-04c844b0e047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 48
        },
        {
            "id": "56f916a2-950a-433b-9770-b263223ca131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 53
        },
        {
            "id": "d28f435a-a800-4cfa-8a41-efe3d5bab084",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 54
        },
        {
            "id": "f2244202-8cf6-49cd-9eaf-183e34863673",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 55
        },
        {
            "id": "fe310f18-7365-4a0b-94f6-81c9fc507706",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 56
        },
        {
            "id": "495d7486-da14-4706-a4c8-7170345e42df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 57
        },
        {
            "id": "cd9c616f-a36f-4b89-b60d-1d63bbe7680d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 66
        },
        {
            "id": "08229a14-1b1d-462b-8bf0-5eadc5c57867",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 67
        },
        {
            "id": "6d4d587f-9644-4362-8d7a-251c58ca5b6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 68
        },
        {
            "id": "823b4860-f52f-4410-b48b-fd1e2b9820f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 69
        },
        {
            "id": "a8dde2c7-8ff0-4782-96a7-ea9c2947e3a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 70
        },
        {
            "id": "df2bd005-d157-4301-bead-c18cef3fcacd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 71
        },
        {
            "id": "05e7385f-ecca-4b8e-9ba7-5bdab5d0c93b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 72
        },
        {
            "id": "beca1898-4075-4058-b062-f0b16fae358a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 73
        },
        {
            "id": "4ebd5cf0-5f4e-457f-a33b-f3e0cdfb19f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 74
        },
        {
            "id": "da51e8a0-4a8e-42d5-b936-5a21662e67a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 75
        },
        {
            "id": "76ea3313-b52f-4a3f-9574-d34e90855f38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 76
        },
        {
            "id": "7ba7decc-ce30-4ea9-940f-671bbafb5df0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 77
        },
        {
            "id": "ded5c06d-be7d-40b0-9752-ee243763ffcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 78
        },
        {
            "id": "a1b96e27-b55d-44f8-b34f-f9bef447ed06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 79
        },
        {
            "id": "3f6823de-9c66-4dc7-b1dd-f03ac8a1c301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 80
        },
        {
            "id": "eef2ee33-5ce3-4c26-ac97-c451f430fed8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 81
        },
        {
            "id": "a2da303b-bd3c-4b45-bb59-89de554b61ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 82
        },
        {
            "id": "ce755cb4-1e3c-42d3-ab76-74baa7b539d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 83
        },
        {
            "id": "bf26233b-08d1-46b3-884e-f699ab822114",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 84
        },
        {
            "id": "de075b18-227a-4504-9e11-7dbb85e87eba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 85
        },
        {
            "id": "f2ee6467-528d-4684-aa2b-7092411ba4df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 86
        },
        {
            "id": "a409f90e-6083-4fb0-824c-3cc56adb4394",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 87
        },
        {
            "id": "399ad5aa-8f21-463e-b1bb-d41839bd3d73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 88
        },
        {
            "id": "4cc296e1-cd8f-463e-ae1b-e96094067a76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 66,
            "second": 89
        },
        {
            "id": "5e4a659e-5e87-4d44-9d0d-a4930523e98c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 98
        },
        {
            "id": "a1e25d53-7831-49d9-97ec-a87aa1784937",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 99
        },
        {
            "id": "61403502-89b2-4164-8a6d-acbb65af2d6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 100
        },
        {
            "id": "d8eb3bcc-50e4-4f02-8a98-96dc24db225e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 101
        },
        {
            "id": "a36e3503-0304-4224-8e8f-42797f235e5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 102
        },
        {
            "id": "38c16d35-1af5-4c8c-9700-9a1e8c1b5ec0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 103
        },
        {
            "id": "93542f0d-a263-4640-8ef9-60d51c627b7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 104
        },
        {
            "id": "fc1b5ade-7ebc-48df-b315-4d10bab7a38a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 105
        },
        {
            "id": "c4e48394-f807-496e-b9e6-3cf3356c0928",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 106
        },
        {
            "id": "55326dcb-9f4b-46a0-b826-0f9bade0401a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 107
        },
        {
            "id": "0328fa23-ba65-4d67-b4b7-e3c82ac914ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 108
        },
        {
            "id": "673ef8a2-58e1-42f5-a45e-cfd5859823c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 109
        },
        {
            "id": "3aacb467-c7c8-4ce6-99a1-beb728105ca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 110
        },
        {
            "id": "013f9476-dfb4-43b2-b7d1-0da94a69c3a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 111
        },
        {
            "id": "f9e92a05-67dd-47d0-8faa-1a40a392041d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 112
        },
        {
            "id": "596a3fbe-f6d1-4526-872a-e49f89bc3c06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 113
        },
        {
            "id": "3911b0d1-63af-458c-81ad-9ff642c7cdd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 114
        },
        {
            "id": "859108d8-5b04-4f6c-b953-4facc38ba29d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 115
        },
        {
            "id": "e69978f4-45f4-4e5f-b3af-e5956fdb7071",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 116
        },
        {
            "id": "a16f14da-d63b-447c-b024-f4110489a582",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 117
        },
        {
            "id": "ecf6614c-a66e-4d59-a89e-955d24194338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 118
        },
        {
            "id": "6af20df0-4616-4e92-ae96-652620ba236f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 119
        },
        {
            "id": "184a6b81-d4b8-44e1-bec7-ec0d6b23603c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 66,
            "second": 120
        },
        {
            "id": "dda2b04e-a503-4399-a313-6b06877f8373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 66,
            "second": 121
        },
        {
            "id": "e97100f8-ca18-4a7b-99a7-add66bb67873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 48
        },
        {
            "id": "4732b19b-284c-489c-b3ce-792ca0498255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 54
        },
        {
            "id": "c44ebfb2-f962-40d6-be0a-b8e9b20ea31f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 56
        },
        {
            "id": "f33d07c4-2d1f-47ca-9f3c-854034030082",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 65
        },
        {
            "id": "912c6e26-14f4-432e-b5ff-a2f6bdda7ca5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 66
        },
        {
            "id": "65583d4c-7b9b-4113-be60-30bf78bfc815",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 67
        },
        {
            "id": "e360eeaa-a75e-46e8-8d4f-4b9490943e35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 68
        },
        {
            "id": "a6fd94fb-dd2f-46c9-9c0d-c41e13823923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 69
        },
        {
            "id": "8c0d1855-9983-4419-aee2-d4e2f55bf30b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 70
        },
        {
            "id": "ea107549-2563-437c-a676-87c974aff7c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 71
        },
        {
            "id": "10cd310c-8515-4532-9a61-4f0b2333ef43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 72
        },
        {
            "id": "9f1abc10-0f6c-4796-8202-bb7d6944885a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 73
        },
        {
            "id": "dce71e5f-89ad-439b-a46c-a4255fe221e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 74
        },
        {
            "id": "c559c9d7-6a2b-4e39-a8af-a324ec24f650",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 75
        },
        {
            "id": "87b6436a-ea72-4e48-8172-fe0ce26060a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 76
        },
        {
            "id": "26e8b674-960f-4a4b-8972-e87d98b422bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 77
        },
        {
            "id": "e5fb18d3-f519-4a31-875a-fc97b196626e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 78
        },
        {
            "id": "eace055a-3408-44c1-bc4e-ff59b875b539",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 79
        },
        {
            "id": "44d655d8-a917-43c8-8acf-aab31860bb84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 80
        },
        {
            "id": "64f9083e-fdf2-4387-996f-eac75af3a655",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 81
        },
        {
            "id": "31b98714-1f0d-4de2-8997-36709fbb2d16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 82
        },
        {
            "id": "e4503931-c429-4782-bbb9-6b971c2c0857",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 85
        },
        {
            "id": "4237ca03-c6ce-4ce3-a833-d8602946b184",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 86
        },
        {
            "id": "087acbc9-41c2-410d-8eb6-af1f32ffc9b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 87
        },
        {
            "id": "41eaef26-8b98-4c5e-84c2-0dfd9f557a75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 88
        },
        {
            "id": "a053edc2-6485-4bb8-8220-f3e1bd498c1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 89
        },
        {
            "id": "dcab061d-161e-4fb7-bb31-aedd5df80270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 97
        },
        {
            "id": "142d1f69-43b5-4e59-9b7d-80898b94821d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 98
        },
        {
            "id": "f63e25b5-8bc3-4718-b9cd-37ff5ad7294e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 99
        },
        {
            "id": "cbfd49ea-bfc8-4868-883e-633b6d370801",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 100
        },
        {
            "id": "cb4f5f2b-c740-46a1-882f-e34be9de0df3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 101
        },
        {
            "id": "ec0ab2f4-ba24-4bf4-8a26-492269d9f065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 102
        },
        {
            "id": "ae7fdd08-b378-4e36-bf83-d6289b45909b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 103
        },
        {
            "id": "eaafd283-5455-4d1a-97a9-18551165dd77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 104
        },
        {
            "id": "2ea06db6-7559-4dcb-b992-b500872b0477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 105
        },
        {
            "id": "36d9feac-252b-438a-8e6b-cdb8731fe913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 106
        },
        {
            "id": "c3a22f06-7858-4235-8492-d549970eff00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 107
        },
        {
            "id": "6ad597fb-1ec8-42b1-b70d-e6cba4bf9132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 108
        },
        {
            "id": "6c8916d8-a427-4451-9e43-ae6c95d75eba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 109
        },
        {
            "id": "1f84445b-4ec0-4799-aa93-c61a9ddb8241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 110
        },
        {
            "id": "dca583a6-2760-4273-bbea-3520cd85bd31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 111
        },
        {
            "id": "0630527d-e952-4aa5-9b76-0c83841c8730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 112
        },
        {
            "id": "f072b156-1d98-4f3d-bb05-4c62364fd1e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 113
        },
        {
            "id": "68ad61d4-5b13-492e-94b8-3d5d537b1647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 114
        },
        {
            "id": "e9cba132-37b3-4fe3-8598-06cf1bcbe4e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 117
        },
        {
            "id": "a87096ef-5143-4b4e-9823-8355edff6a99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 118
        },
        {
            "id": "8f8285c6-e0cf-4481-8263-795972eec776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 119
        },
        {
            "id": "e0c96e28-316e-4443-b310-e4655ed00ea7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 120
        },
        {
            "id": "47610e48-0176-4e9e-a5cc-66fd82b439ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 121
        },
        {
            "id": "072094a9-9814-476b-ae8d-3ca06e4c1c27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 48
        },
        {
            "id": "e84040d1-4e68-4cc2-9c27-15afae3b2488",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 49
        },
        {
            "id": "80a4f9b0-708a-4c99-8ead-1d55f612dfbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 53
        },
        {
            "id": "5de2c4ab-d0c4-49f4-9d1c-b76f495afd41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 54
        },
        {
            "id": "0e974e20-7ea9-4470-9edb-1c64278bcf92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 56
        },
        {
            "id": "ae3e1085-fa13-46a7-a70e-85819f6b0945",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 57
        },
        {
            "id": "45ed034b-9fdc-4cee-a9f6-ecbc356db980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 66
        },
        {
            "id": "99bd0d3a-b21a-4539-868a-a475d8cbf071",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 67
        },
        {
            "id": "bfb376ff-53bc-4b23-a293-b0040756fac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 68
        },
        {
            "id": "d7f2a31a-de34-4470-bed8-c73f6d9b6e37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 69
        },
        {
            "id": "28124496-b700-43b6-af3f-9cebf7e566a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 70
        },
        {
            "id": "5b32bbcc-e2a3-454b-bbe3-ff84af98b780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 71
        },
        {
            "id": "fd6c7d57-3503-4860-b052-33bbbb1b1403",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 72
        },
        {
            "id": "0cb9aeef-de72-43d7-b83d-7058c18834a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 73
        },
        {
            "id": "610234cf-7de8-4aca-be9d-b31947e00799",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 74
        },
        {
            "id": "e5eba24e-97e5-499d-8338-e832159fefbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 75
        },
        {
            "id": "fae7a53d-c537-42b3-a6fd-2f12be73da1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 76
        },
        {
            "id": "1bac2c4e-9f16-423f-b327-388efd1b0ae0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 77
        },
        {
            "id": "bced3a33-f9c5-4962-8d51-45a9234276e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 78
        },
        {
            "id": "f31a97ab-5b8e-4bca-81f2-abfb1ef5daca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 79
        },
        {
            "id": "bb08f5cc-e195-4e4c-a35f-b184d315da3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 80
        },
        {
            "id": "da9bfd55-6f2b-4a14-8c3c-e325ae3d0393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 81
        },
        {
            "id": "80f6c7ef-ff95-49f9-b4ea-a64104dd8c78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 82
        },
        {
            "id": "d124b7a2-5273-4dad-9c55-ebc69d29247b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 83
        },
        {
            "id": "309a319a-686d-4f80-8677-0b4c8fba7551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 85
        },
        {
            "id": "069587c6-eba4-4022-9896-22e8b32a09d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 88
        },
        {
            "id": "97269e61-8c32-486c-9795-5804a984d607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 89
        },
        {
            "id": "0ce1485f-0ca7-4828-b1df-36a130581422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 98
        },
        {
            "id": "b62a1642-b653-4b47-a3db-a6ad78cd4ce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 99
        },
        {
            "id": "0dd5817e-592b-405b-b7bf-b687151907e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 100
        },
        {
            "id": "3fe56f5b-bfad-46f7-9acb-ca77ea79da11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 101
        },
        {
            "id": "1c34ab69-ee33-4523-b236-5bd9e2e323c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 102
        },
        {
            "id": "89da7778-356f-454d-a49f-10953cfd5da4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 103
        },
        {
            "id": "b057d27d-13ed-4c4b-b141-17d5214e0867",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 104
        },
        {
            "id": "82d0713e-1a3e-4abe-86d2-48d12d7d2ba3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 105
        },
        {
            "id": "5840bf85-5769-4166-b578-1db5ceb03079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 106
        },
        {
            "id": "58207df6-e047-4510-8e7f-b6a8e8f92cc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 107
        },
        {
            "id": "bc5c800a-a6ce-4f5f-92a9-c52b26b95abf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 108
        },
        {
            "id": "bd69fe7d-3000-4919-8e5a-cbf83293591d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 109
        },
        {
            "id": "dd7dbf78-d553-458f-a53e-9b63ad14b55a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 110
        },
        {
            "id": "ad179a1b-f0ad-433c-b04b-c9227149dec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 111
        },
        {
            "id": "c7f19112-b389-4be7-975b-701d43bb68d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 112
        },
        {
            "id": "c5398aac-aed0-4a05-af16-6ea654c3b1c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 113
        },
        {
            "id": "f1d5160b-e532-4d6c-b90b-ab1f68404ce8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 114
        },
        {
            "id": "232c467e-4fe8-416d-82e7-24fb1b5960d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 115
        },
        {
            "id": "6c26d183-c6a4-4013-b735-eea898f045b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 117
        },
        {
            "id": "643c1902-1807-435c-824d-893012d337af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 120
        },
        {
            "id": "07019323-bc0c-448e-9f83-9df5e8232bfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 121
        },
        {
            "id": "46abe67b-1b62-4560-becd-4825cacf8fce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 69,
            "second": 52
        },
        {
            "id": "8b0b93af-adc6-4d90-98de-f4e71abed238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 50
        },
        {
            "id": "3dd1e983-4858-4652-b857-1eaf420308a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 51
        },
        {
            "id": "e27c3cd8-e2ae-47c6-a338-4536fb83af90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 52
        },
        {
            "id": "b79d4f4c-ba88-495b-944a-cf261b6fadde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 65
        },
        {
            "id": "0a0c31b5-53d8-43ab-8efb-fc134b62e68d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 70,
            "second": 74
        },
        {
            "id": "951c2475-ac11-48cd-880f-dd6327653b21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 83
        },
        {
            "id": "1f4ab00a-6527-4311-aac0-cffdafa843e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 70,
            "second": 97
        },
        {
            "id": "09c93713-6e13-4d31-a269-ffd3a32e59ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 70,
            "second": 106
        },
        {
            "id": "d4dbc60f-cb5f-4bd9-836d-470ddc6683c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 115
        },
        {
            "id": "a7561db2-1a98-4a78-9984-ef6d9bb65e8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 48
        },
        {
            "id": "96386233-3157-40f5-9eaa-f8c3edbb98bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 53
        },
        {
            "id": "141a5306-0d7e-4604-911d-541c4185cb51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 54
        },
        {
            "id": "cbc0f766-eacf-4ee4-b41a-ca9d0b8860cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 56
        },
        {
            "id": "67f8792f-20b1-495a-bd87-d6298ac18fe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 57
        },
        {
            "id": "88e99ccc-6f66-4df8-838f-3d635685467d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 66
        },
        {
            "id": "853928d7-16ce-40b4-bbe6-cad1f2398806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 67
        },
        {
            "id": "5b516876-e7a1-4af0-9ae9-a6c1bcbe7743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 68
        },
        {
            "id": "ce56c362-3f4b-45ce-a2f5-dc9f6d3915d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 69
        },
        {
            "id": "af2c4ecc-b77f-4199-8866-de93cfd8450b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 70
        },
        {
            "id": "a6e7f871-8440-49d2-803e-796e4a6eb052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 71
        },
        {
            "id": "4300edd2-f5d0-405e-93c5-b835711b35bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 72
        },
        {
            "id": "da670124-9fe2-4d6c-9916-73892d4c293c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 73
        },
        {
            "id": "06b59da7-bb6b-47ff-80b7-e36bed90c088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 74
        },
        {
            "id": "51a879c7-162d-4a2d-b240-6d9efe2c1702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 75
        },
        {
            "id": "abf0b255-357b-4e94-9cf1-b89b9bbe702f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 76
        },
        {
            "id": "cb6e3dcc-1cfd-4371-a8fe-1768e2690a8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 77
        },
        {
            "id": "82f3ce7f-a513-431c-b19c-83e12b63bb65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 78
        },
        {
            "id": "5c308351-6386-4930-b07c-4e5a3434a3d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 79
        },
        {
            "id": "2981d6a5-bc05-4983-9ce8-1f504744c2d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 80
        },
        {
            "id": "3bea8240-4146-4872-b9aa-61c90ad9119e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 81
        },
        {
            "id": "18cb6c60-4c6c-4164-88a7-fe59781000d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 82
        },
        {
            "id": "b88d34d0-9fb6-46b0-b244-7ed5103048ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 83
        },
        {
            "id": "eaf1da4f-9c93-4da2-8795-4b30901b9ff3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 85
        },
        {
            "id": "83a9b583-8c8f-4d7e-8eab-b02222deb9af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 88
        },
        {
            "id": "88a308af-987d-4441-bae6-3d7be148f37d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 89
        },
        {
            "id": "791c4639-5e12-454b-96e6-e74428eb7d46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 98
        },
        {
            "id": "4a125e35-2efc-41da-a95a-2c3b275899cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 99
        },
        {
            "id": "32c2dd1b-5942-4635-840b-5c3bb9e46c38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 100
        },
        {
            "id": "3c69ed93-191b-47df-9384-48e86f3ca530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 101
        },
        {
            "id": "084d7bd9-0128-47dd-bc02-038b858a337c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 102
        },
        {
            "id": "6fd0b09c-d789-4d14-a61b-fd0932135b4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 103
        },
        {
            "id": "ef2fae68-aa84-48aa-a6af-11746b367792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 104
        },
        {
            "id": "39748775-df80-4a2f-bf00-0a1973be1572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 105
        },
        {
            "id": "2c1ae4b2-cdb6-40ca-898f-b130f865433d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 106
        },
        {
            "id": "f5721aad-8253-4b12-8eb8-52e36d7dd34d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 107
        },
        {
            "id": "59ff6b5d-0a3a-49dd-b714-383f958c0c77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 108
        },
        {
            "id": "c75a98e9-5ebe-43d2-9fb0-8554e2b5e4ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 109
        },
        {
            "id": "721e36bd-c4a7-4dbc-83e2-86203a42842c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 110
        },
        {
            "id": "61be36ad-7396-4ead-a594-eea5b60e0bef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 111
        },
        {
            "id": "8f2f53e1-5d58-4c7f-b614-e45b84631285",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 112
        },
        {
            "id": "eccfb8e0-48e3-42dd-9a37-9e2d1d1748ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 113
        },
        {
            "id": "f7d5f58a-d441-46ff-8963-342eaee59eda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 114
        },
        {
            "id": "62e851e9-a2b7-42f5-91af-0a22b267d472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 115
        },
        {
            "id": "390cddd4-3d7a-4462-88ef-edde6ed4e9ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 117
        },
        {
            "id": "a564085e-e956-44bb-80d0-3d7d19cf8c97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 120
        },
        {
            "id": "31bb1418-cf48-4dfd-99ce-e70374339a6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 121
        },
        {
            "id": "3ae387f4-d9bc-4ed4-9e19-fa8071a6e096",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 48
        },
        {
            "id": "97e14dd0-c7ac-4c6f-8aed-443b4a96eb44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 49
        },
        {
            "id": "a38c7172-7bf0-4260-b982-c5d492cfe38f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 50
        },
        {
            "id": "9cbc14cd-94ab-437e-9c9a-63a79e04b82c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 51
        },
        {
            "id": "4a25be62-0a0a-4501-81d6-2f4fc8a10880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 52
        },
        {
            "id": "22bf194d-87ab-40ac-9c97-3a17b5ab0b34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 53
        },
        {
            "id": "d6f5227b-ae57-433a-8eba-b91cf30b18ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 54
        },
        {
            "id": "25b6b3ec-d5d5-4229-89ea-609feb955b87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 56
        },
        {
            "id": "690b753e-3608-4bbe-a851-df04aa7fc9e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 57
        },
        {
            "id": "9bf87004-1d08-4ee1-8bea-b06fa23ae3f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 65
        },
        {
            "id": "7142b18d-7ca8-415e-87c7-807005936412",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 66
        },
        {
            "id": "adabdebe-3d0d-42f2-b572-8f4d791132e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 67
        },
        {
            "id": "06f59df6-a057-4704-b8cf-75407ef41370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 68
        },
        {
            "id": "4a3a8789-a933-4744-85e1-880282e5166b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 69
        },
        {
            "id": "fe46acc4-7852-4684-9ed9-5fdcc11467b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 70
        },
        {
            "id": "872f4cc4-7f48-47a7-bdf9-17c83a51e232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 71
        },
        {
            "id": "d430f355-82b1-4d33-acef-78be94b66698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 72
        },
        {
            "id": "2a43b64e-0311-41c4-bd80-94d33ae8d633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 73
        },
        {
            "id": "b73e21fb-5717-4206-a1df-3421245944ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 75
        },
        {
            "id": "e4665592-21c2-4537-8aab-63748d93f172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 76
        },
        {
            "id": "d32fba18-7e2a-439e-836e-581d7cf459ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 77
        },
        {
            "id": "471db332-6b74-4d47-abf7-ba0c6873868d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 78
        },
        {
            "id": "ed53bb11-2bbc-4723-b305-e2ef0f05298a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 79
        },
        {
            "id": "57565036-7ea7-4889-8719-793139bea384",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 80
        },
        {
            "id": "726ebd35-cb5a-44fd-91e5-82c478c2e6f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 81
        },
        {
            "id": "6f7424a6-fb77-4d84-bf83-9d9c9c9b35b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 82
        },
        {
            "id": "7ac00a48-bbcf-407a-8f20-78660207413e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 83
        },
        {
            "id": "4327660d-b584-4180-b57a-b2128176e9b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 84
        },
        {
            "id": "0f776107-f22e-4159-859f-04b590073b84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 85
        },
        {
            "id": "5bcdb83a-7738-41f1-9266-d49f126919f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 86
        },
        {
            "id": "ed06ca44-8111-478e-910e-0c69ed9377ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 87
        },
        {
            "id": "fd8abb9d-eb6c-4002-bbca-d26e8eb3923d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 88
        },
        {
            "id": "57643214-d1d6-4257-9987-09611459217b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 89
        },
        {
            "id": "637beaf1-35b3-45d9-a2ea-b0b5e5858c80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 90
        },
        {
            "id": "0c16ddad-37e2-4b62-b1cb-0e7517507cf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 97
        },
        {
            "id": "0c360043-0f8e-4e15-98a1-aa147b45a4ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 98
        },
        {
            "id": "5ec61073-2883-41bb-8490-f693894af3b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 99
        },
        {
            "id": "8997a68e-65b2-4f8d-a95f-15b8b5e833ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 100
        },
        {
            "id": "46a47591-44c9-471d-98f1-5de7181c097e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 101
        },
        {
            "id": "909e2e8f-52ab-479e-bba9-9693ff9fd16b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 102
        },
        {
            "id": "4c03d42b-3c2c-4cb5-a5b3-a273b4f806e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 103
        },
        {
            "id": "17a7e7e9-11ab-4de8-ac33-e10f106523ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 104
        },
        {
            "id": "365a5def-e54d-45d0-9022-710549acee03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 105
        },
        {
            "id": "93afaed1-edb3-4bf1-b9e6-808d7ae085a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 107
        },
        {
            "id": "ce4f8bf5-e176-41fd-8960-22a58cc2b04a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 108
        },
        {
            "id": "df9fb307-f1b2-4278-96c2-6a9cf3fc222c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 109
        },
        {
            "id": "a719778c-c8f4-43f9-b5aa-c45fb6d0a2ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 110
        },
        {
            "id": "87228858-1ced-4824-b729-9f82327e1943",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 111
        },
        {
            "id": "b2cda9c6-2d34-4015-9ecd-78d83cd681a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 112
        },
        {
            "id": "ff41be81-12d7-4b19-8a4e-d6c7920f6308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 113
        },
        {
            "id": "99ff7e8a-e53d-4d20-b8bf-47b84fa2e2b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 114
        },
        {
            "id": "71e922d1-f6bb-44d6-b2d6-61a18027a783",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 115
        },
        {
            "id": "326a264c-fd84-417b-8cde-fefdbdf54745",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 116
        },
        {
            "id": "7d9e1083-437d-424e-ab8e-40d053798994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 117
        },
        {
            "id": "8c1252fc-538e-4890-acd8-0604e5b9a131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 118
        },
        {
            "id": "d9bc3d6a-d96f-4c4e-b01b-f61a8cb3c4ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 119
        },
        {
            "id": "5988cc34-ae4b-44b7-a4ab-f300686f6890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 120
        },
        {
            "id": "2d996b88-fd9d-4216-903b-2fdbdc1406dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 121
        },
        {
            "id": "96e1ed0a-c4d7-45f3-a820-78bb7c8b0e76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 122
        },
        {
            "id": "f5cc9d97-8d78-42b6-ad0b-b0dcc31011e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 48
        },
        {
            "id": "3da011a5-a1fa-4c11-85fa-87252787455e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 49
        },
        {
            "id": "6084a11b-e8c6-4443-be51-590df8acec7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 50
        },
        {
            "id": "0aee9ac3-ed9a-4841-b7fa-4f7bb2f9a230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 51
        },
        {
            "id": "ec22b59f-508c-425b-b53e-d7ebbc971dc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 52
        },
        {
            "id": "ebf0ed90-7b43-41ab-959c-074f594ba606",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 53
        },
        {
            "id": "244eb536-9e31-4996-a570-7d6eb84cc6a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 54
        },
        {
            "id": "8cd90f22-0b30-4fcd-8dc6-23dba6525b82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 56
        },
        {
            "id": "943d7b7d-d8a8-4697-9f10-8a6e8bce3b34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 57
        },
        {
            "id": "4a55227f-2ab4-44ac-a36a-60382ea629ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 65
        },
        {
            "id": "bdd26a56-ecb8-4d86-9ceb-43fbf6ba5a9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 66
        },
        {
            "id": "7861c4f7-8def-45af-9c83-c34268ab1ebd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 67
        },
        {
            "id": "7f2f2037-f7c3-42d1-a678-58a1f966dff5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 68
        },
        {
            "id": "c7bdbb35-6a22-4590-bc4c-ec17d9c33a77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 69
        },
        {
            "id": "016f92a4-56a0-4d7a-a636-c5922f1fb6d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 70
        },
        {
            "id": "071615fc-bfc1-48d0-9869-429c6f311441",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 71
        },
        {
            "id": "7288eb53-e0e9-49b4-b96c-777fee08ffe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 72
        },
        {
            "id": "bf7cd1e4-f401-4f17-8f3e-fe489ecfc162",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 73
        },
        {
            "id": "c5c39a15-420e-442c-a5f9-4d8d44ee804f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 75
        },
        {
            "id": "14a33d40-aa27-4220-b1d2-622a692b1bc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 76
        },
        {
            "id": "79a19a8d-1ff9-4da8-af66-d9c40ef6b7a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 77
        },
        {
            "id": "b494924e-215a-4ca3-bbcb-ad6b1e93001b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 78
        },
        {
            "id": "0492944c-9f96-4e3c-b86b-1bf5b867b0b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 79
        },
        {
            "id": "77099e3b-0d19-4c8e-89d9-82a272067a43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 80
        },
        {
            "id": "96454908-1448-41ed-bf29-3751ce9e64de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 81
        },
        {
            "id": "92ed42a8-c8f3-47df-82bc-d5df10088417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 82
        },
        {
            "id": "5e404686-affb-446d-9404-c140c87c4cef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 83
        },
        {
            "id": "7a8703c0-daa7-4c3a-835f-1e7808399867",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 84
        },
        {
            "id": "cf4b683a-6aaf-4a35-a642-8d69497d7eaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 85
        },
        {
            "id": "72c399de-5702-4806-8628-c6900b1c157d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 86
        },
        {
            "id": "1fcadc23-5490-4ce4-99e6-34fc23d0e72e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 87
        },
        {
            "id": "4a4697a6-8a45-493a-99d6-1d4787e46d42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 88
        },
        {
            "id": "11f81a2e-e570-4af1-b266-faf49fd6f40a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 89
        },
        {
            "id": "d573ec8e-3164-4136-8802-495c1370a5b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 90
        },
        {
            "id": "79e61afe-bed8-47dd-9c56-48a4e19218cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 97
        },
        {
            "id": "30d9c7ba-ff1c-47ae-a2d3-696e529af03b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 98
        },
        {
            "id": "212094b7-270b-4f11-8386-a4427b028f3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 99
        },
        {
            "id": "7caa3a13-67b4-4dd7-8ca5-032115f1cf63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 100
        },
        {
            "id": "e6fbd7c5-a2d3-467b-8f1d-8b7a98edd88f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 101
        },
        {
            "id": "2a8ce909-6309-4789-aa55-4b863e5e2859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 102
        },
        {
            "id": "b007adae-42fd-45fb-8093-5e13ed28b39a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 103
        },
        {
            "id": "7e2a81e2-b3e3-4763-a171-9a664803cc5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 104
        },
        {
            "id": "2f2113fa-1095-4237-a5dc-40399ff4627f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 105
        },
        {
            "id": "3de5938d-3228-4827-af9d-f5f1df4be0b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 107
        },
        {
            "id": "3536d8c0-5556-43df-acef-582f42a512e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 108
        },
        {
            "id": "22d10bb8-49d8-4c80-932a-e8e9a9073c7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 109
        },
        {
            "id": "b1d098f1-828a-401b-8ee7-cae27da45734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 110
        },
        {
            "id": "74380bf9-9c9a-401d-8b40-589c7e0e7f18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 111
        },
        {
            "id": "7dbadb32-8242-4791-8b47-7284054e3943",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 112
        },
        {
            "id": "5ded4ff1-849f-4841-9916-052f0f724611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 113
        },
        {
            "id": "b8bc123f-99ab-47d0-8347-7bc87cfa7811",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 114
        },
        {
            "id": "3590dd91-3d5d-4ec7-92ee-733236dcb54b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 115
        },
        {
            "id": "18303c58-4cd3-4731-8049-e8e4df3c8ac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 116
        },
        {
            "id": "2f4fde47-38c5-4774-a340-acbbe3351b48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 117
        },
        {
            "id": "babeadbb-b2d2-47f6-9c6c-08947786ec84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 118
        },
        {
            "id": "fd450f17-b70d-4d3a-a0ca-83d5d9d3739a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 119
        },
        {
            "id": "901ee856-6577-45f1-adb8-3eb922781305",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 120
        },
        {
            "id": "989fb50d-1813-4651-99d2-8f87b78e71e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 121
        },
        {
            "id": "2add7d90-9cf3-4f07-b9d1-f7dd022eb6e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 122
        },
        {
            "id": "17687770-749e-4f07-b330-7a5eec8eaa2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 48
        },
        {
            "id": "deb5e245-c5e6-4a16-b1b6-7db358b29421",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 49
        },
        {
            "id": "149a4ab5-6385-4680-bf49-78d5adf39f61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 50
        },
        {
            "id": "c91f53b0-db82-47e4-b599-8d0a81e87950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 52
        },
        {
            "id": "5ef323b4-8035-484f-b05b-fd8ab92e5091",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 53
        },
        {
            "id": "468f747e-796e-4e90-ae9a-1a75592dcf5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 54
        },
        {
            "id": "242749c9-a4df-4b9a-88fa-03f677d2a70a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 56
        },
        {
            "id": "3458b4e4-c0b9-4163-be96-b193b747c3c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 57
        },
        {
            "id": "a11997d9-d0c1-4331-89da-43a967914de9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 66
        },
        {
            "id": "bff04ca6-c592-4b55-a0b5-991b5c9cc0ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 67
        },
        {
            "id": "47e901ab-48a3-4f9b-82ff-164e836c4fdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 68
        },
        {
            "id": "49a7e679-994b-41a6-96ac-c2b145e3d756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 69
        },
        {
            "id": "2936e49a-3911-48af-aac0-6741cd27e9c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 70
        },
        {
            "id": "64c8f1c0-3a6d-4062-90b3-5c16085e64a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 71
        },
        {
            "id": "68527784-fc29-4ed1-b32b-3236dc8532f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 72
        },
        {
            "id": "0b2f7b84-c3e0-46fa-8f9d-7fd3a338a99b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 73
        },
        {
            "id": "53d5146c-1d37-4128-88c4-9a46796a5fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 75
        },
        {
            "id": "8cdaa4c2-abc0-41ec-9311-34a05b7b1d58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 76
        },
        {
            "id": "0084f159-f117-4bbb-8b1b-e3a7a64fe783",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 77
        },
        {
            "id": "15df4e4b-9d6c-431f-a89f-e8c8e08f9bfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 78
        },
        {
            "id": "a7c715a9-c8d1-4373-aa53-5843de8091d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 79
        },
        {
            "id": "110e5b3c-310f-4e1d-b804-935fdbb44ccf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 80
        },
        {
            "id": "40d66af3-7540-471a-8610-71eb84c5a1ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 81
        },
        {
            "id": "f95ad530-a3d1-45b0-9317-6d89a1da3724",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 82
        },
        {
            "id": "036f3ebe-8512-4ca8-a055-46918b501754",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 83
        },
        {
            "id": "2f569bf9-a7b8-40d6-bc0b-d01afbd8ffe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 84
        },
        {
            "id": "62cbad0e-b600-4d0a-8161-4723a175e006",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 85
        },
        {
            "id": "49edd5e0-0cab-4e60-8a86-21c21f0e030c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 86
        },
        {
            "id": "1fa2776d-5702-4cd6-8d1d-c58d5c19b0c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 87
        },
        {
            "id": "3011e125-fe8f-4a2e-a811-ba22b088deb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 88
        },
        {
            "id": "f6f18487-83b0-4d67-a1e5-461fb584f990",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 89
        },
        {
            "id": "a0290177-bcba-4c2e-b2bc-1c539ca389e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 90
        },
        {
            "id": "2944499f-ddf4-4790-b084-bfa37339cca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 98
        },
        {
            "id": "b3a5f4c2-dc93-4590-b716-949d0f2984e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 99
        },
        {
            "id": "691a7dd7-bf25-41be-a1ca-b57c6ce6a036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 100
        },
        {
            "id": "ae96de22-dc50-4ee1-9af8-ab2fef543d76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 101
        },
        {
            "id": "9a15cabb-d3e8-4d1c-8c6f-35c4ba7305e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 102
        },
        {
            "id": "6e8f08fe-1757-4174-9b98-c8bea72f527e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 103
        },
        {
            "id": "9a34ae8b-163b-4ca4-a84a-988e3bbfd0ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 104
        },
        {
            "id": "af2a0bae-5838-41df-8afa-11bef737ead2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 105
        },
        {
            "id": "24f0f0a0-fa32-4433-8d2d-15c2a4a2bb3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 107
        },
        {
            "id": "78457406-989e-4cb8-a53c-62a5c18e874f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 108
        },
        {
            "id": "74bb29ef-f881-4df9-bc7f-7eeeccf8d455",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 109
        },
        {
            "id": "1399c484-e2ea-4ef0-8c68-dc2acfa1e299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 110
        },
        {
            "id": "1a5b7b4b-e05c-4a9f-8e35-6f478d00751e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 111
        },
        {
            "id": "dc7a5ba1-13c1-4ca2-be47-96e3b8a49d7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 112
        },
        {
            "id": "5773f6ed-7e00-47f9-ac99-db84e48daf9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 113
        },
        {
            "id": "e9f337db-08b6-46d6-8184-4ebc4386f65a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 114
        },
        {
            "id": "52f1fe5b-c1e9-498e-aa70-afbe6dd40d8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 115
        },
        {
            "id": "d92c851a-f39f-4a0b-ac2d-b47ee1fedabe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 116
        },
        {
            "id": "04e0064d-469d-4615-9f1f-07aa9a99145d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 74,
            "second": 117
        },
        {
            "id": "c65bcf31-f697-4965-b0b2-01b880f626e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 118
        },
        {
            "id": "169aa0bd-64cc-4aed-a350-df004f96651c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 119
        },
        {
            "id": "f7b54cee-7191-4aa8-aa2a-670ac5081498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 120
        },
        {
            "id": "54b8cc91-eec9-4e7f-af36-e67db5d9cddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 121
        },
        {
            "id": "dbf4c48a-5047-4871-b0bc-bff19c8a68ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 74,
            "second": 122
        },
        {
            "id": "2e471838-5444-4686-bc9d-9fccfc6968ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 48
        },
        {
            "id": "e686c1fa-f559-4f99-aee6-e67bf644f0ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 49
        },
        {
            "id": "e8fb5e9c-d750-4b3a-a5e5-339d4c71ec67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 51
        },
        {
            "id": "12b79d6a-ba92-42c7-8bbe-eb677e7a249c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 52
        },
        {
            "id": "21a31fb5-a1df-4236-b47e-be18034a1fae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 53
        },
        {
            "id": "97f212e7-7c7b-47db-bafd-a5ec1ba36f9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 54
        },
        {
            "id": "b07c902e-bb8c-4d86-8202-0dacbd836a4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 55
        },
        {
            "id": "c8267ac3-408d-4241-966c-c396630a972e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 56
        },
        {
            "id": "99e416e9-1abb-4585-836f-84c068fd5318",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 57
        },
        {
            "id": "f10e4241-99a2-4ff6-a135-f65e45962e82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 66
        },
        {
            "id": "c310658c-1d70-49b5-9594-17a7299d8f04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 67
        },
        {
            "id": "982ef695-8031-402a-81d6-4504f4e51ec0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 68
        },
        {
            "id": "e34f29fc-8062-4714-aec6-0ed61261c60f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 69
        },
        {
            "id": "54876b7b-c475-4a92-bb20-113d2ee27f92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 70
        },
        {
            "id": "04c83a8d-0d94-432e-99f5-96242dec15e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 71
        },
        {
            "id": "fcda33f8-0815-4beb-9397-57f59ebdc7ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 72
        },
        {
            "id": "ac478ca5-0068-42ca-b6de-23c00a753a74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 73
        },
        {
            "id": "c1f67f66-cd96-4cb4-bb4b-b820a6159270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 75
        },
        {
            "id": "e2ecbc01-50ce-49ae-98eb-fc4521915836",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 76
        },
        {
            "id": "7de72583-6c0b-4d93-878c-68c4c07a0732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 77
        },
        {
            "id": "af5cf677-ef97-43b8-b482-dd993aad4401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 78
        },
        {
            "id": "a1c50a32-0f4c-4480-b233-026885732242",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 79
        },
        {
            "id": "885660ec-b2ff-469c-8f7e-2139f1b41975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 80
        },
        {
            "id": "ed14c42b-b98f-476c-9cc3-0bc31672ecd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 81
        },
        {
            "id": "06e715f4-ce29-4810-b6af-80c1e22a85b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 82
        },
        {
            "id": "ea9ec307-8fb7-462b-b276-a95be47aa9cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 83
        },
        {
            "id": "8fcb48f9-c138-4845-bb37-0c0e67934a0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 84
        },
        {
            "id": "226b8e8b-a642-45e3-878b-ea959e10c513",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 86
        },
        {
            "id": "d165ecbe-54e2-4e6e-8cf2-eb022a5527ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 87
        },
        {
            "id": "e1bcb7fe-80aa-454b-9692-4f22e8254442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 89
        },
        {
            "id": "d9e30b6c-6966-46db-bc1d-74e87a2ce7c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 98
        },
        {
            "id": "aea4cf05-d10d-49ed-bc93-3c1d4f959cbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 99
        },
        {
            "id": "fff3b49c-294a-4907-999e-dbeec80e6619",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 100
        },
        {
            "id": "45936581-9d22-4f1f-8edb-a9ad3b59c916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 101
        },
        {
            "id": "5bd9c0f7-2c98-467d-9b4b-9d6fac1128a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 102
        },
        {
            "id": "e8b5be35-2c0c-49fa-8cb7-eacb49dd5acc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 103
        },
        {
            "id": "bc2ac52f-346c-4e09-806d-ad35a22e9e50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 104
        },
        {
            "id": "2ba36c8e-7297-4689-976d-5bd81f32ac01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 105
        },
        {
            "id": "d3e46e8a-3c5a-46f1-987d-0b33bc74f479",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 107
        },
        {
            "id": "88b48e10-bf0f-4009-afba-b5c3b70bc54b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 108
        },
        {
            "id": "da560cf2-d5b7-49ad-ad7f-76fd2420c8cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 109
        },
        {
            "id": "11d921d7-359d-42dd-bd89-7f9ab3d03478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 110
        },
        {
            "id": "b272225e-81f3-4c21-942c-a050a0d4f95c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 111
        },
        {
            "id": "847be2f8-0a3f-4e9a-8501-889af24f9a62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 112
        },
        {
            "id": "6f732045-aa84-41a9-8d46-f11abb7ca724",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 113
        },
        {
            "id": "188722c1-6faa-4667-a10c-ca0f31bb4ea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 114
        },
        {
            "id": "639de3bd-765b-4885-be7f-47d14a9e1459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 115
        },
        {
            "id": "56954b0f-f6cb-4443-aa72-c12f38d9ad64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 116
        },
        {
            "id": "bbea3943-a2b4-4553-94cc-046df69ef856",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "66f9e464-f6b9-4f74-940e-52c1c669aea0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "914a0021-c613-40b8-9279-28904e934d1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "393fb655-eeed-40ea-a7a6-d57cfb5ebff8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 49
        },
        {
            "id": "47642397-06bb-4094-bc43-b55053b6bef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 76,
            "second": 52
        },
        {
            "id": "12af4a18-f2ec-41be-b91f-585c08dfca21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 55
        },
        {
            "id": "22c835c8-53d6-4633-bab6-8b40868222b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 83
        },
        {
            "id": "bcb5ec30-c2ce-4c10-b5bd-27855d1c73ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 76,
            "second": 84
        },
        {
            "id": "d7615ba4-e4c4-46b2-91df-2080c5547118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 76,
            "second": 86
        },
        {
            "id": "beb63437-dfc1-4570-8482-3cb048807289",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 87
        },
        {
            "id": "42052059-d396-4431-9e6b-b6ccc2f46990",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 89
        },
        {
            "id": "f28ab695-5488-4fc9-ac78-263805f6560d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 115
        },
        {
            "id": "10ad4f67-c014-4458-93bf-f61076fd1096",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 76,
            "second": 116
        },
        {
            "id": "d95ff65c-91ed-459c-b0e6-5bf81a8b1e7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 76,
            "second": 118
        },
        {
            "id": "1e9ff701-f63b-4c98-a3e0-69cf5f9d9a50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 119
        },
        {
            "id": "1d0430ea-1b43-4f14-9b34-cd81fe078494",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 76,
            "second": 121
        },
        {
            "id": "9bb7d509-6d4a-47ed-aa13-7fe4eac3b6e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 48
        },
        {
            "id": "e753171c-09b8-49a3-b4d4-40f164d83c92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 49
        },
        {
            "id": "29b8a591-c117-42a3-8566-311bdf018a21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 50
        },
        {
            "id": "bc9231dd-0b46-468d-81b0-9ae4e4f9f818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 51
        },
        {
            "id": "0e9ccbaa-08a8-4d15-9f82-6d7268ba3b6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 52
        },
        {
            "id": "21009815-80f1-4edd-b39d-b99fb879d40f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 53
        },
        {
            "id": "06d3fa3c-80c5-4598-93bd-dc7cb4f03a83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 54
        },
        {
            "id": "c9d9584f-2234-4053-a26d-74e1811d4485",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 56
        },
        {
            "id": "c90cc519-bd06-495b-917b-aa7ecd1d325d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 57
        },
        {
            "id": "af330c9e-a093-4ac5-9539-d5991e44438b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 65
        },
        {
            "id": "ce3fd731-56ee-4e0d-b939-73ba80f63dfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 66
        },
        {
            "id": "1a317007-bec0-43f0-98e7-7f37509d72aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 67
        },
        {
            "id": "8dc5a632-d0c7-4c80-b4f3-6dc0dad3bb4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 68
        },
        {
            "id": "dd6ce5ec-20fd-4528-bdac-40b41aa74825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 69
        },
        {
            "id": "7e147c1d-88ee-43a6-a457-f723ce7f349c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 70
        },
        {
            "id": "14399a7d-4037-416e-8fc8-d41b5a3d3f03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 71
        },
        {
            "id": "0cb3218b-63c9-443a-93b5-2d37ed09bbda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 72
        },
        {
            "id": "7b74830a-b942-4537-a84f-c4c45fd960c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 73
        },
        {
            "id": "3e92dcf1-0768-4ac3-9f20-ab2895451bb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 75
        },
        {
            "id": "450aa684-5445-4761-8a5e-5edd53a39be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 76
        },
        {
            "id": "c98b6f2b-8ca2-4cda-ad42-f92cde6700a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 77
        },
        {
            "id": "b46d5d61-688f-4af2-b0c2-5b77eb037915",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 78
        },
        {
            "id": "76cb73bf-f096-43af-9466-dc1fbf0c0632",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 79
        },
        {
            "id": "534d7ace-235c-4a6d-8867-f9019c6f887d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 80
        },
        {
            "id": "bba9abff-e3f1-47ab-9d00-bdc76de5e309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 81
        },
        {
            "id": "f5b7299d-015e-4732-aaa0-f972b0ae224f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 82
        },
        {
            "id": "2c36329c-e31f-4982-a2d0-f6d6fd6a66ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 83
        },
        {
            "id": "78fb407c-132f-47c2-aaac-cd61731fe37c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 84
        },
        {
            "id": "d4514638-6950-404a-b4bc-e3766c26e31f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 85
        },
        {
            "id": "f08c3650-ea09-47a7-8065-ed34185854a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 86
        },
        {
            "id": "39a18f75-3740-469c-a793-2e3a210a281b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 87
        },
        {
            "id": "27c7e905-4322-4b87-bf98-d7787da191c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 88
        },
        {
            "id": "ac98334d-f31d-4129-a7f0-c08b8bed1502",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 89
        },
        {
            "id": "69fd0a18-61e6-4273-a362-9bb4b4cbd52c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 90
        },
        {
            "id": "b349a7fb-6c45-4a6e-9734-b63763f9664b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 97
        },
        {
            "id": "27145c59-324a-42e3-92da-00010f31bb8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 98
        },
        {
            "id": "70880cfe-0586-4af8-825f-4632f871e5f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 99
        },
        {
            "id": "b06f94b6-06a2-4947-a5c4-2891b2d900cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 100
        },
        {
            "id": "83a0776f-138f-46e7-a715-5307e4ed4513",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 101
        },
        {
            "id": "a8aa89bf-ec10-4895-bf7d-1c5c00a2e9ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 102
        },
        {
            "id": "5f7dd988-71bc-4285-be31-c08145cebf00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 103
        },
        {
            "id": "78f99403-c99b-4a2a-acd3-52520c24a1dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 104
        },
        {
            "id": "db3e921b-11cf-46c2-ac92-c26ea9665936",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 105
        },
        {
            "id": "85a68e98-3194-4e83-be0c-c5834773b904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 107
        },
        {
            "id": "1f66a317-cd0c-4376-b382-fce5edcb7959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 108
        },
        {
            "id": "4c0e9fdd-be23-4533-b0a8-93b1da748ef5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 109
        },
        {
            "id": "4ce42084-6653-453a-bb27-04bcfefe11f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 110
        },
        {
            "id": "f902d8dd-dc01-4a70-87a4-9251786672bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 111
        },
        {
            "id": "1f365d66-adcc-4e75-ac57-af786f2de0ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 112
        },
        {
            "id": "8450fd8a-47e8-4637-acf9-1222b9840574",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 113
        },
        {
            "id": "05009f6d-6fcf-4854-bf45-5195dbcbcf95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 114
        },
        {
            "id": "7a32ee0b-a7ed-4eec-ab76-1df0afe496e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 115
        },
        {
            "id": "297f415d-f0bc-4b95-9687-f4e318c89adc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 116
        },
        {
            "id": "4c487113-7e8a-43c3-8da6-15d2832f7fe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 77,
            "second": 117
        },
        {
            "id": "6f40a363-5e83-4b9b-9e2d-b1d3f87ac5e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 118
        },
        {
            "id": "3b8983a0-856d-4697-9f44-d62beac74b19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 119
        },
        {
            "id": "2985393d-4b47-4de7-be81-c5000b83a0ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 120
        },
        {
            "id": "6c4e9163-b13a-4b84-afbf-9632ce5dfbdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 121
        },
        {
            "id": "5a26e763-5300-4a82-9fbe-515ce28a1ca0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 122
        },
        {
            "id": "e0adf771-ae0a-4a87-a63a-25bf0bb3f002",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 48
        },
        {
            "id": "921a0e44-df93-46ec-b5f8-c937b889b6dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 49
        },
        {
            "id": "f26b62a9-cb6b-4985-b485-5a79ac5867bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 50
        },
        {
            "id": "d1b35c10-5ccc-4bbc-a7d1-79c525e4a532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 51
        },
        {
            "id": "a4a74497-cf77-4531-a3e9-29e9a6236ec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 52
        },
        {
            "id": "83757bfd-046f-40e7-b0f6-68d8ac5fc543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 53
        },
        {
            "id": "8ff5ed18-16b7-4912-8691-d32569e35e0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 54
        },
        {
            "id": "70977aec-a81d-4198-855c-83e6e3de718a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 56
        },
        {
            "id": "4faaf55f-188c-40a9-82b7-737ec8ded69b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 57
        },
        {
            "id": "7d6ee83e-72ac-4dfa-9f2f-caab1a68f3d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 65
        },
        {
            "id": "9ce15ced-0707-4d2f-8491-74b3f36cf075",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 66
        },
        {
            "id": "f75c12ff-a597-46aa-8735-0fcb2fc4b9c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 67
        },
        {
            "id": "d0fdf8fc-96e9-4de2-81b8-1efef664be3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 68
        },
        {
            "id": "f9388b75-ac98-44d8-b6d7-73fa85f4d28f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 69
        },
        {
            "id": "49d89ca9-c892-4e45-a172-1307123d535a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 70
        },
        {
            "id": "28364bbe-76c7-4a49-aa64-f9ed2e399813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 71
        },
        {
            "id": "e4ef0c62-75a8-4b61-85af-9ea578cc8467",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 72
        },
        {
            "id": "72cb6e5c-1580-4cb8-a39a-e00b4b70ca58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 73
        },
        {
            "id": "6d5658c6-2bb1-4914-a191-f450525996b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 75
        },
        {
            "id": "861ada81-7da2-4a2a-a113-1518a9c5379d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 76
        },
        {
            "id": "3df1de61-0812-4352-8232-f41311e8116c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 77
        },
        {
            "id": "fe0401df-c969-49f6-b03f-884ae2252992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 78
        },
        {
            "id": "5b74b1a2-d005-49fc-80c0-4a363fc88966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 79
        },
        {
            "id": "addb4c35-4418-4a2c-97a0-aeca10784c58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 80
        },
        {
            "id": "abf876c6-b62a-430f-aba4-a067ddcc9822",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 81
        },
        {
            "id": "b00dc1e6-a7ee-4062-8c67-8fa8f343704a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 82
        },
        {
            "id": "9ef4932a-8291-4e03-918d-3f66aaddbac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 83
        },
        {
            "id": "eca7dddf-7217-4ccc-b244-878a5569a4a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 84
        },
        {
            "id": "bec9b6c1-b7d4-40bb-acf4-a937f48b5a79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 85
        },
        {
            "id": "ff0c7297-6b9f-4802-b37f-be68c7d5627e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 86
        },
        {
            "id": "31c79c35-5ddb-42aa-af0f-be293b83e3b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 87
        },
        {
            "id": "68c6c1b7-1ea7-45c9-b019-f75d11663117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 88
        },
        {
            "id": "8c05b3bd-c646-4f94-8f8a-93af578ce2ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 89
        },
        {
            "id": "3b1b0602-01d8-435e-8235-bfdfb4634898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 90
        },
        {
            "id": "315cd9c3-bdcf-43a7-90dc-9cfc023bffc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 97
        },
        {
            "id": "4aa54544-1bdb-4d28-8f2e-f6f12699a8a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 98
        },
        {
            "id": "0bbe6349-2cb3-4c1f-a2ea-5aee294ecb35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 99
        },
        {
            "id": "652d8cef-7956-4d69-bb59-2ffdcc881bbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 100
        },
        {
            "id": "f61b3501-135a-460d-95ab-c7d34e467f12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 101
        },
        {
            "id": "c78fe5fe-4f4d-4d6c-940c-73836555fbe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 102
        },
        {
            "id": "1a1bf9f9-1621-43d1-9a0f-73cc43acc489",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 103
        },
        {
            "id": "e451a220-86c5-4015-a9b7-4f990d7d7f0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 104
        },
        {
            "id": "ecaf7a6c-18d3-4776-b0fb-68bad5285c5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 105
        },
        {
            "id": "809450ca-9be7-443e-b04c-526b07b25c14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 107
        },
        {
            "id": "ec5b9560-a4c5-4a5e-b19d-41958019e525",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 108
        },
        {
            "id": "fce6189f-6938-4b0f-b230-275d7cf429e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 109
        },
        {
            "id": "d321850a-a445-414b-bde1-54e976abbdba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 110
        },
        {
            "id": "d62b74ea-14ae-4b0a-b28c-f649eaf03cd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 111
        },
        {
            "id": "570fbac1-5fa1-421f-9060-2bab0e47d817",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 112
        },
        {
            "id": "75b47036-3f8b-4a31-a46b-d83cf9a08144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 113
        },
        {
            "id": "0fc3006e-5e06-4b87-b0e9-afe8b42a29df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 114
        },
        {
            "id": "72f85224-f774-4dc4-9bf4-f1bd79437614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 115
        },
        {
            "id": "e79e0fcd-0eb9-4ce2-ace4-9ab14d3b04bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 116
        },
        {
            "id": "7a6321e6-cb96-4f08-8f02-50ba27903243",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 78,
            "second": 117
        },
        {
            "id": "fe4adf2c-8b34-4715-b931-260bb4d634f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 118
        },
        {
            "id": "c75acda5-e13a-47a5-886d-91baa13ee272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 119
        },
        {
            "id": "5e654732-5b6d-446f-8d66-2bfbf89a27f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 120
        },
        {
            "id": "f802e48d-4ce6-47d5-bcda-7a75dce42af4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 121
        },
        {
            "id": "e5c711c3-d789-42b0-aba3-17a3d1b0d20e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 78,
            "second": 122
        },
        {
            "id": "64092d58-525f-4e85-ba9f-631bfad15805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 48
        },
        {
            "id": "76351061-a462-449e-8645-0dce4a25e656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 49
        },
        {
            "id": "653f171a-cb06-485e-80bb-df5388794df0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 50
        },
        {
            "id": "3522ea53-1cb4-4306-a367-579f77305d2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 53
        },
        {
            "id": "e94f4821-4df4-447e-b8b2-0235f9328aa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 54
        },
        {
            "id": "17ed8a1b-9589-4935-b592-01bb831a318e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 56
        },
        {
            "id": "24822fd7-cddf-41b4-8de7-6d773f19e1fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 57
        },
        {
            "id": "7e3d21cb-b04c-4058-823f-156c66b06c91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 66
        },
        {
            "id": "48554eb3-5e99-43e7-a4be-efafe1ebdc4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 67
        },
        {
            "id": "739f5d85-b3f5-41c6-ac03-39052a2cc3ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 68
        },
        {
            "id": "870bd637-44b6-4010-8266-d5aa9c2ea099",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 69
        },
        {
            "id": "4b336415-7884-4ceb-b09b-4353ef023c75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 70
        },
        {
            "id": "50ccfd7e-03e2-4995-ac05-793f908a4a3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 71
        },
        {
            "id": "181cab47-5c71-46dc-a481-ba9a00a6c720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 72
        },
        {
            "id": "b1684294-8925-4ac7-93b6-8670793bfd89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 73
        },
        {
            "id": "61b1d336-aa4a-4912-ad03-3ab064892570",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 75
        },
        {
            "id": "eb478ed4-d0ab-4883-ba8d-1cda48db2ef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 76
        },
        {
            "id": "5cf164fb-f105-4da5-89c9-60ed3bf27dfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 77
        },
        {
            "id": "f6ba3ed7-89f9-473b-b8ce-ccd398a4c7d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 78
        },
        {
            "id": "2186a33d-93fa-4476-ba5a-99b9e2b4149b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 79
        },
        {
            "id": "65fda621-c1c3-450c-abd5-125e0a2df1af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 80
        },
        {
            "id": "b19ea79a-6912-4cd8-88c3-f131bc4630e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 81
        },
        {
            "id": "e18da49d-2205-4282-aa37-d8151e928bbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 82
        },
        {
            "id": "1c4764ef-db98-4528-9b42-8dd693b1db15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 83
        },
        {
            "id": "12ded1a8-98f8-4d34-b52a-c7e08c9f7ca9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 85
        },
        {
            "id": "51c48f54-a197-41c0-bc17-73f206fb1d2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 88
        },
        {
            "id": "f112b002-5298-4eff-b80c-8a11349d675c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 89
        },
        {
            "id": "c3ee0ffc-d9db-4fc3-9468-690108339713",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 98
        },
        {
            "id": "545462c9-043b-4ce7-a932-90f79b8d3fb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 99
        },
        {
            "id": "bb23d55a-88ac-465f-a804-d4cc706900d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 100
        },
        {
            "id": "bccfd9f2-27c0-4c42-acfd-aca17a5fa3c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 101
        },
        {
            "id": "018acb5e-ece1-47c6-8b01-daf42a8175c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 102
        },
        {
            "id": "fb07217e-9cac-447d-a409-a051c6a914d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 103
        },
        {
            "id": "0a813042-620c-4b05-82a5-9672ddb9ed0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 104
        },
        {
            "id": "64b7eb46-b3d7-47b5-8513-dfb2f6d377e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 105
        },
        {
            "id": "ccad7360-125d-4e92-93c6-8915b2f8edc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 107
        },
        {
            "id": "b754ef92-1409-4b78-a7ff-312bb81b65c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 108
        },
        {
            "id": "91f7c911-816e-42ff-92f4-fb131bba6508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 109
        },
        {
            "id": "9ff9ff1e-05c2-49f8-b162-37f12a479566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 110
        },
        {
            "id": "c545f91a-ed37-45f0-83f8-9e76430dde1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 111
        },
        {
            "id": "e55da402-8f06-4cc0-8d03-8a52843756eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 112
        },
        {
            "id": "58c7f8c2-c911-4252-ad16-ef746088deb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 113
        },
        {
            "id": "e654971e-3c58-4071-a604-631fc0ba0235",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 114
        },
        {
            "id": "74ac4155-e327-4d6a-bf6e-7adbdc3b4812",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 115
        },
        {
            "id": "f5bfdefd-1247-403e-bf58-c6f17e594bac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 117
        },
        {
            "id": "6e136550-a324-42a5-80e9-c380c565d595",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 120
        },
        {
            "id": "c9ecf472-8a85-4ed4-82b1-355a01077746",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 121
        },
        {
            "id": "9b628fc9-a61d-4a30-a4b3-a1c722bf41bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 51
        },
        {
            "id": "f97930da-1503-479d-875d-b2d6e6fc7ee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 52
        },
        {
            "id": "55ec933a-37d9-4b2d-becc-d3dd4908f9f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 65
        },
        {
            "id": "68dfcccb-3dc1-4f14-83d1-ebfc7423047d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 66
        },
        {
            "id": "ce5e120e-e7fa-4b5c-bcfa-9cac217c6fd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 68
        },
        {
            "id": "1addf530-b8ec-4303-9300-48dfd48bc8be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 69
        },
        {
            "id": "b430e72f-0544-472d-bcb3-25c9b55ccb36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 70
        },
        {
            "id": "87fbe969-d203-4238-b0f2-19ac14db275c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 72
        },
        {
            "id": "5ccd33b9-71dc-4237-9749-33ceac72ac6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 73
        },
        {
            "id": "290e2a38-954e-4881-a7c9-fbd877c65549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 80,
            "second": 74
        },
        {
            "id": "13e3a188-9c02-40cb-942b-6975eaf57c08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 75
        },
        {
            "id": "9ad888f5-7f7e-4dee-ab22-da14bdddce51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 76
        },
        {
            "id": "8391ba9e-fdf8-41a2-9fd5-a2ff0e50fd74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 77
        },
        {
            "id": "2001dc12-c7b4-48e6-9105-752d41edd01a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 78
        },
        {
            "id": "e4bcb9ff-d357-4f42-8c24-548dd30c61e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 80
        },
        {
            "id": "21132bc0-7586-4da9-b228-ac8dc303a13f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 82
        },
        {
            "id": "1763d8c2-72b5-4c1b-b9ea-23f04e881354",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 85
        },
        {
            "id": "21053379-12bf-417a-bb09-8563c7df20c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 86
        },
        {
            "id": "b15d2b30-e703-4a4b-ad5d-542b0a483a8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 87
        },
        {
            "id": "812bb64f-fd73-4087-ac2f-617e71094fc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 88
        },
        {
            "id": "4d2484e0-157b-4479-a237-c93978a3f01e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 89
        },
        {
            "id": "8796ef14-e581-4f14-99e9-0687035206f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 97
        },
        {
            "id": "b3692c43-9b9e-477f-b347-b182d56e2423",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 98
        },
        {
            "id": "074a40b9-ea2a-4fd4-9a4f-97beee8a70e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 100
        },
        {
            "id": "a326e4b0-b54c-4fa7-a710-af2c2a0a247b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 101
        },
        {
            "id": "253e181e-3eae-4950-bd9b-785bea8275c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 102
        },
        {
            "id": "bfd862f0-e942-44fe-a7d1-45df16311929",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 104
        },
        {
            "id": "8b0016c7-555d-4bfa-8624-de2bf60fec55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 105
        },
        {
            "id": "e45af9bb-c16e-4112-852a-7156453ee679",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 80,
            "second": 106
        },
        {
            "id": "48235635-d333-4d4c-bc5e-7c2fa10a858c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 107
        },
        {
            "id": "2ba39998-4e3f-4f65-a99c-22702f8854cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 108
        },
        {
            "id": "d11eeb59-528c-4f87-98d8-a1b4f9776deb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 109
        },
        {
            "id": "18e7a718-c0fd-45ed-88b2-6af1b6606de9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 110
        },
        {
            "id": "9c3a144c-41b4-48b3-9ff5-b3668889bc44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 112
        },
        {
            "id": "990b3ef5-e8bd-437f-aae6-df25e2409c7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 114
        },
        {
            "id": "f6ae6b5f-09ff-4433-b440-c5be667242a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 117
        },
        {
            "id": "7b90443c-19ea-4044-8305-fc1059e73765",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 118
        },
        {
            "id": "f944ad19-cd66-436c-bfbf-b17f69456e0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 119
        },
        {
            "id": "4e537fe5-71b4-43da-9ea7-32f12e2541a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 120
        },
        {
            "id": "9d474765-7a13-4f3d-9ecc-cfab6fe59198",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 121
        },
        {
            "id": "e0f08b4d-1d7d-439a-a8c6-bc615cd8b04d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 50
        },
        {
            "id": "40791ad9-d7d3-456f-8ce4-77e74942156a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 55
        },
        {
            "id": "cdd796f5-2745-46d5-b99f-11c788fa52f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 65
        },
        {
            "id": "66731a18-b9d8-4c88-a206-7af7763720aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 66
        },
        {
            "id": "db3fd335-bd1f-4c23-ab46-5fe1a16881cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 68
        },
        {
            "id": "d4de9c9e-0596-461e-928f-f00c0789385a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 69
        },
        {
            "id": "f2e7ebbf-c49c-41fc-99a6-524d17a025a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 70
        },
        {
            "id": "2b6ffcb1-5f33-4911-8b34-8843964225a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 72
        },
        {
            "id": "a8d2f350-3df5-4d15-9a2c-166e6799e983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 73
        },
        {
            "id": "2e9d6f99-d7a3-4c3d-b309-518203534abc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 75
        },
        {
            "id": "1597f27d-21dd-4204-8ebe-eeb1fab58086",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 76
        },
        {
            "id": "7c40cf4d-264f-4d1f-9e17-a486dcc32368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 77
        },
        {
            "id": "94af041c-0b8a-4ed8-9847-2d80aaccd457",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 78
        },
        {
            "id": "41f98add-6fb3-4985-94bc-32644e1e0719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 80
        },
        {
            "id": "56cfde61-2903-4105-823c-50dcdceeca2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 82
        },
        {
            "id": "dd4fbf7f-943f-431d-9440-92948948a922",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 84
        },
        {
            "id": "b2a00b9f-9c05-43c3-b9e3-4d342add60ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 86
        },
        {
            "id": "9196d64d-ae7a-4a3e-968b-e1303829ecb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 87
        },
        {
            "id": "0694ebda-f8bd-4429-871e-fb12946f872b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 88
        },
        {
            "id": "13b6418e-ae46-4e8e-9dff-b8dc84c2dc6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 81,
            "second": 89
        },
        {
            "id": "dbac81ea-64fb-4746-84aa-bd3cb9b20e0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 90
        },
        {
            "id": "4070d4d4-e6f9-4cdf-be79-c3cda1ac4285",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 97
        },
        {
            "id": "6ac689b2-dc28-4838-8c74-3bd3beda52a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 98
        },
        {
            "id": "01a45794-49ba-426e-8969-ddb22fbba625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 100
        },
        {
            "id": "e5dbb3eb-8cdf-4739-ab68-d7e4dd0c0fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 101
        },
        {
            "id": "f45fd9ed-6d95-48d4-8b99-7cc9939da697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 102
        },
        {
            "id": "f786eb1d-070b-46b4-bbf8-6c8059844e82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 104
        },
        {
            "id": "77e83d21-d224-4f71-b4be-e2eff0d3c144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 105
        },
        {
            "id": "a5a7eafb-0e0e-4ecc-a235-efdb6dbc761c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 107
        },
        {
            "id": "9b4285c0-50b2-45ab-ba6e-5565635240d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 108
        },
        {
            "id": "51710b19-b73f-4fb7-9b85-45245c7bba06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 109
        },
        {
            "id": "693820ba-661d-4aa1-8fac-6df037cfd5dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 110
        },
        {
            "id": "ad3253f1-a2ab-4dc2-b9ba-6508a408b3c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 112
        },
        {
            "id": "4565103d-55d0-4a9b-8070-f86cc3acfc6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 114
        },
        {
            "id": "ce03aafd-6843-4f2c-8f5a-fc52f04e5202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 116
        },
        {
            "id": "87a9e82c-42b6-4ca1-a7c6-b4a5fd15b358",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 118
        },
        {
            "id": "abaa0e9a-51ef-4e5f-bff6-38c42abfce3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 119
        },
        {
            "id": "c2c85e4e-ca7a-4942-a1b4-cb35e1470b48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 120
        },
        {
            "id": "18f9c5cb-9804-44a1-902a-ceb55856d201",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 81,
            "second": 121
        },
        {
            "id": "401e22d7-a12d-4713-bc61-2f01bc4ff054",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 122
        },
        {
            "id": "319ad86f-bb24-4d81-b016-4bc13a6bb62b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 51
        },
        {
            "id": "d5712030-3228-4510-b86a-143c324f32f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 52
        },
        {
            "id": "6479088e-0fc9-48aa-8a62-ad5315e5de51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 53
        },
        {
            "id": "3fb46f6d-b2d2-4f4e-99e3-02483ded3540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 55
        },
        {
            "id": "006f1c9f-3381-41e7-8594-d0d3c884e7fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 56
        },
        {
            "id": "a7708296-9102-4272-91d8-0d1a61ce10de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 65
        },
        {
            "id": "5e2fbe30-0af0-41c5-aba7-a1343420ebbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 66
        },
        {
            "id": "817fa43c-fb24-44d9-839c-8b44e360e1d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 68
        },
        {
            "id": "ddb56f79-9762-4a5a-8c1c-bf246f4348c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 69
        },
        {
            "id": "89981c4e-24b7-49f0-b68e-a4910cc698da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 70
        },
        {
            "id": "3ef1e9a7-1e53-4dcc-8f4f-59b15a2f6273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 72
        },
        {
            "id": "aa13c1fb-7c4b-44ba-adf0-9b2d04c100f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 73
        },
        {
            "id": "c0c6b515-58a1-4a2b-ad9d-e80c84000bb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 75
        },
        {
            "id": "887648f1-25b1-4cd5-9bab-14301bb1cc2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 76
        },
        {
            "id": "12900d37-d2c5-48bf-ae3e-9d1947d890e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 77
        },
        {
            "id": "8c022682-1528-4144-9a5a-e388ab67e2f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 78
        },
        {
            "id": "8a6c07f6-0b27-441e-b7ac-987ce7573312",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 80
        },
        {
            "id": "ef36d55a-6dda-45a3-a062-1f40d7307d16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 82
        },
        {
            "id": "c835f4a6-fabf-4c3a-adff-f73e3ed4b9a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 83
        },
        {
            "id": "52bd1891-020b-4559-9c47-7b0707ddc165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "a949c534-d51c-418a-81f3-2f419a340982",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 86
        },
        {
            "id": "3c0d7c29-b18b-4856-bc62-9b445602292d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 87
        },
        {
            "id": "f2236889-29a1-4eb3-bd3c-169bd2e1e4b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 88
        },
        {
            "id": "c5dd52e3-d648-4f55-a007-c45ec2560c7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 89
        },
        {
            "id": "c1a5be52-6a92-477e-9185-9c362ebed111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 90
        },
        {
            "id": "73db2ce7-17f9-46e7-9ea6-1b3eac44bed4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 97
        },
        {
            "id": "0701d44e-d340-4361-a8ff-19a0eef09fae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 98
        },
        {
            "id": "aab4b453-6d87-4729-b841-173db54ae3e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 100
        },
        {
            "id": "adf39c6b-500a-4581-8e58-c566525b3adc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 101
        },
        {
            "id": "91bf3afb-3572-4584-8b7b-97d36aadf30d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 102
        },
        {
            "id": "77cad81b-ac3f-4929-a550-f406f1d6deec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 104
        },
        {
            "id": "e31084c4-9f0a-4ddd-bf9b-5ff40294212b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 105
        },
        {
            "id": "af090b58-c945-4b7b-8129-2b2a9a684daa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 107
        },
        {
            "id": "7a396c7d-657a-4939-bab7-2038f487ee75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 108
        },
        {
            "id": "198a579e-0798-4ef4-83fe-abf94a584f63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 109
        },
        {
            "id": "de35be6e-c9da-4f86-a7d9-fff68fc76d03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 110
        },
        {
            "id": "8625657d-9194-4a76-8c0d-7d05fc0d8a8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 112
        },
        {
            "id": "f150b08b-bd48-4dfc-9165-04105fe2a9b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 114
        },
        {
            "id": "f60e22d6-4180-4921-86d5-c7766bf71c58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 115
        },
        {
            "id": "b8018a40-8df1-4383-92d4-86bb4bfd3a7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 116
        },
        {
            "id": "478827b4-3fcb-433f-96d5-77587ea8c5bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 118
        },
        {
            "id": "0c57a85d-135c-42a1-9895-4e528cc12143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 119
        },
        {
            "id": "8bfd1096-8910-489a-950c-a1da9fb3882f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 120
        },
        {
            "id": "ef9e7fb8-981f-44c3-96fd-406814d0331b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 121
        },
        {
            "id": "f1439bca-b8ed-441a-8ece-7035cd56f893",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 122
        },
        {
            "id": "6e5ed616-7f92-49ea-8048-2a9cbb08ea6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 48
        },
        {
            "id": "61fac4fd-874c-404d-96c1-5cb21b35476b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 54
        },
        {
            "id": "c538325e-1af2-46ea-a746-59f2aa1194b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 56
        },
        {
            "id": "2564ff2c-f353-4a2a-ba85-97c81f828a01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 65
        },
        {
            "id": "9d80306e-ce8b-4b46-a729-c7764b8323a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 66
        },
        {
            "id": "e59ffd3c-60e9-45ed-907c-9cd72bfd936c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 67
        },
        {
            "id": "064e78bf-dd5a-4e48-b9df-163fd12d1cf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 68
        },
        {
            "id": "c087e470-f929-4b0d-a262-a9b77967a834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 69
        },
        {
            "id": "9145a211-155a-4145-bf0a-1de5c6b61253",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 70
        },
        {
            "id": "da3f01ae-26ef-4dc2-b483-fbef32d57cda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 71
        },
        {
            "id": "5443730b-8d7d-4a2b-94c2-f1945e395611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 72
        },
        {
            "id": "79b592f1-2a51-4d47-8e54-b9bd9cf75d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 73
        },
        {
            "id": "15431929-c876-4a0b-9808-1f88be10d2bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 74
        },
        {
            "id": "189badeb-af38-4148-bebf-8b620d8c00e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 75
        },
        {
            "id": "65ded338-0889-402a-a47f-8c30b50871bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 76
        },
        {
            "id": "111ca0af-227f-4a18-afa5-ff1b05a4df6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 77
        },
        {
            "id": "a5c16ea2-727c-4324-a0c1-e956464e4167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 78
        },
        {
            "id": "cccba8dc-4ee2-42cb-a0b0-2350f7e2b00d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 79
        },
        {
            "id": "e7949af9-725b-43aa-a828-e303c94c02c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 80
        },
        {
            "id": "d9da2b46-6884-49b6-8118-4eb2b1358006",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 81
        },
        {
            "id": "d49b1853-7ce7-49cf-90b6-7ac95f4d2cbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 82
        },
        {
            "id": "cf89c719-ce87-4a86-bf63-37ad18a5449d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 85
        },
        {
            "id": "0a360cb0-67c3-4be2-b0e3-514291c41742",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 86
        },
        {
            "id": "20be0970-5919-4c89-8bee-00b92c8d6d97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 87
        },
        {
            "id": "2513b0a4-927e-48c4-842e-f887a01bad3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 88
        },
        {
            "id": "061ab76d-3c40-42d5-8ba4-c7ef54800dda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 89
        },
        {
            "id": "0e9df471-094f-41df-9c14-d2da87d31905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 97
        },
        {
            "id": "1de7b169-9fbd-4b27-8a6b-210a2df7b9f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 98
        },
        {
            "id": "ef369d33-1d6e-4e92-a7e0-6c763568f9ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 99
        },
        {
            "id": "b9cef0b9-8472-45b5-971f-610a4ad15271",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 100
        },
        {
            "id": "01e4ff3f-2404-4235-b600-96bf2a6499f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 101
        },
        {
            "id": "64bf6a18-b5d5-4542-b255-2d4f415e1de9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 102
        },
        {
            "id": "9c021edb-f0ef-402b-91bc-8b69fa43a20a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 103
        },
        {
            "id": "cc05b550-8a96-46ab-95f9-7f4613b8e156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 104
        },
        {
            "id": "5286f489-bccb-4077-a34b-d3208f527ce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 105
        },
        {
            "id": "3531cd5a-9317-4cbe-b04b-625a05501b73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 106
        },
        {
            "id": "c2c494c1-2cee-46b0-8d50-07e9873c3a6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 107
        },
        {
            "id": "2e1adb56-6f0e-4f65-a3f9-786f2a4104fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 108
        },
        {
            "id": "6efc8ce1-78fc-418f-a8d2-5f10898e4414",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 109
        },
        {
            "id": "230de4ae-010c-49bc-b924-48e893da8032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 110
        },
        {
            "id": "29e456cb-9f9d-477d-85d1-457da4c42c56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 111
        },
        {
            "id": "60cecbe8-7b8d-4040-857d-8e532213e5f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 112
        },
        {
            "id": "6c6ee68b-5e3f-4126-af9f-6eaf4ec6ca84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 113
        },
        {
            "id": "e582ee21-39fe-4429-beb3-acdaf7debda9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 114
        },
        {
            "id": "05728c78-23b1-441c-aaf3-395dff75ed13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 117
        },
        {
            "id": "de1d91a8-60c7-4e1c-82ed-826760c66839",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 118
        },
        {
            "id": "62ed76ee-d0eb-429f-8dd6-e922ea57fb46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 119
        },
        {
            "id": "e1216396-ab3f-4aab-9859-8d446a153f92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 120
        },
        {
            "id": "2c7244aa-a1f3-48a8-b50a-78da4d4a24f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 83,
            "second": 121
        },
        {
            "id": "318ff1d9-55d5-427f-8484-d0b80ec5b722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 51
        },
        {
            "id": "369db584-1870-4da8-895a-f03562c6cb3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 52
        },
        {
            "id": "3770b2e9-1980-4e5f-8566-c3e3c2cedc7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 65
        },
        {
            "id": "57e3603c-9a65-482c-885d-80b03f02a5e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 66
        },
        {
            "id": "84d50ab7-7cdf-4430-afcb-a23d56f13434",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 68
        },
        {
            "id": "0841ad6e-6555-4647-bf6a-0eed395b5575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 69
        },
        {
            "id": "6ee63e19-eda6-42c1-8e77-f6addb45ba94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 70
        },
        {
            "id": "fe7fb54e-1b06-4640-a1b6-46a8c35b524f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 72
        },
        {
            "id": "f3ec5d7b-cc65-4445-9d70-5af8faa5380e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 73
        },
        {
            "id": "2b422673-544d-4c94-b8c4-21b13ea9e5bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 74
        },
        {
            "id": "a9eb7a01-de68-4aed-961f-37eef0e97444",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 75
        },
        {
            "id": "f067b91e-03a7-4aee-943f-0627a75a8dec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 76
        },
        {
            "id": "d3df0411-3c3a-473f-ba5c-22bc8573ee06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 77
        },
        {
            "id": "aa5b610b-3664-47aa-8d37-67ed69f8df08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 78
        },
        {
            "id": "cb9af3aa-e9c5-4bae-8879-82d23e24fe78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 80
        },
        {
            "id": "2de30323-c24b-4f22-ad86-f2d00f4efd88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 82
        },
        {
            "id": "28d8763f-b7b2-4a0f-8b7f-d63507f81204",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 85
        },
        {
            "id": "97f7f15a-796f-46a2-ae77-01b7b124afc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 97
        },
        {
            "id": "c4272cfd-8d59-46fe-9016-8c20e5c4076a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 98
        },
        {
            "id": "75950ba3-cf98-4bb7-b3c5-cbbf76e0a5a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 100
        },
        {
            "id": "d3d08e46-0e12-4a8b-94ab-cb0d0fc4ee79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 101
        },
        {
            "id": "487cf08c-2b00-4b06-8cab-80890201fbde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 102
        },
        {
            "id": "784bfbc6-8553-4c65-a051-bf5942bbf896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 104
        },
        {
            "id": "533a84e2-28b9-4d7f-8310-a6dc031f3394",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 105
        },
        {
            "id": "71c81aac-1eee-458c-8d1d-a09f63ab1752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 84,
            "second": 106
        },
        {
            "id": "e0a4ca47-0ce2-4049-b440-59b1c3b4b638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 107
        },
        {
            "id": "6836c5da-3c0a-4f0a-813e-fe1843230b83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 108
        },
        {
            "id": "edfae62e-9f79-4811-8ff1-e463824801f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 109
        },
        {
            "id": "b6137b9a-76fa-43b3-909e-307ad4e98728",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 110
        },
        {
            "id": "d03eb68d-b74f-4aa0-a2c7-9152ae996297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 112
        },
        {
            "id": "1dea3a60-06c9-4a96-98ef-e4c5a662bfc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 114
        },
        {
            "id": "d38fc70e-5f52-4832-99f1-c6a2d19f215d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 117
        },
        {
            "id": "d6d26597-3bbe-4426-a96a-38c57a932487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 48
        },
        {
            "id": "690db995-427e-4614-92ed-ed9b857b6dfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 49
        },
        {
            "id": "b5473ded-ece8-4e18-9bdc-960ebeda44bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 50
        },
        {
            "id": "ff034fa3-a84a-4ffa-8eef-9807309fff92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 52
        },
        {
            "id": "f91b538c-e0d6-4f66-9906-5ab8a8f55806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 53
        },
        {
            "id": "dba6a78b-73e5-4bed-a7d3-a2042cd8e81d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 54
        },
        {
            "id": "35dd05d9-b161-4b54-870b-726b5e32db25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 56
        },
        {
            "id": "3a10d1a6-94b3-4a45-948d-adbb4f56780a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 57
        },
        {
            "id": "63c8e824-4f86-47dc-9f45-2052bf72302c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 66
        },
        {
            "id": "e4531df9-64f9-4c57-987f-619a80715134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 67
        },
        {
            "id": "ff82c334-09d3-4d67-a927-abf622c818e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 68
        },
        {
            "id": "a2e9066f-9ba4-4d98-b1d1-5bfba08f02aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 69
        },
        {
            "id": "2ce2bded-b66e-494f-98db-9f7eda0f73d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 70
        },
        {
            "id": "c59a5e84-c073-4002-9db1-dadf6bb8885c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 71
        },
        {
            "id": "72cd8291-448a-4009-9beb-11fb2ec3b760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 72
        },
        {
            "id": "f769ba9b-43af-46f4-922c-8f0120c68a58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 73
        },
        {
            "id": "e8e8b57c-c144-4a78-ac1d-f7c8d05df228",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 75
        },
        {
            "id": "59f5518b-06c6-47d8-9f69-e1aa216dd8b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 76
        },
        {
            "id": "6b5495ec-dc6d-4f8c-aa4b-49bef857ed05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 77
        },
        {
            "id": "3e3c6d7a-3c3c-49c7-b22c-1833e1de519b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 78
        },
        {
            "id": "d7dc2f22-761d-4304-81a5-54e8f5a8d83f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 79
        },
        {
            "id": "b7fdece8-429c-4a30-9a54-6628061fa44d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 80
        },
        {
            "id": "5bdb85c9-d007-439c-8cfa-78fb099d7968",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 81
        },
        {
            "id": "3c43cfe3-b154-43da-93db-3e18f9554908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 82
        },
        {
            "id": "6900b39a-4048-40ea-8345-3b3fc36bb814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 83
        },
        {
            "id": "78a90c1e-31d5-441a-8145-6c8c4e229ad7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 84
        },
        {
            "id": "ed28a963-ce95-47e1-90f5-9a78d6bc649a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 85
        },
        {
            "id": "274cf35e-7781-4e54-a051-db5d940a035d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 86
        },
        {
            "id": "1cc8b5be-ec1f-4eeb-bb23-bfe8df472a86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 87
        },
        {
            "id": "fad85af4-722c-4ca5-b2a1-3dd382f918ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 88
        },
        {
            "id": "6c2375ac-1366-4e61-823a-bfa3ff794547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 89
        },
        {
            "id": "55f5bffd-c166-490e-8f65-6bb64451426a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 90
        },
        {
            "id": "6d3f98cf-bcdb-44af-8be3-8e5b95d5c8a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 98
        },
        {
            "id": "3b4bc257-aff2-452f-ac7d-069bc6c9b032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 99
        },
        {
            "id": "2ffa29f1-7a41-462d-a220-d535d4d96a2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 100
        },
        {
            "id": "6ab71ec4-aca5-4925-8805-c9be3de30cb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 101
        },
        {
            "id": "010d95ad-9815-4ebe-8b3e-330b4ccceb85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 102
        },
        {
            "id": "9e71dfcb-05f8-4e61-b4c6-ce62a95b8644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 103
        },
        {
            "id": "253948c8-bf78-40a5-a31b-2c8a7d0e6b43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 104
        },
        {
            "id": "68ec93a9-3287-4828-ba7a-e10c5ac7c683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 105
        },
        {
            "id": "73699001-e618-4f44-a615-3a945e02f465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 107
        },
        {
            "id": "dd540e76-85d3-4de8-ab94-610f2e18c783",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 108
        },
        {
            "id": "cac1bedc-54e1-4cd7-a86e-bb502cbb1dbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 109
        },
        {
            "id": "396e141f-5130-41d1-9c8b-77a2b51d22d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 110
        },
        {
            "id": "840157a8-8e80-40a5-98e0-d463257d3703",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 111
        },
        {
            "id": "de36fff0-e764-4937-9caf-4fc97b22672c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 112
        },
        {
            "id": "e3e1fce6-1f3b-4e41-a833-440d13c3cd57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 113
        },
        {
            "id": "dca9e1d0-c763-41a2-bad4-744b419a376f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 114
        },
        {
            "id": "aad96610-24cb-48ff-8994-5c98b445a284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 115
        },
        {
            "id": "d01cc825-75d5-4756-a22f-2f19eced36ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 116
        },
        {
            "id": "843c00ce-3a8f-4f4a-ae55-f88d29344edb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 85,
            "second": 117
        },
        {
            "id": "ee2dc5ae-3e2b-4eea-9589-e64b5ad20d4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 118
        },
        {
            "id": "b97c3c81-f6e0-494b-9876-27997da8e905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 119
        },
        {
            "id": "affbc898-9ef9-4097-9556-34bcb52e6aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 120
        },
        {
            "id": "f10c75d5-8d34-40a0-8cff-d732c092a5cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 121
        },
        {
            "id": "6b009069-0d37-432e-a6ec-ec4873cc2f1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 85,
            "second": 122
        },
        {
            "id": "e3723e7a-3ef6-42e0-ab70-db226d0e9881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 50
        },
        {
            "id": "c18a27d5-3262-4e72-b006-71870ca46024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 51
        },
        {
            "id": "c2f02929-02fb-4169-8e1c-fc6ee714ed99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 52
        },
        {
            "id": "2968fe5a-4d20-4367-9099-d61fd3efdb12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 56
        },
        {
            "id": "fa6213bc-9c9f-49a1-a260-2fcf1ebeaccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 86,
            "second": 65
        },
        {
            "id": "8418bcfe-2800-4c75-b48c-e30a5d96711c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 66
        },
        {
            "id": "ad9c482f-c0e3-4167-9f10-2629e3fed3da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 68
        },
        {
            "id": "57a96629-a59a-461d-82d8-585619c85c75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 69
        },
        {
            "id": "37987595-53a9-40f0-854e-b92c6936976c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 70
        },
        {
            "id": "8ec9a08d-5b73-4717-af6b-5aa9b47b3adf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 72
        },
        {
            "id": "8b73520a-6ee0-4609-b5d9-328f5b0aade2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 73
        },
        {
            "id": "bf4a48be-27c7-4162-a344-fe4d7edb467d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 86,
            "second": 74
        },
        {
            "id": "796ef171-f4e1-4df9-b2f0-4b4cd5cd71c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 75
        },
        {
            "id": "9128bb36-96c9-4690-b0df-0625a5e2f60a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 76
        },
        {
            "id": "14b361dd-a7af-4bfe-bcc2-2a2d55e99d3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 77
        },
        {
            "id": "872f354f-c484-4e51-a61c-57e2001dbcf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 78
        },
        {
            "id": "abc07ce2-e052-4242-ac6e-3f2dbb644044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 80
        },
        {
            "id": "ee73fc57-c832-45c3-87a6-3be245738d01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 82
        },
        {
            "id": "2426246d-9d43-47c8-bdc5-364866193eb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 83
        },
        {
            "id": "064a9c94-63fb-4684-a6e0-b8755c5979b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 85
        },
        {
            "id": "44a18dd5-9a85-46b1-b68f-b543b43ae6f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 86,
            "second": 97
        },
        {
            "id": "3264f5bd-2d79-449f-b2c7-50a977f9d8bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 98
        },
        {
            "id": "d0c49101-ca42-4bb8-b1c0-bb198940b170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 100
        },
        {
            "id": "e93600a5-bd16-4e69-9239-54846645e23c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 101
        },
        {
            "id": "44582430-964a-4752-af1b-8ade188c22cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 102
        },
        {
            "id": "7ca59d33-10d0-4e11-87d6-b4464d08c1e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 104
        },
        {
            "id": "64e348d7-a819-441d-9ebf-224ae812675d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 105
        },
        {
            "id": "29bcab11-7786-48f5-a061-fdd716e5241f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 86,
            "second": 106
        },
        {
            "id": "e1cfae71-0cb9-4125-86b1-00172625047a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 107
        },
        {
            "id": "34cd97a9-c542-47ad-96b6-4de9b50f0ba4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 108
        },
        {
            "id": "d1883542-4414-4998-9933-9fec5122d965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 109
        },
        {
            "id": "755eb7e8-1494-49c6-b565-da6a943ba15c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 110
        },
        {
            "id": "77e7f9ee-29df-4122-97d7-d42c4ee7144a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 112
        },
        {
            "id": "719e99a5-1dbc-4314-8ffc-445792a5e6bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 114
        },
        {
            "id": "7679f5df-6a8d-40cc-8b41-a2d24397554f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 115
        },
        {
            "id": "db9475bb-be5f-4ef0-a295-31900f6f80ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 117
        },
        {
            "id": "50fefa28-6172-428e-9c98-43b789d28b7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 50
        },
        {
            "id": "0d78b60c-13bd-4c57-b938-97e23537193f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 51
        },
        {
            "id": "85b94fca-55f6-4e4e-9ecb-ce8874fec6cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 52
        },
        {
            "id": "e0c08fc0-d018-4d4c-90ff-a88b22d5b8e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 56
        },
        {
            "id": "a393e86e-b8b5-4170-afcf-b316517323f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 87,
            "second": 65
        },
        {
            "id": "9d1c0975-8da8-490c-bc4a-57cf6ce61d2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 66
        },
        {
            "id": "fb73a164-5354-44fd-b46a-35a96476bda1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 68
        },
        {
            "id": "22d62830-8535-46f6-ae6b-08c6fc3e5984",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 69
        },
        {
            "id": "2613e63d-1870-4a49-9c9d-45182cbe898f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 70
        },
        {
            "id": "7fbb13a3-0e40-4a5d-bcf0-14a14344edec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 72
        },
        {
            "id": "68ff5fc1-fafe-49a6-b112-a5eb4462f9ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 73
        },
        {
            "id": "cc2cfcbe-c0ef-495c-a2a2-7ba417c8276f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 87,
            "second": 74
        },
        {
            "id": "2507d76a-835d-4887-9006-66881bfa80f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 75
        },
        {
            "id": "f6df5540-74a0-460c-bd7c-f93f295fab8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 76
        },
        {
            "id": "80497c47-723c-49fa-b689-d407b45383df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 77
        },
        {
            "id": "0f826d80-5153-4709-86e0-32b93d653f10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 78
        },
        {
            "id": "8e512441-43ef-41bd-8571-2b05a04d9d0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 80
        },
        {
            "id": "e283139d-5dac-4b73-ac4f-7816cd16f66c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 82
        },
        {
            "id": "398a8852-1a98-4252-ae70-0ecad955cdf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 83
        },
        {
            "id": "e62c1936-00f3-4a2a-a67b-93c3eed26462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 85
        },
        {
            "id": "b983dec1-096e-4ec3-9184-6c3fd3cfb26f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 87,
            "second": 97
        },
        {
            "id": "783857a3-ead1-4cf7-9143-9e0e0facdc45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 98
        },
        {
            "id": "321114a0-6c48-4017-994b-11cd411752f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 100
        },
        {
            "id": "80627ff9-6bda-4dbf-8cb1-4e64a19cd73e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 101
        },
        {
            "id": "e69e6e33-dcd1-48ca-b8bf-fbb8b9a7dfe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 102
        },
        {
            "id": "7dd5f952-6f49-429d-8dfa-dabe612d9943",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 104
        },
        {
            "id": "9589be12-f850-48aa-a08c-44f828ecb4ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 105
        },
        {
            "id": "8348d797-c99c-403a-9efb-74981ea26603",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 87,
            "second": 106
        },
        {
            "id": "5134bc7f-e842-46ed-8011-d02d0ed1bdee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 107
        },
        {
            "id": "2ca5e82c-8560-4c87-8b3c-51ed6492f1db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 108
        },
        {
            "id": "af92417a-1e6c-45e5-886a-dc516603e84d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 109
        },
        {
            "id": "95114bde-5110-4b3b-bb4d-a072a76a0808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 110
        },
        {
            "id": "0eae1b0f-d427-43d7-82f3-82bf5b956220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 112
        },
        {
            "id": "b67e6e3f-f992-40f5-b3bb-b79c73e86d0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 114
        },
        {
            "id": "d66a1ce2-557f-4b85-a9ea-93ae0639a63d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 115
        },
        {
            "id": "08f9d566-f598-4045-920e-0a6e0d1c0b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 117
        },
        {
            "id": "cf78a980-51a5-4dfa-92a2-8af02f86a9b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 48
        },
        {
            "id": "bb85e140-feb7-4193-bc81-485f3f377c0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 49
        },
        {
            "id": "57700470-e286-4bba-9a8f-574c453ea2f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 51
        },
        {
            "id": "3b41d90a-8339-4737-802f-cc40e6f8db66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 52
        },
        {
            "id": "5ebaec8d-e92f-4d45-8109-90046e744441",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 54
        },
        {
            "id": "59f1d162-1582-4315-b3e0-7c84812e5b77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 56
        },
        {
            "id": "ef7efa36-9ec8-4cee-a9bb-307d22ad37c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 57
        },
        {
            "id": "12f80fb4-7dbe-4ea2-8828-bd8d7bc2bcf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 66
        },
        {
            "id": "e276975b-3824-442e-86b2-87c33a3c3f89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 67
        },
        {
            "id": "63cfcfac-29f4-41ff-99d4-7c75ecc2db5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 68
        },
        {
            "id": "f9edd009-0da0-494a-adaf-42fcd16710e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 69
        },
        {
            "id": "b63efdd7-24cf-4e0e-be54-c7ea65ed4617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 70
        },
        {
            "id": "dbf75cff-c34d-493b-b6ec-4c4889c9d028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 71
        },
        {
            "id": "40076080-6246-4602-aa42-bf0123453b91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 72
        },
        {
            "id": "48dbf6c2-cbcb-4815-8d38-d6980a8baa48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 73
        },
        {
            "id": "a120175f-0df0-49fb-afaf-388d1d453473",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 75
        },
        {
            "id": "f21f5550-6e7a-4453-b30f-c337f12cf270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 76
        },
        {
            "id": "0c81a5fe-4cc3-4ff2-a333-4cd68a95f395",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 77
        },
        {
            "id": "b4f641c7-b738-40e2-a634-5d30ce97dd1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 78
        },
        {
            "id": "d5277cae-602b-4ba0-95b7-ae10a6e5a2fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 79
        },
        {
            "id": "2e3ab2a3-e071-4db7-a139-293771f7f454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 80
        },
        {
            "id": "2f8b6ac7-46f8-45f8-9a66-88c280d9039d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 81
        },
        {
            "id": "2d2aabd2-d70f-4c6f-a18b-351cdc9ce9ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 82
        },
        {
            "id": "285a72c8-43eb-4c36-ad37-d3c7bdc1c6b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 83
        },
        {
            "id": "55f39baa-a071-4f74-976a-06e77319943b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 85
        },
        {
            "id": "b48ea4c5-1911-4f22-8250-1ce98feaa7d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 98
        },
        {
            "id": "92fbf0ea-6364-4a9a-8586-aaf2d11bcc86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 99
        },
        {
            "id": "16b9ce1f-b650-4572-86a2-55bd6249d84b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 100
        },
        {
            "id": "487f632c-82cf-4ebe-9bce-d25a5722a579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 101
        },
        {
            "id": "6d395c81-7bc7-4e94-8c05-4af8db4045e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 102
        },
        {
            "id": "2ab9e8c1-c070-4739-81b4-510bbca2e584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 103
        },
        {
            "id": "1375c89d-0d1b-4120-b098-2ead0cb15ddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 104
        },
        {
            "id": "67aee6df-1843-4048-b0d7-2234e2c1f1b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 105
        },
        {
            "id": "6cb77b75-dc7f-4a31-b47c-8bda617d0ab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 107
        },
        {
            "id": "4178fc1a-498f-4b98-ba24-8f828d5c3b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 108
        },
        {
            "id": "53ea31f6-d8f3-45ca-88d3-002b8a926716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 109
        },
        {
            "id": "85ae4dca-afed-4999-9a59-c23eb8577fca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 110
        },
        {
            "id": "5a1c1b77-eb8e-4830-83c4-dfc8cbd50e5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 111
        },
        {
            "id": "324c4aff-196c-4757-adaa-0b59baac3e81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 112
        },
        {
            "id": "f37830a4-84b3-40c7-8393-8eff35d32b67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 113
        },
        {
            "id": "d5eea624-4d56-4311-af8b-055224a7f141",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 114
        },
        {
            "id": "e2abcc33-7db5-41c8-91c1-c6be9e7850a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 115
        },
        {
            "id": "99a3a5bc-9c9a-4069-a157-7189ea82b143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 117
        },
        {
            "id": "a51a7e2d-d51d-46f3-863d-d5cd879667d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 48
        },
        {
            "id": "7225dd4f-2318-4ec4-854b-c69780b12013",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 49
        },
        {
            "id": "bbf57a68-474e-411f-bf04-05fb55713a80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 50
        },
        {
            "id": "84a75375-0cd7-4ab0-af47-9deb3dc8d2ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 51
        },
        {
            "id": "86649a1e-9999-41e6-92d3-a054559170f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 89,
            "second": 52
        },
        {
            "id": "7718d61e-24e7-4f13-ae68-ab922e5074fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 54
        },
        {
            "id": "174f3be1-40d7-4769-9d94-fae18874941d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 56
        },
        {
            "id": "9ab8716e-4655-46ae-9b62-8ef40c6db479",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 57
        },
        {
            "id": "4694c08d-36f7-466a-bc91-1cfa11f04e6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 65
        },
        {
            "id": "2b64abf0-4425-4283-a695-36c0ac73525a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 66
        },
        {
            "id": "ed9134ac-23ca-4d78-83f6-6c041a711921",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 67
        },
        {
            "id": "8c3650cc-dea6-40ee-95b1-3fbf6ad74d6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 68
        },
        {
            "id": "e319d9bd-1e3f-4226-a37a-d232d38c050f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 69
        },
        {
            "id": "bd260b7f-32df-4233-aa75-17824beab301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 70
        },
        {
            "id": "38114117-b1d5-45ed-800a-4bc4db42f40f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 71
        },
        {
            "id": "b8518824-9dc6-4fbb-a11d-5588d8f9fa45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 72
        },
        {
            "id": "4a2cf591-a8db-4ff0-8fa6-e3365b49d327",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 73
        },
        {
            "id": "a387ed45-371c-4497-b5ce-736da8f60f72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 89,
            "second": 74
        },
        {
            "id": "cf42d922-6ff2-4823-b06a-aed9205b36fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 75
        },
        {
            "id": "a8e46689-58f7-44a1-88db-576019755349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 76
        },
        {
            "id": "732fc3bb-85a3-493b-be58-87568ad880d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 77
        },
        {
            "id": "9bbe3d33-e175-4eec-a949-fbf0511db81b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 78
        },
        {
            "id": "36d56931-ff2e-4f54-a631-6eb1aa111e35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 79
        },
        {
            "id": "12e7875b-0e60-4701-9a0e-52669197caf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 80
        },
        {
            "id": "877bbcd9-30d7-40aa-aa6a-10284354b9b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 81
        },
        {
            "id": "ecea0e93-7aa1-40b6-b678-f4ad1a722382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 82
        },
        {
            "id": "f5047ee9-9e2c-480c-93bd-a0b3d77fbe53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 83
        },
        {
            "id": "dd143997-e21b-4899-8635-906e4c328e78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 85
        },
        {
            "id": "a3517448-b842-4d56-ae63-b3048eee684e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 89,
            "second": 97
        },
        {
            "id": "86b13c91-04c8-4324-934d-c308e34f37d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 98
        },
        {
            "id": "5cbec73a-56bc-481b-8834-b29d0ed1d599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 99
        },
        {
            "id": "4f1282e8-0b21-46d4-b4d1-ff65ce1ffe0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 100
        },
        {
            "id": "e896d12a-4ed2-4484-9cf5-13b44ddd8d8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 101
        },
        {
            "id": "3b812a15-a724-45d4-a834-17e7a312d3f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 102
        },
        {
            "id": "0daa238c-2945-4a99-8363-6ac45370bf58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 103
        },
        {
            "id": "aa993546-117f-4052-a0f5-d3c9ec043ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 104
        },
        {
            "id": "f3a8eb24-116b-4604-a078-db45bbf8dee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 105
        },
        {
            "id": "7dde8778-b61c-4179-98a4-bac806f581c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 89,
            "second": 106
        },
        {
            "id": "af37f7d8-7db8-4254-a19e-08306ea8645c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 107
        },
        {
            "id": "0e1c3d40-ce6d-4711-8894-b7917d4f05d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 108
        },
        {
            "id": "99f0c4fa-e8eb-4a19-871c-e27c55e2ebca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 109
        },
        {
            "id": "9ff49ee9-00cc-469c-86f6-b44fcdbb0393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 110
        },
        {
            "id": "20b3c7ce-cfac-4662-a8c6-a87cba351f40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "13bbf7bb-16a6-4c26-956d-f21cca0a241d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 112
        },
        {
            "id": "2d149578-c86f-44c3-ad8d-16364bdb6b99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "0de18730-b12b-4380-a1fd-038013825dcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 114
        },
        {
            "id": "855c01a5-a796-4aa7-9b0b-cc7bee03a027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 115
        },
        {
            "id": "e3150f97-29f9-4cb4-b18a-e460c990a93c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 117
        },
        {
            "id": "8b6272a1-f2e6-4bfb-bfc0-bef262c07775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 90,
            "second": 52
        },
        {
            "id": "51eb88dc-56de-4028-a4d1-3b8135b4d045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 66
        },
        {
            "id": "bdf461c3-dd50-43a5-a2c4-0b9030e06ae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 68
        },
        {
            "id": "5e501f5a-3e3b-4785-bcae-5ea486dbef97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 69
        },
        {
            "id": "bf2e8d45-8b0e-4dce-8780-7dfdc69c7a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 70
        },
        {
            "id": "df84bfaf-3c36-4cf0-ac58-906be4818782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 72
        },
        {
            "id": "ab91bfe5-f4e3-458e-8550-0f1675454f0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 73
        },
        {
            "id": "17606655-dee8-4561-b3e2-8545018251c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 75
        },
        {
            "id": "1fe0b46f-ffe5-4a1d-94d7-ca494cf447ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 76
        },
        {
            "id": "fe617e40-8bbf-4a66-9128-df2ddc81c8d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 77
        },
        {
            "id": "1c8e276b-f8df-4d14-afe7-60a9700c9e44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 78
        },
        {
            "id": "ebba9c8c-0a50-438e-b4cd-eb709c93969e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 80
        },
        {
            "id": "8a5d266f-6115-406e-9a5e-4b254a80fa7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 82
        },
        {
            "id": "eda7433a-2607-42a8-9b50-3d30ae840697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 85
        },
        {
            "id": "65848d3a-2502-48ce-ad21-742ce555196d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 98
        },
        {
            "id": "ebef002e-e01b-4124-925e-60e65280d504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 100
        },
        {
            "id": "3bd06a2b-26c6-4549-bbcb-f50851c136f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 101
        },
        {
            "id": "0e41f9e7-301f-475f-b722-dccd4241af0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 102
        },
        {
            "id": "ecdad967-341a-45c8-a2f0-0be30a9d7911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 104
        },
        {
            "id": "6c71c376-77a9-4d1e-8bb4-81ffc2cc8b8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 105
        },
        {
            "id": "bf2a43b5-fc87-4082-aafb-cf1fd45f4722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 107
        },
        {
            "id": "5f009488-a709-4b3d-a484-2c737e8c61eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 108
        },
        {
            "id": "dc1be50b-8796-420c-abe3-01065e02f0e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 109
        },
        {
            "id": "1110e2e2-1608-4602-be93-4ef04031bea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 110
        },
        {
            "id": "b2b7de02-c39d-48ed-b9aa-9cf356a202cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 112
        },
        {
            "id": "c22038dc-67c9-4ada-9160-bbf561b49d78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 114
        },
        {
            "id": "80729c52-b6b5-4f3f-98db-eab1dad4c142",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 117
        },
        {
            "id": "5f22acbd-73aa-4a5d-ab29-9370c57bcc3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 97,
            "second": 49
        },
        {
            "id": "e519275a-8db3-4870-b1ee-723516de6538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 51
        },
        {
            "id": "c17d11c2-8b83-43d3-b3d8-d06bd5739654",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 52
        },
        {
            "id": "8eb5058f-4580-44c0-8ac5-b5f1aaa9be69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 97,
            "second": 55
        },
        {
            "id": "c67a7369-259b-419f-9110-5ec0b1fd97f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 66
        },
        {
            "id": "43341ce4-5161-4e3c-9165-eb1c2382bb80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 68
        },
        {
            "id": "97d20f50-1abe-4760-abcf-517a58541d3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 69
        },
        {
            "id": "769549ae-81e6-41ae-82f2-a216600b7795",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 70
        },
        {
            "id": "0ff61de0-da38-441a-958a-161b8962cacb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 72
        },
        {
            "id": "962f7d01-c70b-4373-bcee-47d31d79bad0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 73
        },
        {
            "id": "32fd0fb5-de02-4b4b-b845-4b3aeafecd82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 75
        },
        {
            "id": "e9a48f8d-c162-43bc-867d-fc4fe74be635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 76
        },
        {
            "id": "506d2589-5fa2-4aad-8478-18dd6d016571",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 77
        },
        {
            "id": "a0970195-2c66-4dfa-8c9e-62d22af6e95a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 78
        },
        {
            "id": "ab9e4256-1735-483e-ba98-6112a0f479ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 80
        },
        {
            "id": "7bd9376d-76eb-4ff9-8c3b-1423a695be44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 82
        },
        {
            "id": "661bfe91-ec20-4b1e-b1f6-81a97f178c2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 83
        },
        {
            "id": "be77e21d-cbd9-42e5-afcd-5b7a316a873a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 97,
            "second": 84
        },
        {
            "id": "14a880a2-9d96-43c7-8977-2834a83e07cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 97,
            "second": 86
        },
        {
            "id": "ee637a3a-9d36-498c-b51c-3b2f1baac873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 97,
            "second": 87
        },
        {
            "id": "0286c1f6-77e2-40a8-b5f5-39c2b807aa3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 97,
            "second": 89
        },
        {
            "id": "115234fc-cf97-44c9-a687-2471d3430298",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 98
        },
        {
            "id": "4648c16a-3fa9-40c5-be5c-810fe973c570",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 100
        },
        {
            "id": "d447df4d-65b1-4e0e-99e7-1ebfd71a283f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 101
        },
        {
            "id": "8575dbba-44ef-4b79-8891-37f1abfa26f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 102
        },
        {
            "id": "93b79a8f-0ad0-4d1c-93d8-1dc7deaf5abd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 104
        },
        {
            "id": "97d57456-db58-4901-9087-2f79571ad648",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 105
        },
        {
            "id": "ab8f465d-8928-4e22-a3ab-d1b7aa3c42a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 107
        },
        {
            "id": "32867926-857e-4e3e-a7ad-acf8889235bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 108
        },
        {
            "id": "269025be-d95b-4317-b8d8-9b5981fb034b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 109
        },
        {
            "id": "52c6f58d-0b1b-44f0-9f50-81d5a0a56e51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 110
        },
        {
            "id": "a81c38d7-73ca-44bd-959e-393bf95d1530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 112
        },
        {
            "id": "fc56b51c-79b1-4e5d-b018-f46fa048752a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 97,
            "second": 114
        },
        {
            "id": "dd2b65e3-b0ea-4f33-9eab-adc84ed85c4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 115
        },
        {
            "id": "16a5b0ef-012f-48b7-8363-18251e080ea3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 97,
            "second": 116
        },
        {
            "id": "099985d5-e833-4662-b74f-9871f1b5f5e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 97,
            "second": 118
        },
        {
            "id": "f493cb40-fa55-40ed-88c3-aa454225de1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 97,
            "second": 119
        },
        {
            "id": "9998105f-d3e5-44d5-a48f-fc0d62610009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 97,
            "second": 121
        },
        {
            "id": "278e6196-4402-454f-9bf6-00f473ed966f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 48
        },
        {
            "id": "1e5cf762-e85b-4063-8517-9435ada47597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 53
        },
        {
            "id": "c7c61747-682f-4b8f-bd32-25f7a0d316cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 54
        },
        {
            "id": "f4498713-3803-44ce-aee8-129867ac917c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 55
        },
        {
            "id": "10a90d63-d139-437d-b994-75f4684ec309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 56
        },
        {
            "id": "89a7387e-d391-4065-a198-f5003a0ebbd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 57
        },
        {
            "id": "29a594d5-9136-4c23-b9f0-f3cbcfa5dc8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 66
        },
        {
            "id": "9b4ea7c8-ebfe-444e-ad8a-ca88a9e73b9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 67
        },
        {
            "id": "be8bc303-1ab7-41e9-a307-98231399453f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 68
        },
        {
            "id": "aa24d4c8-dca0-47fd-ad2f-67a6e0d0853d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 69
        },
        {
            "id": "4ed84476-a62f-4680-8671-0365766a890c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 70
        },
        {
            "id": "733f62a3-e3ec-4ab1-9599-e19f8d4489bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 71
        },
        {
            "id": "d0003f79-1981-4baa-985a-0524dba945f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 72
        },
        {
            "id": "bc1e9110-a7dd-475c-a14e-52df929fe630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 73
        },
        {
            "id": "2873861e-e0e7-458c-8f25-92bbc4241316",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 74
        },
        {
            "id": "45a25538-512f-498e-b015-031b657b4991",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 75
        },
        {
            "id": "a93a1c3a-5660-4f95-8d48-88eed2c4c33d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 76
        },
        {
            "id": "a9d1cd41-c70c-49c0-a7e1-f1846555ea0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 77
        },
        {
            "id": "18253a7a-4721-4ea3-a448-1b49e10f17b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 78
        },
        {
            "id": "1f6864e7-85e0-4847-bc7d-9190b0ad4b94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 79
        },
        {
            "id": "dc19f57c-8b9f-416c-b115-b9197c746d2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 80
        },
        {
            "id": "91940ca3-983f-409f-a6e0-5b5ce42d5453",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 81
        },
        {
            "id": "1de00cbe-b18f-4b2d-9920-6f3f27a73f2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 82
        },
        {
            "id": "6dd2d081-e07d-4a47-b678-ae78e7747a91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 83
        },
        {
            "id": "97f5fc0f-0da8-40d0-bd0c-bb0402d2d5e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 84
        },
        {
            "id": "21979463-0d82-4fef-9f43-247f551c98b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 85
        },
        {
            "id": "148f1eeb-fcce-494a-b8e3-0ddfc9acceee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 86
        },
        {
            "id": "f8125106-75cc-4cf8-88bc-afdbc6bdb082",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 87
        },
        {
            "id": "1d84ec56-c1bc-4a30-bca5-e86e52747ab5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 88
        },
        {
            "id": "ebfa3135-f1b1-401d-9843-990f22978b00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 98,
            "second": 89
        },
        {
            "id": "22e044c9-77f1-48d5-b482-c4f97f4127e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 98
        },
        {
            "id": "cf4e85ca-23c3-496f-a490-422fbdddbe84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 99
        },
        {
            "id": "6a7c2609-c591-43f7-89c4-6c7bc7d85bb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 100
        },
        {
            "id": "8415f9f7-2fe9-43e6-a4eb-09bb6c798c50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 101
        },
        {
            "id": "96a7ad10-d20c-4a97-a663-8cf2ea7eeeb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 102
        },
        {
            "id": "4b9acb3c-f1bb-4fe2-bf0e-eabdf166eb75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 103
        },
        {
            "id": "f8583f5c-cb22-4399-9dcc-a0098d52fcf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 104
        },
        {
            "id": "3dfcbff5-20fd-467d-8511-4f3344b9c836",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 105
        },
        {
            "id": "1e69bd34-47f6-40fe-b046-d280984374a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 106
        },
        {
            "id": "f22a2cd2-bb7c-443d-8d2e-3df25d72d1b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 107
        },
        {
            "id": "8a0de6c0-34c0-4f11-bb3a-01ec785f8a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 108
        },
        {
            "id": "992c9449-8d97-4245-86ce-0016186a7fae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 109
        },
        {
            "id": "0a1839ca-e00a-497b-8052-0452f1d879a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 110
        },
        {
            "id": "e7fea744-6f63-47ca-b66c-d45d9f0afc6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 111
        },
        {
            "id": "6c073e14-4162-4800-9b96-ce4810971c6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 112
        },
        {
            "id": "edc0a9d9-a45e-480b-971f-8e5bf31f8899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 113
        },
        {
            "id": "4897400f-e787-4018-975b-d8dcb96fddb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 98,
            "second": 114
        },
        {
            "id": "031be215-e6c4-47ac-835a-f2c4f5dc35f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 115
        },
        {
            "id": "31be16f1-b658-4bc7-98a8-8120dc86b256",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 116
        },
        {
            "id": "61665b6c-294c-4009-8f66-5f3d6102774a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 98,
            "second": 117
        },
        {
            "id": "9fa2b112-58a9-41b7-8dc0-f3cb03e21a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 118
        },
        {
            "id": "db28072d-dc31-4b91-a2f8-508f7b13ba24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 119
        },
        {
            "id": "46ad7b46-1d49-4bf5-a83f-81426922f14b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 120
        },
        {
            "id": "cafa6e4c-8365-482e-8edc-e0f667824738",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 98,
            "second": 121
        },
        {
            "id": "b9d54a4a-5098-487c-8bd5-86ad1f6b75df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 48
        },
        {
            "id": "b3cb6f67-9359-4a08-88a6-1b927e1936b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 54
        },
        {
            "id": "f23a3da2-0dce-46e2-badd-506adedbb031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 56
        },
        {
            "id": "edb4167a-4109-4156-ba00-43179e7bdf14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 65
        },
        {
            "id": "278ce6b3-19cb-4208-9a54-8041fd1cdd23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 66
        },
        {
            "id": "ad8d6f60-c807-4f8c-8759-816270b5dfb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 67
        },
        {
            "id": "7dbe69e0-626e-4087-83c0-cf41817bc3b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 68
        },
        {
            "id": "4034d49f-558a-45bc-8119-1b1532c2a7a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 69
        },
        {
            "id": "972540ec-036e-4435-8885-08be5834cb44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 70
        },
        {
            "id": "84bea247-f9fa-4bee-84ce-39db42c099a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 71
        },
        {
            "id": "220d4872-6be7-4410-acb1-cd9490a676ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 72
        },
        {
            "id": "0d0b5e87-6717-45c0-a094-998c5d8b8b64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 73
        },
        {
            "id": "9a9443dc-f8be-41b7-b829-d5e016d7262d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 74
        },
        {
            "id": "7feb3485-fe9e-425d-ab20-82d5c4de7a00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 75
        },
        {
            "id": "02e3883e-a251-47ef-a503-83044033783c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 76
        },
        {
            "id": "64533274-eed0-4582-b86c-454df0e1ab4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 77
        },
        {
            "id": "e939714b-390e-4351-be21-79fbef896767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 78
        },
        {
            "id": "45c71d72-338b-426e-a1ad-4210ed4d8d6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 79
        },
        {
            "id": "3c67fd06-706e-465b-8882-ed26b72659c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 80
        },
        {
            "id": "ae6554f8-2add-4475-8097-ca9a72a424bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 81
        },
        {
            "id": "033902be-1363-4b33-9f75-16260f101f16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 82
        },
        {
            "id": "d8b02c67-cabc-4a88-b2b1-67eef45bdcbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 85
        },
        {
            "id": "c8e9081b-82f5-4d64-8128-b103d981075f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 86
        },
        {
            "id": "e71295db-bd39-42c5-a92b-fe3798dab62a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 87
        },
        {
            "id": "85f402c3-1cef-4096-b6c1-f24d676710ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 99,
            "second": 88
        },
        {
            "id": "3b35276a-2fa1-47bd-abba-72f9a3e4e96c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 99,
            "second": 89
        },
        {
            "id": "4989b6a7-d054-43ce-8f9f-0589079cb055",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 97
        },
        {
            "id": "7b528959-dc83-44e2-b054-1446a14c6031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 98
        },
        {
            "id": "bd186c01-b737-4abf-a713-682708686c81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 99
        },
        {
            "id": "25dacabb-4cc5-4310-a328-bd881c486d40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 100
        },
        {
            "id": "5a56c1d8-7414-4153-ab89-a7b47eae371f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 101
        },
        {
            "id": "a4f93ca8-641b-4ddf-8411-90655467df8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 102
        },
        {
            "id": "30db2a6f-62fe-4170-a99c-e77859f55268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 103
        },
        {
            "id": "318ef9d9-64e3-485b-8aab-da301ea5799d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 104
        },
        {
            "id": "20ad5cb5-c470-470c-9a66-dfe7db336ff8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 105
        },
        {
            "id": "ff107404-face-4038-b5bf-283a01ba10f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 106
        },
        {
            "id": "eb0033cd-d9db-4b71-a83f-b863139801ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 107
        },
        {
            "id": "2bccf957-24ad-4a0f-b4ab-8e7831e4c359",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 108
        },
        {
            "id": "4830f6bf-18b1-4aed-b653-a3123ea01766",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 109
        },
        {
            "id": "9f18e65a-31ac-43db-ac91-f5b83c7ef988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 110
        },
        {
            "id": "34c1adc2-6d6b-401a-b0e0-07de496d890b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 111
        },
        {
            "id": "896e4bcc-3b91-4559-9d91-ae01e6508650",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 112
        },
        {
            "id": "6e2b0614-4d5e-4524-818d-d33749182f5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 113
        },
        {
            "id": "93fad9e1-ada2-4141-a960-56b801172a9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 114
        },
        {
            "id": "a7051f5a-33d5-43d4-b210-f73bd0799d38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 117
        },
        {
            "id": "3f51d632-6005-475b-8900-34aae646e6ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 118
        },
        {
            "id": "159bfdf1-67e9-4ebd-bf84-f0e4ae16ab66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 119
        },
        {
            "id": "865ecf28-c3a7-43a9-86e4-b678e84c6f07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 99,
            "second": 120
        },
        {
            "id": "dcbc4fc1-4362-409c-9f7a-8a3335ae5446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 99,
            "second": 121
        },
        {
            "id": "4ca94dd4-d381-46f9-9f4a-463446b65cea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 48
        },
        {
            "id": "84acb231-29d1-4413-867b-f5a90e0e7d7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 49
        },
        {
            "id": "84281430-69a3-450b-ae84-0851314763ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 53
        },
        {
            "id": "efb1e7c6-7326-4656-9eb3-f55592c5c9fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 54
        },
        {
            "id": "ce75a047-592f-443c-9e94-9268894020c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 56
        },
        {
            "id": "b69de91c-8c54-4567-a7c9-cb7b60e4a80a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 57
        },
        {
            "id": "e20e65ec-ddb5-4cf3-9abf-ccc96624c9b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 66
        },
        {
            "id": "ef00549a-a089-46ba-a612-8bab84a64368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 67
        },
        {
            "id": "ccb7dd46-228e-4225-ae53-2b51ccb3bb4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 68
        },
        {
            "id": "e8178246-bb2f-4825-97dd-d8f2737b38ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 69
        },
        {
            "id": "5e6af3e1-1d30-45a4-a3dc-262a43d02b8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 70
        },
        {
            "id": "2620de7d-966f-45b6-a2b6-503425a5681a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 71
        },
        {
            "id": "51448a69-2c80-429c-bb48-fdd3c909d243",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 72
        },
        {
            "id": "16fd2d5b-0ec6-4ab2-9320-19ed2019e5b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 73
        },
        {
            "id": "0ff6591a-7a6e-4eed-bb11-6a21a7651ea2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 74
        },
        {
            "id": "60e77952-077a-4f15-9eb2-927bcd2e067d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 75
        },
        {
            "id": "62e563ea-c833-4a11-85b9-80bb9ada5001",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 76
        },
        {
            "id": "ded29178-92d5-44f6-87ea-11f3712f29cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 77
        },
        {
            "id": "97877e04-f40f-4fa7-a22a-06652037909f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 78
        },
        {
            "id": "4b196d3c-3790-44d6-b404-282f11e0e890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 79
        },
        {
            "id": "02285344-4b3c-4734-9494-e5ec0ec7a6c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 80
        },
        {
            "id": "0ea6549c-7504-47b9-a7fe-8dc3c9471701",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 81
        },
        {
            "id": "bd6bccad-b52f-4724-a2fe-d4dccc62e8e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 82
        },
        {
            "id": "35cf07fa-c1e8-4b00-b5ba-f15f9a0317bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 83
        },
        {
            "id": "9c0cc529-e65a-4a84-9097-7cc81625d431",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 85
        },
        {
            "id": "eb785ff5-ee6b-4fec-9980-0638493c0799",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 100,
            "second": 88
        },
        {
            "id": "753bbc39-2511-4986-9dcf-1ed5be317a99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 100,
            "second": 89
        },
        {
            "id": "b251fc91-b162-47f9-b440-d1f18ff57a9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 98
        },
        {
            "id": "2ad63f2e-5db4-4fdf-996c-cafd32fcc6d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 99
        },
        {
            "id": "6405c8ec-3b4e-47db-b15e-d18876c33c08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 100
        },
        {
            "id": "a8bf6cf4-3a74-4291-8dee-16d788c44277",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 101
        },
        {
            "id": "28ca61aa-6b40-41a5-b88e-073b72e2dd50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 102
        },
        {
            "id": "4a4f4027-b83b-4818-802a-a53954245ab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 103
        },
        {
            "id": "d2da7c6b-0629-46d3-8537-e6eccd2356a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 104
        },
        {
            "id": "1561438b-19f3-43c3-b512-814d010fd606",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 105
        },
        {
            "id": "1adfcd88-3330-45d0-abdf-03a39f96d7f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 106
        },
        {
            "id": "a10ca088-c90f-4f9e-b7e8-8c16e8ac03fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 107
        },
        {
            "id": "55027c97-eb80-4e17-812b-c2bf153a3306",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 108
        },
        {
            "id": "a474397b-e70b-4c84-bd4b-79a477091abd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 109
        },
        {
            "id": "f2197fc4-6b36-4b08-a6a7-0b63ff01a79b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 110
        },
        {
            "id": "6112aba3-4b37-45e0-9045-ad50be8fbc4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 111
        },
        {
            "id": "0c240367-fa3e-437f-a238-81825ad340bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 112
        },
        {
            "id": "03948c7d-3beb-4d8b-bbd3-6ccf986563c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 113
        },
        {
            "id": "85ce79ac-7754-44c6-9150-c2a2ce9f4ab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 114
        },
        {
            "id": "ca5ec057-5785-4def-8241-136ebaff11c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 100,
            "second": 115
        },
        {
            "id": "c9c013e0-b40a-4d6a-9c45-7ce3a01efe32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 100,
            "second": 117
        },
        {
            "id": "a6710815-85fb-4536-bf80-c433f7389eb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 100,
            "second": 120
        },
        {
            "id": "be7c6458-4c8e-4241-9fe5-51effdb9b1cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 100,
            "second": 121
        },
        {
            "id": "1fda79d5-3a0e-4141-8db3-045cd10c6d34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 101,
            "second": 52
        },
        {
            "id": "29b4d9a2-3dd2-497d-b2df-89c74d5c8f9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 50
        },
        {
            "id": "a0060c11-b481-48d5-9e9e-99378a00caee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 51
        },
        {
            "id": "39eb38bc-667c-45ba-a2fd-6cf6add399cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 102,
            "second": 52
        },
        {
            "id": "bd47422b-0da4-441a-8b6b-300b7ceaf464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 102,
            "second": 65
        },
        {
            "id": "0347bf4a-1d05-4290-8de0-99363fc71553",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 102,
            "second": 74
        },
        {
            "id": "23e934c4-8734-4662-bcea-bf925b6aa3af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 83
        },
        {
            "id": "e62b7b7f-502f-43d9-a35d-59e520631428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 102,
            "second": 97
        },
        {
            "id": "b6002907-ce1e-4313-9356-1a281453c233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 102,
            "second": 106
        },
        {
            "id": "d04f291b-dc7c-4b37-a1aa-0adb3c3e7d61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 115
        },
        {
            "id": "efa8b7da-9e3a-4774-989c-f401fa35beb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 48
        },
        {
            "id": "c7a97773-f17c-4dd7-8c6f-5d9f3d6cdefd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 53
        },
        {
            "id": "75b586bc-312a-47e6-afeb-66ab0c0cf97d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 54
        },
        {
            "id": "fbc0beb0-1c70-4344-9f8c-e087fa29c703",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 56
        },
        {
            "id": "2d78e588-b21d-4129-882f-17f0661635cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 57
        },
        {
            "id": "c07c462d-8c7b-407e-b438-7c743d9d693e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 66
        },
        {
            "id": "033a5077-a178-4382-a8d8-5c1f10b7d604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 67
        },
        {
            "id": "c7b4d37c-c7a2-40ba-9a93-4425a862bda1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 68
        },
        {
            "id": "fc236c86-218e-46a9-b25a-c74d05cfc7cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 69
        },
        {
            "id": "fdbb2fcc-61de-49a0-9f0d-721af11fecd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 70
        },
        {
            "id": "c0ea5701-f98e-4d04-9542-369a29d8d954",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 71
        },
        {
            "id": "9efd4b9d-8e62-4e19-978d-63fb2b9dc9f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 72
        },
        {
            "id": "f5176a9a-02b9-4b8f-8423-9ae341bf87d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 73
        },
        {
            "id": "32dd9276-5546-42a2-9b4b-56ee31e2940f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 74
        },
        {
            "id": "391a985b-cc52-4d9f-a810-16b5562269ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 75
        },
        {
            "id": "32dbdf2e-b6d3-4d2f-93bd-815d0b99d6ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 76
        },
        {
            "id": "53be485b-c59f-4a30-9a3c-5d64aec7c603",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 77
        },
        {
            "id": "100f6ef4-9ec8-4a91-82ec-144e1ad6c666",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 78
        },
        {
            "id": "e476d705-77e0-450b-96f9-1c7f07defb39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 79
        },
        {
            "id": "a2495986-8882-41c9-9041-3d0c093c4d44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 80
        },
        {
            "id": "20dc6317-0b44-4e06-8f9e-f6b1740ce7c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 81
        },
        {
            "id": "9d43102e-6482-44c1-ae9a-eb3905dd7934",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 82
        },
        {
            "id": "211e0375-a829-45c2-bbc8-a7fbb8819afa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 83
        },
        {
            "id": "3e4a9339-b5e2-4725-a38c-0e306790d966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 85
        },
        {
            "id": "503277ec-e9fb-41d8-9d17-837626119e86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 103,
            "second": 88
        },
        {
            "id": "cf1459b0-bb5a-4637-8202-ac0964a81f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 103,
            "second": 89
        },
        {
            "id": "ec8f214d-cf1d-469d-b613-e39ecd95bdf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 98
        },
        {
            "id": "bda459a7-c4ac-45af-9110-320ce43f22fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 99
        },
        {
            "id": "982ed141-d373-4152-ac81-2fbca312c4c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 100
        },
        {
            "id": "35035dc6-4bbf-4b25-98d2-e0aceda00545",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 101
        },
        {
            "id": "4bc496e5-55f8-403b-a11e-15aa02687c7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 102
        },
        {
            "id": "01969815-91a2-4a04-bbef-d6dc6b5d9f96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 103
        },
        {
            "id": "1c2c5ff9-5517-47b2-80cb-279ea837c9b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 104
        },
        {
            "id": "98edc090-6257-4289-84cb-c8bd927b6f0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 105
        },
        {
            "id": "c38b6159-d587-46c4-9ce3-3c8a1d56398f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 106
        },
        {
            "id": "359d7772-59c0-4368-9d56-6ca8e894a9d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 107
        },
        {
            "id": "0b039056-6cb2-486c-bddd-124fd0be884f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 108
        },
        {
            "id": "65d6e9c2-1ef0-401f-b3d6-dfd721a5b75b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 109
        },
        {
            "id": "0d2d807f-a743-4c6d-8787-35a8e0639ed0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 110
        },
        {
            "id": "a369b17c-4108-4b10-ad01-634004746347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 111
        },
        {
            "id": "d81c904f-f26e-490e-b751-a6b841406f15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 112
        },
        {
            "id": "90db0f68-db3c-408e-a0e1-780f59110994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 113
        },
        {
            "id": "7ad0f5fa-311f-42e7-b19f-1dcd24c11e30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 103,
            "second": 114
        },
        {
            "id": "3a5f3be8-877a-4033-b28a-cdbf0ed3aea7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 115
        },
        {
            "id": "10ab6ac3-4029-4e31-85db-7d14243577f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 117
        },
        {
            "id": "1a84ba47-541b-421b-8000-72e697bb041f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 103,
            "second": 120
        },
        {
            "id": "896f8307-9d39-403a-92db-28841ab9df74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 103,
            "second": 121
        },
        {
            "id": "b1a43d53-656b-4058-9697-93e8bd239bb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 48
        },
        {
            "id": "0bd41d80-e7cd-4c09-a41c-ce89727f6e8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 49
        },
        {
            "id": "0485018d-897c-4845-9939-3e19ac47ceae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 50
        },
        {
            "id": "b230a444-93fe-42a3-8e44-e57943e316e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 51
        },
        {
            "id": "76c17a17-b7cc-4971-9ea8-c18030c707fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 52
        },
        {
            "id": "dec6b6ac-f634-4fee-8eed-94d27412bb8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 53
        },
        {
            "id": "c411a1bc-cd67-4257-9a2c-27cc2f31a926",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 54
        },
        {
            "id": "11722570-edbf-4dce-bb08-ea54f76ec356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 56
        },
        {
            "id": "64e892a4-18ff-4e88-a806-c8f7bda45253",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 57
        },
        {
            "id": "957edda1-ed46-4bfc-bb93-b0cedee9d873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 65
        },
        {
            "id": "2571772d-676c-4e3c-93c0-fde7ec7c306e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 66
        },
        {
            "id": "88f9e926-2804-4208-93aa-61e2408145fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 67
        },
        {
            "id": "ceccf426-0a19-48d7-9275-b6c64cb43084",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 68
        },
        {
            "id": "ac166f5c-081f-4e53-86d3-589af523af6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 69
        },
        {
            "id": "27663b3f-c9c2-4472-9a21-15e72d944135",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 70
        },
        {
            "id": "baff41eb-c43a-4729-85fb-96441ef53d4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 71
        },
        {
            "id": "2461d47f-ad74-458c-88b6-57f11a45cf51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 72
        },
        {
            "id": "35b15fe6-4ab0-42b4-9d4d-ece7977fee79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 73
        },
        {
            "id": "29a68e45-7bcc-4946-a5b8-04d7650c5eb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 75
        },
        {
            "id": "68722f5a-c567-4736-8fb0-d92b71553d7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 76
        },
        {
            "id": "fa5209b9-e429-46a6-bc94-008ad0434cea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 77
        },
        {
            "id": "fb558eb9-674a-4341-94f6-2f48ebb6f073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 78
        },
        {
            "id": "2e9c910b-4226-4e68-8b91-25a9b18f9e69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 79
        },
        {
            "id": "d62be3d6-a8cf-47ac-96bb-37563b8fb2bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 80
        },
        {
            "id": "632b62ca-b196-4ad6-ae53-1aa1297d301e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 81
        },
        {
            "id": "5345e75b-7e80-40ed-9103-79e5d34eae0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 82
        },
        {
            "id": "08200aab-6eea-4bbd-b304-c6fd9da139c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 83
        },
        {
            "id": "7fa7cb57-e0c1-4a5e-9a72-39609870b4bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 84
        },
        {
            "id": "e9bd8b18-e3b0-41e7-b1fe-cc645ca7292e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 85
        },
        {
            "id": "befde231-8fd9-4c52-a68e-bb8809a070f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 86
        },
        {
            "id": "599c71c2-9e46-478e-a3ff-d3a3d846d214",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 87
        },
        {
            "id": "6487def7-1ac7-4aa6-ba46-466f7b6ce8f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 88
        },
        {
            "id": "f9469222-44dd-48b5-b531-1cb09e3a404d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 89
        },
        {
            "id": "356e07ff-5474-4cc9-939a-05c027c3d693",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 90
        },
        {
            "id": "6156b743-e83f-47ce-8394-c5996b6bb36c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 97
        },
        {
            "id": "d53c1b33-2832-4f0c-9615-f72fa568e1a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 98
        },
        {
            "id": "d366d046-7f77-4851-b4e3-4920c3a8b2c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 99
        },
        {
            "id": "bf24cfbf-d6e2-4981-acc1-e44f0bd8e800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 100
        },
        {
            "id": "34494c07-b959-44f5-8df1-cdd4ea6d29aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 101
        },
        {
            "id": "d3b064a5-8441-4f63-ae13-2f2ce5691da9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 102
        },
        {
            "id": "ce6ea90b-264d-48fe-ba32-9e571481e6ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 103
        },
        {
            "id": "b9498e63-6574-48cf-a5ca-1316cdd5ecc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 104
        },
        {
            "id": "2640f82d-21bd-4515-9bf9-2f3410869b1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 105
        },
        {
            "id": "ea108026-c7f6-4881-a718-340e9dd7ab7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 107
        },
        {
            "id": "0d5b0400-fc43-4687-b067-c689b29efb28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 108
        },
        {
            "id": "824a3c3d-3816-49ef-baba-50834402db3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 109
        },
        {
            "id": "7dcac95d-a3f3-40a4-ac7c-e782b2b5fc49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 110
        },
        {
            "id": "fa132ed4-8e9f-4120-8620-55850c65e25b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 111
        },
        {
            "id": "a6e65d9e-9213-4dc5-b07d-c46af28f6b4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 112
        },
        {
            "id": "16f5fa3e-5cd8-4d6a-abbd-871c23dba8d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 113
        },
        {
            "id": "6be87e50-57b9-4c78-aa94-d7324e81b552",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 114
        },
        {
            "id": "25478115-7c21-4479-b749-c7716d5f42bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 115
        },
        {
            "id": "061e0a35-97a2-45f9-b924-a03654051c82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 116
        },
        {
            "id": "322ed43d-b18b-4a4c-b38b-ca279b56f64e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 104,
            "second": 117
        },
        {
            "id": "571930de-aa08-4ba9-95e4-cef63989261e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 118
        },
        {
            "id": "f55584dc-7662-4141-9ff2-1b5361a02c74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 119
        },
        {
            "id": "5d634bb1-c2ce-44e4-8eaa-e09e18a180f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 120
        },
        {
            "id": "581645e3-28cf-4ee9-88be-14becb2fc1fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 121
        },
        {
            "id": "1878f794-dde0-4963-bd20-3083f17bbf6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 104,
            "second": 122
        },
        {
            "id": "b929f56c-7e43-41c5-80f2-d1dcf12cab09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 48
        },
        {
            "id": "86a542b5-a171-4198-8668-e6eda5fbfbe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 49
        },
        {
            "id": "4cc48b9c-8e80-4bbd-aea5-964d39e87e62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 50
        },
        {
            "id": "b440c4a2-8202-4027-97ab-e52e079c7fdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 51
        },
        {
            "id": "38644ec9-0c4c-4105-894b-7ec84eb28b02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 52
        },
        {
            "id": "2f385802-f34b-465f-9573-3f642089c240",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 53
        },
        {
            "id": "9eb2e5c9-3839-43c4-a90a-57791582575e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 54
        },
        {
            "id": "a72b9166-f2ca-4108-9706-c94743e61a03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 56
        },
        {
            "id": "09f332e6-2b9f-4900-9f16-869bacd91713",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 57
        },
        {
            "id": "ca6d4986-4cf1-462c-b6e8-27b01ad82c8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 65
        },
        {
            "id": "4eae18d9-6a15-41e6-8a88-1eb886ca17ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 66
        },
        {
            "id": "a299bb7c-876c-44ca-b8aa-d44d516bf742",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 67
        },
        {
            "id": "f2d69f4e-e9cd-4187-9a28-910a9771d8d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 68
        },
        {
            "id": "38d8d89f-4481-4d23-9c0d-a2261105486a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 69
        },
        {
            "id": "2299ffb4-d0e5-4c28-8066-93fb1418a3d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 70
        },
        {
            "id": "9dd7d40a-00d0-468b-92ff-eb709d5eb5a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 71
        },
        {
            "id": "5aa8b5b9-f7d7-4a23-bf8f-5e0a22d58507",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 72
        },
        {
            "id": "2b513622-0286-4cc5-9763-4f8141e7afbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 73
        },
        {
            "id": "60b5a78c-c475-43ec-b554-a7b307633e46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 75
        },
        {
            "id": "17d89fdc-4e30-4cdc-8a3e-8e293f645dbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 76
        },
        {
            "id": "013350c9-8bab-4ccd-9fc5-5c1c16bb1ca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 77
        },
        {
            "id": "81ab4bbf-5448-47a3-8394-dc678fb664f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 78
        },
        {
            "id": "c053cdbf-ddf4-4cc3-9167-64f96fc22ac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 79
        },
        {
            "id": "d71836a4-26f5-43ad-87da-1c6d23db662f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 80
        },
        {
            "id": "484577f6-e108-4a4a-bb9b-12d720ae7e02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 81
        },
        {
            "id": "f7533908-6a89-4640-810b-be308bfeea23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 82
        },
        {
            "id": "b9ab86dc-931a-4342-bde3-5e49a3cbff39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 83
        },
        {
            "id": "1e73ad76-9e87-4b65-af69-4eada5ad2004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 84
        },
        {
            "id": "74dc1edc-932b-41c4-b760-43ab87ff37a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 85
        },
        {
            "id": "923e905b-bd90-4742-844d-1e6884f140d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 86
        },
        {
            "id": "5c88b83b-41cf-4055-99b7-0b1a765ccda3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 87
        },
        {
            "id": "936af48e-750c-4cd4-84cd-a666a82d251e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 88
        },
        {
            "id": "4e38becb-89c0-4447-9111-cc1b86f2f52e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 89
        },
        {
            "id": "7b45739d-adf0-40fd-93a4-87593e2ec3af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 90
        },
        {
            "id": "1e1bb010-a66b-4b0c-b27b-da49ffcd7d01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 97
        },
        {
            "id": "ac20402e-44f1-4050-8357-b23e9cb27111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 98
        },
        {
            "id": "fb815a84-6095-43d8-8be4-d1942a46f641",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 99
        },
        {
            "id": "12ace87f-8ed4-4c74-9be9-49bfddd8cd51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 100
        },
        {
            "id": "2d1db1c3-26d5-40a5-ae8c-30d8236f2cd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 101
        },
        {
            "id": "66f650fc-e638-44cd-ae6f-ced1921bc5b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 102
        },
        {
            "id": "ce83227b-fbff-466a-a316-bcf775f96d22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 103
        },
        {
            "id": "bbee4512-a7ba-4768-8af5-2ffeb48ffbd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 104
        },
        {
            "id": "ac6ee312-f50f-465d-9526-1badbdfe83de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 105
        },
        {
            "id": "b68dc074-e877-4cbd-af43-77e08644357f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 107
        },
        {
            "id": "e439b1ca-faf9-4f17-b7d6-baeb4d9b6574",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 108
        },
        {
            "id": "6ecfcef5-07ad-48b9-b8ea-90a0b5ca5967",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 109
        },
        {
            "id": "34daa08b-ba2a-486d-8c54-823cd7e4de85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 110
        },
        {
            "id": "b43cb50c-0ffd-46b6-9d0e-f85a50ed12ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 111
        },
        {
            "id": "7370aabf-e21a-45e4-b238-769ff9f29b23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 112
        },
        {
            "id": "32582aab-ddc0-4154-a298-b49ea6c57a72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 113
        },
        {
            "id": "35cad8c7-63ed-4aa8-9181-e63159c1f01f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 114
        },
        {
            "id": "ecb3dfb9-980b-4b8d-9239-64ca33b28e4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 115
        },
        {
            "id": "0b9de839-a471-48b1-9058-3637dd34d518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 116
        },
        {
            "id": "9b4beceb-b0cf-45f4-ad05-d5844f6944f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 105,
            "second": 117
        },
        {
            "id": "dab5647c-4f4e-4939-a524-a3b6cda2ac11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 118
        },
        {
            "id": "4a213705-952a-4a6b-88ba-6273d1dbfb14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 119
        },
        {
            "id": "6bef68cc-ea9d-45a5-9312-2bffc6292406",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 120
        },
        {
            "id": "1d9a0255-b50b-4b3a-822c-8c42feeef4ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 121
        },
        {
            "id": "8c0a1c1c-f006-4235-97f0-4d972f28ca26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 105,
            "second": 122
        },
        {
            "id": "4e99e99c-8508-4dc4-947a-76f51be56816",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 48
        },
        {
            "id": "dcf87fc4-7869-44f8-b492-a60440275e36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 49
        },
        {
            "id": "6a0eef5f-febf-44ba-8386-1ec1fa3fb619",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 50
        },
        {
            "id": "9e1f4206-dd1c-4880-a0e2-849051f67081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 52
        },
        {
            "id": "2ff6fa2b-2014-4b17-b701-0ae7a8830087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 53
        },
        {
            "id": "6d6986f5-0754-401e-aa6e-9adb568a870b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 54
        },
        {
            "id": "d89e4f0e-fd5f-430e-9a42-5a9d36b2f81b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 56
        },
        {
            "id": "71425430-dddb-4304-bf89-4378a10c5d10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 57
        },
        {
            "id": "e3e90d2b-a916-467b-b217-a36e3d20c9c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 66
        },
        {
            "id": "8a7ed8ec-3475-4813-8ce5-9246350d0736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 67
        },
        {
            "id": "51431a88-e952-429a-96a5-44321a000b09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 68
        },
        {
            "id": "1e92b5d7-61ef-4a92-9cdc-478072b86fea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 69
        },
        {
            "id": "e670e879-926e-450a-babe-5befb6c7152e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 70
        },
        {
            "id": "698705c8-91cb-4397-ba58-d415fc85b9f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 71
        },
        {
            "id": "56f065a9-18ff-4d3c-ae98-b6678a0d5f98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 72
        },
        {
            "id": "a53853b4-a6ed-4d6f-bfdd-06a9cf33e13d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 73
        },
        {
            "id": "68487a9d-0b1e-41a1-a89a-b98cc5ab81dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 75
        },
        {
            "id": "60afcc42-ac52-4d0f-9c39-b59a055b7259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 76
        },
        {
            "id": "978a6f1a-f3f0-49e8-a17f-f6dffed1cc82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 77
        },
        {
            "id": "76207844-6f05-4af6-8d3e-8f83bdb03acd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 78
        },
        {
            "id": "0df82d2a-5b80-4749-a77a-22a1c4b98933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 79
        },
        {
            "id": "7386803d-c28c-4f1e-95c0-16a4516ee3e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 80
        },
        {
            "id": "32678d52-442d-46eb-a2ca-7c921bde7cac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 81
        },
        {
            "id": "b789731a-0165-482e-ad4a-061c508a39b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 82
        },
        {
            "id": "daa97792-0d93-41f4-8486-49bca337f185",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 83
        },
        {
            "id": "e0761795-34db-4444-ae6e-8744487343b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 84
        },
        {
            "id": "d0c35178-7de8-4dd5-8b71-7aeceb0bdf64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 85
        },
        {
            "id": "69d36606-eb65-46e1-9198-489f8b1bedd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 86
        },
        {
            "id": "bc2879ad-433a-40a1-8d97-35f0dc657251",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 87
        },
        {
            "id": "a5e73a14-d117-4a20-8972-f06308036e86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 88
        },
        {
            "id": "a87b4707-a7d3-468f-a9dd-8f567d7860dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 89
        },
        {
            "id": "27f3baae-0b88-4e24-9892-98a0d92b7d0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 90
        },
        {
            "id": "ee55d00e-1fb1-4ca0-a279-b1e473d2915d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 98
        },
        {
            "id": "8ed9a85f-f99b-401b-aee8-1b0b84ae804a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 99
        },
        {
            "id": "ee0af76f-0eaa-4bae-9693-0fb11b67e05e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 100
        },
        {
            "id": "5b1ef896-1baa-4361-88ad-f19b8c214c5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 101
        },
        {
            "id": "f55bc631-99ce-4293-851c-9f082b3b6b22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 102
        },
        {
            "id": "6eac5f1c-7645-488c-a27d-3d3b182ce71e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 103
        },
        {
            "id": "9eb2ede9-7c6e-4715-a46b-fb1411fe67db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 104
        },
        {
            "id": "fa7dfffb-e0b0-431d-a9ec-43cf5226d07a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 105
        },
        {
            "id": "bce05403-0238-4ad2-8005-c582872dcd87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 107
        },
        {
            "id": "f254343a-1ed4-4202-92e3-0ddab6bdb86e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 108
        },
        {
            "id": "3fb1755f-8632-4c82-8505-684f1f93aabf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 109
        },
        {
            "id": "93ea96a5-4f62-4251-88fb-fd65c019276d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 110
        },
        {
            "id": "99bd51ee-605e-4da8-9b65-2ea58f8d9ae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 111
        },
        {
            "id": "03cb099d-4633-4243-afd5-025dec3dcca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 112
        },
        {
            "id": "7be66070-0841-4705-a544-71fcaf4778f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 113
        },
        {
            "id": "eed6cbde-a001-40ae-8104-0b8c7239504f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 114
        },
        {
            "id": "d10b87e5-7c99-41f3-9d21-7f988c9fb89f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 115
        },
        {
            "id": "cd279566-07e1-42b9-8998-af2ee47b88e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 116
        },
        {
            "id": "de2b15fe-0a06-42ef-b751-01e5ae9baf04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 106,
            "second": 117
        },
        {
            "id": "4c5f7039-2681-4b3c-8d3a-6f1a6c08bb30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 118
        },
        {
            "id": "23beb333-4f41-46ba-af57-a934f365d321",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 119
        },
        {
            "id": "ec34984f-b4ee-46e7-ac32-76b7b12e280f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 120
        },
        {
            "id": "4736a9d7-9088-4d3b-a492-988c40057f84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 121
        },
        {
            "id": "824fdf96-bff7-4cc4-84db-b11624b32360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 106,
            "second": 122
        },
        {
            "id": "19edcc89-0c1f-4d38-9aa1-91714db8e64d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 48
        },
        {
            "id": "4eb04587-8f0c-4c8b-ba2f-c8f9552f69e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 107,
            "second": 49
        },
        {
            "id": "67988263-874e-4468-a3fd-47118e32a1b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 51
        },
        {
            "id": "81302b17-84c1-42c8-8c18-f503cad7a5cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 107,
            "second": 52
        },
        {
            "id": "062c4eb4-ff3b-4e01-b685-5c5ce1193c46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 53
        },
        {
            "id": "8e175a3b-9e90-40a7-a389-5ad68527bd00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 54
        },
        {
            "id": "c775c626-2a9b-4366-aa82-178a25f5a8bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 55
        },
        {
            "id": "b6858bfc-c475-46e1-8fc6-5081d867e61a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 56
        },
        {
            "id": "af4caf9b-a62d-47d9-a97a-22385f343106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 57
        },
        {
            "id": "dd6bb775-40ef-4065-aabe-ee0199493676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 66
        },
        {
            "id": "13ede5ed-0e20-4650-b822-9202a05651e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 67
        },
        {
            "id": "558a03ac-a300-45c3-8fd2-c9d5d666fd6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 68
        },
        {
            "id": "948ebfd9-80f8-4cdb-8ed5-19a447a6dc62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 69
        },
        {
            "id": "3177ba83-308f-4719-9fc1-621d0d7552c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 70
        },
        {
            "id": "4c9f173e-a56f-412f-95ba-3aa0958f8e8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 71
        },
        {
            "id": "21bdfbeb-0142-4e44-9a2f-06128b289d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 72
        },
        {
            "id": "508d5235-02fc-4beb-9040-6610c67e7864",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 73
        },
        {
            "id": "fe27e023-937e-4d52-bbda-531a09d86108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 75
        },
        {
            "id": "438b6bdb-82f8-441a-bdbf-9bebb186e50b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 76
        },
        {
            "id": "7ec277cf-3b07-484f-a3e5-1d9dbcc029dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 77
        },
        {
            "id": "18fcb21f-0c3c-461c-8eb6-af5327e03ee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 78
        },
        {
            "id": "424ee19a-38ce-478c-9145-ebe49143a67d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 79
        },
        {
            "id": "5a065af2-2f8e-4a01-b3c8-d0ae28ed0b29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 80
        },
        {
            "id": "693b6f50-8c29-4fd2-8027-9d32bb923421",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 81
        },
        {
            "id": "bccb636e-750c-429a-bf48-73606b72b272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 82
        },
        {
            "id": "2e84c836-352e-4c27-a976-4c1d77df4a88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 83
        },
        {
            "id": "5ea61ca9-5099-4a97-a388-8707eb064bf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 84
        },
        {
            "id": "72c5efe2-51a9-4b54-9d3d-156c2e379818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 86
        },
        {
            "id": "987e4318-f8ec-4829-b2be-2c0886c9a341",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 87
        },
        {
            "id": "a515c6cd-9173-4995-9a96-b9ce7a3e1f70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 89
        },
        {
            "id": "dc0cae66-6ca0-4961-83df-d9d8c78da419",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 98
        },
        {
            "id": "4cd33fa3-cc57-48cc-a388-dfd0fec53fa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 99
        },
        {
            "id": "3f36dfce-f921-4c25-8802-d0d031e09736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 100
        },
        {
            "id": "fe357a6e-4012-4d66-bed9-6d7bbf99045a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 101
        },
        {
            "id": "b3505af7-7e8d-4976-93b5-94d8ec3f0d00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 102
        },
        {
            "id": "6b613621-6408-4f4b-af69-2dc6642d20dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 103
        },
        {
            "id": "5a1d4cbc-1341-4424-95fa-ce5c467e7c25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 104
        },
        {
            "id": "35905bb9-5f05-42f1-b8c4-90c4a82dbce4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 105
        },
        {
            "id": "5fc0eab7-42c4-4f0a-a420-b093e2e98e87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 107
        },
        {
            "id": "54b073ab-f9f4-4467-8670-2700e79bfceb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 108
        },
        {
            "id": "5f418cd7-e92b-4aef-84d6-fe7558985546",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 109
        },
        {
            "id": "d94e5fb3-f083-4ec4-bb41-7e373baf0a89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 110
        },
        {
            "id": "b718901d-c26e-44b9-8376-9f7876e325bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 111
        },
        {
            "id": "04dc29b9-0c59-46df-bacd-ecf4cb29dd35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 112
        },
        {
            "id": "b0f31456-91e0-4095-becf-c65cdd4e4dc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 113
        },
        {
            "id": "efb8566c-174f-4f78-9903-840331f3f08f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 114
        },
        {
            "id": "6e1e0442-55db-4fa4-917d-b9dd1d91f95c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 107,
            "second": 115
        },
        {
            "id": "aa592cf4-48fd-494f-a4d5-ff58fa2eb923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 116
        },
        {
            "id": "024f0538-2df9-4756-a8b6-3039c99d0308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 118
        },
        {
            "id": "6e2317c2-f3b2-4d43-a1e9-f7388a7fc66e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 119
        },
        {
            "id": "83e6484b-038d-4258-a020-82b219f11d0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 121
        },
        {
            "id": "5245b95f-4c53-498b-a146-d2f15dcf554f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 49
        },
        {
            "id": "da1ce04c-7649-4683-8988-45b34a6058c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -14,
            "first": 108,
            "second": 52
        },
        {
            "id": "efd770f3-1f74-473b-b1dc-b5cbd64507ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 108,
            "second": 55
        },
        {
            "id": "16f0100a-9a29-4041-af5d-1cb005132bb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 83
        },
        {
            "id": "21b01483-16ef-4213-b599-4b6d42590eba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 108,
            "second": 84
        },
        {
            "id": "89132ccb-512c-408e-a777-9bdd31321148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 108,
            "second": 86
        },
        {
            "id": "304f7184-352b-4ced-b020-8f24a600d1e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 87
        },
        {
            "id": "5ad80694-f7d6-44c2-ae9f-f254fe51a192",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 108,
            "second": 89
        },
        {
            "id": "9d362f94-9a0c-4869-a0fa-fa42f9d27128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 115
        },
        {
            "id": "7df5d046-a8a5-452c-b4a6-9092bdcfb682",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 108,
            "second": 116
        },
        {
            "id": "d1294e65-ae15-43d5-adcd-ecf3df9c3882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 108,
            "second": 118
        },
        {
            "id": "f603d691-c267-496d-9f2b-cbcb30030765",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 108,
            "second": 119
        },
        {
            "id": "45b9d799-957c-40ee-9367-e6af442a7ca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 108,
            "second": 121
        },
        {
            "id": "2c5bd2f1-2ee9-4170-90c1-c0977b7c274f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 48
        },
        {
            "id": "8fe57350-ebd0-4a21-ab08-0aee9a8a3749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 49
        },
        {
            "id": "4fc75e79-8ea5-453b-b529-f8b46ebf76f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 50
        },
        {
            "id": "b76cdda1-74c6-4bdd-9ed0-7a44e6f90348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 51
        },
        {
            "id": "1f7a81f6-ea09-4b35-9450-2dedc4eee652",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 52
        },
        {
            "id": "5247823f-3411-4486-ba80-c6a2fcad207d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 53
        },
        {
            "id": "bcaebd4d-88be-4f0a-ac06-ffcf2489b4c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 54
        },
        {
            "id": "3b141f16-08ac-4139-8045-da5d38089b5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 56
        },
        {
            "id": "bdd0086e-ebec-4d3d-92da-e188cc442f84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 57
        },
        {
            "id": "38361e8e-b818-4593-8931-4f64cd2c7383",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 65
        },
        {
            "id": "662c5c19-70cc-4d9a-9ae1-8c7cee4a3e80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 66
        },
        {
            "id": "298b469d-3c49-45dd-8d6e-4d89cba5800e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 67
        },
        {
            "id": "7ee1d2b2-fd96-4af6-8983-bc77e624f7da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 68
        },
        {
            "id": "bd9abf53-8eb3-41f7-bd15-2d224bb9f334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 69
        },
        {
            "id": "5c46f390-cfcc-4672-9e12-8d0dd8201f13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 70
        },
        {
            "id": "c79efe47-5720-441f-a488-c25fc367867d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 71
        },
        {
            "id": "f9277c63-75d5-4791-9c43-54a41fac5e0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 72
        },
        {
            "id": "ecdee498-eac9-4a41-83bc-dc6146afed65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 73
        },
        {
            "id": "44f518a4-89cc-47cf-a4f6-4f1cb4b3d724",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 75
        },
        {
            "id": "f013c3f1-31b9-4ce1-837b-9078c024ff27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 76
        },
        {
            "id": "7f0701ac-7db4-4de7-bc29-9e6496f2ea19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 77
        },
        {
            "id": "97cd0d1c-fe12-4ad0-b5d1-ca13364f92f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 78
        },
        {
            "id": "0ecaf36a-9d27-4dfd-83a7-577cabf940d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 79
        },
        {
            "id": "4b620714-58db-4385-b132-675f5c11d410",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 80
        },
        {
            "id": "d7fa4eba-7603-41b4-bd5f-4d9753879825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 81
        },
        {
            "id": "40197fcf-3a89-4dfd-b3bf-b6d2f4195dc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 82
        },
        {
            "id": "1f062e94-3f1a-4c1f-a139-34495869ca9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 83
        },
        {
            "id": "cb1548e4-1387-4d72-a2d0-7d319fd82b19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 84
        },
        {
            "id": "75baecc5-3f8e-466a-acae-52cfdce700df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 85
        },
        {
            "id": "cac19fa1-6cc8-4d87-9ab7-0b79c0b3f995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 86
        },
        {
            "id": "8dd91117-eeb8-439d-81c0-a575d2ac9d75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 87
        },
        {
            "id": "2efa8011-ad7b-4d65-88ad-8ecd2aadc328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 88
        },
        {
            "id": "82682ad7-a2b1-418e-8263-29c1ebfdda82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 89
        },
        {
            "id": "a35e4879-01d7-4184-ab55-d9b658253b2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 90
        },
        {
            "id": "05166197-0b1a-40cb-b420-48d699ff9d0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 97
        },
        {
            "id": "978260c0-d7e4-4b07-bf7e-d048b4f13785",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 98
        },
        {
            "id": "5360e684-54b9-41a6-9779-f0792661ba6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 99
        },
        {
            "id": "d5b79743-1f22-4ade-8cdc-a82de666000c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 100
        },
        {
            "id": "4b7b155b-e0b4-4633-9f20-76b3725466fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 101
        },
        {
            "id": "3f9b3d09-cb1f-4dcc-a096-bcd1df77b281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 102
        },
        {
            "id": "92f0bc70-4749-49c5-b568-181e2852ea00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 103
        },
        {
            "id": "89ec52ce-d2d9-452b-babb-5b0e5e93d0f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 104
        },
        {
            "id": "cb888e1b-4495-4b77-8b50-3aaeab090cc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 105
        },
        {
            "id": "e64dd14a-0752-43c8-9d50-b966b9b5851b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 107
        },
        {
            "id": "9a2c7534-9972-4b48-bfc2-aa35f5731e1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 108
        },
        {
            "id": "580c6812-4eb3-4588-ac21-26ad145683ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 109
        },
        {
            "id": "feb82dea-df19-4bd9-bdd5-b70caf0861e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 110
        },
        {
            "id": "b92bdfeb-b6b1-42eb-addf-8cbc87ed4e7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 111
        },
        {
            "id": "4f3e0445-647d-4f38-8a31-3e3c99d058a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 112
        },
        {
            "id": "ddefc7a9-7487-4c8c-ab4b-af449883f05a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 113
        },
        {
            "id": "9ad2cebb-1db1-43a1-a91a-182d9a47ca42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 114
        },
        {
            "id": "184395f0-93b2-4ad5-a142-04483458855b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 115
        },
        {
            "id": "931574f6-8bf7-4cc6-bc27-cb39f4de3d36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 116
        },
        {
            "id": "2e54d18e-4060-47a2-b0c7-9e67f4c66639",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 109,
            "second": 117
        },
        {
            "id": "acd6f401-19c2-4dfc-b164-6f278326ed0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 118
        },
        {
            "id": "c9e6a6a5-4e68-4115-924d-578ae701f600",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 119
        },
        {
            "id": "1ceec135-ebc1-4b23-bed0-efd617559372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 120
        },
        {
            "id": "f151f06f-581d-45b8-8a9a-f43ea5368c70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 121
        },
        {
            "id": "cbf1b3a1-4907-4052-aff5-14a3b5a3a39c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 109,
            "second": 122
        },
        {
            "id": "fcc6cacb-7186-414c-9b88-470fca327cd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 48
        },
        {
            "id": "f6cf866f-2785-446d-8b60-6536695d3376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 49
        },
        {
            "id": "d42a2932-2cbc-4455-8bf0-48ea64244c7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 50
        },
        {
            "id": "62ebbffa-0b97-44d5-96e4-3ceac211b25e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 51
        },
        {
            "id": "379445fc-5874-4254-8c34-a27c20973a82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 52
        },
        {
            "id": "00694166-ba1d-4431-a4b0-ac97db7d2010",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 53
        },
        {
            "id": "ee539dac-46f8-4726-9c21-e70deab64cdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 54
        },
        {
            "id": "88912452-c29a-4541-b7e2-7be303287c08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 56
        },
        {
            "id": "333f2527-4161-45a3-83f5-bbd4ab041d0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 57
        },
        {
            "id": "af266527-b5be-482c-b5d6-1b354ebd17c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 65
        },
        {
            "id": "590f76f6-494d-4aa8-ac8c-a2a7318e95e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 66
        },
        {
            "id": "1bf62fa2-41a6-407b-994b-42a6831e2f52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 67
        },
        {
            "id": "390b6356-2353-4a83-8960-4ef213445237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 68
        },
        {
            "id": "857f29e2-e9f6-40e7-b748-542580eef7d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 69
        },
        {
            "id": "637b5b4f-fad9-4f67-9cd9-ca9a0d2e545e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 70
        },
        {
            "id": "083df798-6d89-4ec2-b0ec-6f2d616fe07f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 71
        },
        {
            "id": "4525e4f2-4a6a-4a2f-866c-384ff512492b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 72
        },
        {
            "id": "57050426-3a47-4ffb-b0b3-ee5c90b89c75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 73
        },
        {
            "id": "6a5cb560-d761-4e9f-9b08-d4297a9026d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 75
        },
        {
            "id": "6a27e2c9-23e6-4abf-9d6e-27b7990664dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 76
        },
        {
            "id": "3d118720-f95a-48bf-ad48-b0621030a9fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 77
        },
        {
            "id": "b329bad1-88dc-4683-b80e-baf14e153f6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 78
        },
        {
            "id": "a7149461-68a3-4c75-8fa9-c41db4559363",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 79
        },
        {
            "id": "f0e6071b-391e-4010-a754-43a31f486058",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 80
        },
        {
            "id": "808d8d36-4a2f-41b8-8fcf-f6184bf161d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 81
        },
        {
            "id": "8de1c789-6d2a-42c9-abd8-ea3f10108187",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 82
        },
        {
            "id": "7dc5314d-4926-4514-b778-3393f3d9a17b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 83
        },
        {
            "id": "255c0a57-7495-4b25-8987-4cc9e22ca716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 84
        },
        {
            "id": "ce487471-9e8a-40f2-8dd0-ba9d0432c915",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 85
        },
        {
            "id": "c009cc64-af9a-47c0-a712-a19def72b0c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 86
        },
        {
            "id": "ffeb8849-7e78-4cd2-b04e-9b5d45112591",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 87
        },
        {
            "id": "bbd5a446-42f9-4a54-82f1-793a2d10f11e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 88
        },
        {
            "id": "7964eb93-91ef-43e9-af76-cf5b1e265e52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 89
        },
        {
            "id": "14dccd44-4ba9-4d37-a4a4-cd9749dccb6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 90
        },
        {
            "id": "048b1f69-df5e-49ad-bed6-40af64f98a3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 97
        },
        {
            "id": "f342a1b5-5f6b-4543-b489-ddfe6882165d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 98
        },
        {
            "id": "31289e4d-dc5b-41c9-a77e-ffc4644e0d7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 99
        },
        {
            "id": "7ccfcdaa-5502-4948-821b-cd1a5f92d021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 100
        },
        {
            "id": "d5bfcdeb-eb74-443b-a11d-60d5266c9d59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 101
        },
        {
            "id": "5793d08f-0f0a-4691-abfd-5ce6872913f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 102
        },
        {
            "id": "c5ae6d01-8ec7-4cb9-9ae1-e7c776f79330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 103
        },
        {
            "id": "a92a5e6c-9504-4896-a49d-b0a88db26117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 104
        },
        {
            "id": "e615cc34-1d0c-4321-84f6-456fe37f75ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 105
        },
        {
            "id": "cfd5d45d-8eca-4fe5-b25c-a672c2247546",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 107
        },
        {
            "id": "b2c43074-1453-40a2-8cc0-ebb119a40f1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 108
        },
        {
            "id": "0ea3568a-92a9-4c1f-9c0f-0e9d3ed0b248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 109
        },
        {
            "id": "08b73709-5ce2-4c8d-adb0-7b86ac888906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 110
        },
        {
            "id": "9fdcbc3f-b6c3-44a0-84e4-8f20c481a78d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 111
        },
        {
            "id": "c8e2d724-ee8a-4cb9-bcab-3e393dbcdb87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 112
        },
        {
            "id": "7e33decc-1c45-4b56-a20b-76d68ef83beb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 113
        },
        {
            "id": "b5252ecf-9c7d-4794-842c-ebed4081bfc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 114
        },
        {
            "id": "b7634941-959a-47b9-a1e9-180e62ed5053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 115
        },
        {
            "id": "94457698-c1a3-4776-82a0-45cabd2c6c4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 116
        },
        {
            "id": "e35ad659-a251-4515-9bab-6abcf54b7cdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 110,
            "second": 117
        },
        {
            "id": "46fc0a6a-9950-4ce2-acc0-affe3fbc2286",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 118
        },
        {
            "id": "09acf6b6-230f-471e-bdee-4bce6b477a48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 119
        },
        {
            "id": "8853b1e5-2148-45f9-989c-6466c003182f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 120
        },
        {
            "id": "46374818-f462-4e05-8aa6-a46a908ee466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 121
        },
        {
            "id": "9975b925-c270-410b-a33f-c37d6ad6aed3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 110,
            "second": 122
        },
        {
            "id": "cae29dbe-a521-4db7-81a9-873ba99f7ba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 48
        },
        {
            "id": "048d3adc-c8f0-40be-9e1c-4f6ca40449b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 49
        },
        {
            "id": "834dba29-1132-4323-b99a-9191b9fbef92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 50
        },
        {
            "id": "ca1f7804-c854-4099-bd73-321434c34cb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 53
        },
        {
            "id": "52f8b066-cb96-48c1-b5ed-9c6a13ecde25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 54
        },
        {
            "id": "6e0f0ac7-7c88-4a2e-9086-56d1d9daa013",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 56
        },
        {
            "id": "f1524e10-0e88-48b5-aa81-db0a198faddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 57
        },
        {
            "id": "4afc88da-e16f-47ba-9eea-e54919b6f9cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 66
        },
        {
            "id": "278e5068-dfc5-4d5b-a426-26f8a414d21d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 67
        },
        {
            "id": "c886802a-62d3-4d6f-a2e3-5db5c2b68641",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 68
        },
        {
            "id": "b38bb9a1-5f33-4e15-a7cc-9bf0de5ed171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 69
        },
        {
            "id": "9bb0613a-8255-46f9-8147-5eb1f503a7c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 70
        },
        {
            "id": "3d3796a1-4789-4cb6-b884-eba67aa29c10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 71
        },
        {
            "id": "a8543bc3-42f3-4e99-859b-a804e7d5f8f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 72
        },
        {
            "id": "56d8fb12-3161-4d03-8363-60a821c62bd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 73
        },
        {
            "id": "556509e9-9124-48cf-bb81-03f7cf70b743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 75
        },
        {
            "id": "eee93766-31cf-4bec-af5f-dd7e073adfbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 76
        },
        {
            "id": "b844ce5b-5907-4c2a-96f0-0415eda201d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 77
        },
        {
            "id": "cf672323-5fb3-463c-b75b-128b67e86132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 78
        },
        {
            "id": "037670a7-67e7-48b2-a49f-bd8e236e8953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 79
        },
        {
            "id": "528d5667-3dd1-4fb8-b3ce-9c9abd01dc90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 80
        },
        {
            "id": "544ce903-f577-4cde-8987-26e5efa0e3cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 81
        },
        {
            "id": "5917e85b-b976-4937-87a5-45e6d3ebf3b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 82
        },
        {
            "id": "4a28e056-45a0-4c0d-91b1-f9aa8e86658c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 83
        },
        {
            "id": "2bb04f6d-fa56-4d46-ac04-f7869ab13de3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 85
        },
        {
            "id": "093b304d-e808-4723-8527-b29392307128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 88
        },
        {
            "id": "85acc055-1601-4541-a899-2a363883e2cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 89
        },
        {
            "id": "917418db-7072-48d1-9f3a-b9b34f7675c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 98
        },
        {
            "id": "21ede1ba-b549-42ec-9dcb-7bcb11978407",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 99
        },
        {
            "id": "f9e1ec64-c3db-4b24-a3da-2188bec00fe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 100
        },
        {
            "id": "daba97f5-6843-40b3-89df-7b435bf280cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 101
        },
        {
            "id": "0fbf7f4d-1893-4d85-b35d-159da88f08ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 102
        },
        {
            "id": "535a31d9-9aaf-41f4-8b15-49fd62732d7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 103
        },
        {
            "id": "9cc528b3-835f-438c-8fde-5ace39712988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 104
        },
        {
            "id": "491b001f-c8c1-476c-ac76-77176bd8c67b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 105
        },
        {
            "id": "3ea5b098-8b09-4d58-b84e-bd027c529983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 107
        },
        {
            "id": "b8f31ef7-fbb0-4d33-8a15-1307c25aba5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 108
        },
        {
            "id": "93f54783-624f-480d-86ca-f337d6699c75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 109
        },
        {
            "id": "e01fca0b-f2ec-4f90-ab90-d16e244f4620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 110
        },
        {
            "id": "84c906d6-3da3-4138-9d41-062e46509c68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 111
        },
        {
            "id": "18967bd8-767f-451d-b5bd-54e21368b963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 112
        },
        {
            "id": "4f1d2d1b-16f7-48b2-ae97-a59c52d5351f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 113
        },
        {
            "id": "d9c7fdfd-4483-4d21-9b77-3f7aef1cea50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 114
        },
        {
            "id": "4937f313-e521-4794-b6fd-4d1181a549cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 111,
            "second": 115
        },
        {
            "id": "1f01d70f-2b16-4164-97f8-b970489491cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 111,
            "second": 117
        },
        {
            "id": "c9947472-c662-48a6-b5ba-062c719ef1e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 120
        },
        {
            "id": "fc922859-cb82-4478-bd4b-fadeec318f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 121
        },
        {
            "id": "a08ba41c-09a6-4119-9aee-7892e009b76c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 51
        },
        {
            "id": "5d846dd0-ef9e-4b13-8816-056410ec4459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 112,
            "second": 52
        },
        {
            "id": "e0db9f52-1baf-46c5-9cb4-2cde372ebb18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 112,
            "second": 65
        },
        {
            "id": "61610c98-2407-4045-b027-5eb3a0e3f4ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 66
        },
        {
            "id": "5c93269e-f239-4036-86ac-ae2812211483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 68
        },
        {
            "id": "72f8d81b-1fdd-4c51-aa70-cb5887402d17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 69
        },
        {
            "id": "38b5212b-cbd6-4296-9bb4-ffafe6558858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 70
        },
        {
            "id": "8aa77765-408a-4006-8622-54a8517964e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 72
        },
        {
            "id": "4a570349-88b5-49d0-b02f-5061f50ddf7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 73
        },
        {
            "id": "e8fff8a8-9be0-41a4-ae56-07c3328bab77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 112,
            "second": 74
        },
        {
            "id": "f5066c28-d847-4a0e-a155-85f679804504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 75
        },
        {
            "id": "b5e0be29-ebaa-407f-ac9e-da4911ecbb52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 76
        },
        {
            "id": "aff4ef93-d1a1-4c05-9544-a5c070488666",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 77
        },
        {
            "id": "bb629ad0-71de-4ef9-91e9-3e6a559e8693",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 78
        },
        {
            "id": "a54b2f85-62eb-4f9a-8b7c-2ab3cd905134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 80
        },
        {
            "id": "fd556476-0fc8-44fb-b595-a245e462cd35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 82
        },
        {
            "id": "61e4cf66-b8ee-4aef-a2bb-3e2cff336781",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 85
        },
        {
            "id": "932d43eb-6529-4800-9d89-56bb7944d552",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 86
        },
        {
            "id": "26a9e62a-83a9-4d21-8d28-5e161555233a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 87
        },
        {
            "id": "ec2432d3-f925-4461-a30f-57dd78fe6afa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 88
        },
        {
            "id": "95359a28-e70c-4556-92be-f0dd1319d012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 89
        },
        {
            "id": "abf80a5c-f534-41f1-8075-e61d186e1035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 112,
            "second": 97
        },
        {
            "id": "4a25f63a-26e1-4812-9556-4035dcc1f8fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 98
        },
        {
            "id": "2d19e2b2-9dc5-4618-8015-c49b95af32ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 100
        },
        {
            "id": "fa9c1731-8880-4971-a6aa-aeef4b8c7ed3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 101
        },
        {
            "id": "afd9a5ab-cb25-4a24-95bc-57214c3b5e73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 102
        },
        {
            "id": "d3f47aff-4c26-407b-83e1-4985b9ec128a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 104
        },
        {
            "id": "af844b50-f38f-425b-a902-cf7efa2c238a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 105
        },
        {
            "id": "fb989bfa-b2ad-44c7-a074-05a330f86b27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 112,
            "second": 106
        },
        {
            "id": "96ae3a92-db60-4a6d-8218-64b6c976622c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 107
        },
        {
            "id": "851bf6b4-7261-40ee-b320-0693eea6fb08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 108
        },
        {
            "id": "e709a7ab-fdc4-4352-8161-a9a5176d7e2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 109
        },
        {
            "id": "46677aff-7f0b-4273-9583-0b2b62369b85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 110
        },
        {
            "id": "c7e4ca6f-f43a-4474-b4b8-bb739e9f1f14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 112
        },
        {
            "id": "3b0f9012-dec9-4821-a1b5-3957caa0afa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 114
        },
        {
            "id": "11febfea-2517-4446-9bd3-89374cb9f5fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 112,
            "second": 117
        },
        {
            "id": "a9ab07a1-c6b6-4a91-842d-04e2158437c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 118
        },
        {
            "id": "dd6b4967-5aba-484c-b071-14a4246521ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 119
        },
        {
            "id": "44173992-0a14-4583-8a0e-49ee6cb20d77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 120
        },
        {
            "id": "2bb545db-f2ab-4cc6-a88b-ab10103c9653",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 121
        },
        {
            "id": "0a999e09-3c5d-4d6a-b394-ae196d8bcc74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 50
        },
        {
            "id": "9bf8b31f-af1a-41c2-ae6b-5f4b71f8c2f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 55
        },
        {
            "id": "a08dbf74-207f-4a18-8237-7b3145afde8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 65
        },
        {
            "id": "ccf8c387-2287-4256-99e0-428642dd2a08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 66
        },
        {
            "id": "5f1ad702-0f9b-44b8-8f95-61ca6d35ab1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 68
        },
        {
            "id": "6988b5c3-eda9-41e3-a5f0-93ec67b1f21d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 69
        },
        {
            "id": "033bdaf6-a704-424a-b1f9-cc9e738c6bbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 70
        },
        {
            "id": "81988ad0-5992-4796-a39a-da3b24e5f5de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 72
        },
        {
            "id": "9963af47-6e49-4e27-9263-77644c62c884",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 73
        },
        {
            "id": "45a13a2b-35e1-4cc0-a2ae-0779e04e7232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 75
        },
        {
            "id": "62a3aa6a-e66f-4b91-9c46-2799f27618a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 76
        },
        {
            "id": "c661778c-0944-4d59-a059-ef02da8c3457",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 77
        },
        {
            "id": "519efa0f-ec85-4aa4-b738-da40ac158eb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 78
        },
        {
            "id": "2a7cb904-3ad7-4c51-b1ff-c51dc9446db8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 80
        },
        {
            "id": "dca200c8-dbba-4ad8-a5be-c9aa3edcee98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 82
        },
        {
            "id": "bf82ed3c-1259-4b16-9171-9aac769936bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 84
        },
        {
            "id": "aadbf8c9-1db5-427c-b4a1-45ed7eb399bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 113,
            "second": 86
        },
        {
            "id": "39fd5dc1-4266-462f-bd8c-d5b2b7b8501e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 113,
            "second": 87
        },
        {
            "id": "36fc2166-1eea-43d6-9a4e-2cb16412dff9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 88
        },
        {
            "id": "d366a31d-cc28-4afd-918d-23dfb51d298c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 113,
            "second": 89
        },
        {
            "id": "e86cbdcd-cf4e-4205-9f88-ed906ddd3717",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 90
        },
        {
            "id": "0ce04f2c-6f41-4175-ad05-9af6b12cc312",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 97
        },
        {
            "id": "e4409998-5bfc-4b49-9a15-32f0e54f387d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 98
        },
        {
            "id": "709d411c-5b33-4cdf-b1d6-229ccd1fdbee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 100
        },
        {
            "id": "c8b217f4-82cf-4fd7-b432-ce37d4ca9e8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 101
        },
        {
            "id": "37599c23-4e35-4bb0-bc36-58e1d04487c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 102
        },
        {
            "id": "4426ed6d-1b03-443e-aede-ba024a8ce4a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 104
        },
        {
            "id": "bd74fb05-98f5-420c-a90a-06a639609344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 105
        },
        {
            "id": "514f6653-47eb-41aa-a745-48c7e05de0fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 107
        },
        {
            "id": "16d974e8-2baf-447d-bc00-6f73e9eff878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 108
        },
        {
            "id": "444e0c01-6f65-48d7-b825-7cc32e7bc183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 109
        },
        {
            "id": "71aa165f-f8ff-4e6c-ad68-1a1e9959c1b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 110
        },
        {
            "id": "5e26f650-7472-4166-8314-69a9a8565d6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 112
        },
        {
            "id": "422f62a8-987d-4db9-9e5b-5a66a7c5e82b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 113,
            "second": 114
        },
        {
            "id": "fab28896-1fc8-4ad7-a779-f76649aa5484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 116
        },
        {
            "id": "24808b1b-6572-4d8f-962e-b1f0a36ac62c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 113,
            "second": 118
        },
        {
            "id": "935fe648-49e6-476c-8603-1005bb183d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 113,
            "second": 119
        },
        {
            "id": "b8f90a9c-50fe-43da-ba7c-2460ab299dc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 120
        },
        {
            "id": "425b1ee9-5588-4fea-b9fc-589061561d15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 113,
            "second": 121
        },
        {
            "id": "55dc3366-4091-447e-b6b6-042be9028a55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 122
        },
        {
            "id": "8d8ac41c-6672-41a7-8745-67bd86aa8951",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 51
        },
        {
            "id": "c025cf02-4318-40a9-aeac-a18e40cb84a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 52
        },
        {
            "id": "e3f181e6-7a4f-447a-a91c-f6a950b4530a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 53
        },
        {
            "id": "3e9399e8-4870-4b01-adc9-70a32563d6e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 55
        },
        {
            "id": "d58d986e-6e1e-4c7e-874e-c7d79f531724",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 56
        },
        {
            "id": "81fed928-3dfa-4a93-8763-aacdc8cddfea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 65
        },
        {
            "id": "3ec8ead7-5fc1-4c4a-a108-ec371ce9a1a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 66
        },
        {
            "id": "78198612-3c39-4e12-a691-2a4c6906a696",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 68
        },
        {
            "id": "f26e03b2-2a76-48b2-b180-06c849c8608f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 69
        },
        {
            "id": "494228fa-dc91-4a92-9cd0-7f745c8c241e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 70
        },
        {
            "id": "15334a41-223f-4fbc-963a-f21f7b2d6016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 72
        },
        {
            "id": "332d2513-a799-44d3-9db8-27d73a3eba9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 73
        },
        {
            "id": "778097ee-e342-4fac-a635-51234063facd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 75
        },
        {
            "id": "b64d3f18-64ca-4c07-bc69-8a8e3af00027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 76
        },
        {
            "id": "0e465443-69ab-47cb-a6d0-6f1d61a0247d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 77
        },
        {
            "id": "e8c1aabf-5d9c-4040-a79c-3853d4b44082",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 78
        },
        {
            "id": "fb7ebbe9-fdcf-4544-9217-d28bbf4fd064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 80
        },
        {
            "id": "75af51d0-a7d5-4e8b-8418-d878a402f37a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 82
        },
        {
            "id": "ffcc8aab-faad-4beb-a599-953ad4835128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 83
        },
        {
            "id": "03f36053-8d97-414f-9767-a30e0fd154f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 84
        },
        {
            "id": "43cdfe08-7eb6-45d6-b51a-7b5affc0c8d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 86
        },
        {
            "id": "c3bf94b1-27fc-4fc2-9870-5ff2d962e869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 87
        },
        {
            "id": "969bdb7e-6c51-44c1-9aa0-0b6848a9bff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 88
        },
        {
            "id": "293207c0-7246-4224-a411-29c5a2cfce74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 89
        },
        {
            "id": "feceac3c-c83e-4880-8488-65c53413369c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 90
        },
        {
            "id": "6687aa3b-7623-4ae6-b114-0225b36c56db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 97
        },
        {
            "id": "d536c1c9-a468-4473-88d6-8e0ac71e927c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 98
        },
        {
            "id": "964b43cc-ed5c-47ee-819a-e3a950477124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 100
        },
        {
            "id": "c7a9a605-425e-4689-8a48-0c6dec80e9b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 101
        },
        {
            "id": "106b0442-2258-4f88-bbe9-3513c37b3589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 102
        },
        {
            "id": "2b44c546-57f3-4c5a-aa09-cb992962f16c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 104
        },
        {
            "id": "5fdcbf83-e148-4367-89c7-3e960c6e19da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 105
        },
        {
            "id": "17b5ba27-d4af-44a4-a254-f70d9c256493",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 107
        },
        {
            "id": "a528acd9-1de3-4779-a124-71def211d8d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 108
        },
        {
            "id": "44ac66a2-6d8f-4723-a591-f520e881039f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 109
        },
        {
            "id": "5147b170-ee85-4bf2-8ea6-e726064c05ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 110
        },
        {
            "id": "17e365e6-dcb3-4de4-8cc9-cac284a028f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 112
        },
        {
            "id": "eb10b621-0ac3-4390-a475-2eb6988c3929",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 114
        },
        {
            "id": "6358c7ec-9d3b-4cdb-898c-a4c27966d939",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 115
        },
        {
            "id": "57dbad9c-8164-46ea-9e69-68acfe9fc915",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 116
        },
        {
            "id": "a40b1e38-895b-48f9-8e6a-6c2bfc32a9b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 118
        },
        {
            "id": "1829114b-883f-4e1c-8702-fdebc7c42773",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 119
        },
        {
            "id": "1d88e66b-95c7-4c0b-be5f-7a87798245bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 120
        },
        {
            "id": "89d8bd4c-08db-4041-9ca9-666e609f266e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 121
        },
        {
            "id": "85898292-c261-41ce-a029-90696c423014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 122
        },
        {
            "id": "46737a02-558e-4004-a08b-cb9e193db683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 48
        },
        {
            "id": "c5577090-2849-45c8-ad4f-47e6cbc0ad74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 54
        },
        {
            "id": "a98de819-7214-4024-b392-418bb752ec09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 56
        },
        {
            "id": "669593d3-8e13-4b84-9d83-5fffa2f567cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 65
        },
        {
            "id": "b61c2eed-432c-42b2-b2c8-d1994c79a90d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 66
        },
        {
            "id": "18873b4f-fd90-47bb-bc90-6a91a6ce31f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 67
        },
        {
            "id": "9f407499-9f84-48da-ac15-943513980fd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 68
        },
        {
            "id": "5f14b0f2-18de-4e1d-9110-07ce436dd390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 69
        },
        {
            "id": "cf7c0f3e-cce0-4f9b-8e26-63e57b910830",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 70
        },
        {
            "id": "f0d1d4ef-fd79-49c3-b058-5580f8363d47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 71
        },
        {
            "id": "a2b4bf07-bea8-4bc3-aa04-8f84fbc8f586",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 72
        },
        {
            "id": "d29683b0-e1e0-4aab-8d8b-83dee1abfa45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 73
        },
        {
            "id": "e4af8bd1-1374-485e-8d1c-39e96efb2f59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 74
        },
        {
            "id": "00a9eb80-934c-4d87-b609-719e87bb90f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 75
        },
        {
            "id": "a46d47da-d672-4a6f-a8dd-cd08a0f19e82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 76
        },
        {
            "id": "0a64dac9-446c-432a-925b-335adc654a2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 77
        },
        {
            "id": "a501a3ca-1208-4378-b0a4-964a095d6960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 78
        },
        {
            "id": "b5432d23-4273-4a70-bfe4-567714a68d0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 79
        },
        {
            "id": "72a81c6f-47dc-48a5-8653-cd259f01dce5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 80
        },
        {
            "id": "10810fe0-f120-41f1-82cf-b2941e394e8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 81
        },
        {
            "id": "f5eec5d0-14de-425e-b502-4180815732d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 82
        },
        {
            "id": "ab2526df-f422-47b9-b79b-2e7603ff4f60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 85
        },
        {
            "id": "2ad41a8d-bcce-4826-b821-966688299759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 86
        },
        {
            "id": "5da62ccf-95e0-4ae6-ab1c-9f4ef34fb919",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 87
        },
        {
            "id": "0aa7ef40-1975-4207-8b60-30e0eb99201e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 115,
            "second": 88
        },
        {
            "id": "14f8f14f-9b38-4a8e-8479-cc67fdcbe103",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 115,
            "second": 89
        },
        {
            "id": "cb3ae5e4-de5a-4bf7-bdb6-0ad517584830",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 97
        },
        {
            "id": "53d14a74-c289-405f-851e-66fd88b86d7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 98
        },
        {
            "id": "caaf0779-244c-4207-a693-caa813496aa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 99
        },
        {
            "id": "4938294f-c84c-4bac-aca8-afcdef325f50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 100
        },
        {
            "id": "266b03f3-0842-4120-9c69-7f541e237084",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 101
        },
        {
            "id": "394ef528-1355-42a1-b1bb-b55e1e6a1201",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 102
        },
        {
            "id": "5313e870-219e-458e-90b9-afe280ce17de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 103
        },
        {
            "id": "d3f99533-0d3a-4ba6-8716-21d709ced231",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 104
        },
        {
            "id": "6b85a773-1dd7-4983-9133-9c71b3545fd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 105
        },
        {
            "id": "7d54f770-fbfe-4618-b4a7-028150466b5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 106
        },
        {
            "id": "fc843d84-d87b-4ba0-a7f7-b7cc9f94aeb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 107
        },
        {
            "id": "a3e7d3cd-2d89-490d-b4e0-c308eec51202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 108
        },
        {
            "id": "46f526d8-a036-4efe-9e39-3dd08fedc716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 109
        },
        {
            "id": "8b1198ee-b591-4cbc-a875-2389300cb833",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 110
        },
        {
            "id": "dbee8108-4ab4-48b5-a5e7-6ccc5f113b4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 111
        },
        {
            "id": "379b35bb-8768-48b1-98f7-c7b67190c814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 112
        },
        {
            "id": "e67b843d-1a4f-4a71-ae75-2a719b25f4e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 113
        },
        {
            "id": "b66aac18-b90e-463d-9d1c-5149f244939c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 114
        },
        {
            "id": "8bef7bbc-59a1-40a2-a599-a1c2c571702f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 115,
            "second": 117
        },
        {
            "id": "ba15dfa8-0030-4639-a7af-fff11405ade0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 118
        },
        {
            "id": "437f4d32-5360-46e9-b452-b57eca4cd8a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 119
        },
        {
            "id": "79c6e581-88cf-4a05-8e27-7cc67bc23225",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 115,
            "second": 120
        },
        {
            "id": "fc234d5f-2bd5-4c50-9cc9-44b270cd475f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 115,
            "second": 121
        },
        {
            "id": "e0b8233d-79d3-4c83-b355-16e6514ccce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 51
        },
        {
            "id": "8f91a84b-6f1b-42d6-b3d1-ff090a13f394",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 116,
            "second": 52
        },
        {
            "id": "cbc035f6-8dbc-486e-b29f-b68391c5bddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 116,
            "second": 65
        },
        {
            "id": "a155beeb-1315-45bb-bcf4-586b7022d9c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 66
        },
        {
            "id": "d860ffd5-44bb-4451-9bdf-a3944a3c64ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 68
        },
        {
            "id": "6935da8f-48ef-4eb6-ab1c-4621c7935564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 69
        },
        {
            "id": "62ebc7ce-a44e-46c3-af9b-85332303c5ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 70
        },
        {
            "id": "34ec6431-6de7-43d7-88d1-95b48f79f879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 72
        },
        {
            "id": "da7a00f2-9ed9-492f-980f-dc54f4d29b45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 73
        },
        {
            "id": "1fc7df6c-50b1-4482-8bce-acf4b78535bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 116,
            "second": 74
        },
        {
            "id": "27277f2a-bc95-4b2c-ba78-1aab3b973384",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 75
        },
        {
            "id": "59842138-79b1-4c98-841d-a947a1e4b8df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 76
        },
        {
            "id": "a409d4c3-0ff2-436f-a00b-23c266776b03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 77
        },
        {
            "id": "862157fd-b59a-4487-938b-569552e87f43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 78
        },
        {
            "id": "668ff9e9-48fb-4eae-9229-fd0ecc7f81ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 80
        },
        {
            "id": "5d1aa75d-ebf1-404d-9ab2-fd6b99d69b39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 82
        },
        {
            "id": "cd462a88-33ae-4bee-851e-986a2ec74330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 85
        },
        {
            "id": "0c0c1c97-f728-423a-b6e0-79c3f1f69bb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 116,
            "second": 97
        },
        {
            "id": "a2a27277-2e84-447e-ae44-751969fc3faf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 98
        },
        {
            "id": "8be23f60-a608-4652-a072-3201476deb9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 100
        },
        {
            "id": "4f6b6599-bd56-4602-af41-d24faf4934d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 101
        },
        {
            "id": "c350254b-5579-4dbe-984b-7cea74fabb4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 102
        },
        {
            "id": "7deec4ad-7637-4ba0-83b8-5051f0d9c714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 104
        },
        {
            "id": "f9b41ec8-3d9a-47d3-8502-c4f2d8536476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 105
        },
        {
            "id": "9814f668-c229-46d8-a1df-c8248c891f70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 116,
            "second": 106
        },
        {
            "id": "d5fcb294-cfc3-4d5b-9a8b-5e874811a24d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 107
        },
        {
            "id": "a578de98-dc57-46e8-be70-8120257b73d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 108
        },
        {
            "id": "d31e650e-bf37-4c9b-9303-c53254f483ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 109
        },
        {
            "id": "34625f3d-3b2b-4d16-8cb3-a4cebf5355f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 110
        },
        {
            "id": "93bdd7a8-824e-4154-879a-9ab6a3684d24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 112
        },
        {
            "id": "0773944c-f5cf-4ddd-8c37-55db1b663b2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 114
        },
        {
            "id": "b8816df5-1cdd-4a94-9f05-9c56ce7ed9e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 117
        },
        {
            "id": "b4c1ed44-282e-475f-8509-194990b085a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 48
        },
        {
            "id": "4edd383a-b60e-46fc-8e64-b084892c67c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 49
        },
        {
            "id": "128686ff-b564-4127-b7bf-adaf0d4053c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 50
        },
        {
            "id": "0d493d58-f101-4d7f-a4e6-1f236f07c1cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 52
        },
        {
            "id": "69456c76-d988-4a38-bb59-2d7fefb5f87e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 53
        },
        {
            "id": "2faf6f1f-aec5-4c33-bd51-b4c22676560a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 54
        },
        {
            "id": "b1cdde36-d926-4900-8fe7-6036814603b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 56
        },
        {
            "id": "69627806-8e59-4e26-b744-cef19cd8a085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 57
        },
        {
            "id": "e167f2d2-2e5b-4edd-97cb-de184648a3ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 66
        },
        {
            "id": "c620750c-412d-4b38-b997-989044a9830d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 67
        },
        {
            "id": "6d07c887-c422-43a8-876b-2e73fe964640",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 68
        },
        {
            "id": "a5d6f2a0-956e-4d00-8f2e-ed0db2685fdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 69
        },
        {
            "id": "e478e774-5892-4315-b25c-fd2056ef9b9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 70
        },
        {
            "id": "977af1ad-f1e7-44d3-be64-d1ded2bc8d05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 71
        },
        {
            "id": "bb5b5575-0906-48e4-902f-3de2f4e76ff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 72
        },
        {
            "id": "a558e7e8-18d2-4835-bc3f-2bebfa00157b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 73
        },
        {
            "id": "c8e0e30d-37ef-49b1-be66-2d4eafdac6da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 75
        },
        {
            "id": "254a2e0e-2c8b-4549-8c76-79f226334799",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 76
        },
        {
            "id": "f6e10db6-6d17-4bc6-9bb7-8c1703974e0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 77
        },
        {
            "id": "2819ed22-86f7-4d7f-abba-d501ccafa11e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 78
        },
        {
            "id": "3867724f-4cc7-4868-8ceb-218a0b8186b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 79
        },
        {
            "id": "ea1cf5b9-5bce-408f-b9bd-d68d5f6b4393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 80
        },
        {
            "id": "02a4bc8f-8d6e-45c3-92f4-c81f352da8c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 81
        },
        {
            "id": "e90eefeb-e9e1-472b-a2d2-355f90236382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 82
        },
        {
            "id": "8b9d2440-23e7-4015-b3f9-7cf0f2e0ce1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 83
        },
        {
            "id": "5c2bb454-121e-4002-89eb-f3b36f6dba05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 84
        },
        {
            "id": "ec3068af-2378-4c68-9844-3a2479d2cc00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 85
        },
        {
            "id": "48722f95-d0e4-4dee-8458-b15c6cd9f2b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 86
        },
        {
            "id": "a2fb16c3-99bf-417b-a286-93fba58b1518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 87
        },
        {
            "id": "49ecadf5-ea9b-402e-9fd4-db70d0f9080c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 88
        },
        {
            "id": "e13c9298-264b-4c86-a2d1-a25c37e57bf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 89
        },
        {
            "id": "1f0ddd40-ecf6-455e-b607-0a2fa1ce8efd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 90
        },
        {
            "id": "5e6e135d-48f5-457c-9614-ad7dfc73022d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 98
        },
        {
            "id": "c7309e11-4bba-45d6-9af8-dd53192be07d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 99
        },
        {
            "id": "473f115e-98b8-4f40-8a47-f19be13103af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 100
        },
        {
            "id": "c0ae9fe8-690a-4616-9aa2-54cfd29aef33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 101
        },
        {
            "id": "051503ba-21fb-43af-9689-2f5678590aa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 102
        },
        {
            "id": "a6bdca19-55d0-472f-9536-cefcf7c9293d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 103
        },
        {
            "id": "fa9cb4fd-c472-4f3c-8e38-8429411cd75b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 104
        },
        {
            "id": "7c43dbc8-f630-4f4c-9d11-45df5c25bd6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 105
        },
        {
            "id": "bda16250-a3cb-4a86-ab9d-f8313978bc96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 107
        },
        {
            "id": "5daa1bbc-a378-448f-a66d-ea33df933ff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 108
        },
        {
            "id": "f74ca021-e9e9-4533-b937-d4a5a6f8c2f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 109
        },
        {
            "id": "96b8d13f-24e8-456b-92bd-722ef645eee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 110
        },
        {
            "id": "10a25e49-9c35-44c1-807c-75810be42878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 111
        },
        {
            "id": "e8299ed8-d0af-4955-a6a0-602c08e249ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 112
        },
        {
            "id": "4d66ae2a-d714-48a3-b68c-a995351823d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 113
        },
        {
            "id": "4d0b3796-58dd-40ab-b2c7-8d7957f3aed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 114
        },
        {
            "id": "2287a584-3b34-4fa8-bed1-acef2979a2a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 115
        },
        {
            "id": "a35a7572-b829-4792-8ba0-b24347bd25fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 116
        },
        {
            "id": "6a1422c4-5f01-43c1-834b-d75355c9b327",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 117,
            "second": 117
        },
        {
            "id": "4370860c-654a-460b-be51-e9436e171876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 118
        },
        {
            "id": "0ab1e29a-4ae5-4ab9-be94-68b265738036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 119
        },
        {
            "id": "5c600946-4a6f-4d34-8392-7e30aa4aeba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 120
        },
        {
            "id": "f80875ef-11a3-472d-9ed0-f7d02ac42c02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 121
        },
        {
            "id": "73e17a2a-6f31-471c-95d0-1bfde062fc2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 117,
            "second": 122
        },
        {
            "id": "19ffde7d-ee39-4456-b8ec-5bea2c4b610a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 50
        },
        {
            "id": "ce707219-5a20-4d79-9797-5d9870a85a90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 51
        },
        {
            "id": "075be2a0-7b28-424b-b3f7-8e96753c7049",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 118,
            "second": 52
        },
        {
            "id": "b0d65d80-05ba-4a1c-8879-d539ca7d48f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 56
        },
        {
            "id": "63c5eda2-6414-46e4-9813-ce901699308b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 118,
            "second": 65
        },
        {
            "id": "693e63a6-ac94-45ab-91a5-f342dadc010b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 66
        },
        {
            "id": "ea8e2973-308c-4653-b083-83fc9f768100",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 68
        },
        {
            "id": "cf0e49bf-d675-4caa-b330-18740db385d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 69
        },
        {
            "id": "bf7ce82a-4f22-4039-841a-b06cf1a4dbc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 70
        },
        {
            "id": "d12af1f5-a373-4324-b352-ec33c861c548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 72
        },
        {
            "id": "361b3ea6-e0b8-49d9-ba21-dc1648c022f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 73
        },
        {
            "id": "2d4d8fe0-a0ad-4d94-b131-bcd1582e4f5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 118,
            "second": 74
        },
        {
            "id": "02c6f5cb-ae27-4063-8a46-859cef8d21b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 75
        },
        {
            "id": "c3765077-0555-448c-a331-54afcade5e44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 76
        },
        {
            "id": "cbd9cf0d-b455-4fa4-ba16-2e246f0f0396",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 77
        },
        {
            "id": "4ccafe1f-c2a1-4dd0-9e4f-8bfdfe0a9121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 78
        },
        {
            "id": "0f9d22a0-eb7a-4c4a-8776-22d4add506b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 80
        },
        {
            "id": "a7114e3e-b075-48dd-b9ad-d9181449b6f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 82
        },
        {
            "id": "e43886e8-72f1-4118-9a03-a6487c65f12a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 83
        },
        {
            "id": "d3c1abf1-d652-46a1-9fe6-513c765e4c19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 85
        },
        {
            "id": "39bf1a5c-3f29-426e-98fc-16789670ba1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 118,
            "second": 97
        },
        {
            "id": "f74ced30-3660-4e72-a919-2a61ab55525c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 98
        },
        {
            "id": "4cdf86b5-8877-467f-8738-1d9d9afca36c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 100
        },
        {
            "id": "c1a2b9be-9e4a-4bab-a3d4-8d0629aebd3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 101
        },
        {
            "id": "d8c8af52-9a28-424b-8b3a-0bb4e537b5f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 102
        },
        {
            "id": "dc47a0fb-9e09-40a5-b783-ce97299c7fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 104
        },
        {
            "id": "85950b75-3d17-425e-94ee-64a11c935f2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 105
        },
        {
            "id": "f297ddf9-4d40-448d-90a6-34fcf25959cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 118,
            "second": 106
        },
        {
            "id": "df86e9af-9326-4fe4-9595-3d67d69c9fc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 107
        },
        {
            "id": "bf29cdde-0ac5-45f4-acfe-e08e72de0f46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 108
        },
        {
            "id": "d93b1f29-b84b-4729-9844-a8461e92c2a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 109
        },
        {
            "id": "d9505c4c-8ea7-4959-87a6-98b6aa381ce6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 110
        },
        {
            "id": "d1dacdfa-a654-4837-8ce1-b4181b127a0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 112
        },
        {
            "id": "5afb1003-7b79-4d49-826e-5372d48f55ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 114
        },
        {
            "id": "ba905658-e465-4388-a31b-fa08c112803a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 115
        },
        {
            "id": "1d46e8fa-62a7-44e7-a1be-09c333452368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 118,
            "second": 117
        },
        {
            "id": "e3871874-6a2f-43c1-a76e-b08509cda12e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 50
        },
        {
            "id": "a3995181-c8cb-41ed-b418-6a67268a05b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 51
        },
        {
            "id": "5c8946bc-237e-40de-908e-b6a73d7f1ff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 119,
            "second": 52
        },
        {
            "id": "53211475-cd5a-460a-b794-54e27b6e6d76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 56
        },
        {
            "id": "1f9fc7ee-66a8-41f7-bbf0-c416e56015ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 119,
            "second": 65
        },
        {
            "id": "9eb657eb-0612-498f-9b86-4327283a64f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 66
        },
        {
            "id": "6036ec2f-0443-4139-95ae-b6f49efa89cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 68
        },
        {
            "id": "c44dcddc-81d8-4d4f-bf6e-6e2a1e14bcd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 69
        },
        {
            "id": "b3ee823d-2e03-45d0-8816-dbfcaf664270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 70
        },
        {
            "id": "edbfe48e-aaad-4ed7-9ea5-8629f0a2bd36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 72
        },
        {
            "id": "7aadb992-8cbe-4561-ad00-2ae24b3ed05d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 73
        },
        {
            "id": "9d55a71d-5396-4f0c-806f-a4f6b364bd47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 119,
            "second": 74
        },
        {
            "id": "4953ddcb-5294-4b4f-977a-49de421ea4af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 75
        },
        {
            "id": "725ccd40-f5f6-4ba4-a9dc-315d09ddb388",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 76
        },
        {
            "id": "c504440d-1a20-4a40-bd3d-d691e003af32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 77
        },
        {
            "id": "3bdbd057-61bc-427e-a54f-c2859555d1a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 78
        },
        {
            "id": "ec4cee65-375f-4c17-9747-43a4a252708e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 80
        },
        {
            "id": "2290ce54-3f5d-4113-aeb5-84010bc1dceb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 82
        },
        {
            "id": "0eb64e15-138b-4cbd-b157-adb0614dda72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 83
        },
        {
            "id": "e2aaba01-d6eb-4f88-8486-7b5b5533b06a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 85
        },
        {
            "id": "cf9cf4a9-0566-44b8-9891-4e72418ac0bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 119,
            "second": 97
        },
        {
            "id": "b34db2bc-8284-468e-b365-db5ede3b851d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 98
        },
        {
            "id": "d000fe97-5a0a-41b2-8ac8-97d7ad262715",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 100
        },
        {
            "id": "875bec2e-44b0-4a7c-9cd0-25e4d4119942",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 101
        },
        {
            "id": "7fb09f29-bc28-4c65-bec7-a2441b38726a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 102
        },
        {
            "id": "34e6f827-436d-444b-b2c9-e7fff27de05b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 104
        },
        {
            "id": "7b952a72-e116-4c17-8d74-ada8d746612c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 105
        },
        {
            "id": "140276c2-7a83-4015-8ff2-c17cb3fda767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 119,
            "second": 106
        },
        {
            "id": "7de76a45-817a-4ff1-90cb-da303a280edf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 107
        },
        {
            "id": "2b5122c4-d099-4e15-af4b-9cb23fe5bea6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 108
        },
        {
            "id": "ad922fdb-2f92-4cec-8183-200fbf98963c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 109
        },
        {
            "id": "0177c74f-d367-4219-bf3a-0fd7d2167f74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 110
        },
        {
            "id": "f9f6f3b2-1537-42f9-a811-fd50d093f4f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 112
        },
        {
            "id": "2da9a8df-739a-4cf3-882d-5cac2fb0a55b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 114
        },
        {
            "id": "fcde153e-ffee-42e7-943a-834596daf3d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 115
        },
        {
            "id": "3b1a18c4-0bf6-4a60-a81e-f54ae0d28327",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 119,
            "second": 117
        },
        {
            "id": "06b1cace-75cd-4822-a647-6cad1cba09b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 48
        },
        {
            "id": "04b67a10-d61b-4bbd-a4b9-2e2c23a93d36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 49
        },
        {
            "id": "63ceda6e-8749-4906-bd03-c2be0b97af26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 51
        },
        {
            "id": "f50aa5bb-f539-4c9b-9c78-8be2a83564b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 120,
            "second": 52
        },
        {
            "id": "2713d320-a773-474a-a4f1-444be07c70fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 54
        },
        {
            "id": "806649fd-e517-423a-acac-b9a81548fc7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 56
        },
        {
            "id": "e45b6f38-caa1-44b2-837e-27642f30cd43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 57
        },
        {
            "id": "ccdebc48-c783-470f-99e8-e4ee3b453c62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 66
        },
        {
            "id": "c6df9e83-eefd-4fc4-8593-1cf7dd1e875f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 67
        },
        {
            "id": "5c360f24-cc31-4172-86a9-46b0a9b2fa5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 68
        },
        {
            "id": "e3cfe142-208d-47e3-b732-0f6d18dcaacc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 69
        },
        {
            "id": "1ed8c98e-c7cd-4623-8537-cdb86c11061a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 70
        },
        {
            "id": "c24d44fe-2db6-43a7-bc23-4e412d04f0fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 71
        },
        {
            "id": "ad47905b-a1a3-4a0c-a1d9-e33c5338b0ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 72
        },
        {
            "id": "3bd1f091-49f2-491b-bdd5-a5086bd68b69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 73
        },
        {
            "id": "9e686f21-d3ea-490d-ac3c-c0ef38ce6505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 75
        },
        {
            "id": "665b48dc-9e9f-45f1-80c9-8b581d509c40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 76
        },
        {
            "id": "40b49cca-2949-4933-aac1-21319dd99622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 77
        },
        {
            "id": "27d5b4f6-83a0-45d5-9689-f30b814d7e0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 78
        },
        {
            "id": "3e04ffde-ccf1-4cab-a39d-9d55e62844e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 79
        },
        {
            "id": "36ceeb5d-b403-4c06-a8c3-71e96c32156a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 80
        },
        {
            "id": "a2adaab4-1c8d-4d12-8ab7-36467a552bb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 81
        },
        {
            "id": "a164ba22-5e89-4654-9304-6cd21e81d782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 82
        },
        {
            "id": "d5589154-72c6-4ebb-bad4-fff17d31238d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 83
        },
        {
            "id": "383a48eb-422b-4fe4-9829-7d5084a551f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 85
        },
        {
            "id": "a845aa11-9425-4e6c-a2e5-0625401318ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 98
        },
        {
            "id": "3d8da261-7e3d-48d2-8bb5-9b9ce686d16b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 99
        },
        {
            "id": "2df69296-40ad-4d95-b407-cc119e17b861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 100
        },
        {
            "id": "c85ef274-7de2-45c0-97de-b96d6f4ccd42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 101
        },
        {
            "id": "3ff4db4d-a0a1-4aef-814f-c0bf768d5b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 102
        },
        {
            "id": "0fcca2fe-2daf-4b76-ad0d-bdb8032856ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 103
        },
        {
            "id": "c81ca392-1da4-415e-a68c-0497361cd569",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 104
        },
        {
            "id": "0a934a3d-f8dd-4b1f-b6c1-7e08d0a9d9c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 105
        },
        {
            "id": "22d299fc-c8ff-4e51-90b0-d6a1d264a590",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 107
        },
        {
            "id": "8f1c7b76-aaed-44d6-8af4-8b1a64a5b859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 108
        },
        {
            "id": "2bc0236e-665c-4d40-84f7-5be1fde2373a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 109
        },
        {
            "id": "5af566cc-ae2c-4bbf-a65a-d4a3e9548ab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 110
        },
        {
            "id": "a8ebfa9e-b122-4bd0-8c8b-2c531805ca73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 111
        },
        {
            "id": "1f05bb8f-9be4-40b0-837c-8965321ebd55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 112
        },
        {
            "id": "b4767406-5003-4e59-8cc1-bcb9bcd7076c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 120,
            "second": 113
        },
        {
            "id": "fc294e3c-2253-45ed-88d1-cd36317e6685",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 114
        },
        {
            "id": "35b4be88-cdef-4efe-93ff-51e751f057a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 120,
            "second": 115
        },
        {
            "id": "17899580-93f8-43ec-a2ce-51323f1a2df7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 120,
            "second": 117
        },
        {
            "id": "61c89387-e75d-42ae-88c3-0b61abd4a41b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 48
        },
        {
            "id": "8d460272-9682-452d-b0fd-91bf1e947624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 49
        },
        {
            "id": "a493b1ff-559d-4d9b-a137-9960793d3dfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 50
        },
        {
            "id": "dce73525-b48a-44af-ad98-0fb2e504457a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 51
        },
        {
            "id": "967119c0-fb03-4c1d-b2cb-1cbabf2df03e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 121,
            "second": 52
        },
        {
            "id": "1f9a8359-226c-45d1-957b-84cb755222a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 54
        },
        {
            "id": "5b6febe2-bf10-4df5-9f21-8aedaa1d8319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 56
        },
        {
            "id": "ee41060e-ec90-45e1-8618-a30f6e0b4eb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 57
        },
        {
            "id": "e397436a-8b9c-4759-8d56-84a5dc29eaf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 121,
            "second": 65
        },
        {
            "id": "fec877c7-9a89-4afc-9ea6-11a45e01d2ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 66
        },
        {
            "id": "e4dcf8e5-071b-46b7-a9c9-592505932b37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 67
        },
        {
            "id": "f6e8e4ae-b2f6-4fb1-8ce3-c95693fe612c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 68
        },
        {
            "id": "b8c28495-3e8e-4eab-bd97-ed10ea083db0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 69
        },
        {
            "id": "b603793b-4de2-45eb-a4c9-53d5230f0491",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 70
        },
        {
            "id": "004c0600-5696-4064-87fd-09528d64033b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 71
        },
        {
            "id": "9e273da3-ccdb-489a-87c6-eceed55b3835",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 72
        },
        {
            "id": "d80771ef-0bbe-4190-a39c-2883fff74941",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 73
        },
        {
            "id": "f9ffe5d3-2d3f-41b1-92c5-a00761d65d6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 121,
            "second": 74
        },
        {
            "id": "54c69aad-5a4f-4384-a331-344084ceded8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 75
        },
        {
            "id": "e653d438-1e62-4f81-b721-86049ca9285c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 76
        },
        {
            "id": "f5600509-c436-4a87-8b55-81a84009c49f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 77
        },
        {
            "id": "7526dfc5-6a66-4123-b23e-86db0f3a9142",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 78
        },
        {
            "id": "d877ac43-4d23-455e-ad1a-bf9c6604dfe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 79
        },
        {
            "id": "b6e68893-4503-48ca-b779-8daba2c55977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 80
        },
        {
            "id": "6c45391d-a0f3-4026-9d4a-99538fa03e0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 81
        },
        {
            "id": "7b638607-3887-4527-a93e-95c1136ab075",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 82
        },
        {
            "id": "734dfa3a-0d59-4002-a9c8-3a1f0491dd3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 83
        },
        {
            "id": "758d1111-813b-4af7-9c85-db6ad3b9c81f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 85
        },
        {
            "id": "fbbe2995-fd7e-45d1-b0d2-5663c4ba712f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 121,
            "second": 97
        },
        {
            "id": "dc419f14-b41c-47d2-bdad-24e95af35f47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 98
        },
        {
            "id": "273d71a7-a9d8-41e8-a954-e3aa490fed24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 99
        },
        {
            "id": "abaecb45-2e3f-470c-b723-197724c517ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 100
        },
        {
            "id": "93994cf3-5104-46b6-aaa8-db65a56a0785",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 101
        },
        {
            "id": "8c59a601-6c84-4d24-917a-9f54dcb0c30d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 102
        },
        {
            "id": "309dcef1-9c4d-48a9-ba8d-8a735d76f33c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 103
        },
        {
            "id": "eb13fe23-f105-4550-aae0-90fa76197c66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 104
        },
        {
            "id": "effcfe3b-0041-4891-8133-3c8f0c6ac8dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 105
        },
        {
            "id": "a3b4d200-376e-46f6-8f54-7b4f268bcde4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 121,
            "second": 106
        },
        {
            "id": "f5f572de-8bdf-4892-b5aa-c6133c82417a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 107
        },
        {
            "id": "18e047b5-c86c-4708-8261-01e2ac728957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 108
        },
        {
            "id": "76fcb65e-275d-452a-a503-cb67be6eb330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 109
        },
        {
            "id": "73af8709-5275-433d-a214-28af2719ba5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 110
        },
        {
            "id": "f7a61b87-1fb2-4c2a-bbea-ff057480d4af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 111
        },
        {
            "id": "8962ad80-4b32-4684-8088-ad84fc2e9134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 112
        },
        {
            "id": "5e3525ef-7cc0-45c6-872b-fcdeaaab892a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 113
        },
        {
            "id": "1bc5ffef-0ecf-4efa-99d5-90a37862caf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 114
        },
        {
            "id": "332e6c4e-091e-44e3-94ac-7be7da7c3724",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 115
        },
        {
            "id": "2b67d805-07c0-48db-8e87-a3f74a1a6cbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 117
        },
        {
            "id": "f9022b0e-249e-4bd4-bebe-c8dafbd43538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 122,
            "second": 52
        },
        {
            "id": "577e9202-bf35-481a-a370-9ffabca27583",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 66
        },
        {
            "id": "d0714c25-ec84-4568-b2c7-58878e7df472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 68
        },
        {
            "id": "10eba41c-dfb5-4be7-8650-e82a9d233ec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 69
        },
        {
            "id": "106af8c6-2b74-4deb-867c-1a6d73015a4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 70
        },
        {
            "id": "1a3dce59-e46a-46b8-991b-a5d9eedc2819",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 72
        },
        {
            "id": "fd73f757-5639-4560-9b76-354ab5ff5752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 73
        },
        {
            "id": "c1d7eb21-7310-4141-ac59-7005060c7921",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 75
        },
        {
            "id": "7ee051b3-744e-4c25-b949-cc73f073cdec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 76
        },
        {
            "id": "312540d1-d96e-4d2e-9901-1fe34f82c5c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 77
        },
        {
            "id": "08040cf8-de21-4245-b79f-8c15b73fe914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 78
        },
        {
            "id": "baa0969a-956d-4c45-a045-38e73aa17b1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 80
        },
        {
            "id": "1e096b55-0d31-4801-9b43-4a4016f06f25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 82
        },
        {
            "id": "b984eda5-8ede-4f10-b601-ecdf46ab359a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 85
        },
        {
            "id": "a970c361-7d7e-4fc4-b7d9-b750d2f4dc33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 98
        },
        {
            "id": "a570f908-2541-4dff-bbc3-95eb261639e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 100
        },
        {
            "id": "d37d3149-cc36-444b-abff-04923454506e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 101
        },
        {
            "id": "dca4898c-21b0-4978-92f2-397ee8583811",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 102
        },
        {
            "id": "cfa95442-7815-46df-ba9c-4ae2b0c3de6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 104
        },
        {
            "id": "0f3f55bf-4346-4f04-8a7d-a52e427cef02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 105
        },
        {
            "id": "b7af546a-8068-41dc-a5ee-8bc9f733a139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 107
        },
        {
            "id": "45e4e473-a6c6-4fd7-afad-0bf67a6c7b2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 108
        },
        {
            "id": "afb4e703-c5e6-4395-ae0e-437073689053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 109
        },
        {
            "id": "29ac13a0-98d0-4a5f-bd5d-daa7ecc6e026",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 110
        },
        {
            "id": "fbf2e9e8-eb59-4617-b3bc-0f79b17ba59a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 112
        },
        {
            "id": "8ff55f38-acc4-4d11-8408-7d98820b37a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 114
        },
        {
            "id": "a9202b93-71a8-493f-b267-336a84085fb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 122,
            "second": 117
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 48,
    "styleName": "Regular",
    "textureGroupId": "00000000-0000-0000-0000-000000000000"
}