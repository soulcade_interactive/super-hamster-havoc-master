{
    "id": "6c6b34c6-6a8c-4c6c-9752-5899d150c6c7",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_8bitwonder_10pt",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "8BIT WONDER",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "5c7e69f5-d8a2-491b-9d3f-8e38e68e8d5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 179,
                "y": 47
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "10fcdb5e-3452-46b5-972b-8f31cd740f78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 134,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "06eeecf8-e7a1-40c2-b00c-6af091577fce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 13,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 91,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "93daec7b-836b-4dbc-9064-879171ef8ef9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "960e4afc-6111-4840-be71-96c93749a463",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 45,
                "y": 62
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "0341bb30-da80-428d-94d7-a95d0abe0c06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 149,
                "y": 47
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c4f3a143-c33e-4a4a-b8ea-9d62aacd1c37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 139,
                "y": 47
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "769d2afa-0c82-426a-add9-dd19c070d0f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 130,
                "y": 62
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b463e16c-3a99-49a9-8164-b60fa8d09637",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 197,
                "y": 47
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "aef881f2-d61e-4a45-809f-1f3b0fa9991b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 206,
                "y": 47
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "977b2d62-41a9-43ad-91b8-c8bb28215a71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 86,
                "y": 32
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "cc128b8b-f90e-4ece-9523-556f9529f895",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 215,
                "y": 47
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c8e92028-2aa3-4300-bdfd-9a59e742e92a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 111,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "d91d90a5-2928-4ae8-adb4-17cbaa5546af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 13,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 77,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0dac6516-7e8d-448d-80d7-947d778df19e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 121,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "74019a56-bda8-4e2b-b9af-6075db645a70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 61,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "a1d76a11-c231-40db-9e29-4b71c93cbfdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 170,
                "y": 32
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "88c6a3f7-9442-422c-92f0-6ded333260ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 13,
                "offset": -1,
                "shift": 7,
                "w": 7,
                "x": 224,
                "y": 47
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "daebf62b-52f4-48c4-a4fb-aadeee3112c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 198,
                "y": 32
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "729fc47a-c31c-4e9e-94ab-9d4b735c28e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 100,
                "y": 47
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "87eb9693-ef10-4f1d-8251-16809979fabe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 86,
                "y": 47
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c5020a83-83ef-4593-ad71-eb91175c8756",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 72,
                "y": 47
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "10677c34-b458-40cd-8951-ff0675db758e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 58,
                "y": 47
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "3d2908dc-2e4f-4f9b-8253-3663c876732d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 47
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "fe1fd925-b3a1-4d1e-9dd9-8c9150b35bf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 114,
                "y": 47
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f39174f6-b274-4ced-9bb8-80e94939267a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 47
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "0b00c4db-bcd6-4a77-9dde-7ee3814e97da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 126,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "46304f82-d065-4c3d-87fa-a0072fb48be2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 116,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "644e73f4-869b-4a57-8563-a741b8fec18d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 233,
                "y": 47
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "137c55e1-0acc-4cf1-bacc-39b2098f077a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 242,
                "y": 47
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "e64453ba-0338-458a-b9fe-0415c5a206ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 62
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "1f294192-e7e3-4747-a99f-85b6fdb22da7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 69,
                "y": 62
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "5a5724ba-4c42-440a-8d59-aa666d0ebcaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 169,
                "y": 47
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "4d81ee4e-658a-4f3e-bd11-3e5381171c2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 226,
                "y": 17
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "d774a75f-2ac4-4faf-a9be-4995c915ce0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 17
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "bb7e8755-9b67-4976-9e59-ab1b676c29d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d1b15944-3af3-42cd-8bf6-6f086c7a65d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "63ecca5b-73af-4438-b58c-ace739f80532",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "9ca003b4-8f2d-43cd-85d8-8f7801df0dce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "ba8cd4e1-57c9-49f5-98b1-b49ef176b064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "5ddda7b3-d012-4e60-93e2-b1d2c9e3abed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1d69b9f3-a55d-4ed9-bea0-f9d9cc59f53d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 84,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2b0c1213-a71c-4dc7-b722-917276a0ecaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "4e4e243b-07a0-44c2-9d14-3598cdeebde2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "88026efd-36d0-4775-bca1-1d5c0efce522",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "74a8acda-eb02-49f1-a417-4e09df8958d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 13,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "94695871-41d9-439e-921c-dc8f8c246c12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 17
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "dd3ce0e4-9963-448e-aaaa-e1d1c8619977",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "6bdb7a22-9cad-409e-8243-ebe1b132e5b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 17
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d9344eaa-948c-4d6a-a2f6-ad5cce830ed9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 17
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "a6196bec-e0b7-4def-b039-5b3f2ef09291",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 58,
                "y": 17
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "9756f3cc-2115-4a35-ad30-69579d1cd5c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 72,
                "y": 17
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "501b7ddd-9357-4e63-9594-0df41773bf9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 86,
                "y": 17
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "799146b8-07f0-45fe-b442-63771633fb40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 100,
                "y": 17
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4876172a-cb7c-4463-b87b-aa6b3021cdff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 114,
                "y": 17
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "2571e044-7e26-4e85-9829-000c13539472",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 13,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "bb035ef8-7cec-4a7e-b69a-916af9012683",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 128,
                "y": 17
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "80e80183-af02-4f0e-993f-bc420c98e0ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 142,
                "y": 17
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "3c9ec061-7ec4-4100-8d0c-52b142fb1deb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 156,
                "y": 17
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "67e247d9-502a-4090-9da3-ff2aa3c908e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 62
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "fc1b7fb4-18f4-48be-aa9b-a2ea088b1d3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 53,
                "y": 62
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "fe17821e-32b4-4d30-b8a1-3ef4fa2ec05f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 188,
                "y": 47
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "248ec106-7336-4dd8-a3fd-3ce7da630772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 159,
                "y": 47
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ee069784-b2bc-4062-8f6f-48e1a237bb87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 128,
                "y": 47
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "686b5f43-d7eb-40aa-8033-9296df803799",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 4,
                "x": 105,
                "y": 62
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "5b026f99-21e5-428d-a96f-897460dab1ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 240,
                "y": 17
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "8afcd521-e25f-4b8b-a7e2-d5c0ca55e134",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 156,
                "y": 32
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "dd4ed5ec-7622-4385-8555-ad6f545c837f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 198,
                "y": 17
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ae097eda-87dd-4103-94cb-98f79ae37bb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 184,
                "y": 17
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a54df703-dc0b-423a-8fda-55d9d9f749db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 170,
                "y": 17
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "77b76b01-214c-4b24-98e3-90f85bdd596b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 212,
                "y": 17
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f9f99ee5-86b9-4034-a1cb-c88a1671f1dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "51cc8084-7483-45ab-9292-d94b8f52c7d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "917b948c-8e1c-40af-98f2-ee18d0a55d85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 98,
                "y": 62
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3f331759-6013-421d-82af-26a7657e0835",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 212,
                "y": 32
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a7f5f777-96c2-4da8-b245-965836b6c09e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 226,
                "y": 32
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "53c03d46-5648-42c5-9fdd-6071fdd4a4d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 240,
                "y": 32
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "bab63093-7dee-42a1-a451-02ac7a43f0f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 13,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "19cbbc95-5390-4907-b87e-813a29135a13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 47
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ba2b38b0-9d9c-440d-8e4a-e8c15075981a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 47
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "333926d6-c108-4f5f-84e0-39a82e7dd1c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 184,
                "y": 32
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "5d2adb81-5274-4e33-bc75-b73e71f9f2a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 142,
                "y": 32
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "12084cf0-2bd5-4a3a-86f4-754098ee8aef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 128,
                "y": 32
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e87a4519-af96-404d-822a-7e09345b2c9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 114,
                "y": 32
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "99541b13-6c85-4f8d-9162-274ab47128e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 100,
                "y": 32
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9d4cbab7-7bca-4c20-b887-9e3d034c4487",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 72,
                "y": 32
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "04f437ec-e89c-4348-b0fa-753949104643",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 58,
                "y": 32
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "6d67e9b9-bf85-47a2-be8f-9ea51449b7c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 13,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e35ac392-fa85-487e-b4d7-ca87849455dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 32
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "05c7887c-6feb-4dbe-af40-22fd8144ce4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 32
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "e326b210-9ce0-496c-a1cd-d8654666efa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 32
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "a5a077f2-7dc9-494a-8451-18804992c5c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 37,
                "y": 62
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "22cf1e45-03aa-480b-ad86-c3ee1ec7caf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 138,
                "y": 62
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e187013f-ebee-4912-b31d-a7b9ad2fd212",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 29,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "cd5407ab-5234-4065-93b3-581aa590c68a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 62
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 10,
    "styleName": "Nominal",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}