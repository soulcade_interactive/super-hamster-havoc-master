{
    "id": "123ba67f-40de-4668-87ef-c3f888b24174",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_bebas_kai_96_pt",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bebas Kai",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "91068e14-ea3e-4973-b3f7-caf1cdb03b2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 154,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "f7cd3960-bb1f-4931-af22-6ce9967db17e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 154,
                "offset": 7,
                "shift": 29,
                "w": 15,
                "x": 1350,
                "y": 314
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "c4f24577-8b73-452a-93f5-8c9bb37288f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 154,
                "offset": 6,
                "shift": 48,
                "w": 36,
                "x": 1367,
                "y": 314
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "faf84fae-6d24-4e8d-9505-1c4e4e04cbf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 154,
                "offset": 3,
                "shift": 62,
                "w": 56,
                "x": 1405,
                "y": 314
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "4a3d85e5-699d-43b6-8cf4-c05243c3e01c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 154,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 1463,
                "y": 314
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "10328d30-d568-4e77-a427-9d8aef1651f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 154,
                "offset": 3,
                "shift": 74,
                "w": 68,
                "x": 1508,
                "y": 314
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "adb066f4-d419-43d2-a579-972b20553bc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 154,
                "offset": 4,
                "shift": 61,
                "w": 58,
                "x": 1578,
                "y": 314
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "53f939c1-4369-4ab8-8d13-b58a28bc30b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 154,
                "offset": 6,
                "shift": 27,
                "w": 15,
                "x": 1638,
                "y": 314
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "d8fade52-3f00-4ec5-812b-65d1975400c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 154,
                "offset": 6,
                "shift": 38,
                "w": 29,
                "x": 1655,
                "y": 314
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "19d924eb-3674-46ee-b245-8902a8fe22ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 154,
                "offset": 3,
                "shift": 38,
                "w": 29,
                "x": 1686,
                "y": 314
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "0a2a3e9b-bebf-4ab2-9abf-f276351876db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 154,
                "offset": 2,
                "shift": 45,
                "w": 42,
                "x": 1764,
                "y": 314
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "7dd630c1-77ca-4f35-a63d-33c796735892",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 154,
                "offset": 2,
                "shift": 55,
                "w": 51,
                "x": 101,
                "y": 470
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "55ba0445-eab1-420b-9f78-ef7886c8c247",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 154,
                "offset": 7,
                "shift": 29,
                "w": 15,
                "x": 1808,
                "y": 314
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4573c41c-4e5b-4a7d-b60f-1956d0548568",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 154,
                "offset": 5,
                "shift": 38,
                "w": 28,
                "x": 1825,
                "y": 314
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ebcff866-f0dd-4923-bee7-9a9d00b293b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 154,
                "offset": 7,
                "shift": 29,
                "w": 15,
                "x": 1855,
                "y": 314
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "e04d2e2c-b23b-4c99-be74-e0441a13009d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 154,
                "offset": 2,
                "shift": 48,
                "w": 44,
                "x": 1872,
                "y": 314
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1af5e1e7-bcb8-456d-b719-dae31934fd51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 154,
                "offset": 5,
                "shift": 56,
                "w": 45,
                "x": 1918,
                "y": 314
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "898f015a-1d53-4cec-8416-475e7b161aa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 154,
                "offset": 2,
                "shift": 43,
                "w": 34,
                "x": 1965,
                "y": 314
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d38c5990-0395-479e-9932-7723ef683584",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 154,
                "offset": 3,
                "shift": 49,
                "w": 42,
                "x": 2001,
                "y": 314
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "97b45fbf-15e5-4cce-b51e-5a47ff4dbebc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 154,
                "offset": 3,
                "shift": 51,
                "w": 45,
                "x": 2,
                "y": 470
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "859f5b3e-6706-4786-8dec-32d6dcb19548",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 154,
                "offset": 1,
                "shift": 53,
                "w": 50,
                "x": 49,
                "y": 470
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b2ad8148-478c-4902-a14e-3e6dfa74c99c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 154,
                "offset": 4,
                "shift": 51,
                "w": 44,
                "x": 1304,
                "y": 314
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "4f6f6590-9725-4899-b121-aa6141df7672",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 154,
                "offset": 5,
                "shift": 54,
                "w": 45,
                "x": 1717,
                "y": 314
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "81c473f5-3f4a-430e-b24c-b02789cd63a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 154,
                "offset": 1,
                "shift": 45,
                "w": 43,
                "x": 1259,
                "y": 314
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "1011181a-98e6-4e8b-baea-06e83c8c8d38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 154,
                "offset": 3,
                "shift": 53,
                "w": 46,
                "x": 705,
                "y": 314
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "f75395b5-439a-4816-bb1b-e7acb33c4ee1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 154,
                "offset": 4,
                "shift": 54,
                "w": 45,
                "x": 280,
                "y": 314
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "dc4d1cb9-0d7c-4025-8f8c-6036bd3ad254",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 154,
                "offset": 7,
                "shift": 29,
                "w": 15,
                "x": 327,
                "y": 314
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "8b6fc062-d049-4115-a44e-786b3e182475",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 154,
                "offset": 7,
                "shift": 29,
                "w": 15,
                "x": 344,
                "y": 314
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a18270fb-106b-4a12-b9df-4dcbe7e44be5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 154,
                "offset": 2,
                "shift": 55,
                "w": 51,
                "x": 361,
                "y": 314
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "ad3777fe-2dad-4edf-a608-84e3dd2c7dc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 154,
                "offset": 2,
                "shift": 55,
                "w": 51,
                "x": 414,
                "y": 314
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "811a0a14-5756-4e45-90b2-743cdeaf25b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 154,
                "offset": 2,
                "shift": 55,
                "w": 51,
                "x": 467,
                "y": 314
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "affb0269-45be-4ebd-a146-c97757139a23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 154,
                "offset": 2,
                "shift": 47,
                "w": 42,
                "x": 520,
                "y": 314
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "258f212b-cf49-46e8-8444-7fe24219457f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 154,
                "offset": 3,
                "shift": 94,
                "w": 88,
                "x": 564,
                "y": 314
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "a09d566b-90a9-49fa-bcac-3b64d8b9e447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 154,
                "offset": 2,
                "shift": 53,
                "w": 49,
                "x": 654,
                "y": 314
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e4f30dfc-7719-43a5-9472-3b189e1a8d40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 154,
                "offset": 7,
                "shift": 53,
                "w": 43,
                "x": 753,
                "y": 314
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "36678618-2494-4399-9570-c5fcc652cbc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 154,
                "offset": 5,
                "shift": 52,
                "w": 43,
                "x": 1154,
                "y": 314
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b7c08def-2366-444d-98c2-c3d49a39a288",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 154,
                "offset": 7,
                "shift": 55,
                "w": 43,
                "x": 798,
                "y": 314
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "97184452-0909-479b-9d08-7f64d7c97cd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 154,
                "offset": 7,
                "shift": 50,
                "w": 39,
                "x": 843,
                "y": 314
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "a9db263c-a967-4692-865f-65fb4394440e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 154,
                "offset": 7,
                "shift": 46,
                "w": 38,
                "x": 884,
                "y": 314
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "d77a4aa0-fff1-4736-b770-4cedfd3a087b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 154,
                "offset": 5,
                "shift": 54,
                "w": 44,
                "x": 924,
                "y": 314
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f00df417-48e1-4bb0-8d19-3d5805e94009",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 154,
                "offset": 7,
                "shift": 58,
                "w": 44,
                "x": 970,
                "y": 314
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c81f6a0c-f1ee-4b13-aa6a-dcf1808b2d78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 154,
                "offset": 7,
                "shift": 28,
                "w": 14,
                "x": 1016,
                "y": 314
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2ace2f78-dd96-454c-90e2-a76d191e968a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 154,
                "offset": 1,
                "shift": 39,
                "w": 31,
                "x": 1032,
                "y": 314
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "720fc865-b5b6-4af4-a69b-457e06f89838",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 154,
                "offset": 7,
                "shift": 55,
                "w": 48,
                "x": 1065,
                "y": 314
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "7c3862d5-e6d0-45be-940e-55ba34321589",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 154,
                "offset": 7,
                "shift": 45,
                "w": 37,
                "x": 1115,
                "y": 314
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "7b3af6fd-ebc3-4518-bcde-3ad4f74db1b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 154,
                "offset": 7,
                "shift": 72,
                "w": 58,
                "x": 1199,
                "y": 314
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f4c26a43-8f0e-4ffb-ab47-bcf0a8649f65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 154,
                "offset": 7,
                "shift": 59,
                "w": 45,
                "x": 210,
                "y": 470
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "226c5d24-786e-4b4e-a627-41c2d2fd5770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 154,
                "offset": 5,
                "shift": 56,
                "w": 45,
                "x": 1240,
                "y": 470
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c6f9307a-963e-472f-aff8-688056f15b55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 154,
                "offset": 7,
                "shift": 52,
                "w": 43,
                "x": 257,
                "y": 470
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "60d0d645-a569-412f-bd16-d5f5cf8328e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 154,
                "offset": 5,
                "shift": 56,
                "w": 48,
                "x": 1374,
                "y": 470
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ddfb4575-3150-4550-8a60-dbac3cd4c91b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 154,
                "offset": 7,
                "shift": 55,
                "w": 46,
                "x": 1424,
                "y": 470
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "c749b264-18fc-4b4c-966a-9fb405a9a2d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 154,
                "offset": 2,
                "shift": 46,
                "w": 43,
                "x": 1472,
                "y": 470
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "777caea5-c368-4f95-83fb-6932f4cef450",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 154,
                "offset": 1,
                "shift": 45,
                "w": 43,
                "x": 1517,
                "y": 470
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "5efd2671-2ff1-4d32-bce2-92ccc73e8c77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 154,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 1562,
                "y": 470
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "67350c0e-394d-4722-8164-744dbe4cbd1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 154,
                "offset": 2,
                "shift": 53,
                "w": 49,
                "x": 1607,
                "y": 470
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "9693f338-7c50-42fa-9361-5c099ea6bb7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 154,
                "offset": 2,
                "shift": 76,
                "w": 72,
                "x": 1658,
                "y": 470
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "afddf356-6765-4520-bf01-de0e6e4de3ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 154,
                "offset": 1,
                "shift": 53,
                "w": 51,
                "x": 1732,
                "y": 470
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "564e17f3-91b8-4917-843f-ca5e6d7b7a30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 154,
                "offset": -2,
                "shift": 48,
                "w": 52,
                "x": 1785,
                "y": 470
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "375438d2-8a47-41fa-99b9-5491a3f31738",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 154,
                "offset": 2,
                "shift": 45,
                "w": 41,
                "x": 1879,
                "y": 470
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ebc4dd29-b95a-4737-b1b0-f74e5a0db840",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 154,
                "offset": 8,
                "shift": 38,
                "w": 26,
                "x": 324,
                "y": 626
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d509c37c-dcd2-4b45-b365-c03402f8f69b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 154,
                "offset": 2,
                "shift": 48,
                "w": 44,
                "x": 1922,
                "y": 470
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5d2adfdf-598e-46c0-91bd-1a1715275278",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 154,
                "offset": 4,
                "shift": 38,
                "w": 26,
                "x": 1968,
                "y": 470
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "de060046-6079-423d-9644-ac590b0fbfb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 154,
                "offset": 2,
                "shift": 55,
                "w": 51,
                "x": 2,
                "y": 626
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e6a181d3-cc5c-42e6-b515-04386e290cb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 154,
                "offset": 0,
                "shift": 56,
                "w": 57,
                "x": 55,
                "y": 626
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "ab5f46ae-4676-4d26-9607-1702acb9396c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 154,
                "offset": 1,
                "shift": 32,
                "w": 22,
                "x": 114,
                "y": 626
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "e0bc5b67-2fe5-46fd-ba3a-8aa937b4a2a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 154,
                "offset": 2,
                "shift": 53,
                "w": 49,
                "x": 138,
                "y": 626
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "bda39004-090c-4942-bde8-b33b354193ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 154,
                "offset": 7,
                "shift": 53,
                "w": 43,
                "x": 189,
                "y": 626
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5f5be827-060d-4f2c-b80d-1c4b58679162",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 154,
                "offset": 5,
                "shift": 52,
                "w": 43,
                "x": 234,
                "y": 626
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "9e838db6-6b87-433e-b1f1-04429109697b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 154,
                "offset": 7,
                "shift": 55,
                "w": 43,
                "x": 279,
                "y": 626
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f5546048-3589-4106-9257-73f45e149f53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 154,
                "offset": 7,
                "shift": 50,
                "w": 39,
                "x": 1333,
                "y": 470
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "522165b1-2bd2-48fa-90da-cfb0b4f7784d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 154,
                "offset": 7,
                "shift": 46,
                "w": 38,
                "x": 1839,
                "y": 470
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "cfb2cdb0-3f51-4cb0-8e5b-b99b8eb5bd88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 154,
                "offset": 5,
                "shift": 54,
                "w": 44,
                "x": 1287,
                "y": 470
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "940cfd60-c97d-47fa-bf2d-ea38119978e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 154,
                "offset": 7,
                "shift": 58,
                "w": 44,
                "x": 689,
                "y": 470
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4263c294-6a09-44a0-99ce-e71ea0819acc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 154,
                "offset": 7,
                "shift": 28,
                "w": 14,
                "x": 302,
                "y": 470
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "b0fd2527-271b-43d3-a066-17c05ba5d3d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 154,
                "offset": 1,
                "shift": 39,
                "w": 31,
                "x": 318,
                "y": 470
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "044073e6-c850-4104-9633-e4e505f15655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 154,
                "offset": 7,
                "shift": 55,
                "w": 48,
                "x": 351,
                "y": 470
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "94b1e568-7bd8-4f61-bfba-fefa0dac55b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 154,
                "offset": 7,
                "shift": 45,
                "w": 37,
                "x": 401,
                "y": 470
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "3a97a6cc-c850-4f4c-8037-9cabf4e729e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 154,
                "offset": 7,
                "shift": 72,
                "w": 58,
                "x": 440,
                "y": 470
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "e69726e8-b4df-4972-9e98-986c764cb519",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 154,
                "offset": 7,
                "shift": 59,
                "w": 45,
                "x": 500,
                "y": 470
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "dc055498-2444-426e-9c3e-7a69279b5e61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 154,
                "offset": 5,
                "shift": 56,
                "w": 45,
                "x": 547,
                "y": 470
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "db2bdd3a-1c58-46fe-a3df-154638aa5b94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 154,
                "offset": 7,
                "shift": 52,
                "w": 43,
                "x": 594,
                "y": 470
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f63e2044-2209-41e3-a510-2f0b6da6f3c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 154,
                "offset": 5,
                "shift": 56,
                "w": 48,
                "x": 639,
                "y": 470
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "96d15d7a-3ae6-4aef-bdb3-16156c19c023",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 154,
                "offset": 7,
                "shift": 55,
                "w": 46,
                "x": 735,
                "y": 470
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "792a8a22-7eda-4f09-b3af-67506587f007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 154,
                "offset": 2,
                "shift": 46,
                "w": 43,
                "x": 1195,
                "y": 470
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b5efb02e-0980-4342-bf41-84faa089953a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 154,
                "offset": 1,
                "shift": 45,
                "w": 43,
                "x": 783,
                "y": 470
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "04540122-4dbb-4cf6-bb2f-1840a5514f4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 154,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 828,
                "y": 470
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c1a5e9be-bcf7-4ea8-bf56-072fbc3d459f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 154,
                "offset": 2,
                "shift": 53,
                "w": 49,
                "x": 873,
                "y": 470
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "a5b4526f-48a1-46b5-8a0c-4a48c2cf68bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 154,
                "offset": 2,
                "shift": 76,
                "w": 72,
                "x": 924,
                "y": 470
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e3cf95f5-5bdf-4901-8acc-3fd945559bb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 154,
                "offset": 1,
                "shift": 53,
                "w": 51,
                "x": 998,
                "y": 470
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "bb6eab1b-6827-40b8-8d1d-e9ae73e5044b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 154,
                "offset": -2,
                "shift": 48,
                "w": 52,
                "x": 1051,
                "y": 470
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "931a69bc-e99c-4fe7-8fc3-ec385ef9d85f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 154,
                "offset": 2,
                "shift": 45,
                "w": 41,
                "x": 1105,
                "y": 470
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "31e75cbf-b8c6-4028-b5c4-6619312f8249",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 154,
                "offset": 3,
                "shift": 38,
                "w": 31,
                "x": 1148,
                "y": 470
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "4ff87500-99f6-417c-897d-f43925415f31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 154,
                "offset": 9,
                "shift": 30,
                "w": 12,
                "x": 1181,
                "y": 470
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "ce7ab54d-0fe0-4951-9d16-5f8eb74b0cb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 154,
                "offset": 4,
                "shift": 38,
                "w": 31,
                "x": 247,
                "y": 314
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "916d04a4-83d7-4dc3-9909-615ae78d1018",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 154,
                "offset": 1,
                "shift": 55,
                "w": 54,
                "x": 154,
                "y": 470
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "0386e9a8-70ea-4ed3-ae09-85f5e89b1fa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 154,
                "offset": 0,
                "shift": 24,
                "w": 0,
                "x": 245,
                "y": 314
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "c1c00233-1e52-4c5f-a240-1cb65ba071c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 154,
                "offset": 7,
                "shift": 29,
                "w": 15,
                "x": 2,
                "y": 158
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "a9218c2a-6b3e-487f-b73b-44d6c2b7717c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 154,
                "offset": 7,
                "shift": 55,
                "w": 43,
                "x": 1140,
                "y": 2
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "13df91b5-7efe-4fe2-97af-94b8abbfe7ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 154,
                "offset": 4,
                "shift": 55,
                "w": 47,
                "x": 1185,
                "y": 2
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "2a7863b9-3228-48fb-ab1d-0e604ac03d68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 154,
                "offset": 1,
                "shift": 55,
                "w": 53,
                "x": 1234,
                "y": 2
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "303ca7ed-9a3e-4d56-bb00-c6a505b9c084",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 154,
                "offset": 2,
                "shift": 55,
                "w": 51,
                "x": 1289,
                "y": 2
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "4d262758-3f03-4843-bae7-ef44a1d5e177",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 154,
                "offset": 9,
                "shift": 30,
                "w": 12,
                "x": 1342,
                "y": 2
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "89eb9ce6-f7c6-4222-94ec-2957a9f1b17b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 154,
                "offset": 3,
                "shift": 52,
                "w": 45,
                "x": 1356,
                "y": 2
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "0bb7f2c6-a79f-4d24-87d3-c9a7065fe6d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 154,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 1403,
                "y": 2
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "94df2e1c-1322-448f-a398-1b1531fcd1ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 154,
                "offset": 3,
                "shift": 95,
                "w": 89,
                "x": 1437,
                "y": 2
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "a33dafbc-d2b3-48ab-b9a5-303e573b411a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 154,
                "offset": 2,
                "shift": 34,
                "w": 27,
                "x": 1528,
                "y": 2
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "b05b846e-b239-4fab-9c88-61e6fe962ad6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 154,
                "offset": 4,
                "shift": 56,
                "w": 46,
                "x": 1574,
                "y": 2
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "186ec622-41ee-426c-953e-71c36f9934cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 154,
                "offset": 2,
                "shift": 55,
                "w": 51,
                "x": 1984,
                "y": 2
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "06668fea-7e91-4868-a783-8f9bd4e7a40a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 154,
                "offset": 5,
                "shift": 38,
                "w": 28,
                "x": 1622,
                "y": 2
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "1bf09399-1c81-4add-ae80-392c0c34f72e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 154,
                "offset": 3,
                "shift": 95,
                "w": 89,
                "x": 1652,
                "y": 2
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "4eb8c8fb-a64e-49da-a51f-4a9d99d06ead",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 154,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 1743,
                "y": 2
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "22ff0292-1b14-487b-8774-b52fe4fd8a88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 154,
                "offset": 3,
                "shift": 36,
                "w": 29,
                "x": 1771,
                "y": 2
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "82161012-31fb-40ae-8778-8709f3cc0541",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 154,
                "offset": 2,
                "shift": 55,
                "w": 51,
                "x": 1802,
                "y": 2
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "1689cb38-71ea-48c4-9987-82fec883aead",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 154,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 1855,
                "y": 2
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "888ec37a-3847-4e14-888f-dd081e40b9fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 154,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 1885,
                "y": 2
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "98faaefd-abc6-46a4-b343-30fbd80b351e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 154,
                "offset": 9,
                "shift": 32,
                "w": 22,
                "x": 1915,
                "y": 2
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "5abdb1e1-7086-4fcc-8629-62b53828b437",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 154,
                "offset": 7,
                "shift": 56,
                "w": 43,
                "x": 1939,
                "y": 2
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "a3a17952-3386-476b-a278-95874e859338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 154,
                "offset": 4,
                "shift": 64,
                "w": 52,
                "x": 1086,
                "y": 2
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "239e9184-94e6-4e3f-9ce1-10c5eb26fe60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 154,
                "offset": 7,
                "shift": 29,
                "w": 15,
                "x": 1557,
                "y": 2
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "8a0b6944-4d25-438c-a930-458d9627f6de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 154,
                "offset": 3,
                "shift": 32,
                "w": 26,
                "x": 1058,
                "y": 2
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "70e674a0-59b5-4399-b601-2ddf136b2909",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 154,
                "offset": 1,
                "shift": 25,
                "w": 20,
                "x": 516,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "68b8002c-47a1-4a13-ab5f-eef66a0ae81a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 154,
                "offset": 4,
                "shift": 36,
                "w": 28,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "86627135-5741-454a-bf03-f9085725ac3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 154,
                "offset": 5,
                "shift": 56,
                "w": 47,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "fe90a505-ab0b-4c93-8653-f6a2def2a394",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 154,
                "offset": 1,
                "shift": 68,
                "w": 66,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "b8f7aba4-2e7f-4d7b-9300-565db7b41bfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 154,
                "offset": 1,
                "shift": 71,
                "w": 68,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "5b099c40-cc37-4a16-ac5c-4624b37d1c93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 154,
                "offset": 2,
                "shift": 75,
                "w": 72,
                "x": 245,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "94b9e711-3677-4f2b-825e-dfa87ec31691",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 154,
                "offset": 3,
                "shift": 47,
                "w": 42,
                "x": 319,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "1552946b-5b16-412b-8683-c4e7f5444c32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 154,
                "offset": 2,
                "shift": 53,
                "w": 49,
                "x": 363,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "285abc70-9376-485b-b7ae-5abaf7f7018f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 154,
                "offset": 2,
                "shift": 53,
                "w": 49,
                "x": 414,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "662e4ed5-e8e0-4e45-8ee1-4fe23267bc0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 154,
                "offset": 2,
                "shift": 53,
                "w": 49,
                "x": 465,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "61b73fae-7fe9-440b-b369-f44ab6883610",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 154,
                "offset": 2,
                "shift": 53,
                "w": 49,
                "x": 538,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "24eb8c70-cbd6-46bc-b489-cafe2256786b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 154,
                "offset": 2,
                "shift": 53,
                "w": 49,
                "x": 973,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "4b90aadc-572e-4546-b9be-ba03df06aac9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 154,
                "offset": 2,
                "shift": 53,
                "w": 49,
                "x": 589,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "3de11e4e-3930-470c-962c-1ced7742008a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 154,
                "offset": 1,
                "shift": 78,
                "w": 74,
                "x": 640,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "8878115e-0188-46a8-87bc-8d6dacb8b86f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 154,
                "offset": 5,
                "shift": 52,
                "w": 43,
                "x": 716,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "410ef129-648d-4809-9770-481062e2736b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 154,
                "offset": 7,
                "shift": 50,
                "w": 39,
                "x": 761,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "7b2fcc86-19e0-40bb-b48d-a4ab1d0213ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 154,
                "offset": 7,
                "shift": 50,
                "w": 39,
                "x": 802,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "139fe408-8c6f-4fa8-a1a0-77d32b1a1e2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 154,
                "offset": 7,
                "shift": 50,
                "w": 39,
                "x": 843,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "bf339114-0e7d-41e2-aebc-d93cd8384687",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 154,
                "offset": 7,
                "shift": 50,
                "w": 39,
                "x": 884,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "562a5b92-7e1b-4d05-8e6b-e94e8d159c75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 154,
                "offset": -1,
                "shift": 28,
                "w": 22,
                "x": 925,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "b835b06f-b171-426f-aab1-634b68a68556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 154,
                "offset": 7,
                "shift": 28,
                "w": 22,
                "x": 949,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "07ea013c-daa3-4ddd-ac2d-5c59d26e9a62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 154,
                "offset": -2,
                "shift": 28,
                "w": 32,
                "x": 1024,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "873de7c9-1f77-4ae3-adc4-bf28d4bb4927",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 154,
                "offset": -2,
                "shift": 28,
                "w": 32,
                "x": 19,
                "y": 158
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "2152a400-6ad1-4dd5-a65f-7a62d7182f97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 154,
                "offset": 2,
                "shift": 56,
                "w": 49,
                "x": 1007,
                "y": 158
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "42116f57-1bbb-450c-b1da-f50170ef1efa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 154,
                "offset": 7,
                "shift": 59,
                "w": 45,
                "x": 53,
                "y": 158
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "aec7a843-a561-4728-8005-3a8f76d9a0a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 154,
                "offset": 5,
                "shift": 56,
                "w": 45,
                "x": 1175,
                "y": 158
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "7a72a21e-98e6-437d-a665-cfadf8734e55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 154,
                "offset": 5,
                "shift": 56,
                "w": 45,
                "x": 1222,
                "y": 158
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "b4da8fea-1032-4ca1-a24a-e34b4467f5ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 154,
                "offset": 5,
                "shift": 56,
                "w": 45,
                "x": 1269,
                "y": 158
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "4cd40949-8f3d-49fe-839a-172f479162eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 154,
                "offset": 5,
                "shift": 56,
                "w": 45,
                "x": 1316,
                "y": 158
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "507829ac-df45-41f3-8874-6a2008ec8da2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 154,
                "offset": 5,
                "shift": 56,
                "w": 45,
                "x": 1363,
                "y": 158
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "6d8d48c8-4ba6-43de-b896-bc18e7632db4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 154,
                "offset": 3,
                "shift": 55,
                "w": 49,
                "x": 1410,
                "y": 158
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "200f007c-a0a2-4fac-8dbf-b08f9b3741f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 154,
                "offset": 5,
                "shift": 56,
                "w": 46,
                "x": 1461,
                "y": 158
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "684846c1-f937-4aaa-b2fd-a238227f3d70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 154,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 1509,
                "y": 158
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "36ab6d03-cb5a-4200-89b4-af0a298033d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 154,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 1554,
                "y": 158
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "7e14c181-9068-48d9-b71d-db0b0f30fabd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 154,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 1644,
                "y": 158
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "e21ae8be-938b-48db-8495-53a624e78236",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 154,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 155,
                "y": 314
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "7a62cae0-f3c3-4266-b69f-df91d76e12f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 154,
                "offset": -2,
                "shift": 48,
                "w": 52,
                "x": 1689,
                "y": 158
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "502c47fa-0c0d-4c2d-a596-d369d92cf73f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 154,
                "offset": 7,
                "shift": 52,
                "w": 43,
                "x": 1743,
                "y": 158
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "c544277b-edfa-46f1-a2b6-36b1551e522f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 154,
                "offset": 2,
                "shift": 92,
                "w": 89,
                "x": 1788,
                "y": 158
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "362f6c37-8e84-4d07-927c-0b39a2e2b837",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 154,
                "offset": 2,
                "shift": 53,
                "w": 49,
                "x": 1879,
                "y": 158
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "de8456a0-fa7f-4fc2-887c-8b62d44717b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 154,
                "offset": 2,
                "shift": 53,
                "w": 49,
                "x": 1930,
                "y": 158
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "bf766196-a26c-4783-8eb7-d81989ce5cea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 154,
                "offset": 2,
                "shift": 53,
                "w": 49,
                "x": 1981,
                "y": 158
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "168d69f5-6b2e-4fc5-9d73-20a9ca5f6086",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 154,
                "offset": 2,
                "shift": 53,
                "w": 49,
                "x": 2,
                "y": 314
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "d94f5d44-e0c7-4974-b5bb-104dd23a0947",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 154,
                "offset": 2,
                "shift": 53,
                "w": 49,
                "x": 53,
                "y": 314
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "49c7f2e5-9ba2-4f1a-bf94-fc50de14be11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 154,
                "offset": 2,
                "shift": 53,
                "w": 49,
                "x": 104,
                "y": 314
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "32eb53b2-57ca-42a7-aca6-8057d516f820",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 154,
                "offset": 1,
                "shift": 78,
                "w": 74,
                "x": 1099,
                "y": 158
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "29f14fa6-cdf5-4112-ac68-4975be706a7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 154,
                "offset": 5,
                "shift": 52,
                "w": 43,
                "x": 1599,
                "y": 158
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "f58beafc-fb76-4bc3-9ddc-81c433bac47b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 154,
                "offset": 7,
                "shift": 50,
                "w": 39,
                "x": 1058,
                "y": 158
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "a91810b9-1906-4b7b-bfdb-f99aeb7f03c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 154,
                "offset": 7,
                "shift": 50,
                "w": 39,
                "x": 443,
                "y": 158
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "b6c2be31-fa8e-4a1b-866c-09f83d4369f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 154,
                "offset": 7,
                "shift": 50,
                "w": 39,
                "x": 100,
                "y": 158
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "bf035aa8-df25-41dd-9a94-d986a44c35d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 154,
                "offset": 7,
                "shift": 50,
                "w": 39,
                "x": 141,
                "y": 158
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "8ff0468c-e883-4734-90e0-559e1f9bcd96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 154,
                "offset": -1,
                "shift": 28,
                "w": 22,
                "x": 182,
                "y": 158
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "87b63b5b-e348-45ad-8fc2-a1057c79e4cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 154,
                "offset": 7,
                "shift": 28,
                "w": 22,
                "x": 206,
                "y": 158
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "5188d91a-6708-4c81-a970-21b5dfe6bb4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 154,
                "offset": -2,
                "shift": 28,
                "w": 32,
                "x": 230,
                "y": 158
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "199e4c91-00c1-4f37-bb94-c525b24ef625",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 154,
                "offset": -2,
                "shift": 28,
                "w": 32,
                "x": 264,
                "y": 158
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "74a8b2b3-4204-4547-bb66-81b87a06a9d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 154,
                "offset": 2,
                "shift": 56,
                "w": 49,
                "x": 298,
                "y": 158
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "868b7c5c-30ad-476c-acb7-0318e4a623e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 154,
                "offset": 7,
                "shift": 59,
                "w": 45,
                "x": 349,
                "y": 158
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "64553696-bfd3-4815-899e-8c38488bfbdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 154,
                "offset": 5,
                "shift": 56,
                "w": 45,
                "x": 396,
                "y": 158
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "0ae422d4-83ae-44e9-9253-dd50a0114b38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 154,
                "offset": 5,
                "shift": 56,
                "w": 45,
                "x": 484,
                "y": 158
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "5d63f11f-39bb-4da6-ab23-e3f718755575",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 154,
                "offset": 5,
                "shift": 56,
                "w": 45,
                "x": 960,
                "y": 158
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "9db51746-c5d7-4749-9836-698df3391c79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 154,
                "offset": 5,
                "shift": 56,
                "w": 45,
                "x": 531,
                "y": 158
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "4a8e9669-2080-4c87-a532-39ee679b31cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 154,
                "offset": 5,
                "shift": 56,
                "w": 45,
                "x": 578,
                "y": 158
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "c8b91ddf-68a6-47bf-8dc6-128f4defab2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 154,
                "offset": 2,
                "shift": 55,
                "w": 51,
                "x": 625,
                "y": 158
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "9020bd52-d28d-458d-99b0-5f5a170368ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 154,
                "offset": 5,
                "shift": 56,
                "w": 46,
                "x": 678,
                "y": 158
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "f7819f27-df67-49c8-969d-39620872f13c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 154,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 726,
                "y": 158
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "a93a3a9a-bb9e-4572-b51a-01fb439c47a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 154,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 771,
                "y": 158
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "819529c7-2ee0-4f6b-8b9b-2abf62057058",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 154,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 816,
                "y": 158
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "56be4780-62ce-46b6-b792-6b4db8bf1def",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 154,
                "offset": 6,
                "shift": 55,
                "w": 43,
                "x": 861,
                "y": 158
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "352fe70d-85f1-48e2-867d-8b85e9c1e2e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 154,
                "offset": -2,
                "shift": 48,
                "w": 52,
                "x": 906,
                "y": 158
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "5eec83b3-1815-4b4d-b494-23baf729cc55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 154,
                "offset": 7,
                "shift": 52,
                "w": 43,
                "x": 200,
                "y": 314
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "e2e3f8c2-2802-4434-a77f-dcc47c461347",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 154,
                "offset": -2,
                "shift": 48,
                "w": 52,
                "x": 352,
                "y": 626
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 96,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}