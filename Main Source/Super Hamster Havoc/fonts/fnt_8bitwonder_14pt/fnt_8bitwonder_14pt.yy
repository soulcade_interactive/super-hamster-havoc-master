{
    "id": "58c41242-53f3-4601-8700-44efa60301df",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_8bitwonder_14pt",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "8BIT WONDER",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "526ffcc6-f338-47dd-ada8-7d42fa166a56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 14,
                "y": 107
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "81313e86-ce4c-4c62-ab43-21fc50fbd634",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 5,
                "shift": 11,
                "w": 3,
                "x": 221,
                "y": 107
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "c1a39969-70bd-48ce-bf67-8bd6c46aeac4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 3,
                "shift": 11,
                "w": 7,
                "x": 173,
                "y": 107
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "54227943-dffc-46b0-a642-a4bce55321f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "5c7ac1f8-9636-4b78-9aa6-3aff7b8c3f60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 36,
                "y": 107
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "9d6c2aaf-dfd7-42c9-86a3-074ca238d396",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 161,
                "y": 86
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "179059ab-e178-486b-be57-b3ea2e0ece11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 12,
                "x": 189,
                "y": 86
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "0dcb8d7f-6995-472e-8404-0b7c5b851269",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 5,
                "shift": 11,
                "w": 3,
                "x": 216,
                "y": 107
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "1ce61491-ba93-4433-8098-1361a50d0cba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 215,
                "y": 86
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "82c2b88d-aa57-4792-99b4-e4a4eb61844d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 227,
                "y": 86
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "27be1fe9-1b97-438e-85fb-8ae0f125faf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "920e4b3e-c2d5-41c9-a696-3f935d17d760",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 47,
                "y": 107
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "22b71c7e-a9ac-403d-a3e5-1c1a9bbf57fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 4,
                "shift": 11,
                "w": 4,
                "x": 204,
                "y": 107
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a0d2f950-c7c4-4b3f-8b13-40010c7e08f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 3,
                "shift": 11,
                "w": 7,
                "x": 164,
                "y": 107
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "4b9c95cb-3235-442f-a0ce-7772e802c153",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 4,
                "shift": 11,
                "w": 4,
                "x": 198,
                "y": 107
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6745e43a-2d9f-4084-b100-9ece8329cbf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 91,
                "y": 107
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "261b4e4b-a40e-41da-a357-ca07a7fa839e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 110,
                "y": 65
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "08244d43-830f-47bb-93e4-0543a0b466b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": -2,
                "shift": 9,
                "w": 9,
                "x": 58,
                "y": 107
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "b00f4482-03a2-478e-9f47-7390253a487f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 146,
                "y": 65
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d703095f-73b9-48b3-8430-4fd7b5a95fd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 110,
                "y": 86
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "fcc772cb-4aaf-46cd-8776-274087ca61ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 182,
                "y": 65
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "4342439e-4209-4721-9ab1-f7d8f490534c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 200,
                "y": 65
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1e90c9aa-b6b4-45f0-b73f-fe3f152bea61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 218,
                "y": 65
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "6ff29544-0012-490a-9105-59c09512ac1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 236,
                "y": 65
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "750224b3-aac2-4bfd-a7e9-181c33099ecc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "21b84a14-0b57-4ec6-a01c-0a83679926bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 20,
                "y": 86
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "255e8fa9-d6f6-4182-934b-e01c10490c57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 5,
                "shift": 11,
                "w": 3,
                "x": 226,
                "y": 107
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "935caed8-2e36-4a2e-af5c-1a486ad9edf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 4,
                "shift": 11,
                "w": 4,
                "x": 210,
                "y": 107
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "890d0aa7-e3b4-42db-a5fb-a1881b5db2be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 69,
                "y": 107
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9ddde679-0372-45d9-80c3-a7226d170ff0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 80,
                "y": 107
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "61081d4d-92b0-44fb-81fa-b0a89c522cb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 113,
                "y": 107
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "2276634f-494b-49ce-8337-bff28b160631",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 102,
                "y": 107
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "deccbcc6-b810-4975-b398-5883316f0493",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 175,
                "y": 86
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "816e9b5f-c00f-4976-bccd-33483471d8d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 74,
                "y": 65
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "7caff0f0-81f5-4480-9268-8a1a54343430",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 164,
                "y": 44
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "e76b963f-c116-4f8f-bb12-c16f8bfc7cfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 146,
                "y": 23
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "60bf0bf3-f372-40db-b72d-e70537d117f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 74,
                "y": 23
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "bb4ef244-a5db-4dc6-9fc7-071312e0dd3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 56,
                "y": 23
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "490429a8-25d3-4465-92a1-babc455a07be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 38,
                "y": 23
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "251a9b6b-9d12-4ebf-ba4f-601706821a47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 20,
                "y": 23
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "55d54014-281b-47b3-b171-44111fb9d0ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1a89aa91-6395-42a3-a9b0-d4dd336a5cf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 155,
                "y": 107
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2addf6ce-bc4d-4f71-87d2-4029ae3aea87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "ce736a3a-c5f6-46ff-8e2d-415dbc6c3031",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "b813e7df-a7ed-4e58-adc2-94d0417dc50d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "a7edc768-1ce1-449c-9eb7-81668babc6f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 0,
                "shift": 28,
                "w": 25,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "cc7c17b9-b9a1-4660-bfec-0ea5ac497ca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "482a6c83-2cb5-41c5-903a-f3c54aa70447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c3390b51-e265-46a8-9632-59a849188f10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 110,
                "y": 23
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "0aefdd83-19ea-423d-b43d-6bfa0cbc8c88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 128,
                "y": 44
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "27467951-2cd8-4dfc-bbff-40b67a200ca0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 128,
                "y": 23
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8bef91c8-fc20-4fe3-8bb8-4fc0d67a51a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 164,
                "y": 23
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1e227919-8778-4888-9f46-afe443455338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 182,
                "y": 23
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "91a171a6-d685-4ec5-ae8b-df38e6177ee6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 200,
                "y": 23
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "7f329b69-6c65-48d3-b40c-20bb5bcac912",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 218,
                "y": 23
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "3f8a9b32-d2b2-4a28-ab06-6382a15bd60a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 28,
                "w": 25,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "31da12d1-81bf-410f-a4b4-f5286a4e40f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 236,
                "y": 23
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "9785b41e-8752-47e6-acba-e31b9b7d1c10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f0a3150c-a0bd-4ab9-8ea8-826a8db43534",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 20,
                "y": 44
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "927590a0-7672-41dc-93af-4bb277ba82f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 2,
                "y": 107
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c413fb08-a148-405a-9609-0135ea338cb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 25,
                "y": 107
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e4a3b122-2e1d-4136-8523-9963316368ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 203,
                "y": 86
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "5fc479e3-cf5d-4554-ae0d-58261f48e187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 239,
                "y": 86
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "09f67dc1-aa48-42af-875d-65a17783179a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 13,
                "x": 146,
                "y": 86
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "19757864-8f24-40b9-b56a-1717b464a5b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 4,
                "shift": 11,
                "w": 5,
                "x": 191,
                "y": 107
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f714129d-7360-4f75-85f3-4b9dff02a12d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 74,
                "y": 44
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "71b2d18d-d447-4b79-a335-64cecf3378a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 92,
                "y": 44
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "eb3ba8b8-3310-4347-a58e-632d9576d0df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 110,
                "y": 44
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "135682b8-a5b9-44bb-aec9-df04bcf716d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 146,
                "y": 44
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9007a644-4b49-4f2f-bf63-ff5a336951e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 164,
                "y": 65
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "cf5e6d4b-042c-437b-80b3-814a78aaff91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 56,
                "y": 44
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "bfd7e29e-0070-4405-8aec-e655e3a6be32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 38,
                "y": 44
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "76e799ef-d2fa-4d50-9005-57b5abdc1b97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "64bbf47f-961c-4168-88ef-6ecf59925813",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 182,
                "y": 107
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3bdee2bd-5192-4359-b71b-472b5ebec37f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 92,
                "y": 23
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "557c96cc-9c12-49d1-baab-d9df09bc2720",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 74,
                "y": 86
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "fdfb2290-2d41-49b0-9ea7-92cebf4d124f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 92,
                "y": 86
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "9cb13e4d-17c5-4caf-aa2b-6a6c7c220bb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 0,
                "shift": 28,
                "w": 25,
                "x": 29,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "106dd48c-4d49-47db-b826-13920a905d5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 128,
                "y": 86
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "9b1ce087-0873-4f19-accf-935859f57f24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 56,
                "y": 86
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "2624ac72-d341-41ae-9a64-35ebc5bb0b4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 38,
                "y": 86
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "23ac9057-19c6-4e11-aaf9-e59529250f1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 128,
                "y": 65
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "975cbdf0-a0e3-4ad0-b810-a0ad5c4bc9be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 92,
                "y": 65
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "938db066-d9f0-487e-9f62-deef110fc33f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 182,
                "y": 44
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3683e8f8-263c-49fe-baf8-6e9208c48d75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 56,
                "y": 65
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b24f2050-0045-4871-bfb9-8bb42fb9301f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 38,
                "y": 65
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "041d4f57-f729-41f3-a148-891475ccae44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 20,
                "y": 65
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ead4d9ff-63a3-4eb8-bd19-370409bd6c36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 28,
                "w": 25,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "77646c4b-0915-4ec3-b293-e03f1dd084ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 236,
                "y": 44
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "93e73638-0198-434b-a993-b2567e894093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 218,
                "y": 44
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "6c8d0dcc-5bd7-4afc-a4d8-6241cb248da7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 0,
                "shift": 19,
                "w": 16,
                "x": 200,
                "y": 44
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "77c2ce29-7341-44d2-8829-83d7ae49a268",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 145,
                "y": 107
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "dded0c24-7dfc-4280-8eee-52c96106986b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 5,
                "shift": 11,
                "w": 2,
                "x": 231,
                "y": 107
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c2a540bf-79e5-4119-8a3e-0f9945c3c4e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 135,
                "y": 107
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "8a60b7c0-206f-4ee6-b406-c3496f5bc0ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 124,
                "y": 107
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 14,
    "styleName": "Nominal",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}