{
    "id": "018a3934-dd27-4ada-8e8f-3583a8c80433",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_bebas_kai_10_pt",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bebas Kai",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "48d11798-3526-45b1-91b1-048b6785636a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "f2f2e347-78f8-488e-87a1-685579c1a887",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 163,
                "y": 59
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "1196398f-3c5a-4f26-ae7c-a4e314d1ded1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 168,
                "y": 59
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "c853188d-74b1-498a-8be2-0a200cb3a01d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 175,
                "y": 59
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "5023d984-10be-49bc-8887-c44c91948675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 184,
                "y": 59
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "730db378-73f5-4492-b6fd-c42e09832157",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 192,
                "y": 59
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b280ccc0-15c1-4df3-9100-c0a3c8fea1b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 202,
                "y": 59
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "915e7f19-6370-4779-b973-6a4fea8e80b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 211,
                "y": 59
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "43e6fff5-afce-4893-bcfe-31403f9ba595",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 216,
                "y": 59
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "eae53321-5481-47d1-9275-125dbe723a0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 222,
                "y": 59
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "871bd661-04f4-4d37-9e6e-e08ff4161aea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 236,
                "y": 59
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "49e3f809-a60f-4a75-b3d9-db62529d9f26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 78
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6f46b758-f3c5-46f3-87a2-b0f9a1b2431e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 243,
                "y": 59
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "27140cad-f9f5-4c9e-bad2-448698b7a4de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 248,
                "y": 59
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "9a655cc5-92ef-4693-9c02-9484ea9724dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "5620a4cd-5602-4d94-8b66-fc89ff800383",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 7,
                "y": 78
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "4a79cc43-7f89-4a65-b36c-97adccfbc832",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 14,
                "y": 78
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c051cbc9-6f1b-48c7-b7f7-22e8da888efc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 22,
                "y": 78
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d191f203-5f7f-4c1b-a963-5929655ce241",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 28,
                "y": 78
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d876c0f3-b46b-4b18-9b4e-6c185777de2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 35,
                "y": 78
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ad213f9b-728b-4a5f-8dc0-0cf1a075e137",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 42,
                "y": 78
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "8e1270f9-6780-4c42-9a7f-752b7b1b2f9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 156,
                "y": 59
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "5c23175c-dfdd-4b30-8cad-b9516fdd6943",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 228,
                "y": 59
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "b482b80c-4cb3-4517-8a8f-e46af55a7da0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 149,
                "y": 59
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "15d6721f-ff61-48db-96b4-deaf387209c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 53,
                "y": 59
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "84be8291-5e10-4878-880e-9103644e1953",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 232,
                "y": 40
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b3772da4-3077-4f0b-9cff-8dfbe63d79cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 240,
                "y": 40
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e0c120e6-f657-4e68-8a31-49e4da9c6c3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 245,
                "y": 40
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "6bc74425-3edd-4ab2-bfff-007be4967fa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "90ecba5c-7765-460f-9a20-715f7cf2b143",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 10,
                "y": 59
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d5e8123b-755b-41c5-8887-204018f71708",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 18,
                "y": 59
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "612e0fec-a892-4454-a90c-b2919c6f9d2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 26,
                "y": 59
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "179ee76e-b721-4f28-96b5-09213b9b100e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 33,
                "y": 59
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "fa3a391f-864f-44b9-b476-459fbbf4832c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 45,
                "y": 59
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "9870df03-4c74-4ef8-a3ae-2e76eae27bbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 61,
                "y": 59
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "943e38a5-3078-47c4-b000-909622cd7cac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 133,
                "y": 59
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "1113e328-4965-45be-aafa-f444e81252d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 69,
                "y": 59
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "7987b9f5-f7b7-496f-befc-f2f7a8433554",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 77,
                "y": 59
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "11feb5b5-5bf1-4926-9a05-d3e9dcd24fa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 84,
                "y": 59
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "93ea3853-1e33-4761-a5bd-eb20e60aec47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 91,
                "y": 59
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "237da73d-c87a-4eec-9f2d-3288c19f8b12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 99,
                "y": 59
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c17d04e8-ad89-4a73-92b9-ecf240aca596",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 107,
                "y": 59
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b19615b0-0076-4cfa-a205-3a1aa36a853d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 112,
                "y": 59
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "4cb35ee8-1f98-4075-a33d-511d40d5eb40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 118,
                "y": 59
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "30644e3b-1a0a-46c3-8998-34dab2ed5a18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 126,
                "y": 59
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "2da4b5fc-3065-4866-8d0c-f92930ff0dd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 140,
                "y": 59
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c74109d3-ef29-4bd0-b09b-673a2fb72207",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 66,
                "y": 78
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "171bf56e-cbbf-4d5d-b215-7c3a4281a645",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 239,
                "y": 78
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d0e4d60d-bb97-4cba-8273-2c3912d57f2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 74,
                "y": 78
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2ec13681-f94c-41b5-8aca-e41863e55943",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 9,
                "y": 97
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9e27f455-d6ea-4dec-b149-b1332295121c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 17,
                "y": 97
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "011c58d3-5a32-4845-b6bd-d66b1389a7a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 25,
                "y": 97
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "858a3bfe-5fac-498f-a6ac-f6bc424ce947",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 32,
                "y": 97
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "62fc7823-d8d2-440c-9d26-9a8c7fd0cda5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 39,
                "y": 97
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a71d8eac-5d89-4dc4-a809-fcdf89db1f65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 47,
                "y": 97
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b2f2d9e8-c526-435f-90af-6500bdd1e254",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 55,
                "y": 97
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "14e71625-2ebb-4ef4-944b-3f1823953208",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 65,
                "y": 97
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d9af6b81-4da9-422c-a547-5ddd0eba4a92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 73,
                "y": 97
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "115c04c3-148a-4ed6-803a-d52abd71106b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 89,
                "y": 97
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "2cdb9b2c-ba69-473e-8ce5-dec3501f6a7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 161,
                "y": 97
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "56995fd8-fdb8-46e3-8910-b24a648f006e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 96,
                "y": 97
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "6a6cac6e-7ba1-4c1c-be2d-09f58ffe2ffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 103,
                "y": 97
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "c908c0c4-3655-4977-8ce7-5a75883df12b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 109,
                "y": 97
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "9ef78b36-819a-4164-a71b-dec2fd654bb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 117,
                "y": 97
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4e511276-32c1-4d64-b03d-358b76d302b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 125,
                "y": 97
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "11f76292-2e22-43be-bd1b-384e116894c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 130,
                "y": 97
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d0d89d52-4ec0-4b47-ad91-3d03fc5fbeb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 138,
                "y": 97
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7dc68fc7-4816-489d-9c0d-bc08fcec6b53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 146,
                "y": 97
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "59c432ae-c8c1-425e-aaa6-9973d7343d75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 153,
                "y": 97
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "5e25dcc9-18fe-4bd7-8690-65c7e9abe46e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 97
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "8e991219-67c8-4d46-83d4-b8f4f83bf044",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 82,
                "y": 97
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "01f3db5d-f7cc-4a50-9382-41276cffcf4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 247,
                "y": 78
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a95b99f8-b794-44d9-af9d-0ca96b0ef2e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 149,
                "y": 78
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "f0f84a02-617e-43e9-90a2-ed220778e61f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 82,
                "y": 78
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "654c9924-fcd1-4d64-ba82-e466327d0f69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 87,
                "y": 78
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b002a704-ab2a-444b-8e92-c20a56fafe46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 93,
                "y": 78
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b019871a-111d-465b-bbae-6ab877f21d63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 101,
                "y": 78
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6e01dce2-9088-471c-8c17-501521c7e646",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 108,
                "y": 78
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b2f314ca-7f6a-47f8-aa95-b47390ea89d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 117,
                "y": 78
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3a15587b-7d08-49ce-a9a6-f6be7ba76c0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 125,
                "y": 78
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "ac7d8449-835b-452c-a932-eccf45c71d90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 133,
                "y": 78
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "bab8e547-54ca-4d02-ae0c-c6d7dd4da655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 141,
                "y": 78
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "23b21008-3638-4d50-9dd2-acdb68819af4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 157,
                "y": 78
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d32423b0-d235-4391-b442-a962b5d925bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 232,
                "y": 78
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "73c4806b-a692-4753-a6d6-de2a2f7973d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 165,
                "y": 78
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f05c4c59-1551-44c6-bd76-fd098bf742c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 172,
                "y": 78
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "26ffddb5-abec-4a03-836e-d527011611bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 180,
                "y": 78
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "4567fcbc-0d82-4d9f-8d39-6989b811b49d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 188,
                "y": 78
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c63a0cdc-f0d6-4bf4-a838-f03e58df1230",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 198,
                "y": 78
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "51b1b704-3161-4ae9-bb41-92ae00d3232d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 206,
                "y": 78
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "654bb0f5-3a38-41d4-a454-b7e174a4753e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 215,
                "y": 78
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "827dd0f7-2278-4425-a94a-c4b92d2d6d37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 222,
                "y": 78
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "b75adf06-a244-4dc4-9068-352b9d2af495",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 17,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 228,
                "y": 78
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "eebb57bf-379a-4a75-a30b-133cab5571be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 226,
                "y": 40
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b2bd8f05-5256-4473-a1ba-36bea8783d42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 58,
                "y": 78
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "03b5738f-2462-4e12-9e48-34e7b0dbe93b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 0,
                "x": 224,
                "y": 40
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "cad76d0a-2dd3-438d-a06d-1fa7511aa947",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 85,
                "y": 21
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "170c3b53-83d2-4975-a64d-5948092dfa66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "5be61f17-d393-4412-9d7a-15f7ec97d5ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "1ac30ba1-9821-42f4-b5bb-a05c346c090d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 202,
                "y": 2
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "56e3afac-bd53-4d18-99ab-2c89dc8bc9bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "52809f3b-4bf2-4cd1-b675-f7d8b154d22f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 17,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "4d989dfc-61ea-42e3-b4e2-aec86ebb0d74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "eda93223-3929-405a-9579-d562431ff06d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "0e64d984-1e1e-49f2-8cf9-c77b7c415bf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "7a75ccc2-5157-45a6-9f8d-14a1886b00a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 247,
                "y": 2
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "1617e13c-ca9b-443e-a0d0-2e5336400094",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 7,
                "y": 21
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "2534e23c-0c5f-41f5-8cbe-0020f3e97834",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 77,
                "y": 21
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "e505526e-3499-410a-9307-ddb01e37190c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 15,
                "y": 21
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "354707ac-fec3-48b2-b3ae-c6e6b8e9aa1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 21,
                "y": 21
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "bff28ba9-944f-4bb6-a20e-b58a238aedb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 33,
                "y": 21
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "2f5c9cbd-ce97-4552-81de-68c908123c67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 38,
                "y": 21
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "662e17ee-fb3e-4416-8290-55c3ca60bed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 44,
                "y": 21
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "7cac358e-a864-4d56-8144-ba3185507b4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 52,
                "y": 21
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "bc7f2a14-71b7-4326-b1e4-231587f55435",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 57,
                "y": 21
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "0fcd0187-86bb-4d8b-afc6-04f0f5504851",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 63,
                "y": 21
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "92df966b-cc6f-418f-9a6a-2744ac332508",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 69,
                "y": 21
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "552ef260-9d2e-47ce-ae49-3fa970d960a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "b68bbe6f-e8b0-429d-bb7b-691e5d0a33e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 21
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "707fa3bb-4e32-400c-8591-01922c15ec5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "196f88e0-2025-4c71-94da-931e71061741",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "d315433b-d5c1-44d4-8143-d1191e5bcfb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "ead2e0cd-02fc-4ad8-951e-d240804f2973",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "e71358a0-55e1-4aaa-87de-82618fa2b966",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "386858a7-aa71-4fcb-8eb5-ca270a29a0c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "120ca530-4d72-457c-b6af-06525c003b19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "33eed35e-6b9f-407e-826e-ea2b185b5404",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "9d4db20f-0c90-492f-8168-33b3cb646a53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 57,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "12742467-ff33-4517-861b-e6d662e019cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "75db54e2-fa68-4b50-bbd5-f527e771beb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "c688889d-c9bb-45e1-9873-d1d7046beec3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "c59fad44-b87d-4e01-a4a7-72cfa0a0bc1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 158,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "9ef55120-8110-4c7e-88f1-ed719be7b3c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "7278d344-e2dd-4fb2-a9d6-43102cbed6eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "ee480915-157e-4e2b-bce7-625c945b5789",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "e4442a10-f3a0-4e03-ba4f-028bd9581681",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "224e5f66-31f9-44cc-99e3-7e4be64e7bd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "3360df9b-ff90-4dc2-b1fe-3b556e5f04d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "3b5fb750-25e4-4005-a46d-feba111f69fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "7f2a1db3-a5ba-4861-a3c1-e45fcb567798",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 17,
                "offset": -1,
                "shift": 3,
                "w": 4,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "826c1055-481a-49d8-8a81-b859dbe02d4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "32a6425c-ab64-491c-b94c-19d10294b80a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 17,
                "offset": -1,
                "shift": 3,
                "w": 5,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "14415317-02f8-48a0-9ff3-3d752a263aa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 17,
                "offset": -1,
                "shift": 3,
                "w": 5,
                "x": 90,
                "y": 21
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "e167eb46-1afc-478e-8241-1241d4566799",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 19,
                "y": 40
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "4dfd2ec8-8c87-47e8-8154-96e0ca7a1dab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 97,
                "y": 21
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "fe281be3-c61a-440a-8157-ec321bc31c44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 44,
                "y": 40
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "437cdea9-1580-4e5d-9f9b-7e0ee1a1b31d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 52,
                "y": 40
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "43cc93a5-d2de-402e-b2b1-abc7239a8578",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 60,
                "y": 40
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "09905a88-17e7-48f2-b0d9-f6d5a14b9e58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 68,
                "y": 40
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "d29fa134-e4c0-42d2-bbb2-391679358bd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 76,
                "y": 40
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "fa6ed8d4-b494-43d9-b1ba-359afea7c899",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 84,
                "y": 40
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "f4a86a4b-ff6b-4999-93fe-0715536b3388",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 92,
                "y": 40
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "6351b539-e1f2-4045-828d-f3bbbf07d772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 100,
                "y": 40
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "b73824ce-52b9-438b-9421-e12d7d8e7763",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 108,
                "y": 40
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "ce6680da-49a5-44f7-8f75-b40053dc5e76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 123,
                "y": 40
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "8f86226e-fc9c-4316-862d-36dbb0a73f20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 208,
                "y": 40
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "93295dfb-c3be-4418-9e89-4c6ed2066a64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 17,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 131,
                "y": 40
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "5643947a-698e-4274-ace5-ea3418032e3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 140,
                "y": 40
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "33ceb0e4-fab0-4a58-8a9a-67d3e59a7e56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 148,
                "y": 40
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "9e5b2c4d-46f7-4d35-b570-c55bf89bd2bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 160,
                "y": 40
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "77cc4a51-a7de-48bf-8006-e6e7b9acd097",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 168,
                "y": 40
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "8472e18b-2d9e-4750-a125-294b17f41a06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 176,
                "y": 40
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "3ffd7c80-e27e-4912-b133-a76b74cc7138",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 184,
                "y": 40
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "23f7bd52-47a4-462c-a80b-5632dc022a18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 192,
                "y": 40
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "bcc6b5ff-d62a-40d5-8ec9-7b3abeaa6191",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 200,
                "y": 40
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "278eea74-0552-47bd-9c2c-a1f3232bea24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 34,
                "y": 40
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "d4c046ac-ee29-43af-a53c-ba81d94ffda1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 116,
                "y": 40
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "6d907ebc-5970-4a99-abd1-6ffc3ef14ae6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 27,
                "y": 40
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "835e5bcb-8f00-4f41-ac70-9051f5901611",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 168,
                "y": 21
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "a6a8ac7c-9606-402d-bade-caa7c978f273",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 105,
                "y": 21
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "7729c66a-74f5-4405-8ce7-14ea05c13a04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 112,
                "y": 21
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "09bd4037-a803-4907-a878-5ca46c9e5cb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 17,
                "offset": -1,
                "shift": 3,
                "w": 4,
                "x": 119,
                "y": 21
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "e3c86703-7358-4d67-b259-4663d1b8f800",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 125,
                "y": 21
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "a111a68f-c8c6-4a1e-912d-f8b76f5c168a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 17,
                "offset": -1,
                "shift": 3,
                "w": 5,
                "x": 130,
                "y": 21
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "5a0cb06b-0615-4f68-b113-8eb37ae43045",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 17,
                "offset": -1,
                "shift": 3,
                "w": 5,
                "x": 137,
                "y": 21
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "96d6cc08-2a3b-43f1-9a23-44c985900cc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 144,
                "y": 21
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "6c864480-891a-4218-b826-16c5059f0435",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 152,
                "y": 21
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "64122f72-3f42-4d6d-a384-947fadf61c34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 160,
                "y": 21
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "8cf27916-ce92-4415-83f6-781d74ab49de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 175,
                "y": 21
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "540dd52d-9a99-4581-9e02-e8f415f98455",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 11,
                "y": 40
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "322dddf9-6aba-464a-bd66-2b999d2ae799",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 183,
                "y": 21
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "be0b2058-7f4a-473e-b01d-5047a6e58fc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 191,
                "y": 21
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "3f5ca33b-32af-4452-9a3e-ddbfd0b20131",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 199,
                "y": 21
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "7118d690-1bde-458f-a6ef-a3490a41dcea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 207,
                "y": 21
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "82ad1923-96a9-44d3-bd34-3caacd0129f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 215,
                "y": 21
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "a803bf6a-4080-45b8-ba1c-23efeba1270d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 223,
                "y": 21
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "38c308d8-5d9d-4408-a788-75b84d3de0b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 231,
                "y": 21
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "d7158c69-a865-47d4-ba0a-aaba6b013ad8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 239,
                "y": 21
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "ae67a727-1105-4237-95b2-f697aa657c65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 17,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "812a3fb8-1449-4803-a8a6-abd5cc717625",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 216,
                "y": 40
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "92a3aef2-b7b1-4d30-a1b9-9b8f7a85b3fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 17,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 167,
                "y": 97
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        }
    ],
    "sampleText": "•",
    "size": 10,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}