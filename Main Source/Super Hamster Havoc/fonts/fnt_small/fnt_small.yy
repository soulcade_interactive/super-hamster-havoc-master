{
    "id": "ccb2b4c8-5e07-4be1-a1fa-15a8d9ac44cb",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_small",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "8BIT WONDER",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "4a441fe1-4cc2-4248-9b99-fe4cfb5c27e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 123,
                "y": 117
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "40454966-f903-480f-bb94-095c200fe400",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 166,
                "y": 140
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "917f482a-43c0-48e6-8715-88d4f4a10d7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 125,
                "y": 140
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "96c6dcdf-5060-41e8-b9d5-80de8c051f73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e09501f6-78ca-450c-b180-0240895e4710",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 15,
                "y": 140
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "9f66877f-fc1b-47fd-9358-3592f82ec8b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 93,
                "y": 117
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "46815869-46f7-4f47-96fb-cff06534b1d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 78,
                "y": 117
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e47704a1-6683-4e00-b644-8185b137a481",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 161,
                "y": 140
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e877baf0-1efc-4cc1-8922-3fb6a5e72eeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 149,
                "y": 117
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f3e077e8-14dc-40d7-9950-a7f5d3ec09d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 162,
                "y": 117
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "2f19f8f9-0398-4cc8-b614-4914aab26db7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 82,
                "y": 71
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "720427bc-6a88-46e7-acbc-4f43400a751d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 175,
                "y": 117
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "00a325de-ccb7-4d8c-bc8b-8f822b52996b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 4,
                "shift": 13,
                "w": 5,
                "x": 142,
                "y": 140
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "5f22b314-e3e0-46e8-bc3a-ffdd245e0b49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 2,
                "shift": 13,
                "w": 8,
                "x": 95,
                "y": 140
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "d7dc0551-8c9d-4862-a6df-4eea99ed9b3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 4,
                "shift": 13,
                "w": 4,
                "x": 155,
                "y": 140
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "114e1701-960b-4e23-a624-ec97a518c5d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 84,
                "y": 140
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "00f0fcd6-6b23-48ef-8544-dd6230259150",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "cd13fff8-4300-4332-b625-61ba99d48f79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": -2,
                "shift": 11,
                "w": 10,
                "x": 27,
                "y": 140
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "57edb91f-fd57-4fcd-80cb-08f711c05fef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 22,
                "y": 94
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "170d5575-a93a-45ef-8ba5-a508e8bcfdc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 22,
                "y": 117
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e402b5dd-733d-463a-b8ac-81929a94af25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 42,
                "y": 94
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "cc03b30d-7e21-41d3-b0f7-9cb89c1dad61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 62,
                "y": 94
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "12575f3d-82fc-4e42-9ec7-b1ac6684bfda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 82,
                "y": 94
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "43ace453-1557-4fe3-9631-2a50d8fc8b9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 102,
                "y": 94
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "cf44283d-5bfb-4f1b-9d07-46a66fc7a4bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 122,
                "y": 94
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "82a924ea-905e-4e8f-9ee0-c4ecb70a6092",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 142,
                "y": 94
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "6d095d04-265f-48fc-a5fe-b13d1356c81f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 176,
                "y": 140
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "fcccee3b-bdae-4cae-af1c-84625c525a8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 4,
                "shift": 13,
                "w": 4,
                "x": 149,
                "y": 140
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e1c09c1d-e3fd-4685-ae16-90f41a2c45c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 188,
                "y": 117
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "cee864c1-a8c2-4288-90ad-1b7ae86e4880",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 201,
                "y": 117
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d99bae1a-fefc-40ec-8ef3-1f6911968c67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 214,
                "y": 117
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "89a2b760-730b-42a8-b693-5da693a003c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 39,
                "y": 140
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d83ac96f-1da6-4179-a995-9f5614369aa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 108,
                "y": 117
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "3746ba03-6df0-4f88-83df-cb5546027b3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 202,
                "y": 71
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "ec2ff095-c171-42a6-9b63-3f09e1d060c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 202,
                "y": 48
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "08452be4-5af8-4417-9b1c-e2014bd5e941",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 162,
                "y": 25
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "dbf141f7-2c7b-4d13-b41e-6f62503b52e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 82,
                "y": 25
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "28848d69-d27c-47c7-bfcb-99af1a9c1dcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 62,
                "y": 25
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "756c0c9c-91ae-4c94-b64b-e4752e98d902",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 42,
                "y": 25
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f9c40167-1a69-488e-8363-43c1387ea25b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 22,
                "y": 25
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "71b7a0fc-6b41-4f24-95b6-4b27ce96bbdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "642b75b0-e860-4f39-ab72-a0cabf3e96b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 115,
                "y": 140
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5081d9fd-5cd6-4e29-8b92-1c72c3c63f77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "f6ae4b14-67e9-4744-96da-033b9180670d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "3e1cc563-0579-4a95-bbd1-bc33b6fe09ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "088c4012-e57a-4ce2-a543-71d968b8be51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 0,
                "shift": 32,
                "w": 29,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "97922de1-7888-470c-86af-e2c2bc781ad6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "1d56e2b6-1780-4f02-9c34-2c016e5220b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "482055bb-60a5-4bc3-9517-c4ca73b90f3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 182,
                "y": 48
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8f8fec8b-ae56-4854-aa45-37bca74f0b1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 142,
                "y": 25
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9d64ab3a-f4c3-47c3-981c-ade334873027",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 182,
                "y": 25
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "09899e8e-ed9a-45e4-90d5-cf1089968d54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 202,
                "y": 25
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "a424617b-9d32-4d92-9aae-2aba86bfd078",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 222,
                "y": 25
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "db8ed401-7679-4fc6-ba18-838355d1fe03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "1d68d9b8-9797-4340-a14a-517be17cfe46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 22,
                "y": 48
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "e41eaed3-f12a-4dcf-b4b6-4093cb0be4de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 0,
                "shift": 32,
                "w": 29,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "68b7cf67-9516-46cc-9c26-d35a3bb1cc31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 42,
                "y": 48
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "58a825e7-7d54-4c57-8f53-c39f0fdbff65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 62,
                "y": 48
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "51be4813-8467-4eb8-82a1-f7814acf2217",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 82,
                "y": 48
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ec45e047-21f7-448c-a544-fd857b75112a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 240,
                "y": 117
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f8321617-66a7-4ed4-9d9d-ca80b85ebdb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 51,
                "y": 140
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e4cc09ad-522b-4e06-b56c-c0ec9cbd9bcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 140
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d863551f-7d85-4367-ba25-47e0dfff42f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 136,
                "y": 117
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "55250078-a960-440c-a445-b6024761842e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": -1,
                "shift": 13,
                "w": 14,
                "x": 62,
                "y": 117
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5a44cf92-a69f-4cc7-afc9-3711f7ff5488",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 4,
                "shift": 13,
                "w": 5,
                "x": 135,
                "y": 140
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "29c54365-34ef-4a7d-b0a8-647094952b6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 222,
                "y": 48
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "9c57e2e8-22ee-44d9-a92d-f92f497bbe87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 162,
                "y": 48
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "7b205c4d-7d12-4537-9d44-ecce8c0f8483",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 142,
                "y": 48
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "91bdda7f-4673-481a-a596-f019e84b7b53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 122,
                "y": 48
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "d4a28df2-d48f-47e1-a0b2-577a74c7e7c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 102,
                "y": 48
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "68538233-6bdb-4b29-98e9-246d568994bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 122,
                "y": 25
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "13091f26-14a4-4954-a3ef-988ebb4e64f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 102,
                "y": 25
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "14691bd6-f443-4df3-9213-079b2550a4f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 2,
                "y": 117
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "8b291cc2-0c04-4808-821f-f7bd78591541",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 105,
                "y": 140
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "3855b2eb-5268-4786-a271-c167fa653663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 222,
                "y": 94
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "04e60432-900a-4767-98e9-252ff23c3806",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 202,
                "y": 94
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d4ccc4af-02d8-45be-9110-dca86fe83b12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 182,
                "y": 94
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "25249f5f-5b00-405f-a975-6ab150ef8ee9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 0,
                "shift": 32,
                "w": 29,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "490d6cbf-c2e1-40fd-a808-dbe3ac1b6633",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 162,
                "y": 94
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "a50094e8-b133-41c7-b554-ce193a39572b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 222,
                "y": 71
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c8efecce-3cc1-4147-8c57-3a2e7322d6d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 182,
                "y": 71
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "974912f5-91d4-439c-ba6f-9a53bb1ce6c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 162,
                "y": 71
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "bdab7752-cf02-41ce-acd6-f5010ce6c612",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 142,
                "y": 71
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "999c49ab-177d-4a94-8c4c-bd7568240f66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 122,
                "y": 71
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "003c1352-b495-443e-a6c4-beeaf6045f8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 102,
                "y": 71
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d205a5c9-ce3c-401e-b771-5d72467febc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 62,
                "y": 71
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c1e24118-f196-491e-9d54-d339acd964bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 42,
                "y": 71
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "e633582b-4df1-45dd-bddf-d89476e7c3fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 0,
                "shift": 32,
                "w": 29,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "651a83fb-7212-46e5-a828-a41e5854b46d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 42,
                "y": 117
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "9c0c81e7-e846-4327-9b41-f1afedc4d475",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 2,
                "y": 94
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "60fd4ec1-fd77-454f-8a42-a827c010e34b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 22,
                "y": 71
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "abea08c0-0d7d-4f38-aef7-6616e99b1dfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 73,
                "y": 140
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "2049da4d-19ce-44f0-9545-f70c64ef4f0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 171,
                "y": 140
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "50b0299e-c725-4065-8400-b0ab39b95c2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 62,
                "y": 140
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "5cb68476-2dbb-4a54-a202-05217de8c640",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 227,
                "y": 117
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 16,
    "styleName": "Nominal",
    "textureGroupId": "00000000-0000-0000-0000-000000000000"
}