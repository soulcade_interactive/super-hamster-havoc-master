{
    "id": "f9cced57-82f4-4be9-aa22-086004618351",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_8bitwonder_8pt",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "8BIT WONDER",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c51a3c6c-e7a6-49d2-954f-e5a4c0749871",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 179,
                "y": 47
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "f012fb8a-15a4-4327-a2cb-b7b996e61d5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 134,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a5a84abd-5da3-4922-bbce-e86f70412bf1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 13,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 91,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "83f9d880-cb01-4045-9dca-742ed2bb1b90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a70d616d-d81e-43f9-9ce5-a366927dac57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 45,
                "y": 62
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "087ebcaf-178d-4b51-a2e8-a9cdd419e388",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 149,
                "y": 47
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "c546179f-535a-4763-a0fb-93de5d9f4e35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 139,
                "y": 47
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9d72c317-2551-45e1-9206-e954ca23ce02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 130,
                "y": 62
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "88b796bc-44b9-48eb-903c-14d718d268ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 197,
                "y": 47
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "86c95018-29d3-4661-97e6-60b16b7eb77c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 206,
                "y": 47
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "6b11745a-d832-48c7-bb56-bff611829572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 86,
                "y": 32
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "7d66a21e-f0f1-47bf-b1b9-6f28eb809711",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 215,
                "y": 47
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "3d6c5dfe-c192-4cb8-80e3-bb4b3057e986",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 111,
                "y": 62
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "e6ff2b67-c7f3-4ad1-ae0c-8ec74dac7205",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 13,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 77,
                "y": 62
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "ffb9af89-88ae-43a1-a129-6b25104be139",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 121,
                "y": 62
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "5b98cb36-6ceb-4aa6-83d0-e395d8fadc33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 61,
                "y": 62
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9bb2d7b5-85a7-4fd3-bdf7-9e6a6d8be9f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 170,
                "y": 32
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c537282d-274e-452e-98ca-58e303c5776f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 13,
                "offset": -1,
                "shift": 7,
                "w": 7,
                "x": 224,
                "y": 47
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "9054165c-fa47-4b68-b4a7-e67cad6b435b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 198,
                "y": 32
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c9a0ba95-545a-4109-a65a-48eb713710ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 100,
                "y": 47
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "02c4ed58-d3e1-4d0e-8b7d-f73ccc68de33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 86,
                "y": 47
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5ba1ff86-c7fe-4060-b443-81b31917bd2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 72,
                "y": 47
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "90d7fb54-b03f-49fe-9992-029b24cae06a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 58,
                "y": 47
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "69aeb70b-042b-4602-ada6-878579eee11a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 47
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "f55861f9-b15b-49c2-8ccb-49f2c6e7dd5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 114,
                "y": 47
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "9c07a4d4-4121-48a7-8c50-5d764ce4ac10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 47
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "eab44ff5-435d-4e75-98ee-d0cb5addb669",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 126,
                "y": 62
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "b76edd8a-3a06-4434-be80-2190e4b2a5d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 3,
                "x": 116,
                "y": 62
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "299a89a0-41a6-4733-adac-db2ff6d9b111",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 233,
                "y": 47
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b0e302fa-bed5-451d-8cfc-5b0920fc455c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 242,
                "y": 47
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "51046315-9288-4f53-bf8b-dee80188f7d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 20,
                "y": 62
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "2e97e0b6-97db-4640-8f4d-0b2f2c23aaba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 69,
                "y": 62
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "d4d22ac0-8b2f-4b11-9fd6-073070429485",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 169,
                "y": 47
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "8fbee7e8-5675-49ba-ad79-f754cae22663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 226,
                "y": 17
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "9eeec17f-3cc1-4f68-8124-ce0604002a8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 17
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "025316ae-3a26-4bdc-b0d2-b8d7d625c968",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "2ccadc10-a8bd-4e69-9e6e-d7cb22f80fe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "db83dec1-20c6-487c-8055-72f0d974878e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "5e5530de-57ca-44ce-90cc-f70badac62c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "9ba698f6-d838-4d08-a8f9-b0dc26697f13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "b70703a8-e9fb-4e29-88ac-5dc5903f981a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "9c82408d-a35f-4392-857e-4d677584956b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 84,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "0924b5a5-0e32-4190-88b3-d8b9cdf3967a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "3942b583-ddfc-48bc-9df5-87b416f9961d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "39e4ddc9-284b-4338-bbce-2be00a58d7c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e8e14a91-1fec-4346-b347-94179a4390d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 13,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 42,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "2e5a374f-4923-4b5b-975b-2d04f3b504af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 17
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "4e1dc0d0-298e-4d74-a366-2486a9559bea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d3e34829-1ced-4ea9-b15d-7e3e15c35f66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 17
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "db432db7-1562-4a20-b751-d4041b9b8add",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 17
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "a2633e66-080e-4a7e-88f6-ea1552174e0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 58,
                "y": 17
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d17b3e72-4546-483a-8ba1-8e51e4797978",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 72,
                "y": 17
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "417df9d9-3d12-4262-8536-9e8bbabc5f28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 86,
                "y": 17
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "73e7c108-a7e4-49e5-97c0-2d30177ac202",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 100,
                "y": 17
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f4b07cab-06bc-4ed7-87cd-dafa4cfb1eca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 114,
                "y": 17
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "62b29f05-cb43-45f5-b56d-b8b98167a92d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 13,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ebd486df-fc19-4cb3-9bbf-9e66bc492793",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 128,
                "y": 17
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "9faf062a-92af-4880-8e89-23a23a47421f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 142,
                "y": 17
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "7a267d2d-c3e1-4b8d-b858-7067b21404cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 156,
                "y": 17
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "4a649abb-37ca-4e70-970f-eb6b9e14d22d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 11,
                "y": 62
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "29409a8a-8ead-40ac-9d04-91743f788ed5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 53,
                "y": 62
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "31717419-1cca-4a60-ad4d-b075edd537ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 188,
                "y": 47
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "9936fb0d-73bd-46ba-8713-6cc08c8ed08a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 159,
                "y": 47
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "4087f132-ea56-4ebe-a05f-52ade1d42f11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 13,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 128,
                "y": 47
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "bc1b2dee-2649-4f6c-918b-ab93b4110aff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 4,
                "x": 105,
                "y": 62
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "cc7c3527-a534-4ff8-bc2d-0948dfb7f707",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 240,
                "y": 17
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "4171389b-8e27-4f66-b0ba-9662a44f9cb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 156,
                "y": 32
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "bd25bb7b-192c-446b-b3db-4e71df602a8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 198,
                "y": 17
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4e2c1ecc-3ee0-406a-8f7a-c65de8ac77ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 184,
                "y": 17
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8d82466d-85db-4aa2-99f1-772b401aa97b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 170,
                "y": 17
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "6e55f30b-24a9-4b74-812b-2cd7beaf0874",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 212,
                "y": 17
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "69a38194-c41f-40e9-8567-eed5f7528c56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "579824cf-76bd-4807-8cc9-5e910eae8ab8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 32
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "92fb387f-4f0b-4b77-8c65-90b713371669",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 13,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 98,
                "y": 62
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d2da4754-23eb-4808-8e0d-b69681015325",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 212,
                "y": 32
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ca6f2b7c-b251-4108-96de-85ade032a692",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 226,
                "y": 32
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "5878d6b0-b0e5-4f72-9d9d-16d5a516a7c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 240,
                "y": 32
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a2c88385-c1c2-42f0-8223-cec6ac36ab2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 13,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "6a58144b-926c-4b5c-acd6-0eec5b3b72de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 47
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c93a6266-55dd-474a-8a55-88aad3240841",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 47
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "557a350c-c9bc-472d-bdbc-f827cd334eb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 184,
                "y": 32
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "93f4f8a8-f9d2-4702-b782-7c788392f375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 142,
                "y": 32
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "7091c35e-15b4-472b-a340-c287b2f5d748",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 128,
                "y": 32
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "f51d2bb4-2543-4c8c-81e4-4f54bf1fd4f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 114,
                "y": 32
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "cd4940c2-162b-4650-aa80-6dac2696a922",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 100,
                "y": 32
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "7ffd7256-0920-489e-82dc-026b9d913292",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 72,
                "y": 32
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "e9a77cac-67c5-4e05-b1f6-36e5093aac17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 58,
                "y": 32
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "afbbbbd7-833d-40cd-8034-ee9068dc93bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 13,
                "offset": 0,
                "shift": 20,
                "w": 18,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8846d5da-2bb8-4234-a7b1-3ca41a102319",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 32
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "5be1d150-2505-4469-9454-257c96be51a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 32
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ef9d630d-5d36-4017-b396-020de39ef1b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 13,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 32
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "8957a53e-030a-46fa-bede-8217aa60ce90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 37,
                "y": 62
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "0787d057-34f7-4601-be5a-af7b1c67a852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 13,
                "offset": 3,
                "shift": 8,
                "w": 2,
                "x": 138,
                "y": 62
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "152b9b7e-6e64-476a-887f-09d137439e67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 29,
                "y": 62
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "cf01e2de-5dae-4457-ba81-318b9a1b6db7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 62
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 10,
    "styleName": "Nominal",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}