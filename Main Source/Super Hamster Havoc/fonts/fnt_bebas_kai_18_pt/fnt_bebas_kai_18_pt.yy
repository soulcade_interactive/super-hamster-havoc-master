{
    "id": "c165be52-bb86-4a70-a17b-36fcd5e30ac9",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_bebas_kai_18_pt",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bebas Kai",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e5b051d0-6225-4820-9a38-44a93ba8fcbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 29,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "c3934b55-6dec-4098-aec6-dd1755e9fd9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 29,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 294,
                "y": 64
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "fcda4847-7153-4809-8bc9-b906d68b96fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 300,
                "y": 64
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a77c255f-431a-44d9-af1a-1fcb2e1c04dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 309,
                "y": 64
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "0300f838-1973-4153-b763-64fc5ceaafdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 322,
                "y": 64
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "57e3f739-8a37-4fc1-a21a-23623c7e5236",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 29,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 333,
                "y": 64
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "8abd5fe1-f2fe-4223-b235-2ef5b1fc6cf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 349,
                "y": 64
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4ad04c1e-b3eb-4dcf-b6f1-c9de2c1f26de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 29,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 363,
                "y": 64
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2fa25cb1-e443-41ba-b2f3-0884e333ef32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 29,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 368,
                "y": 64
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "3d77150c-17a1-4ae0-b2f5-4029f6de06d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 376,
                "y": 64
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "1604659a-b40a-4ad1-90d0-c8424248ed7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 395,
                "y": 64
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a4f24151-3bf6-48db-95f9-1ee369a646c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 492,
                "y": 64
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "089e716b-b2a7-4903-a748-e508f02316d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 29,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 406,
                "y": 64
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "603f4494-0b6a-4c19-92f7-d5c0bc8765e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 412,
                "y": 64
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "a3d8a1af-c9bf-4241-bf01-bfc0b882f68e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 29,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 421,
                "y": 64
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "99d54919-5f69-425c-9669-ba1bd2e559c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 427,
                "y": 64
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3dccb1b3-19bd-47e3-8393-a89e80d0e275",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 438,
                "y": 64
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "2c1d58af-7c26-45ee-98eb-b461a1cc9e8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 449,
                "y": 64
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f23de9cd-987d-4804-a766-2a6495654ef1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 458,
                "y": 64
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "5439819c-0364-4fa6-a8dc-bd763aff0abf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 469,
                "y": 64
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f0951a5e-5c76-4336-aa34-cd34307972b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 480,
                "y": 64
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b34aef8b-9bf3-4eaa-a8d9-27aebda82945",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 283,
                "y": 64
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "407ce049-16bd-4493-9581-1f2dc098e166",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 384,
                "y": 64
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9ebbcaa7-9512-4ab4-87d0-86df7273b2d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 272,
                "y": 64
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "4f00fcfd-8488-4534-bd27-81e6078cf191",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 137,
                "y": 64
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "be1a5a4e-dd2c-4e4d-aa8a-d96603a33c60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 35,
                "y": 64
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "bdcb838d-a0e8-4cb1-bfdd-b03195dcabc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 29,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 46,
                "y": 64
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a48a5583-f292-4d1b-a50f-5a874c954c7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 29,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 52,
                "y": 64
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "be8d1724-cb88-4e59-a6b2-025c61746fcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 58,
                "y": 64
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "286cf3a1-0b30-4fc3-8504-4aaceafab6f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 70,
                "y": 64
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "dee2e3d1-e83e-49ff-81bf-1a3a34aadff7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 82,
                "y": 64
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "bc58d379-4917-4609-990f-18fdda74f2aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 94,
                "y": 64
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "30d3e16b-08da-43c1-8ffe-1faac21c0235",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 29,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 105,
                "y": 64
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "3be0e6a9-a506-42c3-8505-0b288aee2dc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 125,
                "y": 64
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "66899002-289f-4c61-9743-52f0fdf3e5ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 149,
                "y": 64
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "9967259a-1e5c-4cb9-8855-8d48a1318c3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 248,
                "y": 64
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "80536e25-77f5-4d51-87ba-150d097c4972",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 160,
                "y": 64
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "87309e59-cc08-4dff-8718-907243d16ec5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 171,
                "y": 64
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "97fc6bc9-3889-4715-8912-1bea4757dd3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 181,
                "y": 64
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0b7c51cc-9aeb-4a6d-a7de-3053a4b2ee7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 191,
                "y": 64
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "63611e49-dd87-44b2-9347-15f00d5c784a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 202,
                "y": 64
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "396fa1e8-3d7e-4300-8f9c-cc191126aab3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 29,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 213,
                "y": 64
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "89bea999-fc43-4acc-988d-f35968b1e7cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 218,
                "y": 64
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "8d490f22-95ec-4693-8347-8e9236f6ea1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 226,
                "y": 64
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "ae2e7f20-e089-46fa-a9e7-52d2e6f728f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 29,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 238,
                "y": 64
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d45d8d67-49a9-4e45-b4be-0baddb0a965c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 29,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 258,
                "y": 64
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "dce39477-86dd-4be1-b8b6-d88ccc42a604",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 15,
                "y": 95
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "72fb785d-d526-4581-a90c-ae2becd460c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 262,
                "y": 95
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "1ae73bdb-48d7-412f-8ed9-6574cbae99bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 26,
                "y": 95
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a1acae73-9ba6-46fe-8db6-53299f3269f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 294,
                "y": 95
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "9e72802d-f08b-4ce0-a143-6dd006871df9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 305,
                "y": 95
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "673c4f0b-3619-414e-9702-0727bc5ff07b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 316,
                "y": 95
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e6316110-878f-4a68-a10a-88239d68248c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 327,
                "y": 95
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "de0c2195-a11b-476e-b85a-cb60a5ee8c65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 338,
                "y": 95
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "95fbac42-9de1-404d-808d-aa824abeca7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 349,
                "y": 95
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ca5a3ba0-2545-430c-9766-b91aa72bb0d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 29,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 361,
                "y": 95
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "6dc5a383-f8a0-4ebd-a724-3b38c8cb3624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 377,
                "y": 95
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "1e88de74-f3ce-483f-b27e-d5530a83e495",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 29,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 389,
                "y": 95
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "6434516c-e3c2-425f-b36c-bdafd14186a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 412,
                "y": 95
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "60cc5831-dd77-40ef-a5ad-0fa47fc3f63a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 29,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 13,
                "y": 126
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "bc7ab5e5-3771-42a7-95fa-1eb482e1b658",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 422,
                "y": 95
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "54dcc839-ca07-4f08-9445-9bf6d14eb937",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 433,
                "y": 95
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "318d29d5-dd3a-4b68-b5b8-f1f27f029e2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 441,
                "y": 95
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "9e04f1f6-3952-4054-b0fd-cc1e1dd22210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 453,
                "y": 95
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "8c7be7d1-aa4c-4043-a272-760713d60f13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 29,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 466,
                "y": 95
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "4263b1f6-a8b1-4580-aa5d-622c06fa963b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 473,
                "y": 95
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ebdd5607-c815-40f8-a226-dce5d1fd58f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 485,
                "y": 95
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a180193f-a195-4520-bba3-1246c7df291f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 496,
                "y": 95
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4903a6a2-dc01-4d1f-8354-f43d3a044540",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 126
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "fdc93175-dc4b-4bd3-80f4-eccf78737302",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 284,
                "y": 95
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "4dd836e7-a6ab-4fe9-909b-76357bc3da07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 402,
                "y": 95
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4af32d64-6b51-41aa-a443-a4821c3a4c8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 273,
                "y": 95
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "44f3a1b0-4251-414b-bfc6-05e767e4d174",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 130,
                "y": 95
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "2435a16c-01f7-4a58-aaaf-59af04a8b7e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 29,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 37,
                "y": 95
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "50396e39-c00c-4243-8855-04c8cb29eb1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 42,
                "y": 95
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "762dd29e-e4ee-4a06-a154-3727c3c1a616",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 50,
                "y": 95
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2d0cdc8e-c984-47ad-9091-61e1f5e52c7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 29,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 62,
                "y": 95
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "7d76b61c-c3c7-45e6-a07f-2804b5f2b21e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 29,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 72,
                "y": 95
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "73793ea9-9574-4e2c-9ee2-f235b2541d80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 86,
                "y": 95
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "966c5cfc-6a89-4a81-89b2-c865e6ee278f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 97,
                "y": 95
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "830cd2b3-10b8-4907-8101-4bd29fbab95e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 108,
                "y": 95
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2df80112-c7c8-4d9e-8981-71d039aee2b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 119,
                "y": 95
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "f8c3c07e-6954-459d-9e4d-2afbc248a813",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 141,
                "y": 95
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "e96c2141-b70a-4565-b036-04ca783188b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 251,
                "y": 95
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ef552b4d-dabc-402c-aba2-e33dfd806051",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 152,
                "y": 95
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "c6b7500e-9f4b-4c67-8a59-d195578b2269",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 163,
                "y": 95
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "4d1d7a2d-9756-40e2-ae89-7dba96d73227",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 174,
                "y": 95
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "fe4ba6a6-c421-42b6-ad1f-2c9e5f8681a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 29,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 186,
                "y": 95
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4652572f-6ed4-452e-a44d-5d8b6d43aed1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 202,
                "y": 95
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "e9597c2d-1d39-45fa-a3f7-eb3eeb952557",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 29,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 214,
                "y": 95
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "9a0878f8-08c6-4843-ac58-ef7dcbd664bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 29,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 227,
                "y": 95
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "70e3650f-feb0-4f30-9c78-07d01442f17a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 237,
                "y": 95
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "6b4b2146-2274-4f01-b53a-745deceb13a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 246,
                "y": 95
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "4b6f74ca-67e5-43f1-a4c5-673cfbe75341",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 26,
                "y": 64
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b6cbfefb-7e45-41d2-9208-8865c8be413e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 2,
                "y": 95
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "f5441fa6-4c9d-422f-aa24-b70c3224257f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 29,
                "offset": 0,
                "shift": 5,
                "w": 0,
                "x": 24,
                "y": 64
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "ba2a0e13-97a5-4c32-84f4-53b972be65be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 29,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 492,
                "y": 2
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "421ba2c0-3200-4551-a0c0-71275c982ec7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 273,
                "y": 2
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "9ebf89a9-6031-4134-8701-257568e32a03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 284,
                "y": 2
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "e69ce677-e1a6-4a81-9eac-d1c9c057f913",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 296,
                "y": 2
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "9355f1f8-a1ef-4171-a227-a7fe0b8dfc43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 308,
                "y": 2
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "6e7b0f72-b71a-4711-9db1-a9973f80010b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 320,
                "y": 2
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "e7dd7bf0-4797-4de2-bd68-d79308ca3ecb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 325,
                "y": 2
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "6198f8aa-411d-4a05-aab5-9e1a1e135ba2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 29,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 336,
                "y": 2
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "72c8ac88-dae5-4bc4-8246-c4e995a757a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 29,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 344,
                "y": 2
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "92cd258a-d9e8-49d4-9129-77cbfa065258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 29,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 364,
                "y": 2
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "f10311dd-7fcb-451f-badb-bf4cd6d68022",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 378,
                "y": 2
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "6125cb1a-e83c-40f5-b0ca-4e5698dd9565",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 480,
                "y": 2
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "ffd3210b-602b-4718-9313-403769066b8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 389,
                "y": 2
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "98a9b90f-b86d-4264-a4ff-f02f3effb87d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 29,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 398,
                "y": 2
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "b25e1017-0487-4bd3-8215-cc76ebc61b4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 29,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 418,
                "y": 2
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "b7aec221-4176-46b7-a2ff-f803f70e9b9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 426,
                "y": 2
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "90e3c122-e393-4371-8f26-47ce9f72b7d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 434,
                "y": 2
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "14fae6d7-a7b1-4eb8-a3e3-1dcc2d42ccf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 29,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 446,
                "y": 2
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "9ed21c05-0b5d-457b-a809-f79b57a61274",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 29,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "adc92dfd-12e5-4e99-8859-78b280a0cc7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 29,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 462,
                "y": 2
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "0f811aa4-c8e7-422e-81ef-2646d9a601e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 469,
                "y": 2
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "874ecf3f-fb17-4b22-b844-3cd07108ceba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 29,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 260,
                "y": 2
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "00f7964c-47b0-46e8-8a6e-d86c08187752",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 29,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 372,
                "y": 2
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "4d9932cb-99ca-4e2e-b251-516af14dd9e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 29,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 252,
                "y": 2
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "73cc664b-589f-4572-a0b1-dc1b9fb95d96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 29,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "53b6ac62-27a7-402a-83b6-e21599f7807f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 29,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "d1f77ca7-d48f-4379-8ef7-35d0e280fef7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "673a3d2a-50c7-4d30-beb5-9cf75805095d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "abf861ea-b786-40f7-84fe-719e96ec1fad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 29,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "82845784-255f-4cd7-a6c0-9c916a077c3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 29,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "3a8d7dee-8e1d-46dd-9865-4e054ef6480b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 29,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "04e3a5f0-02a0-4ad9-9d04-b021726931f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "9aa29bad-5dbd-4782-bab4-2f94f8078be0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "46ee94a9-3d76-4d12-8565-bb845fd1fa2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "3de13723-1862-4294-9ca6-93c8244598d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "a86c97c6-333c-4813-a40e-5e29b4b81551",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "3b1af613-35c2-4642-9a80-c75ca105f9fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 139,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "bf78567e-8d14-4419-a108-e1d42d8d95f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 29,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "4e30ce0c-6496-4512-9af1-1311aaf1a490",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "a3d9d110-705c-4447-aef2-917192d73870",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "dcddd1dd-7905-4632-8b5a-ad1d2a42343c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "c6f09e2d-deea-4f79-9621-efa62889e0d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 197,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "ae29775c-c183-470d-b4f8-e70228f3f8c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "3c456b26-e6a6-4b84-a3df-c42f89b8856e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 29,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "c1263077-0e9e-4597-802a-3ffb0a4c1938",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 29,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "10e4d966-8b32-4c8d-ad22-968277579816",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 29,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "4a49d4ab-212c-4397-a92a-02ccf5eab528",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 29,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 498,
                "y": 2
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "1dff6146-8462-46d0-8402-d4c70207a58e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 234,
                "y": 33
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "fe4091b2-1f53-43d9-83aa-961806fa0b5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 33
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "6c1835e2-7dfb-4b49-af46-b90db2d322b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 272,
                "y": 33
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "094569ed-2cb7-4f69-8dd8-6747cf43d42c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 283,
                "y": 33
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "02b93b69-30ad-4e3c-b08e-d7822d78c7c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 294,
                "y": 33
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "1e104179-72da-47ef-a1cc-38f0515ca75c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 305,
                "y": 33
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "785d5399-18e2-4695-9635-bcc652f72633",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 316,
                "y": 33
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "6471a66b-cc80-49e1-b465-5b97ead81ccf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 327,
                "y": 33
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "13ebb2de-b7cc-4c72-8d98-1f3a25b666a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 339,
                "y": 33
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "1ea37920-1afb-47aa-a7ec-49ff7a9159a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 351,
                "y": 33
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "457d17b1-8df0-482a-9d43-2279ef62ff45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 362,
                "y": 33
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "611011f0-84ee-4926-ba65-059235c6e70d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 383,
                "y": 33
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "127cd1fe-168f-4d61-856b-ba62a0b45af1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 64
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "d971e528-2f8e-4818-855c-6ff2357338ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 29,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 394,
                "y": 33
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "449de69a-4e8c-4d57-870d-85ea9f2600e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 407,
                "y": 33
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "0f373b55-94e9-42c4-8f05-7fd5eeb337e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 29,
                "offset": 0,
                "shift": 17,
                "w": 18,
                "x": 418,
                "y": 33
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "fa84d387-c95f-4aee-9ab9-e38f2c411c2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 438,
                "y": 33
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "9b0b19ff-6a0e-4946-b473-b6f4eabd1bc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 450,
                "y": 33
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "009463c5-46cf-49ad-ba40-782c7332b3ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 462,
                "y": 33
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "83f143c6-0a09-4c16-8d85-bb955530794f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 474,
                "y": 33
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "5595c7bd-6179-48cb-b657-0a94f3f927b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 486,
                "y": 33
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "26c3f1aa-590c-4066-bf5a-d9ef2ae4affc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 498,
                "y": 33
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "4a22dd8a-148e-48bb-bbb3-0b92b549c575",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 29,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 256,
                "y": 33
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "a57cd75a-8fed-43e6-9a15-b673c6245744",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 373,
                "y": 33
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "f00bfefd-1a8d-4df3-b586-897e9d66e281",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 246,
                "y": 33
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "8486ab91-923b-49c2-a609-234abe2901ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 99,
                "y": 33
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "82ce2341-8f39-4d03-a108-ed48d68fef29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 13,
                "y": 33
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "8a2b75ac-ded1-4f7d-be4b-524323a6915e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 29,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 23,
                "y": 33
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "d248acc5-ba5d-491f-b67c-b273c08a936f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 29,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 33,
                "y": 33
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "62da2e48-9d6e-4b33-b65c-cbf139d5de5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 29,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 40,
                "y": 33
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "992d98ee-ba54-4ca0-b424-e0ff4706df37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 29,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 47,
                "y": 33
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "536490c5-e0cc-4ea1-819e-ecf76b3f9eea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 29,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 56,
                "y": 33
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "f409a5cf-e880-4858-b528-219499411898",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 29,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 65,
                "y": 33
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "deb49e4d-774d-4cdd-8cdb-89e986dba1ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 29,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 77,
                "y": 33
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "5ee11084-46d7-4f7e-b05c-e42e32fbcc66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 88,
                "y": 33
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "826a96cd-2bbd-4148-9abf-113bf675a009",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 109,
                "y": 33
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "eb445a61-240a-4ae5-a4f7-03a70f66c13d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 223,
                "y": 33
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "5b6d2c8e-d512-4372-93e9-334a28b74fdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 120,
                "y": 33
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "93cf0210-e4a4-4692-98d0-308cb7e9875f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 131,
                "y": 33
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "ab147a1a-51c1-4291-a5ed-47cb50f74624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 142,
                "y": 33
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "fd9d144d-570c-4c77-b13f-97338c933301",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 29,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 154,
                "y": 33
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "320d34b8-9c1c-4c02-9d2f-25d4c5372cc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 166,
                "y": 33
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "077f96dc-d17f-4325-a910-c3ab0499bfdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 177,
                "y": 33
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "7776dd6a-67f0-4e23-8579-e4b854123d0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 188,
                "y": 33
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "160a97d9-508f-4582-89f9-72bd3e1840ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 199,
                "y": 33
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "75a7c4c2-fc5e-44f2-bcac-0984672b739d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 29,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 210,
                "y": 33
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "1f1e4e64-68e0-4a70-9306-ab608f13d95c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 29,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 13,
                "y": 64
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "9e9dc54d-eb61-4d19-b04f-3ea1f16e0850",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 29,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 21,
                "y": 126
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 18,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}