{
    "id": "ba39515b-0fff-4863-9fed-1a49583ff575",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_arcadepix_12pt",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arcadepix",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "266d1150-0df4-432d-b042-643842154892",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 44,
                "y": 38
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ac6c163e-bdfb-4a43-ae42-13e856491eee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 18,
                "y": 74
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "4e6227d2-8148-402b-94eb-aa7ec303ffa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 129,
                "y": 38
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "6bba36ca-0329-47a5-b70f-b1f9591544e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 174,
                "y": 56
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "5ddff27f-c917-4236-b3fc-4418b0463d41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 204,
                "y": 56
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "590d4513-e893-4603-83a1-889c8138674d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 170,
                "y": 20
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "2772785f-d1cf-4760-975e-4107011b8bfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 165,
                "y": 38
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ee3ff011-428b-4a4a-b29d-0bef222bb658",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 72,
                "y": 74
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b62595ef-27bb-4b32-886c-f4bbe62a4e7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 46,
                "y": 74
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "157b517c-706c-4980-928c-98b8195c22a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 39,
                "y": 74
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a4fa2073-3625-44af-a511-a876be212304",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 194,
                "y": 56
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "07fd1950-4ca8-4bdb-aeaa-b077f7927383",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 214,
                "y": 56
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "a62a45af-9623-4c03-be1f-7a3be939d291",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 25,
                "y": 74
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b12ee63a-cde3-4799-a6ae-a5d40f6e8c09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 184,
                "y": 56
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f724f5f6-466b-4d32-b441-f9ab8e787645",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 60,
                "y": 74
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "5a0d43b4-8c20-4f1e-8f8b-58c9902c7bc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 16,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 133,
                "y": 56
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e39b7c41-fa1d-4958-918d-01e21adf095a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 184,
                "y": 20
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6b732f1a-14d3-4255-82be-a03577835ada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 26,
                "y": 56
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "8e8aee2f-3eda-4978-a2b2-bb670fa7af3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 156,
                "y": 20
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "3cadf87d-da02-4a48-8e43-422643b9c9f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 198,
                "y": 20
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "1fdcca8d-388a-4dd2-88f5-df6a673d7949",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 38
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "17249f01-9e13-4cef-9ac8-41276be37787",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 38
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "b2be4ffa-f01f-4339-9741-1994e9d02e2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "488b5682-dc64-420b-9cab-f4f7924a2291",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 240,
                "y": 20
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "921d8201-2878-4e2c-acf4-54340631bafb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 226,
                "y": 20
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "416dca5d-dd34-4758-a285-e79354017e28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 212,
                "y": 20
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "d100f7c8-0123-491a-8698-f446c2d2545f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 66,
                "y": 74
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "9e0bcb13-268f-4042-b151-eebefbf2740d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 11,
                "y": 74
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "015a860e-f6fd-4275-8e45-847cbd71084d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 224,
                "y": 56
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a740c7b3-0e98-4072-9cbf-cbccd6ab547f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 164,
                "y": 56
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6c9c1443-26d2-47ce-842a-ea78b06d7057",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 242,
                "y": 56
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a458abfb-db53-4b3c-bb39-2a0e791ead3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 128,
                "y": 20
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "4721d6ad-6b53-49ca-b5d6-d27901f36a15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 16,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "80ca46a6-e674-4c09-a667-a07731dbd37e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "3d6b0a8f-4562-4851-8ca6-18479704ed91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4dcf9f79-6ff6-4036-b585-c77f603c33d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d4c779ff-d9c8-4f9b-b8a7-e1dcb08d5914",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "05e415e7-afe6-46b9-abe7-7343df117c36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ff2be128-da86-43c3-97cb-47903cbe647b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "170fcda0-c852-4e6d-b512-5a2311d25b67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 115,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "bc659def-1f54-4600-9ef8-4f6306c5eed2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d10c3c64-8851-4729-bd55-8d2266ad493b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 14,
                "y": 56
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "931a836a-c100-4424-b432-b2cda9783943",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "079244fa-f4a6-4916-9cb7-a57dcb57b76b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "1f4b49c8-5e4f-4ca3-83a7-ee3d4d788d03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "fbe52fbe-9857-487b-940f-7efa10aa5268",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "99012fe5-9c9f-45ac-ab46-ed2f97014eea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "005eed44-6639-42b6-b9c5-bc43583e67d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "2f3be9c6-66d3-4e49-9513-eecca2f95762",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 114,
                "y": 20
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d45a7ec4-a5df-461b-a09d-2e31e1029f0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ccc41727-55f6-40d7-afef-b881b0e5ae96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "bfaeb7f8-5d75-4237-8d21-8616a4e47579",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f639697c-4841-48e3-be3e-d11d1c3f67ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 38,
                "y": 56
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "27a7cfa1-ebbe-4ab0-97b8-8aca5a5a8e07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 20
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ea7bc28d-6f05-479b-98fa-8ce182ee2ede",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 16,
                "y": 20
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "3364f3c3-40b1-4133-ab38-3815192c047c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 30,
                "y": 20
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ed1d3caf-4e78-49f0-8959-a596d8d23ae8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 44,
                "y": 20
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "51624d43-4bf1-4f59-8205-029cadd4bc2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 141,
                "y": 38
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4a102175-49ee-4bd5-b7d9-c8c37da2edeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 58,
                "y": 20
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "46e6c6dd-45ac-448d-adb9-987f7617f935",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 16,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 80,
                "y": 74
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "56637990-1992-490c-a19f-f620ad69cda9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 16,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 122,
                "y": 56
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "afad4ab0-0037-4821-8307-20c1490a08d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 16,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 86,
                "y": 74
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "2b5e9eb4-7328-42a6-8736-c67f3607d07b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 81,
                "y": 38
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a5989c32-8d17-4f0b-8df8-d401fc9f8476",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 72,
                "y": 20
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a2823a87-82f9-45d7-b01d-365acf37ec69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 74
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9ac75682-2004-4a11-b8e5-8d177f622b7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 177,
                "y": 38
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "70220892-8f93-4e0b-8b3d-11e885849d68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 93,
                "y": 38
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "f516bb5c-fe63-4b37-b1fd-6c3618cbcb6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 153,
                "y": 38
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "767ed0e8-89e7-455f-9172-e5561fe80668",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 98,
                "y": 56
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ce067067-9922-4141-a282-70d5aed25ccd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 50,
                "y": 56
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "214753c2-dfce-497d-99d8-ad6aa25b77a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 154,
                "y": 56
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f3800ecf-25f9-4eae-bf94-e073556f8dd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 69,
                "y": 38
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b1a8ac9e-c980-49f7-b9c4-41752d554447",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 105,
                "y": 38
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4908b719-1490-44e4-b655-9f021e56ba64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 53,
                "y": 74
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "ebb93cfc-6d4b-43b3-aedd-f537a0029620",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 233,
                "y": 56
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "18bc29be-0eff-44e3-b006-8fc2630ecd2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 117,
                "y": 38
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "efe2fd18-c5c3-4ed6-b1ae-d39d7d3950d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 32,
                "y": 74
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "63a358d5-6f75-45fb-9a78-cffbedff50f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 86,
                "y": 20
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "cea938d3-d6fa-47db-a05e-9ec3717fb4b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 57,
                "y": 38
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "ca59fdc5-c883-4587-8b35-6d7cab145baf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 189,
                "y": 38
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "48803714-564f-4f4d-b1d0-06ad83a9eac0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 213,
                "y": 38
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6a8bf999-8209-4a2f-94a1-e52b40133d5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 110,
                "y": 56
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b83c303e-fce2-4c13-8d0b-388530e69f07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 201,
                "y": 38
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "d9ba2680-ea96-4cfb-a17b-53e0c41437ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 86,
                "y": 56
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "0df94d03-f53f-4e18-81da-3b2e320f4e49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 144,
                "y": 56
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d4133276-23e1-4c0d-9d21-b82d11618e7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 74,
                "y": 56
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "87a6df7a-e861-4dac-a3fb-be6022302f0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 62,
                "y": 56
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "1a95c240-dedc-4410-a57a-fe8a6eb3c570",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 100,
                "y": 20
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "fbaa3063-da5c-4bc3-a610-5ff065ba4168",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "0e457c8c-11d3-4414-88a3-8b216c81dd89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 237,
                "y": 38
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "40fb494c-f924-42e8-b8e4-ddf29f3e12f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 16,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 225,
                "y": 38
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "88fdbd6e-66ce-4ebe-9c47-3a1eb6962f55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 16,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 82,
                "y": 74
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "b2622390-f052-496d-85e8-06f4b5675d1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 16,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 84,
                "y": 74
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "0d8ca2a8-d859-4c90-998e-c2e42ae3f9d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 16,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 78,
                "y": 74
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f40fcc1b-f1c2-4259-ba0b-111564417a52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 16,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 142,
                "y": 20
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "9de21f60-b0d2-406f-a37a-41d8ae652224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 84
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}