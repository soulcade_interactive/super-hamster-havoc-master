{
    "id": "4fa33d44-2e39-4277-b3ce-ffc80eabe161",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_bebas_kai_16_pt",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bebas Kai",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b2dc6e2d-3cc3-4d3e-8d0e-4e516ac51e42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 26,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e5d66b2a-dbda-4b45-bf52-59f589fa9459",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 208,
                "y": 114
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "051d81ab-1745-4274-9f67-5fe48db52c05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 213,
                "y": 114
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "112f9ad4-2bcf-46f1-9730-622e1392a9dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 221,
                "y": 114
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "b156b2c7-b11b-48ac-b7be-ad42584283ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 233,
                "y": 114
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "205324ac-2694-4ab2-b371-dfc1cdca2b18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 142
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "7db5db17-2cc8-4e5e-9715-318648ddd0f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 26,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 16,
                "y": 142
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e578db92-b6cb-4ab0-bff8-e4178db02ca9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 29,
                "y": 142
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "9a4ce3ed-0f84-4ac1-826c-63a8569861db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 26,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 34,
                "y": 142
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "1480a626-e556-4664-a4c9-9ad70400c3c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 41,
                "y": 142
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "42adc75e-9dab-4865-ae67-e8fb7ec89a0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 59,
                "y": 142
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "6260b170-71bd-4efb-bffc-c1b2783527a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 147,
                "y": 142
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8d50f9ef-c01d-45a6-84cc-67e7c4c37c83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 69,
                "y": 142
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "33004e2e-5d4f-473f-9a61-b2178d0271b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 74,
                "y": 142
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "7122518b-2026-4180-8289-d957693188bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 82,
                "y": 142
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "86b45f92-303c-4629-9be0-d42c3ee8bc2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 26,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 87,
                "y": 142
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "10b18050-49cb-405a-bd5f-244b06840010",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 97,
                "y": 142
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "f0b0faaa-3408-4102-a5ff-0b2699375c73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 108,
                "y": 142
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ad80a1b6-8cd0-4d11-9254-426a86fc5c0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 26,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 116,
                "y": 142
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d2ff4660-a6b5-4a5a-bc40-6d7376ad4c7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 126,
                "y": 142
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0dea9280-0768-422f-84cc-f5d19a904349",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 136,
                "y": 142
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "31423e3a-3bd2-4293-9a26-5ce97f149b81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 198,
                "y": 114
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a4672360-6f55-4925-b980-2c5883d1a409",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 48,
                "y": 142
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "216672ff-7102-4935-b040-f45f6c37e0c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 188,
                "y": 114
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "724e2d2f-1982-4a09-8cf4-b0b154071159",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 63,
                "y": 114
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "2975d1f3-a09e-4643-a51d-74578a6c9910",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 222,
                "y": 86
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "071c1c08-4bb9-4211-bf0f-cf725fe1a6ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 233,
                "y": 86
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "cb43e641-202f-4024-862c-daa609760616",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 238,
                "y": 86
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "dfb766dc-10c5-4c80-80a6-d77cbfd2dbc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 243,
                "y": 86
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6811fe0c-03d0-4446-9ed4-33d64c9f8690",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 114
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "bcbe8fed-1e77-4abb-8ecd-2a84e776b615",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 114
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "b4fce831-ac1c-47b4-904f-e2c864a6a4bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 26,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 24,
                "y": 114
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "0d4a1c1e-29ca-4d4e-8638-697534f3b61e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 26,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 34,
                "y": 114
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "5ca6c407-76dc-43ac-ae36-494ae3119e63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 52,
                "y": 114
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "9bdddf57-e59f-4a28-a380-2e2cd447c1a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 74,
                "y": 114
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "81d74b7b-e202-4265-b9df-7c44f73c69ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 166,
                "y": 114
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d2eec7f6-7191-4fff-a126-35619c178cb7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 84,
                "y": 114
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8637d1ce-e51e-4065-9ab0-04df010468ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 94,
                "y": 114
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "2b4795d5-3641-4f80-ba34-fcf375fd21bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 103,
                "y": 114
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "d8eaa208-8243-4583-a979-5ad1c3b93615",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 112,
                "y": 114
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "b4842f47-7367-4ad2-8788-f3776aff243c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 123,
                "y": 114
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "46e83786-4ddf-4492-a35f-79dace659e6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 133,
                "y": 114
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6c3694b5-545a-4e0c-9c41-bb8fe9295300",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 138,
                "y": 114
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "54d37c4f-eb7b-41a3-830c-5b3a2771b801",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 146,
                "y": 114
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "eb9976eb-9a26-4e36-94ff-eea2cf0e2022",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 26,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 157,
                "y": 114
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "ad2b98d9-6150-41bc-b949-23f688f60ebf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 176,
                "y": 114
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "256ade26-8951-4db9-85d2-ee72dcdcd309",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 170,
                "y": 142
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0963b6aa-b78c-4081-bca8-73212abd38d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 156,
                "y": 170
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "add4e242-2cfe-44d3-88d2-b5a1a02064db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 180,
                "y": 142
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "69181c58-5468-433a-918b-866b1d065079",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 187,
                "y": 170
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "de0f1c9a-bf95-4672-877b-512df5dbe227",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 198,
                "y": 170
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8ededf54-e3c5-425f-b3e4-1c79d426e98f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 26,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 208,
                "y": 170
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "85a149ff-c766-4b12-ad50-fb33deb6c9a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 26,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 218,
                "y": 170
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "29249d4c-0238-4a03-b044-1439ac2b53e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 228,
                "y": 170
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "87cdabc0-78d7-483b-b3de-cb540f1b64ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 238,
                "y": 170
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "acbdf2f9-a337-4f4a-a940-3ca1cbef7ec0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 198
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a7adc45d-8857-4158-b432-75baeb02df7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 17,
                "y": 198
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "762943f1-0abb-483a-8c4f-5b90e07acf6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 26,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 28,
                "y": 198
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "68f2d639-e668-4198-80b7-be5e158d60da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 49,
                "y": 198
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b717a64f-485e-4d80-80d5-98e3647585b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 26,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 146,
                "y": 198
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "5849478f-52ea-4341-b3e7-380f4730229a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 26,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 59,
                "y": 198
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1c1a91eb-3587-4e87-812c-c818cfca0696",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 69,
                "y": 198
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "1510c2e0-6cc3-42ae-b8c6-db3ac764cba9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 76,
                "y": 198
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "53b7c6b6-eebe-453b-bc59-f3855f932491",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 87,
                "y": 198
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "b33a6e2a-e9b9-4cdb-a2a5-4fc6c176809d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 26,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 99,
                "y": 198
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "80d08ee0-905d-4110-b348-113fb1085bea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 105,
                "y": 198
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "16687a90-eb4f-4711-8aa5-5cf6c21098ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 116,
                "y": 198
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "443718ad-291a-4b37-9e82-736a1208e699",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 126,
                "y": 198
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "519e663c-6ac3-4659-8113-e7de90c4a7c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 136,
                "y": 198
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "58cb6d6e-9b63-4cbe-8c62-f50969ef7863",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 178,
                "y": 170
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "3c407505-6227-4f31-bde1-e87e19701bc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 40,
                "y": 198
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "06efcab2-8c5d-4c78-bda8-6128caab9558",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 167,
                "y": 170
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "4d5bd00f-000e-401a-a60d-b7cf18acaaf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 34,
                "y": 170
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4d10f54c-d1a0-46b1-816e-93695078a56f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 190,
                "y": 142
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d50e1e6e-253d-412f-a25c-6d1542f65b3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 195,
                "y": 142
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "f308766b-9fa9-4c74-aad2-b0cae4c6fe81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 203,
                "y": 142
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "7ce04b3e-cc0e-4898-9dcd-ee7da0f74617",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 26,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 214,
                "y": 142
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "7fd4830f-3763-4c42-94e9-997bf1edd0bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 26,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 223,
                "y": 142
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3151502f-eec8-4120-8861-13fef48e6058",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 235,
                "y": 142
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "fb3d9b35-929b-4c98-bbf2-898ea4a2ca52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 170
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c292aa03-c9dd-4e59-9e53-dca6c14da377",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 13,
                "y": 170
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f8790c2a-d9f0-4cbf-933c-7af3c4cac8e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 23,
                "y": 170
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "da004470-2bfb-41b2-be8d-0144f670ad0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 44,
                "y": 170
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "8808451f-8973-4956-b502-f553be7a27d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 26,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 146,
                "y": 170
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "dca400c8-f81b-426f-a34e-13eabbafb3a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 26,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 54,
                "y": 170
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a22f4413-9e5a-4dda-8a63-d835a9ce5b2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 64,
                "y": 170
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "58ded4c7-89b4-4d66-a200-1d9c0016b10f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 74,
                "y": 170
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c43f33ca-6f88-47fc-9127-4ba9dedec52c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 85,
                "y": 170
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "3499c5ad-1ece-4616-952c-c1289cb0d55e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 100,
                "y": 170
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1ce91920-7864-41c2-b08c-f5a79809c464",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 26,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 111,
                "y": 170
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d8c562af-e350-44dc-97c6-9528de9772ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 26,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 123,
                "y": 170
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "801ca015-44cd-4eda-b018-94cd3c5f66bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 133,
                "y": 170
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f119b307-8015-4e50-a664-da041b2efaa0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 141,
                "y": 170
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "2804c1fb-0bdc-4feb-80c5-cddaf7fc1248",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 214,
                "y": 86
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7feb4abf-f64f-44cd-b20e-34e84a9809b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 158,
                "y": 142
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "26114312-6f30-44d3-834e-77ce5f882203",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 26,
                "offset": 0,
                "shift": 4,
                "w": 0,
                "x": 212,
                "y": 86
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "014ac298-7cfe-486b-8d3c-4efc7d1d7bf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 203,
                "y": 30
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "cf2b4fcd-a9fd-43ee-ba01-c222cefde824",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 30
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "9efc82a5-dd37-49c3-bad6-64ae71dbe912",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 12,
                "y": 30
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "5efd8ce7-a3be-4d6c-897f-2a286a82faa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 23,
                "y": 30
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "1a955917-7a0f-4806-afe5-d2c3325616a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 34,
                "y": 30
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "0bc7fdee-2375-4e16-9a91-2209eeac5a98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 45,
                "y": 30
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "95c19082-a35e-4cdf-b6b1-a93e3b503e0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 50,
                "y": 30
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "07306e8d-0d62-4dce-8963-6e23e5e499a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 26,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 60,
                "y": 30
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "d1a26b09-ceed-48bb-b9ff-00c58b1cd507",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 26,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 68,
                "y": 30
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "e8a49283-452b-49b5-a4be-d93ab97a70d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 86,
                "y": 30
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "f4eeaadf-5b39-4f52-912b-681cd5b19e5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 98,
                "y": 30
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "f0168f49-6303-4dcc-9069-6ee5b7305bd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 192,
                "y": 30
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "7f6bf4cc-a649-45ff-95f0-3e3cd97912ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 109,
                "y": 30
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "bd2dd935-b7fe-42ba-b109-04628817cc53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 26,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 117,
                "y": 30
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "4d3944fa-302e-4e4e-a61d-ebb718a636f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 26,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 135,
                "y": 30
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "fe576517-10e5-4a7a-bfea-deedd9ee2c77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 142,
                "y": 30
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "2aacba64-5594-416e-a939-67d35691941c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 150,
                "y": 30
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "4ee5ae5a-7afe-497d-ac5b-045e7f5b8af2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 26,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 161,
                "y": 30
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "b22966c6-f7df-4874-9e31-5321a0fa17da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 26,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 168,
                "y": 30
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "f56abd2a-78e5-4b4d-840f-91c53755e60a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 175,
                "y": 30
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "bef41523-6233-428e-a5e1-1a0843e1f3e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 182,
                "y": 30
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "16e639ea-b132-4269-811c-7668837cdec3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 241,
                "y": 2
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "910433ce-da6d-497e-8649-fca2f38acc54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 93,
                "y": 30
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "86013179-7f74-47c9-b77a-c9c9d5984aff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 26,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "36e9ff16-cb6a-428d-9630-5c32b6d85ddf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 26,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "18def87e-f719-4c69-b2d3-8407673585bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 26,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "06bd6051-a35b-4b11-be8f-3e19df0c653f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "1f097be9-38e7-4c33-b578-14f1760ca287",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 26,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "4abebfad-b042-40ce-8fe5-6046789c30f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "03b56d53-75c7-4a04-8d53-dd0d04c40eea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 26,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "e6bf3af4-f063-4b97-985b-ee1279c0e435",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 26,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "4dce345a-0193-4627-a8af-a26660ba76df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "97155f34-5154-4a72-a20e-f42f20d74c3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "66a008b7-9991-4fcd-bbeb-c1cd63a19ab9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "3eb0e3e3-76d1-4f8e-9c74-9c18000cf35c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "357f191c-ab88-4b1e-9784-9fe32199558e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "dea9c2d8-91ad-4ac8-99dc-8b5c4d3fe6de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "f7249157-a11f-412a-9ee0-d512ccbbbd9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "73640306-b91a-43fc-a226-07e7e4bd2f84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "a92eea85-c8fd-4637-9798-74b32ac0b5c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "e40b138c-5cd4-4e4f-a1cb-e44de1b553ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "4b0dabb1-6591-491d-95d4-58a5350cc142",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "0383ce79-e55b-4fed-b01e-700ef242ed79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "4279d32c-1504-4e38-b160-53c1676058ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 26,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 202,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "1ddd3ff2-879b-45e1-b06f-52fa948cd207",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "d4533e5e-d43a-4e00-98ef-4177e17c32e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 26,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 226,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "be6ff510-d5aa-42dd-8feb-4e22b9242085",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 26,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 208,
                "y": 30
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "c892b92d-9cf8-4576-9e1c-850ff6ae685a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 183,
                "y": 58
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "9a74c2ce-cd9e-413e-87b1-71500fd135d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 216,
                "y": 30
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "445ffcfa-5cda-4ceb-9ff6-1de421b6817e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 218,
                "y": 58
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "3fcc7f35-ca94-475c-8fb4-aa0fdb796003",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 229,
                "y": 58
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "ad94a998-74da-4ac4-a29e-b8ec74386f16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 240,
                "y": 58
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "5e947ed4-8b08-4417-8b42-fe20910afde9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "891c88bc-f32e-4d98-ba03-831780a84926",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 13,
                "y": 86
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "8bd2086d-027d-4036-9ec2-36815ee19adb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 86
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "82dfe7e5-3f53-431c-9d66-4afcb3db4001",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 86
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "68b9a3c3-fd93-49ea-b4ae-1b13aa4c41b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 46,
                "y": 86
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "902023c4-9686-4c53-b666-bb1ae5bb3b1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 56,
                "y": 86
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "8b460c4a-b819-4dc8-992a-a1840f686904",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 76,
                "y": 86
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "f6c3fc9f-301d-4c48-bc4b-d6caae473c08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 192,
                "y": 86
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "011a4d61-d23d-45c1-8e01-27ce44526d33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 26,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 86,
                "y": 86
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "19d5ed49-a643-4e3a-aa3f-eca6450ab96f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 98,
                "y": 86
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "80410611-d0e1-48a7-b8b3-3d67bd84eec8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 26,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 108,
                "y": 86
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "0e1205e2-54b1-4874-a154-56e795e45c33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 126,
                "y": 86
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "27244d1b-8b31-4fc1-a013-9bb27725b380",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 137,
                "y": 86
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "9cf66fb2-0ae6-4881-a731-97f59d4ffe3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 148,
                "y": 86
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "711b0238-7d11-442c-8245-ba10016e74f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 159,
                "y": 86
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "c49a90e3-0d4c-46c1-a9e9-26f03add083e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 170,
                "y": 86
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "ee05d652-538d-4981-ba9b-ebddac88f336",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 181,
                "y": 86
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "304a08dd-6d88-445f-8203-77bf32a5cb1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 26,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 203,
                "y": 58
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "6bd0d9f6-d649-4bd0-8030-4136e2cd6af2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 66,
                "y": 86
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "258b2cda-89ac-46aa-8f47-5bd3fc111e18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 194,
                "y": 58
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "4e5c2495-ec1d-4861-b4cd-3a3e0a63e5a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 56,
                "y": 58
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "c5e79e68-df7e-46d1-90dd-41833cf4809a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 226,
                "y": 30
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "fa4f4402-7f47-4725-9dc4-44708ebe6353",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 26,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 235,
                "y": 30
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "4bc02b8a-f284-40cb-acde-d296408deaac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 26,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 244,
                "y": 30
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "dec61500-06da-487b-9a91-0ea4a434c132",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 26,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 2,
                "y": 58
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "346f2230-beaf-4907-9f46-3ad1f7ad375d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 26,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 8,
                "y": 58
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "d2fdb6c0-7cd7-40df-9bf6-05d8931cfca6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 26,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 16,
                "y": 58
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "827ec0d5-e041-4eca-8775-143eddf75d03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 58
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "dfc36ec5-cd9e-40aa-86c5-b3eb3a43d5d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 26,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 35,
                "y": 58
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "77eb8ba7-442b-4c82-a109-b5de209f2528",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 45,
                "y": 58
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "a40361e7-1e15-4546-9ade-eebad50b0c37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 65,
                "y": 58
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "53d0685a-071f-428e-9801-d41578da3f05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 172,
                "y": 58
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "7bfcdee9-fc28-4d29-a289-322b5297cd45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 76,
                "y": 58
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "1cf40083-acf7-46bf-a1b0-715828f99f26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 87,
                "y": 58
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "36576eeb-d1c9-414f-99a8-a83270118651",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 98,
                "y": 58
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "ccf8240c-ba54-4c0e-84d0-2a319f57e70b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 26,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 109,
                "y": 58
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "de4c28f2-1f23-4bf7-9b05-aeaa8b8212d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 120,
                "y": 58
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "af7d33d8-ab26-48fb-8802-7a89a87af22d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 130,
                "y": 58
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "4e1b361b-8d50-49c9-bbf8-ecc29c8e9e1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 140,
                "y": 58
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "833c4140-4ed5-4138-a02c-f8e7bbfe6294",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 150,
                "y": 58
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "1e94a8c1-613e-4351-a76b-d1f89f93faf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 26,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 160,
                "y": 58
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "8849b3ed-659e-4e90-a2fc-8a79ee45381e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 26,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 202,
                "y": 86
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "e8bad542-592e-454e-84b4-e51c284d912d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 26,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 153,
                "y": 198
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}