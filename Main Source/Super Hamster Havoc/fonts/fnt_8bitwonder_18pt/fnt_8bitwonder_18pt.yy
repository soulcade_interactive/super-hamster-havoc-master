{
    "id": "d47d36ee-a90e-483e-969f-0c30a17424fc",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_8bitwonder_18pt",
    "AntiAlias": 0,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "8BIT WONDER",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "2af001bb-31ec-4aec-86cd-32d0413ccf5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 17,
                "y": 158
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "37520e36-b04e-428a-a118-b49428f86975",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 24,
                "offset": 6,
                "shift": 14,
                "w": 4,
                "x": 73,
                "y": 184
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "9f6d8d40-d768-4aca-a85c-752a53a97611",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 24,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 2,
                "y": 184
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "84279269-d4a5-4f6c-a749-9e212969675b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e73138c7-14fc-4a34-aa22-bc627a0abe4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 171,
                "y": 158
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "af53b000-1f43-42dd-9731-95015171cf7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 213,
                "y": 132
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "a101ddd9-f84a-4bcb-8176-17760b34a916",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 15,
                "x": 196,
                "y": 132
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8a9202ba-66df-4200-bfd8-ef27ddd81bba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 24,
                "offset": 6,
                "shift": 14,
                "w": 4,
                "x": 67,
                "y": 184
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "dcc4ad42-5f0f-49c9-a137-a170184b0894",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 45,
                "y": 158
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "fff7dbba-fdcf-40cf-a1a5-3f0a0385b4e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 59,
                "y": 158
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "97243fdc-a873-48e9-9b06-990e925482a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 200,
                "y": 80
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b2d01158-e97f-4e8e-8a62-666e9e78b598",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 73,
                "y": 158
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "e359a332-b919-4d5c-9d79-e864447e6265",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 24,
                "offset": 5,
                "shift": 14,
                "w": 5,
                "x": 41,
                "y": 184
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4895f625-e069-4ec9-a158-cfef215b0ab8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 24,
                "offset": 3,
                "shift": 14,
                "w": 9,
                "x": 244,
                "y": 158
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f5360e79-7de6-4d45-8248-c612f42cb8b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 24,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 55,
                "y": 184
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "dbe6f73d-c769-42e8-89db-d703f0ade407",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 232,
                "y": 158
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "cd69645b-af99-426b-b9ef-456f62625750",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 90,
                "y": 106
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5b5fd833-e609-4dd0-b296-31f70a2b813f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 24,
                "offset": -2,
                "shift": 12,
                "w": 10,
                "x": 220,
                "y": 158
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "783d4a81-a7e1-40a7-9a0d-658ec0f11760",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 134,
                "y": 106
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "670ed319-7ecc-4330-aa95-8d24bb06ec05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 134,
                "y": 132
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7837da7c-42a1-4c57-9a06-3e1af8d6ead1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 112,
                "y": 132
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "f16604d4-5af9-4681-8c6f-24c331046b2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 90,
                "y": 132
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "c3cd437a-474e-4b70-8857-735dd99fbcdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 68,
                "y": 132
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "22cb2acc-8642-47ac-abc2-52c4f1621340",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 46,
                "y": 132
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "0a7570e1-192a-4cc0-8a16-1aabbab3082e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 156,
                "y": 132
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "9fa79f4b-ad76-4e80-a160-0b0539294b29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 2,
                "y": 132
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "677addbd-5c8e-452d-9ae0-706e756323d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 24,
                "offset": 6,
                "shift": 14,
                "w": 4,
                "x": 61,
                "y": 184
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "6ba7db63-292f-46ad-94fa-d9e06552dd93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 24,
                "offset": 5,
                "shift": 14,
                "w": 5,
                "x": 48,
                "y": 184
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "eee69b23-4373-4903-a956-283ef0bbc889",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 87,
                "y": 158
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "96aeaa6f-f885-4342-8442-e6cea529ac46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 101,
                "y": 158
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2623d32a-4d4d-4004-873a-79d55c341e96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 115,
                "y": 158
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "4dc4c012-552a-4dff-8e89-86930cb3ca01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 143,
                "y": 158
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e64580f2-9288-400f-a977-a7aa178b3cd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 230,
                "y": 132
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "28e95a72-086a-4410-bbf4-dd740e55645a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 24,
                "y": 80
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "ba4b9d70-2a21-4c95-893b-486af6d5abfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 200,
                "y": 28
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "5291fa9c-894b-4568-aa1b-7114cf5a96e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 112,
                "y": 28
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "3ba0edde-2070-4ece-b704-2985873671b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 90,
                "y": 28
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "27f278f2-b51f-468b-802d-be98e9053038",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 68,
                "y": 28
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "25d547aa-b047-4854-b8bb-652e1aae6017",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 46,
                "y": 28
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "3c4f02b8-8e33-4c28-9225-98652d2caf1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 24,
                "y": 28
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "0984b150-d961-4572-a6c9-e49ac00915e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 134,
                "y": 28
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "e041f36e-cf3a-4896-adf1-47570c4cd65d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 23,
                "y": 184
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f19e7fed-560b-4c12-aa0a-8e8674537dbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5ac70ad4-c12d-47e6-ade6-0ab1bacc6030",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "debf5e47-8bbb-4073-a782-0273bd33aad7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "50a6d4f2-40c0-4091-b2f4-a8eed728613d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 24,
                "offset": 0,
                "shift": 36,
                "w": 32,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "b4409618-1fa1-428a-bcac-1fc1a36db361",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 156,
                "y": 28
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "3eec519d-e99f-4056-b614-59f9a51fed32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d8541581-5af1-446c-9215-09597d3e320f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 178,
                "y": 28
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "eef9df2c-a639-4312-bc1f-15a9aabbbe3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 222,
                "y": 28
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b33c3c21-c423-49c1-bd84-05c055d3b3b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 2,
                "y": 54
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "7c7a9085-625f-4e4e-9e79-e99caebde139",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 24,
                "y": 54
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "859f48c5-1952-40fe-a35a-5a7f5d336f69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 46,
                "y": 54
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1bd89751-0534-448d-8377-58c3422f4cac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 68,
                "y": 54
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "d2a6e1d4-a8bb-4f4a-9b9e-eff5ca5d856f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 90,
                "y": 54
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "815b37a6-98d7-421f-84e3-8acde6a10dd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 24,
                "offset": 0,
                "shift": 36,
                "w": 32,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e3ebd48c-5d57-4dc9-b04b-1fc9aedc30d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 112,
                "y": 54
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "cc6f102c-f570-4493-bdb1-ac0b881460a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 134,
                "y": 54
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4d46a3e2-5d92-4c24-b079-37c4a1e086a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 156,
                "y": 54
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "5e464300-603e-40bc-a4ad-07ac7c0e686d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 157,
                "y": 158
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d3a1d0a1-bc02-4b57-bc55-3bc31e5feeb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 196,
                "y": 158
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "89e99d2a-183b-49f8-bf38-7f6d07ac8687",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 31,
                "y": 158
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "4d8b99b9-426d-42bd-a8d5-96c2ae147f15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 24,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 158
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "372a935a-1dce-4a4f-b1e1-dc06a80ae05c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 24,
                "offset": 0,
                "shift": 14,
                "w": 16,
                "x": 178,
                "y": 132
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "13d8dc37-17bf-4031-af0a-46fab749454c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 24,
                "offset": 5,
                "shift": 14,
                "w": 6,
                "x": 33,
                "y": 184
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "953a55a8-6270-4611-ac68-78f42bfc890d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 46,
                "y": 80
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "aca7a175-4bf5-4c66-8149-5aa0c1c16ea4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 68,
                "y": 106
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "63644820-0cc8-4051-a0eb-b1a99cb3ffcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 222,
                "y": 54
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ccd89bb7-51d1-4cee-9d18-502a17400ddd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 200,
                "y": 54
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ed159a90-5fa9-4825-834c-423d9b41f0de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 178,
                "y": 54
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "6c785b9a-9176-4b1b-a7a9-96d7ce3b1005",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "881350e1-04a5-44c2-90c4-18678c0c8a7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "01603065-5578-400e-be32-af5116d8123a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 68,
                "y": 80
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "e04ded7e-6d74-4f32-a527-a068f844eb33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 13,
                "y": 184
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "32b81993-9fe9-42cc-9624-fe006084ec37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 156,
                "y": 106
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "083d8939-62bd-40b0-a1e5-b91b03d730a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 178,
                "y": 106
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "7baab241-013d-45c5-8716-e220b6d8fa70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 200,
                "y": 106
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "3a93966f-ccad-4178-ae31-578dd25878a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 24,
                "offset": 0,
                "shift": 36,
                "w": 32,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "4950886e-deec-4598-a2e0-1d13b4173bb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 24,
                "y": 132
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "1867e472-87ed-48e7-bfef-3c0ff274e32e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 222,
                "y": 106
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "bfb23d25-03f9-4259-b5cf-fbe4fa960b0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 112,
                "y": 106
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "aa78f8b6-703b-4f72-88e6-34003ab6394f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 46,
                "y": 106
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "56e565d0-21f0-4c32-ad4e-59e1613f2c30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 24,
                "y": 106
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a67d7391-8580-4dd2-867a-ff4704bc5c69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 2,
                "y": 106
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "09278a0c-a892-4b81-9147-5109f474ceab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 222,
                "y": 80
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "01b61fdd-034e-41db-8b3c-9b6bbe9c2684",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 178,
                "y": 80
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "9f5bbedb-9343-4fb4-8286-4bf2fbd978fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 156,
                "y": 80
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "64afcce5-126d-49e1-be04-5f2cd0893252",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 24,
                "offset": 0,
                "shift": 36,
                "w": 32,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "55ebb11a-ef21-44f0-b699-ccdecf0fc420",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 134,
                "y": 80
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "a037b88b-9466-496a-86a7-bdd6f3d93647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 112,
                "y": 80
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "4fcab397-978b-411a-a822-0a6f02c7577c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 24,
                "offset": 0,
                "shift": 24,
                "w": 20,
                "x": 90,
                "y": 80
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "14d22912-6faa-437e-b3b2-41f2c5cef4b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 24,
                "offset": 3,
                "shift": 14,
                "w": 10,
                "x": 184,
                "y": 158
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "2e2ec5ab-f284-44dc-abdf-3cff3f63a150",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 24,
                "offset": 6,
                "shift": 14,
                "w": 3,
                "x": 79,
                "y": 184
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f8115612-7261-4dce-9850-43a4be28ccfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 24,
                "offset": 3,
                "shift": 14,
                "w": 10,
                "x": 208,
                "y": 158
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "3695b77e-20a5-44a5-849e-1db37564e774",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 24,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 129,
                "y": 158
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 18,
    "styleName": "Nominal",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}