{
    "id": "3a0f120c-4e27-4822-8e84-b341319c973f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_corpse_limb_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aeb38823-02cd-46ff-bcfb-8256e48aae90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a0f120c-4e27-4822-8e84-b341319c973f",
            "compositeImage": {
                "id": "a0de3981-d251-4450-9ca6-809055bda84c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aeb38823-02cd-46ff-bcfb-8256e48aae90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49212224-74d7-4023-ad2d-3d2e1ae64bec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aeb38823-02cd-46ff-bcfb-8256e48aae90",
                    "LayerId": "3759a4c8-2e5a-48c6-9f03-f826c4f18fa4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "3759a4c8-2e5a-48c6-9f03-f826c4f18fa4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a0f120c-4e27-4822-8e84-b341319c973f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 5
}