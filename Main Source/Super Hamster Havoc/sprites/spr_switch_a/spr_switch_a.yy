{
    "id": "b3dc7867-30e5-4cde-a1fe-348b35fc0291",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_switch_a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 9,
    "bbox_right": 90,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e85b7dd-c53a-4eba-9549-3f81f4b798f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3dc7867-30e5-4cde-a1fe-348b35fc0291",
            "compositeImage": {
                "id": "fe2721e1-57b4-44c7-b4df-12a2533fbb9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e85b7dd-c53a-4eba-9549-3f81f4b798f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b508507-a9a9-43e4-abee-e058c0452053",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e85b7dd-c53a-4eba-9549-3f81f4b798f7",
                    "LayerId": "12285c93-75f8-41fd-86dc-9068d970fe8c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "12285c93-75f8-41fd-86dc-9068d970fe8c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3dc7867-30e5-4cde-a1fe-348b35fc0291",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}