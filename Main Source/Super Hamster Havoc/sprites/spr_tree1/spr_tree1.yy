{
    "id": "686688df-a2ed-41d6-a5b9-efc5b565b2c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tree1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 180,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "42369325-6853-49c3-b934-815d10e1b79e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "686688df-a2ed-41d6-a5b9-efc5b565b2c8",
            "compositeImage": {
                "id": "6475be97-65bf-492e-93ce-bc51a58ee107",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42369325-6853-49c3-b934-815d10e1b79e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d903fcf-4cd6-416a-b5d0-71c4370717ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42369325-6853-49c3-b934-815d10e1b79e",
                    "LayerId": "fde5a591-531c-4702-ae92-c1ffa3ca0cd0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "fde5a591-531c-4702-ae92-c1ffa3ca0cd0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "686688df-a2ed-41d6-a5b9-efc5b565b2c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}