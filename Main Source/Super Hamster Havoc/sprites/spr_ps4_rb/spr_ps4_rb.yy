{
    "id": "476e3ebd-faa4-4860-badb-36719c5304fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ps4_rb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 78,
    "bbox_left": 9,
    "bbox_right": 90,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9b27e44-da60-4ca0-92c4-08d9e8710839",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "476e3ebd-faa4-4860-badb-36719c5304fc",
            "compositeImage": {
                "id": "7a73ef4d-b3db-4989-a6e5-943187f05afa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9b27e44-da60-4ca0-92c4-08d9e8710839",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddc040c6-f4f7-4612-9033-9b72f53da0fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9b27e44-da60-4ca0-92c4-08d9e8710839",
                    "LayerId": "15cf96a0-1ef9-4832-84ca-5b5df3057e79"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "15cf96a0-1ef9-4832-84ca-5b5df3057e79",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "476e3ebd-faa4-4860-badb-36719c5304fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}