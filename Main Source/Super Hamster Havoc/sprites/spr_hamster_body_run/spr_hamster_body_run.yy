{
    "id": "7a75fa8a-1079-4cd1-a6be-350c23a406c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_body_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 3,
    "bbox_right": 42,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b33be34e-ac7a-4577-b03c-359bdccba645",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a75fa8a-1079-4cd1-a6be-350c23a406c9",
            "compositeImage": {
                "id": "ea307d96-370d-41cf-8404-86ae4d1c2a59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b33be34e-ac7a-4577-b03c-359bdccba645",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e6fc596-46e5-4c53-a36e-d7fc7fd6843f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b33be34e-ac7a-4577-b03c-359bdccba645",
                    "LayerId": "69234551-1ed1-4ab0-ada3-27f66112ed44"
                }
            ]
        },
        {
            "id": "6943b867-5a7c-4381-918c-4ed1840804c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a75fa8a-1079-4cd1-a6be-350c23a406c9",
            "compositeImage": {
                "id": "3e2bf899-47de-4669-9904-38d02493598b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6943b867-5a7c-4381-918c-4ed1840804c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d0b8a1f-d5c0-4024-841e-0b375a3c9360",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6943b867-5a7c-4381-918c-4ed1840804c1",
                    "LayerId": "69234551-1ed1-4ab0-ada3-27f66112ed44"
                }
            ]
        },
        {
            "id": "df9a1ed5-d7e7-40ca-a258-68e29b9af088",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a75fa8a-1079-4cd1-a6be-350c23a406c9",
            "compositeImage": {
                "id": "b24d7d4c-93cb-4a3d-8d44-be5a4d490782",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df9a1ed5-d7e7-40ca-a258-68e29b9af088",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "672369b7-63a2-4571-b565-41124deadab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df9a1ed5-d7e7-40ca-a258-68e29b9af088",
                    "LayerId": "69234551-1ed1-4ab0-ada3-27f66112ed44"
                }
            ]
        },
        {
            "id": "617b5ebe-aaad-400a-981e-c6e28a78794f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a75fa8a-1079-4cd1-a6be-350c23a406c9",
            "compositeImage": {
                "id": "76748149-7a7d-4d01-8dea-adc4970c5101",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "617b5ebe-aaad-400a-981e-c6e28a78794f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "559f3adc-e30c-409a-93ec-edcfccb7ba5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "617b5ebe-aaad-400a-981e-c6e28a78794f",
                    "LayerId": "69234551-1ed1-4ab0-ada3-27f66112ed44"
                }
            ]
        },
        {
            "id": "1d08342b-9edc-4738-af64-c81fbd1fbcc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a75fa8a-1079-4cd1-a6be-350c23a406c9",
            "compositeImage": {
                "id": "ca230b68-4701-4a44-9a0b-713c4b37be58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d08342b-9edc-4738-af64-c81fbd1fbcc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbb817f0-f3ae-4291-9321-d002d7d478c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d08342b-9edc-4738-af64-c81fbd1fbcc4",
                    "LayerId": "69234551-1ed1-4ab0-ada3-27f66112ed44"
                }
            ]
        },
        {
            "id": "5a40fe4c-5022-477e-b3a6-b515534021e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a75fa8a-1079-4cd1-a6be-350c23a406c9",
            "compositeImage": {
                "id": "8b932d42-7f83-4ce6-b371-b8eeddd487f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a40fe4c-5022-477e-b3a6-b515534021e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c44dfebe-5ca7-4efa-995b-8a18c01ba1d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a40fe4c-5022-477e-b3a6-b515534021e2",
                    "LayerId": "69234551-1ed1-4ab0-ada3-27f66112ed44"
                }
            ]
        },
        {
            "id": "4c941eeb-3fef-4994-ada4-deb153068cca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a75fa8a-1079-4cd1-a6be-350c23a406c9",
            "compositeImage": {
                "id": "0e6ffafc-79e8-4b49-ab6a-0765e01e40e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c941eeb-3fef-4994-ada4-deb153068cca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "875e3f78-e73b-4f97-8546-b77a4642afce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c941eeb-3fef-4994-ada4-deb153068cca",
                    "LayerId": "69234551-1ed1-4ab0-ada3-27f66112ed44"
                }
            ]
        },
        {
            "id": "15c39028-6eb5-4a73-b5ae-bb5d93c37f78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a75fa8a-1079-4cd1-a6be-350c23a406c9",
            "compositeImage": {
                "id": "6b356131-86c7-4a81-adbb-03a67f05975a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15c39028-6eb5-4a73-b5ae-bb5d93c37f78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e72dbd83-cdae-4e2a-815f-3268f54d64a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15c39028-6eb5-4a73-b5ae-bb5d93c37f78",
                    "LayerId": "69234551-1ed1-4ab0-ada3-27f66112ed44"
                }
            ]
        },
        {
            "id": "51770969-0ecb-4933-a10b-03ffa6414485",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a75fa8a-1079-4cd1-a6be-350c23a406c9",
            "compositeImage": {
                "id": "4523ab14-5534-41f7-8eef-495cbca0ae12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51770969-0ecb-4933-a10b-03ffa6414485",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a30a4d4-e4ef-4096-a10c-102820108900",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51770969-0ecb-4933-a10b-03ffa6414485",
                    "LayerId": "69234551-1ed1-4ab0-ada3-27f66112ed44"
                }
            ]
        },
        {
            "id": "84613051-516e-4ce3-a935-6d7a7daf469c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a75fa8a-1079-4cd1-a6be-350c23a406c9",
            "compositeImage": {
                "id": "d33988f6-dc71-48ed-9779-aaf808ed2b34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84613051-516e-4ce3-a935-6d7a7daf469c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b419ee7a-b3d2-4359-a553-df1b60fb1611",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84613051-516e-4ce3-a935-6d7a7daf469c",
                    "LayerId": "69234551-1ed1-4ab0-ada3-27f66112ed44"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 51,
    "layers": [
        {
            "id": "69234551-1ed1-4ab0-ada3-27f66112ed44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a75fa8a-1079-4cd1-a6be-350c23a406c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 25
}