{
    "id": "0f9660f4-13b0-4515-9c61-837e541b1f74",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_0_head_r_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 11,
    "bbox_right": 49,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de525ed2-ec39-49ca-9d55-f5a9b719bbe0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f9660f4-13b0-4515-9c61-837e541b1f74",
            "compositeImage": {
                "id": "93fe5d58-d140-40bc-b283-fa5073941e2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de525ed2-ec39-49ca-9d55-f5a9b719bbe0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba0d0952-38a1-45de-80e6-13ae873ca43b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de525ed2-ec39-49ca-9d55-f5a9b719bbe0",
                    "LayerId": "cbdeddff-75bd-4cd3-a7de-e585b12a1e28"
                }
            ]
        },
        {
            "id": "f14d1c16-3300-49dd-b4f5-e84e2af630d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f9660f4-13b0-4515-9c61-837e541b1f74",
            "compositeImage": {
                "id": "5e18977a-fef6-49e1-a768-db83783a0a3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f14d1c16-3300-49dd-b4f5-e84e2af630d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f37a373-1aa2-4154-b7c4-105351b56b51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f14d1c16-3300-49dd-b4f5-e84e2af630d8",
                    "LayerId": "cbdeddff-75bd-4cd3-a7de-e585b12a1e28"
                }
            ]
        },
        {
            "id": "8e230b74-9efd-425a-855f-08d4a7991f1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f9660f4-13b0-4515-9c61-837e541b1f74",
            "compositeImage": {
                "id": "5256b1c6-0d10-44c8-acd2-348afaa8edf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e230b74-9efd-425a-855f-08d4a7991f1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37c55a05-1338-44c6-9c53-5a0b1b0ac37d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e230b74-9efd-425a-855f-08d4a7991f1f",
                    "LayerId": "cbdeddff-75bd-4cd3-a7de-e585b12a1e28"
                }
            ]
        },
        {
            "id": "e867f729-57cf-43bb-a393-8ff6f841cad1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f9660f4-13b0-4515-9c61-837e541b1f74",
            "compositeImage": {
                "id": "0481f71e-79e7-4e64-99f7-d408ffb7c5d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e867f729-57cf-43bb-a393-8ff6f841cad1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c583151-31c3-42dc-9751-c840710332ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e867f729-57cf-43bb-a393-8ff6f841cad1",
                    "LayerId": "cbdeddff-75bd-4cd3-a7de-e585b12a1e28"
                }
            ]
        },
        {
            "id": "7628050e-2f32-4d7b-be9a-8e0a2d762010",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f9660f4-13b0-4515-9c61-837e541b1f74",
            "compositeImage": {
                "id": "d90a4701-db97-420c-afc8-fca87246e6bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7628050e-2f32-4d7b-be9a-8e0a2d762010",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "972e34fc-182d-400d-9ed8-24a142c59bce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7628050e-2f32-4d7b-be9a-8e0a2d762010",
                    "LayerId": "cbdeddff-75bd-4cd3-a7de-e585b12a1e28"
                }
            ]
        },
        {
            "id": "6cd065ee-ea36-4ebe-bda3-7a36384db542",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f9660f4-13b0-4515-9c61-837e541b1f74",
            "compositeImage": {
                "id": "2966bc77-b6f5-4182-8507-13d757c9a829",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cd065ee-ea36-4ebe-bda3-7a36384db542",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e882d180-b20b-4897-b331-ea552c45c44e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cd065ee-ea36-4ebe-bda3-7a36384db542",
                    "LayerId": "cbdeddff-75bd-4cd3-a7de-e585b12a1e28"
                }
            ]
        },
        {
            "id": "c28e6f9b-68cf-4777-af0d-220cf98a00a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f9660f4-13b0-4515-9c61-837e541b1f74",
            "compositeImage": {
                "id": "4feb071a-6ac3-4a22-b211-81d52152cd7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c28e6f9b-68cf-4777-af0d-220cf98a00a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8535b727-ad1c-48ec-9d01-4e2f3e765d5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c28e6f9b-68cf-4777-af0d-220cf98a00a5",
                    "LayerId": "cbdeddff-75bd-4cd3-a7de-e585b12a1e28"
                }
            ]
        },
        {
            "id": "73597ec2-0fa4-401c-ae58-03b4d4ffe3b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f9660f4-13b0-4515-9c61-837e541b1f74",
            "compositeImage": {
                "id": "bcde5a99-b3f3-4fd4-b025-5d78563a2083",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73597ec2-0fa4-401c-ae58-03b4d4ffe3b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1672bfa-1617-43da-aece-bb7bb82c1ca5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73597ec2-0fa4-401c-ae58-03b4d4ffe3b3",
                    "LayerId": "cbdeddff-75bd-4cd3-a7de-e585b12a1e28"
                }
            ]
        },
        {
            "id": "7b36db6c-5b19-4d89-83ac-f91829766388",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f9660f4-13b0-4515-9c61-837e541b1f74",
            "compositeImage": {
                "id": "642ddd93-eaa6-488a-85ef-a006101f7d41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b36db6c-5b19-4d89-83ac-f91829766388",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "698eb25a-4954-4485-9426-3d54d7e7477e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b36db6c-5b19-4d89-83ac-f91829766388",
                    "LayerId": "cbdeddff-75bd-4cd3-a7de-e585b12a1e28"
                }
            ]
        },
        {
            "id": "712e93c4-a549-48d7-bdf8-c8d457d50a56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f9660f4-13b0-4515-9c61-837e541b1f74",
            "compositeImage": {
                "id": "13ad4940-76d2-49d1-bef6-e09bbe4ccd66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "712e93c4-a549-48d7-bdf8-c8d457d50a56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f21415d6-77e7-4771-9e92-4b8b173a4a5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "712e93c4-a549-48d7-bdf8-c8d457d50a56",
                    "LayerId": "cbdeddff-75bd-4cd3-a7de-e585b12a1e28"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cbdeddff-75bd-4cd3-a7de-e585b12a1e28",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f9660f4-13b0-4515-9c61-837e541b1f74",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}