{
    "id": "6a9a618e-c68d-498a-a117-faa206e8fe23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bfaebd8a-cdda-46bf-a281-fe85b58c3707",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "compositeImage": {
                "id": "a92902ad-a2f3-4866-8a22-5423c570bdea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfaebd8a-cdda-46bf-a281-fe85b58c3707",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e616047-d93b-4ad2-ad2e-c4177fed5a4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfaebd8a-cdda-46bf-a281-fe85b58c3707",
                    "LayerId": "275206f7-2920-449f-90de-3bc393691327"
                }
            ]
        },
        {
            "id": "5aec68a7-2d1b-4ab4-9977-febdce06923d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "compositeImage": {
                "id": "1bc340bf-3df2-4a9c-8192-77081584efc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5aec68a7-2d1b-4ab4-9977-febdce06923d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64307d16-c5f7-4aee-b670-5444da162cd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5aec68a7-2d1b-4ab4-9977-febdce06923d",
                    "LayerId": "275206f7-2920-449f-90de-3bc393691327"
                }
            ]
        },
        {
            "id": "7994ee63-bf1f-4f9e-b1e2-675d3aa1b56f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "compositeImage": {
                "id": "7c47224e-efe2-4ed9-94b2-614328ddee9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7994ee63-bf1f-4f9e-b1e2-675d3aa1b56f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d11e15a-593c-4c59-b14c-71bfff5c04f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7994ee63-bf1f-4f9e-b1e2-675d3aa1b56f",
                    "LayerId": "275206f7-2920-449f-90de-3bc393691327"
                }
            ]
        },
        {
            "id": "40fd67ff-dcdb-447a-8290-66164ce3f669",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "compositeImage": {
                "id": "61b28ef9-bbc4-4a01-a816-7b320195f941",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40fd67ff-dcdb-447a-8290-66164ce3f669",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50859844-04a2-4326-91bc-abfa782985a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40fd67ff-dcdb-447a-8290-66164ce3f669",
                    "LayerId": "275206f7-2920-449f-90de-3bc393691327"
                }
            ]
        },
        {
            "id": "5760a43c-23f4-49a4-8142-a1746261b6b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "compositeImage": {
                "id": "b2d04953-f86b-4a37-9ddd-bb8d17eaf29f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5760a43c-23f4-49a4-8142-a1746261b6b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1a3226d-f80d-4a06-a13e-303ed0537718",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5760a43c-23f4-49a4-8142-a1746261b6b1",
                    "LayerId": "275206f7-2920-449f-90de-3bc393691327"
                }
            ]
        },
        {
            "id": "c0abdbae-2803-4c50-b32a-7e73a01ff5fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "compositeImage": {
                "id": "567a211c-2f9f-4b7e-8a50-928eac77d4f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0abdbae-2803-4c50-b32a-7e73a01ff5fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "452cfa67-d0bb-4921-9985-dc1dbf666203",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0abdbae-2803-4c50-b32a-7e73a01ff5fc",
                    "LayerId": "275206f7-2920-449f-90de-3bc393691327"
                }
            ]
        },
        {
            "id": "ad5d47aa-ffaf-4e59-bb8b-2e5473f40634",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "compositeImage": {
                "id": "f8825744-ec3e-41e3-ade7-cbcc6def5cff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad5d47aa-ffaf-4e59-bb8b-2e5473f40634",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e182fef-c531-4ca4-aae1-915d62329312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad5d47aa-ffaf-4e59-bb8b-2e5473f40634",
                    "LayerId": "275206f7-2920-449f-90de-3bc393691327"
                }
            ]
        },
        {
            "id": "be5a6542-5a4c-44a7-abfc-49aae005c57e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "compositeImage": {
                "id": "3da68257-00fc-486c-8fe1-ca21bd552ac7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be5a6542-5a4c-44a7-abfc-49aae005c57e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4c4a987-e405-4942-805d-524a628a6166",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be5a6542-5a4c-44a7-abfc-49aae005c57e",
                    "LayerId": "275206f7-2920-449f-90de-3bc393691327"
                }
            ]
        },
        {
            "id": "2ae582ce-9e2e-4f05-9ec2-32604a54ac06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "compositeImage": {
                "id": "f03cd99f-a089-4f23-a3b7-1e0ab3a238be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ae582ce-9e2e-4f05-9ec2-32604a54ac06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "815a6506-bb68-4323-ab70-51d716409d93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ae582ce-9e2e-4f05-9ec2-32604a54ac06",
                    "LayerId": "275206f7-2920-449f-90de-3bc393691327"
                }
            ]
        },
        {
            "id": "1e27e6e2-920e-4713-b191-238cb36f7121",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "compositeImage": {
                "id": "bc0726de-36ec-4a23-80c0-434ec40d5f7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e27e6e2-920e-4713-b191-238cb36f7121",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "562e3b2b-5034-4373-b5a5-204ca84f705f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e27e6e2-920e-4713-b191-238cb36f7121",
                    "LayerId": "275206f7-2920-449f-90de-3bc393691327"
                }
            ]
        },
        {
            "id": "d336cb3f-0647-4ca8-b5a4-c684f248c303",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "compositeImage": {
                "id": "f266c474-a79b-4275-8d17-98f50a72513f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d336cb3f-0647-4ca8-b5a4-c684f248c303",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8ea7757-7484-4050-b084-9307b6587505",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d336cb3f-0647-4ca8-b5a4-c684f248c303",
                    "LayerId": "275206f7-2920-449f-90de-3bc393691327"
                }
            ]
        },
        {
            "id": "02083d00-6f38-4f2f-9e54-aa05ddee6437",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "compositeImage": {
                "id": "57007da5-d600-467c-a5e3-37c767be226e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02083d00-6f38-4f2f-9e54-aa05ddee6437",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e8bb451-e0a3-4381-a137-b77530801d36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02083d00-6f38-4f2f-9e54-aa05ddee6437",
                    "LayerId": "275206f7-2920-449f-90de-3bc393691327"
                }
            ]
        },
        {
            "id": "d118b4f0-542e-4a84-9e75-704cb0996bc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "compositeImage": {
                "id": "9305ab2c-b0b0-42b0-9905-2f9ffdce6e2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d118b4f0-542e-4a84-9e75-704cb0996bc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e8dae56-07cd-49e0-80e4-839464d060a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d118b4f0-542e-4a84-9e75-704cb0996bc0",
                    "LayerId": "275206f7-2920-449f-90de-3bc393691327"
                }
            ]
        },
        {
            "id": "3f3b6a90-0a66-40a4-b652-d6642131b373",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "compositeImage": {
                "id": "807dc526-9034-4ca7-8894-bc2f9466adfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f3b6a90-0a66-40a4-b652-d6642131b373",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b3176bf-e192-4c51-ab01-aa695df260a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f3b6a90-0a66-40a4-b652-d6642131b373",
                    "LayerId": "275206f7-2920-449f-90de-3bc393691327"
                }
            ]
        },
        {
            "id": "7e78b7db-504d-4c36-b207-c795e48bfb28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "compositeImage": {
                "id": "8c0e3d60-5bfa-488c-953b-9c617b91f68d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e78b7db-504d-4c36-b207-c795e48bfb28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8616685e-9993-4dbb-8e7d-a57c2b6c4c20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e78b7db-504d-4c36-b207-c795e48bfb28",
                    "LayerId": "275206f7-2920-449f-90de-3bc393691327"
                }
            ]
        },
        {
            "id": "c5b1775c-73e0-480a-875d-60bb5aa27e02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "compositeImage": {
                "id": "67ab48bc-4230-440e-a473-a05f384db3cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5b1775c-73e0-480a-875d-60bb5aa27e02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9643009b-60a5-4906-b134-3a7cda12121f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5b1775c-73e0-480a-875d-60bb5aa27e02",
                    "LayerId": "275206f7-2920-449f-90de-3bc393691327"
                }
            ]
        },
        {
            "id": "2b826ada-f0af-4a1a-a756-8fb32f9a2e28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "compositeImage": {
                "id": "ad663d0e-83ef-465f-87eb-221bafb9bdbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b826ada-f0af-4a1a-a756-8fb32f9a2e28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ea52474-21a1-4475-93cc-0859e4c28c4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b826ada-f0af-4a1a-a756-8fb32f9a2e28",
                    "LayerId": "275206f7-2920-449f-90de-3bc393691327"
                }
            ]
        },
        {
            "id": "ee2e7c47-60a9-4fc9-bcaf-43c7c3ee72e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "compositeImage": {
                "id": "817023c9-f3bc-4977-8d79-2527f17eb80d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee2e7c47-60a9-4fc9-bcaf-43c7c3ee72e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6660b12e-e4d9-4c1b-b3b4-e88af9daafde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee2e7c47-60a9-4fc9-bcaf-43c7c3ee72e6",
                    "LayerId": "275206f7-2920-449f-90de-3bc393691327"
                }
            ]
        },
        {
            "id": "03527da9-2adc-449f-af67-f29b4e074850",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "compositeImage": {
                "id": "14bcc952-334c-4c41-8721-2e5bacab59b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03527da9-2adc-449f-af67-f29b4e074850",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f52f8e22-2cb1-4cc3-8a78-b27a16dffeb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03527da9-2adc-449f-af67-f29b4e074850",
                    "LayerId": "275206f7-2920-449f-90de-3bc393691327"
                }
            ]
        },
        {
            "id": "5375619e-9bb2-41d3-a88d-36c16d1b782f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "compositeImage": {
                "id": "9b1de440-7a57-47b4-97d5-4de11515a125",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5375619e-9bb2-41d3-a88d-36c16d1b782f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e74b59a-ea1f-4196-a17e-16558ebc5551",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5375619e-9bb2-41d3-a88d-36c16d1b782f",
                    "LayerId": "275206f7-2920-449f-90de-3bc393691327"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "275206f7-2920-449f-90de-3bc393691327",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a9a618e-c68d-498a-a117-faa206e8fe23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}