{
    "id": "ebfa854a-20fd-4494-a54d-d806638509e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_switch_rb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 12,
    "bbox_right": 87,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b857f32-0861-46ea-9ddd-d13741b90c81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebfa854a-20fd-4494-a54d-d806638509e1",
            "compositeImage": {
                "id": "df6c56cd-2fdf-40e2-9cd7-4180da867fe8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b857f32-0861-46ea-9ddd-d13741b90c81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44924027-90c7-439e-9040-3629c4c3ef5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b857f32-0861-46ea-9ddd-d13741b90c81",
                    "LayerId": "6cdf93ca-64b4-4970-a6c8-18485c9957a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "6cdf93ca-64b4-4970-a6c8-18485c9957a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebfa854a-20fd-4494-a54d-d806638509e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}