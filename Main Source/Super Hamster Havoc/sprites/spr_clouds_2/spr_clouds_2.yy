{
    "id": "bb515f02-6ef0-4513-9670-2370fdc9f668",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_clouds_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 143,
    "bbox_left": 0,
    "bbox_right": 2399,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7e51148-d4e2-4b70-9538-7e81a6872342",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb515f02-6ef0-4513-9670-2370fdc9f668",
            "compositeImage": {
                "id": "89c39b69-1ac3-4789-8d0f-b12174a5295b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7e51148-d4e2-4b70-9538-7e81a6872342",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39797116-6de1-45eb-b67c-e6430d461325",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7e51148-d4e2-4b70-9538-7e81a6872342",
                    "LayerId": "615da97b-48ad-41c9-88ff-b797aa07c7c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 780,
    "layers": [
        {
            "id": "615da97b-48ad-41c9-88ff-b797aa07c7c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb515f02-6ef0-4513-9670-2370fdc9f668",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2400,
    "xorig": 1200,
    "yorig": 390
}