{
    "id": "c0df1b8c-d321-46dc-82a5-b342f651fe2a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ricochet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "070a4f27-b5e1-43fd-8d84-710b8148650c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0df1b8c-d321-46dc-82a5-b342f651fe2a",
            "compositeImage": {
                "id": "4edeeb0c-e132-40ec-b3d0-7a2675ab02c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "070a4f27-b5e1-43fd-8d84-710b8148650c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ae58b4c-a903-445b-a250-e1bea8d960ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "070a4f27-b5e1-43fd-8d84-710b8148650c",
                    "LayerId": "56142ba4-9ecb-4080-9101-9ef8f1a8b3e2"
                }
            ]
        },
        {
            "id": "101dbb31-4078-451f-b10d-4be6b8f42440",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0df1b8c-d321-46dc-82a5-b342f651fe2a",
            "compositeImage": {
                "id": "62472097-f0aa-4103-bdad-321eeb3d83f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "101dbb31-4078-451f-b10d-4be6b8f42440",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2212ebfc-b6b3-44de-9021-c7d6b27cda86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "101dbb31-4078-451f-b10d-4be6b8f42440",
                    "LayerId": "56142ba4-9ecb-4080-9101-9ef8f1a8b3e2"
                }
            ]
        },
        {
            "id": "0aceefb3-2588-437f-954e-2f0cfbb87948",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0df1b8c-d321-46dc-82a5-b342f651fe2a",
            "compositeImage": {
                "id": "2f5b9b6c-401a-4137-9c94-6236f771e750",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0aceefb3-2588-437f-954e-2f0cfbb87948",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c50f5aee-973e-4f1a-b98c-d02268c9a98b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0aceefb3-2588-437f-954e-2f0cfbb87948",
                    "LayerId": "56142ba4-9ecb-4080-9101-9ef8f1a8b3e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "56142ba4-9ecb-4080-9101-9ef8f1a8b3e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0df1b8c-d321-46dc-82a5-b342f651fe2a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}