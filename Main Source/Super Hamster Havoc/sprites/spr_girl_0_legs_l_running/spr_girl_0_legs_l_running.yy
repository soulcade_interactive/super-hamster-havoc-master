{
    "id": "87aeacb5-7c0d-43e5-b18c-d893663650ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_0_legs_l_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 44,
    "bbox_top": 55,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e15c376c-3fea-475c-84a1-fcaa621c0b33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87aeacb5-7c0d-43e5-b18c-d893663650ae",
            "compositeImage": {
                "id": "9bac9fb7-6c4c-49d7-b0e5-c9f18b76a356",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e15c376c-3fea-475c-84a1-fcaa621c0b33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6a086ad-e912-4ca8-892a-4aeb20f0c2eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e15c376c-3fea-475c-84a1-fcaa621c0b33",
                    "LayerId": "5a8e0983-6b63-4f88-a1c0-825772430213"
                }
            ]
        },
        {
            "id": "bf522787-e977-4949-8760-af2dc51e9472",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87aeacb5-7c0d-43e5-b18c-d893663650ae",
            "compositeImage": {
                "id": "5620a748-aca4-4cc8-8ade-f5523d5753b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf522787-e977-4949-8760-af2dc51e9472",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dca81135-3291-44f9-93bd-166f8daf7085",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf522787-e977-4949-8760-af2dc51e9472",
                    "LayerId": "5a8e0983-6b63-4f88-a1c0-825772430213"
                }
            ]
        },
        {
            "id": "875cd3bc-b205-4c3b-b680-0d8e361ca41e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87aeacb5-7c0d-43e5-b18c-d893663650ae",
            "compositeImage": {
                "id": "80373333-90fd-4cdf-9163-f5936f248814",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "875cd3bc-b205-4c3b-b680-0d8e361ca41e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1083ec93-b302-4f67-98f2-7b99a218d571",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "875cd3bc-b205-4c3b-b680-0d8e361ca41e",
                    "LayerId": "5a8e0983-6b63-4f88-a1c0-825772430213"
                }
            ]
        },
        {
            "id": "783fbaa2-4bed-4e48-8847-799ba2be5382",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87aeacb5-7c0d-43e5-b18c-d893663650ae",
            "compositeImage": {
                "id": "c660beb2-4a0a-47e6-9a82-a859aab6d10e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "783fbaa2-4bed-4e48-8847-799ba2be5382",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43d8c7c3-f8ac-4c2b-a6df-e50cd1c5c441",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "783fbaa2-4bed-4e48-8847-799ba2be5382",
                    "LayerId": "5a8e0983-6b63-4f88-a1c0-825772430213"
                }
            ]
        },
        {
            "id": "3534afb3-2204-4485-aebe-9befc0d59c2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87aeacb5-7c0d-43e5-b18c-d893663650ae",
            "compositeImage": {
                "id": "1ab18ea1-36ce-4dcd-b688-1ecc82b07e70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3534afb3-2204-4485-aebe-9befc0d59c2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e1dd63f-3a00-4ac6-bab5-e6b3f0c28948",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3534afb3-2204-4485-aebe-9befc0d59c2c",
                    "LayerId": "5a8e0983-6b63-4f88-a1c0-825772430213"
                }
            ]
        },
        {
            "id": "eb1a71aa-95be-430b-be33-74a62af0f070",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87aeacb5-7c0d-43e5-b18c-d893663650ae",
            "compositeImage": {
                "id": "96c94137-cea5-40f5-b74a-23350f6ab27d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb1a71aa-95be-430b-be33-74a62af0f070",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bde0465-fc0d-413a-90b9-a95df3aef0d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb1a71aa-95be-430b-be33-74a62af0f070",
                    "LayerId": "5a8e0983-6b63-4f88-a1c0-825772430213"
                }
            ]
        },
        {
            "id": "2fd30cf6-38de-4b3d-b512-6d34aba9fda8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87aeacb5-7c0d-43e5-b18c-d893663650ae",
            "compositeImage": {
                "id": "ad6f5491-72b3-472d-bbfd-a670b51af22d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fd30cf6-38de-4b3d-b512-6d34aba9fda8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "353f0e87-cf24-43d3-8103-20c19db690d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fd30cf6-38de-4b3d-b512-6d34aba9fda8",
                    "LayerId": "5a8e0983-6b63-4f88-a1c0-825772430213"
                }
            ]
        },
        {
            "id": "5b883d02-7be6-4255-ad3c-079eca05dccc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87aeacb5-7c0d-43e5-b18c-d893663650ae",
            "compositeImage": {
                "id": "00928b3a-2b2d-405d-aad4-2e5bdb82081c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b883d02-7be6-4255-ad3c-079eca05dccc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57e710ec-fd8b-4f92-8840-f52539ffb7eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b883d02-7be6-4255-ad3c-079eca05dccc",
                    "LayerId": "5a8e0983-6b63-4f88-a1c0-825772430213"
                }
            ]
        },
        {
            "id": "66a6f7b4-7a5e-4ca6-8acf-bd65667809eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87aeacb5-7c0d-43e5-b18c-d893663650ae",
            "compositeImage": {
                "id": "b0d3a2ac-6a35-4fe7-85b8-a82670924e34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66a6f7b4-7a5e-4ca6-8acf-bd65667809eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cd59b91-1e4f-4909-b8be-07ed343defc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66a6f7b4-7a5e-4ca6-8acf-bd65667809eb",
                    "LayerId": "5a8e0983-6b63-4f88-a1c0-825772430213"
                }
            ]
        },
        {
            "id": "2f6ac43b-46d2-4704-a0e7-865eed9108ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87aeacb5-7c0d-43e5-b18c-d893663650ae",
            "compositeImage": {
                "id": "8c68fdc6-d3a7-4762-b151-b235128fba55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f6ac43b-46d2-4704-a0e7-865eed9108ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c583371-a68e-4985-90c4-2fe8d99c11d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f6ac43b-46d2-4704-a0e7-865eed9108ab",
                    "LayerId": "5a8e0983-6b63-4f88-a1c0-825772430213"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5a8e0983-6b63-4f88-a1c0-825772430213",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87aeacb5-7c0d-43e5-b18c-d893663650ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}