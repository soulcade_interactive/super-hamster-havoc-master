{
    "id": "b785ea8f-a9ec-46b7-beb7-c8fa03edfa6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_body_run_brown_and_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae5f88eb-553d-4d77-aa20-ea0534e82cd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b785ea8f-a9ec-46b7-beb7-c8fa03edfa6e",
            "compositeImage": {
                "id": "1e4cfcd7-c249-4b18-acf1-11f49db02e73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae5f88eb-553d-4d77-aa20-ea0534e82cd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b126909-7556-46ae-974c-db1bc8fd7afc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae5f88eb-553d-4d77-aa20-ea0534e82cd9",
                    "LayerId": "6cdd9663-95e9-4b8a-9b20-1592950b394a"
                }
            ]
        },
        {
            "id": "250b9dac-2d38-4c86-ac86-f8f0e29cbbd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b785ea8f-a9ec-46b7-beb7-c8fa03edfa6e",
            "compositeImage": {
                "id": "fe2089ec-b4eb-4e0f-85bd-0621153d4b72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "250b9dac-2d38-4c86-ac86-f8f0e29cbbd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed813228-9921-4333-bc24-b49d025de69d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "250b9dac-2d38-4c86-ac86-f8f0e29cbbd3",
                    "LayerId": "6cdd9663-95e9-4b8a-9b20-1592950b394a"
                }
            ]
        },
        {
            "id": "61d3dd84-a6c8-4447-9b26-dcec230e3ed4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b785ea8f-a9ec-46b7-beb7-c8fa03edfa6e",
            "compositeImage": {
                "id": "57aa386c-2fc6-4a3b-bfed-80f66db40d71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61d3dd84-a6c8-4447-9b26-dcec230e3ed4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d126664-6e57-4e00-92fc-5e3702c77ad4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61d3dd84-a6c8-4447-9b26-dcec230e3ed4",
                    "LayerId": "6cdd9663-95e9-4b8a-9b20-1592950b394a"
                }
            ]
        },
        {
            "id": "f99b4b8a-9ebc-42fb-a2b2-d60c7bdb18e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b785ea8f-a9ec-46b7-beb7-c8fa03edfa6e",
            "compositeImage": {
                "id": "1365692a-8de9-4c74-a2db-5d0422157904",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f99b4b8a-9ebc-42fb-a2b2-d60c7bdb18e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55339717-5056-4f8b-85dd-5aa2ea086b8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f99b4b8a-9ebc-42fb-a2b2-d60c7bdb18e9",
                    "LayerId": "6cdd9663-95e9-4b8a-9b20-1592950b394a"
                }
            ]
        },
        {
            "id": "f42c634a-a3b0-45e4-a94a-9b2db3b4204c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b785ea8f-a9ec-46b7-beb7-c8fa03edfa6e",
            "compositeImage": {
                "id": "15202f00-c11e-4b47-bc0b-04ce487457da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f42c634a-a3b0-45e4-a94a-9b2db3b4204c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32838860-8391-440b-83f8-de6d51ac5d5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f42c634a-a3b0-45e4-a94a-9b2db3b4204c",
                    "LayerId": "6cdd9663-95e9-4b8a-9b20-1592950b394a"
                }
            ]
        },
        {
            "id": "fb34e105-6ab7-431f-83c9-dc76cd9185e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b785ea8f-a9ec-46b7-beb7-c8fa03edfa6e",
            "compositeImage": {
                "id": "b0e9a78d-24da-4abf-b0ac-b8fb868ba760",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb34e105-6ab7-431f-83c9-dc76cd9185e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7ed9274-c97a-4138-98c9-137f66d2efd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb34e105-6ab7-431f-83c9-dc76cd9185e9",
                    "LayerId": "6cdd9663-95e9-4b8a-9b20-1592950b394a"
                }
            ]
        },
        {
            "id": "8fa07575-77cb-430b-82fa-d2f0985206ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b785ea8f-a9ec-46b7-beb7-c8fa03edfa6e",
            "compositeImage": {
                "id": "5e922ee3-2438-4cb2-9c4d-0fc0ed487c12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fa07575-77cb-430b-82fa-d2f0985206ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b0b82b9-56e3-4d55-8271-78dabeddabc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fa07575-77cb-430b-82fa-d2f0985206ec",
                    "LayerId": "6cdd9663-95e9-4b8a-9b20-1592950b394a"
                }
            ]
        },
        {
            "id": "ebe9259b-7186-40b9-b955-3c18005c1d83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b785ea8f-a9ec-46b7-beb7-c8fa03edfa6e",
            "compositeImage": {
                "id": "041b1aae-1def-416c-9c36-e79ca5db38c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebe9259b-7186-40b9-b955-3c18005c1d83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70562d11-f10f-45db-a222-ac0417dbe85e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebe9259b-7186-40b9-b955-3c18005c1d83",
                    "LayerId": "6cdd9663-95e9-4b8a-9b20-1592950b394a"
                }
            ]
        },
        {
            "id": "ab526470-9568-4281-bb83-e5a794f279ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b785ea8f-a9ec-46b7-beb7-c8fa03edfa6e",
            "compositeImage": {
                "id": "7c7decd9-92bd-4497-b7bd-6f516edce41e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab526470-9568-4281-bb83-e5a794f279ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ce31986-7b5c-45a0-8c99-a5d28bd93b93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab526470-9568-4281-bb83-e5a794f279ec",
                    "LayerId": "6cdd9663-95e9-4b8a-9b20-1592950b394a"
                }
            ]
        },
        {
            "id": "bb2847a9-a440-496a-861e-039366b94397",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b785ea8f-a9ec-46b7-beb7-c8fa03edfa6e",
            "compositeImage": {
                "id": "7d6fa548-0fe5-4ac9-9f63-baa62b5e5667",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb2847a9-a440-496a-861e-039366b94397",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cac5fcc2-1332-48b9-b801-d0ba6a0f7d60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb2847a9-a440-496a-861e-039366b94397",
                    "LayerId": "6cdd9663-95e9-4b8a-9b20-1592950b394a"
                }
            ]
        },
        {
            "id": "9cdd5318-a03c-4494-a130-c7f831b62268",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b785ea8f-a9ec-46b7-beb7-c8fa03edfa6e",
            "compositeImage": {
                "id": "5cda3468-98ea-4850-be0d-f05aa943ec6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cdd5318-a03c-4494-a130-c7f831b62268",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be500d76-de93-4bfb-8ed7-24ee0049488f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cdd5318-a03c-4494-a130-c7f831b62268",
                    "LayerId": "6cdd9663-95e9-4b8a-9b20-1592950b394a"
                }
            ]
        },
        {
            "id": "7689f863-b4ee-46d3-b940-4e5b7a3f86e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b785ea8f-a9ec-46b7-beb7-c8fa03edfa6e",
            "compositeImage": {
                "id": "ba983647-747c-49c6-b9ea-7219e4f91e60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7689f863-b4ee-46d3-b940-4e5b7a3f86e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77873c8d-d3b3-4802-9b62-6c64ab0be956",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7689f863-b4ee-46d3-b940-4e5b7a3f86e5",
                    "LayerId": "6cdd9663-95e9-4b8a-9b20-1592950b394a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "6cdd9663-95e9-4b8a-9b20-1592950b394a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b785ea8f-a9ec-46b7-beb7-c8fa03edfa6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 21
}