{
    "id": "ba5ac2b5-2b21-4928-84b7-de6e6aa0e172",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_switch_lb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 12,
    "bbox_right": 87,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5418ed6a-aff9-49b2-b57b-5d74e45ac310",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba5ac2b5-2b21-4928-84b7-de6e6aa0e172",
            "compositeImage": {
                "id": "1b664ee5-c295-4844-b103-a0f49c9b1d25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5418ed6a-aff9-49b2-b57b-5d74e45ac310",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9aac49a-b2ac-4be6-83df-605f9d408ac7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5418ed6a-aff9-49b2-b57b-5d74e45ac310",
                    "LayerId": "4c757284-c457-46ac-8dd9-548887b7d676"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "4c757284-c457-46ac-8dd9-548887b7d676",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba5ac2b5-2b21-4928-84b7-de6e6aa0e172",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}