{
    "id": "d7e0350e-5558-4422-88bf-aca3122e25b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_floor",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3beb88ef-3bc1-40ce-b064-39e6a0002044",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7e0350e-5558-4422-88bf-aca3122e25b2",
            "compositeImage": {
                "id": "303ba4ec-f6ed-4af6-852b-86ff875177a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3beb88ef-3bc1-40ce-b064-39e6a0002044",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c81f8cf-3a62-4134-a0a0-eecd736cbfdf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3beb88ef-3bc1-40ce-b064-39e6a0002044",
                    "LayerId": "0f5c8e9e-bcf5-4863-883c-780038654d00"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0f5c8e9e-bcf5-4863-883c-780038654d00",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7e0350e-5558-4422-88bf-aca3122e25b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}