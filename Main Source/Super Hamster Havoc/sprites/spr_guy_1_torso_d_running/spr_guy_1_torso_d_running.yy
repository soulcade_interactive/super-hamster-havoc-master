{
    "id": "93254812-effd-493f-b8c7-751198fda618",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_1_torso_d_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 25,
    "bbox_right": 38,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8cbe6ef3-056b-43f3-a3c9-6bf1dd7547b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93254812-effd-493f-b8c7-751198fda618",
            "compositeImage": {
                "id": "957f1c02-4ffb-41bd-b1ec-a8bbfe24cf06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cbe6ef3-056b-43f3-a3c9-6bf1dd7547b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b2c5e7d-9770-4865-96b3-95024b4e92ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cbe6ef3-056b-43f3-a3c9-6bf1dd7547b5",
                    "LayerId": "11ac72ee-3014-4a53-b94b-4de18da83d0e"
                }
            ]
        },
        {
            "id": "dd59f6c9-22bc-4f28-8273-8c9e0df6ebb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93254812-effd-493f-b8c7-751198fda618",
            "compositeImage": {
                "id": "e51f21e4-0503-4bfe-bb3e-028ee8b3629e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd59f6c9-22bc-4f28-8273-8c9e0df6ebb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b351023c-e6ff-4084-b9ce-0cfa72ce114d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd59f6c9-22bc-4f28-8273-8c9e0df6ebb1",
                    "LayerId": "11ac72ee-3014-4a53-b94b-4de18da83d0e"
                }
            ]
        },
        {
            "id": "f7cd4199-0a58-4c93-956f-499c9119ca43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93254812-effd-493f-b8c7-751198fda618",
            "compositeImage": {
                "id": "79f788bd-cfa5-4181-a1e9-6f944d2174bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7cd4199-0a58-4c93-956f-499c9119ca43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e12a1ac8-a2b0-401b-b70e-443a26b140d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7cd4199-0a58-4c93-956f-499c9119ca43",
                    "LayerId": "11ac72ee-3014-4a53-b94b-4de18da83d0e"
                }
            ]
        },
        {
            "id": "2f44b9a9-b805-4fbd-b206-06981a08f162",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93254812-effd-493f-b8c7-751198fda618",
            "compositeImage": {
                "id": "82c52088-cf70-44b1-a96a-018d9af24849",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f44b9a9-b805-4fbd-b206-06981a08f162",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b03fb1ad-811e-46cb-b750-31c7b3958203",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f44b9a9-b805-4fbd-b206-06981a08f162",
                    "LayerId": "11ac72ee-3014-4a53-b94b-4de18da83d0e"
                }
            ]
        },
        {
            "id": "62486e6c-96fc-437a-aaff-83f4f54c985c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93254812-effd-493f-b8c7-751198fda618",
            "compositeImage": {
                "id": "1e7a2f37-82c4-4354-b878-bdacbc4883e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62486e6c-96fc-437a-aaff-83f4f54c985c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3f0c759-e8a0-463b-9299-c40a925c8b80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62486e6c-96fc-437a-aaff-83f4f54c985c",
                    "LayerId": "11ac72ee-3014-4a53-b94b-4de18da83d0e"
                }
            ]
        },
        {
            "id": "f3841477-31ab-4bef-983a-fd049f3a202b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93254812-effd-493f-b8c7-751198fda618",
            "compositeImage": {
                "id": "ad72a866-1f04-42cb-8119-9319ec3e8fce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3841477-31ab-4bef-983a-fd049f3a202b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a424fc5b-4e89-40fc-8259-3708d6e7814e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3841477-31ab-4bef-983a-fd049f3a202b",
                    "LayerId": "11ac72ee-3014-4a53-b94b-4de18da83d0e"
                }
            ]
        },
        {
            "id": "680ee1fc-daf8-4117-a290-98b1eb1f0c01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93254812-effd-493f-b8c7-751198fda618",
            "compositeImage": {
                "id": "bc7a7921-2ca9-4cd8-af74-a6e0eabba95f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "680ee1fc-daf8-4117-a290-98b1eb1f0c01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7baf053b-5e19-4ad4-9670-f94c5dd91944",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "680ee1fc-daf8-4117-a290-98b1eb1f0c01",
                    "LayerId": "11ac72ee-3014-4a53-b94b-4de18da83d0e"
                }
            ]
        },
        {
            "id": "e9813430-e265-4eea-b77c-8b1675902c87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93254812-effd-493f-b8c7-751198fda618",
            "compositeImage": {
                "id": "0aa76dfd-931d-4158-8168-be32a5991f10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9813430-e265-4eea-b77c-8b1675902c87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "587eca39-3f42-458c-b9f3-a0efb94bdb45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9813430-e265-4eea-b77c-8b1675902c87",
                    "LayerId": "11ac72ee-3014-4a53-b94b-4de18da83d0e"
                }
            ]
        },
        {
            "id": "31c9c564-d705-4ff0-8041-da83ed0de828",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93254812-effd-493f-b8c7-751198fda618",
            "compositeImage": {
                "id": "9eb83c89-c33b-47dd-a4b2-3d56a657f94f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31c9c564-d705-4ff0-8041-da83ed0de828",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3596b77-8968-41ad-8396-da0040d1f5e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31c9c564-d705-4ff0-8041-da83ed0de828",
                    "LayerId": "11ac72ee-3014-4a53-b94b-4de18da83d0e"
                }
            ]
        },
        {
            "id": "951d94e1-16b7-487c-847d-587f7cf7f4e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93254812-effd-493f-b8c7-751198fda618",
            "compositeImage": {
                "id": "dded795e-413e-4a6b-9cdd-ab2ab180aacd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "951d94e1-16b7-487c-847d-587f7cf7f4e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab40e4ed-d254-45ae-9631-204ff74f642f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "951d94e1-16b7-487c-847d-587f7cf7f4e1",
                    "LayerId": "11ac72ee-3014-4a53-b94b-4de18da83d0e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "11ac72ee-3014-4a53-b94b-4de18da83d0e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "93254812-effd-493f-b8c7-751198fda618",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}