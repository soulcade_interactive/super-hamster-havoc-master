{
    "id": "f55daa2b-3c34-428f-8808-3d5b9009de09",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_head_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4728f5bc-dff1-43e8-a2db-e22a3bb68e50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f55daa2b-3c34-428f-8808-3d5b9009de09",
            "compositeImage": {
                "id": "037811e6-b117-49e1-b9c4-fce0aa3a019a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4728f5bc-dff1-43e8-a2db-e22a3bb68e50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f889d21-02ac-4c6c-81f0-355731e303d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4728f5bc-dff1-43e8-a2db-e22a3bb68e50",
                    "LayerId": "942e038f-bf87-4615-aeba-d175966f5cbe"
                }
            ]
        },
        {
            "id": "c7711953-35e6-4ecc-9679-b2bdb181fa60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f55daa2b-3c34-428f-8808-3d5b9009de09",
            "compositeImage": {
                "id": "ce9dec78-3f7b-4242-962b-3bf5b252704b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7711953-35e6-4ecc-9679-b2bdb181fa60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f22a46a-cd81-413a-ae6e-4c0d15aaed3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7711953-35e6-4ecc-9679-b2bdb181fa60",
                    "LayerId": "942e038f-bf87-4615-aeba-d175966f5cbe"
                }
            ]
        },
        {
            "id": "af2a478f-0551-4f49-86a8-636b52383a9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f55daa2b-3c34-428f-8808-3d5b9009de09",
            "compositeImage": {
                "id": "645a9751-44d2-4fd5-b928-c3ca4e84f509",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af2a478f-0551-4f49-86a8-636b52383a9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39a86d54-c486-4573-a363-4e77fd0f4c33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af2a478f-0551-4f49-86a8-636b52383a9f",
                    "LayerId": "942e038f-bf87-4615-aeba-d175966f5cbe"
                }
            ]
        },
        {
            "id": "467a27a7-614c-4cc8-8492-3603aa4cd182",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f55daa2b-3c34-428f-8808-3d5b9009de09",
            "compositeImage": {
                "id": "8fe7fa10-5d4a-45ba-a77f-e0c5eebb9219",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "467a27a7-614c-4cc8-8492-3603aa4cd182",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53bdea05-5024-4f7a-a672-1a0c0778b271",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "467a27a7-614c-4cc8-8492-3603aa4cd182",
                    "LayerId": "942e038f-bf87-4615-aeba-d175966f5cbe"
                }
            ]
        },
        {
            "id": "6c55eabc-8e0d-4486-bcf6-5ce69848f09d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f55daa2b-3c34-428f-8808-3d5b9009de09",
            "compositeImage": {
                "id": "8ccb50a6-8fd8-454a-b5bc-b94ebc385a54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c55eabc-8e0d-4486-bcf6-5ce69848f09d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "677182e3-b20c-4912-b025-01b250f2299d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c55eabc-8e0d-4486-bcf6-5ce69848f09d",
                    "LayerId": "942e038f-bf87-4615-aeba-d175966f5cbe"
                }
            ]
        },
        {
            "id": "87bb42f3-691f-4676-8599-1922515c4eac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f55daa2b-3c34-428f-8808-3d5b9009de09",
            "compositeImage": {
                "id": "89781c24-18a5-4fd4-aabe-fbadc5df98f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87bb42f3-691f-4676-8599-1922515c4eac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7464f8dc-9073-4b5d-8e84-dfc8a2e4413e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87bb42f3-691f-4676-8599-1922515c4eac",
                    "LayerId": "942e038f-bf87-4615-aeba-d175966f5cbe"
                }
            ]
        },
        {
            "id": "5204d874-fd2a-4161-8982-ed8a368cda6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f55daa2b-3c34-428f-8808-3d5b9009de09",
            "compositeImage": {
                "id": "ec3b15a8-43b8-4925-98b1-b472b5fef39f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5204d874-fd2a-4161-8982-ed8a368cda6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a8eba96-75ea-4432-850f-6dc5a8b3de0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5204d874-fd2a-4161-8982-ed8a368cda6e",
                    "LayerId": "942e038f-bf87-4615-aeba-d175966f5cbe"
                }
            ]
        },
        {
            "id": "66d446e8-0195-44e1-80d9-384be6ef80f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f55daa2b-3c34-428f-8808-3d5b9009de09",
            "compositeImage": {
                "id": "0bf66fcf-b07b-45c8-bc73-4637f34d641d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66d446e8-0195-44e1-80d9-384be6ef80f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a6e8501-94f7-49c4-9198-31e7d43a4878",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66d446e8-0195-44e1-80d9-384be6ef80f6",
                    "LayerId": "942e038f-bf87-4615-aeba-d175966f5cbe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "942e038f-bf87-4615-aeba-d175966f5cbe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f55daa2b-3c34-428f-8808-3d5b9009de09",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 28
}