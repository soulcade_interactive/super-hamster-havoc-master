{
    "id": "7841b4b7-60b0-4728-8a06-ccbda92cfc60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_0_torso_l_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 22,
    "bbox_right": 42,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b261bf36-e593-4cd5-bf9c-b6f36a66701c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7841b4b7-60b0-4728-8a06-ccbda92cfc60",
            "compositeImage": {
                "id": "9b13988e-8607-4103-b935-d18ccadb0930",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b261bf36-e593-4cd5-bf9c-b6f36a66701c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dbb7621-20e7-424d-9fac-ab69aa545d05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b261bf36-e593-4cd5-bf9c-b6f36a66701c",
                    "LayerId": "a39e1f56-0dc4-41c3-a19b-9c0d95212e1f"
                }
            ]
        },
        {
            "id": "e0f62584-8974-48ac-bf76-bbcf95b44ee5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7841b4b7-60b0-4728-8a06-ccbda92cfc60",
            "compositeImage": {
                "id": "cb4cbeae-570d-440a-ac17-0d709f2378f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0f62584-8974-48ac-bf76-bbcf95b44ee5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de5a4953-3150-4e81-80bd-8244e45a2d27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0f62584-8974-48ac-bf76-bbcf95b44ee5",
                    "LayerId": "a39e1f56-0dc4-41c3-a19b-9c0d95212e1f"
                }
            ]
        },
        {
            "id": "d157bf16-79f4-4be5-892e-c4403c054835",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7841b4b7-60b0-4728-8a06-ccbda92cfc60",
            "compositeImage": {
                "id": "803fedfa-febb-4f1d-858a-d231e28ace00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d157bf16-79f4-4be5-892e-c4403c054835",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfa6a88f-a40c-428b-bcf7-2bfe66f25b07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d157bf16-79f4-4be5-892e-c4403c054835",
                    "LayerId": "a39e1f56-0dc4-41c3-a19b-9c0d95212e1f"
                }
            ]
        },
        {
            "id": "9a43ccc1-2970-4039-b4f1-80abfb87d768",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7841b4b7-60b0-4728-8a06-ccbda92cfc60",
            "compositeImage": {
                "id": "e6557221-4b5f-497b-bcae-4b13a597689e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a43ccc1-2970-4039-b4f1-80abfb87d768",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6457e8f9-d17a-42da-9b99-58a1dcbddc07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a43ccc1-2970-4039-b4f1-80abfb87d768",
                    "LayerId": "a39e1f56-0dc4-41c3-a19b-9c0d95212e1f"
                }
            ]
        },
        {
            "id": "5553169d-ddce-4acf-9752-c87656af0c0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7841b4b7-60b0-4728-8a06-ccbda92cfc60",
            "compositeImage": {
                "id": "f8d2a997-cd5e-42cc-a984-4db9835f6441",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5553169d-ddce-4acf-9752-c87656af0c0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b62d9050-2bda-4d53-a51c-7024e629cbc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5553169d-ddce-4acf-9752-c87656af0c0b",
                    "LayerId": "a39e1f56-0dc4-41c3-a19b-9c0d95212e1f"
                }
            ]
        },
        {
            "id": "a80ff528-1de0-4b21-8410-cda1e2050dd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7841b4b7-60b0-4728-8a06-ccbda92cfc60",
            "compositeImage": {
                "id": "77074714-0207-4bea-b785-0ed2d1ad3f9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a80ff528-1de0-4b21-8410-cda1e2050dd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4338c035-4e34-4c43-a177-5fae0263d814",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a80ff528-1de0-4b21-8410-cda1e2050dd6",
                    "LayerId": "a39e1f56-0dc4-41c3-a19b-9c0d95212e1f"
                }
            ]
        },
        {
            "id": "970dc63e-c1d3-4147-9aa3-761a5e1a553d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7841b4b7-60b0-4728-8a06-ccbda92cfc60",
            "compositeImage": {
                "id": "8787f3f6-662a-424b-9946-885f31226c26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "970dc63e-c1d3-4147-9aa3-761a5e1a553d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1953214c-072e-467e-a52c-3ef806fb20c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "970dc63e-c1d3-4147-9aa3-761a5e1a553d",
                    "LayerId": "a39e1f56-0dc4-41c3-a19b-9c0d95212e1f"
                }
            ]
        },
        {
            "id": "1c7549fc-c937-4686-b50d-72ac64ab8ef7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7841b4b7-60b0-4728-8a06-ccbda92cfc60",
            "compositeImage": {
                "id": "e8f1f1d3-f22e-4551-8664-e7d2ac6d6253",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c7549fc-c937-4686-b50d-72ac64ab8ef7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8632a7b-5474-4a02-a924-9208e78ac59a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c7549fc-c937-4686-b50d-72ac64ab8ef7",
                    "LayerId": "a39e1f56-0dc4-41c3-a19b-9c0d95212e1f"
                }
            ]
        },
        {
            "id": "636a1f5b-a6ff-4c0d-b562-dce486a5ae03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7841b4b7-60b0-4728-8a06-ccbda92cfc60",
            "compositeImage": {
                "id": "8faea927-d27d-47bf-a280-14c0cef099bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "636a1f5b-a6ff-4c0d-b562-dce486a5ae03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2f03dcd-b264-43a0-af09-8da6043e71c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "636a1f5b-a6ff-4c0d-b562-dce486a5ae03",
                    "LayerId": "a39e1f56-0dc4-41c3-a19b-9c0d95212e1f"
                }
            ]
        },
        {
            "id": "1322d92c-26ca-458b-896d-1416560e11b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7841b4b7-60b0-4728-8a06-ccbda92cfc60",
            "compositeImage": {
                "id": "7b3cd3ab-9bdb-4cb7-94c5-6076f2a406d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1322d92c-26ca-458b-896d-1416560e11b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b00b8759-b682-4273-b23b-8c253d8e95ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1322d92c-26ca-458b-896d-1416560e11b7",
                    "LayerId": "a39e1f56-0dc4-41c3-a19b-9c0d95212e1f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a39e1f56-0dc4-41c3-a19b-9c0d95212e1f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7841b4b7-60b0-4728-8a06-ccbda92cfc60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}