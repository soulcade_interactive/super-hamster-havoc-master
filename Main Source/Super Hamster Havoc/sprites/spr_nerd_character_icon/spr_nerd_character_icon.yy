{
    "id": "f356364f-12ca-4cea-8fa2-2ac13865265b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nerd_character_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 14,
    "bbox_right": 113,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "55999f2e-1865-4941-bbdd-6ee8bba4c83d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f356364f-12ca-4cea-8fa2-2ac13865265b",
            "compositeImage": {
                "id": "2bd75059-3a85-498e-a67d-5bb29038aa83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55999f2e-1865-4941-bbdd-6ee8bba4c83d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cab9966-fb29-4ea0-b0f6-5bb3cc860ce6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55999f2e-1865-4941-bbdd-6ee8bba4c83d",
                    "LayerId": "9f1ffd06-2020-40cc-8bf5-7607447fbc4f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "9f1ffd06-2020-40cc-8bf5-7607447fbc4f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f356364f-12ca-4cea-8fa2-2ac13865265b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}