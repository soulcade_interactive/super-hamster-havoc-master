{
    "id": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_muzzle_flash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 17,
    "bbox_right": 42,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "875f9641-a5e1-4b25-8a67-063925355285",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "5f317d6d-d7bc-45d7-8740-5c984180465e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "875f9641-a5e1-4b25-8a67-063925355285",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b9f60a8-0960-48bf-8c43-0f2ae7de8619",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "875f9641-a5e1-4b25-8a67-063925355285",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "81f7a0d6-f969-44a2-b015-5792ddd754e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "969dba2b-a6bb-41a0-ba84-e07666f7653a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81f7a0d6-f969-44a2-b015-5792ddd754e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62e5565e-4b22-4432-b1bd-e8f779fa9068",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81f7a0d6-f969-44a2-b015-5792ddd754e2",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "8c2e9b38-5033-4ea0-bab7-28f17a8605de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "59aceae9-d5e3-4931-a0b9-f64610a20e9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c2e9b38-5033-4ea0-bab7-28f17a8605de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4131af6-5554-4684-bb37-348874cf432a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c2e9b38-5033-4ea0-bab7-28f17a8605de",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "f32eef3d-1cf2-48e2-9ba1-d3cd2ab456e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "2c288f86-0710-4329-9597-782b5e511e97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f32eef3d-1cf2-48e2-9ba1-d3cd2ab456e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6efce92-f285-4784-9fd3-84f13f9e67d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f32eef3d-1cf2-48e2-9ba1-d3cd2ab456e3",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "a6aca970-df10-4703-9df8-99f2879ed48c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "c485ca13-db2b-4c02-b80e-28219d7ddc01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6aca970-df10-4703-9df8-99f2879ed48c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebd443cb-6adc-448b-815f-3133c313a8ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6aca970-df10-4703-9df8-99f2879ed48c",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "2700fb27-bf73-4b2b-891d-6f68ecfbbec3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "6d9969c1-c0c4-4178-ba90-ed0e329767a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2700fb27-bf73-4b2b-891d-6f68ecfbbec3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2257780c-1c55-4a6c-beb0-ab5ba4b85bfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2700fb27-bf73-4b2b-891d-6f68ecfbbec3",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "e0d58887-c533-44c2-909f-1b24f9740f57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "384d3389-dccf-4b6d-a950-57a3fed92ed6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0d58887-c533-44c2-909f-1b24f9740f57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "811218b0-d121-4786-9c77-f0735364ab52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0d58887-c533-44c2-909f-1b24f9740f57",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "d954b090-6b44-437e-82db-2fea08b5f25f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "92dbb23f-87ed-41ec-98cc-07ff946f3133",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d954b090-6b44-437e-82db-2fea08b5f25f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e593da1-c5f5-4da7-a108-6e3f3be058eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d954b090-6b44-437e-82db-2fea08b5f25f",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "1f9a3785-165d-485e-963d-03651c75f2e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "8740bdb2-34cf-4002-a1bb-976464a3989c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f9a3785-165d-485e-963d-03651c75f2e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1de65a30-cc2d-40a9-9af3-dc72efc59899",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f9a3785-165d-485e-963d-03651c75f2e3",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "c74df201-35f5-4b04-b306-7451cf9015c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "be74998e-c89b-4728-afec-65b029d06ea3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c74df201-35f5-4b04-b306-7451cf9015c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b01fef4e-3dae-418a-861c-d0455d7f99cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c74df201-35f5-4b04-b306-7451cf9015c0",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "283edc0e-fb10-49e5-b947-828b3986c6ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "dca191cc-08c0-48f1-a9a3-0fe0eeed7ee9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "283edc0e-fb10-49e5-b947-828b3986c6ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39751f9a-fbf1-4b6e-b188-48e1f9421e0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "283edc0e-fb10-49e5-b947-828b3986c6ad",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "4842d1d9-71e7-4a1b-853d-dc5c77a604cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "222bb3be-88d1-4608-a216-f395c3274536",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4842d1d9-71e7-4a1b-853d-dc5c77a604cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce0b075f-b907-409a-b8bc-8b9e97ba7ce9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4842d1d9-71e7-4a1b-853d-dc5c77a604cb",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "fc3f0328-c69b-4569-8cc5-a2f1355689e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "4eb2f4fe-5c75-4c98-96ba-ae2facd85d59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc3f0328-c69b-4569-8cc5-a2f1355689e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71521e7f-b181-487a-bd46-d01e25a077b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc3f0328-c69b-4569-8cc5-a2f1355689e7",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "5a4d5c56-8703-496e-9e56-8958aabb0aa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "89a592ca-ba0a-4730-988f-92fcc422e8d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a4d5c56-8703-496e-9e56-8958aabb0aa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea2cb629-555d-4cfc-b716-8f6b3d431480",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a4d5c56-8703-496e-9e56-8958aabb0aa8",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "2b89c15e-e113-46f3-a8f0-8f740e227ba6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "541e9f5d-8d9a-4275-91b4-7e60a8d4c1f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b89c15e-e113-46f3-a8f0-8f740e227ba6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "772e0dc5-90a9-44ce-8a13-6e509afe2a28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b89c15e-e113-46f3-a8f0-8f740e227ba6",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "becc4a66-ecb5-42e0-bf78-2b94a8252091",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "52da82a8-2ec9-4a1a-b154-4f99cc5d4948",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "becc4a66-ecb5-42e0-bf78-2b94a8252091",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7752ac99-7376-4380-bd5f-382d420a3f2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "becc4a66-ecb5-42e0-bf78-2b94a8252091",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "7d8181e8-7805-4081-a724-8f82da640b26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "01e557c0-64b8-4be3-94d0-b8b7eb78aed4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d8181e8-7805-4081-a724-8f82da640b26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f1009c2-6777-4b6c-aed1-f8e0b11c997a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d8181e8-7805-4081-a724-8f82da640b26",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "8c204f1e-f507-4e45-86b7-4d7e75c5a011",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "ae8163f7-d89b-4b66-ab6d-19838c103147",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c204f1e-f507-4e45-86b7-4d7e75c5a011",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c4ecce5-a9d3-4c7d-b974-e40f0ef13816",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c204f1e-f507-4e45-86b7-4d7e75c5a011",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "f3367641-bb79-4a1b-898f-a34b4a5b1a1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "efc227d7-4c83-4edc-a645-8819dc7c0841",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3367641-bb79-4a1b-898f-a34b4a5b1a1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "071dfd70-bf01-400d-a8c7-a5ebd14aea41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3367641-bb79-4a1b-898f-a34b4a5b1a1a",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "24aad519-9c42-4840-a868-eea113f9239f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "320c3194-2a91-4b30-9697-51551e18fbcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24aad519-9c42-4840-a868-eea113f9239f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbb6efbb-91b1-4851-92e3-e39e0c8fa0d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24aad519-9c42-4840-a868-eea113f9239f",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "c3a7e1d6-2bc8-41a0-961a-a976c550d800",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "7bc00eed-dc14-451a-a12f-7b7e73d070d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3a7e1d6-2bc8-41a0-961a-a976c550d800",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e10d86ee-79fa-4d92-ae7e-f8784dc93300",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3a7e1d6-2bc8-41a0-961a-a976c550d800",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "5497eac9-df68-4b6f-a51a-c3d8f5691765",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "6090d13a-6c2e-45f1-af6d-c1e46433602f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5497eac9-df68-4b6f-a51a-c3d8f5691765",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09be6966-f6db-4805-8a2e-3418e9d1ca63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5497eac9-df68-4b6f-a51a-c3d8f5691765",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "70f00ce0-e531-4ed9-b7d2-5b5053d880e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "73340c9b-3f26-4bae-8754-b960b1654990",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70f00ce0-e531-4ed9-b7d2-5b5053d880e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53a584dc-8aee-4884-8d34-a94514158997",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70f00ce0-e531-4ed9-b7d2-5b5053d880e4",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "5badb76e-c58e-4538-ac7e-9e05cf6411ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "332ce0e6-2902-4364-9c7f-8a5b32d6b808",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5badb76e-c58e-4538-ac7e-9e05cf6411ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8419192-b0d6-4fc3-b9e8-ca7f9067d672",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5badb76e-c58e-4538-ac7e-9e05cf6411ea",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "250b1337-fa7f-45f5-8674-bf250aaa41ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "6ce8fec1-eaf4-477c-b3f3-8905c66b9f80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "250b1337-fa7f-45f5-8674-bf250aaa41ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efc1b233-0725-44b1-a059-12edc4180050",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "250b1337-fa7f-45f5-8674-bf250aaa41ad",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "17f33c1a-8b85-483b-9d4c-4005d51a029d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "3dcbf142-c117-49c4-8ca9-f580e58a53ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17f33c1a-8b85-483b-9d4c-4005d51a029d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd530685-d238-4f7d-beca-d303877021ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17f33c1a-8b85-483b-9d4c-4005d51a029d",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "50f14d90-ef69-4b1b-9884-532676480133",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "6aca7924-c841-4bc9-9e6b-387551a62037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50f14d90-ef69-4b1b-9884-532676480133",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09079017-a8b7-42ef-9488-0fd2b9296c3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50f14d90-ef69-4b1b-9884-532676480133",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "da7977f0-39fe-4b82-a68e-3d1fa8563428",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "23feb1a6-8429-4f1e-b7ba-974ece8f5297",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da7977f0-39fe-4b82-a68e-3d1fa8563428",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fbf245a-c9f3-4d53-a678-c67e6cce660b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da7977f0-39fe-4b82-a68e-3d1fa8563428",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "e90c3cf7-bc09-4e2e-8088-d7170e328e7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "c60d0b25-968c-44e7-9cbf-e0bd24ed7ae9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e90c3cf7-bc09-4e2e-8088-d7170e328e7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c332a7fd-f8e7-4193-aba1-1d9bc736ce78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e90c3cf7-bc09-4e2e-8088-d7170e328e7b",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "6b4bd4d9-9ba8-43a7-a7be-df7979445956",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "b065a938-440e-40ac-9a95-03293e5ae192",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b4bd4d9-9ba8-43a7-a7be-df7979445956",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1298a34c-34c4-4b3e-ba6c-06603c9314b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b4bd4d9-9ba8-43a7-a7be-df7979445956",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "90ee2139-fcd3-4c82-9aaf-2cdc734d7b98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "79a92935-25e3-408d-9a43-55b3d5638995",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90ee2139-fcd3-4c82-9aaf-2cdc734d7b98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74f237b2-4705-4e9f-9ce9-8f0c2fe4160e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90ee2139-fcd3-4c82-9aaf-2cdc734d7b98",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "bc18d12d-47b9-4174-84e6-2a096ac7fece",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "bcc246dc-1bd4-498b-b5d1-639a83f8ac33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc18d12d-47b9-4174-84e6-2a096ac7fece",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc832934-46b4-4c34-b4a9-12b6193be682",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc18d12d-47b9-4174-84e6-2a096ac7fece",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "a6ed2367-9f27-4cfc-b7f5-ff07a93746cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "6efb898d-a587-4d9a-9fa5-f74d6dfa7a92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6ed2367-9f27-4cfc-b7f5-ff07a93746cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91f3cd74-bed5-4b7b-a331-ed04fa180d61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6ed2367-9f27-4cfc-b7f5-ff07a93746cf",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "3a853efd-4241-472e-a060-6dc774f7d82f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "bfb15b7d-ac73-4def-a0d6-e03b15cbcf82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a853efd-4241-472e-a060-6dc774f7d82f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "debe1472-a608-46fc-ba2a-2bea6aefaf16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a853efd-4241-472e-a060-6dc774f7d82f",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "3a2f7662-1691-4ecb-852c-588212b5b515",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "8eedeafc-96e6-4bee-9476-529b5048fd08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a2f7662-1691-4ecb-852c-588212b5b515",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb5e5abf-3e67-477e-992f-8011c10d4b9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a2f7662-1691-4ecb-852c-588212b5b515",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "2778d2ea-0e4d-48d2-949d-c56d54660ee8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "30275857-a353-40e4-9595-2e27b6631be6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2778d2ea-0e4d-48d2-949d-c56d54660ee8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b577159-1a04-42c1-8b2a-4cfe008f503f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2778d2ea-0e4d-48d2-949d-c56d54660ee8",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "393ab462-4e0e-45c5-8064-9d54e7a204a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "ee5c2adf-b65f-4cee-996d-eac24d4bf30e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "393ab462-4e0e-45c5-8064-9d54e7a204a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fff6b496-86ce-4518-991b-0457769819ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "393ab462-4e0e-45c5-8064-9d54e7a204a0",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "f6d6d93d-f5ea-4459-bc6a-55ca39c61094",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "9a35912d-ae11-44c1-98cc-e8d305fd8953",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6d6d93d-f5ea-4459-bc6a-55ca39c61094",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fec6f4f1-9c4a-43d4-87c0-4f2f3dc19d06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6d6d93d-f5ea-4459-bc6a-55ca39c61094",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "25583483-afa1-4aab-95bd-637776f63aa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "89086583-77f0-4c80-90e9-458072d47870",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25583483-afa1-4aab-95bd-637776f63aa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "023c8914-4602-4758-8b60-20969562e594",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25583483-afa1-4aab-95bd-637776f63aa5",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "b115c38e-ef1a-47ca-a052-0918a260f149",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "dd48088d-b110-4c3d-a7d6-d5f58c2d4110",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b115c38e-ef1a-47ca-a052-0918a260f149",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bf22f58-6c4c-4951-9a4d-86c89db05669",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b115c38e-ef1a-47ca-a052-0918a260f149",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "fe813165-c1d9-4609-87bb-32d68d0b3573",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "8439cacd-e4da-4f18-a0b8-aa402304af7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe813165-c1d9-4609-87bb-32d68d0b3573",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8ef62e4-68b5-4ebb-bbfe-24aaa38554d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe813165-c1d9-4609-87bb-32d68d0b3573",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "fdeb3550-e2db-49a4-8a5e-02d3c9e502da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "dcb70bdd-c361-4ffc-af7b-e5a658bf305d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdeb3550-e2db-49a4-8a5e-02d3c9e502da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e1cb177-b3c5-48b7-aff0-c454afdd9f96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdeb3550-e2db-49a4-8a5e-02d3c9e502da",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "6a16d8f0-0c2d-479c-bd38-2246e66ea192",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "d653a8ab-19bd-4092-b6df-0a415fe1faab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a16d8f0-0c2d-479c-bd38-2246e66ea192",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d226586-85a4-4f1b-851f-b02d16b88a1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a16d8f0-0c2d-479c-bd38-2246e66ea192",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "8825fae7-e0cf-43ea-bb3d-944025343f13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "fa950ed9-9490-4b9b-a11d-42c79a47cfc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8825fae7-e0cf-43ea-bb3d-944025343f13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "890beead-7efb-40a1-8e35-6b83b6d174e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8825fae7-e0cf-43ea-bb3d-944025343f13",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "bd1657f6-76f1-4ae7-9f6d-ca694bfb4da5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "aa4e1cc1-7d0e-4deb-a36d-42ee8ad0ec4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd1657f6-76f1-4ae7-9f6d-ca694bfb4da5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb587340-abac-4105-9483-a3aadd0dfe21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd1657f6-76f1-4ae7-9f6d-ca694bfb4da5",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "fa5f8b52-473d-43fc-96cb-5e5f18d04cfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "7b62046c-4de2-48bc-8d50-fa1cbd9e25c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa5f8b52-473d-43fc-96cb-5e5f18d04cfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe8875e6-2c26-446b-99f8-fa31f4b5889d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa5f8b52-473d-43fc-96cb-5e5f18d04cfd",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "f2d109b9-0766-46af-9663-505b8a200a32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "8cc3ae41-14a0-4115-8d58-08d44d8379bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2d109b9-0766-46af-9663-505b8a200a32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97c1b7b5-ba13-454f-9b39-c6d381821632",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2d109b9-0766-46af-9663-505b8a200a32",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "702a70d6-335f-4575-8eb9-f1e5fb2b2fb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "e05b5989-3f9c-42b7-9453-58285c05698a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "702a70d6-335f-4575-8eb9-f1e5fb2b2fb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4566213c-521e-41a7-9bce-09596f72cdfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "702a70d6-335f-4575-8eb9-f1e5fb2b2fb0",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "a1fcec76-7f17-446f-8cbe-7ea8afab70a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "fbb18f41-9d1f-4c9b-ae40-50f495d8c479",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1fcec76-7f17-446f-8cbe-7ea8afab70a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2488acbf-4520-421d-9332-0bd91c043ebe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1fcec76-7f17-446f-8cbe-7ea8afab70a6",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "de892256-2176-4c8e-9231-c1867e6a2826",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "b701d0da-c3bf-455f-8f8a-e4e0a0f8e20b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de892256-2176-4c8e-9231-c1867e6a2826",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da6684f2-84d1-4393-94e0-80e14d998dfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de892256-2176-4c8e-9231-c1867e6a2826",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "f77b3496-110d-4715-8199-5b9fef01d862",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "67238d28-c392-4df0-92fc-bbf1aa262161",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f77b3496-110d-4715-8199-5b9fef01d862",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c27b136-318d-4dc6-852f-9fe7092a0a1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f77b3496-110d-4715-8199-5b9fef01d862",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "2e0a808f-3ae8-42c9-bf12-6a13320fc128",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "36c24c6a-f547-449f-9c48-1cc8724e5aaa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e0a808f-3ae8-42c9-bf12-6a13320fc128",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8ec110d-2c57-4938-ab23-f64e319b897f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e0a808f-3ae8-42c9-bf12-6a13320fc128",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "a3658faa-53ec-4569-ad99-31611cf70d24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "49a46ea4-c5d7-4b15-a248-b9053f2d35b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3658faa-53ec-4569-ad99-31611cf70d24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fa90725-38b2-4ade-81e2-921a06b6229a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3658faa-53ec-4569-ad99-31611cf70d24",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "c201a4d0-cda0-41a1-9713-f2500673529d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "fee81ffd-059c-4af4-b3b6-cdd9eead0102",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c201a4d0-cda0-41a1-9713-f2500673529d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0dc7d42-be5f-467b-887f-00881f597954",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c201a4d0-cda0-41a1-9713-f2500673529d",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "df6d5f79-82bc-4319-88ef-bd3aa7d7e308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "c074f792-eeef-43d1-a3ee-131ac23aa9b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df6d5f79-82bc-4319-88ef-bd3aa7d7e308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8266a626-a45c-4c80-a382-7884a9c6322e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df6d5f79-82bc-4319-88ef-bd3aa7d7e308",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "b62fcae6-34aa-4cd4-8b45-b4520d979981",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "5f5f86ae-5126-4b06-ae2e-41ef8c9ab63e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b62fcae6-34aa-4cd4-8b45-b4520d979981",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bf737e6-3da5-4e46-9d35-6085b38400c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b62fcae6-34aa-4cd4-8b45-b4520d979981",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "ebbd13d3-1255-4737-bb77-36f4905961a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "1e3a092a-6bc8-4911-b7de-2380781e99e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebbd13d3-1255-4737-bb77-36f4905961a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2af73403-b94a-4902-8065-09651a80e861",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebbd13d3-1255-4737-bb77-36f4905961a1",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "a2f27950-9120-4877-8020-18c71b71bb7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "66f8d93a-1633-4e5c-9f2b-9716b8cf6498",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2f27950-9120-4877-8020-18c71b71bb7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be070548-ca8e-43a9-ab1a-cb6f60504771",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2f27950-9120-4877-8020-18c71b71bb7c",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "e8e67606-52bd-431f-84ce-2e5e55419f79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "0895706b-67e4-4dc6-b85d-ca7a66ab1218",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8e67606-52bd-431f-84ce-2e5e55419f79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78f808f3-79f9-464d-99f1-18c3067eca96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8e67606-52bd-431f-84ce-2e5e55419f79",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "300801af-a554-42a3-9425-c739e67b0644",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "f455fbd5-b5e4-4c6a-bc72-37d43a448627",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "300801af-a554-42a3-9425-c739e67b0644",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b647d3f-cb14-4fe0-86dc-0f493efe96b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "300801af-a554-42a3-9425-c739e67b0644",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "209cd553-90b3-46e7-94b7-ed9b18d66c8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "1ee3b2e9-e933-40a7-b2a8-5d0564e8427e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "209cd553-90b3-46e7-94b7-ed9b18d66c8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "945f0dc8-71b0-470a-82cc-3468c471d8a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "209cd553-90b3-46e7-94b7-ed9b18d66c8e",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "fb06fdd7-ef28-4d3d-937e-d7ccf26428ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "ef12ab8b-7623-47a4-924f-2bedb270a6b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb06fdd7-ef28-4d3d-937e-d7ccf26428ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fe69b82-3266-4377-a3e5-afcbda0ba11b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb06fdd7-ef28-4d3d-937e-d7ccf26428ad",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "49a650ea-4a90-4fe9-8fe2-3f712372d3fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "93c68932-0add-49c7-9d61-b247e0e0d270",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49a650ea-4a90-4fe9-8fe2-3f712372d3fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44b194e5-bcbc-4652-99be-ec466e000dd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49a650ea-4a90-4fe9-8fe2-3f712372d3fe",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "b0124ac7-ab52-472a-ab87-27e3c16c5c32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "61ea44a3-1f49-4181-9f5b-4094d66e1087",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0124ac7-ab52-472a-ab87-27e3c16c5c32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cd86690-1574-4ece-8367-0f0c6fb24a2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0124ac7-ab52-472a-ab87-27e3c16c5c32",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "f674382b-7db2-4cb0-ba37-d69c661eca20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "f27d5412-5316-4a33-8262-fabec96b5371",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f674382b-7db2-4cb0-ba37-d69c661eca20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "671787f5-d4a2-4fba-bd9f-66f1ca174616",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f674382b-7db2-4cb0-ba37-d69c661eca20",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "a941c0c7-4e52-4213-b3f3-4f02bbda7bed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "b9c7acf5-66a4-466f-9b03-7ac9fd4af96c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a941c0c7-4e52-4213-b3f3-4f02bbda7bed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93a4febe-68e5-483c-856e-dded75a1d6e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a941c0c7-4e52-4213-b3f3-4f02bbda7bed",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "b9520db9-cf87-47c0-87c1-c5e67caeb8ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "9ca9f050-214e-4ccd-a6ea-553e7b9e4661",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9520db9-cf87-47c0-87c1-c5e67caeb8ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b168d904-688a-444f-9b63-829dfb10873a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9520db9-cf87-47c0-87c1-c5e67caeb8ca",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "77062658-f35b-41c5-830d-92e3e5d9f981",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "1782efb4-b9ac-41df-a8e2-b33e0941b460",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77062658-f35b-41c5-830d-92e3e5d9f981",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12555684-5d16-464a-a035-e4c55363269d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77062658-f35b-41c5-830d-92e3e5d9f981",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "5717cf17-d029-49c9-be47-ea310dcd1039",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "de94869e-0f45-4ea0-b2d3-be499db69db5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5717cf17-d029-49c9-be47-ea310dcd1039",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7c4c403-2799-4de5-8ca5-d73e940c7101",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5717cf17-d029-49c9-be47-ea310dcd1039",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        },
        {
            "id": "951a9520-67c0-4ca1-ac49-758bc224052e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "compositeImage": {
                "id": "e14544e9-fd7d-48be-bb28-e7ae610e52d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "951a9520-67c0-4ca1-ac49-758bc224052e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1faa2e9-5d5b-42b8-a72c-2a47e60a2ac8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "951a9520-67c0-4ca1-ac49-758bc224052e",
                    "LayerId": "e4f459de-a04e-48da-8bad-cc16c51bb788"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "e4f459de-a04e-48da-8bad-cc16c51bb788",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a15910a6-5d35-4ba1-8d14-16ce2cb92e75",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 19,
    "yorig": 9
}