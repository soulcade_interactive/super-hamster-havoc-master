{
    "id": "46f739db-af0a-4ce5-b9d3-355c470fccbd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_corpse_torso_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71203134-d8fa-4df0-bb4b-6f1e4c06338b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46f739db-af0a-4ce5-b9d3-355c470fccbd",
            "compositeImage": {
                "id": "2b38a81f-96da-4df6-8082-b8f97a76a1cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71203134-d8fa-4df0-bb4b-6f1e4c06338b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b1accaa-629f-4197-8e53-26327f893b81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71203134-d8fa-4df0-bb4b-6f1e4c06338b",
                    "LayerId": "a269ac66-4f12-4aaf-970b-cdc034fe8928"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "a269ac66-4f12-4aaf-970b-cdc034fe8928",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46f739db-af0a-4ce5-b9d3-355c470fccbd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 8
}