{
    "id": "2e0bb18e-be5d-403e-8484-139ef26880db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_corpse_torso_brown_and_gray",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b82eb750-4027-4128-a73f-75bcad12ade6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e0bb18e-be5d-403e-8484-139ef26880db",
            "compositeImage": {
                "id": "fd326fea-19df-4a37-b1d4-cea6d869442e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b82eb750-4027-4128-a73f-75bcad12ade6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "553e741a-ff39-4d9c-a1f4-4c013596a3fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b82eb750-4027-4128-a73f-75bcad12ade6",
                    "LayerId": "f132b3a0-0e2f-4286-abe7-dea70e91ca42"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "f132b3a0-0e2f-4286-abe7-dea70e91ca42",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e0bb18e-be5d-403e-8484-139ef26880db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 8
}