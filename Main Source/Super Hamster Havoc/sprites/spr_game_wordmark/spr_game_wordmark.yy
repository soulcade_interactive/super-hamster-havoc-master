{
    "id": "ddebc326-2728-41c0-bcb8-407593ca7eae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_game_wordmark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 7,
    "bbox_right": 290,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b10fc6be-3774-4d68-87a5-e34686b27135",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddebc326-2728-41c0-bcb8-407593ca7eae",
            "compositeImage": {
                "id": "d45238c5-9a55-451d-bd0c-95972236a7ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b10fc6be-3774-4d68-87a5-e34686b27135",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8b7c813-0d5b-4c17-a4d1-80624f183602",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b10fc6be-3774-4d68-87a5-e34686b27135",
                    "LayerId": "4a229246-7a96-4c8c-beb5-ae058587326e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 213,
    "layers": [
        {
            "id": "4a229246-7a96-4c8c-beb5-ae058587326e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ddebc326-2728-41c0-bcb8-407593ca7eae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 150,
    "yorig": 106
}