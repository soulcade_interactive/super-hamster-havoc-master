{
    "id": "40ae96f7-4e1d-4aac-a09c-e3c6f857fd7f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_body_run_side_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 1,
    "bbox_right": 28,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b98bbdf-3b94-4657-82c6-5df2709c3947",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40ae96f7-4e1d-4aac-a09c-e3c6f857fd7f",
            "compositeImage": {
                "id": "e2211fe9-2963-4a12-a6f1-e2e7c044685c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b98bbdf-3b94-4657-82c6-5df2709c3947",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75d9a7c0-b131-420a-bfab-a69381e15d53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b98bbdf-3b94-4657-82c6-5df2709c3947",
                    "LayerId": "85a648ce-e2c9-430c-b7f4-9d92e8cc6c2f"
                }
            ]
        },
        {
            "id": "8354e181-5495-4427-af4d-5fa2155218db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40ae96f7-4e1d-4aac-a09c-e3c6f857fd7f",
            "compositeImage": {
                "id": "eec34ef2-b6d2-4ac0-be78-d7972d76d039",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8354e181-5495-4427-af4d-5fa2155218db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5457c5db-8da3-4127-8504-2e3bd1d39c54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8354e181-5495-4427-af4d-5fa2155218db",
                    "LayerId": "85a648ce-e2c9-430c-b7f4-9d92e8cc6c2f"
                }
            ]
        },
        {
            "id": "6a1f887e-a076-47ea-887a-e316e77ad99a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40ae96f7-4e1d-4aac-a09c-e3c6f857fd7f",
            "compositeImage": {
                "id": "77b04b5e-81d4-4e1b-ad7d-23c81814496f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a1f887e-a076-47ea-887a-e316e77ad99a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22ad117e-e3f7-452f-8159-f3753538cbcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a1f887e-a076-47ea-887a-e316e77ad99a",
                    "LayerId": "85a648ce-e2c9-430c-b7f4-9d92e8cc6c2f"
                }
            ]
        },
        {
            "id": "5dc39ebd-d529-4979-afbc-4850b0b1abcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40ae96f7-4e1d-4aac-a09c-e3c6f857fd7f",
            "compositeImage": {
                "id": "bc11d8cb-902e-4fb3-b615-ac82a6e21e29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dc39ebd-d529-4979-afbc-4850b0b1abcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57b2d159-a35d-449a-b935-b8c130acf53b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dc39ebd-d529-4979-afbc-4850b0b1abcf",
                    "LayerId": "85a648ce-e2c9-430c-b7f4-9d92e8cc6c2f"
                }
            ]
        },
        {
            "id": "317662e7-fb73-42f5-bb5c-27ddf18e1aec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40ae96f7-4e1d-4aac-a09c-e3c6f857fd7f",
            "compositeImage": {
                "id": "b6298a7e-fd8c-45d1-a578-e5e0465439b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "317662e7-fb73-42f5-bb5c-27ddf18e1aec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0917083e-c28a-4155-8ff2-8ef873c6ce77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "317662e7-fb73-42f5-bb5c-27ddf18e1aec",
                    "LayerId": "85a648ce-e2c9-430c-b7f4-9d92e8cc6c2f"
                }
            ]
        },
        {
            "id": "046e6900-7892-421a-9635-72ada6b8b7b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40ae96f7-4e1d-4aac-a09c-e3c6f857fd7f",
            "compositeImage": {
                "id": "654744e7-1fe0-4e98-91f1-5a0f9775450e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "046e6900-7892-421a-9635-72ada6b8b7b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "716cea8a-aa3c-4c67-9d26-e864281b5dc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "046e6900-7892-421a-9635-72ada6b8b7b5",
                    "LayerId": "85a648ce-e2c9-430c-b7f4-9d92e8cc6c2f"
                }
            ]
        },
        {
            "id": "47c3d4b2-d56a-45e2-92d5-56f049253b03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40ae96f7-4e1d-4aac-a09c-e3c6f857fd7f",
            "compositeImage": {
                "id": "9fc1d550-a012-47e6-bba9-2a71c5afb17d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47c3d4b2-d56a-45e2-92d5-56f049253b03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "990bde94-e45b-4251-86b8-9fdfe74b3707",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47c3d4b2-d56a-45e2-92d5-56f049253b03",
                    "LayerId": "85a648ce-e2c9-430c-b7f4-9d92e8cc6c2f"
                }
            ]
        },
        {
            "id": "3cf2c3d5-761c-4377-9486-bdf68af61f7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40ae96f7-4e1d-4aac-a09c-e3c6f857fd7f",
            "compositeImage": {
                "id": "c2d95240-2e03-4c07-8b13-b15542c64eff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cf2c3d5-761c-4377-9486-bdf68af61f7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b4ca3bd-28ea-4a9a-afa5-b817a835f92f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cf2c3d5-761c-4377-9486-bdf68af61f7e",
                    "LayerId": "85a648ce-e2c9-430c-b7f4-9d92e8cc6c2f"
                }
            ]
        },
        {
            "id": "4c43ee0a-081b-439e-9fa1-717bf3e88fab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40ae96f7-4e1d-4aac-a09c-e3c6f857fd7f",
            "compositeImage": {
                "id": "5a513f2e-a5d7-4e53-8dd6-09b9ad8e4dd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c43ee0a-081b-439e-9fa1-717bf3e88fab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01f932bd-ec6e-4e2a-af92-8c1cd61c0cb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c43ee0a-081b-439e-9fa1-717bf3e88fab",
                    "LayerId": "85a648ce-e2c9-430c-b7f4-9d92e8cc6c2f"
                }
            ]
        },
        {
            "id": "5ecd42a8-048d-493b-ae37-c09f135f8824",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40ae96f7-4e1d-4aac-a09c-e3c6f857fd7f",
            "compositeImage": {
                "id": "81e3f971-3d2d-44c8-a043-70fbea89f6f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ecd42a8-048d-493b-ae37-c09f135f8824",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c596fc51-e0d1-4d2a-a8e4-e2a99294df64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ecd42a8-048d-493b-ae37-c09f135f8824",
                    "LayerId": "85a648ce-e2c9-430c-b7f4-9d92e8cc6c2f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "85a648ce-e2c9-430c-b7f4-9d92e8cc6c2f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40ae96f7-4e1d-4aac-a09c-e3c6f857fd7f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 21
}