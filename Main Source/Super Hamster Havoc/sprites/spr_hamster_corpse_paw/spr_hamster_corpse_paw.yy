{
    "id": "2fad3b15-4857-41f3-a8ea-576b35a7361d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_corpse_paw",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8383ca15-b76e-49fa-bcea-15906ed731d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fad3b15-4857-41f3-a8ea-576b35a7361d",
            "compositeImage": {
                "id": "8b0dab41-d816-40f4-8db8-14e165c1017c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8383ca15-b76e-49fa-bcea-15906ed731d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4fe365a-d9ba-424a-9ec9-8a71b993a740",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8383ca15-b76e-49fa-bcea-15906ed731d3",
                    "LayerId": "bb66c953-b15e-4a02-8e6e-d4471d97c304"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "bb66c953-b15e-4a02-8e6e-d4471d97c304",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2fad3b15-4857-41f3-a8ea-576b35a7361d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 3,
    "yorig": 3
}