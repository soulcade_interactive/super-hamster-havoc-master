{
    "id": "a81f21c4-3af3-4a9f-b64d-7db13e388372",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset_forest",
    "For3D": true,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1307,
    "bbox_left": 0,
    "bbox_right": 1215,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69d91473-e3c4-41e3-904c-c85b2c01562c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a81f21c4-3af3-4a9f-b64d-7db13e388372",
            "compositeImage": {
                "id": "84c59867-4fd6-4e86-872f-e2dcd5ae9652",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69d91473-e3c4-41e3-904c-c85b2c01562c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d22db144-95d6-4336-83cb-8c518bf5e380",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69d91473-e3c4-41e3-904c-c85b2c01562c",
                    "LayerId": "deb460f2-901d-4805-a14d-a423af9e859f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1600,
    "layers": [
        {
            "id": "deb460f2-901d-4805-a14d-a423af9e859f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a81f21c4-3af3-4a9f-b64d-7db13e388372",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1600,
    "xorig": 0,
    "yorig": 0
}