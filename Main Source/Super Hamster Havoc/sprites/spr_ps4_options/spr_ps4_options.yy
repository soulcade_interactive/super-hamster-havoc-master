{
    "id": "92213663-bbb2-4c5f-a1f6-76486dac0d01",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ps4_options",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 3,
    "bbox_right": 97,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6dddfbc-fcb0-4d66-b40c-c75970b396f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92213663-bbb2-4c5f-a1f6-76486dac0d01",
            "compositeImage": {
                "id": "0c09d93a-60e3-40c1-a8ba-51d10b49cb19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6dddfbc-fcb0-4d66-b40c-c75970b396f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a715b224-ec05-45d7-9e41-244067de1b9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6dddfbc-fcb0-4d66-b40c-c75970b396f9",
                    "LayerId": "438a2e0b-43fc-4d76-be04-9168714f5c04"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "438a2e0b-43fc-4d76-be04-9168714f5c04",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92213663-bbb2-4c5f-a1f6-76486dac0d01",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}