{
    "id": "9db05f9c-20e4-431e-969a-081208eb5469",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_xboxone_a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 9,
    "bbox_right": 90,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f7fcba4-7c37-4eb0-921e-59f35896151c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9db05f9c-20e4-431e-969a-081208eb5469",
            "compositeImage": {
                "id": "c67150db-8f3f-4cf1-a8b0-ed4ffee8140e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f7fcba4-7c37-4eb0-921e-59f35896151c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab7c7201-bb3b-4cc4-b3a0-b3ad412c57b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f7fcba4-7c37-4eb0-921e-59f35896151c",
                    "LayerId": "09a8292f-e1f7-4a15-8bdd-201d83a22b46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "09a8292f-e1f7-4a15-8bdd-201d83a22b46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9db05f9c-20e4-431e-969a-081208eb5469",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}