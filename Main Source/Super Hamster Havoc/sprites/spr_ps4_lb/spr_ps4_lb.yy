{
    "id": "65550d5f-7570-46a7-985b-3ab2dcefe45d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ps4_lb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 78,
    "bbox_left": 9,
    "bbox_right": 90,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6e8c52e-1c83-42fb-936e-16149fc711ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65550d5f-7570-46a7-985b-3ab2dcefe45d",
            "compositeImage": {
                "id": "248acc8a-4ec2-43cf-a830-6bc08baf4566",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6e8c52e-1c83-42fb-936e-16149fc711ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5072b680-4a79-45f4-a1bf-25252a949ff0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6e8c52e-1c83-42fb-936e-16149fc711ce",
                    "LayerId": "ae122aa0-9042-44d4-ae9d-cd0f4b240b45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "ae122aa0-9042-44d4-ae9d-cd0f4b240b45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65550d5f-7570-46a7-985b-3ab2dcefe45d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}