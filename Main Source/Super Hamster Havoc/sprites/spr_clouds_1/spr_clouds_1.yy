{
    "id": "2f0be3a1-8b5a-4f1a-a3f8-d531f1c507b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_clouds_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 2399,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b533eaa-b940-45d0-8080-ded2c3e7898a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f0be3a1-8b5a-4f1a-a3f8-d531f1c507b8",
            "compositeImage": {
                "id": "d830224d-0b11-4984-8c03-95482ffd1eb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b533eaa-b940-45d0-8080-ded2c3e7898a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "443b86e9-230e-403d-ba05-fec6e45ddc89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b533eaa-b940-45d0-8080-ded2c3e7898a",
                    "LayerId": "3a49a956-208b-4130-a038-c792fe338ea2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 780,
    "layers": [
        {
            "id": "3a49a956-208b-4130-a038-c792fe338ea2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f0be3a1-8b5a-4f1a-a3f8-d531f1c507b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2400,
    "xorig": 1200,
    "yorig": 390
}