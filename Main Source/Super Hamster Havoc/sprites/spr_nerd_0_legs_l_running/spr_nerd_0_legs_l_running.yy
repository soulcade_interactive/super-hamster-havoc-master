{
    "id": "86174ba2-c771-45a2-bf2d-c71c16682ec3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nerd_0_legs_l_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 44,
    "bbox_top": 55,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b185e10-6d50-4abd-8e14-54d3bfd8222f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86174ba2-c771-45a2-bf2d-c71c16682ec3",
            "compositeImage": {
                "id": "4c472eed-f33a-41e3-8654-697ad7fda8a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b185e10-6d50-4abd-8e14-54d3bfd8222f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "482621b8-22d8-4806-beaa-b3ae2c25da19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b185e10-6d50-4abd-8e14-54d3bfd8222f",
                    "LayerId": "396ab0ae-cdef-43ad-b2b9-b0a67ead71ce"
                }
            ]
        },
        {
            "id": "2ccf9564-71dc-424b-989f-60a5abc95c01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86174ba2-c771-45a2-bf2d-c71c16682ec3",
            "compositeImage": {
                "id": "7a888e35-8cd9-4bc4-9f14-f716018b3372",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ccf9564-71dc-424b-989f-60a5abc95c01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "064169de-6057-47df-9cf8-77420024ea6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ccf9564-71dc-424b-989f-60a5abc95c01",
                    "LayerId": "396ab0ae-cdef-43ad-b2b9-b0a67ead71ce"
                }
            ]
        },
        {
            "id": "9072aa94-2646-4c78-8b37-e6e8e387070a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86174ba2-c771-45a2-bf2d-c71c16682ec3",
            "compositeImage": {
                "id": "19d50ee7-256a-46a3-aaca-6b7a0e1a1b58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9072aa94-2646-4c78-8b37-e6e8e387070a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e958a27e-4e1b-41fa-a5ea-b32f24c944a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9072aa94-2646-4c78-8b37-e6e8e387070a",
                    "LayerId": "396ab0ae-cdef-43ad-b2b9-b0a67ead71ce"
                }
            ]
        },
        {
            "id": "51fc177d-ae06-450c-97cf-855fada1850f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86174ba2-c771-45a2-bf2d-c71c16682ec3",
            "compositeImage": {
                "id": "cbefbc42-b527-49d3-a6c7-d93ce9a0bca0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51fc177d-ae06-450c-97cf-855fada1850f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d2765e5-b418-4112-bc27-cca4fe403931",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51fc177d-ae06-450c-97cf-855fada1850f",
                    "LayerId": "396ab0ae-cdef-43ad-b2b9-b0a67ead71ce"
                }
            ]
        },
        {
            "id": "4978e404-bd08-4b9b-9959-1ef55981941e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86174ba2-c771-45a2-bf2d-c71c16682ec3",
            "compositeImage": {
                "id": "031ad25e-af69-4c8d-8523-9355a32d6fc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4978e404-bd08-4b9b-9959-1ef55981941e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71ef4b5d-4c8d-431d-9c9e-fe4045a4b5bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4978e404-bd08-4b9b-9959-1ef55981941e",
                    "LayerId": "396ab0ae-cdef-43ad-b2b9-b0a67ead71ce"
                }
            ]
        },
        {
            "id": "66807fdb-1950-4b1c-8653-b305f6639021",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86174ba2-c771-45a2-bf2d-c71c16682ec3",
            "compositeImage": {
                "id": "dfefd9c9-6292-421a-af98-5fd80319ab7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66807fdb-1950-4b1c-8653-b305f6639021",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0976980-559f-480c-8272-93107581f7a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66807fdb-1950-4b1c-8653-b305f6639021",
                    "LayerId": "396ab0ae-cdef-43ad-b2b9-b0a67ead71ce"
                }
            ]
        },
        {
            "id": "6d0cbf88-a0eb-47e6-b9fe-d67a69276386",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86174ba2-c771-45a2-bf2d-c71c16682ec3",
            "compositeImage": {
                "id": "464f4e5e-fe61-4594-b233-2a1635e19c94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d0cbf88-a0eb-47e6-b9fe-d67a69276386",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17eb14a8-59be-48d3-a093-f952b9f87509",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d0cbf88-a0eb-47e6-b9fe-d67a69276386",
                    "LayerId": "396ab0ae-cdef-43ad-b2b9-b0a67ead71ce"
                }
            ]
        },
        {
            "id": "860b6065-09b9-4f44-8c2d-7e8514771197",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86174ba2-c771-45a2-bf2d-c71c16682ec3",
            "compositeImage": {
                "id": "66ec5809-a59d-4c71-a3c2-5a0468dfc32c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "860b6065-09b9-4f44-8c2d-7e8514771197",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efa1e4c4-4cda-4da4-a668-910877a6bdd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "860b6065-09b9-4f44-8c2d-7e8514771197",
                    "LayerId": "396ab0ae-cdef-43ad-b2b9-b0a67ead71ce"
                }
            ]
        },
        {
            "id": "73963075-9abb-4755-9c8e-2a0038e28de2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86174ba2-c771-45a2-bf2d-c71c16682ec3",
            "compositeImage": {
                "id": "8d123f7e-b9ed-4481-9945-fb03a9a2ab09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73963075-9abb-4755-9c8e-2a0038e28de2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94efcaff-8eb1-42cd-b2ab-984a831f7d5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73963075-9abb-4755-9c8e-2a0038e28de2",
                    "LayerId": "396ab0ae-cdef-43ad-b2b9-b0a67ead71ce"
                }
            ]
        },
        {
            "id": "7c55cc66-0a0c-422d-b3fe-3c68a884ade1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86174ba2-c771-45a2-bf2d-c71c16682ec3",
            "compositeImage": {
                "id": "93b08230-feea-4ba4-b442-0ce462986829",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c55cc66-0a0c-422d-b3fe-3c68a884ade1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "073c7058-b40f-4fc4-a3bf-7763e8476b8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c55cc66-0a0c-422d-b3fe-3c68a884ade1",
                    "LayerId": "396ab0ae-cdef-43ad-b2b9-b0a67ead71ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "396ab0ae-cdef-43ad-b2b9-b0a67ead71ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86174ba2-c771-45a2-bf2d-c71c16682ec3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}