{
    "id": "4a38555d-b047-4937-95ce-d34c71842db5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_layer_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 779,
    "bbox_left": 0,
    "bbox_right": 2399,
    "bbox_top": 558,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bcbaa01e-ef96-40ba-a889-db5abfb45e5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a38555d-b047-4937-95ce-d34c71842db5",
            "compositeImage": {
                "id": "9954e494-c38b-472e-8815-2d992264ef0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcbaa01e-ef96-40ba-a889-db5abfb45e5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc5c9315-ac74-425d-acdd-98d5c1a84a8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcbaa01e-ef96-40ba-a889-db5abfb45e5d",
                    "LayerId": "5f7feb74-98e0-456b-8083-b62f663050c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 780,
    "layers": [
        {
            "id": "5f7feb74-98e0-456b-8083-b62f663050c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a38555d-b047-4937-95ce-d34c71842db5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2400,
    "xorig": 1200,
    "yorig": 390
}