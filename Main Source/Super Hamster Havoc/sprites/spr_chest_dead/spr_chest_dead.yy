{
    "id": "9b1ff14c-01bd-42a9-9fc6-906999c4a5c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_chest_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ce07a52-f490-4d3d-a5ff-3a6a1ee8120a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b1ff14c-01bd-42a9-9fc6-906999c4a5c1",
            "compositeImage": {
                "id": "685ad681-c4c0-46d0-99af-d49be91fa015",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ce07a52-f490-4d3d-a5ff-3a6a1ee8120a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f43515ce-c0f4-4502-b447-9e780a325ec9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ce07a52-f490-4d3d-a5ff-3a6a1ee8120a",
                    "LayerId": "d2b4c191-4b0b-4a77-8e36-a68826387fb1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 23,
    "layers": [
        {
            "id": "d2b4c191-4b0b-4a77-8e36-a68826387fb1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b1ff14c-01bd-42a9-9fc6-906999c4a5c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 11
}