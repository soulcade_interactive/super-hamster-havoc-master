{
    "id": "1a8598c6-ff78-4195-b5e8-e8d95d03e815",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_button_x",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 4,
    "bbox_right": 21,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9dc0f6a9-a09a-4219-9c18-6d81cedc5cca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a8598c6-ff78-4195-b5e8-e8d95d03e815",
            "compositeImage": {
                "id": "8d6d8f62-11e7-4a3d-a33d-5c5bcf05adec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dc0f6a9-a09a-4219-9c18-6d81cedc5cca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd27323d-8731-486d-96d2-47a9315cf500",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dc0f6a9-a09a-4219-9c18-6d81cedc5cca",
                    "LayerId": "8fc164df-0944-49dd-ad67-bb7b1970eac5"
                }
            ]
        },
        {
            "id": "75b33781-4a67-491d-8015-1f63be144871",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a8598c6-ff78-4195-b5e8-e8d95d03e815",
            "compositeImage": {
                "id": "03741a09-dd1e-4ea0-b7ec-16e548558387",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75b33781-4a67-491d-8015-1f63be144871",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d84e07b-e7f6-45d7-ac6c-081a7cc93546",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75b33781-4a67-491d-8015-1f63be144871",
                    "LayerId": "8fc164df-0944-49dd-ad67-bb7b1970eac5"
                }
            ]
        },
        {
            "id": "015c7d6b-94cd-467e-859a-51bb754abdd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a8598c6-ff78-4195-b5e8-e8d95d03e815",
            "compositeImage": {
                "id": "ee4f78e2-c41a-4470-af87-e1bc9c2a6464",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "015c7d6b-94cd-467e-859a-51bb754abdd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "135a5f12-ccfe-4089-9b1b-c667a7c4ac20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "015c7d6b-94cd-467e-859a-51bb754abdd2",
                    "LayerId": "8fc164df-0944-49dd-ad67-bb7b1970eac5"
                }
            ]
        },
        {
            "id": "06fa787f-b279-4ec4-8ebe-0544aec5ebc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a8598c6-ff78-4195-b5e8-e8d95d03e815",
            "compositeImage": {
                "id": "f8ceb0b5-1984-493d-959a-12f14a92e539",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06fa787f-b279-4ec4-8ebe-0544aec5ebc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ce2210e-800b-4ede-8f94-b39ec8f32663",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06fa787f-b279-4ec4-8ebe-0544aec5ebc9",
                    "LayerId": "8fc164df-0944-49dd-ad67-bb7b1970eac5"
                }
            ]
        },
        {
            "id": "ca72d2f7-0ddc-4405-906c-a198f45d0ce8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a8598c6-ff78-4195-b5e8-e8d95d03e815",
            "compositeImage": {
                "id": "66b60714-5670-4110-a160-fbb846f87d30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca72d2f7-0ddc-4405-906c-a198f45d0ce8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43f7b485-69d3-4553-bf5f-c5812c78d70f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca72d2f7-0ddc-4405-906c-a198f45d0ce8",
                    "LayerId": "8fc164df-0944-49dd-ad67-bb7b1970eac5"
                }
            ]
        },
        {
            "id": "2b50e113-e7a2-4e7b-97b1-3e4d692ead65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a8598c6-ff78-4195-b5e8-e8d95d03e815",
            "compositeImage": {
                "id": "06a234a9-0cdf-4bfc-bf17-43943a415055",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b50e113-e7a2-4e7b-97b1-3e4d692ead65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2c90cc7-4df6-4e04-8743-edfc9347fa57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b50e113-e7a2-4e7b-97b1-3e4d692ead65",
                    "LayerId": "8fc164df-0944-49dd-ad67-bb7b1970eac5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "8fc164df-0944-49dd-ad67-bb7b1970eac5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a8598c6-ff78-4195-b5e8-e8d95d03e815",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 26,
    "xorig": 13,
    "yorig": 14
}