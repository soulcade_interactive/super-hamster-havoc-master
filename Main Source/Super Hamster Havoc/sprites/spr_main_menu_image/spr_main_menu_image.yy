{
    "id": "e65424ba-8a27-4f7f-b26d-ff5ae5a18aaa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_main_menu_image",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 217,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bea8b079-852a-491b-89f8-27b7d09246f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e65424ba-8a27-4f7f-b26d-ff5ae5a18aaa",
            "compositeImage": {
                "id": "cb1f675e-f07a-47fc-a29c-7884fce707fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bea8b079-852a-491b-89f8-27b7d09246f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "909a034a-a68e-40fd-a92a-3e440ce76bef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bea8b079-852a-491b-89f8-27b7d09246f9",
                    "LayerId": "f3e7af4d-a948-4370-995b-e41c84d757bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "f3e7af4d-a948-4370-995b-e41c84d757bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e65424ba-8a27-4f7f-b26d-ff5ae5a18aaa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 218,
    "xorig": 109,
    "yorig": 128
}