{
    "id": "9bd5f817-057f-4a53-803e-840e19f20ef9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_wall",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96b8ed08-55b4-46f7-8cee-5cbdb86e31eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bd5f817-057f-4a53-803e-840e19f20ef9",
            "compositeImage": {
                "id": "b7a6d2d8-566d-4366-9d7c-dc7111adde17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96b8ed08-55b4-46f7-8cee-5cbdb86e31eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63bee6b2-799e-46bd-845c-26a17ab70b60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96b8ed08-55b4-46f7-8cee-5cbdb86e31eb",
                    "LayerId": "bd5cc78b-ff1b-4f71-bfcc-909cf337e7a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bd5cc78b-ff1b-4f71-bfcc-909cf337e7a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9bd5f817-057f-4a53-803e-840e19f20ef9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}