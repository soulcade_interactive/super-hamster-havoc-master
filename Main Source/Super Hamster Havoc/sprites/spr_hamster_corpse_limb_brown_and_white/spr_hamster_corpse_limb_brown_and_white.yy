{
    "id": "e0beb796-8f60-42e2-b3f2-39c797a89116",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_corpse_limb_brown_and_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4fa94506-5473-4287-9d45-10a89bcfcdd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0beb796-8f60-42e2-b3f2-39c797a89116",
            "compositeImage": {
                "id": "7d25eb7b-4dd1-4300-b1e3-10dcbd4d8998",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fa94506-5473-4287-9d45-10a89bcfcdd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "340495bf-8f04-4270-b6b3-ca5f529cdff5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fa94506-5473-4287-9d45-10a89bcfcdd0",
                    "LayerId": "b8944283-4bf3-410f-8cef-70a71bc92dcf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "b8944283-4bf3-410f-8cef-70a71bc92dcf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0beb796-8f60-42e2-b3f2-39c797a89116",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 5
}