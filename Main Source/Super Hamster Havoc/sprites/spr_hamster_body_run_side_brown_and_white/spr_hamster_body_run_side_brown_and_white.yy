{
    "id": "2ea1b3b2-a92d-4bdf-9804-dd26eb78e26e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_body_run_side_brown_and_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 1,
    "bbox_right": 28,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "676afdfd-6150-4548-9dc1-59b7304eab56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ea1b3b2-a92d-4bdf-9804-dd26eb78e26e",
            "compositeImage": {
                "id": "36dc4f31-b3ed-4918-99d9-e0ddab48864c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "676afdfd-6150-4548-9dc1-59b7304eab56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d04adfcf-518b-4c6d-ba37-d9e4cadf3a47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "676afdfd-6150-4548-9dc1-59b7304eab56",
                    "LayerId": "4012dc8c-2e14-42f1-a108-8a4ebb979e12"
                }
            ]
        },
        {
            "id": "a0c3c215-caea-4566-9324-916b10531268",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ea1b3b2-a92d-4bdf-9804-dd26eb78e26e",
            "compositeImage": {
                "id": "75c78483-afaf-4cb3-a645-aa30b0fe765e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0c3c215-caea-4566-9324-916b10531268",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecafa6c8-9101-42e6-9066-22a4cd8d779c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0c3c215-caea-4566-9324-916b10531268",
                    "LayerId": "4012dc8c-2e14-42f1-a108-8a4ebb979e12"
                }
            ]
        },
        {
            "id": "7f86f613-d456-41b8-b4fe-d75d769eaab9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ea1b3b2-a92d-4bdf-9804-dd26eb78e26e",
            "compositeImage": {
                "id": "a3dbb9a2-b9d1-4220-8f3d-284de349b038",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f86f613-d456-41b8-b4fe-d75d769eaab9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55855f7b-204e-4a14-a9ae-db17582d6b4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f86f613-d456-41b8-b4fe-d75d769eaab9",
                    "LayerId": "4012dc8c-2e14-42f1-a108-8a4ebb979e12"
                }
            ]
        },
        {
            "id": "1cd0b783-18f7-4749-8c37-a1e8fae1d639",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ea1b3b2-a92d-4bdf-9804-dd26eb78e26e",
            "compositeImage": {
                "id": "09e55a1c-fef2-481f-aaba-8348bc33ce7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cd0b783-18f7-4749-8c37-a1e8fae1d639",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce8c083f-5919-4163-8695-5f26659f6193",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cd0b783-18f7-4749-8c37-a1e8fae1d639",
                    "LayerId": "4012dc8c-2e14-42f1-a108-8a4ebb979e12"
                }
            ]
        },
        {
            "id": "1c53b1de-c3d6-406f-ac66-6e6289eb494a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ea1b3b2-a92d-4bdf-9804-dd26eb78e26e",
            "compositeImage": {
                "id": "f3c89856-0f60-4681-b514-413265ded870",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c53b1de-c3d6-406f-ac66-6e6289eb494a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d7241a1-3720-4640-8e89-a9ad41fa4835",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c53b1de-c3d6-406f-ac66-6e6289eb494a",
                    "LayerId": "4012dc8c-2e14-42f1-a108-8a4ebb979e12"
                }
            ]
        },
        {
            "id": "3b395dbd-8014-45c1-82b2-846da43a558f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ea1b3b2-a92d-4bdf-9804-dd26eb78e26e",
            "compositeImage": {
                "id": "9eddf7e3-136d-4457-9e0d-e851dc645ca6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b395dbd-8014-45c1-82b2-846da43a558f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abd9b664-4da3-4aa2-9bc3-c574ea095a1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b395dbd-8014-45c1-82b2-846da43a558f",
                    "LayerId": "4012dc8c-2e14-42f1-a108-8a4ebb979e12"
                }
            ]
        },
        {
            "id": "35c2afa6-f96e-488c-8a56-c609a04c2b24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ea1b3b2-a92d-4bdf-9804-dd26eb78e26e",
            "compositeImage": {
                "id": "0b3dd3d9-c1f5-4b0b-a25e-540766376183",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35c2afa6-f96e-488c-8a56-c609a04c2b24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b0b55bc-1b3f-4459-867c-1fd20e3b3811",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35c2afa6-f96e-488c-8a56-c609a04c2b24",
                    "LayerId": "4012dc8c-2e14-42f1-a108-8a4ebb979e12"
                }
            ]
        },
        {
            "id": "a6b6e1b9-785f-4a9b-af5e-2a746aeb01b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ea1b3b2-a92d-4bdf-9804-dd26eb78e26e",
            "compositeImage": {
                "id": "491f4950-a8f6-4ce7-94a4-b6cc23b6447e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6b6e1b9-785f-4a9b-af5e-2a746aeb01b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab5586ea-734f-43bd-98f8-4cf13744e75e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6b6e1b9-785f-4a9b-af5e-2a746aeb01b9",
                    "LayerId": "4012dc8c-2e14-42f1-a108-8a4ebb979e12"
                }
            ]
        },
        {
            "id": "e684625f-527a-4cad-8251-54eb069d8419",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ea1b3b2-a92d-4bdf-9804-dd26eb78e26e",
            "compositeImage": {
                "id": "b4b26c9c-219d-48ed-a9ae-1a0a6f2273bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e684625f-527a-4cad-8251-54eb069d8419",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abe0ebc3-1517-4195-95fe-efc28f945e2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e684625f-527a-4cad-8251-54eb069d8419",
                    "LayerId": "4012dc8c-2e14-42f1-a108-8a4ebb979e12"
                }
            ]
        },
        {
            "id": "086c23ca-c90e-4b88-8d3c-3aab8d9385bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2ea1b3b2-a92d-4bdf-9804-dd26eb78e26e",
            "compositeImage": {
                "id": "e65fea9b-8351-45cd-b73a-df17ab078e06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "086c23ca-c90e-4b88-8d3c-3aab8d9385bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e99fb899-7272-4359-8f36-e5b33c79b261",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "086c23ca-c90e-4b88-8d3c-3aab8d9385bf",
                    "LayerId": "4012dc8c-2e14-42f1-a108-8a4ebb979e12"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "4012dc8c-2e14-42f1-a108-8a4ebb979e12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2ea1b3b2-a92d-4bdf-9804-dd26eb78e26e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 21
}