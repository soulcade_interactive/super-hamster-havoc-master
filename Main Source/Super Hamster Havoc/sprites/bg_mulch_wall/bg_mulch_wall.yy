{
    "id": "3e30e82e-d09d-4d57-ae48-6af484c53e2b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_mulch_wall",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 139,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f07e562-882c-4655-84d1-9ef500df0eb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e30e82e-d09d-4d57-ae48-6af484c53e2b",
            "compositeImage": {
                "id": "58231b89-aed2-46f6-ab36-97b5683c4cfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f07e562-882c-4655-84d1-9ef500df0eb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc1cd576-59ee-4338-acf0-eab4e9d791ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f07e562-882c-4655-84d1-9ef500df0eb9",
                    "LayerId": "0550b3fd-e843-491d-8125-cd83460c2d2b"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 96,
    "layers": [
        {
            "id": "0550b3fd-e843-491d-8125-cd83460c2d2b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e30e82e-d09d-4d57-ae48-6af484c53e2b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}