{
    "id": "b90d19ee-1e8c-477f-bd97-076acc081f83",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buff_0_legs_r_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 21,
    "bbox_right": 46,
    "bbox_top": 43,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0269d1f0-5934-4021-8e29-f335fa7e8fda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b90d19ee-1e8c-477f-bd97-076acc081f83",
            "compositeImage": {
                "id": "90442f19-3bd1-463f-928a-8718ac3ac183",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0269d1f0-5934-4021-8e29-f335fa7e8fda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ef50dbb-4ac7-4fb6-8a2a-bc81990c2a5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0269d1f0-5934-4021-8e29-f335fa7e8fda",
                    "LayerId": "599b27ef-bb0e-4bc5-8510-53a5ad60041b"
                }
            ]
        },
        {
            "id": "de44868a-0b12-4de5-ba14-c9ccb1638582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b90d19ee-1e8c-477f-bd97-076acc081f83",
            "compositeImage": {
                "id": "82c09b51-ffb6-458a-a1b3-f3b3de59bdf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de44868a-0b12-4de5-ba14-c9ccb1638582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "221d36d1-d695-4e57-99e0-2b6968a99109",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de44868a-0b12-4de5-ba14-c9ccb1638582",
                    "LayerId": "599b27ef-bb0e-4bc5-8510-53a5ad60041b"
                }
            ]
        },
        {
            "id": "43d776a1-d05d-423e-b1da-35da8c5d10e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b90d19ee-1e8c-477f-bd97-076acc081f83",
            "compositeImage": {
                "id": "4f831d53-742c-4629-9a6a-e179a6daf25a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43d776a1-d05d-423e-b1da-35da8c5d10e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d16ef402-a94f-4ca8-8a71-5d434ebe270f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43d776a1-d05d-423e-b1da-35da8c5d10e5",
                    "LayerId": "599b27ef-bb0e-4bc5-8510-53a5ad60041b"
                }
            ]
        },
        {
            "id": "4f2f0670-af36-4a12-836c-be8616744b3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b90d19ee-1e8c-477f-bd97-076acc081f83",
            "compositeImage": {
                "id": "9d29d06b-f846-43e9-8964-ce6308cf60b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f2f0670-af36-4a12-836c-be8616744b3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6552e9df-6458-47cf-98c7-175ea0558d53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f2f0670-af36-4a12-836c-be8616744b3c",
                    "LayerId": "599b27ef-bb0e-4bc5-8510-53a5ad60041b"
                }
            ]
        },
        {
            "id": "e7f51e91-f352-4e4a-88ba-eaca368b1e7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b90d19ee-1e8c-477f-bd97-076acc081f83",
            "compositeImage": {
                "id": "3e5576c1-7de5-471f-83b4-af9bb074ceac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7f51e91-f352-4e4a-88ba-eaca368b1e7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f54c418a-eae2-4c03-ac50-c04514c3567b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7f51e91-f352-4e4a-88ba-eaca368b1e7d",
                    "LayerId": "599b27ef-bb0e-4bc5-8510-53a5ad60041b"
                }
            ]
        },
        {
            "id": "55559602-e633-4536-aacb-f973dc382921",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b90d19ee-1e8c-477f-bd97-076acc081f83",
            "compositeImage": {
                "id": "28e87500-64d9-4f0b-8821-b26fc0103cf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55559602-e633-4536-aacb-f973dc382921",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48a6ffdf-e4ed-4e9a-80a4-b587326202ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55559602-e633-4536-aacb-f973dc382921",
                    "LayerId": "599b27ef-bb0e-4bc5-8510-53a5ad60041b"
                }
            ]
        },
        {
            "id": "06a950fa-a3ba-471e-b2ac-0d4da980177e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b90d19ee-1e8c-477f-bd97-076acc081f83",
            "compositeImage": {
                "id": "dec1d5cf-018a-48c0-b3e6-4bc2932ddbe0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06a950fa-a3ba-471e-b2ac-0d4da980177e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba2a7a55-038e-4219-94eb-97e5caf5a675",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06a950fa-a3ba-471e-b2ac-0d4da980177e",
                    "LayerId": "599b27ef-bb0e-4bc5-8510-53a5ad60041b"
                }
            ]
        },
        {
            "id": "94e770c7-3e5f-4cb7-9710-8e50d7fc4ae6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b90d19ee-1e8c-477f-bd97-076acc081f83",
            "compositeImage": {
                "id": "01b3e54f-fff7-451d-8c8c-f1c77ac71fd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94e770c7-3e5f-4cb7-9710-8e50d7fc4ae6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7bcb284-714d-4b9a-95b2-fc7cf4e2f1d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94e770c7-3e5f-4cb7-9710-8e50d7fc4ae6",
                    "LayerId": "599b27ef-bb0e-4bc5-8510-53a5ad60041b"
                }
            ]
        },
        {
            "id": "95a59be3-c309-4c1d-b4d1-0979999f0024",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b90d19ee-1e8c-477f-bd97-076acc081f83",
            "compositeImage": {
                "id": "54bceae9-2c35-4e2f-8fd3-a36aef2e3425",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95a59be3-c309-4c1d-b4d1-0979999f0024",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edb6f748-1fb4-4a53-8644-f0c3562d2d96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95a59be3-c309-4c1d-b4d1-0979999f0024",
                    "LayerId": "599b27ef-bb0e-4bc5-8510-53a5ad60041b"
                }
            ]
        },
        {
            "id": "7d5680ec-f031-4de3-a432-c7fa6721b675",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b90d19ee-1e8c-477f-bd97-076acc081f83",
            "compositeImage": {
                "id": "f0524df7-ff8c-4748-b0fc-ff5267063b65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d5680ec-f031-4de3-a432-c7fa6721b675",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db9d00b4-e906-4e79-8d6c-0296bb2c1d3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d5680ec-f031-4de3-a432-c7fa6721b675",
                    "LayerId": "599b27ef-bb0e-4bc5-8510-53a5ad60041b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "599b27ef-bb0e-4bc5-8510-53a5ad60041b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b90d19ee-1e8c-477f-bd97-076acc081f83",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}