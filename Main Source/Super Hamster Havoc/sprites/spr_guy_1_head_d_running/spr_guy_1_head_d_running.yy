{
    "id": "1dbf2aff-bfb2-4fd2-bc18-221ddc5c8f99",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_1_head_d_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 17,
    "bbox_right": 46,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33a78132-8c1d-4fdd-925b-1fbd05f2f373",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dbf2aff-bfb2-4fd2-bc18-221ddc5c8f99",
            "compositeImage": {
                "id": "805ad79b-e9e4-4792-b5a5-9f4b5edde62e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33a78132-8c1d-4fdd-925b-1fbd05f2f373",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efb86239-ef34-4abe-90aa-f652890b27a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33a78132-8c1d-4fdd-925b-1fbd05f2f373",
                    "LayerId": "a92e145c-d0db-4dad-8c6c-79b5df67ac4d"
                }
            ]
        },
        {
            "id": "9db2040a-7d5d-4d12-bf02-5da19092cb3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dbf2aff-bfb2-4fd2-bc18-221ddc5c8f99",
            "compositeImage": {
                "id": "f74b023b-2077-4696-8529-d11fe9788351",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9db2040a-7d5d-4d12-bf02-5da19092cb3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9af9c1d0-68ca-41d6-8392-0cf6f30d51eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9db2040a-7d5d-4d12-bf02-5da19092cb3d",
                    "LayerId": "a92e145c-d0db-4dad-8c6c-79b5df67ac4d"
                }
            ]
        },
        {
            "id": "ad78fe27-58e8-4e2a-90c6-f69b32808861",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dbf2aff-bfb2-4fd2-bc18-221ddc5c8f99",
            "compositeImage": {
                "id": "2039e703-b031-4d77-90cd-08a2b677b279",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad78fe27-58e8-4e2a-90c6-f69b32808861",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "546c12cd-a407-412a-87e1-e14e6747d86b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad78fe27-58e8-4e2a-90c6-f69b32808861",
                    "LayerId": "a92e145c-d0db-4dad-8c6c-79b5df67ac4d"
                }
            ]
        },
        {
            "id": "fcc4ec30-183f-49c3-8f1e-b9954b7cbd13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dbf2aff-bfb2-4fd2-bc18-221ddc5c8f99",
            "compositeImage": {
                "id": "c05c95c9-9f9c-4bdb-b5bb-235f25178f26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcc4ec30-183f-49c3-8f1e-b9954b7cbd13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d438769e-6607-4e7b-9559-15f221811237",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcc4ec30-183f-49c3-8f1e-b9954b7cbd13",
                    "LayerId": "a92e145c-d0db-4dad-8c6c-79b5df67ac4d"
                }
            ]
        },
        {
            "id": "ec96c1dc-cce7-4165-b5af-d949c434317c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dbf2aff-bfb2-4fd2-bc18-221ddc5c8f99",
            "compositeImage": {
                "id": "5addf4bc-fe41-4028-84c3-2aff55101fd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec96c1dc-cce7-4165-b5af-d949c434317c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d456187-ae6d-4a4f-bf68-7a004b6c2ade",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec96c1dc-cce7-4165-b5af-d949c434317c",
                    "LayerId": "a92e145c-d0db-4dad-8c6c-79b5df67ac4d"
                }
            ]
        },
        {
            "id": "bb82a443-3184-4ba1-999e-02356da2a453",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dbf2aff-bfb2-4fd2-bc18-221ddc5c8f99",
            "compositeImage": {
                "id": "6c7dbbd2-145b-4607-9948-05552c1268d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb82a443-3184-4ba1-999e-02356da2a453",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ec7b947-9398-492a-857a-6e359ce16c2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb82a443-3184-4ba1-999e-02356da2a453",
                    "LayerId": "a92e145c-d0db-4dad-8c6c-79b5df67ac4d"
                }
            ]
        },
        {
            "id": "dfa65ff0-4674-419b-b590-798cf1b14147",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dbf2aff-bfb2-4fd2-bc18-221ddc5c8f99",
            "compositeImage": {
                "id": "f3f77a5b-d0e7-4206-9caa-1011cfe94184",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfa65ff0-4674-419b-b590-798cf1b14147",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf539658-fb16-42d3-b32c-5c2f73dbee66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfa65ff0-4674-419b-b590-798cf1b14147",
                    "LayerId": "a92e145c-d0db-4dad-8c6c-79b5df67ac4d"
                }
            ]
        },
        {
            "id": "05915c1b-de4e-482b-8956-f64a5086d55e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dbf2aff-bfb2-4fd2-bc18-221ddc5c8f99",
            "compositeImage": {
                "id": "4667eeb4-f273-461e-887b-90c96ba23e99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05915c1b-de4e-482b-8956-f64a5086d55e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85901f60-2cd1-4351-a4bf-2473d9e1bd65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05915c1b-de4e-482b-8956-f64a5086d55e",
                    "LayerId": "a92e145c-d0db-4dad-8c6c-79b5df67ac4d"
                }
            ]
        },
        {
            "id": "7d29519c-6658-46b7-aa3e-75e44603e98d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dbf2aff-bfb2-4fd2-bc18-221ddc5c8f99",
            "compositeImage": {
                "id": "0816c3d9-c7e8-48ab-bc5f-ec60171e6bb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d29519c-6658-46b7-aa3e-75e44603e98d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aff5eacf-030a-4dee-9620-9c002c21aa98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d29519c-6658-46b7-aa3e-75e44603e98d",
                    "LayerId": "a92e145c-d0db-4dad-8c6c-79b5df67ac4d"
                }
            ]
        },
        {
            "id": "1e09bdc3-7c0d-4dd7-81dd-d41e491f4c88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1dbf2aff-bfb2-4fd2-bc18-221ddc5c8f99",
            "compositeImage": {
                "id": "f62e71f0-4b14-4344-814e-2972d965f601",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e09bdc3-7c0d-4dd7-81dd-d41e491f4c88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dab3b96e-9d2f-4341-9752-8983e2b1cc42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e09bdc3-7c0d-4dd7-81dd-d41e491f4c88",
                    "LayerId": "a92e145c-d0db-4dad-8c6c-79b5df67ac4d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a92e145c-d0db-4dad-8c6c-79b5df67ac4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1dbf2aff-bfb2-4fd2-bc18-221ddc5c8f99",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}