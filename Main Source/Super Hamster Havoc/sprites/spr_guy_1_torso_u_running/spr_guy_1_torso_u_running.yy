{
    "id": "272293be-e214-4315-a65d-465e4f9601fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_1_torso_u_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 25,
    "bbox_right": 38,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92d4e7c4-b7f2-436f-9a25-b2d8108c48a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "272293be-e214-4315-a65d-465e4f9601fd",
            "compositeImage": {
                "id": "24682b0d-3cf9-4f32-a60c-b2e894ab285b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92d4e7c4-b7f2-436f-9a25-b2d8108c48a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b697d4d-c27c-45b0-b331-6051f05fee84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92d4e7c4-b7f2-436f-9a25-b2d8108c48a6",
                    "LayerId": "e84664b0-7356-49c5-b71d-e1e06865580e"
                }
            ]
        },
        {
            "id": "cecdfefa-0cc0-450d-8095-9e61a39e6041",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "272293be-e214-4315-a65d-465e4f9601fd",
            "compositeImage": {
                "id": "c4147adb-e875-4783-ade6-29df66f89f3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cecdfefa-0cc0-450d-8095-9e61a39e6041",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b99f0197-1ff0-4c68-bb51-f3faa6abd1cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cecdfefa-0cc0-450d-8095-9e61a39e6041",
                    "LayerId": "e84664b0-7356-49c5-b71d-e1e06865580e"
                }
            ]
        },
        {
            "id": "033ffd77-ad79-473e-8472-783b87dd88a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "272293be-e214-4315-a65d-465e4f9601fd",
            "compositeImage": {
                "id": "8c4f6be8-2183-4912-a8e6-ae03b8b35ca7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "033ffd77-ad79-473e-8472-783b87dd88a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83965c71-d4a9-43ef-92ba-d02714e9f594",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "033ffd77-ad79-473e-8472-783b87dd88a1",
                    "LayerId": "e84664b0-7356-49c5-b71d-e1e06865580e"
                }
            ]
        },
        {
            "id": "72f62483-94e3-47e8-abdb-011383723365",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "272293be-e214-4315-a65d-465e4f9601fd",
            "compositeImage": {
                "id": "dc0d0644-e0aa-4b92-abde-783587220666",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72f62483-94e3-47e8-abdb-011383723365",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95f57c2e-2483-4e23-8237-c439bef6f509",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72f62483-94e3-47e8-abdb-011383723365",
                    "LayerId": "e84664b0-7356-49c5-b71d-e1e06865580e"
                }
            ]
        },
        {
            "id": "f6743a0c-9567-4535-a505-cb0dd9fad7fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "272293be-e214-4315-a65d-465e4f9601fd",
            "compositeImage": {
                "id": "27186607-bb8a-48a8-9598-e7b4b3216dcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6743a0c-9567-4535-a505-cb0dd9fad7fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ff85ecd-6ce5-489c-9ecf-3482180b4b68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6743a0c-9567-4535-a505-cb0dd9fad7fb",
                    "LayerId": "e84664b0-7356-49c5-b71d-e1e06865580e"
                }
            ]
        },
        {
            "id": "57026d89-8520-4b91-9c91-bad9f77c4bfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "272293be-e214-4315-a65d-465e4f9601fd",
            "compositeImage": {
                "id": "0c1221ba-21cb-47ae-a662-82f7324ec792",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57026d89-8520-4b91-9c91-bad9f77c4bfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24e86d9e-fa3c-4d04-b14d-9ade703c453e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57026d89-8520-4b91-9c91-bad9f77c4bfb",
                    "LayerId": "e84664b0-7356-49c5-b71d-e1e06865580e"
                }
            ]
        },
        {
            "id": "bfc0988c-0a8d-4d8a-a20a-80aad7f5a167",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "272293be-e214-4315-a65d-465e4f9601fd",
            "compositeImage": {
                "id": "b286ed9e-b783-4878-9ca5-2eccfe4ec15c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfc0988c-0a8d-4d8a-a20a-80aad7f5a167",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "babd5ac2-9b97-4aac-b5d8-a2c2063df306",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfc0988c-0a8d-4d8a-a20a-80aad7f5a167",
                    "LayerId": "e84664b0-7356-49c5-b71d-e1e06865580e"
                }
            ]
        },
        {
            "id": "776c9861-989b-49f6-b609-9796f50d00f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "272293be-e214-4315-a65d-465e4f9601fd",
            "compositeImage": {
                "id": "5019c7fe-6e77-4828-b73c-3218b131c29e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "776c9861-989b-49f6-b609-9796f50d00f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09060d71-9436-47a7-b56b-a812de957f2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "776c9861-989b-49f6-b609-9796f50d00f8",
                    "LayerId": "e84664b0-7356-49c5-b71d-e1e06865580e"
                }
            ]
        },
        {
            "id": "eeeee0b0-a29f-4fb3-8ef2-104e3bcc391e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "272293be-e214-4315-a65d-465e4f9601fd",
            "compositeImage": {
                "id": "2fbaa847-557b-4926-8a09-940909511252",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eeeee0b0-a29f-4fb3-8ef2-104e3bcc391e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c5737fd-165f-4f45-9108-196f833547a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eeeee0b0-a29f-4fb3-8ef2-104e3bcc391e",
                    "LayerId": "e84664b0-7356-49c5-b71d-e1e06865580e"
                }
            ]
        },
        {
            "id": "ce75dc0b-35b1-4398-b811-720b89988571",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "272293be-e214-4315-a65d-465e4f9601fd",
            "compositeImage": {
                "id": "2ecc6f94-f8a2-414d-9687-0987da799751",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce75dc0b-35b1-4398-b811-720b89988571",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e478f05e-e8df-43c6-b372-a2bfe2c745b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce75dc0b-35b1-4398-b811-720b89988571",
                    "LayerId": "e84664b0-7356-49c5-b71d-e1e06865580e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e84664b0-7356-49c5-b71d-e1e06865580e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "272293be-e214-4315-a65d-465e4f9601fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}