{
    "id": "72a807ce-369e-4d79-bcc2-ce47b319e9b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_body_run_side_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 1,
    "bbox_right": 28,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4ffccfb-8941-4760-8aa1-200cdd8b2cca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72a807ce-369e-4d79-bcc2-ce47b319e9b6",
            "compositeImage": {
                "id": "d43e7d2c-4c74-4ef7-9f06-69558eb1a299",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4ffccfb-8941-4760-8aa1-200cdd8b2cca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5748bb3-8a07-425b-afd5-2b97732f9911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4ffccfb-8941-4760-8aa1-200cdd8b2cca",
                    "LayerId": "59e62777-01ba-4318-a53b-c0a9681fd7c2"
                }
            ]
        },
        {
            "id": "849d0421-968d-4eaf-88f3-817bb69b579b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72a807ce-369e-4d79-bcc2-ce47b319e9b6",
            "compositeImage": {
                "id": "29e0c58e-8e87-41ee-87ba-4142ca4e6473",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "849d0421-968d-4eaf-88f3-817bb69b579b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b83e042-5f47-46ec-a7f6-4f6966b2a4dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "849d0421-968d-4eaf-88f3-817bb69b579b",
                    "LayerId": "59e62777-01ba-4318-a53b-c0a9681fd7c2"
                }
            ]
        },
        {
            "id": "a0e86ea0-b5ec-49be-a43c-c193727cf3e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72a807ce-369e-4d79-bcc2-ce47b319e9b6",
            "compositeImage": {
                "id": "ba592beb-fd24-4b0d-b290-b6050b574368",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0e86ea0-b5ec-49be-a43c-c193727cf3e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1456a13-47bc-4d5d-a5c6-17b051ef06f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0e86ea0-b5ec-49be-a43c-c193727cf3e0",
                    "LayerId": "59e62777-01ba-4318-a53b-c0a9681fd7c2"
                }
            ]
        },
        {
            "id": "3f91edec-10a6-4ac2-bfad-2925c31f03bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72a807ce-369e-4d79-bcc2-ce47b319e9b6",
            "compositeImage": {
                "id": "208ce7e9-5a81-4701-9c90-40d8324c1a36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f91edec-10a6-4ac2-bfad-2925c31f03bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6b46705-0587-4ad3-8bec-ef06967d4da3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f91edec-10a6-4ac2-bfad-2925c31f03bf",
                    "LayerId": "59e62777-01ba-4318-a53b-c0a9681fd7c2"
                }
            ]
        },
        {
            "id": "5d01dc57-1334-46ab-9604-50d022c45995",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72a807ce-369e-4d79-bcc2-ce47b319e9b6",
            "compositeImage": {
                "id": "5ef75ee7-616c-4316-9009-283fa506aa06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d01dc57-1334-46ab-9604-50d022c45995",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecffeef6-8ac9-4c41-ab44-d1694887f947",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d01dc57-1334-46ab-9604-50d022c45995",
                    "LayerId": "59e62777-01ba-4318-a53b-c0a9681fd7c2"
                }
            ]
        },
        {
            "id": "b2712b42-3fe5-46cd-bcb7-3cfd71948f1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72a807ce-369e-4d79-bcc2-ce47b319e9b6",
            "compositeImage": {
                "id": "bc82f924-a398-401e-a694-14319b09c6da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2712b42-3fe5-46cd-bcb7-3cfd71948f1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90662435-e4e6-4d0a-be56-0abc51643b99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2712b42-3fe5-46cd-bcb7-3cfd71948f1c",
                    "LayerId": "59e62777-01ba-4318-a53b-c0a9681fd7c2"
                }
            ]
        },
        {
            "id": "d87c3cdc-df94-462a-9da4-1217ebc3c6a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72a807ce-369e-4d79-bcc2-ce47b319e9b6",
            "compositeImage": {
                "id": "6c15248b-3473-46cc-bb27-53db52c0a50a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d87c3cdc-df94-462a-9da4-1217ebc3c6a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "521ed3f8-ba67-4d3a-a2e4-a92eb9ebf53a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d87c3cdc-df94-462a-9da4-1217ebc3c6a5",
                    "LayerId": "59e62777-01ba-4318-a53b-c0a9681fd7c2"
                }
            ]
        },
        {
            "id": "a70f21d3-de50-4ebb-aec2-9df947be98e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72a807ce-369e-4d79-bcc2-ce47b319e9b6",
            "compositeImage": {
                "id": "865196f5-b902-4159-93fd-cd347937ff83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a70f21d3-de50-4ebb-aec2-9df947be98e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc4ed015-c252-4878-b0e5-3e22bcc6c6aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a70f21d3-de50-4ebb-aec2-9df947be98e5",
                    "LayerId": "59e62777-01ba-4318-a53b-c0a9681fd7c2"
                }
            ]
        },
        {
            "id": "ebe8e309-32b8-4289-8952-b96c3bf01e6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72a807ce-369e-4d79-bcc2-ce47b319e9b6",
            "compositeImage": {
                "id": "3b647f44-da36-488a-bd75-603ebdada9ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebe8e309-32b8-4289-8952-b96c3bf01e6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b7eeb31-ea9e-4db4-afb2-24bdb6068309",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebe8e309-32b8-4289-8952-b96c3bf01e6f",
                    "LayerId": "59e62777-01ba-4318-a53b-c0a9681fd7c2"
                }
            ]
        },
        {
            "id": "a4e8caa9-d365-4463-ac4a-041c938bea51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72a807ce-369e-4d79-bcc2-ce47b319e9b6",
            "compositeImage": {
                "id": "2ba6c277-1962-42b1-99d1-34f37849f419",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4e8caa9-d365-4463-ac4a-041c938bea51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b5b7a76-515c-4199-990b-f00e48051b2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4e8caa9-d365-4463-ac4a-041c938bea51",
                    "LayerId": "59e62777-01ba-4318-a53b-c0a9681fd7c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "59e62777-01ba-4318-a53b-c0a9681fd7c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72a807ce-369e-4d79-bcc2-ce47b319e9b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 21
}