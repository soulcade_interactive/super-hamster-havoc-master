{
    "id": "5cf84e15-4850-4c19-a2ac-01c22acfe195",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_head_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d02250f7-babe-4975-9b78-2f1279831a4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5cf84e15-4850-4c19-a2ac-01c22acfe195",
            "compositeImage": {
                "id": "0189626a-8c32-4991-8ddf-2b1c51af74d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d02250f7-babe-4975-9b78-2f1279831a4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6816ccc7-6d4f-4808-8ce0-a2cad6357206",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d02250f7-babe-4975-9b78-2f1279831a4d",
                    "LayerId": "e2312fec-7f00-4dff-a12d-7b4ab215f43a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "e2312fec-7f00-4dff-a12d-7b4ab215f43a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5cf84e15-4850-4c19-a2ac-01c22acfe195",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 12
}