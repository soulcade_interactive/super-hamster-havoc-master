{
    "id": "86773232-0062-4b4d-83a0-00104927508f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_head_brown_and_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b85d046-9c10-441f-ad46-c235b6870300",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86773232-0062-4b4d-83a0-00104927508f",
            "compositeImage": {
                "id": "4127bd37-947e-4686-8dc8-98d445ffad45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b85d046-9c10-441f-ad46-c235b6870300",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebc647c0-9275-4afd-821f-d2310e510408",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b85d046-9c10-441f-ad46-c235b6870300",
                    "LayerId": "783afbcd-6a11-45f8-9401-6782fb908cdf"
                }
            ]
        },
        {
            "id": "d76bb8ae-06e5-4317-b678-f6dd47aacd6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86773232-0062-4b4d-83a0-00104927508f",
            "compositeImage": {
                "id": "2d2b085d-e017-4b60-8382-e65c95576982",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d76bb8ae-06e5-4317-b678-f6dd47aacd6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "580102b3-ed97-4f96-851d-e319b63bd806",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d76bb8ae-06e5-4317-b678-f6dd47aacd6b",
                    "LayerId": "783afbcd-6a11-45f8-9401-6782fb908cdf"
                }
            ]
        },
        {
            "id": "1ba3a5ae-8e74-4981-9078-f7528f3429a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86773232-0062-4b4d-83a0-00104927508f",
            "compositeImage": {
                "id": "b2d01bd4-b661-4c77-a0b5-43114afff8c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ba3a5ae-8e74-4981-9078-f7528f3429a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47a08750-296b-461c-8452-d40e91eb115d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ba3a5ae-8e74-4981-9078-f7528f3429a8",
                    "LayerId": "783afbcd-6a11-45f8-9401-6782fb908cdf"
                }
            ]
        },
        {
            "id": "1c86fbe6-00f6-4af2-801d-d0ea01174290",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86773232-0062-4b4d-83a0-00104927508f",
            "compositeImage": {
                "id": "84ab8b26-124c-4cd5-89d4-70c46cb76df9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c86fbe6-00f6-4af2-801d-d0ea01174290",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d5801b7-0317-4ac6-88b9-1ff54ee2a731",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c86fbe6-00f6-4af2-801d-d0ea01174290",
                    "LayerId": "783afbcd-6a11-45f8-9401-6782fb908cdf"
                }
            ]
        },
        {
            "id": "55b109b5-4959-44da-b2eb-9b90f93c2bc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86773232-0062-4b4d-83a0-00104927508f",
            "compositeImage": {
                "id": "e27b563f-878a-456e-af3d-f86da16abbd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55b109b5-4959-44da-b2eb-9b90f93c2bc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b02225e-1e70-417d-88a2-5d3007aa6c74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55b109b5-4959-44da-b2eb-9b90f93c2bc8",
                    "LayerId": "783afbcd-6a11-45f8-9401-6782fb908cdf"
                }
            ]
        },
        {
            "id": "751f469e-83ed-4ed6-b1a4-f90d427a15f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86773232-0062-4b4d-83a0-00104927508f",
            "compositeImage": {
                "id": "f7982639-f66e-4c71-89be-ede2124d7f20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "751f469e-83ed-4ed6-b1a4-f90d427a15f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5aedacc4-ab2d-41b5-a568-0cd4c359a7fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "751f469e-83ed-4ed6-b1a4-f90d427a15f7",
                    "LayerId": "783afbcd-6a11-45f8-9401-6782fb908cdf"
                }
            ]
        },
        {
            "id": "e73e2204-57be-4e0b-9b8a-d8439bce6e27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86773232-0062-4b4d-83a0-00104927508f",
            "compositeImage": {
                "id": "a2e58d5f-be04-41e9-ae49-6c9c171ee89d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e73e2204-57be-4e0b-9b8a-d8439bce6e27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7505f88f-3dbf-4b69-aa3c-012a30cda4f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e73e2204-57be-4e0b-9b8a-d8439bce6e27",
                    "LayerId": "783afbcd-6a11-45f8-9401-6782fb908cdf"
                }
            ]
        },
        {
            "id": "9e8fa333-2aa9-4ff5-9500-b847cba4fbc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86773232-0062-4b4d-83a0-00104927508f",
            "compositeImage": {
                "id": "61b2ab3f-8830-45a2-af18-b649d8617a91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e8fa333-2aa9-4ff5-9500-b847cba4fbc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fefe49b1-453a-4587-b038-385bd417d0b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e8fa333-2aa9-4ff5-9500-b847cba4fbc7",
                    "LayerId": "783afbcd-6a11-45f8-9401-6782fb908cdf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "783afbcd-6a11-45f8-9401-6782fb908cdf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86773232-0062-4b4d-83a0-00104927508f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 28
}