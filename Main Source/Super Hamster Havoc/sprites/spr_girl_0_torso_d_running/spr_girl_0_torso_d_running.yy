{
    "id": "d4628c5f-db66-442a-839f-e55356ab7a42",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_0_torso_d_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 25,
    "bbox_right": 38,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2da22b84-4ad0-4bcb-af37-77099eb18655",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4628c5f-db66-442a-839f-e55356ab7a42",
            "compositeImage": {
                "id": "4ae43b82-b7b4-4476-9351-b1dd6c5b5b57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2da22b84-4ad0-4bcb-af37-77099eb18655",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88fdb894-29bd-4c37-a2c7-f10633cb96e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2da22b84-4ad0-4bcb-af37-77099eb18655",
                    "LayerId": "bf25ede2-e461-4e2b-986f-717e765fc40d"
                }
            ]
        },
        {
            "id": "f29257cb-83bf-455a-b8f0-979d0e25553f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4628c5f-db66-442a-839f-e55356ab7a42",
            "compositeImage": {
                "id": "36546fc6-32d2-4abd-9b13-78541070807e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f29257cb-83bf-455a-b8f0-979d0e25553f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a0f614c-d924-4c83-92b7-6a25dcdd7207",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f29257cb-83bf-455a-b8f0-979d0e25553f",
                    "LayerId": "bf25ede2-e461-4e2b-986f-717e765fc40d"
                }
            ]
        },
        {
            "id": "8a06c883-edd7-492b-b2fe-333f6c25be14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4628c5f-db66-442a-839f-e55356ab7a42",
            "compositeImage": {
                "id": "355e36a1-5dfa-47ff-ad3e-e8543fe3cc0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a06c883-edd7-492b-b2fe-333f6c25be14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3bc8d29-4383-479e-a95b-a00fb7c27756",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a06c883-edd7-492b-b2fe-333f6c25be14",
                    "LayerId": "bf25ede2-e461-4e2b-986f-717e765fc40d"
                }
            ]
        },
        {
            "id": "cf78e318-08c8-43f1-a224-8e3b13b5cf69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4628c5f-db66-442a-839f-e55356ab7a42",
            "compositeImage": {
                "id": "e3143bf4-1771-4f14-8ccc-e37dac7ff516",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf78e318-08c8-43f1-a224-8e3b13b5cf69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d5a05ee-83a4-4dfc-8848-3caeb22e0291",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf78e318-08c8-43f1-a224-8e3b13b5cf69",
                    "LayerId": "bf25ede2-e461-4e2b-986f-717e765fc40d"
                }
            ]
        },
        {
            "id": "0f435acd-f5c2-40b8-9110-67ca781ff0f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4628c5f-db66-442a-839f-e55356ab7a42",
            "compositeImage": {
                "id": "266634d0-2eff-401c-884f-c17446ac5562",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f435acd-f5c2-40b8-9110-67ca781ff0f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "421bf19d-347d-4be8-91b4-22cda8626a90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f435acd-f5c2-40b8-9110-67ca781ff0f2",
                    "LayerId": "bf25ede2-e461-4e2b-986f-717e765fc40d"
                }
            ]
        },
        {
            "id": "38215750-79bd-46c4-9138-b66d0efa6eb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4628c5f-db66-442a-839f-e55356ab7a42",
            "compositeImage": {
                "id": "039a8411-c941-44ae-8881-254e79be781a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38215750-79bd-46c4-9138-b66d0efa6eb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa0fef48-1397-451a-88fd-2e8a3c0b73d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38215750-79bd-46c4-9138-b66d0efa6eb5",
                    "LayerId": "bf25ede2-e461-4e2b-986f-717e765fc40d"
                }
            ]
        },
        {
            "id": "05769eb8-f2cf-478b-acd9-f0538fe39874",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4628c5f-db66-442a-839f-e55356ab7a42",
            "compositeImage": {
                "id": "7bbb0fb9-5ef7-4aa5-b08f-535668888f3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05769eb8-f2cf-478b-acd9-f0538fe39874",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8ea1ef0-2f11-4946-9940-6e1cef3e5f0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05769eb8-f2cf-478b-acd9-f0538fe39874",
                    "LayerId": "bf25ede2-e461-4e2b-986f-717e765fc40d"
                }
            ]
        },
        {
            "id": "823d5c4d-d732-4516-93c4-86dfa791ea67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4628c5f-db66-442a-839f-e55356ab7a42",
            "compositeImage": {
                "id": "254e1476-83c1-4f9d-888e-ca562e5ce6c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "823d5c4d-d732-4516-93c4-86dfa791ea67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94839839-e7b1-4ba0-876d-6c8b5cd5f7e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "823d5c4d-d732-4516-93c4-86dfa791ea67",
                    "LayerId": "bf25ede2-e461-4e2b-986f-717e765fc40d"
                }
            ]
        },
        {
            "id": "7aa2597d-f7e2-4e65-bd52-5fd8ecd13f40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4628c5f-db66-442a-839f-e55356ab7a42",
            "compositeImage": {
                "id": "64d75029-aa8e-4fda-ad76-2a851dd715c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7aa2597d-f7e2-4e65-bd52-5fd8ecd13f40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca8eeceb-4ce0-4919-84c8-a75d3a7daaf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7aa2597d-f7e2-4e65-bd52-5fd8ecd13f40",
                    "LayerId": "bf25ede2-e461-4e2b-986f-717e765fc40d"
                }
            ]
        },
        {
            "id": "27326398-74ab-4dca-bf76-ff1fc85fe4bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4628c5f-db66-442a-839f-e55356ab7a42",
            "compositeImage": {
                "id": "04cf2b8e-3990-4727-8364-805d8ed5091c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27326398-74ab-4dca-bf76-ff1fc85fe4bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d87108c0-652a-4f31-9aa7-c7e9e96e5646",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27326398-74ab-4dca-bf76-ff1fc85fe4bc",
                    "LayerId": "bf25ede2-e461-4e2b-986f-717e765fc40d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bf25ede2-e461-4e2b-986f-717e765fc40d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4628c5f-db66-442a-839f-e55356ab7a42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}