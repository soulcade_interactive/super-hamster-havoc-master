{
    "id": "f780fa45-2f40-4c35-b6b7-9ede359fa9db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_switch_minus",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 13,
    "bbox_right": 87,
    "bbox_top": 37,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f0384de-0cd6-4776-ab4f-37bf8bc45ed6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f780fa45-2f40-4c35-b6b7-9ede359fa9db",
            "compositeImage": {
                "id": "1ff7c616-3783-4da8-a4b6-015107546e54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f0384de-0cd6-4776-ab4f-37bf8bc45ed6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc9c626e-f1e2-43f1-a84e-e62156607b3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f0384de-0cd6-4776-ab4f-37bf8bc45ed6",
                    "LayerId": "d514025c-6b11-46ee-a430-e0aba643b5f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "d514025c-6b11-46ee-a430-e0aba643b5f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f780fa45-2f40-4c35-b6b7-9ede359fa9db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}