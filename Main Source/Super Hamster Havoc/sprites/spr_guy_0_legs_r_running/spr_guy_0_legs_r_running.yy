{
    "id": "fdcd6fde-bfca-4422-abb3-ebb22113b68d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_0_legs_r_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 19,
    "bbox_right": 41,
    "bbox_top": 55,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e44dfc4-5e16-4afc-9eef-3f35cf7dd7e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcd6fde-bfca-4422-abb3-ebb22113b68d",
            "compositeImage": {
                "id": "931efd4e-dd3c-4865-ac8c-c10b1c918d0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e44dfc4-5e16-4afc-9eef-3f35cf7dd7e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a380e352-b156-4a63-ba3e-c429ee8612c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e44dfc4-5e16-4afc-9eef-3f35cf7dd7e3",
                    "LayerId": "0f1bdf9a-989f-4227-b839-2a1525874adb"
                }
            ]
        },
        {
            "id": "97f048d6-3e94-45c4-b4f6-c57943a24830",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcd6fde-bfca-4422-abb3-ebb22113b68d",
            "compositeImage": {
                "id": "2bb3a91a-2d72-4721-9cdb-11bc042178ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97f048d6-3e94-45c4-b4f6-c57943a24830",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fe557fc-ed27-4fd1-990f-ca5e6b6a9140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97f048d6-3e94-45c4-b4f6-c57943a24830",
                    "LayerId": "0f1bdf9a-989f-4227-b839-2a1525874adb"
                }
            ]
        },
        {
            "id": "2d45d0b5-b010-486e-b31a-354a724de867",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcd6fde-bfca-4422-abb3-ebb22113b68d",
            "compositeImage": {
                "id": "7cad9164-36f7-4aa4-a899-cd228d854301",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d45d0b5-b010-486e-b31a-354a724de867",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "644f44e4-c109-4ece-8144-5e72d803032a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d45d0b5-b010-486e-b31a-354a724de867",
                    "LayerId": "0f1bdf9a-989f-4227-b839-2a1525874adb"
                }
            ]
        },
        {
            "id": "cc0bd91e-a029-42aa-882a-6a291642525e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcd6fde-bfca-4422-abb3-ebb22113b68d",
            "compositeImage": {
                "id": "b4f67767-5cf8-4e0e-a4bc-154823dacd96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc0bd91e-a029-42aa-882a-6a291642525e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96346171-3f78-4cef-b73f-bc6910776f36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc0bd91e-a029-42aa-882a-6a291642525e",
                    "LayerId": "0f1bdf9a-989f-4227-b839-2a1525874adb"
                }
            ]
        },
        {
            "id": "3d10ba55-8616-45a1-9d3c-e9cce0f11c72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcd6fde-bfca-4422-abb3-ebb22113b68d",
            "compositeImage": {
                "id": "9a271c75-87be-4f9f-a669-572ccd09c11d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d10ba55-8616-45a1-9d3c-e9cce0f11c72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4b27ec6-e87a-4e2f-86c1-2237bc934d9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d10ba55-8616-45a1-9d3c-e9cce0f11c72",
                    "LayerId": "0f1bdf9a-989f-4227-b839-2a1525874adb"
                }
            ]
        },
        {
            "id": "a38e8e74-2a2a-45dd-a11b-b1137d9e1c1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcd6fde-bfca-4422-abb3-ebb22113b68d",
            "compositeImage": {
                "id": "920aac80-61bf-49ab-adb3-3830dccb37a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a38e8e74-2a2a-45dd-a11b-b1137d9e1c1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c603d080-1f23-44bc-a07c-ebca7eee5a4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a38e8e74-2a2a-45dd-a11b-b1137d9e1c1c",
                    "LayerId": "0f1bdf9a-989f-4227-b839-2a1525874adb"
                }
            ]
        },
        {
            "id": "aa95d0ae-3cee-4f21-9c99-980cefd28c0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcd6fde-bfca-4422-abb3-ebb22113b68d",
            "compositeImage": {
                "id": "e22cf4b9-5e13-4292-b736-3eaf6c24cab8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa95d0ae-3cee-4f21-9c99-980cefd28c0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf1c30c5-394a-4e32-9306-8796b0f294e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa95d0ae-3cee-4f21-9c99-980cefd28c0f",
                    "LayerId": "0f1bdf9a-989f-4227-b839-2a1525874adb"
                }
            ]
        },
        {
            "id": "ca74fb9f-a941-46a3-92a2-adc6eb7c1806",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcd6fde-bfca-4422-abb3-ebb22113b68d",
            "compositeImage": {
                "id": "7e3007e9-4e15-4f48-a078-e0790618058b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca74fb9f-a941-46a3-92a2-adc6eb7c1806",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42aaf91b-7ad6-4903-94b9-d53295808fdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca74fb9f-a941-46a3-92a2-adc6eb7c1806",
                    "LayerId": "0f1bdf9a-989f-4227-b839-2a1525874adb"
                }
            ]
        },
        {
            "id": "1e6c10a5-ab5f-4d20-a4e4-138ba08a3d6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcd6fde-bfca-4422-abb3-ebb22113b68d",
            "compositeImage": {
                "id": "4e7d7422-9104-462d-904d-38abbc54aff3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e6c10a5-ab5f-4d20-a4e4-138ba08a3d6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cfa10fb-a239-42fc-ba1c-0591e349b801",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e6c10a5-ab5f-4d20-a4e4-138ba08a3d6d",
                    "LayerId": "0f1bdf9a-989f-4227-b839-2a1525874adb"
                }
            ]
        },
        {
            "id": "09d64841-7ff9-4a99-8757-7205e0db29db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdcd6fde-bfca-4422-abb3-ebb22113b68d",
            "compositeImage": {
                "id": "daf9fd3b-2cf0-4728-8ab6-53d3e981d0e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09d64841-7ff9-4a99-8757-7205e0db29db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a504b4c-fa81-4990-b776-fae20cda1e87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09d64841-7ff9-4a99-8757-7205e0db29db",
                    "LayerId": "0f1bdf9a-989f-4227-b839-2a1525874adb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0f1bdf9a-989f-4227-b839-2a1525874adb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fdcd6fde-bfca-4422-abb3-ebb22113b68d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}