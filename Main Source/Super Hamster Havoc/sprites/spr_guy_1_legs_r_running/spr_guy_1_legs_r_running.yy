{
    "id": "be7bcab4-5ec0-4ca1-b148-16054edbdf9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_1_legs_r_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 19,
    "bbox_right": 41,
    "bbox_top": 55,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4cbea26-5f89-43d4-9d13-3171c661ed2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be7bcab4-5ec0-4ca1-b148-16054edbdf9e",
            "compositeImage": {
                "id": "40223803-540a-4689-903e-6ccc1d653945",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4cbea26-5f89-43d4-9d13-3171c661ed2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "850e0c0b-f9ba-4063-b1ce-aac91fd6daf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4cbea26-5f89-43d4-9d13-3171c661ed2a",
                    "LayerId": "7a7ffad1-1726-48df-9d47-c00bfcedfc67"
                }
            ]
        },
        {
            "id": "11c993b6-97e8-40b3-9df7-c1a616b664c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be7bcab4-5ec0-4ca1-b148-16054edbdf9e",
            "compositeImage": {
                "id": "103e99fa-eda0-4265-bc3c-37e663e4fff0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11c993b6-97e8-40b3-9df7-c1a616b664c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fddef1f9-33a3-4a39-bc62-d4440928c02b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11c993b6-97e8-40b3-9df7-c1a616b664c4",
                    "LayerId": "7a7ffad1-1726-48df-9d47-c00bfcedfc67"
                }
            ]
        },
        {
            "id": "54aded12-b5e0-4d6f-b1d5-9bda60ac7135",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be7bcab4-5ec0-4ca1-b148-16054edbdf9e",
            "compositeImage": {
                "id": "1da08c6e-ec00-4d9f-a1f8-9d3d9ca47723",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54aded12-b5e0-4d6f-b1d5-9bda60ac7135",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f505555d-2f14-44c0-8427-ab2a5b16bc8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54aded12-b5e0-4d6f-b1d5-9bda60ac7135",
                    "LayerId": "7a7ffad1-1726-48df-9d47-c00bfcedfc67"
                }
            ]
        },
        {
            "id": "a6ff5133-e94a-41d2-8ee4-b2d858cf089c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be7bcab4-5ec0-4ca1-b148-16054edbdf9e",
            "compositeImage": {
                "id": "8b594a11-a167-4caf-b834-36c5b75cc67d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6ff5133-e94a-41d2-8ee4-b2d858cf089c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5be7601d-c04b-46fb-8349-8ad1b0cffddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6ff5133-e94a-41d2-8ee4-b2d858cf089c",
                    "LayerId": "7a7ffad1-1726-48df-9d47-c00bfcedfc67"
                }
            ]
        },
        {
            "id": "cb10f3f0-7897-440e-b81c-45a4a9e5cb2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be7bcab4-5ec0-4ca1-b148-16054edbdf9e",
            "compositeImage": {
                "id": "1b0cc41c-970d-4c20-9a1c-a55d843754af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb10f3f0-7897-440e-b81c-45a4a9e5cb2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ae1697f-cbbf-405e-8515-dbf1c54cca55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb10f3f0-7897-440e-b81c-45a4a9e5cb2c",
                    "LayerId": "7a7ffad1-1726-48df-9d47-c00bfcedfc67"
                }
            ]
        },
        {
            "id": "08cd417d-f9ee-4654-ab76-7ace369d0379",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be7bcab4-5ec0-4ca1-b148-16054edbdf9e",
            "compositeImage": {
                "id": "7442964a-8379-48eb-bcbd-f41c9f1cbb51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08cd417d-f9ee-4654-ab76-7ace369d0379",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2a727ce-a30f-4c52-b8a0-541fd0f13511",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08cd417d-f9ee-4654-ab76-7ace369d0379",
                    "LayerId": "7a7ffad1-1726-48df-9d47-c00bfcedfc67"
                }
            ]
        },
        {
            "id": "71c8739a-a81d-4bad-b98b-527fcfee06e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be7bcab4-5ec0-4ca1-b148-16054edbdf9e",
            "compositeImage": {
                "id": "932ef74f-d508-4767-9f51-3261366f90d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71c8739a-a81d-4bad-b98b-527fcfee06e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4a0d2ec-a7b5-496f-9489-ff75061bff84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71c8739a-a81d-4bad-b98b-527fcfee06e2",
                    "LayerId": "7a7ffad1-1726-48df-9d47-c00bfcedfc67"
                }
            ]
        },
        {
            "id": "2c991ffa-e812-4762-9460-1dbb2a92bb56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be7bcab4-5ec0-4ca1-b148-16054edbdf9e",
            "compositeImage": {
                "id": "c0a612e4-d5bf-4b51-96a0-6263892a8da2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c991ffa-e812-4762-9460-1dbb2a92bb56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0244cc5b-90c1-4e4a-a851-c7d02046d976",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c991ffa-e812-4762-9460-1dbb2a92bb56",
                    "LayerId": "7a7ffad1-1726-48df-9d47-c00bfcedfc67"
                }
            ]
        },
        {
            "id": "76f8a637-6690-4ad3-adfa-3760a1d7dfbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be7bcab4-5ec0-4ca1-b148-16054edbdf9e",
            "compositeImage": {
                "id": "be21d480-1d47-4c40-a806-46636a277bdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76f8a637-6690-4ad3-adfa-3760a1d7dfbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b437328e-69c0-40c7-bae6-e9639f82f6b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76f8a637-6690-4ad3-adfa-3760a1d7dfbd",
                    "LayerId": "7a7ffad1-1726-48df-9d47-c00bfcedfc67"
                }
            ]
        },
        {
            "id": "875fdfed-d294-4283-88ba-3ac9d70c8134",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "be7bcab4-5ec0-4ca1-b148-16054edbdf9e",
            "compositeImage": {
                "id": "cb2ad663-1d8a-4979-814b-bf657dbba2fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "875fdfed-d294-4283-88ba-3ac9d70c8134",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3126c274-4110-4b44-91d9-7fca9aca4426",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "875fdfed-d294-4283-88ba-3ac9d70c8134",
                    "LayerId": "7a7ffad1-1726-48df-9d47-c00bfcedfc67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7a7ffad1-1726-48df-9d47-c00bfcedfc67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "be7bcab4-5ec0-4ca1-b148-16054edbdf9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}