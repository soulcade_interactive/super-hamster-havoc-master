{
    "id": "c15fa93c-e768-41ec-99ff-7e77701a834b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_switch_plus",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 87,
    "bbox_left": 13,
    "bbox_right": 87,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "608c15eb-04c6-4a92-9ac2-e5f24ffb875b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c15fa93c-e768-41ec-99ff-7e77701a834b",
            "compositeImage": {
                "id": "890e95f4-3fc8-424d-bd6c-bdea742a2227",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "608c15eb-04c6-4a92-9ac2-e5f24ffb875b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2cc5f35-39fe-418c-bb19-95ddb4655dfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "608c15eb-04c6-4a92-9ac2-e5f24ffb875b",
                    "LayerId": "159f2493-a43c-4153-9db2-a0138a954112"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "159f2493-a43c-4153-9db2-a0138a954112",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c15fa93c-e768-41ec-99ff-7e77701a834b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}