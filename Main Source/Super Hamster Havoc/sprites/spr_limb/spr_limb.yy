{
    "id": "c9d19a41-8277-4152-afcd-dec9bca9ac57",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_limb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ff88aca4-27c8-4a83-b2f0-091bb2f0a4e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9d19a41-8277-4152-afcd-dec9bca9ac57",
            "compositeImage": {
                "id": "9addfdb8-6e8f-4677-8fbc-0520ff6d09d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff88aca4-27c8-4a83-b2f0-091bb2f0a4e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da3e397d-7a5c-4b0b-89dd-210086d6285c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff88aca4-27c8-4a83-b2f0-091bb2f0a4e0",
                    "LayerId": "99542a34-6dd4-4d8a-a19e-19c0ca8d6c25"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "99542a34-6dd4-4d8a-a19e-19c0ca8d6c25",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9d19a41-8277-4152-afcd-dec9bca9ac57",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 7
}