{
    "id": "a078dc71-598f-4082-bb97-7bd7ba168d1a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bush2s3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 4,
    "bbox_right": 59,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52fdb913-1e87-46e5-a20c-56ee12b63007",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a078dc71-598f-4082-bb97-7bd7ba168d1a",
            "compositeImage": {
                "id": "ccfdbae0-b0c3-4090-920e-6d98af3ccb90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52fdb913-1e87-46e5-a20c-56ee12b63007",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c17b5d8a-25b8-4104-9e9b-c6a0f3aa7f2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52fdb913-1e87-46e5-a20c-56ee12b63007",
                    "LayerId": "8d6d79b8-e2d7-48ed-9970-32f3f831b19b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8d6d79b8-e2d7-48ed-9970-32f3f831b19b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a078dc71-598f-4082-bb97-7bd7ba168d1a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}