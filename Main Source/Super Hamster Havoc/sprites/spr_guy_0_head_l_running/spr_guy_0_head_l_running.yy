{
    "id": "efc3d97e-e792-4276-8b36-e490f171b183",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_0_head_l_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 14,
    "bbox_right": 52,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d704a0a-d0ea-423d-a556-b25ba996baba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efc3d97e-e792-4276-8b36-e490f171b183",
            "compositeImage": {
                "id": "94e71983-7ec0-4605-a4ae-5e3bf6eeb8d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d704a0a-d0ea-423d-a556-b25ba996baba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42dddcb2-7e4c-4c20-8d37-64eba431115e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d704a0a-d0ea-423d-a556-b25ba996baba",
                    "LayerId": "38e08f79-c6ae-4be0-a711-d78d33e22999"
                }
            ]
        },
        {
            "id": "931d2f19-c99b-4e70-a661-f11fbb1d3685",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efc3d97e-e792-4276-8b36-e490f171b183",
            "compositeImage": {
                "id": "c5878f1a-95ee-4e5a-af69-e0b57e199479",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "931d2f19-c99b-4e70-a661-f11fbb1d3685",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "606ee2c2-c681-4a7b-b4bf-375320f71801",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "931d2f19-c99b-4e70-a661-f11fbb1d3685",
                    "LayerId": "38e08f79-c6ae-4be0-a711-d78d33e22999"
                }
            ]
        },
        {
            "id": "4c9de14d-1f77-4f96-b79c-48976677a7a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efc3d97e-e792-4276-8b36-e490f171b183",
            "compositeImage": {
                "id": "15a382fa-c436-4bde-bc29-7b575072a0f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c9de14d-1f77-4f96-b79c-48976677a7a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7569737e-a379-4fcb-ab66-2c285e3abb40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c9de14d-1f77-4f96-b79c-48976677a7a2",
                    "LayerId": "38e08f79-c6ae-4be0-a711-d78d33e22999"
                }
            ]
        },
        {
            "id": "7f6d2276-263c-406d-9e8c-00e06c2a9de5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efc3d97e-e792-4276-8b36-e490f171b183",
            "compositeImage": {
                "id": "76ef98fd-d5ab-4838-b1f3-ff07214d6594",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f6d2276-263c-406d-9e8c-00e06c2a9de5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d435419c-8e81-4fc1-9d55-9bf8eb35e2ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f6d2276-263c-406d-9e8c-00e06c2a9de5",
                    "LayerId": "38e08f79-c6ae-4be0-a711-d78d33e22999"
                }
            ]
        },
        {
            "id": "46bee86e-40ef-4d03-8d27-c0378c326fa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efc3d97e-e792-4276-8b36-e490f171b183",
            "compositeImage": {
                "id": "da5220d8-bbd0-4357-80a1-e43d7805e68c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46bee86e-40ef-4d03-8d27-c0378c326fa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9332d92c-be8d-4728-b4e6-89b4815ad531",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46bee86e-40ef-4d03-8d27-c0378c326fa2",
                    "LayerId": "38e08f79-c6ae-4be0-a711-d78d33e22999"
                }
            ]
        },
        {
            "id": "4a9543ad-5062-4930-9884-64037656f6a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efc3d97e-e792-4276-8b36-e490f171b183",
            "compositeImage": {
                "id": "42f135f4-909d-494d-9846-d3667f7daf8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a9543ad-5062-4930-9884-64037656f6a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d687bc31-46b0-4563-9fb3-6632aaa07288",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a9543ad-5062-4930-9884-64037656f6a2",
                    "LayerId": "38e08f79-c6ae-4be0-a711-d78d33e22999"
                }
            ]
        },
        {
            "id": "9c9198c5-1700-47f2-bbae-82968d1aac55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efc3d97e-e792-4276-8b36-e490f171b183",
            "compositeImage": {
                "id": "c346142b-29e4-4530-8241-82352c86d5a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c9198c5-1700-47f2-bbae-82968d1aac55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4af5936-bfb9-4286-bd9d-bee087fe7807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c9198c5-1700-47f2-bbae-82968d1aac55",
                    "LayerId": "38e08f79-c6ae-4be0-a711-d78d33e22999"
                }
            ]
        },
        {
            "id": "deadcbe6-fda0-4801-b255-5350f4973a24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efc3d97e-e792-4276-8b36-e490f171b183",
            "compositeImage": {
                "id": "d3b6a263-0985-49ff-89f2-b95625cf0bd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "deadcbe6-fda0-4801-b255-5350f4973a24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40254638-dc51-454e-9fc7-0cdbf3b8f31b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "deadcbe6-fda0-4801-b255-5350f4973a24",
                    "LayerId": "38e08f79-c6ae-4be0-a711-d78d33e22999"
                }
            ]
        },
        {
            "id": "3df411ed-6200-4674-8a00-81dab1e1529a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efc3d97e-e792-4276-8b36-e490f171b183",
            "compositeImage": {
                "id": "ee3efcf9-4272-4d2d-bf25-83d1270302e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3df411ed-6200-4674-8a00-81dab1e1529a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f45cbf60-daf1-477d-8eff-3c62fae98bd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3df411ed-6200-4674-8a00-81dab1e1529a",
                    "LayerId": "38e08f79-c6ae-4be0-a711-d78d33e22999"
                }
            ]
        },
        {
            "id": "e0f67086-c924-4996-b2d1-36a6e105f010",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "efc3d97e-e792-4276-8b36-e490f171b183",
            "compositeImage": {
                "id": "7e956fcf-e9a4-48e9-9cfb-5385f5d40107",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0f67086-c924-4996-b2d1-36a6e105f010",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f87d52d9-5e2c-43a4-bbd8-6201833492bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0f67086-c924-4996-b2d1-36a6e105f010",
                    "LayerId": "38e08f79-c6ae-4be0-a711-d78d33e22999"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "38e08f79-c6ae-4be0-a711-d78d33e22999",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "efc3d97e-e792-4276-8b36-e490f171b183",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}