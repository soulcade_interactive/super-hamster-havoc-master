{
    "id": "19528ffc-4a5f-4ded-8a29-d55acbf241de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_0_head_d_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 17,
    "bbox_right": 46,
    "bbox_top": 43,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d756f8f-d108-4d1e-a021-76c222f2d607",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19528ffc-4a5f-4ded-8a29-d55acbf241de",
            "compositeImage": {
                "id": "902db4ba-aa5a-402a-b628-6973838af0ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d756f8f-d108-4d1e-a021-76c222f2d607",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c919710-85e1-4214-b943-b71bce4479d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d756f8f-d108-4d1e-a021-76c222f2d607",
                    "LayerId": "053bffb5-d036-460f-b9ca-ac95100f7a42"
                }
            ]
        },
        {
            "id": "27263b1c-1e96-422b-8b2f-e0478191337f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19528ffc-4a5f-4ded-8a29-d55acbf241de",
            "compositeImage": {
                "id": "7ebd7348-4e3f-4cf4-b00b-5acaa31b1bfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27263b1c-1e96-422b-8b2f-e0478191337f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab3d32f8-776a-4c48-b23a-033637c4d785",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27263b1c-1e96-422b-8b2f-e0478191337f",
                    "LayerId": "053bffb5-d036-460f-b9ca-ac95100f7a42"
                }
            ]
        },
        {
            "id": "8824d0ed-ea6e-4e01-a28c-f14fae87bc55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19528ffc-4a5f-4ded-8a29-d55acbf241de",
            "compositeImage": {
                "id": "c73d217c-fb78-4035-b832-267307c5d961",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8824d0ed-ea6e-4e01-a28c-f14fae87bc55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c591d697-e073-4361-af5e-082ead370442",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8824d0ed-ea6e-4e01-a28c-f14fae87bc55",
                    "LayerId": "053bffb5-d036-460f-b9ca-ac95100f7a42"
                }
            ]
        },
        {
            "id": "bd9bdd5b-8eab-4081-9bbb-3942f6aacb2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19528ffc-4a5f-4ded-8a29-d55acbf241de",
            "compositeImage": {
                "id": "3d87c38f-45b5-4fce-8409-c62f277a0baf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd9bdd5b-8eab-4081-9bbb-3942f6aacb2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52552613-cc4e-457d-bc2d-f4ee1c152dfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd9bdd5b-8eab-4081-9bbb-3942f6aacb2a",
                    "LayerId": "053bffb5-d036-460f-b9ca-ac95100f7a42"
                }
            ]
        },
        {
            "id": "401c3318-13ad-45c0-8738-ca4006e7b28a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19528ffc-4a5f-4ded-8a29-d55acbf241de",
            "compositeImage": {
                "id": "e064067a-7bf6-4d82-995f-5ddb4b6fcad9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "401c3318-13ad-45c0-8738-ca4006e7b28a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3540ac44-f38b-49b0-b9c2-5cd23e314c5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "401c3318-13ad-45c0-8738-ca4006e7b28a",
                    "LayerId": "053bffb5-d036-460f-b9ca-ac95100f7a42"
                }
            ]
        },
        {
            "id": "94cf3208-00bd-4e68-834b-4c8482b29f21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19528ffc-4a5f-4ded-8a29-d55acbf241de",
            "compositeImage": {
                "id": "006f5434-2972-45fd-91c6-fb2a3cf098a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94cf3208-00bd-4e68-834b-4c8482b29f21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69b58c1c-fdda-4a6b-9d35-61be675f451f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94cf3208-00bd-4e68-834b-4c8482b29f21",
                    "LayerId": "053bffb5-d036-460f-b9ca-ac95100f7a42"
                }
            ]
        },
        {
            "id": "70e01917-dd7e-4a46-8ee3-a09cf578d4ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19528ffc-4a5f-4ded-8a29-d55acbf241de",
            "compositeImage": {
                "id": "59fa81fe-d8c2-4f33-abd5-a77bb88f4844",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70e01917-dd7e-4a46-8ee3-a09cf578d4ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3454357-3c26-45e6-9afa-a1bb087469bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70e01917-dd7e-4a46-8ee3-a09cf578d4ab",
                    "LayerId": "053bffb5-d036-460f-b9ca-ac95100f7a42"
                }
            ]
        },
        {
            "id": "e2e459a5-8fc7-44d9-8047-879142a5921d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19528ffc-4a5f-4ded-8a29-d55acbf241de",
            "compositeImage": {
                "id": "8147ca72-00bc-49d5-aa9f-6bc0d7db8184",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2e459a5-8fc7-44d9-8047-879142a5921d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0702f11c-41ee-4b52-912e-56e338c477ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2e459a5-8fc7-44d9-8047-879142a5921d",
                    "LayerId": "053bffb5-d036-460f-b9ca-ac95100f7a42"
                }
            ]
        },
        {
            "id": "e830cb55-fa45-4990-a0aa-99b1f1a34d56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19528ffc-4a5f-4ded-8a29-d55acbf241de",
            "compositeImage": {
                "id": "897ccf4a-ec14-44a0-8e1c-cdd25eed49c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e830cb55-fa45-4990-a0aa-99b1f1a34d56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9facaf70-2540-4607-8dae-76426a090db0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e830cb55-fa45-4990-a0aa-99b1f1a34d56",
                    "LayerId": "053bffb5-d036-460f-b9ca-ac95100f7a42"
                }
            ]
        },
        {
            "id": "81bda4ba-8d8e-4dd5-af1b-13b756ab5c8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "19528ffc-4a5f-4ded-8a29-d55acbf241de",
            "compositeImage": {
                "id": "a36997d1-debf-458a-b251-e9b8017c6463",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81bda4ba-8d8e-4dd5-af1b-13b756ab5c8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "730d1cfb-e530-44c7-af0b-935feae34947",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81bda4ba-8d8e-4dd5-af1b-13b756ab5c8e",
                    "LayerId": "053bffb5-d036-460f-b9ca-ac95100f7a42"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "053bffb5-d036-460f-b9ca-ac95100f7a42",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "19528ffc-4a5f-4ded-8a29-d55acbf241de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}