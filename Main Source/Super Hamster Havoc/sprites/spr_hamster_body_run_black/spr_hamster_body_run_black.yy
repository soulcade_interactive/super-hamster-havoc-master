{
    "id": "a79c3b52-7e74-4cbf-927c-2c2717a90539",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_body_run_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6c2ceb0-30bd-4e84-8020-4728787fec34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a79c3b52-7e74-4cbf-927c-2c2717a90539",
            "compositeImage": {
                "id": "4ed7fadc-53d0-4dd5-9a87-f76a69ba30a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6c2ceb0-30bd-4e84-8020-4728787fec34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6348b412-9887-4176-aabb-662778a435fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6c2ceb0-30bd-4e84-8020-4728787fec34",
                    "LayerId": "475e0752-de6e-4a12-a891-1fd90ad1e7b3"
                }
            ]
        },
        {
            "id": "ff748ee9-b2b8-4056-9bff-ab6a251717e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a79c3b52-7e74-4cbf-927c-2c2717a90539",
            "compositeImage": {
                "id": "62b8b69a-dc4f-4dbd-b85c-f381bf634443",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff748ee9-b2b8-4056-9bff-ab6a251717e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ad8425b-5677-4ead-ba24-0ef416563da7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff748ee9-b2b8-4056-9bff-ab6a251717e0",
                    "LayerId": "475e0752-de6e-4a12-a891-1fd90ad1e7b3"
                }
            ]
        },
        {
            "id": "bd5daa9b-e199-429a-813c-5d5518299ae8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a79c3b52-7e74-4cbf-927c-2c2717a90539",
            "compositeImage": {
                "id": "0fd32e10-0f4c-435a-ab86-85ef757631b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd5daa9b-e199-429a-813c-5d5518299ae8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77afafe5-8e9f-4a43-bb72-550513980087",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd5daa9b-e199-429a-813c-5d5518299ae8",
                    "LayerId": "475e0752-de6e-4a12-a891-1fd90ad1e7b3"
                }
            ]
        },
        {
            "id": "083dcc00-2e37-4c64-bfd3-4157b9cd8818",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a79c3b52-7e74-4cbf-927c-2c2717a90539",
            "compositeImage": {
                "id": "4419a0c7-e394-45f4-ac96-cd3f349af19a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "083dcc00-2e37-4c64-bfd3-4157b9cd8818",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d52c360-b787-48c2-b881-ead23e5cdcfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "083dcc00-2e37-4c64-bfd3-4157b9cd8818",
                    "LayerId": "475e0752-de6e-4a12-a891-1fd90ad1e7b3"
                }
            ]
        },
        {
            "id": "f42a93d1-8afd-4bdf-aeb2-e23d5c732c39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a79c3b52-7e74-4cbf-927c-2c2717a90539",
            "compositeImage": {
                "id": "cc69e6e2-def2-4be8-b7a3-aa1ad5139924",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f42a93d1-8afd-4bdf-aeb2-e23d5c732c39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "561621b3-4b12-48d4-b4e9-8043dff54f73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f42a93d1-8afd-4bdf-aeb2-e23d5c732c39",
                    "LayerId": "475e0752-de6e-4a12-a891-1fd90ad1e7b3"
                }
            ]
        },
        {
            "id": "6576d67f-882f-4d54-9045-8d41464d0e4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a79c3b52-7e74-4cbf-927c-2c2717a90539",
            "compositeImage": {
                "id": "61abc71a-3b1a-4809-8282-5cfb16bd065e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6576d67f-882f-4d54-9045-8d41464d0e4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01729b22-fcf5-45d2-b96d-9c86a751be1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6576d67f-882f-4d54-9045-8d41464d0e4f",
                    "LayerId": "475e0752-de6e-4a12-a891-1fd90ad1e7b3"
                }
            ]
        },
        {
            "id": "1f725c44-2386-45bd-8b82-84f63747c291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a79c3b52-7e74-4cbf-927c-2c2717a90539",
            "compositeImage": {
                "id": "5b812cee-858b-4f76-b352-074b65a43886",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f725c44-2386-45bd-8b82-84f63747c291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb440772-6a73-4e28-8a49-21b331d4197f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f725c44-2386-45bd-8b82-84f63747c291",
                    "LayerId": "475e0752-de6e-4a12-a891-1fd90ad1e7b3"
                }
            ]
        },
        {
            "id": "7ef63173-7201-48ee-aabe-5a0241af699e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a79c3b52-7e74-4cbf-927c-2c2717a90539",
            "compositeImage": {
                "id": "9bfb17d7-fcc8-4e75-ae83-4d1b77dc868c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ef63173-7201-48ee-aabe-5a0241af699e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80ef1f5a-2869-456d-95eb-25a252f084ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ef63173-7201-48ee-aabe-5a0241af699e",
                    "LayerId": "475e0752-de6e-4a12-a891-1fd90ad1e7b3"
                }
            ]
        },
        {
            "id": "df40a381-8aca-4d79-8338-60d7a9752658",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a79c3b52-7e74-4cbf-927c-2c2717a90539",
            "compositeImage": {
                "id": "631f00f2-e5cc-4068-82da-38035659604e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df40a381-8aca-4d79-8338-60d7a9752658",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dacc211b-df91-434a-aab4-f74613b402f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df40a381-8aca-4d79-8338-60d7a9752658",
                    "LayerId": "475e0752-de6e-4a12-a891-1fd90ad1e7b3"
                }
            ]
        },
        {
            "id": "d6a5ba67-a729-45ea-82b0-61a719915387",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a79c3b52-7e74-4cbf-927c-2c2717a90539",
            "compositeImage": {
                "id": "0ebe1557-b454-438d-8009-42d6dde4ccef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6a5ba67-a729-45ea-82b0-61a719915387",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc2b709f-b6dd-4ba2-9a1c-4ca030969b93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6a5ba67-a729-45ea-82b0-61a719915387",
                    "LayerId": "475e0752-de6e-4a12-a891-1fd90ad1e7b3"
                }
            ]
        },
        {
            "id": "16f01fb0-0792-49d0-9106-a06450863e0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a79c3b52-7e74-4cbf-927c-2c2717a90539",
            "compositeImage": {
                "id": "c88edaad-8da6-4a8a-94f0-b707b7e0349c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16f01fb0-0792-49d0-9106-a06450863e0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57c0fb01-b4c1-486e-b510-b89802e0dd4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16f01fb0-0792-49d0-9106-a06450863e0a",
                    "LayerId": "475e0752-de6e-4a12-a891-1fd90ad1e7b3"
                }
            ]
        },
        {
            "id": "ec21e9dc-1b59-4ff4-a678-fd775c04a585",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a79c3b52-7e74-4cbf-927c-2c2717a90539",
            "compositeImage": {
                "id": "f49f0f80-8777-4c85-a321-0e70053117af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec21e9dc-1b59-4ff4-a678-fd775c04a585",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f694b04-1004-4d09-b389-fe19fd790ab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec21e9dc-1b59-4ff4-a678-fd775c04a585",
                    "LayerId": "475e0752-de6e-4a12-a891-1fd90ad1e7b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "475e0752-de6e-4a12-a891-1fd90ad1e7b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a79c3b52-7e74-4cbf-927c-2c2717a90539",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 21
}