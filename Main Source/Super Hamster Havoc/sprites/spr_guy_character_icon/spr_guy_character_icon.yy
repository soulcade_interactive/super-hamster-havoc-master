{
    "id": "7b4973ed-4bb3-43c4-baaa-9313917a4058",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_character_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 14,
    "bbox_right": 113,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd2e6d72-dcf0-45bd-b0ed-03a70ea809ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7b4973ed-4bb3-43c4-baaa-9313917a4058",
            "compositeImage": {
                "id": "2ec673b2-25d2-4cc1-947f-de9184477d82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd2e6d72-dcf0-45bd-b0ed-03a70ea809ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55b61e5b-f352-4378-9ab2-29443b2d2327",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd2e6d72-dcf0-45bd-b0ed-03a70ea809ab",
                    "LayerId": "16b9d78c-2925-4615-a190-931c64db7a80"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "16b9d78c-2925-4615-a190-931c64db7a80",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7b4973ed-4bb3-43c4-baaa-9313917a4058",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}