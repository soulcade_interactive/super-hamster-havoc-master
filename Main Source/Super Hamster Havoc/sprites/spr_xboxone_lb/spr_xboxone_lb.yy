{
    "id": "301e1526-a94e-487c-af61-f18eb63333fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_xboxone_lb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 6,
    "bbox_right": 93,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "61991326-5c72-4752-8b1c-3d9620c0adab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "301e1526-a94e-487c-af61-f18eb63333fd",
            "compositeImage": {
                "id": "2ade4cbd-6f2e-4004-a7c8-0d89b44d2156",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61991326-5c72-4752-8b1c-3d9620c0adab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83025798-f08b-4bb8-a79b-cf72b645060c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61991326-5c72-4752-8b1c-3d9620c0adab",
                    "LayerId": "ffd277e5-b7f6-4f49-ab84-fba9fea99e24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "ffd277e5-b7f6-4f49-ab84-fba9fea99e24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "301e1526-a94e-487c-af61-f18eb63333fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}