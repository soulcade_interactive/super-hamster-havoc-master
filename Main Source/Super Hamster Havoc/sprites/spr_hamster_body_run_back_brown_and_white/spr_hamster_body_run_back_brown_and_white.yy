{
    "id": "8fc0526a-47b5-419d-82f9-16168c1b56f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_body_run_back_brown_and_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec9ff434-d7d8-4166-9870-cc7170d09608",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fc0526a-47b5-419d-82f9-16168c1b56f8",
            "compositeImage": {
                "id": "d47b1603-3fdd-4848-beda-4a6035807128",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec9ff434-d7d8-4166-9870-cc7170d09608",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a188ae90-1eaf-4f6e-92e4-f40930481a7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec9ff434-d7d8-4166-9870-cc7170d09608",
                    "LayerId": "aa7c23db-35ad-4ec0-9948-22d9f533639f"
                }
            ]
        },
        {
            "id": "14aeeb86-547d-4471-bd23-48ee22a1d173",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fc0526a-47b5-419d-82f9-16168c1b56f8",
            "compositeImage": {
                "id": "a4078d09-acc2-40ef-b5a0-a9cfb2ed9498",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14aeeb86-547d-4471-bd23-48ee22a1d173",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5ec8119-d27b-47f5-a7c6-28c4f1a52d79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14aeeb86-547d-4471-bd23-48ee22a1d173",
                    "LayerId": "aa7c23db-35ad-4ec0-9948-22d9f533639f"
                }
            ]
        },
        {
            "id": "06aa988f-e21d-4c84-a7da-3ca6e049c1ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fc0526a-47b5-419d-82f9-16168c1b56f8",
            "compositeImage": {
                "id": "3470f669-0acc-4065-ac46-7933c7cc4d44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06aa988f-e21d-4c84-a7da-3ca6e049c1ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf1acb92-4fda-4186-b31b-c52531c8bfbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06aa988f-e21d-4c84-a7da-3ca6e049c1ad",
                    "LayerId": "aa7c23db-35ad-4ec0-9948-22d9f533639f"
                }
            ]
        },
        {
            "id": "c19ca1ec-62d2-459c-9b01-e68c541d0439",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fc0526a-47b5-419d-82f9-16168c1b56f8",
            "compositeImage": {
                "id": "539313f3-6a05-438f-bea3-b7f363791ca5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c19ca1ec-62d2-459c-9b01-e68c541d0439",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f66edba-3c1a-416e-9cdd-55ebe9c191b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c19ca1ec-62d2-459c-9b01-e68c541d0439",
                    "LayerId": "aa7c23db-35ad-4ec0-9948-22d9f533639f"
                }
            ]
        },
        {
            "id": "266cea66-7f98-4b22-8b0e-7fa8de49d765",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fc0526a-47b5-419d-82f9-16168c1b56f8",
            "compositeImage": {
                "id": "4be91758-0115-4b8e-b1cd-ad75c55ede33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "266cea66-7f98-4b22-8b0e-7fa8de49d765",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13758a95-cc90-4063-bdad-6e6653335594",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "266cea66-7f98-4b22-8b0e-7fa8de49d765",
                    "LayerId": "aa7c23db-35ad-4ec0-9948-22d9f533639f"
                }
            ]
        },
        {
            "id": "c0df45d7-2375-4bcc-a670-28740ba84cde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fc0526a-47b5-419d-82f9-16168c1b56f8",
            "compositeImage": {
                "id": "66d3e6e7-c37e-4d93-a91e-e5ae019e3d6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0df45d7-2375-4bcc-a670-28740ba84cde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6814e4f-4b17-4483-8d34-2f7bf27f4c6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0df45d7-2375-4bcc-a670-28740ba84cde",
                    "LayerId": "aa7c23db-35ad-4ec0-9948-22d9f533639f"
                }
            ]
        },
        {
            "id": "853ed820-f815-4922-ba7e-24a239e462dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fc0526a-47b5-419d-82f9-16168c1b56f8",
            "compositeImage": {
                "id": "14a597e4-741f-412e-9d46-26a4547328a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "853ed820-f815-4922-ba7e-24a239e462dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e524fc6-fb80-4050-83d7-63eb943edf58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "853ed820-f815-4922-ba7e-24a239e462dc",
                    "LayerId": "aa7c23db-35ad-4ec0-9948-22d9f533639f"
                }
            ]
        },
        {
            "id": "b6108c99-bae6-4b40-a3f7-a5f2cd981f8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fc0526a-47b5-419d-82f9-16168c1b56f8",
            "compositeImage": {
                "id": "1eb3d2eb-7c01-4b00-adfb-1f76cd5d22a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6108c99-bae6-4b40-a3f7-a5f2cd981f8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a5c10a5-2ab1-40ec-85af-f3f5a1f6a8fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6108c99-bae6-4b40-a3f7-a5f2cd981f8c",
                    "LayerId": "aa7c23db-35ad-4ec0-9948-22d9f533639f"
                }
            ]
        },
        {
            "id": "c039a2e1-b6fe-41b7-8eec-2b5b0f813f48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fc0526a-47b5-419d-82f9-16168c1b56f8",
            "compositeImage": {
                "id": "70371ffc-44ca-4708-b3ce-70c521b1d884",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c039a2e1-b6fe-41b7-8eec-2b5b0f813f48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c77bfaa3-3f62-48dd-a826-34aa2afec526",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c039a2e1-b6fe-41b7-8eec-2b5b0f813f48",
                    "LayerId": "aa7c23db-35ad-4ec0-9948-22d9f533639f"
                }
            ]
        },
        {
            "id": "a38eb91e-57ea-4886-9220-e3e44374ae9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fc0526a-47b5-419d-82f9-16168c1b56f8",
            "compositeImage": {
                "id": "5c18378a-796f-4caa-9331-6c5b132a6358",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a38eb91e-57ea-4886-9220-e3e44374ae9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "688c562a-b162-42ea-a7c6-46b09fb7c925",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a38eb91e-57ea-4886-9220-e3e44374ae9d",
                    "LayerId": "aa7c23db-35ad-4ec0-9948-22d9f533639f"
                }
            ]
        },
        {
            "id": "f2fbe3b8-88ee-49f9-bcd8-4bc37c5d909b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fc0526a-47b5-419d-82f9-16168c1b56f8",
            "compositeImage": {
                "id": "23b90303-b428-4642-957c-94466b074111",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2fbe3b8-88ee-49f9-bcd8-4bc37c5d909b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec2c60bc-092a-4bd3-8a88-4f31338443b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2fbe3b8-88ee-49f9-bcd8-4bc37c5d909b",
                    "LayerId": "aa7c23db-35ad-4ec0-9948-22d9f533639f"
                }
            ]
        },
        {
            "id": "a5b2a5fb-ef2f-4690-bab5-8c34d9ae6c27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fc0526a-47b5-419d-82f9-16168c1b56f8",
            "compositeImage": {
                "id": "e2e9a95d-41ad-4cb3-a15f-54e0ac763ad3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5b2a5fb-ef2f-4690-bab5-8c34d9ae6c27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9e2f03a-5b56-43dc-99ff-2b0e674b8f59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5b2a5fb-ef2f-4690-bab5-8c34d9ae6c27",
                    "LayerId": "aa7c23db-35ad-4ec0-9948-22d9f533639f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "aa7c23db-35ad-4ec0-9948-22d9f533639f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8fc0526a-47b5-419d-82f9-16168c1b56f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 21
}