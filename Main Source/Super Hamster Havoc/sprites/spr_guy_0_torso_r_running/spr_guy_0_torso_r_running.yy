{
    "id": "6c61196e-9b0e-4c65-98b2-3aeeac1d2616",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_0_torso_r_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 21,
    "bbox_right": 41,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1438216b-a88b-4aac-8bc4-97075d8893be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c61196e-9b0e-4c65-98b2-3aeeac1d2616",
            "compositeImage": {
                "id": "3d838e2d-4be1-4e49-ad99-1f42be6d74d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1438216b-a88b-4aac-8bc4-97075d8893be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8c77234-549c-4c40-8a0f-62be87da573a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1438216b-a88b-4aac-8bc4-97075d8893be",
                    "LayerId": "ffee1521-a4a3-4d13-aeb9-c00eb70d63a3"
                }
            ]
        },
        {
            "id": "468989e6-8593-4d34-b9b4-d9cbac737b1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c61196e-9b0e-4c65-98b2-3aeeac1d2616",
            "compositeImage": {
                "id": "a8a4d44d-f30b-400a-9907-47f33799c028",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "468989e6-8593-4d34-b9b4-d9cbac737b1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1536cfaa-1d67-4ab9-b36c-d45b2f7ad859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "468989e6-8593-4d34-b9b4-d9cbac737b1d",
                    "LayerId": "ffee1521-a4a3-4d13-aeb9-c00eb70d63a3"
                }
            ]
        },
        {
            "id": "f6cdcaed-5152-43e1-b0ae-7a32f5ab61ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c61196e-9b0e-4c65-98b2-3aeeac1d2616",
            "compositeImage": {
                "id": "c26288dd-3787-4158-8b96-a2e03890c4f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6cdcaed-5152-43e1-b0ae-7a32f5ab61ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "974e0f5b-c04d-4e6f-9dcb-55237155bf06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6cdcaed-5152-43e1-b0ae-7a32f5ab61ea",
                    "LayerId": "ffee1521-a4a3-4d13-aeb9-c00eb70d63a3"
                }
            ]
        },
        {
            "id": "cf1e1c6b-61e8-481b-a636-ca8eebd8801f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c61196e-9b0e-4c65-98b2-3aeeac1d2616",
            "compositeImage": {
                "id": "c0954002-5510-4f86-82ea-c3b32480e2cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf1e1c6b-61e8-481b-a636-ca8eebd8801f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7139f7fc-8338-4c3d-9d32-99eb7374d2b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf1e1c6b-61e8-481b-a636-ca8eebd8801f",
                    "LayerId": "ffee1521-a4a3-4d13-aeb9-c00eb70d63a3"
                }
            ]
        },
        {
            "id": "371bea76-3537-4180-adda-6b912e654559",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c61196e-9b0e-4c65-98b2-3aeeac1d2616",
            "compositeImage": {
                "id": "8fb71684-9c66-45dc-aef9-0bece7b6d5d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "371bea76-3537-4180-adda-6b912e654559",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45175421-6f98-4721-b1c7-bc85412afb3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "371bea76-3537-4180-adda-6b912e654559",
                    "LayerId": "ffee1521-a4a3-4d13-aeb9-c00eb70d63a3"
                }
            ]
        },
        {
            "id": "de6e71a4-aac4-4727-8558-b3507881cac3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c61196e-9b0e-4c65-98b2-3aeeac1d2616",
            "compositeImage": {
                "id": "82470385-8454-43e2-8ab0-8e7bd9e8b6ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de6e71a4-aac4-4727-8558-b3507881cac3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91dc517d-a1e3-467a-83c7-aeba9dccfb1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de6e71a4-aac4-4727-8558-b3507881cac3",
                    "LayerId": "ffee1521-a4a3-4d13-aeb9-c00eb70d63a3"
                }
            ]
        },
        {
            "id": "d05d0c1b-0a54-4cd4-942e-9ef0bd6e7348",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c61196e-9b0e-4c65-98b2-3aeeac1d2616",
            "compositeImage": {
                "id": "31986175-9a5f-4238-a587-d7a03c7f07f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d05d0c1b-0a54-4cd4-942e-9ef0bd6e7348",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3aed5d6-13b7-4999-90c5-91bc0c6b365d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d05d0c1b-0a54-4cd4-942e-9ef0bd6e7348",
                    "LayerId": "ffee1521-a4a3-4d13-aeb9-c00eb70d63a3"
                }
            ]
        },
        {
            "id": "3fd22298-ccc8-4d6c-8f6a-453776cc9ff1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c61196e-9b0e-4c65-98b2-3aeeac1d2616",
            "compositeImage": {
                "id": "4f2c1853-939d-4eea-8553-a67e328c6b2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fd22298-ccc8-4d6c-8f6a-453776cc9ff1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2c60fd4-1332-4e47-990f-ea2c3ec73330",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fd22298-ccc8-4d6c-8f6a-453776cc9ff1",
                    "LayerId": "ffee1521-a4a3-4d13-aeb9-c00eb70d63a3"
                }
            ]
        },
        {
            "id": "5957f672-b64c-4d30-994d-f6840a1fbba8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c61196e-9b0e-4c65-98b2-3aeeac1d2616",
            "compositeImage": {
                "id": "e8285f19-e398-43a1-953d-3c0260b1e517",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5957f672-b64c-4d30-994d-f6840a1fbba8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeeed15a-8ace-4878-b743-1650c730e5dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5957f672-b64c-4d30-994d-f6840a1fbba8",
                    "LayerId": "ffee1521-a4a3-4d13-aeb9-c00eb70d63a3"
                }
            ]
        },
        {
            "id": "a055e2e8-163c-4332-918f-22e38b6f91fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c61196e-9b0e-4c65-98b2-3aeeac1d2616",
            "compositeImage": {
                "id": "25c67600-1119-4c6c-872d-51acbd966fc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a055e2e8-163c-4332-918f-22e38b6f91fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0267e0de-29c6-49c0-af9e-8028a6ccfd54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a055e2e8-163c-4332-918f-22e38b6f91fe",
                    "LayerId": "ffee1521-a4a3-4d13-aeb9-c00eb70d63a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ffee1521-a4a3-4d13-aeb9-c00eb70d63a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c61196e-9b0e-4c65-98b2-3aeeac1d2616",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}