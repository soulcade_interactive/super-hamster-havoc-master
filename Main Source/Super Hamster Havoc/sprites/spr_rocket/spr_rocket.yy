{
    "id": "dea7d15a-5017-4ad5-b97d-44483cbe9ece",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rocket",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 32,
    "bbox_right": 54,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ccc9adea-d738-4f9b-a8e9-bfbe5b6a240c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dea7d15a-5017-4ad5-b97d-44483cbe9ece",
            "compositeImage": {
                "id": "5ca18fcd-3867-4fbd-9533-7b7ff04f0a0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccc9adea-d738-4f9b-a8e9-bfbe5b6a240c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7589fed-2ea8-4d79-88f1-9b608a839c1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccc9adea-d738-4f9b-a8e9-bfbe5b6a240c",
                    "LayerId": "b96d9373-dde4-4be8-9b63-18c9e0a27d1c"
                }
            ]
        },
        {
            "id": "0ea502de-b12d-44cc-9d33-09fc0d9928bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dea7d15a-5017-4ad5-b97d-44483cbe9ece",
            "compositeImage": {
                "id": "4e02fd1f-a8c9-4d39-b637-2f977892fdd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ea502de-b12d-44cc-9d33-09fc0d9928bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26356c97-b6af-49a9-9303-35c6994f9b70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ea502de-b12d-44cc-9d33-09fc0d9928bd",
                    "LayerId": "b96d9373-dde4-4be8-9b63-18c9e0a27d1c"
                }
            ]
        },
        {
            "id": "a185c93c-6d92-4497-a3a8-fc7242a05587",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dea7d15a-5017-4ad5-b97d-44483cbe9ece",
            "compositeImage": {
                "id": "9b197759-1e31-4df2-b1d0-3c192b8ffa95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a185c93c-6d92-4497-a3a8-fc7242a05587",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "750782f2-273e-4fad-b4a3-ddc508a0e74b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a185c93c-6d92-4497-a3a8-fc7242a05587",
                    "LayerId": "b96d9373-dde4-4be8-9b63-18c9e0a27d1c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b96d9373-dde4-4be8-9b63-18c9e0a27d1c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dea7d15a-5017-4ad5-b97d-44483cbe9ece",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}