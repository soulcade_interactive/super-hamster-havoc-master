{
    "id": "48a513b7-cf49-4759-82de-6ebd2554ba19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buff_0_torso_l_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 7,
    "bbox_right": 56,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "288f6e17-c7ad-4b18-9d1d-e290347cbbc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48a513b7-cf49-4759-82de-6ebd2554ba19",
            "compositeImage": {
                "id": "60c3f031-e6b3-4a97-b1c9-9fe0ba651725",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "288f6e17-c7ad-4b18-9d1d-e290347cbbc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "013a34fe-984d-418b-b4e5-989e69bc13d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "288f6e17-c7ad-4b18-9d1d-e290347cbbc0",
                    "LayerId": "203b9dab-a197-42ad-88da-83f1cd440dae"
                }
            ]
        },
        {
            "id": "88d4e08c-ccd2-4308-85d6-6cd31e07fd2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48a513b7-cf49-4759-82de-6ebd2554ba19",
            "compositeImage": {
                "id": "df86a18d-493f-4d1d-af09-37a6a66a7d90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88d4e08c-ccd2-4308-85d6-6cd31e07fd2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d9043f1-182e-4092-a7e7-27bbb309105d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88d4e08c-ccd2-4308-85d6-6cd31e07fd2e",
                    "LayerId": "203b9dab-a197-42ad-88da-83f1cd440dae"
                }
            ]
        },
        {
            "id": "c4b3214a-6899-4e1a-b0e4-f7fa84544c8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48a513b7-cf49-4759-82de-6ebd2554ba19",
            "compositeImage": {
                "id": "f23747d1-34a2-4d9f-8cac-48b3358e3db6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4b3214a-6899-4e1a-b0e4-f7fa84544c8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc575e36-3c45-4762-af04-bddc891fa6cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4b3214a-6899-4e1a-b0e4-f7fa84544c8a",
                    "LayerId": "203b9dab-a197-42ad-88da-83f1cd440dae"
                }
            ]
        },
        {
            "id": "0765a7cf-35e2-4d71-9b92-2cd48bb1ba89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48a513b7-cf49-4759-82de-6ebd2554ba19",
            "compositeImage": {
                "id": "78c377aa-4a21-4d4d-a4e2-57de19fa3bd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0765a7cf-35e2-4d71-9b92-2cd48bb1ba89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d55c9dba-3429-4149-a4e5-db65c7228dd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0765a7cf-35e2-4d71-9b92-2cd48bb1ba89",
                    "LayerId": "203b9dab-a197-42ad-88da-83f1cd440dae"
                }
            ]
        },
        {
            "id": "8ddfca24-1198-4e6a-a67c-31443d9e8240",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48a513b7-cf49-4759-82de-6ebd2554ba19",
            "compositeImage": {
                "id": "c09586d9-3b9e-436c-adb0-ba717e60710a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ddfca24-1198-4e6a-a67c-31443d9e8240",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec00789e-7fe3-4834-9ed8-21d5cd0cd88a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ddfca24-1198-4e6a-a67c-31443d9e8240",
                    "LayerId": "203b9dab-a197-42ad-88da-83f1cd440dae"
                }
            ]
        },
        {
            "id": "a77846b3-ce1d-4265-bc72-0e71bd4927cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48a513b7-cf49-4759-82de-6ebd2554ba19",
            "compositeImage": {
                "id": "0ef3a6af-ed1d-49ba-a847-f6a05c1af769",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a77846b3-ce1d-4265-bc72-0e71bd4927cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d76a9a7e-6098-4368-a305-3d3aeb6da4c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a77846b3-ce1d-4265-bc72-0e71bd4927cf",
                    "LayerId": "203b9dab-a197-42ad-88da-83f1cd440dae"
                }
            ]
        },
        {
            "id": "8f87cc65-5b85-4836-a9da-227cd38632c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48a513b7-cf49-4759-82de-6ebd2554ba19",
            "compositeImage": {
                "id": "7aae59fb-3410-40cf-a8f8-9f9106db2311",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f87cc65-5b85-4836-a9da-227cd38632c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c40e097a-37e3-40f4-b555-6d13a62a2f25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f87cc65-5b85-4836-a9da-227cd38632c7",
                    "LayerId": "203b9dab-a197-42ad-88da-83f1cd440dae"
                }
            ]
        },
        {
            "id": "afe5dfd8-e91a-41a2-a526-c31d506fe755",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48a513b7-cf49-4759-82de-6ebd2554ba19",
            "compositeImage": {
                "id": "2c554a6a-9d56-467f-b739-194ebcfe8e76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afe5dfd8-e91a-41a2-a526-c31d506fe755",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24f0bc6d-ab7e-479a-8a28-76ed1ee94479",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afe5dfd8-e91a-41a2-a526-c31d506fe755",
                    "LayerId": "203b9dab-a197-42ad-88da-83f1cd440dae"
                }
            ]
        },
        {
            "id": "a14bf1fa-0ee2-4103-971b-1c7ddb9d5e30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48a513b7-cf49-4759-82de-6ebd2554ba19",
            "compositeImage": {
                "id": "7216322b-0789-4c85-9f63-a37fd6a26dbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a14bf1fa-0ee2-4103-971b-1c7ddb9d5e30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a9b2299-35a5-4a5c-9eba-b90814e28c57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a14bf1fa-0ee2-4103-971b-1c7ddb9d5e30",
                    "LayerId": "203b9dab-a197-42ad-88da-83f1cd440dae"
                }
            ]
        },
        {
            "id": "087bf18f-ccc9-462b-b96a-8bdc1c56987b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48a513b7-cf49-4759-82de-6ebd2554ba19",
            "compositeImage": {
                "id": "1e1b1b12-0e4d-4fc0-b75d-c9fd4c9a5571",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "087bf18f-ccc9-462b-b96a-8bdc1c56987b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40e4f734-0009-4de9-b611-b9a386547ebe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "087bf18f-ccc9-462b-b96a-8bdc1c56987b",
                    "LayerId": "203b9dab-a197-42ad-88da-83f1cd440dae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "203b9dab-a197-42ad-88da-83f1cd440dae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48a513b7-cf49-4759-82de-6ebd2554ba19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}