{
    "id": "c0f71da7-bb6b-4ec9-9e1e-4fd6be2a6e0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_1_head_u_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 17,
    "bbox_right": 46,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9f635d1-ce47-4f5f-8401-4436ecfe83ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0f71da7-bb6b-4ec9-9e1e-4fd6be2a6e0c",
            "compositeImage": {
                "id": "370069a2-4e9f-44c9-8d1f-687034c091c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9f635d1-ce47-4f5f-8401-4436ecfe83ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d13e7ab2-fda2-4d16-91e7-27f53ab04115",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9f635d1-ce47-4f5f-8401-4436ecfe83ff",
                    "LayerId": "5fe9d434-83fd-413a-815a-41ef79e1f9fb"
                }
            ]
        },
        {
            "id": "94222ab9-bfc2-437c-be59-7d4d83a5aa26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0f71da7-bb6b-4ec9-9e1e-4fd6be2a6e0c",
            "compositeImage": {
                "id": "ba40ca9a-959e-4e38-9a36-3acae8703ab5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94222ab9-bfc2-437c-be59-7d4d83a5aa26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9bc3888-aeb6-4b16-b948-fe20921b0df9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94222ab9-bfc2-437c-be59-7d4d83a5aa26",
                    "LayerId": "5fe9d434-83fd-413a-815a-41ef79e1f9fb"
                }
            ]
        },
        {
            "id": "a8e1702b-44c9-4ed2-8691-d49cf67725a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0f71da7-bb6b-4ec9-9e1e-4fd6be2a6e0c",
            "compositeImage": {
                "id": "89e27382-e89a-4502-830d-5cf4133c57d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8e1702b-44c9-4ed2-8691-d49cf67725a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74da66f9-0e85-48b2-a328-948f82a97005",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8e1702b-44c9-4ed2-8691-d49cf67725a3",
                    "LayerId": "5fe9d434-83fd-413a-815a-41ef79e1f9fb"
                }
            ]
        },
        {
            "id": "17b5654f-5028-4e7f-9af6-533bae813308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0f71da7-bb6b-4ec9-9e1e-4fd6be2a6e0c",
            "compositeImage": {
                "id": "78bac812-3b17-4c89-865f-ecaad47975de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17b5654f-5028-4e7f-9af6-533bae813308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6859101-0da0-448d-9309-c340126f4090",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17b5654f-5028-4e7f-9af6-533bae813308",
                    "LayerId": "5fe9d434-83fd-413a-815a-41ef79e1f9fb"
                }
            ]
        },
        {
            "id": "1f3cdf4d-5719-47fb-8868-3373a36a6851",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0f71da7-bb6b-4ec9-9e1e-4fd6be2a6e0c",
            "compositeImage": {
                "id": "f04e8c28-85c0-4f59-8325-1075c96a2c2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f3cdf4d-5719-47fb-8868-3373a36a6851",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "852fd3b8-121e-4286-9705-d0efa7e17f01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f3cdf4d-5719-47fb-8868-3373a36a6851",
                    "LayerId": "5fe9d434-83fd-413a-815a-41ef79e1f9fb"
                }
            ]
        },
        {
            "id": "b5a0b301-5484-4297-a642-ce991e541a8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0f71da7-bb6b-4ec9-9e1e-4fd6be2a6e0c",
            "compositeImage": {
                "id": "f82265e5-f77a-4ca2-a289-43e918356c81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5a0b301-5484-4297-a642-ce991e541a8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c79d667-6858-4bf7-9c92-6bb14a2a1f69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5a0b301-5484-4297-a642-ce991e541a8e",
                    "LayerId": "5fe9d434-83fd-413a-815a-41ef79e1f9fb"
                }
            ]
        },
        {
            "id": "22d83090-92ed-47e9-93bc-678fb98ace90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0f71da7-bb6b-4ec9-9e1e-4fd6be2a6e0c",
            "compositeImage": {
                "id": "cacb51c6-8992-4290-afb8-8c21e8939962",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22d83090-92ed-47e9-93bc-678fb98ace90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cc838b5-6def-4c33-ba44-5d727558527e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22d83090-92ed-47e9-93bc-678fb98ace90",
                    "LayerId": "5fe9d434-83fd-413a-815a-41ef79e1f9fb"
                }
            ]
        },
        {
            "id": "32031fe8-7cf0-44af-aebf-618e47f86806",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0f71da7-bb6b-4ec9-9e1e-4fd6be2a6e0c",
            "compositeImage": {
                "id": "58619d8a-1203-4929-a3c5-e9d2d74f5ebc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32031fe8-7cf0-44af-aebf-618e47f86806",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c8bf2b7-36fd-4dd0-ac4b-8763545ccee4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32031fe8-7cf0-44af-aebf-618e47f86806",
                    "LayerId": "5fe9d434-83fd-413a-815a-41ef79e1f9fb"
                }
            ]
        },
        {
            "id": "2d753873-04e4-4a35-8d3f-75878bdd01bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0f71da7-bb6b-4ec9-9e1e-4fd6be2a6e0c",
            "compositeImage": {
                "id": "7d5b96d7-e972-43a4-b9c5-1257c8d68c8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d753873-04e4-4a35-8d3f-75878bdd01bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "376700a8-5f73-484b-9994-7bd8195e934c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d753873-04e4-4a35-8d3f-75878bdd01bc",
                    "LayerId": "5fe9d434-83fd-413a-815a-41ef79e1f9fb"
                }
            ]
        },
        {
            "id": "d6434742-d12a-4075-a60a-67dd89adb7b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0f71da7-bb6b-4ec9-9e1e-4fd6be2a6e0c",
            "compositeImage": {
                "id": "b68fb1e3-148c-4998-86f3-8465d9fc37f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6434742-d12a-4075-a60a-67dd89adb7b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b89038c6-1799-4c68-b6a8-8ef8ea67e0f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6434742-d12a-4075-a60a-67dd89adb7b1",
                    "LayerId": "5fe9d434-83fd-413a-815a-41ef79e1f9fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5fe9d434-83fd-413a-815a-41ef79e1f9fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0f71da7-bb6b-4ec9-9e1e-4fd6be2a6e0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}