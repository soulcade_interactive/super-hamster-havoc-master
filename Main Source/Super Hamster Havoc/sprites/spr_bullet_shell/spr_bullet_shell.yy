{
    "id": "0ff6eba4-a601-4ffd-a9a7-be3a3b2c7039",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet_shell",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21cf7c3c-3af5-4e30-ae03-a532deff3768",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ff6eba4-a601-4ffd-a9a7-be3a3b2c7039",
            "compositeImage": {
                "id": "d38b8f18-c1d1-44a9-93be-772e7f195d98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21cf7c3c-3af5-4e30-ae03-a532deff3768",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a843898c-9142-4957-af51-00ecf375b153",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21cf7c3c-3af5-4e30-ae03-a532deff3768",
                    "LayerId": "db80f350-072e-49d6-979c-2dcb477f573b"
                }
            ]
        },
        {
            "id": "e055e4f5-0960-4367-a3cb-e1e3c39ec070",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ff6eba4-a601-4ffd-a9a7-be3a3b2c7039",
            "compositeImage": {
                "id": "d7ac2156-042e-4b51-ba05-58fdf063a3d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e055e4f5-0960-4367-a3cb-e1e3c39ec070",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a17f01c-538e-4379-8812-051463fb108f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e055e4f5-0960-4367-a3cb-e1e3c39ec070",
                    "LayerId": "db80f350-072e-49d6-979c-2dcb477f573b"
                }
            ]
        },
        {
            "id": "6140ef17-d284-404a-a760-1aeee3e8756d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ff6eba4-a601-4ffd-a9a7-be3a3b2c7039",
            "compositeImage": {
                "id": "ce6fd15a-eccb-4247-8bb8-6d4d1d98e3de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6140ef17-d284-404a-a760-1aeee3e8756d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2319700-f132-4eb8-bf4c-af5f4b11dea0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6140ef17-d284-404a-a760-1aeee3e8756d",
                    "LayerId": "db80f350-072e-49d6-979c-2dcb477f573b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "db80f350-072e-49d6-979c-2dcb477f573b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ff6eba4-a601-4ffd-a9a7-be3a3b2c7039",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}