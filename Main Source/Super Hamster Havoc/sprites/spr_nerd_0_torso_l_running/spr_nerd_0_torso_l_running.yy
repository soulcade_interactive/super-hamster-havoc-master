{
    "id": "dfb85476-90b2-4871-951d-510e4bb4870d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nerd_0_torso_l_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 24,
    "bbox_right": 41,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f88e52f0-6df2-49f8-b268-e9e051d33967",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfb85476-90b2-4871-951d-510e4bb4870d",
            "compositeImage": {
                "id": "ece3448e-018e-4dfc-af81-b81f69b5e986",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f88e52f0-6df2-49f8-b268-e9e051d33967",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f8626b4-1d11-4525-96f5-9267b0679219",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f88e52f0-6df2-49f8-b268-e9e051d33967",
                    "LayerId": "bea8beb9-d34b-4539-9708-a65617d96d7e"
                }
            ]
        },
        {
            "id": "2f3ec43f-68d4-4a0b-9370-770fec415312",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfb85476-90b2-4871-951d-510e4bb4870d",
            "compositeImage": {
                "id": "2ea6ed36-09b3-43b9-82a5-782f3fb10641",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f3ec43f-68d4-4a0b-9370-770fec415312",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67ae9bbb-ff23-43c7-9fb3-7ddd2ba816c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f3ec43f-68d4-4a0b-9370-770fec415312",
                    "LayerId": "bea8beb9-d34b-4539-9708-a65617d96d7e"
                }
            ]
        },
        {
            "id": "8fbceb6c-dfce-4373-905c-fc4206bc3d9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfb85476-90b2-4871-951d-510e4bb4870d",
            "compositeImage": {
                "id": "85fdf10a-f12e-4734-b5bf-33ad49f986c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fbceb6c-dfce-4373-905c-fc4206bc3d9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7a36b3c-1703-4f71-9a84-82489825c9f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fbceb6c-dfce-4373-905c-fc4206bc3d9b",
                    "LayerId": "bea8beb9-d34b-4539-9708-a65617d96d7e"
                }
            ]
        },
        {
            "id": "537b917c-fc99-473b-b65f-3dfd09d62c26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfb85476-90b2-4871-951d-510e4bb4870d",
            "compositeImage": {
                "id": "aab63a27-4c2b-43bd-8318-33f36df678b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "537b917c-fc99-473b-b65f-3dfd09d62c26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b20850c-9990-44f2-8115-a014e69e9bf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "537b917c-fc99-473b-b65f-3dfd09d62c26",
                    "LayerId": "bea8beb9-d34b-4539-9708-a65617d96d7e"
                }
            ]
        },
        {
            "id": "77da03b5-ecfc-4d95-8abc-7828522a5a80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfb85476-90b2-4871-951d-510e4bb4870d",
            "compositeImage": {
                "id": "26b11106-b8a4-4201-bfce-c325c2a9b954",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77da03b5-ecfc-4d95-8abc-7828522a5a80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0b79290-89c5-495d-b57f-e967c68a8d48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77da03b5-ecfc-4d95-8abc-7828522a5a80",
                    "LayerId": "bea8beb9-d34b-4539-9708-a65617d96d7e"
                }
            ]
        },
        {
            "id": "72c09deb-a3d5-46f6-806b-0bb3fa771205",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfb85476-90b2-4871-951d-510e4bb4870d",
            "compositeImage": {
                "id": "6140990e-604b-4bfc-8fb8-478bd2008f8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72c09deb-a3d5-46f6-806b-0bb3fa771205",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "219ad153-7d26-43cd-9565-d1a26e5fcebf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72c09deb-a3d5-46f6-806b-0bb3fa771205",
                    "LayerId": "bea8beb9-d34b-4539-9708-a65617d96d7e"
                }
            ]
        },
        {
            "id": "53614971-214e-4dbb-98d2-89a500c4909e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfb85476-90b2-4871-951d-510e4bb4870d",
            "compositeImage": {
                "id": "84d1a020-69c9-4596-842f-1d97119ed955",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53614971-214e-4dbb-98d2-89a500c4909e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1145f61-84d4-43dd-87cb-d3aefb56f375",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53614971-214e-4dbb-98d2-89a500c4909e",
                    "LayerId": "bea8beb9-d34b-4539-9708-a65617d96d7e"
                }
            ]
        },
        {
            "id": "fff3e471-702c-40ab-9480-7dda12f1e34f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfb85476-90b2-4871-951d-510e4bb4870d",
            "compositeImage": {
                "id": "a530f6e4-05ab-48db-a505-ab2f1df8d76b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fff3e471-702c-40ab-9480-7dda12f1e34f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8021df3-cf95-4b77-a590-67b222373aa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fff3e471-702c-40ab-9480-7dda12f1e34f",
                    "LayerId": "bea8beb9-d34b-4539-9708-a65617d96d7e"
                }
            ]
        },
        {
            "id": "cedbdd6c-cfa6-4064-852c-f4699e331998",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfb85476-90b2-4871-951d-510e4bb4870d",
            "compositeImage": {
                "id": "d4f3a076-a4a9-41ae-8500-2f0bbd2fd2e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cedbdd6c-cfa6-4064-852c-f4699e331998",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bba78a4b-9189-41ee-98c7-e8f30b61b21e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cedbdd6c-cfa6-4064-852c-f4699e331998",
                    "LayerId": "bea8beb9-d34b-4539-9708-a65617d96d7e"
                }
            ]
        },
        {
            "id": "7a9242f6-a29d-4134-ace3-11a735c0743a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfb85476-90b2-4871-951d-510e4bb4870d",
            "compositeImage": {
                "id": "9a7c1c5e-6f60-4d2f-922b-1b40884dbdd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a9242f6-a29d-4134-ace3-11a735c0743a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77975454-facb-432e-8827-6a42a6d52ce1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a9242f6-a29d-4134-ace3-11a735c0743a",
                    "LayerId": "bea8beb9-d34b-4539-9708-a65617d96d7e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bea8beb9-d34b-4539-9708-a65617d96d7e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dfb85476-90b2-4871-951d-510e4bb4870d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}