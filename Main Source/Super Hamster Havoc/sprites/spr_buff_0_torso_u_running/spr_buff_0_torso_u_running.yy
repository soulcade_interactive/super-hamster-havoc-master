{
    "id": "5ec092eb-98d5-4421-8a57-323cbc055896",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buff_0_torso_u_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 7,
    "bbox_right": 56,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4752dc1d-61b2-4480-8805-fbff9a1a176f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ec092eb-98d5-4421-8a57-323cbc055896",
            "compositeImage": {
                "id": "43d2dc2f-a102-4cec-bf3d-8006e9fad32c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4752dc1d-61b2-4480-8805-fbff9a1a176f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "835e0a92-942e-4015-b472-811e364d9bef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4752dc1d-61b2-4480-8805-fbff9a1a176f",
                    "LayerId": "26986a06-2956-48b9-b537-a9194d9655f4"
                }
            ]
        },
        {
            "id": "a3a92f2c-f5a9-4625-acf4-478c9baaa4f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ec092eb-98d5-4421-8a57-323cbc055896",
            "compositeImage": {
                "id": "fd1174ea-e8bc-4a39-b21b-09f13085db6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3a92f2c-f5a9-4625-acf4-478c9baaa4f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44d39dd3-d80e-48cb-ae01-93e336d83d13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3a92f2c-f5a9-4625-acf4-478c9baaa4f5",
                    "LayerId": "26986a06-2956-48b9-b537-a9194d9655f4"
                }
            ]
        },
        {
            "id": "94e4acdf-33f4-4093-80da-3c469c330343",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ec092eb-98d5-4421-8a57-323cbc055896",
            "compositeImage": {
                "id": "d6911dc2-3298-47fb-a8b1-cbfb95be8064",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94e4acdf-33f4-4093-80da-3c469c330343",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea59a2a3-03eb-411b-b2ac-241d426ca3c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94e4acdf-33f4-4093-80da-3c469c330343",
                    "LayerId": "26986a06-2956-48b9-b537-a9194d9655f4"
                }
            ]
        },
        {
            "id": "39ece5a6-ab8c-45df-9acf-67703a8d588d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ec092eb-98d5-4421-8a57-323cbc055896",
            "compositeImage": {
                "id": "bb1e1113-596f-452d-b758-e43ecead6d14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39ece5a6-ab8c-45df-9acf-67703a8d588d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4196df2c-e054-4f66-8c4d-a51c861790f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39ece5a6-ab8c-45df-9acf-67703a8d588d",
                    "LayerId": "26986a06-2956-48b9-b537-a9194d9655f4"
                }
            ]
        },
        {
            "id": "14287d98-26b7-4a4b-bebc-7202fe432197",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ec092eb-98d5-4421-8a57-323cbc055896",
            "compositeImage": {
                "id": "971f1fef-951f-4dc4-b8c4-29ad3d0a2619",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14287d98-26b7-4a4b-bebc-7202fe432197",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f88b958-6f1e-4a0b-b437-c120ff830345",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14287d98-26b7-4a4b-bebc-7202fe432197",
                    "LayerId": "26986a06-2956-48b9-b537-a9194d9655f4"
                }
            ]
        },
        {
            "id": "90c2d038-6a77-4181-9931-95307ed12b2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ec092eb-98d5-4421-8a57-323cbc055896",
            "compositeImage": {
                "id": "2bb03f34-c606-492d-9568-a18845605867",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90c2d038-6a77-4181-9931-95307ed12b2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a6930f2-f887-4127-8573-cec1b2c1f232",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90c2d038-6a77-4181-9931-95307ed12b2c",
                    "LayerId": "26986a06-2956-48b9-b537-a9194d9655f4"
                }
            ]
        },
        {
            "id": "7d4b90a9-02d4-4723-9f20-1749323a2c9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ec092eb-98d5-4421-8a57-323cbc055896",
            "compositeImage": {
                "id": "47fb0b66-09f3-42d4-a514-7229d360a6a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d4b90a9-02d4-4723-9f20-1749323a2c9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66341eff-223f-4235-a5c4-13b96c938e34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d4b90a9-02d4-4723-9f20-1749323a2c9b",
                    "LayerId": "26986a06-2956-48b9-b537-a9194d9655f4"
                }
            ]
        },
        {
            "id": "823c24b0-493c-4609-bb4a-53bf358ed817",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ec092eb-98d5-4421-8a57-323cbc055896",
            "compositeImage": {
                "id": "2d70d2f6-217b-4929-b0c8-eb8aeeacd66b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "823c24b0-493c-4609-bb4a-53bf358ed817",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7643d83-e671-4457-9718-d3c95288002d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "823c24b0-493c-4609-bb4a-53bf358ed817",
                    "LayerId": "26986a06-2956-48b9-b537-a9194d9655f4"
                }
            ]
        },
        {
            "id": "0647b01d-66c6-4dd2-b18d-b5919e0fccc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ec092eb-98d5-4421-8a57-323cbc055896",
            "compositeImage": {
                "id": "69f4311d-5acc-4924-a0fb-f79dd26d039e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0647b01d-66c6-4dd2-b18d-b5919e0fccc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c93ec9c-0807-4ecb-b2f3-76383765ade0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0647b01d-66c6-4dd2-b18d-b5919e0fccc8",
                    "LayerId": "26986a06-2956-48b9-b537-a9194d9655f4"
                }
            ]
        },
        {
            "id": "cc7bb33d-bcb8-4f39-8c31-8f12dd12e273",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ec092eb-98d5-4421-8a57-323cbc055896",
            "compositeImage": {
                "id": "9175fe64-df77-4860-852b-c66252fb4127",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc7bb33d-bcb8-4f39-8c31-8f12dd12e273",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0271eaa-8113-4274-a8b1-d5a16ea144dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc7bb33d-bcb8-4f39-8c31-8f12dd12e273",
                    "LayerId": "26986a06-2956-48b9-b537-a9194d9655f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "26986a06-2956-48b9-b537-a9194d9655f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ec092eb-98d5-4421-8a57-323cbc055896",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}