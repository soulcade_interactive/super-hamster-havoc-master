{
    "id": "90679b59-9a22-45c7-bc11-67868ead4335",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ps4_rstick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 5,
    "bbox_right": 94,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "42a9e575-68c3-41b2-bbda-481d40c67bd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90679b59-9a22-45c7-bc11-67868ead4335",
            "compositeImage": {
                "id": "09b805ef-d90a-4d1a-841a-c47fc9f0e972",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42a9e575-68c3-41b2-bbda-481d40c67bd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35e731f1-3e06-458d-9683-a21daa1c74fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42a9e575-68c3-41b2-bbda-481d40c67bd6",
                    "LayerId": "37f4adba-6dac-45f7-a73e-482f19be11ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "37f4adba-6dac-45f7-a73e-482f19be11ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90679b59-9a22-45c7-bc11-67868ead4335",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}