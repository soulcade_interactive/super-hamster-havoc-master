{
    "id": "088df5b9-1c4c-4797-8920-76062fd21cbf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_floor_alt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0354283b-6c94-4932-9700-0cc39032686b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "088df5b9-1c4c-4797-8920-76062fd21cbf",
            "compositeImage": {
                "id": "8be554ee-cfbd-472e-b90c-a4ac7df7bc1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0354283b-6c94-4932-9700-0cc39032686b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21926adb-f536-4e50-9e39-e359b116e8d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0354283b-6c94-4932-9700-0cc39032686b",
                    "LayerId": "043440a8-3c87-475b-b981-dcae360ad08e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "043440a8-3c87-475b-b981-dcae360ad08e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "088df5b9-1c4c-4797-8920-76062fd21cbf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}