{
    "id": "c666f71a-46f1-46dc-8910-171d1d87635c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_layer_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 779,
    "bbox_left": 0,
    "bbox_right": 2399,
    "bbox_top": 448,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d7228f8-791c-4e2b-8fe5-6002f45e2aa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c666f71a-46f1-46dc-8910-171d1d87635c",
            "compositeImage": {
                "id": "6b0c0c61-cd2a-43c4-9a5d-5af7f330887c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d7228f8-791c-4e2b-8fe5-6002f45e2aa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dfc3d8f-68a4-4163-b528-9552e4b6c208",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d7228f8-791c-4e2b-8fe5-6002f45e2aa2",
                    "LayerId": "11199835-74a1-42b2-845a-d40a4d1d1748"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 780,
    "layers": [
        {
            "id": "11199835-74a1-42b2-845a-d40a4d1d1748",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c666f71a-46f1-46dc-8910-171d1d87635c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2400,
    "xorig": 1200,
    "yorig": 390
}