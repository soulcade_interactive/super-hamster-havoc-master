{
    "id": "3095c203-d0e2-496c-94ca-b86a10ad4a22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nerd_0_head_u_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 18,
    "bbox_right": 45,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "809dff0d-c981-4eb2-be0e-be909b7f2ea1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3095c203-d0e2-496c-94ca-b86a10ad4a22",
            "compositeImage": {
                "id": "47775719-991f-4633-8df8-8814af18918e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "809dff0d-c981-4eb2-be0e-be909b7f2ea1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb31d0d9-63fa-48e3-92de-2b99c882fe42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "809dff0d-c981-4eb2-be0e-be909b7f2ea1",
                    "LayerId": "24c81f37-c289-49e7-9ff1-b0e509221a21"
                }
            ]
        },
        {
            "id": "6c2020e4-1361-430e-b899-436009833906",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3095c203-d0e2-496c-94ca-b86a10ad4a22",
            "compositeImage": {
                "id": "9ac1a14f-dfbd-4870-a89a-e942851287d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c2020e4-1361-430e-b899-436009833906",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "447dd4ef-c3db-4ae9-950b-7fffaf9fc99f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c2020e4-1361-430e-b899-436009833906",
                    "LayerId": "24c81f37-c289-49e7-9ff1-b0e509221a21"
                }
            ]
        },
        {
            "id": "dff3a7d4-3c7e-4016-9765-5336e7ad2db9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3095c203-d0e2-496c-94ca-b86a10ad4a22",
            "compositeImage": {
                "id": "9c2c32cd-82ce-4fff-b3fe-0c6eaec88b80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dff3a7d4-3c7e-4016-9765-5336e7ad2db9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "914f69b5-6884-4176-a647-25945c7c0a92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dff3a7d4-3c7e-4016-9765-5336e7ad2db9",
                    "LayerId": "24c81f37-c289-49e7-9ff1-b0e509221a21"
                }
            ]
        },
        {
            "id": "8a66983b-096a-40ea-8ce3-9e7e1844bbd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3095c203-d0e2-496c-94ca-b86a10ad4a22",
            "compositeImage": {
                "id": "f2fb3540-6146-4ca9-aec1-8890c3d8a555",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a66983b-096a-40ea-8ce3-9e7e1844bbd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cc741cb-2ef3-400a-902a-59f9df61bff5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a66983b-096a-40ea-8ce3-9e7e1844bbd2",
                    "LayerId": "24c81f37-c289-49e7-9ff1-b0e509221a21"
                }
            ]
        },
        {
            "id": "bfedbf5d-02d0-42fb-9979-9bcaeac6de38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3095c203-d0e2-496c-94ca-b86a10ad4a22",
            "compositeImage": {
                "id": "6a7eb5b0-72bb-4579-828c-2e39ea592c18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfedbf5d-02d0-42fb-9979-9bcaeac6de38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9581ae5-7650-4809-8707-f7af38d979bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfedbf5d-02d0-42fb-9979-9bcaeac6de38",
                    "LayerId": "24c81f37-c289-49e7-9ff1-b0e509221a21"
                }
            ]
        },
        {
            "id": "1030973d-7473-4c02-859c-0f951d4f3e4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3095c203-d0e2-496c-94ca-b86a10ad4a22",
            "compositeImage": {
                "id": "c9e42184-4df0-4b9d-b0e3-613660b812f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1030973d-7473-4c02-859c-0f951d4f3e4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d827640c-0714-4db7-9a21-2771fd3c3d40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1030973d-7473-4c02-859c-0f951d4f3e4d",
                    "LayerId": "24c81f37-c289-49e7-9ff1-b0e509221a21"
                }
            ]
        },
        {
            "id": "5ce38882-b5ee-4525-b529-1129285bf483",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3095c203-d0e2-496c-94ca-b86a10ad4a22",
            "compositeImage": {
                "id": "2f7bff79-880a-471d-afff-f66847507c23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ce38882-b5ee-4525-b529-1129285bf483",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f25c696-7f95-482a-b8a9-09fda8665186",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ce38882-b5ee-4525-b529-1129285bf483",
                    "LayerId": "24c81f37-c289-49e7-9ff1-b0e509221a21"
                }
            ]
        },
        {
            "id": "a6a0c1ab-b164-46d7-b00a-098b52e3f0e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3095c203-d0e2-496c-94ca-b86a10ad4a22",
            "compositeImage": {
                "id": "7608cd00-b96a-4e45-9438-0d1205eeb0d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6a0c1ab-b164-46d7-b00a-098b52e3f0e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d27037f-6e6a-427b-837c-723c4ed494eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6a0c1ab-b164-46d7-b00a-098b52e3f0e8",
                    "LayerId": "24c81f37-c289-49e7-9ff1-b0e509221a21"
                }
            ]
        },
        {
            "id": "2a9c1513-a646-4881-a231-1f75ba5ddbf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3095c203-d0e2-496c-94ca-b86a10ad4a22",
            "compositeImage": {
                "id": "74b6f0c5-f52b-4421-b886-3ed4b7896084",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a9c1513-a646-4881-a231-1f75ba5ddbf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b6d9337-ca6e-4d6f-8c11-aaca2b9fdaf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a9c1513-a646-4881-a231-1f75ba5ddbf7",
                    "LayerId": "24c81f37-c289-49e7-9ff1-b0e509221a21"
                }
            ]
        },
        {
            "id": "b8ee1aef-c722-4f0d-ae6e-bb4d45d5f5a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3095c203-d0e2-496c-94ca-b86a10ad4a22",
            "compositeImage": {
                "id": "c9c01232-d201-4445-abb6-215db8f67e51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8ee1aef-c722-4f0d-ae6e-bb4d45d5f5a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de89aef0-b008-4e00-88e3-3f129ead307f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8ee1aef-c722-4f0d-ae6e-bb4d45d5f5a5",
                    "LayerId": "24c81f37-c289-49e7-9ff1-b0e509221a21"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "24c81f37-c289-49e7-9ff1-b0e509221a21",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3095c203-d0e2-496c-94ca-b86a10ad4a22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}