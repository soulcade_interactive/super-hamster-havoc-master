{
    "id": "1575f09e-6a89-480d-805a-52705e447267",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ps4_square",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 9,
    "bbox_right": 90,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d697dd28-5740-4db5-81a2-760872cf374d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1575f09e-6a89-480d-805a-52705e447267",
            "compositeImage": {
                "id": "197d3dc5-2838-4c23-9e55-422a8fec89ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d697dd28-5740-4db5-81a2-760872cf374d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3bd2769-c276-408d-a62e-9e77ef947d4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d697dd28-5740-4db5-81a2-760872cf374d",
                    "LayerId": "6f29a579-3d6c-4fee-940c-c7e4bd71f50d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "6f29a579-3d6c-4fee-940c-c7e4bd71f50d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1575f09e-6a89-480d-805a-52705e447267",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}