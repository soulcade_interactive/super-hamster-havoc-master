{
    "id": "96121079-d0e6-41e7-9685-12ced6ed40c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_body_run_back_brown_and_gray",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99be352a-68a2-4e94-be70-c4869783e986",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96121079-d0e6-41e7-9685-12ced6ed40c9",
            "compositeImage": {
                "id": "6a992b3f-a6f5-4375-a4ee-f84068b15342",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99be352a-68a2-4e94-be70-c4869783e986",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4998b64-414a-475b-aa38-786565e70a18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99be352a-68a2-4e94-be70-c4869783e986",
                    "LayerId": "62c72802-b1ec-4052-8dcb-5525749d7024"
                }
            ]
        },
        {
            "id": "32d3b239-bea0-48ce-8a07-6be510ad5964",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96121079-d0e6-41e7-9685-12ced6ed40c9",
            "compositeImage": {
                "id": "06522db9-274d-4278-86a0-13b32452eacb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32d3b239-bea0-48ce-8a07-6be510ad5964",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "150531c8-c6ae-459f-af29-f08b0efe9c63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32d3b239-bea0-48ce-8a07-6be510ad5964",
                    "LayerId": "62c72802-b1ec-4052-8dcb-5525749d7024"
                }
            ]
        },
        {
            "id": "4493c326-992e-41a5-83f5-8c272eb18016",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96121079-d0e6-41e7-9685-12ced6ed40c9",
            "compositeImage": {
                "id": "3047d86a-11bd-4a5e-82d9-838f8657ac6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4493c326-992e-41a5-83f5-8c272eb18016",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08e6a674-c3c3-4f6b-a68e-5dad62b7bb38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4493c326-992e-41a5-83f5-8c272eb18016",
                    "LayerId": "62c72802-b1ec-4052-8dcb-5525749d7024"
                }
            ]
        },
        {
            "id": "be804a52-ba21-4723-bfc4-8f0686bc56cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96121079-d0e6-41e7-9685-12ced6ed40c9",
            "compositeImage": {
                "id": "8c63deef-a79b-4971-af18-8bd8ff9b7672",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be804a52-ba21-4723-bfc4-8f0686bc56cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b9520e6-f342-4679-8be0-ea2f05a205e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be804a52-ba21-4723-bfc4-8f0686bc56cf",
                    "LayerId": "62c72802-b1ec-4052-8dcb-5525749d7024"
                }
            ]
        },
        {
            "id": "ac96ac6c-cbf8-47c4-be53-8317b02e0e68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96121079-d0e6-41e7-9685-12ced6ed40c9",
            "compositeImage": {
                "id": "0e986721-9f87-45f8-907e-c4b7850d471d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac96ac6c-cbf8-47c4-be53-8317b02e0e68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed51aa9c-c740-4e90-8546-393692aff7c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac96ac6c-cbf8-47c4-be53-8317b02e0e68",
                    "LayerId": "62c72802-b1ec-4052-8dcb-5525749d7024"
                }
            ]
        },
        {
            "id": "4e2a3d24-69d7-4cb6-bd0c-e69347a01606",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96121079-d0e6-41e7-9685-12ced6ed40c9",
            "compositeImage": {
                "id": "69e1f7db-444d-41bb-9076-ab9ccd311565",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e2a3d24-69d7-4cb6-bd0c-e69347a01606",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b688b47c-9c87-4022-b495-7b828f1735a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e2a3d24-69d7-4cb6-bd0c-e69347a01606",
                    "LayerId": "62c72802-b1ec-4052-8dcb-5525749d7024"
                }
            ]
        },
        {
            "id": "ba90588c-590b-466f-8c20-eddb208fd84a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96121079-d0e6-41e7-9685-12ced6ed40c9",
            "compositeImage": {
                "id": "5ca7893b-3934-43ba-a14d-6ba53158d718",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba90588c-590b-466f-8c20-eddb208fd84a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f85bd1f-8a06-4924-83d6-326c4e3947b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba90588c-590b-466f-8c20-eddb208fd84a",
                    "LayerId": "62c72802-b1ec-4052-8dcb-5525749d7024"
                }
            ]
        },
        {
            "id": "a9a99da1-a957-403c-be8e-10dde4b9dbb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96121079-d0e6-41e7-9685-12ced6ed40c9",
            "compositeImage": {
                "id": "a04b94c3-5118-4219-917e-ed0796a9332f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9a99da1-a957-403c-be8e-10dde4b9dbb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb10d1ec-d443-4a88-ad30-9aac48c7e6e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9a99da1-a957-403c-be8e-10dde4b9dbb7",
                    "LayerId": "62c72802-b1ec-4052-8dcb-5525749d7024"
                }
            ]
        },
        {
            "id": "a2d2adb0-8275-4d67-b20d-ae0fab20e9a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96121079-d0e6-41e7-9685-12ced6ed40c9",
            "compositeImage": {
                "id": "ae53d245-03df-4b8a-8a41-f7c0d1d75c72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2d2adb0-8275-4d67-b20d-ae0fab20e9a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0872c8f-4cc7-4e24-a13b-dc7f0b67c684",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2d2adb0-8275-4d67-b20d-ae0fab20e9a8",
                    "LayerId": "62c72802-b1ec-4052-8dcb-5525749d7024"
                }
            ]
        },
        {
            "id": "bb462d6e-86f4-424f-a775-e41349e2017a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96121079-d0e6-41e7-9685-12ced6ed40c9",
            "compositeImage": {
                "id": "db4a9647-d252-45da-8941-3e6216928251",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb462d6e-86f4-424f-a775-e41349e2017a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ab82184-95ff-4392-abe1-ced1a337cdc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb462d6e-86f4-424f-a775-e41349e2017a",
                    "LayerId": "62c72802-b1ec-4052-8dcb-5525749d7024"
                }
            ]
        },
        {
            "id": "ceb7b940-ea85-4160-9722-749f166b5bc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96121079-d0e6-41e7-9685-12ced6ed40c9",
            "compositeImage": {
                "id": "1c56b513-9a19-4e71-8696-ff73ce6227c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ceb7b940-ea85-4160-9722-749f166b5bc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04fddb57-61dc-4af9-8419-6ffa582bbcb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ceb7b940-ea85-4160-9722-749f166b5bc2",
                    "LayerId": "62c72802-b1ec-4052-8dcb-5525749d7024"
                }
            ]
        },
        {
            "id": "73af8196-4775-4583-a94b-9c2f7074b712",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96121079-d0e6-41e7-9685-12ced6ed40c9",
            "compositeImage": {
                "id": "3c3aa967-0344-408e-9a2d-67538c5dae15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73af8196-4775-4583-a94b-9c2f7074b712",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e98e84fb-58ef-4127-9413-0e346c72fcc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73af8196-4775-4583-a94b-9c2f7074b712",
                    "LayerId": "62c72802-b1ec-4052-8dcb-5525749d7024"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "62c72802-b1ec-4052-8dcb-5525749d7024",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96121079-d0e6-41e7-9685-12ced6ed40c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 21
}