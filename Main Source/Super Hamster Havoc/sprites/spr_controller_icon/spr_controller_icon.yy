{
    "id": "cbf5b707-3326-4526-a2b0-d542b0d385f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_controller_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 6,
    "bbox_right": 57,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4ab916d-f516-43d3-b786-808b60e9a1ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbf5b707-3326-4526-a2b0-d542b0d385f6",
            "compositeImage": {
                "id": "5f2f57f3-ae66-4685-b518-216be77280b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4ab916d-f516-43d3-b786-808b60e9a1ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51bc3cde-e888-44c6-bd16-c55bfe11cb9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4ab916d-f516-43d3-b786-808b60e9a1ac",
                    "LayerId": "d762ae37-3b22-4d07-af55-0da123a6d9cc"
                }
            ]
        },
        {
            "id": "eaed54fa-fef6-41f7-8e9c-d1f46e1d78fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbf5b707-3326-4526-a2b0-d542b0d385f6",
            "compositeImage": {
                "id": "5ab060e9-46bf-49bb-993b-1dd8e27722cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaed54fa-fef6-41f7-8e9c-d1f46e1d78fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "281b5908-1807-4ad4-8fc9-802e08d152a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaed54fa-fef6-41f7-8e9c-d1f46e1d78fe",
                    "LayerId": "d762ae37-3b22-4d07-af55-0da123a6d9cc"
                }
            ]
        },
        {
            "id": "3a9662e2-0deb-4d8f-8172-610798e8a3c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbf5b707-3326-4526-a2b0-d542b0d385f6",
            "compositeImage": {
                "id": "3446a0a9-1db9-47c4-888e-7b222bc70678",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a9662e2-0deb-4d8f-8172-610798e8a3c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5b515ae-3a9f-4650-bb9f-0e12ca1af59e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a9662e2-0deb-4d8f-8172-610798e8a3c7",
                    "LayerId": "d762ae37-3b22-4d07-af55-0da123a6d9cc"
                }
            ]
        },
        {
            "id": "ef234aca-4190-4147-bac8-858f76fd0ebd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbf5b707-3326-4526-a2b0-d542b0d385f6",
            "compositeImage": {
                "id": "d4947ee9-e3db-47c6-b782-195f1fd33fc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef234aca-4190-4147-bac8-858f76fd0ebd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "745506d2-37ce-4179-a46b-623a34cbf527",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef234aca-4190-4147-bac8-858f76fd0ebd",
                    "LayerId": "d762ae37-3b22-4d07-af55-0da123a6d9cc"
                }
            ]
        },
        {
            "id": "3da4acb7-cba3-4ecb-9347-5becdf1d0133",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbf5b707-3326-4526-a2b0-d542b0d385f6",
            "compositeImage": {
                "id": "ba7bc512-1d72-4176-ba14-18af7ff300d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3da4acb7-cba3-4ecb-9347-5becdf1d0133",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b744c7a6-1dd0-489b-afeb-a7166045459e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3da4acb7-cba3-4ecb-9347-5becdf1d0133",
                    "LayerId": "d762ae37-3b22-4d07-af55-0da123a6d9cc"
                }
            ]
        },
        {
            "id": "c7b44615-3e1d-4efe-bf2f-bde4ad488c6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cbf5b707-3326-4526-a2b0-d542b0d385f6",
            "compositeImage": {
                "id": "724a3d0f-00d8-4626-928a-e019c8aa7080",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7b44615-3e1d-4efe-bf2f-bde4ad488c6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "539ecd64-08e6-414a-b9dc-9bd372b0cbda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7b44615-3e1d-4efe-bf2f-bde4ad488c6e",
                    "LayerId": "d762ae37-3b22-4d07-af55-0da123a6d9cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d762ae37-3b22-4d07-af55-0da123a6d9cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cbf5b707-3326-4526-a2b0-d542b0d385f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}