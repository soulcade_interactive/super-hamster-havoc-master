{
    "id": "4d597a31-6bf4-4c51-a813-5f50087e93eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buff_0_torso_d_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 7,
    "bbox_right": 56,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a25cc02a-ad7c-4d49-a68f-4f62238e433e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d597a31-6bf4-4c51-a813-5f50087e93eb",
            "compositeImage": {
                "id": "bdf983e9-fce3-464a-a96b-d441fc4fb5f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a25cc02a-ad7c-4d49-a68f-4f62238e433e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27f17b68-2dc8-4271-b033-a663e15da5cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a25cc02a-ad7c-4d49-a68f-4f62238e433e",
                    "LayerId": "0f42f9e5-a2b3-42aa-8c92-e2125e01413c"
                }
            ]
        },
        {
            "id": "377d9676-475a-4e22-ba31-1ec073052c92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d597a31-6bf4-4c51-a813-5f50087e93eb",
            "compositeImage": {
                "id": "b756803b-e0c5-43b0-85fd-c891bef80774",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "377d9676-475a-4e22-ba31-1ec073052c92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf644d62-289b-41ee-9669-8f236cda801d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "377d9676-475a-4e22-ba31-1ec073052c92",
                    "LayerId": "0f42f9e5-a2b3-42aa-8c92-e2125e01413c"
                }
            ]
        },
        {
            "id": "820387af-5fac-400e-88b7-b1ca170e30ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d597a31-6bf4-4c51-a813-5f50087e93eb",
            "compositeImage": {
                "id": "d3823872-556d-496e-85fc-9890e1685a2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "820387af-5fac-400e-88b7-b1ca170e30ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fc73c07-4699-49e9-b505-7ee8f029136f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "820387af-5fac-400e-88b7-b1ca170e30ff",
                    "LayerId": "0f42f9e5-a2b3-42aa-8c92-e2125e01413c"
                }
            ]
        },
        {
            "id": "09f053a5-9c40-45ee-b548-a5bb7432d4bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d597a31-6bf4-4c51-a813-5f50087e93eb",
            "compositeImage": {
                "id": "cb01dba0-7c36-4a6d-a321-40ee9979ad11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09f053a5-9c40-45ee-b548-a5bb7432d4bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e9161a6-63d6-4346-ab89-97016fa79e14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09f053a5-9c40-45ee-b548-a5bb7432d4bc",
                    "LayerId": "0f42f9e5-a2b3-42aa-8c92-e2125e01413c"
                }
            ]
        },
        {
            "id": "00c66321-d40f-4a64-88b7-779ca99193ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d597a31-6bf4-4c51-a813-5f50087e93eb",
            "compositeImage": {
                "id": "238b9293-a44d-4513-89a0-42a348f188cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00c66321-d40f-4a64-88b7-779ca99193ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f4bcf21-e06f-402f-be5d-3748691c4d4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00c66321-d40f-4a64-88b7-779ca99193ef",
                    "LayerId": "0f42f9e5-a2b3-42aa-8c92-e2125e01413c"
                }
            ]
        },
        {
            "id": "f8a0097e-8494-42a2-9076-50f3dabda259",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d597a31-6bf4-4c51-a813-5f50087e93eb",
            "compositeImage": {
                "id": "8c9dea22-b8f3-468d-922b-e252c283efca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8a0097e-8494-42a2-9076-50f3dabda259",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18f91e0c-f86b-40cc-98e1-5ff841e8928e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8a0097e-8494-42a2-9076-50f3dabda259",
                    "LayerId": "0f42f9e5-a2b3-42aa-8c92-e2125e01413c"
                }
            ]
        },
        {
            "id": "ebed27b6-90a3-4f43-8cd5-853c6284b72b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d597a31-6bf4-4c51-a813-5f50087e93eb",
            "compositeImage": {
                "id": "62f9ddb0-81aa-4a0d-a8c4-3cdd3328e7c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebed27b6-90a3-4f43-8cd5-853c6284b72b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "707e8f53-40da-4edf-8ac9-62a797489341",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebed27b6-90a3-4f43-8cd5-853c6284b72b",
                    "LayerId": "0f42f9e5-a2b3-42aa-8c92-e2125e01413c"
                }
            ]
        },
        {
            "id": "b74b7879-2abc-4793-9833-3638fd919afc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d597a31-6bf4-4c51-a813-5f50087e93eb",
            "compositeImage": {
                "id": "f81413ee-5d62-4981-a9d7-fcefe492047c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b74b7879-2abc-4793-9833-3638fd919afc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57f821bb-5443-4f93-8d4b-d6838b0ad559",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b74b7879-2abc-4793-9833-3638fd919afc",
                    "LayerId": "0f42f9e5-a2b3-42aa-8c92-e2125e01413c"
                }
            ]
        },
        {
            "id": "d7a59bce-89f0-49f0-85a9-31ddd77bd31a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d597a31-6bf4-4c51-a813-5f50087e93eb",
            "compositeImage": {
                "id": "9b370343-f3f2-4c06-bdbb-c135f59e64f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7a59bce-89f0-49f0-85a9-31ddd77bd31a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d70be2e3-5e52-4727-982c-5490b76f3b28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7a59bce-89f0-49f0-85a9-31ddd77bd31a",
                    "LayerId": "0f42f9e5-a2b3-42aa-8c92-e2125e01413c"
                }
            ]
        },
        {
            "id": "b0cfdf9b-5357-4e6e-b1ee-be93e330c2fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d597a31-6bf4-4c51-a813-5f50087e93eb",
            "compositeImage": {
                "id": "58cc3b16-340c-439a-ba01-15ee0253c327",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0cfdf9b-5357-4e6e-b1ee-be93e330c2fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85f08bf4-bcae-4835-a204-666c7aa6ff8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0cfdf9b-5357-4e6e-b1ee-be93e330c2fb",
                    "LayerId": "0f42f9e5-a2b3-42aa-8c92-e2125e01413c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0f42f9e5-a2b3-42aa-8c92-e2125e01413c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d597a31-6bf4-4c51-a813-5f50087e93eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}