{
    "id": "3bd1a674-6f0b-43d5-b721-2f509781c176",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_body_run_back_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d7bac6e-3a10-429b-8374-56931f2aeaf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bd1a674-6f0b-43d5-b721-2f509781c176",
            "compositeImage": {
                "id": "1bf41c58-23da-438f-88dd-e6f76dc414b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d7bac6e-3a10-429b-8374-56931f2aeaf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "debdd4fb-2270-48f4-a755-0e4423814fef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d7bac6e-3a10-429b-8374-56931f2aeaf3",
                    "LayerId": "6d089867-4b08-4638-b7c0-c86b59b73c4e"
                }
            ]
        },
        {
            "id": "9707fd7d-1871-46bf-85ef-e5ca549b3b80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bd1a674-6f0b-43d5-b721-2f509781c176",
            "compositeImage": {
                "id": "cef109c7-8098-4014-ab6e-7e99e33234eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9707fd7d-1871-46bf-85ef-e5ca549b3b80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "577b2775-1cdc-46ff-922c-bdef66c6c6e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9707fd7d-1871-46bf-85ef-e5ca549b3b80",
                    "LayerId": "6d089867-4b08-4638-b7c0-c86b59b73c4e"
                }
            ]
        },
        {
            "id": "6d63668d-9f19-4f0e-ad18-26f8b55966b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bd1a674-6f0b-43d5-b721-2f509781c176",
            "compositeImage": {
                "id": "a01ad155-4a82-4c40-bd62-19f208f1b5bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d63668d-9f19-4f0e-ad18-26f8b55966b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f422fc1-6db2-476d-b23d-99496822c800",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d63668d-9f19-4f0e-ad18-26f8b55966b3",
                    "LayerId": "6d089867-4b08-4638-b7c0-c86b59b73c4e"
                }
            ]
        },
        {
            "id": "33cb9833-ede8-4263-a0ce-d60f79da1cd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bd1a674-6f0b-43d5-b721-2f509781c176",
            "compositeImage": {
                "id": "e1425294-225c-449f-9a43-4fdf177c906d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33cb9833-ede8-4263-a0ce-d60f79da1cd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3455cc38-e25e-4c35-8c98-0d5d38365ee5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33cb9833-ede8-4263-a0ce-d60f79da1cd9",
                    "LayerId": "6d089867-4b08-4638-b7c0-c86b59b73c4e"
                }
            ]
        },
        {
            "id": "17cd4677-8548-4550-969d-dd3a8d078de9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bd1a674-6f0b-43d5-b721-2f509781c176",
            "compositeImage": {
                "id": "f1a6701f-ae38-40a2-b592-cc00cc1a94a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17cd4677-8548-4550-969d-dd3a8d078de9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2450812e-cfae-49b1-b5cd-c9d8d974754c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17cd4677-8548-4550-969d-dd3a8d078de9",
                    "LayerId": "6d089867-4b08-4638-b7c0-c86b59b73c4e"
                }
            ]
        },
        {
            "id": "4b45c515-3313-4934-8c95-9f6e301dde76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bd1a674-6f0b-43d5-b721-2f509781c176",
            "compositeImage": {
                "id": "ff2f6cbb-6c7a-4a48-923a-fefc95dd770f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b45c515-3313-4934-8c95-9f6e301dde76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77f81558-bc48-418c-b4aa-4b7816b27cf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b45c515-3313-4934-8c95-9f6e301dde76",
                    "LayerId": "6d089867-4b08-4638-b7c0-c86b59b73c4e"
                }
            ]
        },
        {
            "id": "dec996d9-63de-49a3-a777-74d40061cf13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bd1a674-6f0b-43d5-b721-2f509781c176",
            "compositeImage": {
                "id": "d83fae61-820c-4dda-ade3-bb949ff4f304",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dec996d9-63de-49a3-a777-74d40061cf13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7fd7142-4948-4a69-a053-2fcca5841e65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dec996d9-63de-49a3-a777-74d40061cf13",
                    "LayerId": "6d089867-4b08-4638-b7c0-c86b59b73c4e"
                }
            ]
        },
        {
            "id": "7291c83c-6186-4b2b-8199-136190f5ed78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bd1a674-6f0b-43d5-b721-2f509781c176",
            "compositeImage": {
                "id": "96c306d4-c339-498a-85b5-5f62adf45e4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7291c83c-6186-4b2b-8199-136190f5ed78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85e19b45-49a2-4e50-8d35-9968540d2082",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7291c83c-6186-4b2b-8199-136190f5ed78",
                    "LayerId": "6d089867-4b08-4638-b7c0-c86b59b73c4e"
                }
            ]
        },
        {
            "id": "8b5854ec-e428-4330-83e2-ea8a7dd4a09c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bd1a674-6f0b-43d5-b721-2f509781c176",
            "compositeImage": {
                "id": "8f4da7ed-dbec-45bc-838a-9c3f65b00ef2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b5854ec-e428-4330-83e2-ea8a7dd4a09c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e558b5d-3247-4a70-9355-6366c7e52e2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b5854ec-e428-4330-83e2-ea8a7dd4a09c",
                    "LayerId": "6d089867-4b08-4638-b7c0-c86b59b73c4e"
                }
            ]
        },
        {
            "id": "92c86a0d-5eed-4288-b434-99dc93c02698",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bd1a674-6f0b-43d5-b721-2f509781c176",
            "compositeImage": {
                "id": "1abefbb3-5f3f-4c90-8647-90078927a5e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92c86a0d-5eed-4288-b434-99dc93c02698",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1028c4ea-c223-416d-b2d8-2995b8ef1ced",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92c86a0d-5eed-4288-b434-99dc93c02698",
                    "LayerId": "6d089867-4b08-4638-b7c0-c86b59b73c4e"
                }
            ]
        },
        {
            "id": "12ffe55a-cd1c-48fb-9462-a1075cb4a653",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bd1a674-6f0b-43d5-b721-2f509781c176",
            "compositeImage": {
                "id": "005bc5e0-78df-4e2d-8867-bd711000f74f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12ffe55a-cd1c-48fb-9462-a1075cb4a653",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "311f0c07-e682-447e-ae04-0deb8ef666bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12ffe55a-cd1c-48fb-9462-a1075cb4a653",
                    "LayerId": "6d089867-4b08-4638-b7c0-c86b59b73c4e"
                }
            ]
        },
        {
            "id": "6841adbd-4bc9-4aac-8292-65d7d60c1e50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3bd1a674-6f0b-43d5-b721-2f509781c176",
            "compositeImage": {
                "id": "84b36f59-989f-467f-afbf-41e9cf7a8c16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6841adbd-4bc9-4aac-8292-65d7d60c1e50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18d942b6-05ce-43ac-8ced-ba55fc60cf91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6841adbd-4bc9-4aac-8292-65d7d60c1e50",
                    "LayerId": "6d089867-4b08-4638-b7c0-c86b59b73c4e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "6d089867-4b08-4638-b7c0-c86b59b73c4e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3bd1a674-6f0b-43d5-b721-2f509781c176",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 21
}