{
    "id": "ff688443-1d3c-48eb-8165-bcfccded85ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_explosion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4b7dec2-dee3-4b99-8697-8662b419f6eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff688443-1d3c-48eb-8165-bcfccded85ba",
            "compositeImage": {
                "id": "6cd151fe-eb91-435e-9411-79cca15e9613",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4b7dec2-dee3-4b99-8697-8662b419f6eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "072d3a50-406a-41f1-94fb-e482cdc98765",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4b7dec2-dee3-4b99-8697-8662b419f6eb",
                    "LayerId": "159ed336-60d4-4890-a72d-ea990252d17e"
                }
            ]
        },
        {
            "id": "12f7c772-7cf8-49a6-ab21-7247c33ab7ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff688443-1d3c-48eb-8165-bcfccded85ba",
            "compositeImage": {
                "id": "320e718e-a5cc-4cb2-a058-3d631cad6e51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12f7c772-7cf8-49a6-ab21-7247c33ab7ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5408f5b9-2397-42e4-97c1-465aac89a6f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12f7c772-7cf8-49a6-ab21-7247c33ab7ab",
                    "LayerId": "159ed336-60d4-4890-a72d-ea990252d17e"
                }
            ]
        },
        {
            "id": "693d728e-7f40-4628-baf4-473e641d9803",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff688443-1d3c-48eb-8165-bcfccded85ba",
            "compositeImage": {
                "id": "0347532c-894e-4849-87a7-b0b538294517",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "693d728e-7f40-4628-baf4-473e641d9803",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d0fa6d2-2307-4a6b-be62-3e24cc87d7bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "693d728e-7f40-4628-baf4-473e641d9803",
                    "LayerId": "159ed336-60d4-4890-a72d-ea990252d17e"
                }
            ]
        },
        {
            "id": "a3481eb5-c74e-43a9-8baa-f081af700b15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff688443-1d3c-48eb-8165-bcfccded85ba",
            "compositeImage": {
                "id": "a037a8af-cd50-49a6-81e4-d41ff3bcba0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3481eb5-c74e-43a9-8baa-f081af700b15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "925e57eb-0429-48a4-a918-4992b6817bde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3481eb5-c74e-43a9-8baa-f081af700b15",
                    "LayerId": "159ed336-60d4-4890-a72d-ea990252d17e"
                }
            ]
        },
        {
            "id": "6c9f4b02-9168-4fc0-93cc-644f3a93ed97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff688443-1d3c-48eb-8165-bcfccded85ba",
            "compositeImage": {
                "id": "9803b8aa-65ca-4229-a57a-d9f27f667636",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c9f4b02-9168-4fc0-93cc-644f3a93ed97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55939acd-bd3d-4f94-aa9e-8a2cb6fea727",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c9f4b02-9168-4fc0-93cc-644f3a93ed97",
                    "LayerId": "159ed336-60d4-4890-a72d-ea990252d17e"
                }
            ]
        },
        {
            "id": "34b50a58-23b7-4b81-b9f9-1583e2b7f7a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff688443-1d3c-48eb-8165-bcfccded85ba",
            "compositeImage": {
                "id": "fe2712a5-850b-42e1-b017-24ea4eecd6db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34b50a58-23b7-4b81-b9f9-1583e2b7f7a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f8dc778-b569-4b16-82ca-49959ee46269",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34b50a58-23b7-4b81-b9f9-1583e2b7f7a9",
                    "LayerId": "159ed336-60d4-4890-a72d-ea990252d17e"
                }
            ]
        },
        {
            "id": "f8c86dd8-2859-4d73-93bf-1bf3a256366f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff688443-1d3c-48eb-8165-bcfccded85ba",
            "compositeImage": {
                "id": "d7bbed31-d9d6-44fb-bb78-83a92ec6e1d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8c86dd8-2859-4d73-93bf-1bf3a256366f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c46cf707-ff92-447a-b315-79056cfca0d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8c86dd8-2859-4d73-93bf-1bf3a256366f",
                    "LayerId": "159ed336-60d4-4890-a72d-ea990252d17e"
                }
            ]
        },
        {
            "id": "3f443f84-8d4c-49b5-a492-9782e4bf5ca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff688443-1d3c-48eb-8165-bcfccded85ba",
            "compositeImage": {
                "id": "a66b9856-c952-43ba-b801-ab2b7da50a0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f443f84-8d4c-49b5-a492-9782e4bf5ca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7485d974-2248-4dbd-879c-410d573026a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f443f84-8d4c-49b5-a492-9782e4bf5ca9",
                    "LayerId": "159ed336-60d4-4890-a72d-ea990252d17e"
                }
            ]
        },
        {
            "id": "e7c26320-bf3d-4f1e-804b-933e8ebf9424",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff688443-1d3c-48eb-8165-bcfccded85ba",
            "compositeImage": {
                "id": "5a86cc9b-97c9-4a4c-b02d-40d0eed4503c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7c26320-bf3d-4f1e-804b-933e8ebf9424",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87fc47a1-8291-4093-b6f7-36daa88c6141",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7c26320-bf3d-4f1e-804b-933e8ebf9424",
                    "LayerId": "159ed336-60d4-4890-a72d-ea990252d17e"
                }
            ]
        },
        {
            "id": "1cb7173b-2a38-4287-8856-c17f296494c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff688443-1d3c-48eb-8165-bcfccded85ba",
            "compositeImage": {
                "id": "60fc1a0b-dec3-4813-b33f-2696e580c7b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cb7173b-2a38-4287-8856-c17f296494c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15c50b6a-3b17-4418-937f-5330b1d6f7f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cb7173b-2a38-4287-8856-c17f296494c4",
                    "LayerId": "159ed336-60d4-4890-a72d-ea990252d17e"
                }
            ]
        },
        {
            "id": "0f5b2747-a43e-4559-a36b-7b04f796e11c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff688443-1d3c-48eb-8165-bcfccded85ba",
            "compositeImage": {
                "id": "33667679-65e8-465e-ad83-d37a5b34d74d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f5b2747-a43e-4559-a36b-7b04f796e11c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dd678b8-2fbf-49ab-82bd-7050d850782e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f5b2747-a43e-4559-a36b-7b04f796e11c",
                    "LayerId": "159ed336-60d4-4890-a72d-ea990252d17e"
                }
            ]
        },
        {
            "id": "6006bf9a-3500-46d0-9763-a10aadf2f997",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff688443-1d3c-48eb-8165-bcfccded85ba",
            "compositeImage": {
                "id": "9b229700-d845-427f-890c-a8708b0649b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6006bf9a-3500-46d0-9763-a10aadf2f997",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bdc2ef9-2b5f-48d1-8629-23ac7f0ec4dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6006bf9a-3500-46d0-9763-a10aadf2f997",
                    "LayerId": "159ed336-60d4-4890-a72d-ea990252d17e"
                }
            ]
        },
        {
            "id": "03ca8066-c440-4f66-b6e7-901ad77edf2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff688443-1d3c-48eb-8165-bcfccded85ba",
            "compositeImage": {
                "id": "50e9c45a-6f1d-479a-9552-6855ff623e05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03ca8066-c440-4f66-b6e7-901ad77edf2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23bd3b47-5758-4279-ae27-2bdbd7ff80b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03ca8066-c440-4f66-b6e7-901ad77edf2b",
                    "LayerId": "159ed336-60d4-4890-a72d-ea990252d17e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "159ed336-60d4-4890-a72d-ea990252d17e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff688443-1d3c-48eb-8165-bcfccded85ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}