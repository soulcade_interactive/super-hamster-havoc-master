{
    "id": "b69a7a00-a435-403f-95c1-6a2c37d942c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_switch_lstick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 5,
    "bbox_right": 94,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb28512c-d4ce-43e5-8a7c-80e4e160f3ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b69a7a00-a435-403f-95c1-6a2c37d942c9",
            "compositeImage": {
                "id": "2c5273dd-e088-41ff-88b8-6bd053e08071",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb28512c-d4ce-43e5-8a7c-80e4e160f3ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4913841f-df38-4265-9183-1c633a98a415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb28512c-d4ce-43e5-8a7c-80e4e160f3ca",
                    "LayerId": "8a30bcb4-1a31-4343-ac81-6c812c1737d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "8a30bcb4-1a31-4343-ac81-6c812c1737d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b69a7a00-a435-403f-95c1-6a2c37d942c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}