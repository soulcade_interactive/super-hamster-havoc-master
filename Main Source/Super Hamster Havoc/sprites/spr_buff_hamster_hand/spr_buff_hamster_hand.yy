{
    "id": "64664f5c-4c3c-4fec-99fa-71fa802c72a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buff_hamster_hand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 1,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72a19bc1-01d2-497e-a160-4234fb0e5225",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64664f5c-4c3c-4fec-99fa-71fa802c72a9",
            "compositeImage": {
                "id": "6d5eff2d-19f7-4067-a0ed-3a9e3ad98d75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72a19bc1-01d2-497e-a160-4234fb0e5225",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12d94539-5ba9-426e-8eaa-65a2106c4a6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72a19bc1-01d2-497e-a160-4234fb0e5225",
                    "LayerId": "9df678c6-6fa8-483e-a296-179a74d34763"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "9df678c6-6fa8-483e-a296-179a74d34763",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64664f5c-4c3c-4fec-99fa-71fa802c72a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 3,
    "yorig": 3
}