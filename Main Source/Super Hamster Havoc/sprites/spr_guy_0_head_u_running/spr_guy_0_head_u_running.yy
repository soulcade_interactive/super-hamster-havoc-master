{
    "id": "1e8662c9-50cc-4404-a71f-702701ab6326",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_0_head_u_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 14,
    "bbox_right": 49,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "335296ba-e472-4f3b-8909-fb57f90dfeef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e8662c9-50cc-4404-a71f-702701ab6326",
            "compositeImage": {
                "id": "08744fdb-9a80-417a-9f70-1a41d780a6c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "335296ba-e472-4f3b-8909-fb57f90dfeef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daafe3a7-aa49-44d9-bdf3-92671f39a86d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "335296ba-e472-4f3b-8909-fb57f90dfeef",
                    "LayerId": "c4d371bb-a950-40de-ae47-9333d7cf18ef"
                }
            ]
        },
        {
            "id": "c3aad931-9664-4611-8eb5-eb453c19758a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e8662c9-50cc-4404-a71f-702701ab6326",
            "compositeImage": {
                "id": "dac02152-110e-4c3d-9b89-11c125f06615",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3aad931-9664-4611-8eb5-eb453c19758a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a64bcce-5d8b-4455-aa9d-4e3082ae6da5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3aad931-9664-4611-8eb5-eb453c19758a",
                    "LayerId": "c4d371bb-a950-40de-ae47-9333d7cf18ef"
                }
            ]
        },
        {
            "id": "31e679f6-e291-4a36-8d87-6a3cf8470d3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e8662c9-50cc-4404-a71f-702701ab6326",
            "compositeImage": {
                "id": "b0b67bab-7b4f-4d06-b61b-178315eae55c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31e679f6-e291-4a36-8d87-6a3cf8470d3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1de2f977-6c56-4e0e-b5b7-106052698dd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31e679f6-e291-4a36-8d87-6a3cf8470d3a",
                    "LayerId": "c4d371bb-a950-40de-ae47-9333d7cf18ef"
                }
            ]
        },
        {
            "id": "ec760b6f-7071-407d-af47-a95b5b25284f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e8662c9-50cc-4404-a71f-702701ab6326",
            "compositeImage": {
                "id": "4c376ee8-5cc7-4d2b-ba4e-1ac0fbdf58a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec760b6f-7071-407d-af47-a95b5b25284f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ae976c9-e3a8-4ed2-9bd2-68dbe9164b42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec760b6f-7071-407d-af47-a95b5b25284f",
                    "LayerId": "c4d371bb-a950-40de-ae47-9333d7cf18ef"
                }
            ]
        },
        {
            "id": "b3661006-2d9d-4e87-8caa-b4e52e90eae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e8662c9-50cc-4404-a71f-702701ab6326",
            "compositeImage": {
                "id": "bfd9d66a-70d4-40dc-aa94-2f48e8b43aed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3661006-2d9d-4e87-8caa-b4e52e90eae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3d34413-30de-4e50-aee2-a64261c3f093",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3661006-2d9d-4e87-8caa-b4e52e90eae3",
                    "LayerId": "c4d371bb-a950-40de-ae47-9333d7cf18ef"
                }
            ]
        },
        {
            "id": "f9da6fa2-2a13-4869-9200-82a9c2ddfbeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e8662c9-50cc-4404-a71f-702701ab6326",
            "compositeImage": {
                "id": "2c00f49a-58dc-4c93-9c1b-477e6e0dc068",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9da6fa2-2a13-4869-9200-82a9c2ddfbeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bb31851-bc87-43f0-acd1-97afee9a31ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9da6fa2-2a13-4869-9200-82a9c2ddfbeb",
                    "LayerId": "c4d371bb-a950-40de-ae47-9333d7cf18ef"
                }
            ]
        },
        {
            "id": "a90c4ced-4e12-4b7f-a99e-bfa941db5101",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e8662c9-50cc-4404-a71f-702701ab6326",
            "compositeImage": {
                "id": "0cb1527e-f275-40e1-abf9-1935285e72e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a90c4ced-4e12-4b7f-a99e-bfa941db5101",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0627bdf1-2b89-42b0-86be-90e933c1523b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a90c4ced-4e12-4b7f-a99e-bfa941db5101",
                    "LayerId": "c4d371bb-a950-40de-ae47-9333d7cf18ef"
                }
            ]
        },
        {
            "id": "d9d5e548-3de0-40fe-a905-ab4b3f50ff41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e8662c9-50cc-4404-a71f-702701ab6326",
            "compositeImage": {
                "id": "5dc327b8-052f-4c2b-ac55-9913c4ed79c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9d5e548-3de0-40fe-a905-ab4b3f50ff41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05cb35ad-dbf0-4d15-a08a-7e2a84d37acb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9d5e548-3de0-40fe-a905-ab4b3f50ff41",
                    "LayerId": "c4d371bb-a950-40de-ae47-9333d7cf18ef"
                }
            ]
        },
        {
            "id": "3c6e7b18-ee66-46fc-8adb-8853914dcd39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e8662c9-50cc-4404-a71f-702701ab6326",
            "compositeImage": {
                "id": "56684f32-5dbb-4724-b4cf-a321326efae1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c6e7b18-ee66-46fc-8adb-8853914dcd39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "266c3ea2-eedc-4ea2-bc80-73a71128f141",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c6e7b18-ee66-46fc-8adb-8853914dcd39",
                    "LayerId": "c4d371bb-a950-40de-ae47-9333d7cf18ef"
                }
            ]
        },
        {
            "id": "0457105c-df91-4b3d-b98e-be08485a15a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e8662c9-50cc-4404-a71f-702701ab6326",
            "compositeImage": {
                "id": "54300bd4-20db-483d-857a-8f1d325a875b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0457105c-df91-4b3d-b98e-be08485a15a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fcfa4ae-3c8f-4df2-82d1-a26b36feb147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0457105c-df91-4b3d-b98e-be08485a15a3",
                    "LayerId": "c4d371bb-a950-40de-ae47-9333d7cf18ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c4d371bb-a950-40de-ae47-9333d7cf18ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e8662c9-50cc-4404-a71f-702701ab6326",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}