{
    "id": "6ea751c0-f6f0-4fd1-bc73-4796defb7db9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flower5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e4a860f-9224-406f-bf07-d5eb444b0fe3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ea751c0-f6f0-4fd1-bc73-4796defb7db9",
            "compositeImage": {
                "id": "cdcffee2-33c1-4e7a-b250-86a72459f529",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e4a860f-9224-406f-bf07-d5eb444b0fe3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4765355f-ec76-48f9-89b3-4c4a646ec917",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e4a860f-9224-406f-bf07-d5eb444b0fe3",
                    "LayerId": "0bfa62ad-3eec-4b20-ab29-8409e27e321c"
                }
            ]
        },
        {
            "id": "cbeda39b-4005-4ddc-9e1b-ba54cfbadb84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ea751c0-f6f0-4fd1-bc73-4796defb7db9",
            "compositeImage": {
                "id": "c3beb0a0-34b2-4458-b1e8-dd41227a449b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbeda39b-4005-4ddc-9e1b-ba54cfbadb84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5d5280e-b8cd-48ea-b529-31d5211033b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbeda39b-4005-4ddc-9e1b-ba54cfbadb84",
                    "LayerId": "0bfa62ad-3eec-4b20-ab29-8409e27e321c"
                }
            ]
        },
        {
            "id": "4d902ded-c470-43b5-8707-4905a46fc901",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ea751c0-f6f0-4fd1-bc73-4796defb7db9",
            "compositeImage": {
                "id": "4bed3d2e-a1aa-429f-9fa6-2fa0c5756c32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d902ded-c470-43b5-8707-4905a46fc901",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a928da9-f053-433d-ae34-80ae19c841b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d902ded-c470-43b5-8707-4905a46fc901",
                    "LayerId": "0bfa62ad-3eec-4b20-ab29-8409e27e321c"
                }
            ]
        },
        {
            "id": "cbb76226-2ca7-4eed-b44e-d06273ac093f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ea751c0-f6f0-4fd1-bc73-4796defb7db9",
            "compositeImage": {
                "id": "8c620b78-4bb4-4ffc-b673-b4271f029cc0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbb76226-2ca7-4eed-b44e-d06273ac093f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a01650f-eb7d-4007-9b94-70d0d6c7c3b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbb76226-2ca7-4eed-b44e-d06273ac093f",
                    "LayerId": "0bfa62ad-3eec-4b20-ab29-8409e27e321c"
                }
            ]
        },
        {
            "id": "eb2bbb97-897b-45a7-831e-15618dc6a718",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ea751c0-f6f0-4fd1-bc73-4796defb7db9",
            "compositeImage": {
                "id": "cbbea149-9aa0-4609-a054-0a0615844406",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb2bbb97-897b-45a7-831e-15618dc6a718",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d642553-94d7-44e4-8e55-398b70946ae9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb2bbb97-897b-45a7-831e-15618dc6a718",
                    "LayerId": "0bfa62ad-3eec-4b20-ab29-8409e27e321c"
                }
            ]
        },
        {
            "id": "c6c46c7a-3e4d-46fe-a299-69deefac674d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ea751c0-f6f0-4fd1-bc73-4796defb7db9",
            "compositeImage": {
                "id": "02a8a2b4-307b-487f-a15b-a861b4338109",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6c46c7a-3e4d-46fe-a299-69deefac674d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ff7f53a-3b0f-422c-8829-de2de13c4dc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6c46c7a-3e4d-46fe-a299-69deefac674d",
                    "LayerId": "0bfa62ad-3eec-4b20-ab29-8409e27e321c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "0bfa62ad-3eec-4b20-ab29-8409e27e321c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ea751c0-f6f0-4fd1-bc73-4796defb7db9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}