{
    "id": "8619ce4a-db6c-40e1-8be0-87976847e2b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_wall_tilesheet",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 139,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a149157f-62a5-4d70-bd59-6568b73a27c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8619ce4a-db6c-40e1-8be0-87976847e2b9",
            "compositeImage": {
                "id": "22b9b81d-94f3-4d34-a2aa-8c922941f94f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a149157f-62a5-4d70-bd59-6568b73a27c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8730a9f-f436-4310-a18c-ee07e8d4e859",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a149157f-62a5-4d70-bd59-6568b73a27c2",
                    "LayerId": "254f9ede-3f50-49d2-a09f-4593346c3f76"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "254f9ede-3f50-49d2-a09f-4593346c3f76",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8619ce4a-db6c-40e1-8be0-87976847e2b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}