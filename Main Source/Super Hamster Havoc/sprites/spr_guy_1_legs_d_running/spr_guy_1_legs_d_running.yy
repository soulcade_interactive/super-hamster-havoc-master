{
    "id": "5938cad5-e878-437a-b1ae-93e0ed602c2e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_1_legs_d_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 25,
    "bbox_right": 38,
    "bbox_top": 56,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b60224c-e55b-4f7d-804d-6b39dadeef0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5938cad5-e878-437a-b1ae-93e0ed602c2e",
            "compositeImage": {
                "id": "2996067b-c273-45bc-b1d2-a3b16c8e7d2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b60224c-e55b-4f7d-804d-6b39dadeef0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5e49a9f-41d0-4b39-b6be-ce0f09e27faa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b60224c-e55b-4f7d-804d-6b39dadeef0d",
                    "LayerId": "69e0a9bf-c31c-4dcc-8a6b-1d49f91bdf36"
                }
            ]
        },
        {
            "id": "476d7ae1-7a04-4a37-b45d-740f2e28bf36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5938cad5-e878-437a-b1ae-93e0ed602c2e",
            "compositeImage": {
                "id": "88dccb59-d16a-4cbf-bb9d-fbe8e2563eea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "476d7ae1-7a04-4a37-b45d-740f2e28bf36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e634ac43-8c8c-4b58-8838-88e260991313",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "476d7ae1-7a04-4a37-b45d-740f2e28bf36",
                    "LayerId": "69e0a9bf-c31c-4dcc-8a6b-1d49f91bdf36"
                }
            ]
        },
        {
            "id": "10bb9ef5-612d-4239-9713-024c33b13abd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5938cad5-e878-437a-b1ae-93e0ed602c2e",
            "compositeImage": {
                "id": "252fbb9b-e25e-4610-9769-22b8cba1e57a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10bb9ef5-612d-4239-9713-024c33b13abd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d22c61e-8d2d-42c4-a2f3-47d8ff73b9c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10bb9ef5-612d-4239-9713-024c33b13abd",
                    "LayerId": "69e0a9bf-c31c-4dcc-8a6b-1d49f91bdf36"
                }
            ]
        },
        {
            "id": "c5df7461-53d0-42ca-b83c-b7179c1c24c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5938cad5-e878-437a-b1ae-93e0ed602c2e",
            "compositeImage": {
                "id": "d9d56c94-4b92-42a8-9792-0a6345a2e9c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5df7461-53d0-42ca-b83c-b7179c1c24c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67233167-29be-4dfb-9d67-1ff15797103d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5df7461-53d0-42ca-b83c-b7179c1c24c1",
                    "LayerId": "69e0a9bf-c31c-4dcc-8a6b-1d49f91bdf36"
                }
            ]
        },
        {
            "id": "e33b9a75-238b-4dcf-a822-6643a08539d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5938cad5-e878-437a-b1ae-93e0ed602c2e",
            "compositeImage": {
                "id": "65dbadb9-cb0a-42a1-a8f0-df0ec8cd9b20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e33b9a75-238b-4dcf-a822-6643a08539d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e16bf145-39ca-46d0-9a36-f5460948ce98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e33b9a75-238b-4dcf-a822-6643a08539d9",
                    "LayerId": "69e0a9bf-c31c-4dcc-8a6b-1d49f91bdf36"
                }
            ]
        },
        {
            "id": "d264dd09-6b28-45cf-b703-c36591a11920",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5938cad5-e878-437a-b1ae-93e0ed602c2e",
            "compositeImage": {
                "id": "67f2a6ef-edef-4379-af36-1e308a74a9f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d264dd09-6b28-45cf-b703-c36591a11920",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "705c1a24-ce12-4f64-9fb1-f0c7354db195",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d264dd09-6b28-45cf-b703-c36591a11920",
                    "LayerId": "69e0a9bf-c31c-4dcc-8a6b-1d49f91bdf36"
                }
            ]
        },
        {
            "id": "3b003986-4218-4fcb-97d8-79847d067701",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5938cad5-e878-437a-b1ae-93e0ed602c2e",
            "compositeImage": {
                "id": "1038e3e7-5e08-4304-a7c4-7d47b8dd0405",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b003986-4218-4fcb-97d8-79847d067701",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eed3214-c7de-4dbd-9bb8-9fed01146ec5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b003986-4218-4fcb-97d8-79847d067701",
                    "LayerId": "69e0a9bf-c31c-4dcc-8a6b-1d49f91bdf36"
                }
            ]
        },
        {
            "id": "ef41f600-b5f4-433b-bdc6-d0167d2aea96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5938cad5-e878-437a-b1ae-93e0ed602c2e",
            "compositeImage": {
                "id": "61fbe378-bd6b-4ff0-90b9-7c199a7c2aeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef41f600-b5f4-433b-bdc6-d0167d2aea96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba91ffc2-2e93-4b9a-b690-21e3f4ab675c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef41f600-b5f4-433b-bdc6-d0167d2aea96",
                    "LayerId": "69e0a9bf-c31c-4dcc-8a6b-1d49f91bdf36"
                }
            ]
        },
        {
            "id": "0e316986-09c6-409b-850f-90c477436106",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5938cad5-e878-437a-b1ae-93e0ed602c2e",
            "compositeImage": {
                "id": "2eeeef9d-fb01-4c02-a143-e5dacffbd2a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e316986-09c6-409b-850f-90c477436106",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83d1da04-3589-48e0-837c-58c9fc5d98d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e316986-09c6-409b-850f-90c477436106",
                    "LayerId": "69e0a9bf-c31c-4dcc-8a6b-1d49f91bdf36"
                }
            ]
        },
        {
            "id": "5bff8544-60e9-45ed-a5b8-e943fad7222b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5938cad5-e878-437a-b1ae-93e0ed602c2e",
            "compositeImage": {
                "id": "5776e534-f1ab-478f-bd24-cde820d93f9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bff8544-60e9-45ed-a5b8-e943fad7222b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e736b85c-bcda-4f23-b702-78f85289bb98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bff8544-60e9-45ed-a5b8-e943fad7222b",
                    "LayerId": "69e0a9bf-c31c-4dcc-8a6b-1d49f91bdf36"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "69e0a9bf-c31c-4dcc-8a6b-1d49f91bdf36",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5938cad5-e878-437a-b1ae-93e0ed602c2e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}