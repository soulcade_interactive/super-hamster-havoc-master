{
    "id": "24214e1c-13e9-483d-bc2f-0a62292af4bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buff_0_legs_d_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 19,
    "bbox_right": 44,
    "bbox_top": 43,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a412696e-1347-437a-8db7-be3e2cd8e8f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24214e1c-13e9-483d-bc2f-0a62292af4bd",
            "compositeImage": {
                "id": "3626d3c6-3240-489a-9bab-21bce45b4160",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a412696e-1347-437a-8db7-be3e2cd8e8f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a130091-0daf-49d6-bafc-d06a3f260437",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a412696e-1347-437a-8db7-be3e2cd8e8f0",
                    "LayerId": "e974e564-e0fe-4cb1-bdce-b17cd636bdec"
                }
            ]
        },
        {
            "id": "2d9ec823-a3cb-4d5a-a68f-e9740be96e7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24214e1c-13e9-483d-bc2f-0a62292af4bd",
            "compositeImage": {
                "id": "f507de9a-a95b-4e65-b4c5-c9588c6bbe32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d9ec823-a3cb-4d5a-a68f-e9740be96e7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e5430cd-85cd-4ec9-9e2f-78025f0b3210",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d9ec823-a3cb-4d5a-a68f-e9740be96e7f",
                    "LayerId": "e974e564-e0fe-4cb1-bdce-b17cd636bdec"
                }
            ]
        },
        {
            "id": "64ee3b6a-72be-4205-a3f1-4c01d8163b89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24214e1c-13e9-483d-bc2f-0a62292af4bd",
            "compositeImage": {
                "id": "a1c69266-3be6-4524-9b32-0434fa7245a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64ee3b6a-72be-4205-a3f1-4c01d8163b89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d1509c0-c9ab-4019-a9fd-0ef3e58b4ab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64ee3b6a-72be-4205-a3f1-4c01d8163b89",
                    "LayerId": "e974e564-e0fe-4cb1-bdce-b17cd636bdec"
                }
            ]
        },
        {
            "id": "c82c2436-c7a9-4cb0-ae45-b6a3f002040d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24214e1c-13e9-483d-bc2f-0a62292af4bd",
            "compositeImage": {
                "id": "054ba1e8-1e59-4c3a-8c0c-8d8fcf830074",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c82c2436-c7a9-4cb0-ae45-b6a3f002040d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b50c824f-a644-4119-a228-6817350d9105",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c82c2436-c7a9-4cb0-ae45-b6a3f002040d",
                    "LayerId": "e974e564-e0fe-4cb1-bdce-b17cd636bdec"
                }
            ]
        },
        {
            "id": "c932391c-7cf2-471e-a71d-1ce86ef1cb9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24214e1c-13e9-483d-bc2f-0a62292af4bd",
            "compositeImage": {
                "id": "4a625c69-a308-4680-a016-56c64ada3572",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c932391c-7cf2-471e-a71d-1ce86ef1cb9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "908aaefe-2ef4-4847-ae99-3d64f23466d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c932391c-7cf2-471e-a71d-1ce86ef1cb9b",
                    "LayerId": "e974e564-e0fe-4cb1-bdce-b17cd636bdec"
                }
            ]
        },
        {
            "id": "e641ab13-e0ac-43aa-97ff-6577d650b0b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24214e1c-13e9-483d-bc2f-0a62292af4bd",
            "compositeImage": {
                "id": "106e46bb-1e90-4a27-9139-75dd748efd42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e641ab13-e0ac-43aa-97ff-6577d650b0b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db871b7a-1375-4822-bfea-154a350e40dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e641ab13-e0ac-43aa-97ff-6577d650b0b0",
                    "LayerId": "e974e564-e0fe-4cb1-bdce-b17cd636bdec"
                }
            ]
        },
        {
            "id": "9ad45e5a-d85d-447c-aac2-462c203130e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24214e1c-13e9-483d-bc2f-0a62292af4bd",
            "compositeImage": {
                "id": "2b36b106-7236-4811-b571-33c24c63cad1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ad45e5a-d85d-447c-aac2-462c203130e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70ccdda6-ce5d-4799-9345-d4a345a38d50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ad45e5a-d85d-447c-aac2-462c203130e2",
                    "LayerId": "e974e564-e0fe-4cb1-bdce-b17cd636bdec"
                }
            ]
        },
        {
            "id": "03738d92-68d0-4325-80dd-dcad9744c0eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24214e1c-13e9-483d-bc2f-0a62292af4bd",
            "compositeImage": {
                "id": "f6d642c7-8752-497c-8796-b2af446b896f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03738d92-68d0-4325-80dd-dcad9744c0eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c34286a2-e341-4e0d-91b6-83fc2a04f292",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03738d92-68d0-4325-80dd-dcad9744c0eb",
                    "LayerId": "e974e564-e0fe-4cb1-bdce-b17cd636bdec"
                }
            ]
        },
        {
            "id": "e8225ba3-0dcd-41b6-8f9f-9bcbed3af104",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24214e1c-13e9-483d-bc2f-0a62292af4bd",
            "compositeImage": {
                "id": "6bf3d65b-c60c-4d51-898d-69dfac0b8e98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8225ba3-0dcd-41b6-8f9f-9bcbed3af104",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c415450d-b056-4bab-9bb2-9f2c4fe7e406",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8225ba3-0dcd-41b6-8f9f-9bcbed3af104",
                    "LayerId": "e974e564-e0fe-4cb1-bdce-b17cd636bdec"
                }
            ]
        },
        {
            "id": "de304e74-aea8-4ffa-a1fd-9015b464b886",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24214e1c-13e9-483d-bc2f-0a62292af4bd",
            "compositeImage": {
                "id": "c0eaab13-189e-470a-9c15-ed4ea802e985",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de304e74-aea8-4ffa-a1fd-9015b464b886",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc0b042a-2f41-4e2e-9747-17238dfe4110",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de304e74-aea8-4ffa-a1fd-9015b464b886",
                    "LayerId": "e974e564-e0fe-4cb1-bdce-b17cd636bdec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e974e564-e0fe-4cb1-bdce-b17cd636bdec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24214e1c-13e9-483d-bc2f-0a62292af4bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}