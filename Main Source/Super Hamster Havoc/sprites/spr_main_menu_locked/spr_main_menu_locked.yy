{
    "id": "96200e7d-25a9-45c9-bfb2-e9356d5fbe82",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_main_menu_locked",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 237,
    "bbox_left": 54,
    "bbox_right": 195,
    "bbox_top": 31,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb0616e3-dfed-4317-84ee-641b3fa4a12a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96200e7d-25a9-45c9-bfb2-e9356d5fbe82",
            "compositeImage": {
                "id": "c9d6db45-d1af-4ae6-bbeb-5530a24a01a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb0616e3-dfed-4317-84ee-641b3fa4a12a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87ef68e1-6356-40f9-a4e3-b50e82b22f06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb0616e3-dfed-4317-84ee-641b3fa4a12a",
                    "LayerId": "4f8f5bd1-0a0f-4a6a-a671-db155024b925"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "4f8f5bd1-0a0f-4a6a-a671-db155024b925",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96200e7d-25a9-45c9-bfb2-e9356d5fbe82",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}