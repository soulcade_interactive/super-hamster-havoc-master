{
    "id": "4283fc53-9677-4fc5-a998-69d49fbc1954",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_body_run_back_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d9f1751-6a57-4223-b74c-da2438a97d30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4283fc53-9677-4fc5-a998-69d49fbc1954",
            "compositeImage": {
                "id": "8d29a01a-9c30-46e0-8501-cbb23f1c8de4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d9f1751-6a57-4223-b74c-da2438a97d30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fb22ed2-7d56-465c-80a3-a15927ad5446",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d9f1751-6a57-4223-b74c-da2438a97d30",
                    "LayerId": "b6c964c8-298b-4bc0-8ceb-2926d79a66b9"
                }
            ]
        },
        {
            "id": "a4c81462-5ff8-42f5-8f54-2c737a6bddd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4283fc53-9677-4fc5-a998-69d49fbc1954",
            "compositeImage": {
                "id": "68346d02-09eb-4682-9b6e-e3b7e14d691d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4c81462-5ff8-42f5-8f54-2c737a6bddd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2df4b935-cceb-4baf-90f1-efc65b6f7dd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4c81462-5ff8-42f5-8f54-2c737a6bddd5",
                    "LayerId": "b6c964c8-298b-4bc0-8ceb-2926d79a66b9"
                }
            ]
        },
        {
            "id": "199b76df-6162-43ea-8231-a859b68500a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4283fc53-9677-4fc5-a998-69d49fbc1954",
            "compositeImage": {
                "id": "692896cd-28e5-4c54-aa58-b17763942825",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "199b76df-6162-43ea-8231-a859b68500a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c353757c-f20d-401f-a902-38bc4966fc0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "199b76df-6162-43ea-8231-a859b68500a4",
                    "LayerId": "b6c964c8-298b-4bc0-8ceb-2926d79a66b9"
                }
            ]
        },
        {
            "id": "354f1359-e916-4396-bc4e-7c9757d4460c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4283fc53-9677-4fc5-a998-69d49fbc1954",
            "compositeImage": {
                "id": "b92fccda-2279-492c-900a-c23bdc74dd1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "354f1359-e916-4396-bc4e-7c9757d4460c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "885ce677-6133-4120-93f6-f5ef31f24c46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "354f1359-e916-4396-bc4e-7c9757d4460c",
                    "LayerId": "b6c964c8-298b-4bc0-8ceb-2926d79a66b9"
                }
            ]
        },
        {
            "id": "e70a1b48-591d-4df5-9b10-15d6ac1dc012",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4283fc53-9677-4fc5-a998-69d49fbc1954",
            "compositeImage": {
                "id": "5560d4ea-5269-4af8-99dc-3511e37c177b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e70a1b48-591d-4df5-9b10-15d6ac1dc012",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfdbda0a-73d9-4b8e-bee9-1713ea123781",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e70a1b48-591d-4df5-9b10-15d6ac1dc012",
                    "LayerId": "b6c964c8-298b-4bc0-8ceb-2926d79a66b9"
                }
            ]
        },
        {
            "id": "a092ed6d-bd66-4017-9e99-fe7972f676e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4283fc53-9677-4fc5-a998-69d49fbc1954",
            "compositeImage": {
                "id": "32e96721-44f0-4bad-88b3-ec473956c36d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a092ed6d-bd66-4017-9e99-fe7972f676e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a45b8c07-6a03-423c-b3b5-78b51f71c627",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a092ed6d-bd66-4017-9e99-fe7972f676e3",
                    "LayerId": "b6c964c8-298b-4bc0-8ceb-2926d79a66b9"
                }
            ]
        },
        {
            "id": "96b13830-3ad6-4f56-8538-c2efd3173435",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4283fc53-9677-4fc5-a998-69d49fbc1954",
            "compositeImage": {
                "id": "df90f610-39f8-4f4f-b0a7-fb5b636006d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96b13830-3ad6-4f56-8538-c2efd3173435",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e89effe-6692-4ed5-b879-96e30ca9e62e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96b13830-3ad6-4f56-8538-c2efd3173435",
                    "LayerId": "b6c964c8-298b-4bc0-8ceb-2926d79a66b9"
                }
            ]
        },
        {
            "id": "723807fb-ecd4-4ff6-b54e-f40f545249f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4283fc53-9677-4fc5-a998-69d49fbc1954",
            "compositeImage": {
                "id": "e1731d67-8c18-41f6-98e8-101f1aaa2c1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "723807fb-ecd4-4ff6-b54e-f40f545249f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "674665b8-3b0f-46b1-97c9-e9b1169ce03c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "723807fb-ecd4-4ff6-b54e-f40f545249f4",
                    "LayerId": "b6c964c8-298b-4bc0-8ceb-2926d79a66b9"
                }
            ]
        },
        {
            "id": "5fa13104-9b87-4d64-aa1e-7309c5607dbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4283fc53-9677-4fc5-a998-69d49fbc1954",
            "compositeImage": {
                "id": "357e1a11-4ac1-444b-afd9-365a5de8a260",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fa13104-9b87-4d64-aa1e-7309c5607dbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea71b0b2-b95c-4943-a56c-435ba62b8cfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fa13104-9b87-4d64-aa1e-7309c5607dbd",
                    "LayerId": "b6c964c8-298b-4bc0-8ceb-2926d79a66b9"
                }
            ]
        },
        {
            "id": "d51358e3-abfa-4dce-9237-77a7adb36058",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4283fc53-9677-4fc5-a998-69d49fbc1954",
            "compositeImage": {
                "id": "8fe8da70-ef4a-4f17-9ed8-3817db21d5ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d51358e3-abfa-4dce-9237-77a7adb36058",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81131a52-27b0-456f-8c53-43f34aa3c311",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d51358e3-abfa-4dce-9237-77a7adb36058",
                    "LayerId": "b6c964c8-298b-4bc0-8ceb-2926d79a66b9"
                }
            ]
        },
        {
            "id": "b999758d-68a1-49eb-a0df-ec8948e1edad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4283fc53-9677-4fc5-a998-69d49fbc1954",
            "compositeImage": {
                "id": "0de3a2d7-a646-4626-b25a-ad89519d7e3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b999758d-68a1-49eb-a0df-ec8948e1edad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d2d1b26-6b69-435c-bba7-aea14ec6ef68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b999758d-68a1-49eb-a0df-ec8948e1edad",
                    "LayerId": "b6c964c8-298b-4bc0-8ceb-2926d79a66b9"
                }
            ]
        },
        {
            "id": "870a781d-005a-469a-8f7c-3cbe0aa4b7e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4283fc53-9677-4fc5-a998-69d49fbc1954",
            "compositeImage": {
                "id": "dd224dc5-e958-4a12-9bd9-15a5e6abf3bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "870a781d-005a-469a-8f7c-3cbe0aa4b7e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "160286a0-2c75-4ff5-b1ac-54921dcc4f61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "870a781d-005a-469a-8f7c-3cbe0aa4b7e6",
                    "LayerId": "b6c964c8-298b-4bc0-8ceb-2926d79a66b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "b6c964c8-298b-4bc0-8ceb-2926d79a66b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4283fc53-9677-4fc5-a998-69d49fbc1954",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 21
}