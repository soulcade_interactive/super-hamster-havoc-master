{
    "id": "f918e021-0046-4748-b947-9c9bf8306c15",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_0_head_l_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 9,
    "bbox_right": 56,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3fb3f5ef-510b-4b17-9a97-019ef9968330",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f918e021-0046-4748-b947-9c9bf8306c15",
            "compositeImage": {
                "id": "65437d69-5b75-4765-88fa-067b0e59ce9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fb3f5ef-510b-4b17-9a97-019ef9968330",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1887e838-6dc9-402f-939e-4259979a1532",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fb3f5ef-510b-4b17-9a97-019ef9968330",
                    "LayerId": "94a1ba80-a896-4d69-b214-fcaa43e4057f"
                }
            ]
        },
        {
            "id": "7a86383d-8d17-45d9-b269-88433dc614ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f918e021-0046-4748-b947-9c9bf8306c15",
            "compositeImage": {
                "id": "aeb44037-2e97-4a2e-a127-45cc3b2b6cd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a86383d-8d17-45d9-b269-88433dc614ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62a09e44-75bf-4518-98fb-bf74d9dc97b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a86383d-8d17-45d9-b269-88433dc614ed",
                    "LayerId": "94a1ba80-a896-4d69-b214-fcaa43e4057f"
                }
            ]
        },
        {
            "id": "406217f8-95dc-45cc-9064-345c6bf54c4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f918e021-0046-4748-b947-9c9bf8306c15",
            "compositeImage": {
                "id": "5f6c2589-886a-4bd3-b546-304ed47a894d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "406217f8-95dc-45cc-9064-345c6bf54c4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4357372d-0b12-4582-8960-8244fd9d37d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "406217f8-95dc-45cc-9064-345c6bf54c4f",
                    "LayerId": "94a1ba80-a896-4d69-b214-fcaa43e4057f"
                }
            ]
        },
        {
            "id": "ba37c79c-662e-4a9d-a2fa-7cdf13318ba6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f918e021-0046-4748-b947-9c9bf8306c15",
            "compositeImage": {
                "id": "4eb36402-b731-4fdc-a6f0-c9031d76273f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba37c79c-662e-4a9d-a2fa-7cdf13318ba6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87d4ef50-52bc-4185-a07f-8486f11b1787",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba37c79c-662e-4a9d-a2fa-7cdf13318ba6",
                    "LayerId": "94a1ba80-a896-4d69-b214-fcaa43e4057f"
                }
            ]
        },
        {
            "id": "e231ee83-c42c-43a4-bc29-5c8edbdcf1e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f918e021-0046-4748-b947-9c9bf8306c15",
            "compositeImage": {
                "id": "4f94e584-8622-440f-ae96-0ed240dfafba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e231ee83-c42c-43a4-bc29-5c8edbdcf1e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fe1290d-2dd6-4e0e-9ce9-a362cbf7e00b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e231ee83-c42c-43a4-bc29-5c8edbdcf1e3",
                    "LayerId": "94a1ba80-a896-4d69-b214-fcaa43e4057f"
                }
            ]
        },
        {
            "id": "81e63e06-c3c0-4984-8efe-6fc06630eec8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f918e021-0046-4748-b947-9c9bf8306c15",
            "compositeImage": {
                "id": "6a1a47c5-8ae8-45db-a6de-66dc737e4bab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81e63e06-c3c0-4984-8efe-6fc06630eec8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52a4eb12-d864-44a4-8794-829c48d0ff85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81e63e06-c3c0-4984-8efe-6fc06630eec8",
                    "LayerId": "94a1ba80-a896-4d69-b214-fcaa43e4057f"
                }
            ]
        },
        {
            "id": "92d33637-2e1d-456c-966e-bd3ec9fd6bf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f918e021-0046-4748-b947-9c9bf8306c15",
            "compositeImage": {
                "id": "704f3f5a-900a-46df-b4f2-ea36fa7986e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92d33637-2e1d-456c-966e-bd3ec9fd6bf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6abef8b-214a-42a0-a1a8-35c7a5305e7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92d33637-2e1d-456c-966e-bd3ec9fd6bf0",
                    "LayerId": "94a1ba80-a896-4d69-b214-fcaa43e4057f"
                }
            ]
        },
        {
            "id": "e5d2eeb2-6f5e-4f16-89cd-f35b75661f3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f918e021-0046-4748-b947-9c9bf8306c15",
            "compositeImage": {
                "id": "9ce4ed3d-7ff9-46c2-8fbe-e7482541b725",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5d2eeb2-6f5e-4f16-89cd-f35b75661f3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6f4b4a5-5a64-4e9b-b2f5-a438a840e6b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5d2eeb2-6f5e-4f16-89cd-f35b75661f3f",
                    "LayerId": "94a1ba80-a896-4d69-b214-fcaa43e4057f"
                }
            ]
        },
        {
            "id": "6e322edb-3546-47a2-b35c-7299667d7a52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f918e021-0046-4748-b947-9c9bf8306c15",
            "compositeImage": {
                "id": "56dc7c6d-eab7-4264-8e27-215d77f3fb7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e322edb-3546-47a2-b35c-7299667d7a52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec09f4e3-e6ef-4712-9d4f-a6100c4a036a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e322edb-3546-47a2-b35c-7299667d7a52",
                    "LayerId": "94a1ba80-a896-4d69-b214-fcaa43e4057f"
                }
            ]
        },
        {
            "id": "7ba94be5-b335-413d-98b0-8c29e6d3aefd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f918e021-0046-4748-b947-9c9bf8306c15",
            "compositeImage": {
                "id": "6e31fabb-fa75-4a48-8955-1f4b6be21889",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ba94be5-b335-413d-98b0-8c29e6d3aefd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd92429a-8d30-4478-b402-53f48c7b697b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ba94be5-b335-413d-98b0-8c29e6d3aefd",
                    "LayerId": "94a1ba80-a896-4d69-b214-fcaa43e4057f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "94a1ba80-a896-4d69-b214-fcaa43e4057f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f918e021-0046-4748-b947-9c9bf8306c15",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}