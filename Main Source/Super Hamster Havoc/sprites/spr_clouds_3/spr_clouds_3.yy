{
    "id": "b8df6ef9-b3fa-4303-b6c6-b93e50a025a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_clouds_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 2399,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9f42d1f-e55a-47ec-acc7-86f8e04c405d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8df6ef9-b3fa-4303-b6c6-b93e50a025a5",
            "compositeImage": {
                "id": "493c9ae0-d608-4b34-b0df-6045cf5951b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9f42d1f-e55a-47ec-acc7-86f8e04c405d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46a3b797-8b91-4e11-a40c-a604e90260a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9f42d1f-e55a-47ec-acc7-86f8e04c405d",
                    "LayerId": "bacb235c-e074-4643-b138-7c2b2d60738f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 780,
    "layers": [
        {
            "id": "bacb235c-e074-4643-b138-7c2b2d60738f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8df6ef9-b3fa-4303-b6c6-b93e50a025a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2400,
    "xorig": 1200,
    "yorig": 390
}