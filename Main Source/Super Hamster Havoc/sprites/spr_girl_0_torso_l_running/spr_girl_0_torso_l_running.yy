{
    "id": "53a81d82-a2cf-4440-ae1c-8dd454006fa4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_0_torso_l_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 26,
    "bbox_right": 39,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6a6e149-0a52-469a-9a97-565be9826a5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53a81d82-a2cf-4440-ae1c-8dd454006fa4",
            "compositeImage": {
                "id": "3ae6c06b-a2c7-4d9c-80df-4bc4721cae78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6a6e149-0a52-469a-9a97-565be9826a5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8551a891-9da4-41b6-be6a-acc3d7718443",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6a6e149-0a52-469a-9a97-565be9826a5b",
                    "LayerId": "4f838eef-5481-46d8-96d3-7367d5fa2920"
                }
            ]
        },
        {
            "id": "0671fe3f-9028-420b-a754-c498cd0cbd48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53a81d82-a2cf-4440-ae1c-8dd454006fa4",
            "compositeImage": {
                "id": "44abf5a4-e1fa-4d69-ab55-8891b4392e41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0671fe3f-9028-420b-a754-c498cd0cbd48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf0497da-825a-4cc7-9e84-58b372d32f11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0671fe3f-9028-420b-a754-c498cd0cbd48",
                    "LayerId": "4f838eef-5481-46d8-96d3-7367d5fa2920"
                }
            ]
        },
        {
            "id": "e774ec31-918a-4ab6-9577-dd0933bce6d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53a81d82-a2cf-4440-ae1c-8dd454006fa4",
            "compositeImage": {
                "id": "41db44d6-283f-47d4-8ff4-5a11c2d65afc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e774ec31-918a-4ab6-9577-dd0933bce6d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a840643-a2d5-445a-80e9-ace45aab32f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e774ec31-918a-4ab6-9577-dd0933bce6d8",
                    "LayerId": "4f838eef-5481-46d8-96d3-7367d5fa2920"
                }
            ]
        },
        {
            "id": "f9b3ac69-1b56-40de-95c0-7e858a6d5f14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53a81d82-a2cf-4440-ae1c-8dd454006fa4",
            "compositeImage": {
                "id": "a3f8c356-eeff-4393-8871-2a0e9106abe6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9b3ac69-1b56-40de-95c0-7e858a6d5f14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "136ec67c-5fd1-4ab5-a1c2-36b54b65cf6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9b3ac69-1b56-40de-95c0-7e858a6d5f14",
                    "LayerId": "4f838eef-5481-46d8-96d3-7367d5fa2920"
                }
            ]
        },
        {
            "id": "73c610aa-5b3b-4127-9739-0de65c771565",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53a81d82-a2cf-4440-ae1c-8dd454006fa4",
            "compositeImage": {
                "id": "feb69e3c-9cb0-4c3b-af97-0d78f6a92192",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73c610aa-5b3b-4127-9739-0de65c771565",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81056f13-3a72-4800-b3ed-eec1782fa49b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73c610aa-5b3b-4127-9739-0de65c771565",
                    "LayerId": "4f838eef-5481-46d8-96d3-7367d5fa2920"
                }
            ]
        },
        {
            "id": "a6202b1c-caa4-4717-a0ec-fa982b4da76c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53a81d82-a2cf-4440-ae1c-8dd454006fa4",
            "compositeImage": {
                "id": "dd00c885-6094-48c6-9fdd-f7fedf75292f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6202b1c-caa4-4717-a0ec-fa982b4da76c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6141c26c-d586-4c4a-bddc-afdbeea37afb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6202b1c-caa4-4717-a0ec-fa982b4da76c",
                    "LayerId": "4f838eef-5481-46d8-96d3-7367d5fa2920"
                }
            ]
        },
        {
            "id": "c6082e79-0980-4daa-9209-482180a0fd5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53a81d82-a2cf-4440-ae1c-8dd454006fa4",
            "compositeImage": {
                "id": "025f78e1-57f5-456d-86ba-5f095ef97bd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6082e79-0980-4daa-9209-482180a0fd5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfe6a32f-6541-4466-a4bd-358e9fd09a2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6082e79-0980-4daa-9209-482180a0fd5a",
                    "LayerId": "4f838eef-5481-46d8-96d3-7367d5fa2920"
                }
            ]
        },
        {
            "id": "9db9a422-d3e1-4946-9ae0-f65df22d6455",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53a81d82-a2cf-4440-ae1c-8dd454006fa4",
            "compositeImage": {
                "id": "a78fddb5-ea99-429e-aa01-9d5ac7850e1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9db9a422-d3e1-4946-9ae0-f65df22d6455",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2fc7411-b5b6-4089-9a63-a22aae03d8b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9db9a422-d3e1-4946-9ae0-f65df22d6455",
                    "LayerId": "4f838eef-5481-46d8-96d3-7367d5fa2920"
                }
            ]
        },
        {
            "id": "5e5ff513-f15a-4651-b7af-723222c55caa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53a81d82-a2cf-4440-ae1c-8dd454006fa4",
            "compositeImage": {
                "id": "361efd63-3312-4883-b2fa-1c2f4b74fac9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e5ff513-f15a-4651-b7af-723222c55caa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f143cecd-19cd-42c5-a027-eec459018e29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e5ff513-f15a-4651-b7af-723222c55caa",
                    "LayerId": "4f838eef-5481-46d8-96d3-7367d5fa2920"
                }
            ]
        },
        {
            "id": "ffa68265-a2e5-4988-b872-fdbe42792a86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "53a81d82-a2cf-4440-ae1c-8dd454006fa4",
            "compositeImage": {
                "id": "5c518fbe-d193-4456-ad66-425298540934",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffa68265-a2e5-4988-b872-fdbe42792a86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "644a11e9-70e0-4c50-89b9-08b647ea291e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffa68265-a2e5-4988-b872-fdbe42792a86",
                    "LayerId": "4f838eef-5481-46d8-96d3-7367d5fa2920"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4f838eef-5481-46d8-96d3-7367d5fa2920",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "53a81d82-a2cf-4440-ae1c-8dd454006fa4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}