{
    "id": "5d150f75-16b6-4ff5-bbcc-9b892a488501",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_body_run_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ccd7b8c2-327c-4d6e-a3ea-5cd0d92efd13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d150f75-16b6-4ff5-bbcc-9b892a488501",
            "compositeImage": {
                "id": "ac716cf5-ded8-46a1-ba29-792524ba7b00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccd7b8c2-327c-4d6e-a3ea-5cd0d92efd13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "861125bc-0aa9-47b8-b64a-0deb0f0de392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccd7b8c2-327c-4d6e-a3ea-5cd0d92efd13",
                    "LayerId": "c8b9afa8-0353-4d68-98ff-6a2eed8bcf0d"
                }
            ]
        },
        {
            "id": "d2da763b-d1e4-484f-8029-a38176d3674b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d150f75-16b6-4ff5-bbcc-9b892a488501",
            "compositeImage": {
                "id": "3f2c8e48-d0cb-43de-88b2-4d3ff102a02d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2da763b-d1e4-484f-8029-a38176d3674b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cca6d95-563e-47d1-8621-f5d125d52ad3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2da763b-d1e4-484f-8029-a38176d3674b",
                    "LayerId": "c8b9afa8-0353-4d68-98ff-6a2eed8bcf0d"
                }
            ]
        },
        {
            "id": "b13f7d77-332c-4da5-976e-fcc1a4f2cdf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d150f75-16b6-4ff5-bbcc-9b892a488501",
            "compositeImage": {
                "id": "53e16d27-cc7d-4385-80b4-e323c4cacf55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b13f7d77-332c-4da5-976e-fcc1a4f2cdf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d915ef1-7f2d-43e7-b855-6b3c63a0c75d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b13f7d77-332c-4da5-976e-fcc1a4f2cdf9",
                    "LayerId": "c8b9afa8-0353-4d68-98ff-6a2eed8bcf0d"
                }
            ]
        },
        {
            "id": "ac8421d0-d147-47ac-b5b6-19e3d4d88862",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d150f75-16b6-4ff5-bbcc-9b892a488501",
            "compositeImage": {
                "id": "9f10c23e-0b7d-44e9-baf0-15435789b461",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac8421d0-d147-47ac-b5b6-19e3d4d88862",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86903ef7-4f3a-4094-a215-8d69e314c2b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac8421d0-d147-47ac-b5b6-19e3d4d88862",
                    "LayerId": "c8b9afa8-0353-4d68-98ff-6a2eed8bcf0d"
                }
            ]
        },
        {
            "id": "51e44622-76c7-449c-b954-3d5da248ef79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d150f75-16b6-4ff5-bbcc-9b892a488501",
            "compositeImage": {
                "id": "2c06a9f7-0c31-47bd-a69e-1c2a34cd724b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51e44622-76c7-449c-b954-3d5da248ef79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ecdb5df-71b5-454f-af64-abad7d3946b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51e44622-76c7-449c-b954-3d5da248ef79",
                    "LayerId": "c8b9afa8-0353-4d68-98ff-6a2eed8bcf0d"
                }
            ]
        },
        {
            "id": "c671eb77-d700-42b8-b314-ca7ff7d27ed3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d150f75-16b6-4ff5-bbcc-9b892a488501",
            "compositeImage": {
                "id": "0806cdcc-e6c1-4317-8659-0232eac9bea3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c671eb77-d700-42b8-b314-ca7ff7d27ed3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddce975a-49bf-4c69-a8c9-e01c87dd2595",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c671eb77-d700-42b8-b314-ca7ff7d27ed3",
                    "LayerId": "c8b9afa8-0353-4d68-98ff-6a2eed8bcf0d"
                }
            ]
        },
        {
            "id": "540f1a23-d90a-41b9-9787-da2011fb8842",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d150f75-16b6-4ff5-bbcc-9b892a488501",
            "compositeImage": {
                "id": "df925065-450c-4dfe-b299-3cd460b8e939",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "540f1a23-d90a-41b9-9787-da2011fb8842",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6da404c-0b40-41a4-a398-c1a18018ccc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "540f1a23-d90a-41b9-9787-da2011fb8842",
                    "LayerId": "c8b9afa8-0353-4d68-98ff-6a2eed8bcf0d"
                }
            ]
        },
        {
            "id": "8592169b-f4d5-4ad2-86fc-2df3e1a0cfc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d150f75-16b6-4ff5-bbcc-9b892a488501",
            "compositeImage": {
                "id": "4bdcc332-307f-4441-8242-fc93617676f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8592169b-f4d5-4ad2-86fc-2df3e1a0cfc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1d8c3d2-a1cb-425c-94bf-a5304f1bacda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8592169b-f4d5-4ad2-86fc-2df3e1a0cfc3",
                    "LayerId": "c8b9afa8-0353-4d68-98ff-6a2eed8bcf0d"
                }
            ]
        },
        {
            "id": "0304ead3-54cb-4d17-92f2-92635369cbe4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d150f75-16b6-4ff5-bbcc-9b892a488501",
            "compositeImage": {
                "id": "ec9ae5e1-a5e4-4951-a282-8cd0502fe685",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0304ead3-54cb-4d17-92f2-92635369cbe4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73244efe-88bc-415e-91f8-44841fa27865",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0304ead3-54cb-4d17-92f2-92635369cbe4",
                    "LayerId": "c8b9afa8-0353-4d68-98ff-6a2eed8bcf0d"
                }
            ]
        },
        {
            "id": "b130b62e-3baf-4f5b-87e3-7b10e2ce1186",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d150f75-16b6-4ff5-bbcc-9b892a488501",
            "compositeImage": {
                "id": "74344700-639e-44ae-b255-27380fffeaec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b130b62e-3baf-4f5b-87e3-7b10e2ce1186",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26c19c81-0ac2-4cf1-bc4c-024ab0048c9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b130b62e-3baf-4f5b-87e3-7b10e2ce1186",
                    "LayerId": "c8b9afa8-0353-4d68-98ff-6a2eed8bcf0d"
                }
            ]
        },
        {
            "id": "9fd3bc58-d751-4c0f-87ac-7f82279aa283",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d150f75-16b6-4ff5-bbcc-9b892a488501",
            "compositeImage": {
                "id": "ff8779a9-e719-41dc-9331-0b8b526208cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fd3bc58-d751-4c0f-87ac-7f82279aa283",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cb55509-7586-4fa0-876d-a9c625ba476b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fd3bc58-d751-4c0f-87ac-7f82279aa283",
                    "LayerId": "c8b9afa8-0353-4d68-98ff-6a2eed8bcf0d"
                }
            ]
        },
        {
            "id": "955981d9-9d8b-45cb-b3b3-3416c97626e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d150f75-16b6-4ff5-bbcc-9b892a488501",
            "compositeImage": {
                "id": "10c6c5ef-d760-47a8-ae97-0258ff74c895",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "955981d9-9d8b-45cb-b3b3-3416c97626e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d3c3c96-3c82-43d6-b51c-2b2a52b67b9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "955981d9-9d8b-45cb-b3b3-3416c97626e1",
                    "LayerId": "c8b9afa8-0353-4d68-98ff-6a2eed8bcf0d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "c8b9afa8-0353-4d68-98ff-6a2eed8bcf0d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d150f75-16b6-4ff5-bbcc-9b892a488501",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 21
}