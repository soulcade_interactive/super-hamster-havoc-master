{
    "id": "7d37b997-2a67-470b-a73f-b9c6f7ec7117",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_switch_lt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 8,
    "bbox_right": 90,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f7abf8c-ab3e-46d1-bce9-0be7469cabaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d37b997-2a67-470b-a73f-b9c6f7ec7117",
            "compositeImage": {
                "id": "304008a5-272c-4faf-a83a-9ed506843739",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f7abf8c-ab3e-46d1-bce9-0be7469cabaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95853b46-7adc-4cb0-b2e8-adc678f5f420",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f7abf8c-ab3e-46d1-bce9-0be7469cabaf",
                    "LayerId": "9e38012f-830f-438a-a422-1bbf3dac1b0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "9e38012f-830f-438a-a422-1bbf3dac1b0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d37b997-2a67-470b-a73f-b9c6f7ec7117",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}