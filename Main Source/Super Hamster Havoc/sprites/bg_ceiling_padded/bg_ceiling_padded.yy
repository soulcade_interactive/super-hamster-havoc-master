{
    "id": "032f1b31-8b41-4dd0-a3ba-65a27c6966d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_ceiling_padded",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5bd5282a-4ff5-41dd-be59-dee376d721d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "032f1b31-8b41-4dd0-a3ba-65a27c6966d9",
            "compositeImage": {
                "id": "90af996b-67b3-41c6-82af-ba38dd838fb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bd5282a-4ff5-41dd-be59-dee376d721d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a26a221e-8fd9-4150-a948-54d5a6a2d328",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bd5282a-4ff5-41dd-be59-dee376d721d2",
                    "LayerId": "dfd005b9-64d9-40a3-be0b-7a43f355de96"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "dfd005b9-64d9-40a3-be0b-7a43f355de96",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "032f1b31-8b41-4dd0-a3ba-65a27c6966d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}