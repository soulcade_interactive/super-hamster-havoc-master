{
    "id": "e927e59b-6229-44fa-a25e-57cef543ece2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_hand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b730c94e-43a2-418b-a75b-109f601530be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e927e59b-6229-44fa-a25e-57cef543ece2",
            "compositeImage": {
                "id": "2fdf8243-5b1f-4046-85e2-30c30f01a0c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b730c94e-43a2-418b-a75b-109f601530be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d475414c-a0b2-40da-9bb4-c82a6bac9e85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b730c94e-43a2-418b-a75b-109f601530be",
                    "LayerId": "1a34d548-3789-489e-97ff-875331a3bfbe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "1a34d548-3789-489e-97ff-875331a3bfbe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e927e59b-6229-44fa-a25e-57cef543ece2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 3,
    "yorig": 0
}