{
    "id": "0a7fe004-a213-4d48-b141-f23140972915",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buff_0_head_u_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 21,
    "bbox_right": 42,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6a0c9c0-1aa4-4426-8a78-5e6ecf098bb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a7fe004-a213-4d48-b141-f23140972915",
            "compositeImage": {
                "id": "d31d8048-6bdd-4b65-b005-b1c522cad248",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6a0c9c0-1aa4-4426-8a78-5e6ecf098bb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01b215ed-8885-486e-84af-3a95316b797c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6a0c9c0-1aa4-4426-8a78-5e6ecf098bb2",
                    "LayerId": "d661810c-2398-4e6d-8dd9-10f14f147978"
                }
            ]
        },
        {
            "id": "8a687200-f9cb-4d93-a4c6-5f36ed3850dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a7fe004-a213-4d48-b141-f23140972915",
            "compositeImage": {
                "id": "de144237-d2fc-4cb7-9f1d-d22fdf9b051f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a687200-f9cb-4d93-a4c6-5f36ed3850dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89ee7794-3772-46bd-b225-8c37ad265348",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a687200-f9cb-4d93-a4c6-5f36ed3850dd",
                    "LayerId": "d661810c-2398-4e6d-8dd9-10f14f147978"
                }
            ]
        },
        {
            "id": "04ab42ff-a5af-4a61-9c78-567519b2e675",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a7fe004-a213-4d48-b141-f23140972915",
            "compositeImage": {
                "id": "3cfebe70-e7e0-4c9e-9eab-1020cd6a0633",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04ab42ff-a5af-4a61-9c78-567519b2e675",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab5c9cda-81d7-45f0-8029-07a24125347a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04ab42ff-a5af-4a61-9c78-567519b2e675",
                    "LayerId": "d661810c-2398-4e6d-8dd9-10f14f147978"
                }
            ]
        },
        {
            "id": "850d2412-5d97-4951-8a50-7d98ca21c53e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a7fe004-a213-4d48-b141-f23140972915",
            "compositeImage": {
                "id": "5c1e1451-4e22-4f87-9113-235078940660",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "850d2412-5d97-4951-8a50-7d98ca21c53e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fee15c00-6f44-4dbb-9d5c-0f2d13281447",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "850d2412-5d97-4951-8a50-7d98ca21c53e",
                    "LayerId": "d661810c-2398-4e6d-8dd9-10f14f147978"
                }
            ]
        },
        {
            "id": "05a9c308-1bf1-4c6d-b9bb-615949ac84e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a7fe004-a213-4d48-b141-f23140972915",
            "compositeImage": {
                "id": "2edb3854-7032-40f8-b76a-56d3a92f4e82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05a9c308-1bf1-4c6d-b9bb-615949ac84e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "371febb8-3e1d-4251-ad2e-d493afc01096",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05a9c308-1bf1-4c6d-b9bb-615949ac84e2",
                    "LayerId": "d661810c-2398-4e6d-8dd9-10f14f147978"
                }
            ]
        },
        {
            "id": "6e6dd4ba-59a2-4690-8a59-e1b5ba3c222c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a7fe004-a213-4d48-b141-f23140972915",
            "compositeImage": {
                "id": "e3783cd2-56bf-4fb9-b6af-3b8eb8cf8440",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e6dd4ba-59a2-4690-8a59-e1b5ba3c222c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c97b2070-e93c-41a0-9f8c-76e066d446d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e6dd4ba-59a2-4690-8a59-e1b5ba3c222c",
                    "LayerId": "d661810c-2398-4e6d-8dd9-10f14f147978"
                }
            ]
        },
        {
            "id": "d6f40021-34e2-436e-bdf5-da849c242f8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a7fe004-a213-4d48-b141-f23140972915",
            "compositeImage": {
                "id": "c4b97a43-e0a2-463e-9cf8-fbcd1bc325b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6f40021-34e2-436e-bdf5-da849c242f8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f83a8cb-6750-4c91-897a-4b66c2608c3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6f40021-34e2-436e-bdf5-da849c242f8c",
                    "LayerId": "d661810c-2398-4e6d-8dd9-10f14f147978"
                }
            ]
        },
        {
            "id": "0ba04cb8-a687-473b-a409-8e6d08c81c01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a7fe004-a213-4d48-b141-f23140972915",
            "compositeImage": {
                "id": "e4d3f72b-49d7-4c3f-9a18-b34129d1bd04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ba04cb8-a687-473b-a409-8e6d08c81c01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0843d2ad-1c36-41e5-afca-1670b68ebb1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ba04cb8-a687-473b-a409-8e6d08c81c01",
                    "LayerId": "d661810c-2398-4e6d-8dd9-10f14f147978"
                }
            ]
        },
        {
            "id": "c0dd92e8-a60f-44f0-8873-be6f6d3dcf11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a7fe004-a213-4d48-b141-f23140972915",
            "compositeImage": {
                "id": "2078f6a6-9785-42a9-8883-bfea8cf22bce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0dd92e8-a60f-44f0-8873-be6f6d3dcf11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5686e872-b151-49a2-9cff-67f2db97f4af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0dd92e8-a60f-44f0-8873-be6f6d3dcf11",
                    "LayerId": "d661810c-2398-4e6d-8dd9-10f14f147978"
                }
            ]
        },
        {
            "id": "2e798f3e-a36d-46a4-af52-0d7945ba69db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a7fe004-a213-4d48-b141-f23140972915",
            "compositeImage": {
                "id": "68113399-0c3f-4827-b73f-68865defd2de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e798f3e-a36d-46a4-af52-0d7945ba69db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0653a4f-ce36-4e49-b21c-4473d5bf2119",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e798f3e-a36d-46a4-af52-0d7945ba69db",
                    "LayerId": "d661810c-2398-4e6d-8dd9-10f14f147978"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d661810c-2398-4e6d-8dd9-10f14f147978",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0a7fe004-a213-4d48-b141-f23140972915",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}