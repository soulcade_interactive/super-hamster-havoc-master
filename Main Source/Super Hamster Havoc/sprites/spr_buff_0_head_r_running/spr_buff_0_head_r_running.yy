{
    "id": "cc6f3fad-27ce-4819-8cc7-8ca753d91023",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buff_0_head_r_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 21,
    "bbox_right": 43,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c95908c2-0632-47cb-8088-d923769176c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc6f3fad-27ce-4819-8cc7-8ca753d91023",
            "compositeImage": {
                "id": "d52a9dba-bc30-4b0b-ba55-8919792b1c81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c95908c2-0632-47cb-8088-d923769176c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e264123-bb88-4a3e-8bdf-f0708967a18e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c95908c2-0632-47cb-8088-d923769176c2",
                    "LayerId": "51cf4d47-b244-4b41-bca2-674ded8e1032"
                }
            ]
        },
        {
            "id": "de1e7d9c-06f2-405d-8269-a93fb8ee191a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc6f3fad-27ce-4819-8cc7-8ca753d91023",
            "compositeImage": {
                "id": "dfae4512-b2b8-494f-baef-811c494a62ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de1e7d9c-06f2-405d-8269-a93fb8ee191a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec89eebe-644d-4786-95af-572ba8f3f118",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de1e7d9c-06f2-405d-8269-a93fb8ee191a",
                    "LayerId": "51cf4d47-b244-4b41-bca2-674ded8e1032"
                }
            ]
        },
        {
            "id": "9a9255dd-a50a-4f6a-b61e-e93e686d911c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc6f3fad-27ce-4819-8cc7-8ca753d91023",
            "compositeImage": {
                "id": "6a849386-586f-46d8-8386-ae50bdd96326",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a9255dd-a50a-4f6a-b61e-e93e686d911c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "517c6ac2-68c1-4675-a6c5-130b3e9c65c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a9255dd-a50a-4f6a-b61e-e93e686d911c",
                    "LayerId": "51cf4d47-b244-4b41-bca2-674ded8e1032"
                }
            ]
        },
        {
            "id": "f7d24ebc-8ffd-41ba-8d3c-e009bad74663",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc6f3fad-27ce-4819-8cc7-8ca753d91023",
            "compositeImage": {
                "id": "331cd674-8649-49a5-a19f-bd35a37d5f50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7d24ebc-8ffd-41ba-8d3c-e009bad74663",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38fb6270-99b8-47bb-9e8c-b908899dd023",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7d24ebc-8ffd-41ba-8d3c-e009bad74663",
                    "LayerId": "51cf4d47-b244-4b41-bca2-674ded8e1032"
                }
            ]
        },
        {
            "id": "33761a26-fb79-4a8b-81ea-86e12d3cdc07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc6f3fad-27ce-4819-8cc7-8ca753d91023",
            "compositeImage": {
                "id": "39e272a1-c810-4bc0-8327-cc8856a91f78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33761a26-fb79-4a8b-81ea-86e12d3cdc07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "016e90a2-be13-42e9-bb90-f7d2181464ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33761a26-fb79-4a8b-81ea-86e12d3cdc07",
                    "LayerId": "51cf4d47-b244-4b41-bca2-674ded8e1032"
                }
            ]
        },
        {
            "id": "65e18149-fc81-422c-aaaf-f30bc70293f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc6f3fad-27ce-4819-8cc7-8ca753d91023",
            "compositeImage": {
                "id": "fc7c57b7-d285-48ac-8980-8a5b374f12d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65e18149-fc81-422c-aaaf-f30bc70293f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d3c5149-ff2c-4d43-8893-8d59e1369d04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65e18149-fc81-422c-aaaf-f30bc70293f6",
                    "LayerId": "51cf4d47-b244-4b41-bca2-674ded8e1032"
                }
            ]
        },
        {
            "id": "f16a0fe7-ca36-43d0-8e20-f9ba4075bf70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc6f3fad-27ce-4819-8cc7-8ca753d91023",
            "compositeImage": {
                "id": "1ecae3da-fe8c-43c5-9ca5-91a970c2c58a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f16a0fe7-ca36-43d0-8e20-f9ba4075bf70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "774704d5-a67e-46d4-a13a-b5d03f129a87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f16a0fe7-ca36-43d0-8e20-f9ba4075bf70",
                    "LayerId": "51cf4d47-b244-4b41-bca2-674ded8e1032"
                }
            ]
        },
        {
            "id": "5df3f1ab-9257-4f72-973f-ca00bacb99a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc6f3fad-27ce-4819-8cc7-8ca753d91023",
            "compositeImage": {
                "id": "74d2bb10-4e0a-47bd-b25f-b282e3b56e71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5df3f1ab-9257-4f72-973f-ca00bacb99a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "583bfb37-49ca-48c1-afd5-68c2c00a7620",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5df3f1ab-9257-4f72-973f-ca00bacb99a3",
                    "LayerId": "51cf4d47-b244-4b41-bca2-674ded8e1032"
                }
            ]
        },
        {
            "id": "a40d54fb-e809-431e-bc20-dd1af28281a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc6f3fad-27ce-4819-8cc7-8ca753d91023",
            "compositeImage": {
                "id": "fdc41618-3aaa-416b-a4a2-e46e89efd960",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a40d54fb-e809-431e-bc20-dd1af28281a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df3561b9-cbd6-4f87-afaa-e8377f417434",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a40d54fb-e809-431e-bc20-dd1af28281a2",
                    "LayerId": "51cf4d47-b244-4b41-bca2-674ded8e1032"
                }
            ]
        },
        {
            "id": "9bcfdb6c-05b4-4b78-ba08-a2458da56667",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc6f3fad-27ce-4819-8cc7-8ca753d91023",
            "compositeImage": {
                "id": "16badd96-234c-4430-a4f1-829011c4fd56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bcfdb6c-05b4-4b78-ba08-a2458da56667",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5aca445-e92f-4fbd-b77d-9834fbdacf20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bcfdb6c-05b4-4b78-ba08-a2458da56667",
                    "LayerId": "51cf4d47-b244-4b41-bca2-674ded8e1032"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "51cf4d47-b244-4b41-bca2-674ded8e1032",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc6f3fad-27ce-4819-8cc7-8ca753d91023",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}