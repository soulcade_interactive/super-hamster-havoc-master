{
    "id": "7803040d-7a43-49ec-a390-013237ae8215",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 7,
    "bbox_right": 24,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "af8691c2-991a-4a33-a880-99d07ea369cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7803040d-7a43-49ec-a390-013237ae8215",
            "compositeImage": {
                "id": "58154dbe-de66-42f1-8db7-28c4a30df64b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af8691c2-991a-4a33-a880-99d07ea369cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "610f62c2-f9a8-47e1-9359-fd7ff69258bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af8691c2-991a-4a33-a880-99d07ea369cd",
                    "LayerId": "10b2023a-7573-4ef7-8d49-381637b4d1be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "10b2023a-7573-4ef7-8d49-381637b4d1be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7803040d-7a43-49ec-a390-013237ae8215",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}