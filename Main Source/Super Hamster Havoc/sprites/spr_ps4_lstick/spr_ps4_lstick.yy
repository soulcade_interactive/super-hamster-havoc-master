{
    "id": "245f7f24-06aa-4176-8675-55c3fb5a2207",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ps4_lstick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 5,
    "bbox_right": 94,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea5d4e2e-86d0-4200-bf5d-d0343d3fba44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "245f7f24-06aa-4176-8675-55c3fb5a2207",
            "compositeImage": {
                "id": "1e327d51-3c9a-4305-a179-88e47615ff78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea5d4e2e-86d0-4200-bf5d-d0343d3fba44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9446c14b-3b41-4623-aea7-054bd0bfac29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea5d4e2e-86d0-4200-bf5d-d0343d3fba44",
                    "LayerId": "482c3a7e-2d5a-42f0-b605-6d4c0dc961ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "482c3a7e-2d5a-42f0-b605-6d4c0dc961ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "245f7f24-06aa-4176-8675-55c3fb5a2207",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}