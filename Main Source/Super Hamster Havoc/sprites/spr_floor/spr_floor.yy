{
    "id": "4ebcdca3-4b96-4046-adf8-4c132cadd1bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_floor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9db28fc9-b516-4645-9b85-cadb5522cd6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4ebcdca3-4b96-4046-adf8-4c132cadd1bc",
            "compositeImage": {
                "id": "62c98e3a-5d22-4ea6-a675-f28c7ade6faa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9db28fc9-b516-4645-9b85-cadb5522cd6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0cef2af-3118-44fc-a646-49dfa1986c70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9db28fc9-b516-4645-9b85-cadb5522cd6f",
                    "LayerId": "01c8e082-8524-499e-a35e-4b13d295bee5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "01c8e082-8524-499e-a35e-4b13d295bee5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4ebcdca3-4b96-4046-adf8-4c132cadd1bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}