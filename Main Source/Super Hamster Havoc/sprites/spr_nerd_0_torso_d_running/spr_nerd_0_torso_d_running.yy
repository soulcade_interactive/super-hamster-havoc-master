{
    "id": "a702c99c-5ed6-4a18-bfa6-f26946cbba2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nerd_0_torso_d_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 23,
    "bbox_right": 40,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d39a8ed-ba51-46b8-a68d-cf5fa4429105",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a702c99c-5ed6-4a18-bfa6-f26946cbba2f",
            "compositeImage": {
                "id": "19a12e3e-da65-49d9-a3e9-42968f58161c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d39a8ed-ba51-46b8-a68d-cf5fa4429105",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bca6448b-9b7e-46f4-8626-eff96c54662d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d39a8ed-ba51-46b8-a68d-cf5fa4429105",
                    "LayerId": "b764a311-894d-4939-9727-9afee94bef91"
                }
            ]
        },
        {
            "id": "699a1385-f7f0-4bf8-a8ac-0983da35b117",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a702c99c-5ed6-4a18-bfa6-f26946cbba2f",
            "compositeImage": {
                "id": "3a9abcd6-084f-4e78-bf15-6f24e2b55f92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "699a1385-f7f0-4bf8-a8ac-0983da35b117",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd255659-5abf-4fe4-816f-845df149527f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "699a1385-f7f0-4bf8-a8ac-0983da35b117",
                    "LayerId": "b764a311-894d-4939-9727-9afee94bef91"
                }
            ]
        },
        {
            "id": "f500ebe5-2bb0-42af-bee0-dab395db4f6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a702c99c-5ed6-4a18-bfa6-f26946cbba2f",
            "compositeImage": {
                "id": "358537a3-1e8d-45f5-9142-8a280c2af3eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f500ebe5-2bb0-42af-bee0-dab395db4f6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a291ac81-2a6a-40a8-9469-25e7f3811b47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f500ebe5-2bb0-42af-bee0-dab395db4f6d",
                    "LayerId": "b764a311-894d-4939-9727-9afee94bef91"
                }
            ]
        },
        {
            "id": "e218598f-e708-4a27-8be2-d8f37426cf1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a702c99c-5ed6-4a18-bfa6-f26946cbba2f",
            "compositeImage": {
                "id": "0c180b61-2a95-488b-972b-db724b7b0827",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e218598f-e708-4a27-8be2-d8f37426cf1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edc35d2b-6416-49e8-95c2-8efdbe248fa9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e218598f-e708-4a27-8be2-d8f37426cf1b",
                    "LayerId": "b764a311-894d-4939-9727-9afee94bef91"
                }
            ]
        },
        {
            "id": "abe6ba86-9a39-4158-9dbd-86a5e67e5dd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a702c99c-5ed6-4a18-bfa6-f26946cbba2f",
            "compositeImage": {
                "id": "8e64fb20-9d69-48b6-8f0c-0f612f8706bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abe6ba86-9a39-4158-9dbd-86a5e67e5dd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c3c6f26-d660-42ca-94ef-a7df4948118c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abe6ba86-9a39-4158-9dbd-86a5e67e5dd3",
                    "LayerId": "b764a311-894d-4939-9727-9afee94bef91"
                }
            ]
        },
        {
            "id": "5e2d660c-2cc1-4f6f-910d-7aa255cf4fc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a702c99c-5ed6-4a18-bfa6-f26946cbba2f",
            "compositeImage": {
                "id": "b913c537-a02a-45a6-b315-b8b90df5cd55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e2d660c-2cc1-4f6f-910d-7aa255cf4fc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52392845-05df-4c7f-8a30-7f1d78d0d453",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e2d660c-2cc1-4f6f-910d-7aa255cf4fc2",
                    "LayerId": "b764a311-894d-4939-9727-9afee94bef91"
                }
            ]
        },
        {
            "id": "fc00a09c-db99-4511-b79e-077297169802",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a702c99c-5ed6-4a18-bfa6-f26946cbba2f",
            "compositeImage": {
                "id": "83d518b7-a969-47af-8f32-ca50134047fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc00a09c-db99-4511-b79e-077297169802",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3df6d65-b76f-4c40-b639-d42ecc361b1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc00a09c-db99-4511-b79e-077297169802",
                    "LayerId": "b764a311-894d-4939-9727-9afee94bef91"
                }
            ]
        },
        {
            "id": "457d244d-d256-4b9c-b6e2-96bec8b4f2c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a702c99c-5ed6-4a18-bfa6-f26946cbba2f",
            "compositeImage": {
                "id": "a5102ef9-f0db-4f9f-996e-cb7c4230c342",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "457d244d-d256-4b9c-b6e2-96bec8b4f2c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7f726d3-9bf2-42a8-9686-875e2941bcc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "457d244d-d256-4b9c-b6e2-96bec8b4f2c3",
                    "LayerId": "b764a311-894d-4939-9727-9afee94bef91"
                }
            ]
        },
        {
            "id": "5317cfa8-c526-48ef-8c47-fc8737cdc44d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a702c99c-5ed6-4a18-bfa6-f26946cbba2f",
            "compositeImage": {
                "id": "f20c2814-b4fe-43f0-b248-7c669b08da5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5317cfa8-c526-48ef-8c47-fc8737cdc44d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da230e0b-9b0d-4c6c-9a0a-62e1b0999d64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5317cfa8-c526-48ef-8c47-fc8737cdc44d",
                    "LayerId": "b764a311-894d-4939-9727-9afee94bef91"
                }
            ]
        },
        {
            "id": "b4fe4cb2-4d00-47f9-aafd-1f962e09e11a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a702c99c-5ed6-4a18-bfa6-f26946cbba2f",
            "compositeImage": {
                "id": "c00b51a1-6c42-4bb1-a87b-179a31695a95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4fe4cb2-4d00-47f9-aafd-1f962e09e11a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b1f359f-5c06-46d1-8985-7bce0fe06ecb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4fe4cb2-4d00-47f9-aafd-1f962e09e11a",
                    "LayerId": "b764a311-894d-4939-9727-9afee94bef91"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b764a311-894d-4939-9727-9afee94bef91",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a702c99c-5ed6-4a18-bfa6-f26946cbba2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}