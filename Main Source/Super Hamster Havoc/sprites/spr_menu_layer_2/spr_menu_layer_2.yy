{
    "id": "b45ac0a6-0638-4b32-8fcf-cef2bf8e6200",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_layer_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 779,
    "bbox_left": 0,
    "bbox_right": 1199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c22113d8-e31a-4e09-b010-363cbe1037c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b45ac0a6-0638-4b32-8fcf-cef2bf8e6200",
            "compositeImage": {
                "id": "7de8649a-b14f-4b52-8a3b-c5d9310fdead",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c22113d8-e31a-4e09-b010-363cbe1037c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f49c8d4-be4a-4d1b-8457-c8e8993fe5b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c22113d8-e31a-4e09-b010-363cbe1037c7",
                    "LayerId": "714ac850-971c-46ec-ba2c-5192c12826ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 780,
    "layers": [
        {
            "id": "714ac850-971c-46ec-ba2c-5192c12826ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b45ac0a6-0638-4b32-8fcf-cef2bf8e6200",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1200,
    "xorig": 600,
    "yorig": 390
}