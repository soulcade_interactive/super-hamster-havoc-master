{
    "id": "c3cc4829-9786-45a3-bab3-16cb1b509f75",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flower2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e8775b0-8008-415c-8dc7-ebe7eed24f96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3cc4829-9786-45a3-bab3-16cb1b509f75",
            "compositeImage": {
                "id": "fc1e2e03-a1b7-42c9-b1d5-a6d0716ef667",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e8775b0-8008-415c-8dc7-ebe7eed24f96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47bd2f96-3edf-468e-8117-1e745ecde5d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e8775b0-8008-415c-8dc7-ebe7eed24f96",
                    "LayerId": "ffe9c1c7-b0da-43fb-923a-f40cb434e7be"
                }
            ]
        },
        {
            "id": "2f1674df-32cb-4e86-a79c-40f3408f5d87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3cc4829-9786-45a3-bab3-16cb1b509f75",
            "compositeImage": {
                "id": "b80cabc1-081a-4d79-8de8-59fcd956dfd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f1674df-32cb-4e86-a79c-40f3408f5d87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "033cb0dc-73c6-4b51-8d1a-599fe27e8b37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f1674df-32cb-4e86-a79c-40f3408f5d87",
                    "LayerId": "ffe9c1c7-b0da-43fb-923a-f40cb434e7be"
                }
            ]
        },
        {
            "id": "241e0b28-9846-4738-b470-7f160c346ba3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3cc4829-9786-45a3-bab3-16cb1b509f75",
            "compositeImage": {
                "id": "636b660d-8938-4a49-af1f-64421e769cb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "241e0b28-9846-4738-b470-7f160c346ba3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1b46f3a-837d-4d8d-913e-cb06a80d3448",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "241e0b28-9846-4738-b470-7f160c346ba3",
                    "LayerId": "ffe9c1c7-b0da-43fb-923a-f40cb434e7be"
                }
            ]
        },
        {
            "id": "b245a713-6d7a-4af9-af2e-a1549ca7548b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3cc4829-9786-45a3-bab3-16cb1b509f75",
            "compositeImage": {
                "id": "f6f6eece-bf3a-400b-a5fb-e73a17fafd94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b245a713-6d7a-4af9-af2e-a1549ca7548b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13870fa8-13cb-4fd1-8410-c0013f16fcde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b245a713-6d7a-4af9-af2e-a1549ca7548b",
                    "LayerId": "ffe9c1c7-b0da-43fb-923a-f40cb434e7be"
                }
            ]
        },
        {
            "id": "3185ffdf-de14-4c8e-9802-047a41f406da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3cc4829-9786-45a3-bab3-16cb1b509f75",
            "compositeImage": {
                "id": "f6da0658-3da0-4b7a-9d99-d6214dec4f93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3185ffdf-de14-4c8e-9802-047a41f406da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96999049-06c4-4c23-aa2c-689ffe0da6d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3185ffdf-de14-4c8e-9802-047a41f406da",
                    "LayerId": "ffe9c1c7-b0da-43fb-923a-f40cb434e7be"
                }
            ]
        },
        {
            "id": "543093d4-9252-4672-9271-9316e2f1b9ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3cc4829-9786-45a3-bab3-16cb1b509f75",
            "compositeImage": {
                "id": "c48be055-b4c5-403b-95ff-61b72c89c32b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "543093d4-9252-4672-9271-9316e2f1b9ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "528e2531-a8fc-4ab6-857f-ecbe28c48917",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "543093d4-9252-4672-9271-9316e2f1b9ea",
                    "LayerId": "ffe9c1c7-b0da-43fb-923a-f40cb434e7be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "ffe9c1c7-b0da-43fb-923a-f40cb434e7be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3cc4829-9786-45a3-bab3-16cb1b509f75",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}