{
    "id": "281c18e9-b753-4e24-b376-883cd09845d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ps4_triangle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 9,
    "bbox_right": 90,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90079bd6-1986-4624-b6ea-025b08739851",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "281c18e9-b753-4e24-b376-883cd09845d9",
            "compositeImage": {
                "id": "c993d83f-1ffd-4292-8306-8682f318dfe6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90079bd6-1986-4624-b6ea-025b08739851",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5debf73a-5d2b-4032-ab09-d0f77692c039",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90079bd6-1986-4624-b6ea-025b08739851",
                    "LayerId": "617ddc44-2633-4bf4-a55d-8bb4b8b0aa72"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "617ddc44-2633-4bf4-a55d-8bb4b8b0aa72",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "281c18e9-b753-4e24-b376-883cd09845d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}