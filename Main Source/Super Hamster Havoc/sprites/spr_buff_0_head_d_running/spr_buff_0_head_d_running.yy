{
    "id": "afb41163-8010-4099-a8da-d294b3adb081",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buff_0_head_d_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 21,
    "bbox_right": 42,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51b30765-763c-4847-baf6-b5a1ba25e115",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afb41163-8010-4099-a8da-d294b3adb081",
            "compositeImage": {
                "id": "629bc980-9081-4f44-97d7-093b41201ff2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51b30765-763c-4847-baf6-b5a1ba25e115",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1f7a7a0-6e0d-48e1-b698-a62f50e4f5dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51b30765-763c-4847-baf6-b5a1ba25e115",
                    "LayerId": "5406d254-f5ec-44e4-99f5-2f40f76e684b"
                }
            ]
        },
        {
            "id": "1081b9b3-3141-4ef1-b3ef-6b4fb04b07ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afb41163-8010-4099-a8da-d294b3adb081",
            "compositeImage": {
                "id": "864632a9-10e8-4e36-b510-7f9d29a1b7ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1081b9b3-3141-4ef1-b3ef-6b4fb04b07ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f88a106-e49f-42b1-b5c3-2f1bb50e5191",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1081b9b3-3141-4ef1-b3ef-6b4fb04b07ac",
                    "LayerId": "5406d254-f5ec-44e4-99f5-2f40f76e684b"
                }
            ]
        },
        {
            "id": "ef94d4cd-fede-4c10-9f6c-98ac80c5725d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afb41163-8010-4099-a8da-d294b3adb081",
            "compositeImage": {
                "id": "78fee3c2-4dde-4022-8d41-6a847076b3fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef94d4cd-fede-4c10-9f6c-98ac80c5725d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64dca958-810a-470a-b76f-af31f3f06435",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef94d4cd-fede-4c10-9f6c-98ac80c5725d",
                    "LayerId": "5406d254-f5ec-44e4-99f5-2f40f76e684b"
                }
            ]
        },
        {
            "id": "6748fe72-72a6-451a-ba3b-0d174ee1d59e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afb41163-8010-4099-a8da-d294b3adb081",
            "compositeImage": {
                "id": "036aee62-b761-4476-83b4-e8099c83cabc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6748fe72-72a6-451a-ba3b-0d174ee1d59e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2c1086b-1474-4b9b-b1f8-aa68ae6e3428",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6748fe72-72a6-451a-ba3b-0d174ee1d59e",
                    "LayerId": "5406d254-f5ec-44e4-99f5-2f40f76e684b"
                }
            ]
        },
        {
            "id": "9497ea73-99ef-4762-a1b5-c08a5651ae87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afb41163-8010-4099-a8da-d294b3adb081",
            "compositeImage": {
                "id": "4f8cc1c8-5abe-4a7b-a83a-16a228885c59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9497ea73-99ef-4762-a1b5-c08a5651ae87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55e8d6d8-4490-4006-9274-1cc98de84734",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9497ea73-99ef-4762-a1b5-c08a5651ae87",
                    "LayerId": "5406d254-f5ec-44e4-99f5-2f40f76e684b"
                }
            ]
        },
        {
            "id": "7cbc3118-3862-46ac-9e09-f3643deab7d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afb41163-8010-4099-a8da-d294b3adb081",
            "compositeImage": {
                "id": "e5b042fe-15d6-42a6-8254-f2a054fb2b5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cbc3118-3862-46ac-9e09-f3643deab7d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "722f5ef2-f5eb-40bf-886b-68d16cbacba5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cbc3118-3862-46ac-9e09-f3643deab7d4",
                    "LayerId": "5406d254-f5ec-44e4-99f5-2f40f76e684b"
                }
            ]
        },
        {
            "id": "d45588d2-8da0-481b-9fc7-8c1519fcf254",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afb41163-8010-4099-a8da-d294b3adb081",
            "compositeImage": {
                "id": "e2249e8e-1ed5-48de-83d4-caa136c0e43a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d45588d2-8da0-481b-9fc7-8c1519fcf254",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e756c70e-4c59-4048-85fe-b4a7801f9cb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d45588d2-8da0-481b-9fc7-8c1519fcf254",
                    "LayerId": "5406d254-f5ec-44e4-99f5-2f40f76e684b"
                }
            ]
        },
        {
            "id": "38223001-0f02-4829-bae1-93f296fc1274",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afb41163-8010-4099-a8da-d294b3adb081",
            "compositeImage": {
                "id": "0a75a5ca-d8e0-4461-b51e-12bb6de985f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38223001-0f02-4829-bae1-93f296fc1274",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd7073a6-fae0-4e3b-9abe-9921561ba7a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38223001-0f02-4829-bae1-93f296fc1274",
                    "LayerId": "5406d254-f5ec-44e4-99f5-2f40f76e684b"
                }
            ]
        },
        {
            "id": "77ec8cfa-0451-4d9b-9d47-3eef6b824da0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afb41163-8010-4099-a8da-d294b3adb081",
            "compositeImage": {
                "id": "41b45684-cc3c-48a8-8b53-6b681d4f2b94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77ec8cfa-0451-4d9b-9d47-3eef6b824da0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b21f7ba7-aedd-42c2-a569-57e6468a8a44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77ec8cfa-0451-4d9b-9d47-3eef6b824da0",
                    "LayerId": "5406d254-f5ec-44e4-99f5-2f40f76e684b"
                }
            ]
        },
        {
            "id": "d46c2664-18a7-4b13-b03d-5d668f724850",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afb41163-8010-4099-a8da-d294b3adb081",
            "compositeImage": {
                "id": "4030192d-a128-4eca-adaa-8fbae4a8104b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d46c2664-18a7-4b13-b03d-5d668f724850",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aed8e306-e482-4b7f-b24f-b3fc8c6cb5e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d46c2664-18a7-4b13-b03d-5d668f724850",
                    "LayerId": "5406d254-f5ec-44e4-99f5-2f40f76e684b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5406d254-f5ec-44e4-99f5-2f40f76e684b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "afb41163-8010-4099-a8da-d294b3adb081",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}