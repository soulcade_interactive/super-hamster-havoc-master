{
    "id": "c74c9218-e8ce-4e9f-8746-85f0a3345298",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bush2s1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 3,
    "bbox_right": 60,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d5d25858-2e01-401e-ae50-f23e048903e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c74c9218-e8ce-4e9f-8746-85f0a3345298",
            "compositeImage": {
                "id": "ea0030c5-7e57-4869-ab73-1aea3ce22235",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5d25858-2e01-401e-ae50-f23e048903e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "101725a2-1085-497e-919f-14e83238e23c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5d25858-2e01-401e-ae50-f23e048903e2",
                    "LayerId": "d93e5d33-fd0f-437f-8256-2cc0499236ed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d93e5d33-fd0f-437f-8256-2cc0499236ed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c74c9218-e8ce-4e9f-8746-85f0a3345298",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}