{
    "id": "a52885ab-351a-431b-9c9c-0ffccc4b7af6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_0_legs_r_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 19,
    "bbox_right": 41,
    "bbox_top": 55,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "393802fd-a919-46c9-863b-fbbf3b55c48c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a52885ab-351a-431b-9c9c-0ffccc4b7af6",
            "compositeImage": {
                "id": "671a7136-9993-4755-9bcf-73821e8cbf02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "393802fd-a919-46c9-863b-fbbf3b55c48c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bca69d5-fbd8-4422-ba4e-48d70c6d3db6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "393802fd-a919-46c9-863b-fbbf3b55c48c",
                    "LayerId": "37fd7d3e-afb0-4068-8aa8-08dea04b02d9"
                }
            ]
        },
        {
            "id": "3acd4a43-3f02-4f4e-a6eb-261f5a3d1ba0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a52885ab-351a-431b-9c9c-0ffccc4b7af6",
            "compositeImage": {
                "id": "9a7e8a26-ff84-4d67-a1a0-4dd34480af03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3acd4a43-3f02-4f4e-a6eb-261f5a3d1ba0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a50b8cc0-84b6-499a-bff4-3eb465785b88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3acd4a43-3f02-4f4e-a6eb-261f5a3d1ba0",
                    "LayerId": "37fd7d3e-afb0-4068-8aa8-08dea04b02d9"
                }
            ]
        },
        {
            "id": "5210d94a-c2fb-4e96-8df4-f2196a669c60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a52885ab-351a-431b-9c9c-0ffccc4b7af6",
            "compositeImage": {
                "id": "592218b4-1626-4f47-8320-3516ca0ca362",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5210d94a-c2fb-4e96-8df4-f2196a669c60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8ff6b9f-8b5c-4edf-835c-591e105993f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5210d94a-c2fb-4e96-8df4-f2196a669c60",
                    "LayerId": "37fd7d3e-afb0-4068-8aa8-08dea04b02d9"
                }
            ]
        },
        {
            "id": "5b5ac225-a07e-4dbc-9e4d-aae88a049c49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a52885ab-351a-431b-9c9c-0ffccc4b7af6",
            "compositeImage": {
                "id": "65021f4a-260e-4add-a2de-79ebe8fcc22c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b5ac225-a07e-4dbc-9e4d-aae88a049c49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac4c9d99-208f-4b79-836b-efdc338dadc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b5ac225-a07e-4dbc-9e4d-aae88a049c49",
                    "LayerId": "37fd7d3e-afb0-4068-8aa8-08dea04b02d9"
                }
            ]
        },
        {
            "id": "006c0f1a-e79c-4cd6-91d7-10ca41145272",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a52885ab-351a-431b-9c9c-0ffccc4b7af6",
            "compositeImage": {
                "id": "be74f1a1-136a-4f6c-a794-9bdfc1e07924",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "006c0f1a-e79c-4cd6-91d7-10ca41145272",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c8d21f7-436d-43ec-ad29-0b139a311c52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "006c0f1a-e79c-4cd6-91d7-10ca41145272",
                    "LayerId": "37fd7d3e-afb0-4068-8aa8-08dea04b02d9"
                }
            ]
        },
        {
            "id": "f81b7676-912d-4b45-b0f3-c5c68c124d7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a52885ab-351a-431b-9c9c-0ffccc4b7af6",
            "compositeImage": {
                "id": "bd78aae9-8f6c-4247-822c-cca22e3c1786",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f81b7676-912d-4b45-b0f3-c5c68c124d7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bc7284f-dac8-4682-b8eb-308ffff63a75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f81b7676-912d-4b45-b0f3-c5c68c124d7c",
                    "LayerId": "37fd7d3e-afb0-4068-8aa8-08dea04b02d9"
                }
            ]
        },
        {
            "id": "2e05c693-86e1-4e3d-aa4d-03ccb5f55c9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a52885ab-351a-431b-9c9c-0ffccc4b7af6",
            "compositeImage": {
                "id": "87eab6ef-e83b-42c4-9575-b40b2e5c01b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e05c693-86e1-4e3d-aa4d-03ccb5f55c9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fed3912-74ec-44b1-85f3-8a82af3cf449",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e05c693-86e1-4e3d-aa4d-03ccb5f55c9e",
                    "LayerId": "37fd7d3e-afb0-4068-8aa8-08dea04b02d9"
                }
            ]
        },
        {
            "id": "4cb47d32-cf38-41eb-871d-9840e93e01b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a52885ab-351a-431b-9c9c-0ffccc4b7af6",
            "compositeImage": {
                "id": "13b9288a-33d4-4708-8bba-caabef1ecabe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cb47d32-cf38-41eb-871d-9840e93e01b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f8abb77-9292-4cab-8158-a1bcad1da99a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cb47d32-cf38-41eb-871d-9840e93e01b2",
                    "LayerId": "37fd7d3e-afb0-4068-8aa8-08dea04b02d9"
                }
            ]
        },
        {
            "id": "7e4ae5c9-cdd7-4b40-b79a-50128ac08b52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a52885ab-351a-431b-9c9c-0ffccc4b7af6",
            "compositeImage": {
                "id": "28ae2caa-70e5-4667-8a3f-078fa6bfeb21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e4ae5c9-cdd7-4b40-b79a-50128ac08b52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c169af2-096e-4dac-8b38-d56183a5fc79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e4ae5c9-cdd7-4b40-b79a-50128ac08b52",
                    "LayerId": "37fd7d3e-afb0-4068-8aa8-08dea04b02d9"
                }
            ]
        },
        {
            "id": "9a0e938b-626a-4efc-bbcb-915f25ae826e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a52885ab-351a-431b-9c9c-0ffccc4b7af6",
            "compositeImage": {
                "id": "e5ee8271-5782-475b-96ee-953392c82a9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a0e938b-626a-4efc-bbcb-915f25ae826e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e03c3ed5-970e-4d16-97b5-f1800460d6db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a0e938b-626a-4efc-bbcb-915f25ae826e",
                    "LayerId": "37fd7d3e-afb0-4068-8aa8-08dea04b02d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "37fd7d3e-afb0-4068-8aa8-08dea04b02d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a52885ab-351a-431b-9c9c-0ffccc4b7af6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}