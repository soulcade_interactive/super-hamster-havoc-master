{
    "id": "8187baac-25ee-44ee-82a9-3443f053e115",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_0_torso_u_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 25,
    "bbox_right": 38,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f551122c-223f-4d6e-8782-398dc45961af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8187baac-25ee-44ee-82a9-3443f053e115",
            "compositeImage": {
                "id": "ae77199e-e194-4deb-aec8-e96ad728e3e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f551122c-223f-4d6e-8782-398dc45961af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b91b3f7-8488-4eeb-b737-16170ba57dfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f551122c-223f-4d6e-8782-398dc45961af",
                    "LayerId": "be7d345b-b0cb-479f-95f3-43f637f31e87"
                }
            ]
        },
        {
            "id": "ee229f29-7881-4ef6-9a7f-4968e7ef9f92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8187baac-25ee-44ee-82a9-3443f053e115",
            "compositeImage": {
                "id": "5dd1850c-225b-40d3-8759-5f78e27f16e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee229f29-7881-4ef6-9a7f-4968e7ef9f92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b898e0a3-49eb-4b88-b89d-06f272a05f68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee229f29-7881-4ef6-9a7f-4968e7ef9f92",
                    "LayerId": "be7d345b-b0cb-479f-95f3-43f637f31e87"
                }
            ]
        },
        {
            "id": "6152de0a-5614-4960-bb6a-923b60924a77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8187baac-25ee-44ee-82a9-3443f053e115",
            "compositeImage": {
                "id": "e8afa006-cb6f-4c83-9d4e-378a990e71bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6152de0a-5614-4960-bb6a-923b60924a77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3d07d3f-00f6-4135-81b7-833053e13dfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6152de0a-5614-4960-bb6a-923b60924a77",
                    "LayerId": "be7d345b-b0cb-479f-95f3-43f637f31e87"
                }
            ]
        },
        {
            "id": "56bf7381-6eb0-4259-81de-6736dcf38d10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8187baac-25ee-44ee-82a9-3443f053e115",
            "compositeImage": {
                "id": "8dd4904d-9501-401e-95f6-282f26b8bd29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56bf7381-6eb0-4259-81de-6736dcf38d10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55278fb3-9529-4d2d-9075-88c102d39396",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56bf7381-6eb0-4259-81de-6736dcf38d10",
                    "LayerId": "be7d345b-b0cb-479f-95f3-43f637f31e87"
                }
            ]
        },
        {
            "id": "ff24e491-8e1c-450f-9e67-450368d5dee2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8187baac-25ee-44ee-82a9-3443f053e115",
            "compositeImage": {
                "id": "81007f95-c19d-47ae-9534-7eda7abf194c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff24e491-8e1c-450f-9e67-450368d5dee2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "154f3cb8-462a-442e-9da2-3ab6dd82cf1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff24e491-8e1c-450f-9e67-450368d5dee2",
                    "LayerId": "be7d345b-b0cb-479f-95f3-43f637f31e87"
                }
            ]
        },
        {
            "id": "24e7c1c1-1602-4d9f-9284-11e94d9d165b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8187baac-25ee-44ee-82a9-3443f053e115",
            "compositeImage": {
                "id": "48c598da-8918-426d-a4c4-f34e6860e38e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24e7c1c1-1602-4d9f-9284-11e94d9d165b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8998bd1e-4d87-4c86-b1a4-ef45b3b54aa3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24e7c1c1-1602-4d9f-9284-11e94d9d165b",
                    "LayerId": "be7d345b-b0cb-479f-95f3-43f637f31e87"
                }
            ]
        },
        {
            "id": "db1bc510-1d4f-4b4b-9e59-cd3b1ac962e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8187baac-25ee-44ee-82a9-3443f053e115",
            "compositeImage": {
                "id": "66e4bd98-dd87-4b62-b417-1d410dafcc8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db1bc510-1d4f-4b4b-9e59-cd3b1ac962e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58ef55ae-17f4-46e0-9747-d18ca2c158a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db1bc510-1d4f-4b4b-9e59-cd3b1ac962e5",
                    "LayerId": "be7d345b-b0cb-479f-95f3-43f637f31e87"
                }
            ]
        },
        {
            "id": "2e91e6c9-8c1e-4d5c-aefe-99fec27c2db7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8187baac-25ee-44ee-82a9-3443f053e115",
            "compositeImage": {
                "id": "e0d86694-a3d7-4cc9-a7ce-56db1baa4193",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e91e6c9-8c1e-4d5c-aefe-99fec27c2db7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6223d0a4-178c-4ea6-857b-84ffba96c160",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e91e6c9-8c1e-4d5c-aefe-99fec27c2db7",
                    "LayerId": "be7d345b-b0cb-479f-95f3-43f637f31e87"
                }
            ]
        },
        {
            "id": "64a24861-87f2-4f08-88ed-8cbbc656eb58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8187baac-25ee-44ee-82a9-3443f053e115",
            "compositeImage": {
                "id": "aad4eb67-7114-4bee-a14b-25be6c38fa3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64a24861-87f2-4f08-88ed-8cbbc656eb58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e92657c-b909-4319-964a-5bc1592945a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64a24861-87f2-4f08-88ed-8cbbc656eb58",
                    "LayerId": "be7d345b-b0cb-479f-95f3-43f637f31e87"
                }
            ]
        },
        {
            "id": "b9e4a51a-4697-4cc4-bddc-979768128517",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8187baac-25ee-44ee-82a9-3443f053e115",
            "compositeImage": {
                "id": "e819816d-c230-4531-b0e6-33f70bc45892",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9e4a51a-4697-4cc4-bddc-979768128517",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3ef0522-1bc6-492d-9fb7-4670dd7cd854",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9e4a51a-4697-4cc4-bddc-979768128517",
                    "LayerId": "be7d345b-b0cb-479f-95f3-43f637f31e87"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "be7d345b-b0cb-479f-95f3-43f637f31e87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8187baac-25ee-44ee-82a9-3443f053e115",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}