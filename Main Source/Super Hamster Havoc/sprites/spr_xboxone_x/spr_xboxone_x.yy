{
    "id": "371090db-11ed-4359-8310-2436a4391960",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_xboxone_x",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 9,
    "bbox_right": 90,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "311786b8-ef9b-4be5-bc75-4763b3fc2e9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "371090db-11ed-4359-8310-2436a4391960",
            "compositeImage": {
                "id": "0ce4b292-241e-41bc-a040-ca39a54d497e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "311786b8-ef9b-4be5-bc75-4763b3fc2e9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2c7c054-6031-4f7f-b878-0496886760ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "311786b8-ef9b-4be5-bc75-4763b3fc2e9f",
                    "LayerId": "639d5523-6676-4f37-9b95-b9af85939673"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "639d5523-6676-4f37-9b95-b9af85939673",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "371090db-11ed-4359-8310-2436a4391960",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}