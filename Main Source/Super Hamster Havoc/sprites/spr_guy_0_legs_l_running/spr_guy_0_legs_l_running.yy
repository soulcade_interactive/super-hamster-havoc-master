{
    "id": "846f8b8e-3501-47f6-9498-936e47380cf4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_0_legs_l_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 44,
    "bbox_top": 55,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8cea8f43-a398-499f-93ae-d0ac8f5d7f8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "846f8b8e-3501-47f6-9498-936e47380cf4",
            "compositeImage": {
                "id": "353e264d-5fa6-44df-83c8-5299cc203028",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cea8f43-a398-499f-93ae-d0ac8f5d7f8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99a6b958-1f0c-4122-8574-6d4faf39bc9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cea8f43-a398-499f-93ae-d0ac8f5d7f8f",
                    "LayerId": "d83ec644-3f10-489b-bbfb-3f48ac221d33"
                }
            ]
        },
        {
            "id": "adb64363-adc6-4442-89cf-9470d9a03f45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "846f8b8e-3501-47f6-9498-936e47380cf4",
            "compositeImage": {
                "id": "c42d844c-101c-46db-9718-f6215ee2fa90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adb64363-adc6-4442-89cf-9470d9a03f45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "444b7e00-fe30-4907-8df9-1039a5bb28f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adb64363-adc6-4442-89cf-9470d9a03f45",
                    "LayerId": "d83ec644-3f10-489b-bbfb-3f48ac221d33"
                }
            ]
        },
        {
            "id": "ae5c8beb-4b2d-413e-bf04-0db379dc1b78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "846f8b8e-3501-47f6-9498-936e47380cf4",
            "compositeImage": {
                "id": "a027e960-b7bb-4f5b-956e-72c2227b2098",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae5c8beb-4b2d-413e-bf04-0db379dc1b78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea854657-c6bd-4158-985a-4b17b8f06277",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae5c8beb-4b2d-413e-bf04-0db379dc1b78",
                    "LayerId": "d83ec644-3f10-489b-bbfb-3f48ac221d33"
                }
            ]
        },
        {
            "id": "d1435348-780d-4256-8288-234bbd1210a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "846f8b8e-3501-47f6-9498-936e47380cf4",
            "compositeImage": {
                "id": "e580070b-99e8-481b-a6da-edb4546d775d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1435348-780d-4256-8288-234bbd1210a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e00a84c-6248-47f5-98af-01475f73e9b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1435348-780d-4256-8288-234bbd1210a4",
                    "LayerId": "d83ec644-3f10-489b-bbfb-3f48ac221d33"
                }
            ]
        },
        {
            "id": "5ddaeabc-a841-4aa3-8ca7-249fabdbd56e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "846f8b8e-3501-47f6-9498-936e47380cf4",
            "compositeImage": {
                "id": "0083168e-2493-46b3-bf74-d27063c596f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ddaeabc-a841-4aa3-8ca7-249fabdbd56e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "496bf538-9b7a-4699-9fa1-3d1d408fc582",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ddaeabc-a841-4aa3-8ca7-249fabdbd56e",
                    "LayerId": "d83ec644-3f10-489b-bbfb-3f48ac221d33"
                }
            ]
        },
        {
            "id": "a40196bb-f77f-4e0f-b5a3-d6e8fbbab293",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "846f8b8e-3501-47f6-9498-936e47380cf4",
            "compositeImage": {
                "id": "0edd3834-be0b-4398-a657-9a5b23cf9997",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a40196bb-f77f-4e0f-b5a3-d6e8fbbab293",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58889e71-4029-42f2-b912-9be83313a354",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a40196bb-f77f-4e0f-b5a3-d6e8fbbab293",
                    "LayerId": "d83ec644-3f10-489b-bbfb-3f48ac221d33"
                }
            ]
        },
        {
            "id": "e03c8e25-0f24-4ff2-b41d-48100d1859c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "846f8b8e-3501-47f6-9498-936e47380cf4",
            "compositeImage": {
                "id": "94f7f021-e436-4430-9a86-e8cece93c8e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e03c8e25-0f24-4ff2-b41d-48100d1859c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44a6c09b-df61-4717-98b0-1cff7811a6d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e03c8e25-0f24-4ff2-b41d-48100d1859c4",
                    "LayerId": "d83ec644-3f10-489b-bbfb-3f48ac221d33"
                }
            ]
        },
        {
            "id": "9cdd1298-45fc-4c6b-8d77-4761ce9aae98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "846f8b8e-3501-47f6-9498-936e47380cf4",
            "compositeImage": {
                "id": "5bb854bf-ae05-4a17-b0b9-71cf05e51cb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cdd1298-45fc-4c6b-8d77-4761ce9aae98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6084b679-ea69-415f-bea7-fcc1d488a7d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cdd1298-45fc-4c6b-8d77-4761ce9aae98",
                    "LayerId": "d83ec644-3f10-489b-bbfb-3f48ac221d33"
                }
            ]
        },
        {
            "id": "b93115e5-1a22-4df6-9c18-0a4d75e45710",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "846f8b8e-3501-47f6-9498-936e47380cf4",
            "compositeImage": {
                "id": "1fc89f3a-3b3b-46bd-ab09-88e9cae9051e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b93115e5-1a22-4df6-9c18-0a4d75e45710",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "017ed68d-dc53-4ce6-b5c4-7b7ad4588068",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b93115e5-1a22-4df6-9c18-0a4d75e45710",
                    "LayerId": "d83ec644-3f10-489b-bbfb-3f48ac221d33"
                }
            ]
        },
        {
            "id": "d6f1083f-4593-4715-a226-4267f92cdd3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "846f8b8e-3501-47f6-9498-936e47380cf4",
            "compositeImage": {
                "id": "2749bf71-fb4f-4c31-92c9-1c2beeba7764",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6f1083f-4593-4715-a226-4267f92cdd3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5c5bce5-5bed-4e1c-9848-cd25cf861cf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6f1083f-4593-4715-a226-4267f92cdd3b",
                    "LayerId": "d83ec644-3f10-489b-bbfb-3f48ac221d33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d83ec644-3f10-489b-bbfb-3f48ac221d33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "846f8b8e-3501-47f6-9498-936e47380cf4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}