{
    "id": "18e417f4-cbc9-41b0-b546-534848289d44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_0_legs_u_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 25,
    "bbox_right": 38,
    "bbox_top": 56,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89ba275d-46b6-41ec-82c9-6d592619f8ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18e417f4-cbc9-41b0-b546-534848289d44",
            "compositeImage": {
                "id": "693471e3-c906-4e70-b4a6-7c31978604c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89ba275d-46b6-41ec-82c9-6d592619f8ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a03abb83-1d06-4be4-9702-ed1510732f87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89ba275d-46b6-41ec-82c9-6d592619f8ff",
                    "LayerId": "70c02ed7-7d93-460e-95e2-164af086e1d0"
                }
            ]
        },
        {
            "id": "77353cb4-d63e-419c-a33f-75d394c3b35d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18e417f4-cbc9-41b0-b546-534848289d44",
            "compositeImage": {
                "id": "2a169e33-6db9-4ab0-965f-d0c93758f068",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77353cb4-d63e-419c-a33f-75d394c3b35d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91e9297a-eea4-4e11-b56f-0c4ae6391330",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77353cb4-d63e-419c-a33f-75d394c3b35d",
                    "LayerId": "70c02ed7-7d93-460e-95e2-164af086e1d0"
                }
            ]
        },
        {
            "id": "2416ac86-9bbc-4eb4-b903-8b90eeb04f32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18e417f4-cbc9-41b0-b546-534848289d44",
            "compositeImage": {
                "id": "ff382cd4-89fa-40a8-aed7-ba5ad8dc9f2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2416ac86-9bbc-4eb4-b903-8b90eeb04f32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e0a07c6-d012-4f45-9180-138e710eda7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2416ac86-9bbc-4eb4-b903-8b90eeb04f32",
                    "LayerId": "70c02ed7-7d93-460e-95e2-164af086e1d0"
                }
            ]
        },
        {
            "id": "e7aad9df-7227-494c-81c2-e9b22a6db204",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18e417f4-cbc9-41b0-b546-534848289d44",
            "compositeImage": {
                "id": "4aef90dd-b42d-4091-ad20-ef828e6bb829",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7aad9df-7227-494c-81c2-e9b22a6db204",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95c206e0-80b0-42af-a4d7-5fc1bc8ee22d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7aad9df-7227-494c-81c2-e9b22a6db204",
                    "LayerId": "70c02ed7-7d93-460e-95e2-164af086e1d0"
                }
            ]
        },
        {
            "id": "b3e3f54b-dbdf-4fa0-84ea-1f7acbab746b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18e417f4-cbc9-41b0-b546-534848289d44",
            "compositeImage": {
                "id": "fbee60d2-fc0f-4f35-baef-344bf6c92e73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3e3f54b-dbdf-4fa0-84ea-1f7acbab746b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6242c121-0f15-4003-9efc-c870ace761bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3e3f54b-dbdf-4fa0-84ea-1f7acbab746b",
                    "LayerId": "70c02ed7-7d93-460e-95e2-164af086e1d0"
                }
            ]
        },
        {
            "id": "606dcee1-73b5-488b-83db-79a1869f74a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18e417f4-cbc9-41b0-b546-534848289d44",
            "compositeImage": {
                "id": "7305c1ca-31cb-4ca3-983e-3d4d408d92ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "606dcee1-73b5-488b-83db-79a1869f74a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3b89e87-5db8-40f9-9085-1157bc9a8313",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "606dcee1-73b5-488b-83db-79a1869f74a1",
                    "LayerId": "70c02ed7-7d93-460e-95e2-164af086e1d0"
                }
            ]
        },
        {
            "id": "27a2d6cf-8def-49c8-b59a-27ef9d69e177",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18e417f4-cbc9-41b0-b546-534848289d44",
            "compositeImage": {
                "id": "79446208-f705-47aa-b73d-b21ba9a7bb98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27a2d6cf-8def-49c8-b59a-27ef9d69e177",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5c8f5cf-eeb8-4e46-9c5c-9a85a7cf8ca9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27a2d6cf-8def-49c8-b59a-27ef9d69e177",
                    "LayerId": "70c02ed7-7d93-460e-95e2-164af086e1d0"
                }
            ]
        },
        {
            "id": "c7020ad7-ce42-4972-a95c-8b898251d554",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18e417f4-cbc9-41b0-b546-534848289d44",
            "compositeImage": {
                "id": "6315f7aa-1b79-4531-8609-cb011fb8e668",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7020ad7-ce42-4972-a95c-8b898251d554",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c29e06a3-f0c4-4d30-ba81-3945278d16e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7020ad7-ce42-4972-a95c-8b898251d554",
                    "LayerId": "70c02ed7-7d93-460e-95e2-164af086e1d0"
                }
            ]
        },
        {
            "id": "03f3a0a2-2121-42fc-a05c-e6183cb85b48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18e417f4-cbc9-41b0-b546-534848289d44",
            "compositeImage": {
                "id": "a8096c16-2b6d-4a85-a5ba-45c888f44970",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03f3a0a2-2121-42fc-a05c-e6183cb85b48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84bb0942-73ea-4ace-9c34-d16956a05b53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03f3a0a2-2121-42fc-a05c-e6183cb85b48",
                    "LayerId": "70c02ed7-7d93-460e-95e2-164af086e1d0"
                }
            ]
        },
        {
            "id": "9cd27737-04f7-4457-95be-d0a410937f81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18e417f4-cbc9-41b0-b546-534848289d44",
            "compositeImage": {
                "id": "067775b4-0126-46e2-8c29-dfdced8d7490",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cd27737-04f7-4457-95be-d0a410937f81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ad5db25-65d7-48f0-bff4-18afc0c2aa45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cd27737-04f7-4457-95be-d0a410937f81",
                    "LayerId": "70c02ed7-7d93-460e-95e2-164af086e1d0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "70c02ed7-7d93-460e-95e2-164af086e1d0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "18e417f4-cbc9-41b0-b546-534848289d44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}