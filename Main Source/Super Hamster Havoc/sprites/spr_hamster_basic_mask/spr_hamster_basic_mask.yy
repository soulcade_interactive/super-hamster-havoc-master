{
    "id": "5aea9789-92b2-426a-95b4-012fee3d5a22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_basic_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 20,
    "bbox_right": 43,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3941e59-4752-444f-b0df-0cffe6133b2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5aea9789-92b2-426a-95b4-012fee3d5a22",
            "compositeImage": {
                "id": "ad394aa3-6ab2-4ee1-9bd1-262f3c11c4f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3941e59-4752-444f-b0df-0cffe6133b2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73c1a24a-f876-4345-9ac0-ef3d2cc343be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3941e59-4752-444f-b0df-0cffe6133b2a",
                    "LayerId": "fc9ec857-8e10-49ff-8036-9cd9ad60eb78"
                }
            ]
        }
    ],
    "gridX": 8,
    "gridY": 8,
    "height": 64,
    "layers": [
        {
            "id": "fc9ec857-8e10-49ff-8036-9cd9ad60eb78",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5aea9789-92b2-426a-95b4-012fee3d5a22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}