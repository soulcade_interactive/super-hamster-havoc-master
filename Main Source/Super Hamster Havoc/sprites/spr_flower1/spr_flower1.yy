{
    "id": "6797ece8-3485-49d4-af2f-f985947fa9e9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flower1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d697cc4-3a77-44d7-bda8-47d05a4000f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6797ece8-3485-49d4-af2f-f985947fa9e9",
            "compositeImage": {
                "id": "ffa06629-aef2-4db5-95fe-2b3a5de2bf8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d697cc4-3a77-44d7-bda8-47d05a4000f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f82e11aa-72cf-46dd-8695-33e5580f8dda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d697cc4-3a77-44d7-bda8-47d05a4000f5",
                    "LayerId": "d6e18e07-bbd5-41af-922c-0f7ff1a39acd"
                }
            ]
        },
        {
            "id": "ded7faa8-8851-4fa1-b633-97363aae5f58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6797ece8-3485-49d4-af2f-f985947fa9e9",
            "compositeImage": {
                "id": "05e52417-b8c4-406f-bf54-5599e70b148b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ded7faa8-8851-4fa1-b633-97363aae5f58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6f3badb-10ec-45d2-9b24-21e5c7179664",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ded7faa8-8851-4fa1-b633-97363aae5f58",
                    "LayerId": "d6e18e07-bbd5-41af-922c-0f7ff1a39acd"
                }
            ]
        },
        {
            "id": "748f1f0c-8547-4d59-9346-bfa6ed3cd116",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6797ece8-3485-49d4-af2f-f985947fa9e9",
            "compositeImage": {
                "id": "288bc340-b39a-4906-96fc-0baae078b7cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "748f1f0c-8547-4d59-9346-bfa6ed3cd116",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "952eedaa-4abf-485b-ab5f-bfb8e75b8f83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "748f1f0c-8547-4d59-9346-bfa6ed3cd116",
                    "LayerId": "d6e18e07-bbd5-41af-922c-0f7ff1a39acd"
                }
            ]
        },
        {
            "id": "c75bb46a-11c0-472f-ba4b-a2af87a6f08e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6797ece8-3485-49d4-af2f-f985947fa9e9",
            "compositeImage": {
                "id": "01f84b32-c544-4976-934a-a98fdd3a724a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c75bb46a-11c0-472f-ba4b-a2af87a6f08e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0868916-7edd-416e-b3b3-0959de6e5c47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c75bb46a-11c0-472f-ba4b-a2af87a6f08e",
                    "LayerId": "d6e18e07-bbd5-41af-922c-0f7ff1a39acd"
                }
            ]
        },
        {
            "id": "0ceb750f-dba5-4c3a-9a56-8619739bbc6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6797ece8-3485-49d4-af2f-f985947fa9e9",
            "compositeImage": {
                "id": "05e614f4-c48d-4f80-8ec4-697856fd3d23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ceb750f-dba5-4c3a-9a56-8619739bbc6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d102611-12e8-4f2e-8b4c-528e738f6866",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ceb750f-dba5-4c3a-9a56-8619739bbc6f",
                    "LayerId": "d6e18e07-bbd5-41af-922c-0f7ff1a39acd"
                }
            ]
        },
        {
            "id": "c71ee1ec-38aa-4b0a-aebc-6d68f47d0002",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6797ece8-3485-49d4-af2f-f985947fa9e9",
            "compositeImage": {
                "id": "8c1cf55d-d930-4c57-b599-a10fe950178f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c71ee1ec-38aa-4b0a-aebc-6d68f47d0002",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67d34bb3-4a50-4f1a-9a10-505dcaf8c368",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c71ee1ec-38aa-4b0a-aebc-6d68f47d0002",
                    "LayerId": "d6e18e07-bbd5-41af-922c-0f7ff1a39acd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "d6e18e07-bbd5-41af-922c-0f7ff1a39acd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6797ece8-3485-49d4-af2f-f985947fa9e9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}