{
    "id": "79a4737b-c82a-4572-9129-61bdefceddac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ps4_share",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 8,
    "bbox_right": 90,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db620ed9-16e1-4c59-95ba-e7ff85550b31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79a4737b-c82a-4572-9129-61bdefceddac",
            "compositeImage": {
                "id": "de38a856-1d4f-4c67-8063-70367b54bd05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db620ed9-16e1-4c59-95ba-e7ff85550b31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be4865f3-df62-4bd2-9925-198c170b2e02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db620ed9-16e1-4c59-95ba-e7ff85550b31",
                    "LayerId": "45038aba-5379-4e1b-af00-268ad75d2c84"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "45038aba-5379-4e1b-af00-268ad75d2c84",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79a4737b-c82a-4572-9129-61bdefceddac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}