{
    "id": "0df6b28a-e286-4865-9d78-42e75cd4b82f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_body_run_brown_and_gray",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d98a466b-91b5-43e3-bf4f-015ab9a17151",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df6b28a-e286-4865-9d78-42e75cd4b82f",
            "compositeImage": {
                "id": "a5dd561f-958c-4326-8ab0-fe7c94f03485",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d98a466b-91b5-43e3-bf4f-015ab9a17151",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1264e4cf-f718-43ba-837f-382f7ec33081",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d98a466b-91b5-43e3-bf4f-015ab9a17151",
                    "LayerId": "f4ad6e0f-ef82-4282-9bd8-692dcc54af02"
                }
            ]
        },
        {
            "id": "179c170d-1e81-492d-8ecd-3f4c178baf2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df6b28a-e286-4865-9d78-42e75cd4b82f",
            "compositeImage": {
                "id": "0022ddf2-3fc2-4c8a-b1f9-3d80918c93ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "179c170d-1e81-492d-8ecd-3f4c178baf2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "298f5b9b-eae6-496e-97ae-a6cee14dc7b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "179c170d-1e81-492d-8ecd-3f4c178baf2a",
                    "LayerId": "f4ad6e0f-ef82-4282-9bd8-692dcc54af02"
                }
            ]
        },
        {
            "id": "5cb9a4ca-8240-457c-a343-59e00299c8cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df6b28a-e286-4865-9d78-42e75cd4b82f",
            "compositeImage": {
                "id": "e84cc1a7-786e-4b9a-84a7-1e2cc931101b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cb9a4ca-8240-457c-a343-59e00299c8cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc5b0b22-475a-4314-88c2-de3b898c030c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cb9a4ca-8240-457c-a343-59e00299c8cd",
                    "LayerId": "f4ad6e0f-ef82-4282-9bd8-692dcc54af02"
                }
            ]
        },
        {
            "id": "5c846c2f-6f23-475c-8b7f-0ac446c6c970",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df6b28a-e286-4865-9d78-42e75cd4b82f",
            "compositeImage": {
                "id": "a4af9b1b-11b6-4dda-bce2-6961b1b7f384",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c846c2f-6f23-475c-8b7f-0ac446c6c970",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d89e5e7e-d7ca-41e5-9de6-14da90bd036f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c846c2f-6f23-475c-8b7f-0ac446c6c970",
                    "LayerId": "f4ad6e0f-ef82-4282-9bd8-692dcc54af02"
                }
            ]
        },
        {
            "id": "7fa76b54-c6b8-45fb-ab0f-b75abccc86dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df6b28a-e286-4865-9d78-42e75cd4b82f",
            "compositeImage": {
                "id": "d97f5f3b-551f-4e57-a056-2b4eb1c13edb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fa76b54-c6b8-45fb-ab0f-b75abccc86dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e66a4629-bd27-431f-bbf0-a5afee71c0f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fa76b54-c6b8-45fb-ab0f-b75abccc86dc",
                    "LayerId": "f4ad6e0f-ef82-4282-9bd8-692dcc54af02"
                }
            ]
        },
        {
            "id": "f3451d59-8879-453f-86ea-e7e121d3ab12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df6b28a-e286-4865-9d78-42e75cd4b82f",
            "compositeImage": {
                "id": "f804e17f-da1a-4c9f-8fa7-f2556b8dc4b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3451d59-8879-453f-86ea-e7e121d3ab12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec725a8f-9a6f-44df-bf18-8a6776fae724",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3451d59-8879-453f-86ea-e7e121d3ab12",
                    "LayerId": "f4ad6e0f-ef82-4282-9bd8-692dcc54af02"
                }
            ]
        },
        {
            "id": "15572e95-3d2f-441c-b293-a9af686bea0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df6b28a-e286-4865-9d78-42e75cd4b82f",
            "compositeImage": {
                "id": "8da18504-0ccb-4124-9155-27cfc3feb6a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15572e95-3d2f-441c-b293-a9af686bea0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9376c84b-d80b-4534-b39e-77c5579cece0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15572e95-3d2f-441c-b293-a9af686bea0b",
                    "LayerId": "f4ad6e0f-ef82-4282-9bd8-692dcc54af02"
                }
            ]
        },
        {
            "id": "a420f6fb-45a9-419e-836b-549b3a35744c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df6b28a-e286-4865-9d78-42e75cd4b82f",
            "compositeImage": {
                "id": "a51f75c1-1297-41b9-aeeb-c911153a6f68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a420f6fb-45a9-419e-836b-549b3a35744c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37a6bcc7-5cc0-4ef7-b57e-ddc960898d97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a420f6fb-45a9-419e-836b-549b3a35744c",
                    "LayerId": "f4ad6e0f-ef82-4282-9bd8-692dcc54af02"
                }
            ]
        },
        {
            "id": "ae1f7a3b-29a1-4b1e-b08b-38be6318a4e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df6b28a-e286-4865-9d78-42e75cd4b82f",
            "compositeImage": {
                "id": "e9d27931-27e9-439f-ad72-75d5c5accd5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae1f7a3b-29a1-4b1e-b08b-38be6318a4e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03e6c9e3-5ad0-4686-a366-826001b54660",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae1f7a3b-29a1-4b1e-b08b-38be6318a4e3",
                    "LayerId": "f4ad6e0f-ef82-4282-9bd8-692dcc54af02"
                }
            ]
        },
        {
            "id": "4fd56d03-4f51-46ab-9af1-cdcdb91618fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df6b28a-e286-4865-9d78-42e75cd4b82f",
            "compositeImage": {
                "id": "31e59fd1-b01e-4ffc-b502-6def7238cc5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fd56d03-4f51-46ab-9af1-cdcdb91618fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d68a4a9-75db-473d-90f8-1497ce3d4062",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fd56d03-4f51-46ab-9af1-cdcdb91618fb",
                    "LayerId": "f4ad6e0f-ef82-4282-9bd8-692dcc54af02"
                }
            ]
        },
        {
            "id": "d91c40ed-7579-44e8-9b18-b94cdec6e897",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df6b28a-e286-4865-9d78-42e75cd4b82f",
            "compositeImage": {
                "id": "5b895376-8012-433e-abea-c7063f59d4ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d91c40ed-7579-44e8-9b18-b94cdec6e897",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e240b83d-6132-4cab-9ea9-a84d50bcd8a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d91c40ed-7579-44e8-9b18-b94cdec6e897",
                    "LayerId": "f4ad6e0f-ef82-4282-9bd8-692dcc54af02"
                }
            ]
        },
        {
            "id": "4460d3d8-448f-42e5-9d6d-6310c52e33fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0df6b28a-e286-4865-9d78-42e75cd4b82f",
            "compositeImage": {
                "id": "6793b3df-5ec9-4326-8f36-01692743ae58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4460d3d8-448f-42e5-9d6d-6310c52e33fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f88d895-daed-4c60-bf7b-e66e639a0733",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4460d3d8-448f-42e5-9d6d-6310c52e33fd",
                    "LayerId": "f4ad6e0f-ef82-4282-9bd8-692dcc54af02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "f4ad6e0f-ef82-4282-9bd8-692dcc54af02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0df6b28a-e286-4865-9d78-42e75cd4b82f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 21
}