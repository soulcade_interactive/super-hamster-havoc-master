{
    "id": "5ee55059-dc88-4c1e-9421-ac76048e18e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_xboxone_rt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 10,
    "bbox_right": 89,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a043f106-4d98-4b4a-81cd-18858693b1f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5ee55059-dc88-4c1e-9421-ac76048e18e6",
            "compositeImage": {
                "id": "16e9dcc7-8633-4718-923a-fe5cac7ab760",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a043f106-4d98-4b4a-81cd-18858693b1f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d39633bd-2695-4c77-929f-2c0d616a4458",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a043f106-4d98-4b4a-81cd-18858693b1f9",
                    "LayerId": "99e67b56-0bfe-4599-b185-f634a4b44d4b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "99e67b56-0bfe-4599-b185-f634a4b44d4b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5ee55059-dc88-4c1e-9421-ac76048e18e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}