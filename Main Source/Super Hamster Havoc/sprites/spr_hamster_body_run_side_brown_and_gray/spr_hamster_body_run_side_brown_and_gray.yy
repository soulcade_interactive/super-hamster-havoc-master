{
    "id": "3dba656c-a9ef-46c0-bf0e-9be2e69c12f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_body_run_side_brown_and_gray",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 1,
    "bbox_right": 28,
    "bbox_top": 24,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "83ece467-72d3-4ad6-b0bc-6f3039a3f140",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dba656c-a9ef-46c0-bf0e-9be2e69c12f6",
            "compositeImage": {
                "id": "9650cc0b-662a-4985-9785-2eaf5276dc69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83ece467-72d3-4ad6-b0bc-6f3039a3f140",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d581a7dd-2028-4b9f-93a4-7ca1d2732986",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83ece467-72d3-4ad6-b0bc-6f3039a3f140",
                    "LayerId": "2a041fe4-b891-4058-816e-2903f2c7b13e"
                }
            ]
        },
        {
            "id": "50d6e072-f63e-48ca-95c1-bdd352afd5a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dba656c-a9ef-46c0-bf0e-9be2e69c12f6",
            "compositeImage": {
                "id": "7fda65d0-13fd-49af-b10c-c960db75ad57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50d6e072-f63e-48ca-95c1-bdd352afd5a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eab19d49-e8d4-4a13-9cc6-3078cb0d4a97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50d6e072-f63e-48ca-95c1-bdd352afd5a4",
                    "LayerId": "2a041fe4-b891-4058-816e-2903f2c7b13e"
                }
            ]
        },
        {
            "id": "d972ea21-1a37-4db9-a26b-04cb253e0cc6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dba656c-a9ef-46c0-bf0e-9be2e69c12f6",
            "compositeImage": {
                "id": "c01bb30d-84dd-4dfe-8701-31e0de2bb823",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d972ea21-1a37-4db9-a26b-04cb253e0cc6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58543da8-6e3f-40cc-a3f6-2eb71671f504",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d972ea21-1a37-4db9-a26b-04cb253e0cc6",
                    "LayerId": "2a041fe4-b891-4058-816e-2903f2c7b13e"
                }
            ]
        },
        {
            "id": "3a8dab39-5622-4ada-8c28-47ac7f79d922",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dba656c-a9ef-46c0-bf0e-9be2e69c12f6",
            "compositeImage": {
                "id": "486a3a89-c1b7-476d-905d-e691b16fb9ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a8dab39-5622-4ada-8c28-47ac7f79d922",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79d51ba0-9dc7-425b-a0e0-b31479023d17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a8dab39-5622-4ada-8c28-47ac7f79d922",
                    "LayerId": "2a041fe4-b891-4058-816e-2903f2c7b13e"
                }
            ]
        },
        {
            "id": "a57be019-4146-44a1-bca4-c52f706dc1fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dba656c-a9ef-46c0-bf0e-9be2e69c12f6",
            "compositeImage": {
                "id": "c983e395-478a-4198-a7ea-b39b2cc2e609",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a57be019-4146-44a1-bca4-c52f706dc1fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c388297-a79b-4a6e-ba67-1472a9ef34b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a57be019-4146-44a1-bca4-c52f706dc1fc",
                    "LayerId": "2a041fe4-b891-4058-816e-2903f2c7b13e"
                }
            ]
        },
        {
            "id": "cda6e1e3-7727-49d6-a875-b2524505aea9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dba656c-a9ef-46c0-bf0e-9be2e69c12f6",
            "compositeImage": {
                "id": "947b6fdd-a8ca-431f-b7c2-5761e06bfed1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cda6e1e3-7727-49d6-a875-b2524505aea9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed62bc69-b9b4-4119-8b8c-017bfa9ad998",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cda6e1e3-7727-49d6-a875-b2524505aea9",
                    "LayerId": "2a041fe4-b891-4058-816e-2903f2c7b13e"
                }
            ]
        },
        {
            "id": "f2ce4487-f4c0-4d47-b562-c9b0d489b1b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dba656c-a9ef-46c0-bf0e-9be2e69c12f6",
            "compositeImage": {
                "id": "eac6bba7-10c6-4cec-a760-a93cf0d0cb93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2ce4487-f4c0-4d47-b562-c9b0d489b1b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "042a22e6-8eef-462a-943e-00e7a73066ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2ce4487-f4c0-4d47-b562-c9b0d489b1b2",
                    "LayerId": "2a041fe4-b891-4058-816e-2903f2c7b13e"
                }
            ]
        },
        {
            "id": "b2d16bb1-7035-42b1-9464-bdfb4f7b2636",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dba656c-a9ef-46c0-bf0e-9be2e69c12f6",
            "compositeImage": {
                "id": "625001eb-bd6d-46ae-8e84-787f9cc8c781",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2d16bb1-7035-42b1-9464-bdfb4f7b2636",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45e89471-c0bd-4619-96c4-42f68f93a353",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2d16bb1-7035-42b1-9464-bdfb4f7b2636",
                    "LayerId": "2a041fe4-b891-4058-816e-2903f2c7b13e"
                }
            ]
        },
        {
            "id": "5e354f23-c5d3-481c-9c00-293d81741af0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dba656c-a9ef-46c0-bf0e-9be2e69c12f6",
            "compositeImage": {
                "id": "bef00f9b-97d5-43ba-a8cd-5203c9cf95c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e354f23-c5d3-481c-9c00-293d81741af0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62dd90c3-8570-4b71-94d1-f2e3741adb9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e354f23-c5d3-481c-9c00-293d81741af0",
                    "LayerId": "2a041fe4-b891-4058-816e-2903f2c7b13e"
                }
            ]
        },
        {
            "id": "d5ae9e96-7092-4279-af22-cc1150adb11a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dba656c-a9ef-46c0-bf0e-9be2e69c12f6",
            "compositeImage": {
                "id": "4b183dab-a168-4208-ada3-90a54e327c42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5ae9e96-7092-4279-af22-cc1150adb11a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c5df9c6-3aff-4317-a76d-76b3b5286a9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5ae9e96-7092-4279-af22-cc1150adb11a",
                    "LayerId": "2a041fe4-b891-4058-816e-2903f2c7b13e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "2a041fe4-b891-4058-816e-2903f2c7b13e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3dba656c-a9ef-46c0-bf0e-9be2e69c12f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 21
}