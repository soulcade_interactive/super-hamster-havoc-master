{
    "id": "63b3f932-6932-427e-9054-fa6f6b221b23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_xboxone_dpad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 4,
    "bbox_right": 95,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4fa1a4fe-c8e7-4378-86da-0da69d0711d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63b3f932-6932-427e-9054-fa6f6b221b23",
            "compositeImage": {
                "id": "4a084661-1688-4ed2-a49f-e1b8652034a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fa1a4fe-c8e7-4378-86da-0da69d0711d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a637324-3876-446a-b069-38319f0d75d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fa1a4fe-c8e7-4378-86da-0da69d0711d3",
                    "LayerId": "32e904f9-db70-45fb-a471-79aaf4531ce6"
                }
            ]
        },
        {
            "id": "3027ba72-eaa5-4a1e-92ea-646940eeb1bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63b3f932-6932-427e-9054-fa6f6b221b23",
            "compositeImage": {
                "id": "497fbd64-c292-4a80-a247-269ee5777599",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3027ba72-eaa5-4a1e-92ea-646940eeb1bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13dcfb31-7331-46f7-906d-9a224c1b3727",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3027ba72-eaa5-4a1e-92ea-646940eeb1bb",
                    "LayerId": "32e904f9-db70-45fb-a471-79aaf4531ce6"
                }
            ]
        },
        {
            "id": "5f3d03a5-979d-4a08-a7cf-f73bc5946cd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63b3f932-6932-427e-9054-fa6f6b221b23",
            "compositeImage": {
                "id": "a7a3a513-7a32-42cf-ba68-d361ca45782b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f3d03a5-979d-4a08-a7cf-f73bc5946cd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a12148a8-cd4e-4fe1-a7bf-ae4a4646f80b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f3d03a5-979d-4a08-a7cf-f73bc5946cd2",
                    "LayerId": "32e904f9-db70-45fb-a471-79aaf4531ce6"
                }
            ]
        },
        {
            "id": "5ad0e255-31d6-41ca-9333-061290b19938",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63b3f932-6932-427e-9054-fa6f6b221b23",
            "compositeImage": {
                "id": "72e6c843-83b7-4f27-ab65-71f9ec4ab7d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ad0e255-31d6-41ca-9333-061290b19938",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c15df272-c073-40d9-a1e4-fe7877540638",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ad0e255-31d6-41ca-9333-061290b19938",
                    "LayerId": "32e904f9-db70-45fb-a471-79aaf4531ce6"
                }
            ]
        },
        {
            "id": "e9fa6388-f250-4621-b877-0d90bb3c47dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63b3f932-6932-427e-9054-fa6f6b221b23",
            "compositeImage": {
                "id": "57b3a439-2675-40ee-94d0-6fe245226ff6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9fa6388-f250-4621-b877-0d90bb3c47dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "297c3a78-c07f-4670-b1d7-3902829bb896",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9fa6388-f250-4621-b877-0d90bb3c47dc",
                    "LayerId": "32e904f9-db70-45fb-a471-79aaf4531ce6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "32e904f9-db70-45fb-a471-79aaf4531ce6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63b3f932-6932-427e-9054-fa6f6b221b23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}