{
    "id": "3af48353-476d-4b60-9c9b-23f6578036f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_0_head_d_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 17,
    "bbox_right": 46,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77ad0227-6923-4a62-8bd2-9a1bf6ebbda8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af48353-476d-4b60-9c9b-23f6578036f0",
            "compositeImage": {
                "id": "22b32e24-3828-4d27-9f2e-21a6965b500e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77ad0227-6923-4a62-8bd2-9a1bf6ebbda8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80b7fbbb-43d4-4769-8cd3-1fb5997a9808",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77ad0227-6923-4a62-8bd2-9a1bf6ebbda8",
                    "LayerId": "4195738d-0b43-43b2-b367-1fc50639eb17"
                }
            ]
        },
        {
            "id": "0099d8cc-97ed-4700-a323-7d5f7dff1161",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af48353-476d-4b60-9c9b-23f6578036f0",
            "compositeImage": {
                "id": "c6aabb63-597c-4451-9cf5-2875259e2218",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0099d8cc-97ed-4700-a323-7d5f7dff1161",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4a7b9f9-914d-4b78-a1b8-36d39e19fdc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0099d8cc-97ed-4700-a323-7d5f7dff1161",
                    "LayerId": "4195738d-0b43-43b2-b367-1fc50639eb17"
                }
            ]
        },
        {
            "id": "4c4c16f3-3fa6-4bc0-9e68-fac372abdace",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af48353-476d-4b60-9c9b-23f6578036f0",
            "compositeImage": {
                "id": "01a13805-783b-4436-9c4b-d4ab5bad05c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c4c16f3-3fa6-4bc0-9e68-fac372abdace",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3e66e80-300f-4b58-b9a0-3d507959218a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c4c16f3-3fa6-4bc0-9e68-fac372abdace",
                    "LayerId": "4195738d-0b43-43b2-b367-1fc50639eb17"
                }
            ]
        },
        {
            "id": "056559f6-7d89-420b-8e12-25dcb6b5271a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af48353-476d-4b60-9c9b-23f6578036f0",
            "compositeImage": {
                "id": "7a2b515e-521e-405b-8ebc-7c0414303340",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "056559f6-7d89-420b-8e12-25dcb6b5271a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab8942d5-a125-4c00-a489-b7e795033a53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "056559f6-7d89-420b-8e12-25dcb6b5271a",
                    "LayerId": "4195738d-0b43-43b2-b367-1fc50639eb17"
                }
            ]
        },
        {
            "id": "f5b11f45-6929-4ee5-9aad-6b471bcfc739",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af48353-476d-4b60-9c9b-23f6578036f0",
            "compositeImage": {
                "id": "4646502a-6898-44c7-a091-0016d80b49f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5b11f45-6929-4ee5-9aad-6b471bcfc739",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc24c195-36fb-407d-92b4-03de630ab948",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5b11f45-6929-4ee5-9aad-6b471bcfc739",
                    "LayerId": "4195738d-0b43-43b2-b367-1fc50639eb17"
                }
            ]
        },
        {
            "id": "4561edea-3276-4355-bbfd-076b6a31f8d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af48353-476d-4b60-9c9b-23f6578036f0",
            "compositeImage": {
                "id": "c04eb23e-74b1-41db-9b7f-b96373937cb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4561edea-3276-4355-bbfd-076b6a31f8d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ed61029-d0f2-4ebb-b6b1-76da50160ed9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4561edea-3276-4355-bbfd-076b6a31f8d8",
                    "LayerId": "4195738d-0b43-43b2-b367-1fc50639eb17"
                }
            ]
        },
        {
            "id": "b4382169-4b34-41f4-8e6c-91449c3895f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af48353-476d-4b60-9c9b-23f6578036f0",
            "compositeImage": {
                "id": "5862164f-efdf-402e-ac76-210cb332f8b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4382169-4b34-41f4-8e6c-91449c3895f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82ba8a37-2614-4349-8e7e-734a639fd014",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4382169-4b34-41f4-8e6c-91449c3895f1",
                    "LayerId": "4195738d-0b43-43b2-b367-1fc50639eb17"
                }
            ]
        },
        {
            "id": "e3db2b35-96ac-4dc9-824b-6c6b8b6fe0e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af48353-476d-4b60-9c9b-23f6578036f0",
            "compositeImage": {
                "id": "825efe20-0de3-4b3a-a424-1e8f16af2376",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3db2b35-96ac-4dc9-824b-6c6b8b6fe0e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5be0ccf-1b9b-47bc-b858-42b98e195f55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3db2b35-96ac-4dc9-824b-6c6b8b6fe0e6",
                    "LayerId": "4195738d-0b43-43b2-b367-1fc50639eb17"
                }
            ]
        },
        {
            "id": "6df38397-dc54-402d-a9bc-b79ef5a268ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af48353-476d-4b60-9c9b-23f6578036f0",
            "compositeImage": {
                "id": "297bf6b6-b96b-4f16-8f34-e2d200b027cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6df38397-dc54-402d-a9bc-b79ef5a268ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba4e8165-14ae-4714-bef8-31b773a0a556",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6df38397-dc54-402d-a9bc-b79ef5a268ae",
                    "LayerId": "4195738d-0b43-43b2-b367-1fc50639eb17"
                }
            ]
        },
        {
            "id": "698e2a68-0af0-44d9-9fd2-9fc1e1e0e44b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af48353-476d-4b60-9c9b-23f6578036f0",
            "compositeImage": {
                "id": "898cf201-a430-4b37-9aa4-3641368dc022",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "698e2a68-0af0-44d9-9fd2-9fc1e1e0e44b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29e9229b-1a9a-4526-9c33-4e04a65bac60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "698e2a68-0af0-44d9-9fd2-9fc1e1e0e44b",
                    "LayerId": "4195738d-0b43-43b2-b367-1fc50639eb17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4195738d-0b43-43b2-b367-1fc50639eb17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3af48353-476d-4b60-9c9b-23f6578036f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}