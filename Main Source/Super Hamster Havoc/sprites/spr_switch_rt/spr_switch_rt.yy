{
    "id": "31883f8f-4a86-4902-bf44-1c25cc92c2b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_switch_rt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 8,
    "bbox_right": 90,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa6836c0-2a4f-4d23-b40a-c2e52bbb39cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31883f8f-4a86-4902-bf44-1c25cc92c2b7",
            "compositeImage": {
                "id": "463fb4c7-3590-4e80-9283-82e7669d1371",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa6836c0-2a4f-4d23-b40a-c2e52bbb39cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "668312c2-dab3-4bdd-9e6e-663fba7b32d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa6836c0-2a4f-4d23-b40a-c2e52bbb39cb",
                    "LayerId": "7ce40f4e-2066-4e46-a716-230e67ec6001"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "7ce40f4e-2066-4e46-a716-230e67ec6001",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31883f8f-4a86-4902-bf44-1c25cc92c2b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 0,
    "yorig": 0
}