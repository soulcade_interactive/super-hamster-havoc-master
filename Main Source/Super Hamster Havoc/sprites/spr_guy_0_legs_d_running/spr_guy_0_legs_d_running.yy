{
    "id": "e47e6e66-1111-4575-aa02-c7014c88c7d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_0_legs_d_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 25,
    "bbox_right": 38,
    "bbox_top": 56,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3995609a-c269-49fe-a055-38b4ee273f2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e47e6e66-1111-4575-aa02-c7014c88c7d7",
            "compositeImage": {
                "id": "5594a27d-ed82-40dc-ac2d-45e6d3275bc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3995609a-c269-49fe-a055-38b4ee273f2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c07cc52e-e1bc-4d71-8deb-dc3cf47406b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3995609a-c269-49fe-a055-38b4ee273f2c",
                    "LayerId": "ac358dca-9383-4fd9-b077-ef0268616986"
                }
            ]
        },
        {
            "id": "822f8f12-6bb9-4b85-8f6c-a6834b9e4259",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e47e6e66-1111-4575-aa02-c7014c88c7d7",
            "compositeImage": {
                "id": "17df2202-ac51-4fd0-a332-b6871261cee7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "822f8f12-6bb9-4b85-8f6c-a6834b9e4259",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bca6ee3b-3998-4a99-b7ee-0c260fac8caf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "822f8f12-6bb9-4b85-8f6c-a6834b9e4259",
                    "LayerId": "ac358dca-9383-4fd9-b077-ef0268616986"
                }
            ]
        },
        {
            "id": "a9d55c31-b03a-4886-9e4d-6e082ced98dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e47e6e66-1111-4575-aa02-c7014c88c7d7",
            "compositeImage": {
                "id": "9dafe72b-fc72-4e6a-ac7c-d8987a49e2f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9d55c31-b03a-4886-9e4d-6e082ced98dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47a4ca89-a399-4671-8db2-913e7bb59fee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9d55c31-b03a-4886-9e4d-6e082ced98dd",
                    "LayerId": "ac358dca-9383-4fd9-b077-ef0268616986"
                }
            ]
        },
        {
            "id": "552105b3-8505-4c28-9182-44544094d62d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e47e6e66-1111-4575-aa02-c7014c88c7d7",
            "compositeImage": {
                "id": "389470a8-e9f9-49e0-b992-60099a9852e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "552105b3-8505-4c28-9182-44544094d62d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f490ae0-4770-4048-9ed5-2129488f33e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "552105b3-8505-4c28-9182-44544094d62d",
                    "LayerId": "ac358dca-9383-4fd9-b077-ef0268616986"
                }
            ]
        },
        {
            "id": "100ab959-d1d7-4c19-9c21-534b5eff9bf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e47e6e66-1111-4575-aa02-c7014c88c7d7",
            "compositeImage": {
                "id": "9b61cbcf-33d1-4602-911d-33089c512661",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "100ab959-d1d7-4c19-9c21-534b5eff9bf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c766a0c-7cd0-4de2-a6b6-08cb6ed7c942",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "100ab959-d1d7-4c19-9c21-534b5eff9bf2",
                    "LayerId": "ac358dca-9383-4fd9-b077-ef0268616986"
                }
            ]
        },
        {
            "id": "3833141b-89a7-4032-91ca-ef0b0b08294a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e47e6e66-1111-4575-aa02-c7014c88c7d7",
            "compositeImage": {
                "id": "ed76a5f2-4932-4c09-a191-ff4e9729996b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3833141b-89a7-4032-91ca-ef0b0b08294a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6ee0518-e5dd-48cd-8076-53a6176b3491",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3833141b-89a7-4032-91ca-ef0b0b08294a",
                    "LayerId": "ac358dca-9383-4fd9-b077-ef0268616986"
                }
            ]
        },
        {
            "id": "cafb7399-48e0-4961-b0db-762a79036e0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e47e6e66-1111-4575-aa02-c7014c88c7d7",
            "compositeImage": {
                "id": "e9f686cb-766d-4bd9-a8ca-3d37d70277cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cafb7399-48e0-4961-b0db-762a79036e0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3051c266-90ac-40ea-820f-ae31eaff5440",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cafb7399-48e0-4961-b0db-762a79036e0c",
                    "LayerId": "ac358dca-9383-4fd9-b077-ef0268616986"
                }
            ]
        },
        {
            "id": "f95493f0-343d-49e9-9d2d-e665d68d52c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e47e6e66-1111-4575-aa02-c7014c88c7d7",
            "compositeImage": {
                "id": "02eeb7f1-c041-4a08-861d-4365e7b86a31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f95493f0-343d-49e9-9d2d-e665d68d52c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bae0652-2073-4d03-b345-1925002f9490",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f95493f0-343d-49e9-9d2d-e665d68d52c0",
                    "LayerId": "ac358dca-9383-4fd9-b077-ef0268616986"
                }
            ]
        },
        {
            "id": "8b47ccb4-d29f-47ab-a31e-6cf6822faea2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e47e6e66-1111-4575-aa02-c7014c88c7d7",
            "compositeImage": {
                "id": "e48261a8-0fca-4c48-8e54-da0a8258ceb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b47ccb4-d29f-47ab-a31e-6cf6822faea2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9ecea41-de35-431b-bd56-a80d15c9ce00",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b47ccb4-d29f-47ab-a31e-6cf6822faea2",
                    "LayerId": "ac358dca-9383-4fd9-b077-ef0268616986"
                }
            ]
        },
        {
            "id": "b8c3be1d-cc06-4233-bf73-96fe751b918d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e47e6e66-1111-4575-aa02-c7014c88c7d7",
            "compositeImage": {
                "id": "b0d5f52c-0918-4cec-bfe5-2811040d8824",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8c3be1d-cc06-4233-bf73-96fe751b918d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "820e3802-6909-49be-8a23-c87aea61b9fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8c3be1d-cc06-4233-bf73-96fe751b918d",
                    "LayerId": "ac358dca-9383-4fd9-b077-ef0268616986"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ac358dca-9383-4fd9-b077-ef0268616986",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e47e6e66-1111-4575-aa02-c7014c88c7d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}