{
    "id": "1b856833-36c4-489b-a6de-b4b1b0ea4ac8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_button_a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 6,
    "bbox_right": 23,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "565ca326-6b1e-4711-9bb7-910b33bfd13a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b856833-36c4-489b-a6de-b4b1b0ea4ac8",
            "compositeImage": {
                "id": "2a04940e-4d9d-40ab-8f67-9ee66514e841",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "565ca326-6b1e-4711-9bb7-910b33bfd13a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f08edc1-ec97-41ea-82b1-fd1ac7c027df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "565ca326-6b1e-4711-9bb7-910b33bfd13a",
                    "LayerId": "4e008299-f7d3-4bbe-a291-c3d2a4b8e001"
                }
            ]
        },
        {
            "id": "aca8a898-6470-445d-b052-891d3e825a1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b856833-36c4-489b-a6de-b4b1b0ea4ac8",
            "compositeImage": {
                "id": "8f4095f0-6c68-4511-a671-070b8f61f69c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aca8a898-6470-445d-b052-891d3e825a1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "408b9092-31ff-431d-9519-c9e16c35bd6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aca8a898-6470-445d-b052-891d3e825a1c",
                    "LayerId": "4e008299-f7d3-4bbe-a291-c3d2a4b8e001"
                }
            ]
        },
        {
            "id": "b4603dca-82ce-485a-8a57-d1612aa53efb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b856833-36c4-489b-a6de-b4b1b0ea4ac8",
            "compositeImage": {
                "id": "5db601e0-f169-4588-a0fc-afeb55e6bbf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4603dca-82ce-485a-8a57-d1612aa53efb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "badc2fbc-112a-41e0-8285-b0bfba5cb46b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4603dca-82ce-485a-8a57-d1612aa53efb",
                    "LayerId": "4e008299-f7d3-4bbe-a291-c3d2a4b8e001"
                }
            ]
        },
        {
            "id": "2c721cb3-1a5e-4744-b42a-587ffb30efdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b856833-36c4-489b-a6de-b4b1b0ea4ac8",
            "compositeImage": {
                "id": "d766ac58-c254-49ed-8ac6-f028b0a0c3dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c721cb3-1a5e-4744-b42a-587ffb30efdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6484db5-29af-4e83-a523-de2b8e3b7821",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c721cb3-1a5e-4744-b42a-587ffb30efdb",
                    "LayerId": "4e008299-f7d3-4bbe-a291-c3d2a4b8e001"
                }
            ]
        },
        {
            "id": "63b5486d-acc3-4750-aafb-ed46144a8ada",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b856833-36c4-489b-a6de-b4b1b0ea4ac8",
            "compositeImage": {
                "id": "658fb134-9700-45fc-86da-3c33aae51e33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63b5486d-acc3-4750-aafb-ed46144a8ada",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7bf800f-6edf-4ad2-9377-e1b182e787f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63b5486d-acc3-4750-aafb-ed46144a8ada",
                    "LayerId": "4e008299-f7d3-4bbe-a291-c3d2a4b8e001"
                }
            ]
        },
        {
            "id": "f7210110-7200-4f4f-b92e-390f3d4732c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b856833-36c4-489b-a6de-b4b1b0ea4ac8",
            "compositeImage": {
                "id": "b4fc6e64-cf75-41c5-abc8-45736cd78d91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7210110-7200-4f4f-b92e-390f3d4732c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e31d5e8b-3ef3-4a38-bf50-a5fcacad59bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7210110-7200-4f4f-b92e-390f3d4732c3",
                    "LayerId": "4e008299-f7d3-4bbe-a291-c3d2a4b8e001"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "4e008299-f7d3-4bbe-a291-c3d2a4b8e001",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b856833-36c4-489b-a6de-b4b1b0ea4ac8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 17
}