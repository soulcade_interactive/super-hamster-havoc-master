{
    "id": "76841a90-a623-4d48-9f73-44ae0c6603c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_body_idle_brown_and_gray",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 3,
    "bbox_right": 31,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9712e453-06e8-4707-866e-52403fda71ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76841a90-a623-4d48-9f73-44ae0c6603c7",
            "compositeImage": {
                "id": "05f69248-65a5-4609-bddc-a243e9ee90c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9712e453-06e8-4707-866e-52403fda71ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "151dc8e2-9c54-4bdc-943d-f450b0c2a70f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9712e453-06e8-4707-866e-52403fda71ef",
                    "LayerId": "1c81955b-1238-4ed6-a307-e55218df18a9"
                }
            ]
        },
        {
            "id": "8b80a230-2bce-4ca0-9044-0ffed6e4eaa2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76841a90-a623-4d48-9f73-44ae0c6603c7",
            "compositeImage": {
                "id": "7409982e-b55c-46cf-9b6b-a4703a14e91a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b80a230-2bce-4ca0-9044-0ffed6e4eaa2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc3ced99-48a5-4e55-960a-743a3125a0f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b80a230-2bce-4ca0-9044-0ffed6e4eaa2",
                    "LayerId": "1c81955b-1238-4ed6-a307-e55218df18a9"
                }
            ]
        },
        {
            "id": "872aac01-2b2c-414d-a4c6-6cb7d02af2d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76841a90-a623-4d48-9f73-44ae0c6603c7",
            "compositeImage": {
                "id": "d9c09268-4397-4b17-8251-531711017ca5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "872aac01-2b2c-414d-a4c6-6cb7d02af2d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0aca3b1b-003f-46de-974e-9393bb2f2024",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "872aac01-2b2c-414d-a4c6-6cb7d02af2d7",
                    "LayerId": "1c81955b-1238-4ed6-a307-e55218df18a9"
                }
            ]
        },
        {
            "id": "4cf247cd-82e8-4153-a6ed-ca2e006597af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76841a90-a623-4d48-9f73-44ae0c6603c7",
            "compositeImage": {
                "id": "f7f2c831-6cf3-474d-87b9-847427166085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cf247cd-82e8-4153-a6ed-ca2e006597af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abf25404-92d0-430d-a35d-77d9d25dc66b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cf247cd-82e8-4153-a6ed-ca2e006597af",
                    "LayerId": "1c81955b-1238-4ed6-a307-e55218df18a9"
                }
            ]
        },
        {
            "id": "2d5689c1-7e12-44b0-8d5a-4945dd5666ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76841a90-a623-4d48-9f73-44ae0c6603c7",
            "compositeImage": {
                "id": "402500d8-1d62-4783-8973-23d8978537f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d5689c1-7e12-44b0-8d5a-4945dd5666ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00d9f007-be40-473c-ab53-e6eebf4667ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d5689c1-7e12-44b0-8d5a-4945dd5666ec",
                    "LayerId": "1c81955b-1238-4ed6-a307-e55218df18a9"
                }
            ]
        },
        {
            "id": "f97e97b3-f8de-4f94-936c-3d905f8eccf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76841a90-a623-4d48-9f73-44ae0c6603c7",
            "compositeImage": {
                "id": "58d8b735-2b5a-4eac-a855-cdb00b108d32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f97e97b3-f8de-4f94-936c-3d905f8eccf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bad201aa-53f7-49e6-a20c-966b68be4e3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f97e97b3-f8de-4f94-936c-3d905f8eccf9",
                    "LayerId": "1c81955b-1238-4ed6-a307-e55218df18a9"
                }
            ]
        },
        {
            "id": "a308c6ea-4616-4e49-b292-3d5105523b0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76841a90-a623-4d48-9f73-44ae0c6603c7",
            "compositeImage": {
                "id": "466de7be-5c9e-4018-8fea-0cb1de183780",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a308c6ea-4616-4e49-b292-3d5105523b0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bebd7b6a-0649-412f-a666-ed7f84955ff0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a308c6ea-4616-4e49-b292-3d5105523b0e",
                    "LayerId": "1c81955b-1238-4ed6-a307-e55218df18a9"
                }
            ]
        },
        {
            "id": "f7d8ecd6-06c3-40bd-a194-58262a501970",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76841a90-a623-4d48-9f73-44ae0c6603c7",
            "compositeImage": {
                "id": "8f9db6e2-ff38-4df9-870b-7e9441965bcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7d8ecd6-06c3-40bd-a194-58262a501970",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e4a0b3b-af4a-4ee3-9e19-be6835fb31fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7d8ecd6-06c3-40bd-a194-58262a501970",
                    "LayerId": "1c81955b-1238-4ed6-a307-e55218df18a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 39,
    "layers": [
        {
            "id": "1c81955b-1238-4ed6-a307-e55218df18a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76841a90-a623-4d48-9f73-44ae0c6603c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 19
}