{
    "id": "a41fc2fc-3725-494e-a0e5-592f93db19bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_character_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 14,
    "bbox_right": 113,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e765609-3f4b-4585-88d8-8a284dbf9e65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a41fc2fc-3725-494e-a0e5-592f93db19bd",
            "compositeImage": {
                "id": "121d4879-6a8a-4f75-b289-ab76ca076cf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e765609-3f4b-4585-88d8-8a284dbf9e65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f8891dc-21e2-4aff-a299-d8b6fb59447e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e765609-3f4b-4585-88d8-8a284dbf9e65",
                    "LayerId": "404eba93-0b1a-4e24-b259-635673b88eb1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "404eba93-0b1a-4e24-b259-635673b88eb1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a41fc2fc-3725-494e-a0e5-592f93db19bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}