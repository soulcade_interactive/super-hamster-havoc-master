{
    "id": "857b62b8-2877-4c4d-b552-9aa35a4abf52",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bush2s2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 3,
    "bbox_right": 60,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dbf26d08-d38a-443b-b312-5a318097fd85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "857b62b8-2877-4c4d-b552-9aa35a4abf52",
            "compositeImage": {
                "id": "0bb59f2d-d68d-4c58-a8e8-14f0cc44c57d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbf26d08-d38a-443b-b312-5a318097fd85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff232ebe-1c01-429b-b39e-190ac64ce65d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbf26d08-d38a-443b-b312-5a318097fd85",
                    "LayerId": "834c96c2-02f3-4fbb-9aef-b4215a7e5ea2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "834c96c2-02f3-4fbb-9aef-b4215a7e5ea2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "857b62b8-2877-4c4d-b552-9aa35a4abf52",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}