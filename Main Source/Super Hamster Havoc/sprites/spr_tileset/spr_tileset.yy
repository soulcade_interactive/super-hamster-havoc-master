{
    "id": "a4e2cd40-0a56-4aed-b150-005564db829e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 283,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad87b18f-b583-471d-a7fb-73e4b81226d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4e2cd40-0a56-4aed-b150-005564db829e",
            "compositeImage": {
                "id": "2fa78dd4-578f-456d-a2f4-4888269132ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad87b18f-b583-471d-a7fb-73e4b81226d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4954353-98a5-43c3-ad7c-90907c051eb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad87b18f-b583-471d-a7fb-73e4b81226d6",
                    "LayerId": "a4066686-940c-48aa-b5cf-3b3952a42500"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "a4066686-940c-48aa-b5cf-3b3952a42500",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4e2cd40-0a56-4aed-b150-005564db829e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 160
}