{
    "id": "52949ad3-3cbe-4e33-98f0-1f1a5eb80232",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ps4_circle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 9,
    "bbox_right": 90,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "780802d9-a4fa-42c5-bf99-c494b35593d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52949ad3-3cbe-4e33-98f0-1f1a5eb80232",
            "compositeImage": {
                "id": "746043d8-96bb-4156-a159-19ab9f54fffc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "780802d9-a4fa-42c5-bf99-c494b35593d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d108848e-a6d4-4389-af77-2c7546018e44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "780802d9-a4fa-42c5-bf99-c494b35593d1",
                    "LayerId": "d5626c38-e1fb-4e40-bf7c-c45395b58382"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "d5626c38-e1fb-4e40-bf7c-c45395b58382",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52949ad3-3cbe-4e33-98f0-1f1a5eb80232",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}