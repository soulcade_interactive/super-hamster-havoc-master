{
    "id": "1e6e8f4e-811b-441e-bfb0-d5ae9d6a46e7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_1_legs_l_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 22,
    "bbox_right": 44,
    "bbox_top": 55,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f274a7f-936a-4d27-b0ba-2efab81c4cc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e6e8f4e-811b-441e-bfb0-d5ae9d6a46e7",
            "compositeImage": {
                "id": "bc74bc72-b245-49b4-ac41-a3548c5cba01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f274a7f-936a-4d27-b0ba-2efab81c4cc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0bd4c2d-5324-44e0-b58f-c871666b1312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f274a7f-936a-4d27-b0ba-2efab81c4cc4",
                    "LayerId": "5e41eaed-5587-4ed3-8617-ffd974ac0ac4"
                }
            ]
        },
        {
            "id": "3a00310c-fb53-4f93-a07e-4a72054ea0ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e6e8f4e-811b-441e-bfb0-d5ae9d6a46e7",
            "compositeImage": {
                "id": "27754c25-6227-4cc7-93ab-0d45aa5340b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a00310c-fb53-4f93-a07e-4a72054ea0ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87b24c97-acae-4112-88bf-0116b68fe85a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a00310c-fb53-4f93-a07e-4a72054ea0ee",
                    "LayerId": "5e41eaed-5587-4ed3-8617-ffd974ac0ac4"
                }
            ]
        },
        {
            "id": "f769e1d4-0dc7-4695-b294-cba3a86a95eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e6e8f4e-811b-441e-bfb0-d5ae9d6a46e7",
            "compositeImage": {
                "id": "e91f5952-cf26-4d3b-9603-12526e017d32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f769e1d4-0dc7-4695-b294-cba3a86a95eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "589039b1-8287-49b6-843d-9d719d2f0eea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f769e1d4-0dc7-4695-b294-cba3a86a95eb",
                    "LayerId": "5e41eaed-5587-4ed3-8617-ffd974ac0ac4"
                }
            ]
        },
        {
            "id": "a4243301-ddb8-4dca-b223-daf49012f942",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e6e8f4e-811b-441e-bfb0-d5ae9d6a46e7",
            "compositeImage": {
                "id": "9ee1af04-75c3-4bb4-8bf5-136377e55d05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4243301-ddb8-4dca-b223-daf49012f942",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e5f27dc-fe71-4985-8c0b-5d2b731ff763",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4243301-ddb8-4dca-b223-daf49012f942",
                    "LayerId": "5e41eaed-5587-4ed3-8617-ffd974ac0ac4"
                }
            ]
        },
        {
            "id": "b2f3f7ec-3425-4d72-ac5b-1569ab794f8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e6e8f4e-811b-441e-bfb0-d5ae9d6a46e7",
            "compositeImage": {
                "id": "b28f9f5b-6088-4efe-b584-69117c763b8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2f3f7ec-3425-4d72-ac5b-1569ab794f8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ad940db-1880-4cfc-b29b-58febbe86828",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2f3f7ec-3425-4d72-ac5b-1569ab794f8e",
                    "LayerId": "5e41eaed-5587-4ed3-8617-ffd974ac0ac4"
                }
            ]
        },
        {
            "id": "bc7a74a8-2b13-4618-9a9d-0751c6ee0dbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e6e8f4e-811b-441e-bfb0-d5ae9d6a46e7",
            "compositeImage": {
                "id": "28a66e52-4238-48a5-a1d5-584d14346c96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc7a74a8-2b13-4618-9a9d-0751c6ee0dbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "970e25ae-97e7-48ce-ad57-f1094f68417b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc7a74a8-2b13-4618-9a9d-0751c6ee0dbe",
                    "LayerId": "5e41eaed-5587-4ed3-8617-ffd974ac0ac4"
                }
            ]
        },
        {
            "id": "f81399dd-ccd4-4e13-95c4-99bc31b3cea5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e6e8f4e-811b-441e-bfb0-d5ae9d6a46e7",
            "compositeImage": {
                "id": "7e4edac1-7ade-491c-9aae-0168eaad23f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f81399dd-ccd4-4e13-95c4-99bc31b3cea5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52e1ddc2-b1db-450a-a5ca-26d9f5e7257f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f81399dd-ccd4-4e13-95c4-99bc31b3cea5",
                    "LayerId": "5e41eaed-5587-4ed3-8617-ffd974ac0ac4"
                }
            ]
        },
        {
            "id": "270b2ade-72cc-4224-831a-11b468ed961f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e6e8f4e-811b-441e-bfb0-d5ae9d6a46e7",
            "compositeImage": {
                "id": "02739878-20d9-49e1-9efb-537970d99d0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "270b2ade-72cc-4224-831a-11b468ed961f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f852a1c-e59a-48e9-95d8-bf98f4376613",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "270b2ade-72cc-4224-831a-11b468ed961f",
                    "LayerId": "5e41eaed-5587-4ed3-8617-ffd974ac0ac4"
                }
            ]
        },
        {
            "id": "4c9ca5b3-e5a8-4f45-b49d-72c14fcc49b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e6e8f4e-811b-441e-bfb0-d5ae9d6a46e7",
            "compositeImage": {
                "id": "08eceee4-b209-439c-bce7-8b14bcc61036",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c9ca5b3-e5a8-4f45-b49d-72c14fcc49b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e41054ff-9bdf-426d-a670-23d61ab97099",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c9ca5b3-e5a8-4f45-b49d-72c14fcc49b1",
                    "LayerId": "5e41eaed-5587-4ed3-8617-ffd974ac0ac4"
                }
            ]
        },
        {
            "id": "fbe170d6-2ae4-479e-9309-b7b3f02ed180",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e6e8f4e-811b-441e-bfb0-d5ae9d6a46e7",
            "compositeImage": {
                "id": "8f4d91f9-4738-4cc0-8953-712d551f9ce9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbe170d6-2ae4-479e-9309-b7b3f02ed180",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d79b53ee-6084-42ab-be10-b8f5f74ea289",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbe170d6-2ae4-479e-9309-b7b3f02ed180",
                    "LayerId": "5e41eaed-5587-4ed3-8617-ffd974ac0ac4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5e41eaed-5587-4ed3-8617-ffd974ac0ac4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e6e8f4e-811b-441e-bfb0-d5ae9d6a46e7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}