{
    "id": "ec68241a-8501-49d5-9b1b-799183b6af9d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_1_head_r_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 12,
    "bbox_right": 47,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e791622-1443-4e21-99be-df14381e8759",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec68241a-8501-49d5-9b1b-799183b6af9d",
            "compositeImage": {
                "id": "2af78438-be14-4d49-84a1-da62682e7a4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e791622-1443-4e21-99be-df14381e8759",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1858c43-9451-4d3e-896b-eba64b16cbc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e791622-1443-4e21-99be-df14381e8759",
                    "LayerId": "10368f12-53fa-467b-8316-e3d83af73bd2"
                }
            ]
        },
        {
            "id": "d55e4cb4-4145-4e6f-afd7-eca9a92c82af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec68241a-8501-49d5-9b1b-799183b6af9d",
            "compositeImage": {
                "id": "90e8d1af-1ae3-4c54-a604-504f9d3fbd48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d55e4cb4-4145-4e6f-afd7-eca9a92c82af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f6ac85d-a924-4f49-9f8c-b46e0c40f59e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d55e4cb4-4145-4e6f-afd7-eca9a92c82af",
                    "LayerId": "10368f12-53fa-467b-8316-e3d83af73bd2"
                }
            ]
        },
        {
            "id": "09179909-4720-49b3-9daf-99910d601802",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec68241a-8501-49d5-9b1b-799183b6af9d",
            "compositeImage": {
                "id": "91d0e6b3-6f91-454c-a4a5-8f53ea5ce53b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09179909-4720-49b3-9daf-99910d601802",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf4fecda-4561-4d5e-bd47-b28f31f44e26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09179909-4720-49b3-9daf-99910d601802",
                    "LayerId": "10368f12-53fa-467b-8316-e3d83af73bd2"
                }
            ]
        },
        {
            "id": "3521892b-eef5-447b-9f7c-e4ddd0d79b9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec68241a-8501-49d5-9b1b-799183b6af9d",
            "compositeImage": {
                "id": "9b71b7e1-1dec-4757-9995-731edd4aac12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3521892b-eef5-447b-9f7c-e4ddd0d79b9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ac0f411-4e5b-4879-af32-b30323a409d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3521892b-eef5-447b-9f7c-e4ddd0d79b9a",
                    "LayerId": "10368f12-53fa-467b-8316-e3d83af73bd2"
                }
            ]
        },
        {
            "id": "96e3bff5-d8ca-4536-97db-84fc676d4fab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec68241a-8501-49d5-9b1b-799183b6af9d",
            "compositeImage": {
                "id": "738992c2-ecfc-4645-9aae-7fd767526172",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96e3bff5-d8ca-4536-97db-84fc676d4fab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ca42b21-53cf-42cb-9de9-2f82aef092d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96e3bff5-d8ca-4536-97db-84fc676d4fab",
                    "LayerId": "10368f12-53fa-467b-8316-e3d83af73bd2"
                }
            ]
        },
        {
            "id": "01c61eaa-7e03-498f-bf42-882296786d63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec68241a-8501-49d5-9b1b-799183b6af9d",
            "compositeImage": {
                "id": "e6c9ba98-aa75-43d0-b70f-95c4e0c33490",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01c61eaa-7e03-498f-bf42-882296786d63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7206f537-757a-4a03-8156-8ab246008e5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01c61eaa-7e03-498f-bf42-882296786d63",
                    "LayerId": "10368f12-53fa-467b-8316-e3d83af73bd2"
                }
            ]
        },
        {
            "id": "2217363c-2be7-4a37-939b-b3f2b2311a28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec68241a-8501-49d5-9b1b-799183b6af9d",
            "compositeImage": {
                "id": "9b9ed68e-6313-415a-893c-1f47766bb4ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2217363c-2be7-4a37-939b-b3f2b2311a28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18428e91-d72e-4647-b531-1307ef29d6c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2217363c-2be7-4a37-939b-b3f2b2311a28",
                    "LayerId": "10368f12-53fa-467b-8316-e3d83af73bd2"
                }
            ]
        },
        {
            "id": "3a87edc5-7fb0-4980-b936-23b28b609632",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec68241a-8501-49d5-9b1b-799183b6af9d",
            "compositeImage": {
                "id": "962fe397-81b2-4209-835c-e1fb8ab9a818",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a87edc5-7fb0-4980-b936-23b28b609632",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1ccda07-c888-4ddd-aa21-c34ad73afd39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a87edc5-7fb0-4980-b936-23b28b609632",
                    "LayerId": "10368f12-53fa-467b-8316-e3d83af73bd2"
                }
            ]
        },
        {
            "id": "dd7e84d7-d3bb-47b3-8f3c-33449b0dc4b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec68241a-8501-49d5-9b1b-799183b6af9d",
            "compositeImage": {
                "id": "8f53cb02-adac-4eab-a741-73e306073582",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd7e84d7-d3bb-47b3-8f3c-33449b0dc4b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "161db786-a942-4a5e-a44f-eba74e368f4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd7e84d7-d3bb-47b3-8f3c-33449b0dc4b0",
                    "LayerId": "10368f12-53fa-467b-8316-e3d83af73bd2"
                }
            ]
        },
        {
            "id": "5c7b3b1a-6635-4d9c-afd4-1f1f05ca6ebf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec68241a-8501-49d5-9b1b-799183b6af9d",
            "compositeImage": {
                "id": "5abe5e4e-b10c-4728-8190-0b4930b4cc40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c7b3b1a-6635-4d9c-afd4-1f1f05ca6ebf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3aa5639-7fbf-45d6-b9d8-a5ed95fbb6fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c7b3b1a-6635-4d9c-afd4-1f1f05ca6ebf",
                    "LayerId": "10368f12-53fa-467b-8316-e3d83af73bd2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "10368f12-53fa-467b-8316-e3d83af73bd2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec68241a-8501-49d5-9b1b-799183b6af9d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}