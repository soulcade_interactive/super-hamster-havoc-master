{
    "id": "462e5caa-5458-4aa4-acb6-b006722a27fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bush1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 1,
    "bbox_right": 60,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48557ab9-a117-4af6-b45b-5dc78323b528",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "462e5caa-5458-4aa4-acb6-b006722a27fa",
            "compositeImage": {
                "id": "dfddc26e-583a-46c4-8f20-28a48dca7787",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48557ab9-a117-4af6-b45b-5dc78323b528",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ac243fa-feb7-46a1-88e7-6be1dd7029ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48557ab9-a117-4af6-b45b-5dc78323b528",
                    "LayerId": "9375f0ee-978f-4c01-b4e1-b730c558b80a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9375f0ee-978f-4c01-b4e1-b730c558b80a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "462e5caa-5458-4aa4-acb6-b006722a27fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}