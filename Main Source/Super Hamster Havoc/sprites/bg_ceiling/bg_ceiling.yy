{
    "id": "6ae9bdd5-c1b2-4746-aa08-190171bf2fd3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_ceiling",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a0aede7-fcc3-4749-8360-2c9642e826a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ae9bdd5-c1b2-4746-aa08-190171bf2fd3",
            "compositeImage": {
                "id": "aa46f68a-3541-4efb-b86b-bf3d2aef1e0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a0aede7-fcc3-4749-8360-2c9642e826a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8779778f-673e-48b0-8db5-3943a4ed4cb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a0aede7-fcc3-4749-8360-2c9642e826a3",
                    "LayerId": "2ebb6d0d-0850-42d4-b454-ca18b366a3df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2ebb6d0d-0850-42d4-b454-ca18b366a3df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ae9bdd5-c1b2-4746-aa08-190171bf2fd3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}