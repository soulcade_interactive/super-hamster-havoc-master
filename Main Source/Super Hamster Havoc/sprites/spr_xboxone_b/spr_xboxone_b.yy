{
    "id": "4abe94fe-2298-4c65-9408-d9d619c4794a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_xboxone_b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 9,
    "bbox_right": 90,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1cde1b71-ba66-44cd-a094-143c910323b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4abe94fe-2298-4c65-9408-d9d619c4794a",
            "compositeImage": {
                "id": "c942484b-d019-4728-92e4-5cbc34b1b7ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cde1b71-ba66-44cd-a094-143c910323b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a5ce6ee-4184-466c-ba08-ed062005fb0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cde1b71-ba66-44cd-a094-143c910323b3",
                    "LayerId": "2a0e47e8-9c9f-48b6-901a-2f28c73b46fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "2a0e47e8-9c9f-48b6-901a-2f28c73b46fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4abe94fe-2298-4c65-9408-d9d619c4794a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}