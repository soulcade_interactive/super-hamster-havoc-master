{
    "id": "ee374c61-a6d2-44de-8103-50a597186ee1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_body_idle_brown_and_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 3,
    "bbox_right": 31,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69a1e22b-153f-4ad5-91dd-6f95637ca921",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee374c61-a6d2-44de-8103-50a597186ee1",
            "compositeImage": {
                "id": "346442d5-4dbb-45b3-8a6a-16bd1acacfec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69a1e22b-153f-4ad5-91dd-6f95637ca921",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a5d3248-ad98-4c5f-a94c-8af219371d38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69a1e22b-153f-4ad5-91dd-6f95637ca921",
                    "LayerId": "a2877685-c27a-4597-9e94-cae7822aa085"
                }
            ]
        },
        {
            "id": "3661f4b4-b165-4c7a-8978-007e34e3a8b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee374c61-a6d2-44de-8103-50a597186ee1",
            "compositeImage": {
                "id": "9cd793d9-de5c-43ab-8c60-36139a2ba0eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3661f4b4-b165-4c7a-8978-007e34e3a8b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d3984fd-015c-4ff0-a920-8be2bfff2f10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3661f4b4-b165-4c7a-8978-007e34e3a8b0",
                    "LayerId": "a2877685-c27a-4597-9e94-cae7822aa085"
                }
            ]
        },
        {
            "id": "b3027616-a825-4901-9555-f8cd9fb53e5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee374c61-a6d2-44de-8103-50a597186ee1",
            "compositeImage": {
                "id": "85238ca2-9fe5-4d14-801a-7c7575b99f67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3027616-a825-4901-9555-f8cd9fb53e5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "796c11ca-40ae-46b9-b1ed-ec5bc616893d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3027616-a825-4901-9555-f8cd9fb53e5e",
                    "LayerId": "a2877685-c27a-4597-9e94-cae7822aa085"
                }
            ]
        },
        {
            "id": "214fdb96-66c7-475b-ba14-556051d907d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee374c61-a6d2-44de-8103-50a597186ee1",
            "compositeImage": {
                "id": "07f4fcc4-dc5d-4e94-8b2d-d924915bd51f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "214fdb96-66c7-475b-ba14-556051d907d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19720872-e454-4689-9b5e-869cbee4c57f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "214fdb96-66c7-475b-ba14-556051d907d2",
                    "LayerId": "a2877685-c27a-4597-9e94-cae7822aa085"
                }
            ]
        },
        {
            "id": "2afd07e2-0a28-4a45-9cad-9f4d7b2ab022",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee374c61-a6d2-44de-8103-50a597186ee1",
            "compositeImage": {
                "id": "17abf3a4-9747-45d1-98ef-29076de950ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2afd07e2-0a28-4a45-9cad-9f4d7b2ab022",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bbd46e5-4346-479e-8290-04feab77fd4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2afd07e2-0a28-4a45-9cad-9f4d7b2ab022",
                    "LayerId": "a2877685-c27a-4597-9e94-cae7822aa085"
                }
            ]
        },
        {
            "id": "3e0b7e7f-cbdc-4239-8f48-00477fdad6cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee374c61-a6d2-44de-8103-50a597186ee1",
            "compositeImage": {
                "id": "773f76bb-6872-43b4-b430-87aaf43dca76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e0b7e7f-cbdc-4239-8f48-00477fdad6cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7c5b36a-60df-49be-a56f-0e04f947ebd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e0b7e7f-cbdc-4239-8f48-00477fdad6cf",
                    "LayerId": "a2877685-c27a-4597-9e94-cae7822aa085"
                }
            ]
        },
        {
            "id": "cdbb42dc-0d8a-469a-bb6e-f60e74f569e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee374c61-a6d2-44de-8103-50a597186ee1",
            "compositeImage": {
                "id": "04d35ebc-115f-44b4-80be-c03a488708cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdbb42dc-0d8a-469a-bb6e-f60e74f569e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3869356-3b9b-42d4-bbe4-b021fc2ab807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdbb42dc-0d8a-469a-bb6e-f60e74f569e2",
                    "LayerId": "a2877685-c27a-4597-9e94-cae7822aa085"
                }
            ]
        },
        {
            "id": "6b05c666-cee2-4dd1-8ec7-fe0940e384c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee374c61-a6d2-44de-8103-50a597186ee1",
            "compositeImage": {
                "id": "63a0cf8b-1bf2-4913-933f-8c20eb043157",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b05c666-cee2-4dd1-8ec7-fe0940e384c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31b17f92-83f8-4548-b340-86dcf6c3c6be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b05c666-cee2-4dd1-8ec7-fe0940e384c7",
                    "LayerId": "a2877685-c27a-4597-9e94-cae7822aa085"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 39,
    "layers": [
        {
            "id": "a2877685-c27a-4597-9e94-cae7822aa085",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee374c61-a6d2-44de-8103-50a597186ee1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 19
}