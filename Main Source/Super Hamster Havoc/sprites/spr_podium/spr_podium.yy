{
    "id": "8237562a-5217-43be-910c-e4f32acacf6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_podium",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 41,
    "bbox_right": 109,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "869c7bd9-0180-4fcc-84b6-a0dd001c9c7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8237562a-5217-43be-910c-e4f32acacf6a",
            "compositeImage": {
                "id": "ec6cc9ec-c3f8-4c41-b3b2-88c2a4263164",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "869c7bd9-0180-4fcc-84b6-a0dd001c9c7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b72756d4-2667-40b3-bb51-96ac2b6d4e3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "869c7bd9-0180-4fcc-84b6-a0dd001c9c7b",
                    "LayerId": "67e1301f-3076-4225-8742-82bddad19cf7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "67e1301f-3076-4225-8742-82bddad19cf7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8237562a-5217-43be-910c-e4f32acacf6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 75,
    "yorig": 50
}