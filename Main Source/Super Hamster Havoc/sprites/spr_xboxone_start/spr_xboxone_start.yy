{
    "id": "5f30087e-38d7-4f11-96df-a897f1ccb8a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_xboxone_start",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 88,
    "bbox_left": 11,
    "bbox_right": 88,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5590db5d-07ad-47fa-b466-d5445aeef7e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f30087e-38d7-4f11-96df-a897f1ccb8a7",
            "compositeImage": {
                "id": "fe02e9a7-8efd-4c0d-8021-1361197ee49d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5590db5d-07ad-47fa-b466-d5445aeef7e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ecb91e9-02da-4ae8-be7a-6fee579f0d0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5590db5d-07ad-47fa-b466-d5445aeef7e1",
                    "LayerId": "213b8d77-1bdc-4631-ae90-38805887b084"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "213b8d77-1bdc-4631-ae90-38805887b084",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f30087e-38d7-4f11-96df-a897f1ccb8a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}