{
    "id": "91a346f6-2300-49c6-beab-bc341c651a49",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_body_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 50,
    "bbox_left": 9,
    "bbox_right": 38,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2bc68f60-f672-4b5d-a1e9-8014b945a582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91a346f6-2300-49c6-beab-bc341c651a49",
            "compositeImage": {
                "id": "486c0e2d-3de7-4367-925a-2c5054d5dbf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bc68f60-f672-4b5d-a1e9-8014b945a582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4508135-0564-4dd9-9261-03ec4c21ea64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bc68f60-f672-4b5d-a1e9-8014b945a582",
                    "LayerId": "d7476a04-39d2-4570-b2c6-caa7d9678763"
                }
            ]
        },
        {
            "id": "77ac5198-2d15-41c9-a0ab-9cf11a33e3d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91a346f6-2300-49c6-beab-bc341c651a49",
            "compositeImage": {
                "id": "a338910c-d7e4-4d4b-b01f-08f884db7905",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77ac5198-2d15-41c9-a0ab-9cf11a33e3d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39630f51-266b-4679-b400-357a329ea849",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77ac5198-2d15-41c9-a0ab-9cf11a33e3d2",
                    "LayerId": "d7476a04-39d2-4570-b2c6-caa7d9678763"
                }
            ]
        },
        {
            "id": "94741e48-63d2-4670-bb03-3472b1f2d2f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91a346f6-2300-49c6-beab-bc341c651a49",
            "compositeImage": {
                "id": "5c751aae-fd8d-410e-8275-9ca5ad2972e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94741e48-63d2-4670-bb03-3472b1f2d2f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e43e3bf4-b911-443f-bb58-6c9b4b669ead",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94741e48-63d2-4670-bb03-3472b1f2d2f1",
                    "LayerId": "d7476a04-39d2-4570-b2c6-caa7d9678763"
                }
            ]
        },
        {
            "id": "8c1a8ff3-7a35-41d1-b71a-71114014bf8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91a346f6-2300-49c6-beab-bc341c651a49",
            "compositeImage": {
                "id": "67806104-3c79-41fc-b268-c7b250d636f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c1a8ff3-7a35-41d1-b71a-71114014bf8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "556594d3-adc6-4f99-8ce1-88f69dbd7f44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c1a8ff3-7a35-41d1-b71a-71114014bf8d",
                    "LayerId": "d7476a04-39d2-4570-b2c6-caa7d9678763"
                }
            ]
        },
        {
            "id": "fa2d0eec-c6c9-49f6-ad45-0ed824bfd5e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91a346f6-2300-49c6-beab-bc341c651a49",
            "compositeImage": {
                "id": "b32ce5f1-4c78-409c-9eb6-b0bc0c588a3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa2d0eec-c6c9-49f6-ad45-0ed824bfd5e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3405fd78-e91d-4251-8208-7f3a0e9f2eee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa2d0eec-c6c9-49f6-ad45-0ed824bfd5e1",
                    "LayerId": "d7476a04-39d2-4570-b2c6-caa7d9678763"
                }
            ]
        },
        {
            "id": "feac851e-de38-44d5-9305-ba4a79183385",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91a346f6-2300-49c6-beab-bc341c651a49",
            "compositeImage": {
                "id": "d0969ee2-4293-457e-bd66-2daf91c9b157",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "feac851e-de38-44d5-9305-ba4a79183385",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba189138-eb8b-4651-9ddd-fc685cc899ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "feac851e-de38-44d5-9305-ba4a79183385",
                    "LayerId": "d7476a04-39d2-4570-b2c6-caa7d9678763"
                }
            ]
        },
        {
            "id": "67b4c846-8374-4703-9b59-341b5a59024d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91a346f6-2300-49c6-beab-bc341c651a49",
            "compositeImage": {
                "id": "f83b3193-19a6-4938-ab74-4b8558129cc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67b4c846-8374-4703-9b59-341b5a59024d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce12c764-ea1a-4e99-a392-c4df5c7d413a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67b4c846-8374-4703-9b59-341b5a59024d",
                    "LayerId": "d7476a04-39d2-4570-b2c6-caa7d9678763"
                }
            ]
        },
        {
            "id": "b8cc90ab-55b8-44f5-9f6a-638f0f5054a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91a346f6-2300-49c6-beab-bc341c651a49",
            "compositeImage": {
                "id": "461b66c9-cb37-4a58-8763-b9fc96d0013f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8cc90ab-55b8-44f5-9f6a-638f0f5054a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bcecc4b-0dcd-4d49-8a50-166355a65a43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8cc90ab-55b8-44f5-9f6a-638f0f5054a0",
                    "LayerId": "d7476a04-39d2-4570-b2c6-caa7d9678763"
                }
            ]
        },
        {
            "id": "6a808271-2466-44b2-a241-358bd263118e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91a346f6-2300-49c6-beab-bc341c651a49",
            "compositeImage": {
                "id": "4ddd028e-95cd-40a0-8ea3-a3df3cd96eb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a808271-2466-44b2-a241-358bd263118e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20f47369-b0db-46de-b2c5-28bca301d14e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a808271-2466-44b2-a241-358bd263118e",
                    "LayerId": "d7476a04-39d2-4570-b2c6-caa7d9678763"
                }
            ]
        },
        {
            "id": "1d74da15-0fdc-45e9-a8e1-948db819b82c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91a346f6-2300-49c6-beab-bc341c651a49",
            "compositeImage": {
                "id": "70894a52-f636-490f-a2ff-e297a194d6b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d74da15-0fdc-45e9-a8e1-948db819b82c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de6ab261-1ec6-49c5-99c9-22f225669768",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d74da15-0fdc-45e9-a8e1-948db819b82c",
                    "LayerId": "d7476a04-39d2-4570-b2c6-caa7d9678763"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 51,
    "layers": [
        {
            "id": "d7476a04-39d2-4570-b2c6-caa7d9678763",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91a346f6-2300-49c6-beab-bc341c651a49",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 25
}