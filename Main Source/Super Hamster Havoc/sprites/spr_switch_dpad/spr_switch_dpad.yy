{
    "id": "170befe9-5bd2-47e8-9288-4bb99771836e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_switch_dpad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 4,
    "bbox_right": 95,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54468719-685e-4104-87ed-6d9c7fb1ed38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "170befe9-5bd2-47e8-9288-4bb99771836e",
            "compositeImage": {
                "id": "ab9fea40-1f73-4cc7-bd10-e95547ab166b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54468719-685e-4104-87ed-6d9c7fb1ed38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7344ea9-d09e-4b52-a163-83750306580d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54468719-685e-4104-87ed-6d9c7fb1ed38",
                    "LayerId": "4085d070-baac-4117-9ead-1292e4d53d61"
                }
            ]
        },
        {
            "id": "eaf2c255-d10b-434b-9b2b-5606b9b2cc13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "170befe9-5bd2-47e8-9288-4bb99771836e",
            "compositeImage": {
                "id": "d5ed8adc-8f93-420f-a882-dde97e113e0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaf2c255-d10b-434b-9b2b-5606b9b2cc13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b11e8603-0bfb-40c7-9e40-a7fa8678c1e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaf2c255-d10b-434b-9b2b-5606b9b2cc13",
                    "LayerId": "4085d070-baac-4117-9ead-1292e4d53d61"
                }
            ]
        },
        {
            "id": "a4edc032-a159-4df8-b857-addae104006f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "170befe9-5bd2-47e8-9288-4bb99771836e",
            "compositeImage": {
                "id": "4c651448-0dc7-41d1-80f4-cbbe2a48903e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4edc032-a159-4df8-b857-addae104006f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6396ba10-09b7-4b92-ad1a-6d511055b12e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4edc032-a159-4df8-b857-addae104006f",
                    "LayerId": "4085d070-baac-4117-9ead-1292e4d53d61"
                }
            ]
        },
        {
            "id": "f58e21fd-d156-438a-95f1-f85a01faad1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "170befe9-5bd2-47e8-9288-4bb99771836e",
            "compositeImage": {
                "id": "40ce6a5f-213c-41c9-bd2e-11756a965bfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f58e21fd-d156-438a-95f1-f85a01faad1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "177d6dfb-636e-451a-905a-15ed09f0aba3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f58e21fd-d156-438a-95f1-f85a01faad1b",
                    "LayerId": "4085d070-baac-4117-9ead-1292e4d53d61"
                }
            ]
        },
        {
            "id": "98135e19-5a25-4a60-aa54-8c21bc5af1a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "170befe9-5bd2-47e8-9288-4bb99771836e",
            "compositeImage": {
                "id": "24228649-3e86-4b2a-9052-428a3d216d9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98135e19-5a25-4a60-aa54-8c21bc5af1a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "518d6076-a632-4e0f-8b31-783e9cbba855",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98135e19-5a25-4a60-aa54-8c21bc5af1a7",
                    "LayerId": "4085d070-baac-4117-9ead-1292e4d53d61"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "4085d070-baac-4117-9ead-1292e4d53d61",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "170befe9-5bd2-47e8-9288-4bb99771836e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}