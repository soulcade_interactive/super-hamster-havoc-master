{
    "id": "4f53e52c-d394-4118-8415-45c9c45d0a0a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_select",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 2,
    "bbox_right": 15,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c059e878-2a03-419a-b47b-51cac2e83a58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f53e52c-d394-4118-8415-45c9c45d0a0a",
            "compositeImage": {
                "id": "e7109d97-2b08-4e85-ba1f-8f48b13a221f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c059e878-2a03-419a-b47b-51cac2e83a58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a0e30cd-b44e-4b0d-9df4-e0b2887c7d38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c059e878-2a03-419a-b47b-51cac2e83a58",
                    "LayerId": "1e8f693f-282e-4d34-88d9-f073a82914d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "1e8f693f-282e-4d34-88d9-f073a82914d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f53e52c-d394-4118-8415-45c9c45d0a0a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}