{
    "id": "b3a3f2c4-d32f-4e7f-b3b8-8b7f292a1ecf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_0_torso_u_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 23,
    "bbox_right": 40,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e125a2d4-d9e0-4c73-963b-f8418789cc43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3a3f2c4-d32f-4e7f-b3b8-8b7f292a1ecf",
            "compositeImage": {
                "id": "0b6581d1-5824-4230-af80-00b15ca2eeed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e125a2d4-d9e0-4c73-963b-f8418789cc43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca0bd3bb-307d-4b20-8d16-9a5efa269fb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e125a2d4-d9e0-4c73-963b-f8418789cc43",
                    "LayerId": "7811b5a9-3ddb-4b36-8bc2-a65c1c4a1874"
                }
            ]
        },
        {
            "id": "5532cebd-ba79-4e0b-8ca8-57457b540b5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3a3f2c4-d32f-4e7f-b3b8-8b7f292a1ecf",
            "compositeImage": {
                "id": "c9a17465-3fc9-44d6-a78c-141e73b77a06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5532cebd-ba79-4e0b-8ca8-57457b540b5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d91c4a6-3e43-4d6c-a641-c1f0d9f84580",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5532cebd-ba79-4e0b-8ca8-57457b540b5f",
                    "LayerId": "7811b5a9-3ddb-4b36-8bc2-a65c1c4a1874"
                }
            ]
        },
        {
            "id": "32033692-4f88-4803-b004-8df1d82ef835",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3a3f2c4-d32f-4e7f-b3b8-8b7f292a1ecf",
            "compositeImage": {
                "id": "721c18fb-1538-40f8-832b-c72bb6896ed4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32033692-4f88-4803-b004-8df1d82ef835",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b79de76e-b710-4b94-8259-b394ae621b3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32033692-4f88-4803-b004-8df1d82ef835",
                    "LayerId": "7811b5a9-3ddb-4b36-8bc2-a65c1c4a1874"
                }
            ]
        },
        {
            "id": "3e3bf26c-ff79-4aa6-ad36-9e2e3ef40138",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3a3f2c4-d32f-4e7f-b3b8-8b7f292a1ecf",
            "compositeImage": {
                "id": "8cc56823-bb2c-4730-bc7b-82b07ee63e85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e3bf26c-ff79-4aa6-ad36-9e2e3ef40138",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dec5717-59a5-447a-90fc-5a87b6e05c71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e3bf26c-ff79-4aa6-ad36-9e2e3ef40138",
                    "LayerId": "7811b5a9-3ddb-4b36-8bc2-a65c1c4a1874"
                }
            ]
        },
        {
            "id": "a132096c-074a-41e5-a96d-9bb9079168cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3a3f2c4-d32f-4e7f-b3b8-8b7f292a1ecf",
            "compositeImage": {
                "id": "b9858f98-2deb-4fd0-9f37-fd5e7017b6ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a132096c-074a-41e5-a96d-9bb9079168cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65c82076-f5a8-4c1e-b2f6-2e54e8401791",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a132096c-074a-41e5-a96d-9bb9079168cd",
                    "LayerId": "7811b5a9-3ddb-4b36-8bc2-a65c1c4a1874"
                }
            ]
        },
        {
            "id": "3bc0f0fe-362d-4f4d-9d84-ddcd6d7aa57a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3a3f2c4-d32f-4e7f-b3b8-8b7f292a1ecf",
            "compositeImage": {
                "id": "36a09a4b-ae72-4f2a-bb17-467337bfddc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bc0f0fe-362d-4f4d-9d84-ddcd6d7aa57a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c04ebb8f-3d25-4eeb-87ad-d94b2c0b27d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bc0f0fe-362d-4f4d-9d84-ddcd6d7aa57a",
                    "LayerId": "7811b5a9-3ddb-4b36-8bc2-a65c1c4a1874"
                }
            ]
        },
        {
            "id": "440c104e-0b65-4caf-bd03-d6b7f2aed0fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3a3f2c4-d32f-4e7f-b3b8-8b7f292a1ecf",
            "compositeImage": {
                "id": "d441c3bc-824d-4125-b243-5a6025075113",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "440c104e-0b65-4caf-bd03-d6b7f2aed0fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ece5a1b9-052a-45e9-a15a-aefb899f26a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "440c104e-0b65-4caf-bd03-d6b7f2aed0fb",
                    "LayerId": "7811b5a9-3ddb-4b36-8bc2-a65c1c4a1874"
                }
            ]
        },
        {
            "id": "7b56c25b-8f0b-474d-be74-2b906de188f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3a3f2c4-d32f-4e7f-b3b8-8b7f292a1ecf",
            "compositeImage": {
                "id": "0f56dbd5-d038-44be-a2c4-9ab7d80b7cbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b56c25b-8f0b-474d-be74-2b906de188f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a677e79-c8da-418b-8571-98493a9fe5af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b56c25b-8f0b-474d-be74-2b906de188f0",
                    "LayerId": "7811b5a9-3ddb-4b36-8bc2-a65c1c4a1874"
                }
            ]
        },
        {
            "id": "1695a1a6-fd27-41c8-ae39-01244ee6636c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3a3f2c4-d32f-4e7f-b3b8-8b7f292a1ecf",
            "compositeImage": {
                "id": "88affb28-0d90-4166-a3f6-7c10d9448fe8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1695a1a6-fd27-41c8-ae39-01244ee6636c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d30538a7-5769-4754-9a31-a7914e2d5e03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1695a1a6-fd27-41c8-ae39-01244ee6636c",
                    "LayerId": "7811b5a9-3ddb-4b36-8bc2-a65c1c4a1874"
                }
            ]
        },
        {
            "id": "88ca348c-471b-43ea-8912-d3ae8c292424",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b3a3f2c4-d32f-4e7f-b3b8-8b7f292a1ecf",
            "compositeImage": {
                "id": "a1295abc-272b-4cff-9dc6-8552b36a9171",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88ca348c-471b-43ea-8912-d3ae8c292424",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "020701b6-2674-445e-b4d7-fdfab850dcaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88ca348c-471b-43ea-8912-d3ae8c292424",
                    "LayerId": "7811b5a9-3ddb-4b36-8bc2-a65c1c4a1874"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7811b5a9-3ddb-4b36-8bc2-a65c1c4a1874",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b3a3f2c4-d32f-4e7f-b3b8-8b7f292a1ecf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}