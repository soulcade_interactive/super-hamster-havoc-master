{
    "id": "0584e0ab-35ca-4bb5-a27b-4db973a73bd2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_0_torso_d_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 23,
    "bbox_right": 40,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eab066c0-6962-42a8-b79d-04ce301bdb2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0584e0ab-35ca-4bb5-a27b-4db973a73bd2",
            "compositeImage": {
                "id": "9328e9d3-7bfa-4f09-85b3-08eb0a8ede2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eab066c0-6962-42a8-b79d-04ce301bdb2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a9a51d5-a355-47af-a0b3-f728a939eea7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eab066c0-6962-42a8-b79d-04ce301bdb2c",
                    "LayerId": "23591952-fa85-472b-9e94-b0518b955f17"
                }
            ]
        },
        {
            "id": "4d23a271-d1c4-41a2-aa3f-b54d8a2d6fae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0584e0ab-35ca-4bb5-a27b-4db973a73bd2",
            "compositeImage": {
                "id": "ae03d919-9f29-42d0-aafc-075ee041f7b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d23a271-d1c4-41a2-aa3f-b54d8a2d6fae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67db8148-1ec7-4214-a00b-dfc0a721fa7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d23a271-d1c4-41a2-aa3f-b54d8a2d6fae",
                    "LayerId": "23591952-fa85-472b-9e94-b0518b955f17"
                }
            ]
        },
        {
            "id": "746df9f6-baa0-49a9-906b-f7990ce5ce68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0584e0ab-35ca-4bb5-a27b-4db973a73bd2",
            "compositeImage": {
                "id": "7109b737-eab8-4cc6-833c-d02e9d5ffccf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "746df9f6-baa0-49a9-906b-f7990ce5ce68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4133cee3-5078-41da-92c8-fd38967e9ff6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "746df9f6-baa0-49a9-906b-f7990ce5ce68",
                    "LayerId": "23591952-fa85-472b-9e94-b0518b955f17"
                }
            ]
        },
        {
            "id": "56eb1b5f-1975-4f3c-9296-5d372d4f2a96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0584e0ab-35ca-4bb5-a27b-4db973a73bd2",
            "compositeImage": {
                "id": "b9c7d191-1975-4c87-a1e0-11a358da527d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56eb1b5f-1975-4f3c-9296-5d372d4f2a96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c968c79-11ff-4ab6-9bbf-cbc2d032d9ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56eb1b5f-1975-4f3c-9296-5d372d4f2a96",
                    "LayerId": "23591952-fa85-472b-9e94-b0518b955f17"
                }
            ]
        },
        {
            "id": "3753042c-50d8-404e-b3c8-35f0d2b15081",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0584e0ab-35ca-4bb5-a27b-4db973a73bd2",
            "compositeImage": {
                "id": "3640fbb7-7150-4518-bdfc-ecda0c6e8fd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3753042c-50d8-404e-b3c8-35f0d2b15081",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7535064f-7f2c-4761-b363-00aa5851b342",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3753042c-50d8-404e-b3c8-35f0d2b15081",
                    "LayerId": "23591952-fa85-472b-9e94-b0518b955f17"
                }
            ]
        },
        {
            "id": "03a162f8-6884-4b32-8b14-34a90a6565e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0584e0ab-35ca-4bb5-a27b-4db973a73bd2",
            "compositeImage": {
                "id": "c2c41878-3dc8-43d5-b213-9055e5297e94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03a162f8-6884-4b32-8b14-34a90a6565e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e520afd5-e66a-4e88-9833-64623ae33dc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03a162f8-6884-4b32-8b14-34a90a6565e5",
                    "LayerId": "23591952-fa85-472b-9e94-b0518b955f17"
                }
            ]
        },
        {
            "id": "eca8934b-7a18-4b87-a19e-18b809061f6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0584e0ab-35ca-4bb5-a27b-4db973a73bd2",
            "compositeImage": {
                "id": "89cdb175-455f-4cae-8ffd-47a3db63291f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eca8934b-7a18-4b87-a19e-18b809061f6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69d20a3c-f924-4431-ab45-bb650c7f9353",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eca8934b-7a18-4b87-a19e-18b809061f6e",
                    "LayerId": "23591952-fa85-472b-9e94-b0518b955f17"
                }
            ]
        },
        {
            "id": "80881eec-56bd-43a4-b11e-30dfa103096f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0584e0ab-35ca-4bb5-a27b-4db973a73bd2",
            "compositeImage": {
                "id": "56047ea8-7a7a-4e91-957e-a4a44cde3696",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80881eec-56bd-43a4-b11e-30dfa103096f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d1cb04a-c3fb-4fde-ae61-d3db94b233f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80881eec-56bd-43a4-b11e-30dfa103096f",
                    "LayerId": "23591952-fa85-472b-9e94-b0518b955f17"
                }
            ]
        },
        {
            "id": "2b6f3a56-e497-4724-9dc0-7e7bbb9a6148",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0584e0ab-35ca-4bb5-a27b-4db973a73bd2",
            "compositeImage": {
                "id": "8f5995ce-a969-42f7-bacf-bd9a64482365",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b6f3a56-e497-4724-9dc0-7e7bbb9a6148",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1cbeffd-b7a6-4480-86d2-ecd9e6c980c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b6f3a56-e497-4724-9dc0-7e7bbb9a6148",
                    "LayerId": "23591952-fa85-472b-9e94-b0518b955f17"
                }
            ]
        },
        {
            "id": "641bbfd3-c505-4bf4-b509-16d399fd6ef9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0584e0ab-35ca-4bb5-a27b-4db973a73bd2",
            "compositeImage": {
                "id": "f21f8484-9d1a-47f9-9c25-3b095d729d1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "641bbfd3-c505-4bf4-b509-16d399fd6ef9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03f5bd34-cc49-4004-975b-eb2a1d04a2e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "641bbfd3-c505-4bf4-b509-16d399fd6ef9",
                    "LayerId": "23591952-fa85-472b-9e94-b0518b955f17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "23591952-fa85-472b-9e94-b0518b955f17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0584e0ab-35ca-4bb5-a27b-4db973a73bd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}