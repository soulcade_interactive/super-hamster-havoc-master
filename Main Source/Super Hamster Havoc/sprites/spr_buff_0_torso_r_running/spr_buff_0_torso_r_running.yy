{
    "id": "b0e38a18-4f9c-42a9-b9e9-81e4f95e90e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buff_0_torso_r_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 7,
    "bbox_right": 56,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86e8d7fc-9dce-4bde-b868-45ecb7a38fd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e38a18-4f9c-42a9-b9e9-81e4f95e90e3",
            "compositeImage": {
                "id": "bae1d883-cf64-4b9c-a16f-6387e68e6042",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86e8d7fc-9dce-4bde-b868-45ecb7a38fd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6ab8371-79d2-414e-9dc3-f8ebf1b92940",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86e8d7fc-9dce-4bde-b868-45ecb7a38fd2",
                    "LayerId": "9e1958a1-5918-43a9-81f2-9453dd853c67"
                }
            ]
        },
        {
            "id": "53cfbe51-4cae-479e-ac99-90d33967461c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e38a18-4f9c-42a9-b9e9-81e4f95e90e3",
            "compositeImage": {
                "id": "e41909cb-a493-49e5-b6be-c45f7327985b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53cfbe51-4cae-479e-ac99-90d33967461c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71f94d43-d102-4d34-b867-87efd89c7397",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53cfbe51-4cae-479e-ac99-90d33967461c",
                    "LayerId": "9e1958a1-5918-43a9-81f2-9453dd853c67"
                }
            ]
        },
        {
            "id": "1d7259d8-a0b4-4620-8085-44cc6e8da08c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e38a18-4f9c-42a9-b9e9-81e4f95e90e3",
            "compositeImage": {
                "id": "63aa91ab-250f-43bb-8328-aff2c33e1a24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d7259d8-a0b4-4620-8085-44cc6e8da08c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "563db275-1371-42ef-a7dc-413e5b058f38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d7259d8-a0b4-4620-8085-44cc6e8da08c",
                    "LayerId": "9e1958a1-5918-43a9-81f2-9453dd853c67"
                }
            ]
        },
        {
            "id": "a945fe08-7d59-4748-8d93-ed3cf5de55fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e38a18-4f9c-42a9-b9e9-81e4f95e90e3",
            "compositeImage": {
                "id": "dbffe0ed-2034-4918-8969-e9486de61f6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a945fe08-7d59-4748-8d93-ed3cf5de55fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04ebba5a-bb32-416d-918c-6a205c5bd1f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a945fe08-7d59-4748-8d93-ed3cf5de55fd",
                    "LayerId": "9e1958a1-5918-43a9-81f2-9453dd853c67"
                }
            ]
        },
        {
            "id": "13adc2a2-53b1-4c39-b909-329080b56130",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e38a18-4f9c-42a9-b9e9-81e4f95e90e3",
            "compositeImage": {
                "id": "d18d0d5f-3cf8-4da2-b474-823d71fb8400",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13adc2a2-53b1-4c39-b909-329080b56130",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e5467c9-5fc5-4f88-9a29-c48ab3feb10f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13adc2a2-53b1-4c39-b909-329080b56130",
                    "LayerId": "9e1958a1-5918-43a9-81f2-9453dd853c67"
                }
            ]
        },
        {
            "id": "8e65d9fb-5a07-4557-bbba-278f4f246dc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e38a18-4f9c-42a9-b9e9-81e4f95e90e3",
            "compositeImage": {
                "id": "1a7b6a3b-6c68-4e71-abe6-07eb20b485fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e65d9fb-5a07-4557-bbba-278f4f246dc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "907fbf05-44ac-47fd-b744-17e09a0d4fe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e65d9fb-5a07-4557-bbba-278f4f246dc7",
                    "LayerId": "9e1958a1-5918-43a9-81f2-9453dd853c67"
                }
            ]
        },
        {
            "id": "6aba88cc-2c90-443b-a460-f1b8d4dcd409",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e38a18-4f9c-42a9-b9e9-81e4f95e90e3",
            "compositeImage": {
                "id": "61dd883d-6427-4211-9801-693e1a29a8ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aba88cc-2c90-443b-a460-f1b8d4dcd409",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a507cccd-f252-40c1-abe1-81be9df44f95",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aba88cc-2c90-443b-a460-f1b8d4dcd409",
                    "LayerId": "9e1958a1-5918-43a9-81f2-9453dd853c67"
                }
            ]
        },
        {
            "id": "23628fa2-1118-4e18-a748-3307e2b0c814",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e38a18-4f9c-42a9-b9e9-81e4f95e90e3",
            "compositeImage": {
                "id": "2aba4ba8-f2e4-4391-ab0d-577c3bca13f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23628fa2-1118-4e18-a748-3307e2b0c814",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4047dfc1-b574-46a5-aae6-4ac8d8ad7df7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23628fa2-1118-4e18-a748-3307e2b0c814",
                    "LayerId": "9e1958a1-5918-43a9-81f2-9453dd853c67"
                }
            ]
        },
        {
            "id": "bfdfa44f-00f7-4775-8076-d192c4097bba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e38a18-4f9c-42a9-b9e9-81e4f95e90e3",
            "compositeImage": {
                "id": "c4bec68f-e998-4c4d-a598-340b40c152ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfdfa44f-00f7-4775-8076-d192c4097bba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95534003-2198-48a0-ac79-cc2dce244547",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfdfa44f-00f7-4775-8076-d192c4097bba",
                    "LayerId": "9e1958a1-5918-43a9-81f2-9453dd853c67"
                }
            ]
        },
        {
            "id": "59fb313f-ac2a-4806-91ee-836ed5ddf127",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0e38a18-4f9c-42a9-b9e9-81e4f95e90e3",
            "compositeImage": {
                "id": "203659b4-6513-4118-a7cf-715dc326d390",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59fb313f-ac2a-4806-91ee-836ed5ddf127",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0da4733b-8c36-4959-a467-001cbb444cfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59fb313f-ac2a-4806-91ee-836ed5ddf127",
                    "LayerId": "9e1958a1-5918-43a9-81f2-9453dd853c67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9e1958a1-5918-43a9-81f2-9453dd853c67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0e38a18-4f9c-42a9-b9e9-81e4f95e90e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}