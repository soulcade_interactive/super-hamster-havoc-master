{
    "id": "77b298e4-c7a2-452c-850b-abf01cc060b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blood",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37026415-43db-4798-b483-6bd22f416f3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b298e4-c7a2-452c-850b-abf01cc060b0",
            "compositeImage": {
                "id": "8be779c7-b32e-4488-98bd-6e81d30f8908",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37026415-43db-4798-b483-6bd22f416f3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29bca823-92d7-4ad2-b2c7-35062a9e8b33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37026415-43db-4798-b483-6bd22f416f3b",
                    "LayerId": "20d932b5-25c4-44aa-91bb-a45487f20498"
                }
            ]
        },
        {
            "id": "9f695fa5-a4f8-4c8b-a1ef-7bf5a0738b83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b298e4-c7a2-452c-850b-abf01cc060b0",
            "compositeImage": {
                "id": "7fe3eccc-1fa9-4585-b812-871a95e86e94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f695fa5-a4f8-4c8b-a1ef-7bf5a0738b83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a32e81cd-d9f5-4c0e-8d2a-f181ead2b585",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f695fa5-a4f8-4c8b-a1ef-7bf5a0738b83",
                    "LayerId": "20d932b5-25c4-44aa-91bb-a45487f20498"
                }
            ]
        },
        {
            "id": "94233a36-a573-4174-b211-cff2ef548f1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b298e4-c7a2-452c-850b-abf01cc060b0",
            "compositeImage": {
                "id": "a7b9afc4-7c1d-486b-8774-23a70e2bf787",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94233a36-a573-4174-b211-cff2ef548f1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb69136a-e0c2-4df1-ad48-20a181b0dcf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94233a36-a573-4174-b211-cff2ef548f1b",
                    "LayerId": "20d932b5-25c4-44aa-91bb-a45487f20498"
                }
            ]
        },
        {
            "id": "faae0acd-bf8e-4490-91f2-033bfa635197",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b298e4-c7a2-452c-850b-abf01cc060b0",
            "compositeImage": {
                "id": "c747fd6d-df49-49c0-a7e2-f94b53adb1b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "faae0acd-bf8e-4490-91f2-033bfa635197",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afd183bf-fa72-47b9-ad44-96779399f252",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "faae0acd-bf8e-4490-91f2-033bfa635197",
                    "LayerId": "20d932b5-25c4-44aa-91bb-a45487f20498"
                }
            ]
        },
        {
            "id": "285ab794-baa1-459b-a556-d01645007e73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b298e4-c7a2-452c-850b-abf01cc060b0",
            "compositeImage": {
                "id": "d2634997-6e6e-445c-aad5-03ea0194f2ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "285ab794-baa1-459b-a556-d01645007e73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95a2e202-566b-4203-aefb-c986e7e35bc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "285ab794-baa1-459b-a556-d01645007e73",
                    "LayerId": "20d932b5-25c4-44aa-91bb-a45487f20498"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "20d932b5-25c4-44aa-91bb-a45487f20498",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77b298e4-c7a2-452c-850b-abf01cc060b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}