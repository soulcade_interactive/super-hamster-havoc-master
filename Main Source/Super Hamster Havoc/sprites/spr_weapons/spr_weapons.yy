{
    "id": "12aa13b5-28ae-4fde-8206-b51fdb1f83b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_weapons",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4a63ebb-5825-456b-8d1d-521b454e6e50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12aa13b5-28ae-4fde-8206-b51fdb1f83b8",
            "compositeImage": {
                "id": "eebf217d-3f3e-462b-841c-fb4b8b5935e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4a63ebb-5825-456b-8d1d-521b454e6e50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "001cd594-9670-4f08-9ae6-4ce4a093b842",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4a63ebb-5825-456b-8d1d-521b454e6e50",
                    "LayerId": "93d74355-9ac3-406c-b277-c6768a2de4c7"
                }
            ]
        },
        {
            "id": "9140b193-d3e8-4bfe-9de0-f486512ef74a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12aa13b5-28ae-4fde-8206-b51fdb1f83b8",
            "compositeImage": {
                "id": "d978a8e6-1642-40ea-b0d7-bd0cedcf1d46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9140b193-d3e8-4bfe-9de0-f486512ef74a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1deb8e88-28ea-407c-b49b-3d8c753fabc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9140b193-d3e8-4bfe-9de0-f486512ef74a",
                    "LayerId": "93d74355-9ac3-406c-b277-c6768a2de4c7"
                }
            ]
        },
        {
            "id": "5d3c59cd-914e-4ec9-b7bb-998a40ffc6a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12aa13b5-28ae-4fde-8206-b51fdb1f83b8",
            "compositeImage": {
                "id": "073553e8-2bc4-4b60-a7a4-3b79544110ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d3c59cd-914e-4ec9-b7bb-998a40ffc6a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cff5705-1d9c-47ea-b776-049c5a24c0bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d3c59cd-914e-4ec9-b7bb-998a40ffc6a6",
                    "LayerId": "93d74355-9ac3-406c-b277-c6768a2de4c7"
                }
            ]
        },
        {
            "id": "ae73e007-46af-4a48-ad93-84aa3c200b35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12aa13b5-28ae-4fde-8206-b51fdb1f83b8",
            "compositeImage": {
                "id": "ab355a57-7073-47a7-8b71-ec3397d6bcc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae73e007-46af-4a48-ad93-84aa3c200b35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69efb539-e564-4701-a156-68ff2a48f1ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae73e007-46af-4a48-ad93-84aa3c200b35",
                    "LayerId": "93d74355-9ac3-406c-b277-c6768a2de4c7"
                }
            ]
        },
        {
            "id": "86383013-04d4-41dd-9411-63ef0c5fb4ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12aa13b5-28ae-4fde-8206-b51fdb1f83b8",
            "compositeImage": {
                "id": "7cb98437-aa9c-4aa5-8f7f-d0b823991469",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86383013-04d4-41dd-9411-63ef0c5fb4ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0ca1c18-d602-49b2-9d1a-2c98d447d551",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86383013-04d4-41dd-9411-63ef0c5fb4ed",
                    "LayerId": "93d74355-9ac3-406c-b277-c6768a2de4c7"
                }
            ]
        },
        {
            "id": "417ed40b-d1ca-467f-ab2e-f98b6fd5ce74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12aa13b5-28ae-4fde-8206-b51fdb1f83b8",
            "compositeImage": {
                "id": "57d924f6-108e-4a99-802f-5907a80e455a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "417ed40b-d1ca-467f-ab2e-f98b6fd5ce74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a5e2a9e-9e80-4e95-9016-a53e2a89e0d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "417ed40b-d1ca-467f-ab2e-f98b6fd5ce74",
                    "LayerId": "93d74355-9ac3-406c-b277-c6768a2de4c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "93d74355-9ac3-406c-b277-c6768a2de4c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12aa13b5-28ae-4fde-8206-b51fdb1f83b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}