{
    "id": "afd04b06-cf89-4a64-b8a6-f672ae2e7e9c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buff_0_legs_u_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 19,
    "bbox_right": 44,
    "bbox_top": 43,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7badc32-1097-4d42-b3e3-41d1961dd812",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afd04b06-cf89-4a64-b8a6-f672ae2e7e9c",
            "compositeImage": {
                "id": "8db8c66f-60fc-4be9-939c-9bd6a69548fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7badc32-1097-4d42-b3e3-41d1961dd812",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25abebff-93d9-4322-8df7-08a2777954d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7badc32-1097-4d42-b3e3-41d1961dd812",
                    "LayerId": "d5cf6bd6-e1a4-404b-9873-017ed8a2a687"
                }
            ]
        },
        {
            "id": "47228594-e4e2-4647-a4a0-14812a2b4d35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afd04b06-cf89-4a64-b8a6-f672ae2e7e9c",
            "compositeImage": {
                "id": "a1f4ce15-a47f-45d4-b0d0-79e0c724a679",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47228594-e4e2-4647-a4a0-14812a2b4d35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7f45441-aa0f-41e5-b251-e067e739628d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47228594-e4e2-4647-a4a0-14812a2b4d35",
                    "LayerId": "d5cf6bd6-e1a4-404b-9873-017ed8a2a687"
                }
            ]
        },
        {
            "id": "75aac4ba-aa79-4fcf-9a8a-9152fbacead0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afd04b06-cf89-4a64-b8a6-f672ae2e7e9c",
            "compositeImage": {
                "id": "d5e02ebd-a7e7-4c4e-8909-a5a26a0d0457",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75aac4ba-aa79-4fcf-9a8a-9152fbacead0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb6a74c9-ae76-4e19-9e9a-7a6624afc79b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75aac4ba-aa79-4fcf-9a8a-9152fbacead0",
                    "LayerId": "d5cf6bd6-e1a4-404b-9873-017ed8a2a687"
                }
            ]
        },
        {
            "id": "b0905fb7-6b1b-463b-849b-7e9ae25ae627",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afd04b06-cf89-4a64-b8a6-f672ae2e7e9c",
            "compositeImage": {
                "id": "d500958d-3915-448d-9810-238ae86c4410",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0905fb7-6b1b-463b-849b-7e9ae25ae627",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6eb4bff-c8cc-4cc7-9dc6-7375ef64b596",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0905fb7-6b1b-463b-849b-7e9ae25ae627",
                    "LayerId": "d5cf6bd6-e1a4-404b-9873-017ed8a2a687"
                }
            ]
        },
        {
            "id": "2e702747-5fde-4d27-a7a8-6ab711372059",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afd04b06-cf89-4a64-b8a6-f672ae2e7e9c",
            "compositeImage": {
                "id": "23c63fe8-f150-4151-9e8f-563a4a87b9cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e702747-5fde-4d27-a7a8-6ab711372059",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "998803f5-fa4a-41f0-b8a9-f78f45579355",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e702747-5fde-4d27-a7a8-6ab711372059",
                    "LayerId": "d5cf6bd6-e1a4-404b-9873-017ed8a2a687"
                }
            ]
        },
        {
            "id": "b42eda8d-21ab-4966-87c1-59da0496f406",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afd04b06-cf89-4a64-b8a6-f672ae2e7e9c",
            "compositeImage": {
                "id": "6982fa5c-c496-48ab-b7d0-f791a81099a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b42eda8d-21ab-4966-87c1-59da0496f406",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d4a3d98-a588-48a9-b776-286e4afa94cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b42eda8d-21ab-4966-87c1-59da0496f406",
                    "LayerId": "d5cf6bd6-e1a4-404b-9873-017ed8a2a687"
                }
            ]
        },
        {
            "id": "3300b43b-cf49-48b5-9e07-952ac1059638",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afd04b06-cf89-4a64-b8a6-f672ae2e7e9c",
            "compositeImage": {
                "id": "48eb8ddf-ec6e-449c-bec0-da859731fe29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3300b43b-cf49-48b5-9e07-952ac1059638",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdfb8927-2c1d-4f04-89e0-efc4b9735a63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3300b43b-cf49-48b5-9e07-952ac1059638",
                    "LayerId": "d5cf6bd6-e1a4-404b-9873-017ed8a2a687"
                }
            ]
        },
        {
            "id": "d59fb75d-6cfa-42b5-83bf-d94600f34019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afd04b06-cf89-4a64-b8a6-f672ae2e7e9c",
            "compositeImage": {
                "id": "af53d1df-0af1-4cc4-ae15-73591ea2bcfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d59fb75d-6cfa-42b5-83bf-d94600f34019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b117de97-24c8-4091-9e51-f22a6be5e826",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d59fb75d-6cfa-42b5-83bf-d94600f34019",
                    "LayerId": "d5cf6bd6-e1a4-404b-9873-017ed8a2a687"
                }
            ]
        },
        {
            "id": "9e42753c-8832-49d5-bcaf-913355a039ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afd04b06-cf89-4a64-b8a6-f672ae2e7e9c",
            "compositeImage": {
                "id": "ed7d4de1-5a28-4124-aa55-213b0450a5c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e42753c-8832-49d5-bcaf-913355a039ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09b83710-4d95-4cf9-b7b7-61e49562638a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e42753c-8832-49d5-bcaf-913355a039ca",
                    "LayerId": "d5cf6bd6-e1a4-404b-9873-017ed8a2a687"
                }
            ]
        },
        {
            "id": "5c16026d-abe8-4d35-b46b-517de924cadc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afd04b06-cf89-4a64-b8a6-f672ae2e7e9c",
            "compositeImage": {
                "id": "8e20086d-270d-4e97-a189-40d3dfd3f5f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c16026d-abe8-4d35-b46b-517de924cadc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20fdcb67-196d-48bf-a138-9499743a5f27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c16026d-abe8-4d35-b46b-517de924cadc",
                    "LayerId": "d5cf6bd6-e1a4-404b-9873-017ed8a2a687"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d5cf6bd6-e1a4-404b-9873-017ed8a2a687",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "afd04b06-cf89-4a64-b8a6-f672ae2e7e9c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}