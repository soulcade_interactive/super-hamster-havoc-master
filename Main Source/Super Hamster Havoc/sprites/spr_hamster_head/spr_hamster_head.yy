{
    "id": "20e40e41-cac4-4bad-a41d-542716c3f2d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_head",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d0caa15a-c152-4f4d-af6d-8ea626e345e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20e40e41-cac4-4bad-a41d-542716c3f2d4",
            "compositeImage": {
                "id": "d698c87c-1f3a-4885-98b2-31c436fa521a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0caa15a-c152-4f4d-af6d-8ea626e345e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33f2e22e-c816-446d-beee-b78945a48e2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0caa15a-c152-4f4d-af6d-8ea626e345e9",
                    "LayerId": "3e5a87f9-b750-4df3-b92b-dd7a2be2b76d"
                }
            ]
        },
        {
            "id": "2fc6cdbc-cfe6-4d8b-90d3-5467779e6601",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20e40e41-cac4-4bad-a41d-542716c3f2d4",
            "compositeImage": {
                "id": "6afa4fce-933b-4ed5-95da-621417e49535",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fc6cdbc-cfe6-4d8b-90d3-5467779e6601",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caeae0a3-e5e7-4784-a14f-13a25f09e75a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fc6cdbc-cfe6-4d8b-90d3-5467779e6601",
                    "LayerId": "3e5a87f9-b750-4df3-b92b-dd7a2be2b76d"
                }
            ]
        },
        {
            "id": "fa6acb0c-1bdc-46ec-82fc-33d835c4582f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20e40e41-cac4-4bad-a41d-542716c3f2d4",
            "compositeImage": {
                "id": "d95a5532-7db5-415c-929a-f80ded8dcd89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa6acb0c-1bdc-46ec-82fc-33d835c4582f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d38b155e-19d6-48f3-bc90-2cf4b953d2c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa6acb0c-1bdc-46ec-82fc-33d835c4582f",
                    "LayerId": "3e5a87f9-b750-4df3-b92b-dd7a2be2b76d"
                }
            ]
        },
        {
            "id": "0c8a6f0a-1439-453e-be26-a4d4368e1525",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20e40e41-cac4-4bad-a41d-542716c3f2d4",
            "compositeImage": {
                "id": "651c7c3d-f061-4820-8d59-d4699d439a63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c8a6f0a-1439-453e-be26-a4d4368e1525",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99d65d03-bde6-4e04-a058-924960e6972c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c8a6f0a-1439-453e-be26-a4d4368e1525",
                    "LayerId": "3e5a87f9-b750-4df3-b92b-dd7a2be2b76d"
                }
            ]
        },
        {
            "id": "5fecbd6f-6a9c-47ae-8ff2-f8b23dfdda41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20e40e41-cac4-4bad-a41d-542716c3f2d4",
            "compositeImage": {
                "id": "e8a89ba7-9327-4ba6-93e0-3dc80f386ee8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fecbd6f-6a9c-47ae-8ff2-f8b23dfdda41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37093869-fd20-4613-9a7a-4ed472813ecf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fecbd6f-6a9c-47ae-8ff2-f8b23dfdda41",
                    "LayerId": "3e5a87f9-b750-4df3-b92b-dd7a2be2b76d"
                }
            ]
        },
        {
            "id": "661bbbb6-e590-4cdf-a241-48beb1d6f6ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20e40e41-cac4-4bad-a41d-542716c3f2d4",
            "compositeImage": {
                "id": "4bc0267b-0b76-44a6-81b7-57bb3be2c5ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "661bbbb6-e590-4cdf-a241-48beb1d6f6ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c23b410e-d53f-422f-89e8-24e78d82a3f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "661bbbb6-e590-4cdf-a241-48beb1d6f6ed",
                    "LayerId": "3e5a87f9-b750-4df3-b92b-dd7a2be2b76d"
                }
            ]
        },
        {
            "id": "ceacdf5b-330f-46b5-9dd1-7ca2c47b8b93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20e40e41-cac4-4bad-a41d-542716c3f2d4",
            "compositeImage": {
                "id": "7646d5fb-dd02-427f-8b2e-61285bad009e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ceacdf5b-330f-46b5-9dd1-7ca2c47b8b93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4459983-fc04-4871-8d6a-2c659ada91f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ceacdf5b-330f-46b5-9dd1-7ca2c47b8b93",
                    "LayerId": "3e5a87f9-b750-4df3-b92b-dd7a2be2b76d"
                }
            ]
        },
        {
            "id": "79e70028-9152-4013-b57c-5fa2fae1974e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20e40e41-cac4-4bad-a41d-542716c3f2d4",
            "compositeImage": {
                "id": "5d24019c-301f-46bc-abe5-dc457e3cba34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79e70028-9152-4013-b57c-5fa2fae1974e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f3d6285-cea1-43b5-ac22-2d82e1c1868a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79e70028-9152-4013-b57c-5fa2fae1974e",
                    "LayerId": "3e5a87f9-b750-4df3-b92b-dd7a2be2b76d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3e5a87f9-b750-4df3-b92b-dd7a2be2b76d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20e40e41-cac4-4bad-a41d-542716c3f2d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}