{
    "id": "44773cd1-36aa-4f7a-938c-0b86d6c4ec97",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nerd_0_torso_r_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 22,
    "bbox_right": 39,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98913c35-06f8-42ba-a398-9793fb82664b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44773cd1-36aa-4f7a-938c-0b86d6c4ec97",
            "compositeImage": {
                "id": "724019de-9022-4fc7-a1bd-15d1abf72b49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98913c35-06f8-42ba-a398-9793fb82664b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a13bad5-6ece-47af-b954-f0a83449bf8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98913c35-06f8-42ba-a398-9793fb82664b",
                    "LayerId": "a0480c0b-88fe-4b67-aa93-09f8fcbf0e34"
                }
            ]
        },
        {
            "id": "a80ccf36-b041-4920-bf2e-9975234b2b2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44773cd1-36aa-4f7a-938c-0b86d6c4ec97",
            "compositeImage": {
                "id": "4afd6166-e95e-440c-81bc-a0bc8c913e2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a80ccf36-b041-4920-bf2e-9975234b2b2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa500463-396a-444e-91a2-074220d48462",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a80ccf36-b041-4920-bf2e-9975234b2b2b",
                    "LayerId": "a0480c0b-88fe-4b67-aa93-09f8fcbf0e34"
                }
            ]
        },
        {
            "id": "63c908cd-2f69-40de-8092-4972992150e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44773cd1-36aa-4f7a-938c-0b86d6c4ec97",
            "compositeImage": {
                "id": "aeb1c6a1-8eff-4892-8d0a-800dfd4aadff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63c908cd-2f69-40de-8092-4972992150e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7eb37cc-0741-42fe-9118-14f6d985d53e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63c908cd-2f69-40de-8092-4972992150e2",
                    "LayerId": "a0480c0b-88fe-4b67-aa93-09f8fcbf0e34"
                }
            ]
        },
        {
            "id": "a96787fd-8fff-4e30-a12f-a869d558284e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44773cd1-36aa-4f7a-938c-0b86d6c4ec97",
            "compositeImage": {
                "id": "741977ff-dd44-4052-a21d-3f8af37c4c09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a96787fd-8fff-4e30-a12f-a869d558284e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2fe9c59-dd1b-47c5-9c7b-613d9a3ead10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a96787fd-8fff-4e30-a12f-a869d558284e",
                    "LayerId": "a0480c0b-88fe-4b67-aa93-09f8fcbf0e34"
                }
            ]
        },
        {
            "id": "85bb1b94-7670-425f-a89f-582f1df1f8ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44773cd1-36aa-4f7a-938c-0b86d6c4ec97",
            "compositeImage": {
                "id": "93bbf87d-da91-4fcf-ab01-2f2cc1ea47f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85bb1b94-7670-425f-a89f-582f1df1f8ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "552dc27f-61a6-4597-b48a-f7f7f0de49dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85bb1b94-7670-425f-a89f-582f1df1f8ff",
                    "LayerId": "a0480c0b-88fe-4b67-aa93-09f8fcbf0e34"
                }
            ]
        },
        {
            "id": "9ec5066d-f738-40fb-865d-c02b8c43a8c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44773cd1-36aa-4f7a-938c-0b86d6c4ec97",
            "compositeImage": {
                "id": "4151985e-6aa7-433c-ba2a-a573b8eed8b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ec5066d-f738-40fb-865d-c02b8c43a8c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e057edf-2e13-4902-a023-be1d7570f43a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ec5066d-f738-40fb-865d-c02b8c43a8c2",
                    "LayerId": "a0480c0b-88fe-4b67-aa93-09f8fcbf0e34"
                }
            ]
        },
        {
            "id": "9b0ecaf8-461f-4a06-92a6-83c4cf79d060",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44773cd1-36aa-4f7a-938c-0b86d6c4ec97",
            "compositeImage": {
                "id": "99af04e3-e697-40e6-91cf-171d41f1bfb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b0ecaf8-461f-4a06-92a6-83c4cf79d060",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08040066-cc89-4596-a109-5bed9b141af5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b0ecaf8-461f-4a06-92a6-83c4cf79d060",
                    "LayerId": "a0480c0b-88fe-4b67-aa93-09f8fcbf0e34"
                }
            ]
        },
        {
            "id": "e311d109-a72d-4db6-be37-45686c2b3872",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44773cd1-36aa-4f7a-938c-0b86d6c4ec97",
            "compositeImage": {
                "id": "0a8583a5-b908-4b6b-bdff-41a07dc9c49a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e311d109-a72d-4db6-be37-45686c2b3872",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "184bde5b-a8ae-4d09-a1e2-2a06f656ca97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e311d109-a72d-4db6-be37-45686c2b3872",
                    "LayerId": "a0480c0b-88fe-4b67-aa93-09f8fcbf0e34"
                }
            ]
        },
        {
            "id": "577c6119-fa88-48eb-9f09-7eb899e08ce3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44773cd1-36aa-4f7a-938c-0b86d6c4ec97",
            "compositeImage": {
                "id": "6dc1dcc2-dbe0-42c9-a106-917d435e9629",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "577c6119-fa88-48eb-9f09-7eb899e08ce3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03984434-866d-4c61-b615-e8a80456eadf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "577c6119-fa88-48eb-9f09-7eb899e08ce3",
                    "LayerId": "a0480c0b-88fe-4b67-aa93-09f8fcbf0e34"
                }
            ]
        },
        {
            "id": "077f573a-6016-4fe9-a855-0d8ccc70fb06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "44773cd1-36aa-4f7a-938c-0b86d6c4ec97",
            "compositeImage": {
                "id": "1694925d-7582-4fc5-9101-4bc1cbe6cbed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "077f573a-6016-4fe9-a855-0d8ccc70fb06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52460f70-f0a4-4b42-8450-ec517f35a7a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "077f573a-6016-4fe9-a855-0d8ccc70fb06",
                    "LayerId": "a0480c0b-88fe-4b67-aa93-09f8fcbf0e34"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a0480c0b-88fe-4b67-aa93-09f8fcbf0e34",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "44773cd1-36aa-4f7a-938c-0b86d6c4ec97",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}