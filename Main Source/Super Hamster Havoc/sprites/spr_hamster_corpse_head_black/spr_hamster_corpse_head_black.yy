{
    "id": "d3f31d27-48ce-4ea9-af75-5fca8b8203c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_corpse_head_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "408bc393-ea71-4b06-bba0-068962666521",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3f31d27-48ce-4ea9-af75-5fca8b8203c7",
            "compositeImage": {
                "id": "98747dfe-3bce-430e-b33e-b516a67a38f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "408bc393-ea71-4b06-bba0-068962666521",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "497fc0c2-9e05-42ce-a5af-50850957352c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "408bc393-ea71-4b06-bba0-068962666521",
                    "LayerId": "743d5a1e-415b-41f7-b69e-f135f477f2e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "743d5a1e-415b-41f7-b69e-f135f477f2e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3f31d27-48ce-4ea9-af75-5fca8b8203c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 15,
    "yorig": 12
}