{
    "id": "6cd3f7c2-89f6-4f21-b081-3c89effe695d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_xboxone_select",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 88,
    "bbox_left": 11,
    "bbox_right": 88,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2c797d4-1913-41df-9513-cefa349ec15f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cd3f7c2-89f6-4f21-b081-3c89effe695d",
            "compositeImage": {
                "id": "190faccd-52d9-4ce6-9f0c-4908b8089818",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2c797d4-1913-41df-9513-cefa349ec15f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51986172-5a48-47ef-920f-cf3ff0eab5bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2c797d4-1913-41df-9513-cefa349ec15f",
                    "LayerId": "cb4284e8-7836-4c3a-940b-3194de5e8b46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "cb4284e8-7836-4c3a-940b-3194de5e8b46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6cd3f7c2-89f6-4f21-b081-3c89effe695d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}