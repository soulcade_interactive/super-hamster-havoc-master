{
    "id": "6f328566-745d-42cb-8cb1-95a2d44d04a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_main_menu_settings",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 241,
    "bbox_left": 14,
    "bbox_right": 241,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "410122f0-c154-4ca9-bdbf-d30ef1bfece3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f328566-745d-42cb-8cb1-95a2d44d04a0",
            "compositeImage": {
                "id": "fd893ecd-7d4f-4b54-8344-74998113b04e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "410122f0-c154-4ca9-bdbf-d30ef1bfece3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8577d59e-ca64-4bd2-b66b-5cac94fe74a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "410122f0-c154-4ca9-bdbf-d30ef1bfece3",
                    "LayerId": "a8adb466-99b1-4897-8fa3-4b6d15e70d34"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "a8adb466-99b1-4897-8fa3-4b6d15e70d34",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f328566-745d-42cb-8cb1-95a2d44d04a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}