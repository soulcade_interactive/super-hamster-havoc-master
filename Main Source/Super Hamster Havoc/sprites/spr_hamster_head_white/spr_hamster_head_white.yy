{
    "id": "a3b3495c-04d6-45e8-bc2f-1015464bab6f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_head_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f85dabdf-561f-44a7-bb37-aa51ff64310b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3b3495c-04d6-45e8-bc2f-1015464bab6f",
            "compositeImage": {
                "id": "3699147a-5fe9-4ceb-91c2-862bf09c07f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f85dabdf-561f-44a7-bb37-aa51ff64310b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f7a232a-7e92-468c-b2d9-84affc8be659",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f85dabdf-561f-44a7-bb37-aa51ff64310b",
                    "LayerId": "832c0a2a-a6ff-4b58-a183-34e4d7bea4c6"
                }
            ]
        },
        {
            "id": "411b0a6c-6364-4ad5-a8a3-48d83abe7ff8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3b3495c-04d6-45e8-bc2f-1015464bab6f",
            "compositeImage": {
                "id": "4d6d9bbf-99af-48e8-ab07-3e4659407c50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "411b0a6c-6364-4ad5-a8a3-48d83abe7ff8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5912161c-befc-4e1f-a95e-b8c189bced36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "411b0a6c-6364-4ad5-a8a3-48d83abe7ff8",
                    "LayerId": "832c0a2a-a6ff-4b58-a183-34e4d7bea4c6"
                }
            ]
        },
        {
            "id": "23db1d9f-5975-42d8-8e2b-954884fa7006",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3b3495c-04d6-45e8-bc2f-1015464bab6f",
            "compositeImage": {
                "id": "6bf3cc81-4fc7-48fa-b9aa-de62b2fd7d96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23db1d9f-5975-42d8-8e2b-954884fa7006",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bd67b6d-9b1b-49f5-bffb-e57c1196c873",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23db1d9f-5975-42d8-8e2b-954884fa7006",
                    "LayerId": "832c0a2a-a6ff-4b58-a183-34e4d7bea4c6"
                }
            ]
        },
        {
            "id": "0b8bfa5a-37a5-43da-bb81-0e104742e0a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3b3495c-04d6-45e8-bc2f-1015464bab6f",
            "compositeImage": {
                "id": "61f40d42-4a53-41a2-ab09-02c157cbe10a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b8bfa5a-37a5-43da-bb81-0e104742e0a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "072b3882-2013-49c8-aa7b-8ab98674aaa5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b8bfa5a-37a5-43da-bb81-0e104742e0a6",
                    "LayerId": "832c0a2a-a6ff-4b58-a183-34e4d7bea4c6"
                }
            ]
        },
        {
            "id": "0c474dee-3a7c-48a1-aaa2-4c12310c58af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3b3495c-04d6-45e8-bc2f-1015464bab6f",
            "compositeImage": {
                "id": "5d692dd0-3457-44aa-a9ab-5402a90d6ab0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c474dee-3a7c-48a1-aaa2-4c12310c58af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2742c3a5-da61-483a-bfe8-6e0ffa7aa0ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c474dee-3a7c-48a1-aaa2-4c12310c58af",
                    "LayerId": "832c0a2a-a6ff-4b58-a183-34e4d7bea4c6"
                }
            ]
        },
        {
            "id": "f5a99056-b0b7-4310-a5af-89fd47dfbef0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3b3495c-04d6-45e8-bc2f-1015464bab6f",
            "compositeImage": {
                "id": "19b83296-e404-4859-b2f2-28f5d82967d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5a99056-b0b7-4310-a5af-89fd47dfbef0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96fbba24-7989-4a98-b904-7d15541e1029",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5a99056-b0b7-4310-a5af-89fd47dfbef0",
                    "LayerId": "832c0a2a-a6ff-4b58-a183-34e4d7bea4c6"
                }
            ]
        },
        {
            "id": "fe262232-e2ab-4605-864a-bf5c8fa2f946",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3b3495c-04d6-45e8-bc2f-1015464bab6f",
            "compositeImage": {
                "id": "b8dde564-6b6e-42c6-afce-29ee6fd4bb76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe262232-e2ab-4605-864a-bf5c8fa2f946",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "995ce0de-7214-4712-87f2-1c6b25669663",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe262232-e2ab-4605-864a-bf5c8fa2f946",
                    "LayerId": "832c0a2a-a6ff-4b58-a183-34e4d7bea4c6"
                }
            ]
        },
        {
            "id": "2de18b48-08ee-4855-ae17-fd07882c4bd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3b3495c-04d6-45e8-bc2f-1015464bab6f",
            "compositeImage": {
                "id": "ff2eea8c-d612-4f63-9e08-3143545743dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2de18b48-08ee-4855-ae17-fd07882c4bd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6c668c6-79b9-43c5-a5a8-4c54bb61871b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2de18b48-08ee-4855-ae17-fd07882c4bd5",
                    "LayerId": "832c0a2a-a6ff-4b58-a183-34e4d7bea4c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "832c0a2a-a6ff-4b58-a183-34e4d7bea4c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3b3495c-04d6-45e8-bc2f-1015464bab6f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 28
}