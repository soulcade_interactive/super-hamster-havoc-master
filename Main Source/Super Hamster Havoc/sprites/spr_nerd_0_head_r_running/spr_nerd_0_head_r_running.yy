{
    "id": "5e9fdb0c-a3a4-4711-971d-fe7c53bc306e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nerd_0_head_r_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 15,
    "bbox_right": 48,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f912cea-e7ea-43a0-bd76-bbd47fbc1580",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e9fdb0c-a3a4-4711-971d-fe7c53bc306e",
            "compositeImage": {
                "id": "95a78197-9c9f-4094-abcf-104094a203a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f912cea-e7ea-43a0-bd76-bbd47fbc1580",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c392942f-9d6d-4ccc-9baa-01945911a2db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f912cea-e7ea-43a0-bd76-bbd47fbc1580",
                    "LayerId": "841e0f4d-64e0-432e-8517-48f96de7f536"
                }
            ]
        },
        {
            "id": "21b067b4-d837-4e2d-9dfa-4452db5463e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e9fdb0c-a3a4-4711-971d-fe7c53bc306e",
            "compositeImage": {
                "id": "61ca6735-5757-4e68-8ef0-baa87354cea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21b067b4-d837-4e2d-9dfa-4452db5463e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "713363d0-d400-4721-b01b-c26a1ea38b18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21b067b4-d837-4e2d-9dfa-4452db5463e1",
                    "LayerId": "841e0f4d-64e0-432e-8517-48f96de7f536"
                }
            ]
        },
        {
            "id": "c5183f0d-aaf6-42d2-9b9e-6f7bb7b9da24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e9fdb0c-a3a4-4711-971d-fe7c53bc306e",
            "compositeImage": {
                "id": "7f29def6-7c57-429a-8b21-be26cf81700f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5183f0d-aaf6-42d2-9b9e-6f7bb7b9da24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9d04edc-9968-43d4-9a23-2ce7afba054f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5183f0d-aaf6-42d2-9b9e-6f7bb7b9da24",
                    "LayerId": "841e0f4d-64e0-432e-8517-48f96de7f536"
                }
            ]
        },
        {
            "id": "a0b18557-37cd-47cb-8d71-c97e75a229eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e9fdb0c-a3a4-4711-971d-fe7c53bc306e",
            "compositeImage": {
                "id": "1e2bd63c-7f05-4dfb-b5e9-9591c12a2f08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0b18557-37cd-47cb-8d71-c97e75a229eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "725f1983-1bed-4277-844c-0214ae99f125",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0b18557-37cd-47cb-8d71-c97e75a229eb",
                    "LayerId": "841e0f4d-64e0-432e-8517-48f96de7f536"
                }
            ]
        },
        {
            "id": "6d1abb2e-da1e-4f90-adc3-66fa7b0f1e9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e9fdb0c-a3a4-4711-971d-fe7c53bc306e",
            "compositeImage": {
                "id": "9cfff647-e337-4334-bb2b-095ce8573717",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d1abb2e-da1e-4f90-adc3-66fa7b0f1e9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd0b0a02-f137-45a3-b4cf-97346debe170",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d1abb2e-da1e-4f90-adc3-66fa7b0f1e9e",
                    "LayerId": "841e0f4d-64e0-432e-8517-48f96de7f536"
                }
            ]
        },
        {
            "id": "46b89223-dd7c-4f2b-8ccf-a015ea3e6fb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e9fdb0c-a3a4-4711-971d-fe7c53bc306e",
            "compositeImage": {
                "id": "304bccba-bf7c-4b64-b9b6-e3be5d631119",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46b89223-dd7c-4f2b-8ccf-a015ea3e6fb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a9ff431-c1eb-4ef0-a0da-4e4ebc2304d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46b89223-dd7c-4f2b-8ccf-a015ea3e6fb6",
                    "LayerId": "841e0f4d-64e0-432e-8517-48f96de7f536"
                }
            ]
        },
        {
            "id": "ef96a4cf-d313-4ed1-bda4-af2a10fad12d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e9fdb0c-a3a4-4711-971d-fe7c53bc306e",
            "compositeImage": {
                "id": "8b6cdd98-3883-4eaf-926b-141a84d09f68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef96a4cf-d313-4ed1-bda4-af2a10fad12d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09d24ddf-b072-477c-84d1-c9adad6c41df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef96a4cf-d313-4ed1-bda4-af2a10fad12d",
                    "LayerId": "841e0f4d-64e0-432e-8517-48f96de7f536"
                }
            ]
        },
        {
            "id": "512dc5e2-34f9-4276-9260-f761bf8aede8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e9fdb0c-a3a4-4711-971d-fe7c53bc306e",
            "compositeImage": {
                "id": "20c60ddb-fd2c-48b6-b7ce-64df38f43aee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "512dc5e2-34f9-4276-9260-f761bf8aede8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b296b3d4-ca78-449e-bab8-ff766ccd3ccb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "512dc5e2-34f9-4276-9260-f761bf8aede8",
                    "LayerId": "841e0f4d-64e0-432e-8517-48f96de7f536"
                }
            ]
        },
        {
            "id": "added43b-74a5-4b3c-a606-90549725bb4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e9fdb0c-a3a4-4711-971d-fe7c53bc306e",
            "compositeImage": {
                "id": "c11e6023-7e03-4308-bda5-1fb6927f3709",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "added43b-74a5-4b3c-a606-90549725bb4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ea366c4-e3ad-4d5a-9b99-ac6d0a221cfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "added43b-74a5-4b3c-a606-90549725bb4f",
                    "LayerId": "841e0f4d-64e0-432e-8517-48f96de7f536"
                }
            ]
        },
        {
            "id": "5fe0611b-da35-4fce-8bbb-96708af3d402",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e9fdb0c-a3a4-4711-971d-fe7c53bc306e",
            "compositeImage": {
                "id": "69263720-685c-4999-a0bf-31b7a387832a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fe0611b-da35-4fce-8bbb-96708af3d402",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "537a2c79-4e6b-43ff-bf42-7907514e10e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fe0611b-da35-4fce-8bbb-96708af3d402",
                    "LayerId": "841e0f4d-64e0-432e-8517-48f96de7f536"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "841e0f4d-64e0-432e-8517-48f96de7f536",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e9fdb0c-a3a4-4711-971d-fe7c53bc306e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}