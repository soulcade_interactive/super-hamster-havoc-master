{
    "id": "aac685a5-2d82-4348-95dd-362ad25da7d9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_corpse_torso_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "da05e99e-78f0-4d49-9ddc-5905ca8c5e7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aac685a5-2d82-4348-95dd-362ad25da7d9",
            "compositeImage": {
                "id": "4ae08421-c12d-4808-b022-8c9826fb6663",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da05e99e-78f0-4d49-9ddc-5905ca8c5e7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c91c4ef-bcbf-4159-8059-bbb8cbcfd1ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da05e99e-78f0-4d49-9ddc-5905ca8c5e7b",
                    "LayerId": "59bd5e9a-d92b-4a2a-865a-0c5718d84f4b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "59bd5e9a-d92b-4a2a-865a-0c5718d84f4b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aac685a5-2d82-4348-95dd-362ad25da7d9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 8
}