{
    "id": "b17fdec9-e83e-41f4-ae4f-c9643e5a13fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_1_torso_r_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 24,
    "bbox_right": 39,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f59f5be-d352-4199-b1df-b68446e15ed9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b17fdec9-e83e-41f4-ae4f-c9643e5a13fc",
            "compositeImage": {
                "id": "56436d57-735f-4d53-824c-2c78c5b2f326",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f59f5be-d352-4199-b1df-b68446e15ed9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "918acba1-e51c-41f3-99da-7bebb8adc669",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f59f5be-d352-4199-b1df-b68446e15ed9",
                    "LayerId": "4ea4710c-6555-4c85-96c3-38e54928fcff"
                }
            ]
        },
        {
            "id": "2e41cbfb-2e73-40f5-982e-8b7756043802",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b17fdec9-e83e-41f4-ae4f-c9643e5a13fc",
            "compositeImage": {
                "id": "e0efd6db-9527-46b4-aff3-02c1a7f6ce9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e41cbfb-2e73-40f5-982e-8b7756043802",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30d4ccee-1a41-4c65-9730-894307e23ff8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e41cbfb-2e73-40f5-982e-8b7756043802",
                    "LayerId": "4ea4710c-6555-4c85-96c3-38e54928fcff"
                }
            ]
        },
        {
            "id": "847e9569-dd59-43e0-9298-663c4641c8eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b17fdec9-e83e-41f4-ae4f-c9643e5a13fc",
            "compositeImage": {
                "id": "5106dc6f-9f5b-4538-8373-65a2c2dd8714",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "847e9569-dd59-43e0-9298-663c4641c8eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15b49602-6dae-4878-838b-573e39739697",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "847e9569-dd59-43e0-9298-663c4641c8eb",
                    "LayerId": "4ea4710c-6555-4c85-96c3-38e54928fcff"
                }
            ]
        },
        {
            "id": "8f7e7c31-309a-49b5-b3d0-a95859335714",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b17fdec9-e83e-41f4-ae4f-c9643e5a13fc",
            "compositeImage": {
                "id": "5fb86cd5-8916-4cfe-8bfe-7e6086968ffc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f7e7c31-309a-49b5-b3d0-a95859335714",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6edbb4a5-6b88-464a-b1c6-bbe14e4ea70c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f7e7c31-309a-49b5-b3d0-a95859335714",
                    "LayerId": "4ea4710c-6555-4c85-96c3-38e54928fcff"
                }
            ]
        },
        {
            "id": "59aa8cbe-6da6-4f7b-8a29-9adfcacc2e95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b17fdec9-e83e-41f4-ae4f-c9643e5a13fc",
            "compositeImage": {
                "id": "ebcc80ba-fa6f-46f2-8e40-ddb376ba031d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59aa8cbe-6da6-4f7b-8a29-9adfcacc2e95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8816b05d-f133-41ca-888c-f548804d968a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59aa8cbe-6da6-4f7b-8a29-9adfcacc2e95",
                    "LayerId": "4ea4710c-6555-4c85-96c3-38e54928fcff"
                }
            ]
        },
        {
            "id": "0f7ff6f1-605c-4992-89d9-64c187b33b0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b17fdec9-e83e-41f4-ae4f-c9643e5a13fc",
            "compositeImage": {
                "id": "915faa7b-7093-4887-aef0-cef1bec1bd99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f7ff6f1-605c-4992-89d9-64c187b33b0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2c2a955-8788-4223-8ae6-2a20855a0e29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f7ff6f1-605c-4992-89d9-64c187b33b0e",
                    "LayerId": "4ea4710c-6555-4c85-96c3-38e54928fcff"
                }
            ]
        },
        {
            "id": "bcf7fc5f-f4a8-4e50-8648-2b04a2a777a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b17fdec9-e83e-41f4-ae4f-c9643e5a13fc",
            "compositeImage": {
                "id": "400a2f7b-4d65-4b81-9e00-70ae2bccb1a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcf7fc5f-f4a8-4e50-8648-2b04a2a777a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d501a6b-ecf2-4cd7-a87f-a1aa00490022",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcf7fc5f-f4a8-4e50-8648-2b04a2a777a6",
                    "LayerId": "4ea4710c-6555-4c85-96c3-38e54928fcff"
                }
            ]
        },
        {
            "id": "8996f39e-eda5-4fef-95d6-ff6531bbb07f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b17fdec9-e83e-41f4-ae4f-c9643e5a13fc",
            "compositeImage": {
                "id": "e0c489ea-d914-4560-a5a7-591f41d4a96c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8996f39e-eda5-4fef-95d6-ff6531bbb07f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13ba9c88-da88-426f-b365-9a1d6d989a24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8996f39e-eda5-4fef-95d6-ff6531bbb07f",
                    "LayerId": "4ea4710c-6555-4c85-96c3-38e54928fcff"
                }
            ]
        },
        {
            "id": "d54ffc3e-dee5-4b28-b05d-1ecb39244269",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b17fdec9-e83e-41f4-ae4f-c9643e5a13fc",
            "compositeImage": {
                "id": "26e304cc-28fa-4d9c-b75f-345392275da4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d54ffc3e-dee5-4b28-b05d-1ecb39244269",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1175838-391e-4c0b-a668-c12809aa59a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d54ffc3e-dee5-4b28-b05d-1ecb39244269",
                    "LayerId": "4ea4710c-6555-4c85-96c3-38e54928fcff"
                }
            ]
        },
        {
            "id": "70102b68-6336-4162-b530-499413325926",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b17fdec9-e83e-41f4-ae4f-c9643e5a13fc",
            "compositeImage": {
                "id": "9f5242d8-d06d-44b6-9a9d-4596b229d320",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70102b68-6336-4162-b530-499413325926",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5f46e27-8d30-4ec7-8cb1-90fdb8436c19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70102b68-6336-4162-b530-499413325926",
                    "LayerId": "4ea4710c-6555-4c85-96c3-38e54928fcff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4ea4710c-6555-4c85-96c3-38e54928fcff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b17fdec9-e83e-41f4-ae4f-c9643e5a13fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}