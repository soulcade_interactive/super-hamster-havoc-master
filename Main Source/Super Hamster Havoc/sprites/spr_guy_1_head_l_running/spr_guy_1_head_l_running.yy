{
    "id": "da46fba3-26cd-4cba-a541-68cc5d094228",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_1_head_l_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 16,
    "bbox_right": 51,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe344c09-a7e0-47ec-9330-5f336b6f87b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da46fba3-26cd-4cba-a541-68cc5d094228",
            "compositeImage": {
                "id": "55845858-13a7-4597-b85d-3d87c6dec54e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe344c09-a7e0-47ec-9330-5f336b6f87b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18ff890f-9e3f-44c9-8027-eabcf670ff6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe344c09-a7e0-47ec-9330-5f336b6f87b7",
                    "LayerId": "79dc9706-7482-4b75-9309-8f9b732f8ff3"
                }
            ]
        },
        {
            "id": "2ece0948-d1ae-48e2-bb80-693198981f3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da46fba3-26cd-4cba-a541-68cc5d094228",
            "compositeImage": {
                "id": "9f19f790-9807-44a4-8787-669735e29e2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ece0948-d1ae-48e2-bb80-693198981f3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69efa0ef-5896-48a8-a82e-fab1362062a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ece0948-d1ae-48e2-bb80-693198981f3e",
                    "LayerId": "79dc9706-7482-4b75-9309-8f9b732f8ff3"
                }
            ]
        },
        {
            "id": "310e9b0d-d25e-4c76-8dcd-9151dd4bd24f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da46fba3-26cd-4cba-a541-68cc5d094228",
            "compositeImage": {
                "id": "54b2ecab-0562-41e6-bfa2-bec1efee1d4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "310e9b0d-d25e-4c76-8dcd-9151dd4bd24f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac6f3bcd-c2f6-42fb-ac3d-e56334d7c613",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "310e9b0d-d25e-4c76-8dcd-9151dd4bd24f",
                    "LayerId": "79dc9706-7482-4b75-9309-8f9b732f8ff3"
                }
            ]
        },
        {
            "id": "c21ba896-54ad-4248-ad4b-e57319a1ed8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da46fba3-26cd-4cba-a541-68cc5d094228",
            "compositeImage": {
                "id": "5006d709-37b4-4201-ad41-28845c5c146d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c21ba896-54ad-4248-ad4b-e57319a1ed8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dee8f502-92e0-4a11-8bc4-3f1aa9df0c6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c21ba896-54ad-4248-ad4b-e57319a1ed8c",
                    "LayerId": "79dc9706-7482-4b75-9309-8f9b732f8ff3"
                }
            ]
        },
        {
            "id": "da692c67-9552-4984-b7fb-69235f64de67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da46fba3-26cd-4cba-a541-68cc5d094228",
            "compositeImage": {
                "id": "2adde5a2-cf2c-4842-93c3-68576811eb6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da692c67-9552-4984-b7fb-69235f64de67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae2979e7-c474-43b3-a3fa-e21115383281",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da692c67-9552-4984-b7fb-69235f64de67",
                    "LayerId": "79dc9706-7482-4b75-9309-8f9b732f8ff3"
                }
            ]
        },
        {
            "id": "578e8759-3938-4e07-804c-5ce4068fd85e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da46fba3-26cd-4cba-a541-68cc5d094228",
            "compositeImage": {
                "id": "72beb2aa-dd39-43d2-a66e-7ac645388a34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "578e8759-3938-4e07-804c-5ce4068fd85e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaf0120f-bd58-4828-8fdc-30c420f20e88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "578e8759-3938-4e07-804c-5ce4068fd85e",
                    "LayerId": "79dc9706-7482-4b75-9309-8f9b732f8ff3"
                }
            ]
        },
        {
            "id": "7669be53-eefe-4bcd-9372-416657b92724",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da46fba3-26cd-4cba-a541-68cc5d094228",
            "compositeImage": {
                "id": "bb02bc43-85e8-4ea1-8dc6-f87ea72ecd2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7669be53-eefe-4bcd-9372-416657b92724",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "173ef886-ba0f-47b4-9d4c-cf2115f905ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7669be53-eefe-4bcd-9372-416657b92724",
                    "LayerId": "79dc9706-7482-4b75-9309-8f9b732f8ff3"
                }
            ]
        },
        {
            "id": "6c6079a7-dc54-41c3-b558-5f06ae909f0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da46fba3-26cd-4cba-a541-68cc5d094228",
            "compositeImage": {
                "id": "5c43b47b-1a41-48fa-8fdc-30d0745c58f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c6079a7-dc54-41c3-b558-5f06ae909f0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f35df03-69d1-4c96-8161-4d67f84c2d85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c6079a7-dc54-41c3-b558-5f06ae909f0e",
                    "LayerId": "79dc9706-7482-4b75-9309-8f9b732f8ff3"
                }
            ]
        },
        {
            "id": "abe1ec9a-26fc-4e35-b093-91feb2fba4d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da46fba3-26cd-4cba-a541-68cc5d094228",
            "compositeImage": {
                "id": "49e75734-32f3-437d-89e1-154e5a050022",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abe1ec9a-26fc-4e35-b093-91feb2fba4d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74988948-252d-4df4-84b6-d9c901ccd21d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abe1ec9a-26fc-4e35-b093-91feb2fba4d2",
                    "LayerId": "79dc9706-7482-4b75-9309-8f9b732f8ff3"
                }
            ]
        },
        {
            "id": "329bdd41-7626-4a53-9674-678bfe40aefc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "da46fba3-26cd-4cba-a541-68cc5d094228",
            "compositeImage": {
                "id": "93016a64-0fec-484e-a9f8-e319f6626d67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "329bdd41-7626-4a53-9674-678bfe40aefc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "170c4f20-0f5c-4d64-ad0a-95351546adb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "329bdd41-7626-4a53-9674-678bfe40aefc",
                    "LayerId": "79dc9706-7482-4b75-9309-8f9b732f8ff3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "79dc9706-7482-4b75-9309-8f9b732f8ff3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "da46fba3-26cd-4cba-a541-68cc5d094228",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}