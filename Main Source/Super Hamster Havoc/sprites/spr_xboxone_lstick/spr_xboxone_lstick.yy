{
    "id": "bba8e062-b369-49a2-ba48-29b5cd91a084",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_xboxone_lstick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 5,
    "bbox_right": 94,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19702dd2-362f-4d79-89f3-8cd67117d118",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bba8e062-b369-49a2-ba48-29b5cd91a084",
            "compositeImage": {
                "id": "de6e5d01-2e73-4d1e-8f29-5ec92bca5d4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19702dd2-362f-4d79-89f3-8cd67117d118",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dbef1fa-4baf-4dc0-bb97-502f4fbc3175",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19702dd2-362f-4d79-89f3-8cd67117d118",
                    "LayerId": "fef5d09a-5929-4ca6-82fd-13370dabd7b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "fef5d09a-5929-4ca6-82fd-13370dabd7b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bba8e062-b369-49a2-ba48-29b5cd91a084",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}