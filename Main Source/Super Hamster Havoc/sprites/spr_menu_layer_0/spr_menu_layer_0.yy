{
    "id": "66c65acf-2c5b-424b-b190-d38dccbcf4fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menu_layer_0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 779,
    "bbox_left": 0,
    "bbox_right": 2399,
    "bbox_top": 594,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23f8d6bb-010c-4488-9d05-f52b6c735fe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66c65acf-2c5b-424b-b190-d38dccbcf4fe",
            "compositeImage": {
                "id": "4e626910-e90e-453c-8dc5-b9c9d63efc43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23f8d6bb-010c-4488-9d05-f52b6c735fe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa49013a-5684-4ffb-a69f-3f9a1c3106c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23f8d6bb-010c-4488-9d05-f52b6c735fe1",
                    "LayerId": "b35e25e0-b0d9-4322-8ae6-f77e68dfc80a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 780,
    "layers": [
        {
            "id": "b35e25e0-b0d9-4322-8ae6-f77e68dfc80a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66c65acf-2c5b-424b-b190-d38dccbcf4fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2400,
    "xorig": 1200,
    "yorig": 390
}