{
    "id": "9cdc76fa-1ee3-4493-91fc-39e495976914",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buff_0_head_l_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 20,
    "bbox_right": 42,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b4c4e15-6b16-4ef8-94b2-66ed8b670ef3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cdc76fa-1ee3-4493-91fc-39e495976914",
            "compositeImage": {
                "id": "6d515f77-99cd-4ecd-a376-df6a93ec4904",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b4c4e15-6b16-4ef8-94b2-66ed8b670ef3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17d222e6-eefc-45f6-be87-a6bcbab0cdc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b4c4e15-6b16-4ef8-94b2-66ed8b670ef3",
                    "LayerId": "360ca235-da46-4b32-8dae-2e68e45a4de6"
                }
            ]
        },
        {
            "id": "fbb98c96-2c3c-4caa-8f0a-c9c764a13163",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cdc76fa-1ee3-4493-91fc-39e495976914",
            "compositeImage": {
                "id": "53ba8117-5b95-4cdb-a75c-3e1f67d6b5e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbb98c96-2c3c-4caa-8f0a-c9c764a13163",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad985642-34ce-4da4-82b9-0d9d04f15610",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbb98c96-2c3c-4caa-8f0a-c9c764a13163",
                    "LayerId": "360ca235-da46-4b32-8dae-2e68e45a4de6"
                }
            ]
        },
        {
            "id": "6c685cc4-094f-4d4e-b2c2-0a14829c2ba0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cdc76fa-1ee3-4493-91fc-39e495976914",
            "compositeImage": {
                "id": "25835882-d468-41e6-9bfd-2bbc4a862aea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c685cc4-094f-4d4e-b2c2-0a14829c2ba0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07dcf604-3aff-47b8-b406-91eb2951dfb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c685cc4-094f-4d4e-b2c2-0a14829c2ba0",
                    "LayerId": "360ca235-da46-4b32-8dae-2e68e45a4de6"
                }
            ]
        },
        {
            "id": "ab091f3c-174c-4d10-90f7-9125c66186f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cdc76fa-1ee3-4493-91fc-39e495976914",
            "compositeImage": {
                "id": "cfec5ced-cecf-458a-843e-c37c1d5d5421",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab091f3c-174c-4d10-90f7-9125c66186f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c164724d-8422-416c-a753-e7ed125c12b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab091f3c-174c-4d10-90f7-9125c66186f2",
                    "LayerId": "360ca235-da46-4b32-8dae-2e68e45a4de6"
                }
            ]
        },
        {
            "id": "784b87d4-351d-4ba8-a3a0-7f2a284b0f51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cdc76fa-1ee3-4493-91fc-39e495976914",
            "compositeImage": {
                "id": "65e78657-dbdc-4fc0-aed7-74237f7ecd62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "784b87d4-351d-4ba8-a3a0-7f2a284b0f51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "658f1138-a8dc-4696-b094-e630df610069",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "784b87d4-351d-4ba8-a3a0-7f2a284b0f51",
                    "LayerId": "360ca235-da46-4b32-8dae-2e68e45a4de6"
                }
            ]
        },
        {
            "id": "fd6424d8-65e6-4513-b0ae-df3d39e0fbea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cdc76fa-1ee3-4493-91fc-39e495976914",
            "compositeImage": {
                "id": "78c6fef7-0873-4ee2-bbd6-562407a640e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd6424d8-65e6-4513-b0ae-df3d39e0fbea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bfd557e-dd15-4539-a4d8-323bb7f5f4a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd6424d8-65e6-4513-b0ae-df3d39e0fbea",
                    "LayerId": "360ca235-da46-4b32-8dae-2e68e45a4de6"
                }
            ]
        },
        {
            "id": "e8e804ab-10ee-4b85-aabb-07fc13dd6420",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cdc76fa-1ee3-4493-91fc-39e495976914",
            "compositeImage": {
                "id": "267c1def-222c-47fa-a379-6016d7f1d60c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8e804ab-10ee-4b85-aabb-07fc13dd6420",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "616777a2-e045-45d3-87e0-1065dc7a330b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8e804ab-10ee-4b85-aabb-07fc13dd6420",
                    "LayerId": "360ca235-da46-4b32-8dae-2e68e45a4de6"
                }
            ]
        },
        {
            "id": "6d29e59c-3b42-40a0-9d8e-f7fa06c8311e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cdc76fa-1ee3-4493-91fc-39e495976914",
            "compositeImage": {
                "id": "1adc0702-50e3-4f73-8ad9-986ca8c05f1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d29e59c-3b42-40a0-9d8e-f7fa06c8311e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "548870de-1d22-4731-b832-6140fa311e9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d29e59c-3b42-40a0-9d8e-f7fa06c8311e",
                    "LayerId": "360ca235-da46-4b32-8dae-2e68e45a4de6"
                }
            ]
        },
        {
            "id": "1fc8d1ad-fe0e-4dce-86af-983b5bba63d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cdc76fa-1ee3-4493-91fc-39e495976914",
            "compositeImage": {
                "id": "2c82dcaf-527c-4384-810c-e65b2d919028",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fc8d1ad-fe0e-4dce-86af-983b5bba63d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65ee0e8a-6405-41fe-8a81-b84da10230df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fc8d1ad-fe0e-4dce-86af-983b5bba63d3",
                    "LayerId": "360ca235-da46-4b32-8dae-2e68e45a4de6"
                }
            ]
        },
        {
            "id": "d17fc95b-2b65-40a3-a63d-50a4a3042286",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cdc76fa-1ee3-4493-91fc-39e495976914",
            "compositeImage": {
                "id": "01a79987-9b23-4260-a799-53b54e62655f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d17fc95b-2b65-40a3-a63d-50a4a3042286",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8a1e4e3-33c7-43fa-8918-d61335177a32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d17fc95b-2b65-40a3-a63d-50a4a3042286",
                    "LayerId": "360ca235-da46-4b32-8dae-2e68e45a4de6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "360ca235-da46-4b32-8dae-2e68e45a4de6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cdc76fa-1ee3-4493-91fc-39e495976914",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}