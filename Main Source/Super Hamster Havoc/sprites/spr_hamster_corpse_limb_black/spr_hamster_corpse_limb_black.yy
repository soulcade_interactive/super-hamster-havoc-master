{
    "id": "1738f2b9-5bb1-4389-b895-4da047b52c82",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_corpse_limb_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f718bb0d-ed68-482b-b95c-20ca85292881",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1738f2b9-5bb1-4389-b895-4da047b52c82",
            "compositeImage": {
                "id": "daed0707-579f-43a4-a31f-acaedf336f9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f718bb0d-ed68-482b-b95c-20ca85292881",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22a29dc3-e283-4778-9461-9d75bf3639ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f718bb0d-ed68-482b-b95c-20ca85292881",
                    "LayerId": "559a992c-3a99-4427-8ba8-59634dfd9b1d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "559a992c-3a99-4427-8ba8-59634dfd9b1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1738f2b9-5bb1-4389-b895-4da047b52c82",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 4
}