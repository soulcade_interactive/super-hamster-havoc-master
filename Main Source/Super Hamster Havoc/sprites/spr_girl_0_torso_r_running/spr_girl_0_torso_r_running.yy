{
    "id": "afa64bf0-4605-4b95-8a51-1bc7a2650baf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_0_torso_r_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 24,
    "bbox_right": 37,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54a8cc9d-88b0-40c0-afd4-5b3cf7476e1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afa64bf0-4605-4b95-8a51-1bc7a2650baf",
            "compositeImage": {
                "id": "6d6aa44d-aaec-4dbc-a22d-7c05f14d72b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54a8cc9d-88b0-40c0-afd4-5b3cf7476e1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3de9978-969b-46ef-a0c5-75f9d381adc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54a8cc9d-88b0-40c0-afd4-5b3cf7476e1f",
                    "LayerId": "d82e6d08-1b3d-4e15-ae6c-825868057695"
                }
            ]
        },
        {
            "id": "4d7c7573-99d0-4d00-b880-bb2beff4e37f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afa64bf0-4605-4b95-8a51-1bc7a2650baf",
            "compositeImage": {
                "id": "98092c73-8057-4dba-bcef-2e69673c9f4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d7c7573-99d0-4d00-b880-bb2beff4e37f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1cac2fd-3b04-4915-81cd-dcab4d9d39df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d7c7573-99d0-4d00-b880-bb2beff4e37f",
                    "LayerId": "d82e6d08-1b3d-4e15-ae6c-825868057695"
                }
            ]
        },
        {
            "id": "48bc0c34-9d03-402e-9d08-c7045930970f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afa64bf0-4605-4b95-8a51-1bc7a2650baf",
            "compositeImage": {
                "id": "e10902d2-629e-4139-9ba8-046c2403f33f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48bc0c34-9d03-402e-9d08-c7045930970f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a6bf99b-02bc-4634-88af-db3d6c0092f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48bc0c34-9d03-402e-9d08-c7045930970f",
                    "LayerId": "d82e6d08-1b3d-4e15-ae6c-825868057695"
                }
            ]
        },
        {
            "id": "8ce8fd2e-2def-4989-86e5-cd4326d3a8a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afa64bf0-4605-4b95-8a51-1bc7a2650baf",
            "compositeImage": {
                "id": "309392d4-012f-4076-aa4c-3f53e7c0fb74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ce8fd2e-2def-4989-86e5-cd4326d3a8a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef61d5b4-ac64-46c2-b16f-4f020d188b9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ce8fd2e-2def-4989-86e5-cd4326d3a8a7",
                    "LayerId": "d82e6d08-1b3d-4e15-ae6c-825868057695"
                }
            ]
        },
        {
            "id": "490767e2-e9c7-468c-9560-b2498a8015c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afa64bf0-4605-4b95-8a51-1bc7a2650baf",
            "compositeImage": {
                "id": "25ef476d-f96f-4d01-aa64-6bb0e1c3e3df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "490767e2-e9c7-468c-9560-b2498a8015c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "683e055c-d31a-41d4-9be7-bcd8b08bbec6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "490767e2-e9c7-468c-9560-b2498a8015c0",
                    "LayerId": "d82e6d08-1b3d-4e15-ae6c-825868057695"
                }
            ]
        },
        {
            "id": "4607ac5d-198e-4966-b141-97c59ed33d3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afa64bf0-4605-4b95-8a51-1bc7a2650baf",
            "compositeImage": {
                "id": "8aa5d485-c00e-4928-ab45-2f7867819d9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4607ac5d-198e-4966-b141-97c59ed33d3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca5812e6-1a79-456e-80b8-04c32983164d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4607ac5d-198e-4966-b141-97c59ed33d3f",
                    "LayerId": "d82e6d08-1b3d-4e15-ae6c-825868057695"
                }
            ]
        },
        {
            "id": "7a3adf51-3133-49af-909e-19fc304953da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afa64bf0-4605-4b95-8a51-1bc7a2650baf",
            "compositeImage": {
                "id": "bc241594-df61-4c22-a25d-04efaf767fd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a3adf51-3133-49af-909e-19fc304953da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "800daa4b-7982-4888-8ac4-28009941d595",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a3adf51-3133-49af-909e-19fc304953da",
                    "LayerId": "d82e6d08-1b3d-4e15-ae6c-825868057695"
                }
            ]
        },
        {
            "id": "a53f39bd-331d-451a-8501-63fc5d411162",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afa64bf0-4605-4b95-8a51-1bc7a2650baf",
            "compositeImage": {
                "id": "9322478c-d9d4-499c-a7e9-cae9b24dacca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a53f39bd-331d-451a-8501-63fc5d411162",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a001f99-988e-4d20-865e-f206b9d17d90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a53f39bd-331d-451a-8501-63fc5d411162",
                    "LayerId": "d82e6d08-1b3d-4e15-ae6c-825868057695"
                }
            ]
        },
        {
            "id": "594e8b24-8e08-4e16-92de-b30cb5a13f0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afa64bf0-4605-4b95-8a51-1bc7a2650baf",
            "compositeImage": {
                "id": "ed42aea8-895e-42ea-8164-16778f8c17a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "594e8b24-8e08-4e16-92de-b30cb5a13f0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f2233bb-017d-45ce-9ae5-a73dba217ebe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "594e8b24-8e08-4e16-92de-b30cb5a13f0b",
                    "LayerId": "d82e6d08-1b3d-4e15-ae6c-825868057695"
                }
            ]
        },
        {
            "id": "d597a2e5-c363-44d0-a0c3-2b7d94234049",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afa64bf0-4605-4b95-8a51-1bc7a2650baf",
            "compositeImage": {
                "id": "3142ba45-ab4d-4fdf-8908-3ea44007a835",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d597a2e5-c363-44d0-a0c3-2b7d94234049",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48f788f4-5b92-4510-acde-db873d54a2a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d597a2e5-c363-44d0-a0c3-2b7d94234049",
                    "LayerId": "d82e6d08-1b3d-4e15-ae6c-825868057695"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d82e6d08-1b3d-4e15-ae6c-825868057695",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "afa64bf0-4605-4b95-8a51-1bc7a2650baf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}