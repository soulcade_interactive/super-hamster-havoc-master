{
    "id": "a9e71f36-4782-41ca-bd68-c95c86e587dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_0_head_u_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 17,
    "bbox_right": 46,
    "bbox_top": 17,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9fb43476-a670-4e5e-b273-fd98ed0c5c3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9e71f36-4782-41ca-bd68-c95c86e587dd",
            "compositeImage": {
                "id": "91949716-d391-4e08-a083-425a16846ea9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fb43476-a670-4e5e-b273-fd98ed0c5c3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3956832-9622-4edc-a63d-b5cb0d82bcce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fb43476-a670-4e5e-b273-fd98ed0c5c3e",
                    "LayerId": "5dd625e5-628b-4b16-836e-5f012962d74c"
                }
            ]
        },
        {
            "id": "55be9529-e34a-4e01-935f-c93bdb18ac3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9e71f36-4782-41ca-bd68-c95c86e587dd",
            "compositeImage": {
                "id": "59fec830-a1b2-44a8-8fcc-b6201a145efe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55be9529-e34a-4e01-935f-c93bdb18ac3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa096f75-1318-40d4-bad6-d4b65c214b73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55be9529-e34a-4e01-935f-c93bdb18ac3d",
                    "LayerId": "5dd625e5-628b-4b16-836e-5f012962d74c"
                }
            ]
        },
        {
            "id": "64205ed3-a716-4c04-abf3-7bfd34df2101",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9e71f36-4782-41ca-bd68-c95c86e587dd",
            "compositeImage": {
                "id": "cf21f6c9-38e0-43a5-bac4-66d5e85f0eaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64205ed3-a716-4c04-abf3-7bfd34df2101",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e3f50ab-fcd3-4fd1-93aa-eece9c3d02fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64205ed3-a716-4c04-abf3-7bfd34df2101",
                    "LayerId": "5dd625e5-628b-4b16-836e-5f012962d74c"
                }
            ]
        },
        {
            "id": "35a2c772-6ecd-477c-a465-484c2bf0fca7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9e71f36-4782-41ca-bd68-c95c86e587dd",
            "compositeImage": {
                "id": "024b375f-3263-4e75-ba5d-74dfdb78aaeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35a2c772-6ecd-477c-a465-484c2bf0fca7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5173e61-1ae3-455f-92a4-39627c787c56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35a2c772-6ecd-477c-a465-484c2bf0fca7",
                    "LayerId": "5dd625e5-628b-4b16-836e-5f012962d74c"
                }
            ]
        },
        {
            "id": "6b58249a-a9b9-4a63-a392-94ffe9ae5d48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9e71f36-4782-41ca-bd68-c95c86e587dd",
            "compositeImage": {
                "id": "73071c47-70fc-4905-b389-4ec09cc46923",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b58249a-a9b9-4a63-a392-94ffe9ae5d48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0046cc3d-255a-4e85-b760-c6913e87d63d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b58249a-a9b9-4a63-a392-94ffe9ae5d48",
                    "LayerId": "5dd625e5-628b-4b16-836e-5f012962d74c"
                }
            ]
        },
        {
            "id": "2f5121f5-4b07-4d67-b9ef-26dadda3fea3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9e71f36-4782-41ca-bd68-c95c86e587dd",
            "compositeImage": {
                "id": "8e6e349d-8d90-4782-a4da-843c19896b82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f5121f5-4b07-4d67-b9ef-26dadda3fea3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35ddde45-9f02-496c-9769-5056c4ce02f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f5121f5-4b07-4d67-b9ef-26dadda3fea3",
                    "LayerId": "5dd625e5-628b-4b16-836e-5f012962d74c"
                }
            ]
        },
        {
            "id": "fbde3873-0d85-456b-82ef-5613dd9fc892",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9e71f36-4782-41ca-bd68-c95c86e587dd",
            "compositeImage": {
                "id": "61194c10-569d-47ac-a8cf-445eee13c365",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbde3873-0d85-456b-82ef-5613dd9fc892",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a466d3c8-d8cf-4aef-8d66-6df86ea62ae0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbde3873-0d85-456b-82ef-5613dd9fc892",
                    "LayerId": "5dd625e5-628b-4b16-836e-5f012962d74c"
                }
            ]
        },
        {
            "id": "8c3942a3-fea5-4ee7-b06e-422fa4a39d44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9e71f36-4782-41ca-bd68-c95c86e587dd",
            "compositeImage": {
                "id": "60a9bac3-c38b-486b-a324-87bb9caa9ccd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c3942a3-fea5-4ee7-b06e-422fa4a39d44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27e91a18-a7ba-407a-b95c-7a5381464db3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c3942a3-fea5-4ee7-b06e-422fa4a39d44",
                    "LayerId": "5dd625e5-628b-4b16-836e-5f012962d74c"
                }
            ]
        },
        {
            "id": "968d237d-5be0-47ed-8e90-fb3a5f5e9901",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9e71f36-4782-41ca-bd68-c95c86e587dd",
            "compositeImage": {
                "id": "5196e868-000b-4111-8c3e-b828a3a99e0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "968d237d-5be0-47ed-8e90-fb3a5f5e9901",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce632df0-fb54-4dfa-bd9b-3cf4851b7a69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "968d237d-5be0-47ed-8e90-fb3a5f5e9901",
                    "LayerId": "5dd625e5-628b-4b16-836e-5f012962d74c"
                }
            ]
        },
        {
            "id": "abaa9440-6c60-4130-9968-01a6c1b0fd24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9e71f36-4782-41ca-bd68-c95c86e587dd",
            "compositeImage": {
                "id": "dcf8b2b9-51b2-4fbf-a07b-ca275b89ca65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abaa9440-6c60-4130-9968-01a6c1b0fd24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac423ac7-2f1a-4a1c-9b62-c7938135ffb4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abaa9440-6c60-4130-9968-01a6c1b0fd24",
                    "LayerId": "5dd625e5-628b-4b16-836e-5f012962d74c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5dd625e5-628b-4b16-836e-5f012962d74c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9e71f36-4782-41ca-bd68-c95c86e587dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}