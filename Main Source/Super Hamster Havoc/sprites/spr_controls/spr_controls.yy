{
    "id": "2e459a19-c112-4940-898b-cfdb801167c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_controls",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 394,
    "bbox_left": 34,
    "bbox_right": 466,
    "bbox_top": 66,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8ebdb0be-98ea-4193-b928-1584fab1e46b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e459a19-c112-4940-898b-cfdb801167c0",
            "compositeImage": {
                "id": "f36f8006-f0c0-4ca4-af8e-bb873f0b0a42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ebdb0be-98ea-4193-b928-1584fab1e46b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffd80dbf-6959-43e1-b04b-a46b212180ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ebdb0be-98ea-4193-b928-1584fab1e46b",
                    "LayerId": "8c55958c-e6c9-48cd-bfc0-a0cd36abc68e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "8c55958c-e6c9-48cd-bfc0-a0cd36abc68e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e459a19-c112-4940-898b-cfdb801167c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 480,
    "xorig": 240,
    "yorig": 0
}