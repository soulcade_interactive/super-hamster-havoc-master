{
    "id": "c289e40c-bdd5-4851-a3b7-e26f2996ac8b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset_grassland",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1215,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 64,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3d2eccb4-eed8-480b-a76f-6fbaf1e72891",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c289e40c-bdd5-4851-a3b7-e26f2996ac8b",
            "compositeImage": {
                "id": "0480d7d8-0e65-4957-9402-74feb70e6b26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d2eccb4-eed8-480b-a76f-6fbaf1e72891",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f9a4136-7a65-40be-92ff-77a08be3753a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d2eccb4-eed8-480b-a76f-6fbaf1e72891",
                    "LayerId": "09abcf77-0d61-466f-991d-1f08dc9bd030"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1600,
    "layers": [
        {
            "id": "09abcf77-0d61-466f-991d-1f08dc9bd030",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c289e40c-bdd5-4851-a3b7-e26f2996ac8b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 320,
    "yorig": 800
}