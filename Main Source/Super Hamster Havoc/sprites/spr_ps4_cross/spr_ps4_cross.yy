{
    "id": "6545b9d9-9565-41de-8211-5c5756f9482f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ps4_cross",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 9,
    "bbox_right": 90,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e8c1abf-8d58-4f00-a57d-467fb33dc227",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6545b9d9-9565-41de-8211-5c5756f9482f",
            "compositeImage": {
                "id": "6b3cbd38-5bb1-4a7a-86d6-ca6649e76705",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e8c1abf-8d58-4f00-a57d-467fb33dc227",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c482c3f4-838f-4000-9d39-228dcd040f75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e8c1abf-8d58-4f00-a57d-467fb33dc227",
                    "LayerId": "0a2d6f1c-a536-40f3-97a1-b81b8d98f9d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "0a2d6f1c-a536-40f3-97a1-b81b8d98f9d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6545b9d9-9565-41de-8211-5c5756f9482f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}