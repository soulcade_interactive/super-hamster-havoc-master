{
    "id": "7fb1db05-786f-4a6e-9c4c-1c2d4e58f3b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_body_idle_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 3,
    "bbox_right": 31,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6791b6e1-faa7-4192-b898-472f26ef7b34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fb1db05-786f-4a6e-9c4c-1c2d4e58f3b6",
            "compositeImage": {
                "id": "06e9503c-e8ce-4724-bd22-2b639287f3c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6791b6e1-faa7-4192-b898-472f26ef7b34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91b322a4-b94c-4c4a-ab2b-1385a60a6ef4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6791b6e1-faa7-4192-b898-472f26ef7b34",
                    "LayerId": "06011773-a530-413d-aeeb-b3c0c2fce227"
                }
            ]
        },
        {
            "id": "562d1626-6712-42a0-aa75-f172f917a079",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fb1db05-786f-4a6e-9c4c-1c2d4e58f3b6",
            "compositeImage": {
                "id": "68c7693e-8829-4e12-91c3-01273f2e15f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "562d1626-6712-42a0-aa75-f172f917a079",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "101db0f8-272e-4bb1-a495-d959a5bd8979",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "562d1626-6712-42a0-aa75-f172f917a079",
                    "LayerId": "06011773-a530-413d-aeeb-b3c0c2fce227"
                }
            ]
        },
        {
            "id": "61453491-36e8-4e0d-9f44-1ddc2f70b9cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fb1db05-786f-4a6e-9c4c-1c2d4e58f3b6",
            "compositeImage": {
                "id": "2194fdd7-128c-41fe-8e6f-f51339ac20be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61453491-36e8-4e0d-9f44-1ddc2f70b9cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3cb4b6c-c2c6-4dab-819c-175ef8aea284",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61453491-36e8-4e0d-9f44-1ddc2f70b9cc",
                    "LayerId": "06011773-a530-413d-aeeb-b3c0c2fce227"
                }
            ]
        },
        {
            "id": "b7d5fccb-fa0e-4135-ba5b-ef1e21a0a3e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fb1db05-786f-4a6e-9c4c-1c2d4e58f3b6",
            "compositeImage": {
                "id": "0fc9f9a4-363e-41c6-a062-ca3a097aba9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7d5fccb-fa0e-4135-ba5b-ef1e21a0a3e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "897f666b-c18a-4f7d-ad16-f9d84b25bf91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7d5fccb-fa0e-4135-ba5b-ef1e21a0a3e1",
                    "LayerId": "06011773-a530-413d-aeeb-b3c0c2fce227"
                }
            ]
        },
        {
            "id": "f49bf4f7-eaac-46a2-8a4e-5a7f9b39a7d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fb1db05-786f-4a6e-9c4c-1c2d4e58f3b6",
            "compositeImage": {
                "id": "cc9ad07c-1460-4787-86a0-976a27b9813c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f49bf4f7-eaac-46a2-8a4e-5a7f9b39a7d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04b02ca7-8535-44e0-9bb7-bc63afbc0aea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f49bf4f7-eaac-46a2-8a4e-5a7f9b39a7d3",
                    "LayerId": "06011773-a530-413d-aeeb-b3c0c2fce227"
                }
            ]
        },
        {
            "id": "7761c8c0-9618-42c6-b522-4fdd61856b55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fb1db05-786f-4a6e-9c4c-1c2d4e58f3b6",
            "compositeImage": {
                "id": "308b39a8-91ba-427d-8811-e6cc3bce0624",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7761c8c0-9618-42c6-b522-4fdd61856b55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "725b1733-d265-409e-b4a1-2d822810ca4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7761c8c0-9618-42c6-b522-4fdd61856b55",
                    "LayerId": "06011773-a530-413d-aeeb-b3c0c2fce227"
                }
            ]
        },
        {
            "id": "1dc7ff69-83be-4f14-b528-c7178ca37b67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fb1db05-786f-4a6e-9c4c-1c2d4e58f3b6",
            "compositeImage": {
                "id": "af60c501-a406-45e1-a3e0-2730e030cc02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dc7ff69-83be-4f14-b528-c7178ca37b67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a03f620-3aa7-4b23-a267-c1605e54ee92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dc7ff69-83be-4f14-b528-c7178ca37b67",
                    "LayerId": "06011773-a530-413d-aeeb-b3c0c2fce227"
                }
            ]
        },
        {
            "id": "5496320c-6ee8-45b9-9c74-e72a3c96f9eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7fb1db05-786f-4a6e-9c4c-1c2d4e58f3b6",
            "compositeImage": {
                "id": "3ffa6f38-35c8-4f0a-8ac0-3090cb55bce7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5496320c-6ee8-45b9-9c74-e72a3c96f9eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8319e9bc-d1ad-4cc3-b378-960f9fb82305",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5496320c-6ee8-45b9-9c74-e72a3c96f9eb",
                    "LayerId": "06011773-a530-413d-aeeb-b3c0c2fce227"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 39,
    "layers": [
        {
            "id": "06011773-a530-413d-aeeb-b3c0c2fce227",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7fb1db05-786f-4a6e-9c4c-1c2d4e58f3b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 19
}