{
    "id": "521fba2e-342f-4af4-b71e-630b169f6ac6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_switch_rstick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 5,
    "bbox_right": 94,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f2b1199-e8aa-4b39-885c-e2a5ed969cbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "521fba2e-342f-4af4-b71e-630b169f6ac6",
            "compositeImage": {
                "id": "3d46e874-5588-4995-9023-fc3332bf6853",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f2b1199-e8aa-4b39-885c-e2a5ed969cbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffb7adc7-e63d-4041-b1b5-bc497e483287",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f2b1199-e8aa-4b39-885c-e2a5ed969cbd",
                    "LayerId": "dac9ce6e-8082-4f89-9c41-f4f186275c8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "dac9ce6e-8082-4f89-9c41-f4f186275c8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "521fba2e-342f-4af4-b71e-630b169f6ac6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}