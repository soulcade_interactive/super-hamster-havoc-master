{
    "id": "06c5acf2-d095-46c7-98cf-3fc298cf420d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nerd_0_legs_d_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 25,
    "bbox_right": 38,
    "bbox_top": 56,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72ec93af-faf9-48f6-8cd2-be9676aad4d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06c5acf2-d095-46c7-98cf-3fc298cf420d",
            "compositeImage": {
                "id": "fb4813fa-0ea9-4f88-b0b4-7c713a2f3575",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72ec93af-faf9-48f6-8cd2-be9676aad4d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afdcbfa7-3663-4d3d-a1ea-2a4df24218cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72ec93af-faf9-48f6-8cd2-be9676aad4d2",
                    "LayerId": "c43150d7-1a10-4af6-90e1-0449859227c5"
                }
            ]
        },
        {
            "id": "78aff9ef-ac41-443e-a642-e7f369bfc959",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06c5acf2-d095-46c7-98cf-3fc298cf420d",
            "compositeImage": {
                "id": "6de377b3-af33-4629-b760-cdffefcd291a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78aff9ef-ac41-443e-a642-e7f369bfc959",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad18450b-b68f-4e5b-b9de-d7ea90645d64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78aff9ef-ac41-443e-a642-e7f369bfc959",
                    "LayerId": "c43150d7-1a10-4af6-90e1-0449859227c5"
                }
            ]
        },
        {
            "id": "344fa859-279e-4c8b-aad9-f99fff00fe40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06c5acf2-d095-46c7-98cf-3fc298cf420d",
            "compositeImage": {
                "id": "60e8d402-4e0d-46c6-b2ae-764480452167",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "344fa859-279e-4c8b-aad9-f99fff00fe40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f215be0c-d3fa-42f4-97b1-5df1a7968242",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "344fa859-279e-4c8b-aad9-f99fff00fe40",
                    "LayerId": "c43150d7-1a10-4af6-90e1-0449859227c5"
                }
            ]
        },
        {
            "id": "323cc67d-c44f-46ff-99b4-f6b2827ba15b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06c5acf2-d095-46c7-98cf-3fc298cf420d",
            "compositeImage": {
                "id": "99f2bcc8-6246-4c4b-93b8-853c8a33d5f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "323cc67d-c44f-46ff-99b4-f6b2827ba15b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff1d0450-bd58-42f8-baef-6cd1e59c1da1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "323cc67d-c44f-46ff-99b4-f6b2827ba15b",
                    "LayerId": "c43150d7-1a10-4af6-90e1-0449859227c5"
                }
            ]
        },
        {
            "id": "4cff20cd-0bea-4947-8334-98967d8a1730",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06c5acf2-d095-46c7-98cf-3fc298cf420d",
            "compositeImage": {
                "id": "14bdfe29-2511-49a4-9361-513a548ea35e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4cff20cd-0bea-4947-8334-98967d8a1730",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "971b9b71-3629-42f2-a439-0731d55319a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4cff20cd-0bea-4947-8334-98967d8a1730",
                    "LayerId": "c43150d7-1a10-4af6-90e1-0449859227c5"
                }
            ]
        },
        {
            "id": "1db2fbc5-7b64-4782-8b1e-d4e4d63d9ac3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06c5acf2-d095-46c7-98cf-3fc298cf420d",
            "compositeImage": {
                "id": "48757469-4bb1-42b6-9c99-a7c7d58f0366",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1db2fbc5-7b64-4782-8b1e-d4e4d63d9ac3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b62721c-2871-483f-a1c3-e4f866fe3d60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1db2fbc5-7b64-4782-8b1e-d4e4d63d9ac3",
                    "LayerId": "c43150d7-1a10-4af6-90e1-0449859227c5"
                }
            ]
        },
        {
            "id": "77b7b29b-a1ae-4f41-81be-1956ee698f38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06c5acf2-d095-46c7-98cf-3fc298cf420d",
            "compositeImage": {
                "id": "e0483a37-a404-4c8d-b49a-7935280b4a7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77b7b29b-a1ae-4f41-81be-1956ee698f38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10cbd17e-f60f-4f03-bee1-1d8cdeb7d1d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77b7b29b-a1ae-4f41-81be-1956ee698f38",
                    "LayerId": "c43150d7-1a10-4af6-90e1-0449859227c5"
                }
            ]
        },
        {
            "id": "58ab17ec-72be-4180-86b2-3a1be341a756",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06c5acf2-d095-46c7-98cf-3fc298cf420d",
            "compositeImage": {
                "id": "b4b87f10-2b2e-41fd-8516-a7cf0f6d4dfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58ab17ec-72be-4180-86b2-3a1be341a756",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd98d18d-d2f3-42fd-8864-2cf96aab3b56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58ab17ec-72be-4180-86b2-3a1be341a756",
                    "LayerId": "c43150d7-1a10-4af6-90e1-0449859227c5"
                }
            ]
        },
        {
            "id": "9ea0dee1-9b08-4df4-bdea-f51b383080ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06c5acf2-d095-46c7-98cf-3fc298cf420d",
            "compositeImage": {
                "id": "06593191-0c94-406a-9c63-c581f08b3e36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ea0dee1-9b08-4df4-bdea-f51b383080ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a696c118-c49a-4a4a-a11d-1c1dbbbf6340",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ea0dee1-9b08-4df4-bdea-f51b383080ae",
                    "LayerId": "c43150d7-1a10-4af6-90e1-0449859227c5"
                }
            ]
        },
        {
            "id": "5f24acec-4607-426e-9f8e-f9f21e11c4d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06c5acf2-d095-46c7-98cf-3fc298cf420d",
            "compositeImage": {
                "id": "8cb7406d-5f98-422b-8c13-eb76762d707f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f24acec-4607-426e-9f8e-f9f21e11c4d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e0e8848-f017-4fb4-9392-ec1010a4d5dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f24acec-4607-426e-9f8e-f9f21e11c4d4",
                    "LayerId": "c43150d7-1a10-4af6-90e1-0449859227c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c43150d7-1a10-4af6-90e1-0449859227c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06c5acf2-d095-46c7-98cf-3fc298cf420d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}