{
    "id": "bab07c83-eacb-447b-829c-5c2611978b36",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground_layer_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 779,
    "bbox_left": 0,
    "bbox_right": 2399,
    "bbox_top": 556,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c6ee1e7-5e48-448b-a01e-a60ce03eb3f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bab07c83-eacb-447b-829c-5c2611978b36",
            "compositeImage": {
                "id": "0e5c1c7d-e464-4b22-af40-b9ca41a2756f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c6ee1e7-5e48-448b-a01e-a60ce03eb3f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7909a1c-fb90-4856-af89-8b4fb75007c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c6ee1e7-5e48-448b-a01e-a60ce03eb3f1",
                    "LayerId": "f3478a79-c5c7-400d-bf6c-3a8d48f15e38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 780,
    "layers": [
        {
            "id": "f3478a79-c5c7-400d-bf6c-3a8d48f15e38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bab07c83-eacb-447b-829c-5c2611978b36",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2400,
    "xorig": 1200,
    "yorig": 390
}