{
    "id": "8195f3ca-0cac-4ea5-b4ce-a3eee9d2fb84",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_xboxone_y",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 9,
    "bbox_right": 90,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b533bcaf-19bc-4160-a98e-aecb806c5a53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8195f3ca-0cac-4ea5-b4ce-a3eee9d2fb84",
            "compositeImage": {
                "id": "0c1d522e-8ecb-4f35-bad1-14fe6d174f55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b533bcaf-19bc-4160-a98e-aecb806c5a53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b491d35a-dae5-4ded-8b74-038145c3c12a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b533bcaf-19bc-4160-a98e-aecb806c5a53",
                    "LayerId": "60a60733-cb91-4d92-b607-0b746af5f477"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "60a60733-cb91-4d92-b607-0b746af5f477",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8195f3ca-0cac-4ea5-b4ce-a3eee9d2fb84",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}