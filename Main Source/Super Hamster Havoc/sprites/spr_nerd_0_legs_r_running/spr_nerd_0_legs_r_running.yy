{
    "id": "6fa07f28-fb61-4796-a43d-b322846b6121",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nerd_0_legs_r_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 19,
    "bbox_right": 41,
    "bbox_top": 55,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69dfde64-c94c-4ead-8925-53dae6f1df4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fa07f28-fb61-4796-a43d-b322846b6121",
            "compositeImage": {
                "id": "aab09198-3ad9-48a5-957d-cf430551040e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69dfde64-c94c-4ead-8925-53dae6f1df4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e44d2f95-2e90-4142-91a0-8299962d1c98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69dfde64-c94c-4ead-8925-53dae6f1df4c",
                    "LayerId": "51412475-5453-43f7-a91b-632c5684024f"
                }
            ]
        },
        {
            "id": "e3c28209-f903-4ec7-af62-198380c5851c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fa07f28-fb61-4796-a43d-b322846b6121",
            "compositeImage": {
                "id": "cdea953f-855f-4567-afc1-cd034a56946b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3c28209-f903-4ec7-af62-198380c5851c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dee2c78-881f-4e38-b916-d7f9b4b1697e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3c28209-f903-4ec7-af62-198380c5851c",
                    "LayerId": "51412475-5453-43f7-a91b-632c5684024f"
                }
            ]
        },
        {
            "id": "4991daf1-2496-4b9d-a07a-5cd80b5e9c1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fa07f28-fb61-4796-a43d-b322846b6121",
            "compositeImage": {
                "id": "fbf373e0-2d12-47c1-9f6d-5b8599abfecc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4991daf1-2496-4b9d-a07a-5cd80b5e9c1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c1a54c7-5f5f-4b66-9192-f2ee2c76e76e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4991daf1-2496-4b9d-a07a-5cd80b5e9c1e",
                    "LayerId": "51412475-5453-43f7-a91b-632c5684024f"
                }
            ]
        },
        {
            "id": "229d38a1-43be-4035-93c6-60a25634eb14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fa07f28-fb61-4796-a43d-b322846b6121",
            "compositeImage": {
                "id": "3d10409c-e7c4-495a-b8c2-9545e8b3b119",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "229d38a1-43be-4035-93c6-60a25634eb14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad6e6d8f-6953-4dac-a0f0-78e156d3c549",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "229d38a1-43be-4035-93c6-60a25634eb14",
                    "LayerId": "51412475-5453-43f7-a91b-632c5684024f"
                }
            ]
        },
        {
            "id": "92d52190-b818-4687-84a5-525ea25d8fb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fa07f28-fb61-4796-a43d-b322846b6121",
            "compositeImage": {
                "id": "37dc3372-d260-406f-9ee2-0bd51e22380d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92d52190-b818-4687-84a5-525ea25d8fb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acde510e-9470-422f-9da2-5a7e4fce6c02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92d52190-b818-4687-84a5-525ea25d8fb6",
                    "LayerId": "51412475-5453-43f7-a91b-632c5684024f"
                }
            ]
        },
        {
            "id": "80dc83ed-e4cd-425f-b102-afaf80fb63cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fa07f28-fb61-4796-a43d-b322846b6121",
            "compositeImage": {
                "id": "7017aae5-81eb-4d4a-bd48-8f2f62c64f72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80dc83ed-e4cd-425f-b102-afaf80fb63cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cba4de77-4795-4927-9c2d-3abb61347250",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80dc83ed-e4cd-425f-b102-afaf80fb63cf",
                    "LayerId": "51412475-5453-43f7-a91b-632c5684024f"
                }
            ]
        },
        {
            "id": "d1f7c4e6-14a7-4e3c-994a-c102ca0eb842",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fa07f28-fb61-4796-a43d-b322846b6121",
            "compositeImage": {
                "id": "b65d35f8-0d96-4abf-96fa-f9d7eec791d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1f7c4e6-14a7-4e3c-994a-c102ca0eb842",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09ef7e8f-b8d8-42d7-ac18-04369decece4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1f7c4e6-14a7-4e3c-994a-c102ca0eb842",
                    "LayerId": "51412475-5453-43f7-a91b-632c5684024f"
                }
            ]
        },
        {
            "id": "5fd152c1-6d24-4398-9f5c-edf10a242920",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fa07f28-fb61-4796-a43d-b322846b6121",
            "compositeImage": {
                "id": "6b38f54e-8f6d-4af0-8150-3f705ec91a31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fd152c1-6d24-4398-9f5c-edf10a242920",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cd52ab5-ffbc-410d-9aed-0c668bd97445",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fd152c1-6d24-4398-9f5c-edf10a242920",
                    "LayerId": "51412475-5453-43f7-a91b-632c5684024f"
                }
            ]
        },
        {
            "id": "5bf7f865-1c9d-4743-bdda-4a0656f55f1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fa07f28-fb61-4796-a43d-b322846b6121",
            "compositeImage": {
                "id": "8641b66b-c1ba-4375-8d1c-fc8d009ea2c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bf7f865-1c9d-4743-bdda-4a0656f55f1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a319de7-6061-491c-a8b1-e8a8a64aac06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bf7f865-1c9d-4743-bdda-4a0656f55f1d",
                    "LayerId": "51412475-5453-43f7-a91b-632c5684024f"
                }
            ]
        },
        {
            "id": "66372052-3183-437e-bf2b-ab3514d0daad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6fa07f28-fb61-4796-a43d-b322846b6121",
            "compositeImage": {
                "id": "1e1a6ca3-ed81-47df-a23f-6ab222b3ad7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66372052-3183-437e-bf2b-ab3514d0daad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc62a5a2-2ff1-43bc-977d-0228167b4ef9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66372052-3183-437e-bf2b-ab3514d0daad",
                    "LayerId": "51412475-5453-43f7-a91b-632c5684024f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "51412475-5453-43f7-a91b-632c5684024f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6fa07f28-fb61-4796-a43d-b322846b6121",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}