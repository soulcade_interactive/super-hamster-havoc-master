{
    "id": "1b79285c-d5d5-4b83-9d5d-0e8270121e1a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_0_head_r_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 7,
    "bbox_right": 54,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cfda0dd0-abc7-4283-9460-c63f04c7d03f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b79285c-d5d5-4b83-9d5d-0e8270121e1a",
            "compositeImage": {
                "id": "feb3d700-583e-49ec-8a44-364789648cb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfda0dd0-abc7-4283-9460-c63f04c7d03f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30715af3-9e98-422e-ad08-76964069846d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfda0dd0-abc7-4283-9460-c63f04c7d03f",
                    "LayerId": "93aa4abf-f764-4659-9eef-022c16a08a3b"
                }
            ]
        },
        {
            "id": "8b1376f7-0c92-4988-9f0d-c25b625405d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b79285c-d5d5-4b83-9d5d-0e8270121e1a",
            "compositeImage": {
                "id": "77b466d3-70f7-45df-b6ee-1002b0b0bc10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b1376f7-0c92-4988-9f0d-c25b625405d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "569903f7-14f8-4bd5-afe3-e5d8fd8cea45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b1376f7-0c92-4988-9f0d-c25b625405d8",
                    "LayerId": "93aa4abf-f764-4659-9eef-022c16a08a3b"
                }
            ]
        },
        {
            "id": "1a2ded57-1e21-42ec-b2a6-1510dd8546d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b79285c-d5d5-4b83-9d5d-0e8270121e1a",
            "compositeImage": {
                "id": "1beef8ae-52bb-4cf5-9833-1ef3a9b7f7d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a2ded57-1e21-42ec-b2a6-1510dd8546d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc7c96b7-e576-4fd9-b974-970831cb9014",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a2ded57-1e21-42ec-b2a6-1510dd8546d5",
                    "LayerId": "93aa4abf-f764-4659-9eef-022c16a08a3b"
                }
            ]
        },
        {
            "id": "ca043b70-fd3a-410f-b743-d91c0739e50e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b79285c-d5d5-4b83-9d5d-0e8270121e1a",
            "compositeImage": {
                "id": "9b7a492a-be76-4dac-9403-8307bf55cf8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca043b70-fd3a-410f-b743-d91c0739e50e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11c43efe-e6f5-43bb-8cbc-3b88ea895fba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca043b70-fd3a-410f-b743-d91c0739e50e",
                    "LayerId": "93aa4abf-f764-4659-9eef-022c16a08a3b"
                }
            ]
        },
        {
            "id": "1d7e09ea-3286-46df-9e9d-c379a3afbc22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b79285c-d5d5-4b83-9d5d-0e8270121e1a",
            "compositeImage": {
                "id": "4ab9d246-3ecf-44dd-abd4-989cf25c421f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d7e09ea-3286-46df-9e9d-c379a3afbc22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62ba8564-99ba-46e4-8fea-fe6f4fb5ffb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d7e09ea-3286-46df-9e9d-c379a3afbc22",
                    "LayerId": "93aa4abf-f764-4659-9eef-022c16a08a3b"
                }
            ]
        },
        {
            "id": "08ca42e4-8997-4e61-acf7-5ddb17c23908",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b79285c-d5d5-4b83-9d5d-0e8270121e1a",
            "compositeImage": {
                "id": "a19c9af8-f394-4bdd-9cbd-543312111dd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08ca42e4-8997-4e61-acf7-5ddb17c23908",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cce2792-541d-4e1e-b9c0-0e44d71310a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08ca42e4-8997-4e61-acf7-5ddb17c23908",
                    "LayerId": "93aa4abf-f764-4659-9eef-022c16a08a3b"
                }
            ]
        },
        {
            "id": "5a124519-bd2a-4c88-af5d-03c91a76d0f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b79285c-d5d5-4b83-9d5d-0e8270121e1a",
            "compositeImage": {
                "id": "803ff706-12d2-4358-827d-b6292f03c274",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a124519-bd2a-4c88-af5d-03c91a76d0f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43f8426a-e2ff-4686-be1d-a7764ad813ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a124519-bd2a-4c88-af5d-03c91a76d0f1",
                    "LayerId": "93aa4abf-f764-4659-9eef-022c16a08a3b"
                }
            ]
        },
        {
            "id": "50a516bf-0456-4c9d-aab8-a234ca52ae08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b79285c-d5d5-4b83-9d5d-0e8270121e1a",
            "compositeImage": {
                "id": "2167f3c9-352b-4de7-90db-ad2cea8f9e0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50a516bf-0456-4c9d-aab8-a234ca52ae08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdbd44db-86eb-430c-8fbb-ef08d8724a7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50a516bf-0456-4c9d-aab8-a234ca52ae08",
                    "LayerId": "93aa4abf-f764-4659-9eef-022c16a08a3b"
                }
            ]
        },
        {
            "id": "e5c66a65-faf0-4348-a0c4-ec1bdc1eaf78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b79285c-d5d5-4b83-9d5d-0e8270121e1a",
            "compositeImage": {
                "id": "4caac468-017f-420c-8478-b6444b516b02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5c66a65-faf0-4348-a0c4-ec1bdc1eaf78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f8ad39a-8d02-4a59-a24a-198955e75648",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5c66a65-faf0-4348-a0c4-ec1bdc1eaf78",
                    "LayerId": "93aa4abf-f764-4659-9eef-022c16a08a3b"
                }
            ]
        },
        {
            "id": "28431dde-3838-472e-ab60-564b6386d47f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b79285c-d5d5-4b83-9d5d-0e8270121e1a",
            "compositeImage": {
                "id": "aa301b21-cb28-4e1a-9e32-348787aac10a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28431dde-3838-472e-ab60-564b6386d47f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01909b9d-acc9-49fc-bc84-17e7b97c77a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28431dde-3838-472e-ab60-564b6386d47f",
                    "LayerId": "93aa4abf-f764-4659-9eef-022c16a08a3b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "93aa4abf-f764-4659-9eef-022c16a08a3b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b79285c-d5d5-4b83-9d5d-0e8270121e1a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}