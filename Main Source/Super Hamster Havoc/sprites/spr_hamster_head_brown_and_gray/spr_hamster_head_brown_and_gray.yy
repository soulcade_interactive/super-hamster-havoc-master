{
    "id": "de4a520f-4a1b-4a1e-a03b-a513dc21f28a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_head_brown_and_gray",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6f49593-1b40-49a0-9a61-f2d6bb2aa8a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de4a520f-4a1b-4a1e-a03b-a513dc21f28a",
            "compositeImage": {
                "id": "04fbfe7a-4215-49e3-884e-dca763e1b4f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6f49593-1b40-49a0-9a61-f2d6bb2aa8a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e06d70c6-71a8-4e94-93c3-1adfa06acb6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6f49593-1b40-49a0-9a61-f2d6bb2aa8a1",
                    "LayerId": "d297851f-9177-4f39-bf4e-ba32b34f1a27"
                }
            ]
        },
        {
            "id": "0c099021-6994-4439-abbb-e6eb5f91ad76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de4a520f-4a1b-4a1e-a03b-a513dc21f28a",
            "compositeImage": {
                "id": "58ec26d8-7e40-4d15-a0c1-f6e2e90dbf9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c099021-6994-4439-abbb-e6eb5f91ad76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0f6e286-7623-40d8-91cc-495b3e0d96f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c099021-6994-4439-abbb-e6eb5f91ad76",
                    "LayerId": "d297851f-9177-4f39-bf4e-ba32b34f1a27"
                }
            ]
        },
        {
            "id": "bdf8bc96-1fb7-44be-8eeb-29eab5bbb5ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de4a520f-4a1b-4a1e-a03b-a513dc21f28a",
            "compositeImage": {
                "id": "af34339a-986f-43e7-82dd-0a91b5af6c80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdf8bc96-1fb7-44be-8eeb-29eab5bbb5ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2361c9ed-4734-4311-ae9d-3c6e81723581",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdf8bc96-1fb7-44be-8eeb-29eab5bbb5ed",
                    "LayerId": "d297851f-9177-4f39-bf4e-ba32b34f1a27"
                }
            ]
        },
        {
            "id": "a543b761-7313-4a78-be0c-30f203b9df7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de4a520f-4a1b-4a1e-a03b-a513dc21f28a",
            "compositeImage": {
                "id": "a3f68861-20b2-4ac0-b25f-cfbb6c9fe054",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a543b761-7313-4a78-be0c-30f203b9df7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c92f5ff7-26c9-4c4e-b7f8-90fc13a0c104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a543b761-7313-4a78-be0c-30f203b9df7a",
                    "LayerId": "d297851f-9177-4f39-bf4e-ba32b34f1a27"
                }
            ]
        },
        {
            "id": "e80a2df1-64cb-4238-8a23-c9520ad0b1c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de4a520f-4a1b-4a1e-a03b-a513dc21f28a",
            "compositeImage": {
                "id": "54fb83c0-5a3f-4548-80d3-f5287f3a318a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e80a2df1-64cb-4238-8a23-c9520ad0b1c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efdc21f7-2df3-49b4-a44b-fdf45d39a339",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e80a2df1-64cb-4238-8a23-c9520ad0b1c9",
                    "LayerId": "d297851f-9177-4f39-bf4e-ba32b34f1a27"
                }
            ]
        },
        {
            "id": "70d76bd0-d008-4a1c-8d93-36c5aef48b2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de4a520f-4a1b-4a1e-a03b-a513dc21f28a",
            "compositeImage": {
                "id": "cc8196bd-bebb-4056-a7b7-7c223d0e8534",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70d76bd0-d008-4a1c-8d93-36c5aef48b2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e6682c5-d838-4256-807e-12afe1c536bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70d76bd0-d008-4a1c-8d93-36c5aef48b2a",
                    "LayerId": "d297851f-9177-4f39-bf4e-ba32b34f1a27"
                }
            ]
        },
        {
            "id": "da891fd0-b73a-4217-b804-89706c7d778a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de4a520f-4a1b-4a1e-a03b-a513dc21f28a",
            "compositeImage": {
                "id": "cdee0ee2-730b-407a-b48c-929dc18006dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da891fd0-b73a-4217-b804-89706c7d778a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6dd49bd-2433-457f-bddf-57c31398ac66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da891fd0-b73a-4217-b804-89706c7d778a",
                    "LayerId": "d297851f-9177-4f39-bf4e-ba32b34f1a27"
                }
            ]
        },
        {
            "id": "aa914b63-76a9-4efb-969e-8a710745d6f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de4a520f-4a1b-4a1e-a03b-a513dc21f28a",
            "compositeImage": {
                "id": "04df8767-e333-4f22-83f6-05c72a900d56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa914b63-76a9-4efb-969e-8a710745d6f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ab4c298-7475-402a-ae95-cb716f9a3124",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa914b63-76a9-4efb-969e-8a710745d6f3",
                    "LayerId": "d297851f-9177-4f39-bf4e-ba32b34f1a27"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d297851f-9177-4f39-bf4e-ba32b34f1a27",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de4a520f-4a1b-4a1e-a03b-a513dc21f28a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 28
}