{
    "id": "f5718b43-b724-440c-8035-e1c0a047fac2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_water_grassland",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8e86f7ef-7f16-486e-8f6e-1ba08039c2be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5718b43-b724-440c-8035-e1c0a047fac2",
            "compositeImage": {
                "id": "7cd331df-2433-4e0f-853f-a45d4da8a241",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e86f7ef-7f16-486e-8f6e-1ba08039c2be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00512a16-4392-4bd1-81de-e17c8be6e1dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e86f7ef-7f16-486e-8f6e-1ba08039c2be",
                    "LayerId": "65d3590d-25fa-4d2f-936d-7655e3dd810b"
                }
            ]
        },
        {
            "id": "fb172434-b016-4261-a753-e95d01863cc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5718b43-b724-440c-8035-e1c0a047fac2",
            "compositeImage": {
                "id": "edc406b9-40b9-4bd7-b477-3f2bfade5d0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb172434-b016-4261-a753-e95d01863cc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff96d671-1003-4bf2-9303-014af481b89a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb172434-b016-4261-a753-e95d01863cc4",
                    "LayerId": "65d3590d-25fa-4d2f-936d-7655e3dd810b"
                }
            ]
        },
        {
            "id": "1f2071a2-f31d-4ae0-b20e-3153f09ea9a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5718b43-b724-440c-8035-e1c0a047fac2",
            "compositeImage": {
                "id": "fa9dd68e-1742-456f-bcb2-a7aadd17bfb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f2071a2-f31d-4ae0-b20e-3153f09ea9a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22dfe616-4a15-42cb-ae51-2ab08d6d3e62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f2071a2-f31d-4ae0-b20e-3153f09ea9a8",
                    "LayerId": "65d3590d-25fa-4d2f-936d-7655e3dd810b"
                }
            ]
        },
        {
            "id": "e991cd1b-4af8-4ed8-afa7-cd739b2dc666",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5718b43-b724-440c-8035-e1c0a047fac2",
            "compositeImage": {
                "id": "be6f4a59-070f-40f8-b144-d527a3ec527f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e991cd1b-4af8-4ed8-afa7-cd739b2dc666",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9950c7f-b857-415d-a145-6e95761d1876",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e991cd1b-4af8-4ed8-afa7-cd739b2dc666",
                    "LayerId": "65d3590d-25fa-4d2f-936d-7655e3dd810b"
                }
            ]
        },
        {
            "id": "2ad2ae00-6f17-402d-9ce5-faf2867dafb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5718b43-b724-440c-8035-e1c0a047fac2",
            "compositeImage": {
                "id": "fe6f6de2-b773-4093-bee8-b5f8e9a413c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ad2ae00-6f17-402d-9ce5-faf2867dafb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "091c11d6-95e0-4060-bf72-951bb4a99218",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ad2ae00-6f17-402d-9ce5-faf2867dafb5",
                    "LayerId": "65d3590d-25fa-4d2f-936d-7655e3dd810b"
                }
            ]
        },
        {
            "id": "fec4cb9c-10cd-4215-8607-be85f737ce98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5718b43-b724-440c-8035-e1c0a047fac2",
            "compositeImage": {
                "id": "aee4c7dd-9ff3-4794-838c-d00781d65696",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fec4cb9c-10cd-4215-8607-be85f737ce98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07dab053-c38e-4b59-950b-b3e69b1d39c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fec4cb9c-10cd-4215-8607-be85f737ce98",
                    "LayerId": "65d3590d-25fa-4d2f-936d-7655e3dd810b"
                }
            ]
        },
        {
            "id": "5435e85d-37d5-4a35-b037-30118bf94491",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5718b43-b724-440c-8035-e1c0a047fac2",
            "compositeImage": {
                "id": "73b09160-3bcd-4cc5-b489-bae73763d0f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5435e85d-37d5-4a35-b037-30118bf94491",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ec2dba3-67c1-4f3e-a773-8171094e4934",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5435e85d-37d5-4a35-b037-30118bf94491",
                    "LayerId": "65d3590d-25fa-4d2f-936d-7655e3dd810b"
                }
            ]
        },
        {
            "id": "f565c206-dd8f-4c8c-8126-240ae7afc5ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5718b43-b724-440c-8035-e1c0a047fac2",
            "compositeImage": {
                "id": "192e9eee-5487-496c-8d7b-0fc6d3ab1fa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f565c206-dd8f-4c8c-8126-240ae7afc5ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5abde778-a70b-4c71-9431-d5bd92960a99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f565c206-dd8f-4c8c-8126-240ae7afc5ba",
                    "LayerId": "65d3590d-25fa-4d2f-936d-7655e3dd810b"
                }
            ]
        },
        {
            "id": "47e9e180-c9ad-43b9-9419-7676461fcd4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5718b43-b724-440c-8035-e1c0a047fac2",
            "compositeImage": {
                "id": "4ed78cbf-859d-428f-abc2-59d150507d94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47e9e180-c9ad-43b9-9419-7676461fcd4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca293a9e-bd53-47de-8116-14aff82ba239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47e9e180-c9ad-43b9-9419-7676461fcd4a",
                    "LayerId": "65d3590d-25fa-4d2f-936d-7655e3dd810b"
                }
            ]
        },
        {
            "id": "3effdabf-28e7-4b4f-a224-15f07c957cf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5718b43-b724-440c-8035-e1c0a047fac2",
            "compositeImage": {
                "id": "867d9c6e-b29e-425b-8b40-db2d7475b48b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3effdabf-28e7-4b4f-a224-15f07c957cf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2e4b3ce-d2b6-409b-915a-3ef2a58771ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3effdabf-28e7-4b4f-a224-15f07c957cf0",
                    "LayerId": "65d3590d-25fa-4d2f-936d-7655e3dd810b"
                }
            ]
        },
        {
            "id": "426127dc-5a3a-4140-9225-f21b75a529a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5718b43-b724-440c-8035-e1c0a047fac2",
            "compositeImage": {
                "id": "1f7a1240-158c-4a31-a606-9e62c8a9c5cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "426127dc-5a3a-4140-9225-f21b75a529a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a62aec0-d8ff-482e-bddc-807d75a0b060",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "426127dc-5a3a-4140-9225-f21b75a529a5",
                    "LayerId": "65d3590d-25fa-4d2f-936d-7655e3dd810b"
                }
            ]
        },
        {
            "id": "fd04d4df-89e6-472b-a69f-3e74553a3a7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5718b43-b724-440c-8035-e1c0a047fac2",
            "compositeImage": {
                "id": "ffcb808a-463a-45a3-8982-f4aa2459d014",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd04d4df-89e6-472b-a69f-3e74553a3a7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bc9df95-dc7b-41bf-9d9b-0250e05e8554",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd04d4df-89e6-472b-a69f-3e74553a3a7d",
                    "LayerId": "65d3590d-25fa-4d2f-936d-7655e3dd810b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "65d3590d-25fa-4d2f-936d-7655e3dd810b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5718b43-b724-440c-8035-e1c0a047fac2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}