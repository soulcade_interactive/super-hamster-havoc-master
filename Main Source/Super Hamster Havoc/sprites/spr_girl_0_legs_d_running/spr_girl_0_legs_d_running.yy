{
    "id": "3c4ffbd1-509b-4536-980c-99a91ba6916d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_girl_0_legs_d_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 25,
    "bbox_right": 38,
    "bbox_top": 56,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b98f98b5-7508-4fa6-83cf-510499693d04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4ffbd1-509b-4536-980c-99a91ba6916d",
            "compositeImage": {
                "id": "4bd95b9e-3524-438c-9e45-28c1a627c301",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b98f98b5-7508-4fa6-83cf-510499693d04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64a68ca8-49f1-4be6-989e-69c6bee13330",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b98f98b5-7508-4fa6-83cf-510499693d04",
                    "LayerId": "cf690376-7af5-4b15-a330-855387030775"
                }
            ]
        },
        {
            "id": "1efe31a9-1ad8-4d7f-825d-f177a937d9d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4ffbd1-509b-4536-980c-99a91ba6916d",
            "compositeImage": {
                "id": "a5f9d952-494a-43d7-bdf4-67e40f00030f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1efe31a9-1ad8-4d7f-825d-f177a937d9d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3af609dc-b9ac-45dd-a857-7c3f9b92cf59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1efe31a9-1ad8-4d7f-825d-f177a937d9d6",
                    "LayerId": "cf690376-7af5-4b15-a330-855387030775"
                }
            ]
        },
        {
            "id": "7503dfb9-d2b7-4de2-82f7-c39496e2072d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4ffbd1-509b-4536-980c-99a91ba6916d",
            "compositeImage": {
                "id": "eca04a80-84d2-4768-925a-10d3d4aea0b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7503dfb9-d2b7-4de2-82f7-c39496e2072d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8623c23-d4fc-4879-a51b-1636059081ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7503dfb9-d2b7-4de2-82f7-c39496e2072d",
                    "LayerId": "cf690376-7af5-4b15-a330-855387030775"
                }
            ]
        },
        {
            "id": "43e8a677-afdd-4a2c-8f1d-33f8a4dbf48b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4ffbd1-509b-4536-980c-99a91ba6916d",
            "compositeImage": {
                "id": "5001ffe4-45b6-43ed-a29a-d3be44114168",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43e8a677-afdd-4a2c-8f1d-33f8a4dbf48b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf32838a-04fb-48fb-9501-bc5597a58493",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43e8a677-afdd-4a2c-8f1d-33f8a4dbf48b",
                    "LayerId": "cf690376-7af5-4b15-a330-855387030775"
                }
            ]
        },
        {
            "id": "b6ea0f58-ca36-4b00-bbb9-270ac7a46824",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4ffbd1-509b-4536-980c-99a91ba6916d",
            "compositeImage": {
                "id": "b8c2f86b-ac3c-41dc-952b-6403f93a4049",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6ea0f58-ca36-4b00-bbb9-270ac7a46824",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f84cdb0-4ea4-480a-88fe-7dff872bdaf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6ea0f58-ca36-4b00-bbb9-270ac7a46824",
                    "LayerId": "cf690376-7af5-4b15-a330-855387030775"
                }
            ]
        },
        {
            "id": "ed475c8f-80ea-408d-b221-b09a383b785f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4ffbd1-509b-4536-980c-99a91ba6916d",
            "compositeImage": {
                "id": "350f3534-8410-44df-9924-35b08e553992",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed475c8f-80ea-408d-b221-b09a383b785f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08d6462f-b9a2-4b3a-8816-627db5717bee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed475c8f-80ea-408d-b221-b09a383b785f",
                    "LayerId": "cf690376-7af5-4b15-a330-855387030775"
                }
            ]
        },
        {
            "id": "1ffb55d3-9d3c-4caf-9202-b5435155b3b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4ffbd1-509b-4536-980c-99a91ba6916d",
            "compositeImage": {
                "id": "2f486f87-a951-4504-a1b0-50376215990f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ffb55d3-9d3c-4caf-9202-b5435155b3b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45f0bb31-8b86-4a9d-8e2f-f3fcb03cdbe3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ffb55d3-9d3c-4caf-9202-b5435155b3b8",
                    "LayerId": "cf690376-7af5-4b15-a330-855387030775"
                }
            ]
        },
        {
            "id": "d6cbbab5-b51d-4ed8-b67f-34b202a3501e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4ffbd1-509b-4536-980c-99a91ba6916d",
            "compositeImage": {
                "id": "f8c79a62-c16c-4944-9c65-1b9c9ba4e950",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6cbbab5-b51d-4ed8-b67f-34b202a3501e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ae2fd5b-76d2-413c-acf2-b3477b856fd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6cbbab5-b51d-4ed8-b67f-34b202a3501e",
                    "LayerId": "cf690376-7af5-4b15-a330-855387030775"
                }
            ]
        },
        {
            "id": "bcaa5edf-09d8-4610-83e4-f2c68d9ad9d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4ffbd1-509b-4536-980c-99a91ba6916d",
            "compositeImage": {
                "id": "bcef37cd-6522-4cbf-a5e7-8b116238b53d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcaa5edf-09d8-4610-83e4-f2c68d9ad9d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dc60cc9-d647-4ba4-8cf3-ebe048ddae83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcaa5edf-09d8-4610-83e4-f2c68d9ad9d2",
                    "LayerId": "cf690376-7af5-4b15-a330-855387030775"
                }
            ]
        },
        {
            "id": "966a0e00-2671-482e-a69e-4a687fe96bab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c4ffbd1-509b-4536-980c-99a91ba6916d",
            "compositeImage": {
                "id": "c81d551d-336a-4178-9c40-e98622b0ab21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "966a0e00-2671-482e-a69e-4a687fe96bab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50b03554-5e96-487e-953b-22c159ae123d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "966a0e00-2671-482e-a69e-4a687fe96bab",
                    "LayerId": "cf690376-7af5-4b15-a330-855387030775"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cf690376-7af5-4b15-a330-855387030775",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c4ffbd1-509b-4536-980c-99a91ba6916d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}