{
    "id": "a67c2288-42c2-419a-bf40-2a79f89acd62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23a7a06a-b05e-4085-88d8-8fc4ea0b3541",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a67c2288-42c2-419a-bf40-2a79f89acd62",
            "compositeImage": {
                "id": "20e2bafc-c4ee-4bc2-810a-7355606fb571",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23a7a06a-b05e-4085-88d8-8fc4ea0b3541",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "128c7847-dbfd-48ab-b897-53bda64b82a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23a7a06a-b05e-4085-88d8-8fc4ea0b3541",
                    "LayerId": "20623228-3a20-414d-b8ea-d4a2bc9b479e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "20623228-3a20-414d-b8ea-d4a2bc9b479e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a67c2288-42c2-419a-bf40-2a79f89acd62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}