{
    "id": "c5156e44-2888-4d0a-a3f0-33b3dbc27341",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flower3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6f163c1-c798-45bb-8c0c-755b14f8f466",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5156e44-2888-4d0a-a3f0-33b3dbc27341",
            "compositeImage": {
                "id": "88664d83-4f62-403b-9412-01adef1fbd61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6f163c1-c798-45bb-8c0c-755b14f8f466",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddaf6026-532f-4083-b692-832f58e42b8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6f163c1-c798-45bb-8c0c-755b14f8f466",
                    "LayerId": "8292fd3c-d1a7-404d-8964-29c62d9b4315"
                }
            ]
        },
        {
            "id": "7ddc6f85-1ff7-4900-bfec-1ce31c479726",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5156e44-2888-4d0a-a3f0-33b3dbc27341",
            "compositeImage": {
                "id": "a2b6399c-4dc1-497a-93e8-65c06ebfb05e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ddc6f85-1ff7-4900-bfec-1ce31c479726",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61b3c418-dc74-46d9-8a00-502d91ff9e61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ddc6f85-1ff7-4900-bfec-1ce31c479726",
                    "LayerId": "8292fd3c-d1a7-404d-8964-29c62d9b4315"
                }
            ]
        },
        {
            "id": "b25f401a-efd4-4d50-829e-dd948af34ccf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5156e44-2888-4d0a-a3f0-33b3dbc27341",
            "compositeImage": {
                "id": "29b7f7d7-a687-4eee-af7b-9740b776c5a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b25f401a-efd4-4d50-829e-dd948af34ccf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79c2b867-d51d-42bf-9d67-b99b6ba119be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b25f401a-efd4-4d50-829e-dd948af34ccf",
                    "LayerId": "8292fd3c-d1a7-404d-8964-29c62d9b4315"
                }
            ]
        },
        {
            "id": "c9ed7d39-7ad9-42a3-ab37-a8d2ae4448a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5156e44-2888-4d0a-a3f0-33b3dbc27341",
            "compositeImage": {
                "id": "2667ccf1-8d6f-49cd-8f4e-b9789aeacb2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9ed7d39-7ad9-42a3-ab37-a8d2ae4448a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e5636aa-f9d3-4593-81f4-37e0bc3b88da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9ed7d39-7ad9-42a3-ab37-a8d2ae4448a1",
                    "LayerId": "8292fd3c-d1a7-404d-8964-29c62d9b4315"
                }
            ]
        },
        {
            "id": "b85faa05-ec2f-4502-9fbc-23ee107d7d50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5156e44-2888-4d0a-a3f0-33b3dbc27341",
            "compositeImage": {
                "id": "6ab7b901-fb10-4205-b8c9-903b8d8c87b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b85faa05-ec2f-4502-9fbc-23ee107d7d50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "710962f3-63f8-4f37-b25f-d30a872c3c2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b85faa05-ec2f-4502-9fbc-23ee107d7d50",
                    "LayerId": "8292fd3c-d1a7-404d-8964-29c62d9b4315"
                }
            ]
        },
        {
            "id": "defe64f0-922b-497d-937d-894b9e371250",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c5156e44-2888-4d0a-a3f0-33b3dbc27341",
            "compositeImage": {
                "id": "cb931166-2859-459f-bb44-575bd183e260",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "defe64f0-922b-497d-937d-894b9e371250",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93c143fe-a5f9-44c8-90d5-ec63c8bff676",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "defe64f0-922b-497d-937d-894b9e371250",
                    "LayerId": "8292fd3c-d1a7-404d-8964-29c62d9b4315"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "8292fd3c-d1a7-404d-8964-29c62d9b4315",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c5156e44-2888-4d0a-a3f0-33b3dbc27341",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}