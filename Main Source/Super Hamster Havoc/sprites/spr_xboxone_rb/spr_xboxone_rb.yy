{
    "id": "2209e697-bd93-4761-aec8-130e931817f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_xboxone_rb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 6,
    "bbox_right": 93,
    "bbox_top": 27,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1e9b15d-cac8-4a17-8c4e-0f4d22ffbcab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2209e697-bd93-4761-aec8-130e931817f5",
            "compositeImage": {
                "id": "8f309415-8e1e-4670-a44e-1c16ff8ebf40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1e9b15d-cac8-4a17-8c4e-0f4d22ffbcab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eabf987-d74a-44ad-bf97-3d661e80429a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1e9b15d-cac8-4a17-8c4e-0f4d22ffbcab",
                    "LayerId": "d0f69227-3ae3-47f9-af3e-3000dda2f82f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "d0f69227-3ae3-47f9-af3e-3000dda2f82f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2209e697-bd93-4761-aec8-130e931817f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}