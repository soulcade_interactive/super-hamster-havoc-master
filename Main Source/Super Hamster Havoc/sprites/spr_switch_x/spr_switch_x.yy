{
    "id": "f4a62d1c-d59c-49d1-8de6-b74a0243824b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_switch_x",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 9,
    "bbox_right": 90,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "58e6b4c2-2de1-4c7b-85df-1149bc1466b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4a62d1c-d59c-49d1-8de6-b74a0243824b",
            "compositeImage": {
                "id": "1cd8542c-c96a-499b-82d7-891bf259df54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58e6b4c2-2de1-4c7b-85df-1149bc1466b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af48f3dd-2778-494e-ae7b-d99c8ccd90bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58e6b4c2-2de1-4c7b-85df-1149bc1466b6",
                    "LayerId": "4fb0c713-a1ea-4fa6-9984-cceb0ce455d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "4fb0c713-a1ea-4fa6-9984-cceb0ce455d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4a62d1c-d59c-49d1-8de6-b74a0243824b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}