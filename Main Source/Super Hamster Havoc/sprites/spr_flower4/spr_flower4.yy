{
    "id": "42ff55ac-c2dc-49b2-a0af-854236da1d61",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_flower4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7346fd86-5000-4a51-9ea8-15bdd858499e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42ff55ac-c2dc-49b2-a0af-854236da1d61",
            "compositeImage": {
                "id": "444c94cf-b4fe-4d79-b3c0-1b17a6c231fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7346fd86-5000-4a51-9ea8-15bdd858499e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "703856da-2794-4d19-885f-36f07ea6a7a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7346fd86-5000-4a51-9ea8-15bdd858499e",
                    "LayerId": "c64c6233-f3a4-4d81-9315-c4cc5bd62d18"
                }
            ]
        },
        {
            "id": "727c8c40-e1d3-43bc-8944-bf9f5e2ba1c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42ff55ac-c2dc-49b2-a0af-854236da1d61",
            "compositeImage": {
                "id": "8c640f41-ba2d-4b33-93d9-530754fc6b78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "727c8c40-e1d3-43bc-8944-bf9f5e2ba1c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1eb5aacd-c4ce-4d62-aec0-fd695798e508",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "727c8c40-e1d3-43bc-8944-bf9f5e2ba1c3",
                    "LayerId": "c64c6233-f3a4-4d81-9315-c4cc5bd62d18"
                }
            ]
        },
        {
            "id": "ef565659-09e3-47c3-b1d9-38f980ef30bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42ff55ac-c2dc-49b2-a0af-854236da1d61",
            "compositeImage": {
                "id": "dca0af08-2bcb-4c02-92b4-a3e4956ad669",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef565659-09e3-47c3-b1d9-38f980ef30bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c67dfc7-f940-4bb4-926e-a761776ebad8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef565659-09e3-47c3-b1d9-38f980ef30bd",
                    "LayerId": "c64c6233-f3a4-4d81-9315-c4cc5bd62d18"
                }
            ]
        },
        {
            "id": "c12203cc-2648-4cba-9f7a-f129d355cbfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42ff55ac-c2dc-49b2-a0af-854236da1d61",
            "compositeImage": {
                "id": "cec6e516-c970-4465-93cc-bb45bcf2ce30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c12203cc-2648-4cba-9f7a-f129d355cbfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bff84f5e-ce1a-4840-9cf3-df2b5eaaa582",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c12203cc-2648-4cba-9f7a-f129d355cbfb",
                    "LayerId": "c64c6233-f3a4-4d81-9315-c4cc5bd62d18"
                }
            ]
        },
        {
            "id": "310d2b05-6263-49d5-b680-052328c7f147",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42ff55ac-c2dc-49b2-a0af-854236da1d61",
            "compositeImage": {
                "id": "eace2d6a-7fec-4ffe-a37c-2cc0756ba863",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "310d2b05-6263-49d5-b680-052328c7f147",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6290fb3-c3fd-41d4-99dc-4f53e961a83b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "310d2b05-6263-49d5-b680-052328c7f147",
                    "LayerId": "c64c6233-f3a4-4d81-9315-c4cc5bd62d18"
                }
            ]
        },
        {
            "id": "a53b6d79-652b-4fd7-8b16-10fd8dd34e1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42ff55ac-c2dc-49b2-a0af-854236da1d61",
            "compositeImage": {
                "id": "9d9c5ec8-da86-4724-8503-d2a63664a3cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a53b6d79-652b-4fd7-8b16-10fd8dd34e1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a683ee51-2eac-4cec-a1d3-45865740144e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a53b6d79-652b-4fd7-8b16-10fd8dd34e1e",
                    "LayerId": "c64c6233-f3a4-4d81-9315-c4cc5bd62d18"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "c64c6233-f3a4-4d81-9315-c4cc5bd62d18",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "42ff55ac-c2dc-49b2-a0af-854236da1d61",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}