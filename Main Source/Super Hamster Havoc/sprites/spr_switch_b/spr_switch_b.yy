{
    "id": "406d38dc-f2e2-4c51-884f-ac4f1d5f887f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_switch_b",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 9,
    "bbox_right": 90,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b703edaf-385d-44c7-b644-d627b266d6a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "406d38dc-f2e2-4c51-884f-ac4f1d5f887f",
            "compositeImage": {
                "id": "39125c7a-fe8b-4b52-81f4-98c04e31d66c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b703edaf-385d-44c7-b644-d627b266d6a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e08dc00c-5637-4e6c-a80f-e5876aab901c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b703edaf-385d-44c7-b644-d627b266d6a6",
                    "LayerId": "8c691a3c-c17b-43e2-a174-a1193de96ae8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "8c691a3c-c17b-43e2-a174-a1193de96ae8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "406d38dc-f2e2-4c51-884f-ac4f1d5f887f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}