{
    "id": "0a3f2ee7-4b8b-4b28-b7a5-7111df41d3fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_xboxone_rstick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 5,
    "bbox_right": 94,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e89abe7e-bf33-4a4d-bd08-1c31330698ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0a3f2ee7-4b8b-4b28-b7a5-7111df41d3fa",
            "compositeImage": {
                "id": "f29af52d-32dc-4596-891d-7ed4fab9cbda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e89abe7e-bf33-4a4d-bd08-1c31330698ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a21042e6-45c2-4a09-be06-dc9fe28e1e01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e89abe7e-bf33-4a4d-bd08-1c31330698ce",
                    "LayerId": "a79aa321-8d13-4995-8cdc-6009a0c585e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "a79aa321-8d13-4995-8cdc-6009a0c585e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0a3f2ee7-4b8b-4b28-b7a5-7111df41d3fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}