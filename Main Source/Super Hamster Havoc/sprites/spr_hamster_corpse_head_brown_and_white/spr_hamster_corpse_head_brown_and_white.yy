{
    "id": "3b3cbce9-4357-48df-8ace-ecd48bb2e44a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_corpse_head_brown_and_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ad8bf91-b661-4fff-bf25-11b839d3eecd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b3cbce9-4357-48df-8ace-ecd48bb2e44a",
            "compositeImage": {
                "id": "69b26ecd-e2f0-492f-8d9c-54092f5396e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ad8bf91-b661-4fff-bf25-11b839d3eecd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c580bf3-61f0-4092-a1d4-3384c1d1bf90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ad8bf91-b661-4fff-bf25-11b839d3eecd",
                    "LayerId": "44193277-0913-4877-95d6-505b63f16a38"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "44193277-0913-4877-95d6-505b63f16a38",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b3cbce9-4357-48df-8ace-ecd48bb2e44a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 15,
    "yorig": 12
}