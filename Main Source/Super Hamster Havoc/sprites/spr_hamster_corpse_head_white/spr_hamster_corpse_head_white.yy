{
    "id": "7d16f160-a2ea-439e-a5a2-78086740eb24",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_corpse_head_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54db0f0e-8b57-4ce0-8f8d-6a374ec8cf0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d16f160-a2ea-439e-a5a2-78086740eb24",
            "compositeImage": {
                "id": "b057df8e-c69a-4b38-b6b2-8abe96e41480",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54db0f0e-8b57-4ce0-8f8d-6a374ec8cf0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4850a272-b146-45fb-98a2-35c7e1a00bd2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54db0f0e-8b57-4ce0-8f8d-6a374ec8cf0c",
                    "LayerId": "90c8bab4-e68b-4780-9a97-92bcd2d9c53f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "90c8bab4-e68b-4780-9a97-92bcd2d9c53f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d16f160-a2ea-439e-a5a2-78086740eb24",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 15,
    "yorig": 12
}