{
    "id": "bdbab6bd-7424-410e-ac51-f091ec798ab3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ps4_lt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 15,
    "bbox_right": 88,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60efafc3-e563-408a-9457-726fefda259f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdbab6bd-7424-410e-ac51-f091ec798ab3",
            "compositeImage": {
                "id": "dcbd72b1-29d0-4037-991f-d1bc95781af5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60efafc3-e563-408a-9457-726fefda259f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6e77250-5786-46e5-b3bf-3c5dd967ea01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60efafc3-e563-408a-9457-726fefda259f",
                    "LayerId": "993bc473-3f03-4dba-8d01-f5e9af0b4320"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "993bc473-3f03-4dba-8d01-f5e9af0b4320",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bdbab6bd-7424-410e-ac51-f091ec798ab3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}