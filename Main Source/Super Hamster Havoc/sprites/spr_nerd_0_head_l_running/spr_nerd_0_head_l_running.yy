{
    "id": "a9dde79b-b046-4769-808b-740c593a44da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nerd_0_head_l_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 15,
    "bbox_right": 48,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3fcffdf6-b0e2-4a4c-9d5d-c28bd10ccd51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9dde79b-b046-4769-808b-740c593a44da",
            "compositeImage": {
                "id": "c81016f1-63b4-4d71-926e-2d387d3a810f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fcffdf6-b0e2-4a4c-9d5d-c28bd10ccd51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d636c9c6-d7fd-4509-9b19-af7849006fbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fcffdf6-b0e2-4a4c-9d5d-c28bd10ccd51",
                    "LayerId": "bed4429a-9324-45da-b83a-8e295aead6c5"
                }
            ]
        },
        {
            "id": "db7effca-61f6-4e8f-8a3f-1355293a2114",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9dde79b-b046-4769-808b-740c593a44da",
            "compositeImage": {
                "id": "d396c8d2-f64b-444d-b1b8-1eb588021434",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db7effca-61f6-4e8f-8a3f-1355293a2114",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8357a5fe-ca08-45ec-b445-e945699d5a7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db7effca-61f6-4e8f-8a3f-1355293a2114",
                    "LayerId": "bed4429a-9324-45da-b83a-8e295aead6c5"
                }
            ]
        },
        {
            "id": "efb99048-1490-4a48-80ed-548645999bc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9dde79b-b046-4769-808b-740c593a44da",
            "compositeImage": {
                "id": "52ea64fd-c4ec-4b49-be8f-70f3571852af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efb99048-1490-4a48-80ed-548645999bc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d55a6bf3-2ee0-4f16-bb75-1e9e2794e247",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efb99048-1490-4a48-80ed-548645999bc7",
                    "LayerId": "bed4429a-9324-45da-b83a-8e295aead6c5"
                }
            ]
        },
        {
            "id": "650b1a40-b4f8-499d-a89f-ce6470044628",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9dde79b-b046-4769-808b-740c593a44da",
            "compositeImage": {
                "id": "0a9fdc93-fa69-4058-8186-ca976983d1aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "650b1a40-b4f8-499d-a89f-ce6470044628",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ee266a1-390c-4938-8b7f-326368bdfbdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "650b1a40-b4f8-499d-a89f-ce6470044628",
                    "LayerId": "bed4429a-9324-45da-b83a-8e295aead6c5"
                }
            ]
        },
        {
            "id": "ae8114ec-6408-4f69-a26f-8a06a062c869",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9dde79b-b046-4769-808b-740c593a44da",
            "compositeImage": {
                "id": "02763e17-5bce-4418-ab6b-83607dec77d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae8114ec-6408-4f69-a26f-8a06a062c869",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52274891-9821-4675-aa2b-1e07926f28bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae8114ec-6408-4f69-a26f-8a06a062c869",
                    "LayerId": "bed4429a-9324-45da-b83a-8e295aead6c5"
                }
            ]
        },
        {
            "id": "0b3764ac-5410-46b2-8af7-10925870dec7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9dde79b-b046-4769-808b-740c593a44da",
            "compositeImage": {
                "id": "85669e80-8506-4596-8b12-c2161e6094da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b3764ac-5410-46b2-8af7-10925870dec7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "978b3050-bb1c-409a-9fab-1057fcce10e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b3764ac-5410-46b2-8af7-10925870dec7",
                    "LayerId": "bed4429a-9324-45da-b83a-8e295aead6c5"
                }
            ]
        },
        {
            "id": "e2198497-ff3f-4a05-8607-3d3b648e32ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9dde79b-b046-4769-808b-740c593a44da",
            "compositeImage": {
                "id": "40761e0a-c66f-4010-9c0e-cdffe2ee4eea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2198497-ff3f-4a05-8607-3d3b648e32ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "929dc270-2921-45ee-bfcb-0bf0a6d84c3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2198497-ff3f-4a05-8607-3d3b648e32ce",
                    "LayerId": "bed4429a-9324-45da-b83a-8e295aead6c5"
                }
            ]
        },
        {
            "id": "553874ea-b58a-4aa7-a99e-0a11113fb835",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9dde79b-b046-4769-808b-740c593a44da",
            "compositeImage": {
                "id": "5abac3cd-980a-4e80-85d0-efd50a6b1068",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "553874ea-b58a-4aa7-a99e-0a11113fb835",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1317b46a-83b6-4d9f-a041-2e1094bd2151",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "553874ea-b58a-4aa7-a99e-0a11113fb835",
                    "LayerId": "bed4429a-9324-45da-b83a-8e295aead6c5"
                }
            ]
        },
        {
            "id": "7adf2b40-7238-4188-bee8-95f1b945a02d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9dde79b-b046-4769-808b-740c593a44da",
            "compositeImage": {
                "id": "40c2028e-be1d-40d8-8f6a-822d7c3a77da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7adf2b40-7238-4188-bee8-95f1b945a02d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa726fda-c30e-442e-9129-4267e40e9ba5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7adf2b40-7238-4188-bee8-95f1b945a02d",
                    "LayerId": "bed4429a-9324-45da-b83a-8e295aead6c5"
                }
            ]
        },
        {
            "id": "887a90e0-b7e5-45b8-80d9-e6d079492156",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9dde79b-b046-4769-808b-740c593a44da",
            "compositeImage": {
                "id": "f3490ea5-ccff-442b-9112-8d18d40ae5ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "887a90e0-b7e5-45b8-80d9-e6d079492156",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ca10def-6560-43b8-afa5-e5d63dd1e307",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "887a90e0-b7e5-45b8-80d9-e6d079492156",
                    "LayerId": "bed4429a-9324-45da-b83a-8e295aead6c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bed4429a-9324-45da-b83a-8e295aead6c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9dde79b-b046-4769-808b-740c593a44da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}