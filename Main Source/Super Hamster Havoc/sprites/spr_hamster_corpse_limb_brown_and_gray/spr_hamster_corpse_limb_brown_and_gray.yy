{
    "id": "baaba8e9-caa1-4b7d-864c-6e6453189f17",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_corpse_limb_brown_and_gray",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe1101dd-82fa-4703-ae50-3e27417668fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "baaba8e9-caa1-4b7d-864c-6e6453189f17",
            "compositeImage": {
                "id": "36cc0bdb-cbb7-4e5f-b04b-21741f2a625f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe1101dd-82fa-4703-ae50-3e27417668fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0cc7f0f-bb62-4d89-884a-1c9c2ae0d02f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe1101dd-82fa-4703-ae50-3e27417668fd",
                    "LayerId": "1ba62a0d-308f-45ae-8fa9-970b9680d22f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "1ba62a0d-308f-45ae-8fa9-970b9680d22f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "baaba8e9-caa1-4b7d-864c-6e6453189f17",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 5
}