{
    "id": "90dadd92-e31e-464c-b598-4c351e4afb17",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_1_torso_l_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 24,
    "bbox_right": 39,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aca5ed0a-d990-4c62-9e9d-7ed2346dfa1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90dadd92-e31e-464c-b598-4c351e4afb17",
            "compositeImage": {
                "id": "176b374f-991c-4000-bc39-202ae88e544e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aca5ed0a-d990-4c62-9e9d-7ed2346dfa1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77face9d-b5ff-426c-8090-a818134bb80b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aca5ed0a-d990-4c62-9e9d-7ed2346dfa1e",
                    "LayerId": "ea45484f-8072-4db9-b28d-22efd93c24d9"
                }
            ]
        },
        {
            "id": "d8666ad2-5153-454b-95b7-a5197b8fd47b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90dadd92-e31e-464c-b598-4c351e4afb17",
            "compositeImage": {
                "id": "43c44843-43b1-41f9-8270-731dd724e870",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8666ad2-5153-454b-95b7-a5197b8fd47b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24fcfd41-bcf5-41df-a933-2feb90c130e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8666ad2-5153-454b-95b7-a5197b8fd47b",
                    "LayerId": "ea45484f-8072-4db9-b28d-22efd93c24d9"
                }
            ]
        },
        {
            "id": "874ce6e0-ecb1-4520-8001-145cefcf2f6a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90dadd92-e31e-464c-b598-4c351e4afb17",
            "compositeImage": {
                "id": "254ae355-e659-4af6-a57a-4ed49a4cb5d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "874ce6e0-ecb1-4520-8001-145cefcf2f6a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d1264b4-b386-4067-a80d-e7da520dbf06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "874ce6e0-ecb1-4520-8001-145cefcf2f6a",
                    "LayerId": "ea45484f-8072-4db9-b28d-22efd93c24d9"
                }
            ]
        },
        {
            "id": "95aa905c-a174-4deb-8ee4-5b95fb8d518a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90dadd92-e31e-464c-b598-4c351e4afb17",
            "compositeImage": {
                "id": "fbbf422c-dce4-4772-92ad-bf73810fd5cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95aa905c-a174-4deb-8ee4-5b95fb8d518a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e43e9b1-4a36-4ffd-9039-97d5a32c618b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95aa905c-a174-4deb-8ee4-5b95fb8d518a",
                    "LayerId": "ea45484f-8072-4db9-b28d-22efd93c24d9"
                }
            ]
        },
        {
            "id": "9520dd26-6e4c-4805-a78e-d90cb2395c9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90dadd92-e31e-464c-b598-4c351e4afb17",
            "compositeImage": {
                "id": "9ff94df4-611a-4d82-8acb-05130ea69343",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9520dd26-6e4c-4805-a78e-d90cb2395c9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58354822-0ab8-481c-bad6-f22e2542dde5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9520dd26-6e4c-4805-a78e-d90cb2395c9b",
                    "LayerId": "ea45484f-8072-4db9-b28d-22efd93c24d9"
                }
            ]
        },
        {
            "id": "d7fa01c4-e8b1-4601-8853-5ad6a4ef3869",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90dadd92-e31e-464c-b598-4c351e4afb17",
            "compositeImage": {
                "id": "2a31cde3-b5ff-458e-9aa4-41c4efb05ae3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7fa01c4-e8b1-4601-8853-5ad6a4ef3869",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b81f5ef1-7144-4c0e-8146-a46a6b629f3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7fa01c4-e8b1-4601-8853-5ad6a4ef3869",
                    "LayerId": "ea45484f-8072-4db9-b28d-22efd93c24d9"
                }
            ]
        },
        {
            "id": "7cb9cf85-fbc6-4e32-824d-4245a68cd339",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90dadd92-e31e-464c-b598-4c351e4afb17",
            "compositeImage": {
                "id": "87e26c3a-df1c-40bc-bbe1-f647f78156c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cb9cf85-fbc6-4e32-824d-4245a68cd339",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9241513b-4f20-41cd-827f-b7f3310e7ce1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cb9cf85-fbc6-4e32-824d-4245a68cd339",
                    "LayerId": "ea45484f-8072-4db9-b28d-22efd93c24d9"
                }
            ]
        },
        {
            "id": "23394899-2817-46d1-9c7c-b30bf65c7ea5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90dadd92-e31e-464c-b598-4c351e4afb17",
            "compositeImage": {
                "id": "ff2388c0-918a-4a3b-8ffd-c2b2a1ea9da1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23394899-2817-46d1-9c7c-b30bf65c7ea5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f2201fd-421e-4169-845c-eb66bae83415",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23394899-2817-46d1-9c7c-b30bf65c7ea5",
                    "LayerId": "ea45484f-8072-4db9-b28d-22efd93c24d9"
                }
            ]
        },
        {
            "id": "9490e04a-45aa-4c6a-b188-ccce5039763e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90dadd92-e31e-464c-b598-4c351e4afb17",
            "compositeImage": {
                "id": "6be0046f-07ff-437d-9bb9-4b242ecc7548",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9490e04a-45aa-4c6a-b188-ccce5039763e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47499f97-13c7-48fa-aa5f-166bf2bcd0b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9490e04a-45aa-4c6a-b188-ccce5039763e",
                    "LayerId": "ea45484f-8072-4db9-b28d-22efd93c24d9"
                }
            ]
        },
        {
            "id": "e8950bf0-c496-43fc-95b5-88d9340864a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90dadd92-e31e-464c-b598-4c351e4afb17",
            "compositeImage": {
                "id": "59c50290-3f22-4d89-a31b-4985ed43ebbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8950bf0-c496-43fc-95b5-88d9340864a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e435ccc-b753-48be-99ae-bf64a79abddf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8950bf0-c496-43fc-95b5-88d9340864a4",
                    "LayerId": "ea45484f-8072-4db9-b28d-22efd93c24d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ea45484f-8072-4db9-b28d-22efd93c24d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90dadd92-e31e-464c-b598-4c351e4afb17",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}