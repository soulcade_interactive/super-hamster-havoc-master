{
    "id": "f90ad0b0-e64f-4bbb-ad5f-5c261e83acb9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_corpse_torso_brown_and_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b750696-a54a-4690-a3ec-f31c268eb5e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f90ad0b0-e64f-4bbb-ad5f-5c261e83acb9",
            "compositeImage": {
                "id": "8f295300-f607-4e31-9e58-d156e66cfdce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b750696-a54a-4690-a3ec-f31c268eb5e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86ad5c84-2c99-4a79-ad91-1a76ff2985b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b750696-a54a-4690-a3ec-f31c268eb5e0",
                    "LayerId": "54e80618-2dfb-4284-9ce2-a99050c75189"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "54e80618-2dfb-4284-9ce2-a99050c75189",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f90ad0b0-e64f-4bbb-ad5f-5c261e83acb9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 8
}