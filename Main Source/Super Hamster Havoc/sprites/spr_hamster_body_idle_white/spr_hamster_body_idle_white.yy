{
    "id": "2b2747e7-c114-4346-99c0-3b3116f5d68b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_body_idle_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 38,
    "bbox_left": 3,
    "bbox_right": 31,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3e34895-d146-40a1-a1c4-4cad7630ce82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b2747e7-c114-4346-99c0-3b3116f5d68b",
            "compositeImage": {
                "id": "9c7cb0a4-c61d-475b-bc87-146f85c46342",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3e34895-d146-40a1-a1c4-4cad7630ce82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "147b12de-4c32-4c3e-992f-fa8f229d47bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3e34895-d146-40a1-a1c4-4cad7630ce82",
                    "LayerId": "315f84db-2fd8-4ddb-aa37-a604397ea5d2"
                }
            ]
        },
        {
            "id": "fcd9ce3a-67f7-4387-b322-b76656e36ed8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b2747e7-c114-4346-99c0-3b3116f5d68b",
            "compositeImage": {
                "id": "065ce6ed-5189-42df-bfda-2ae6d3dd7b2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcd9ce3a-67f7-4387-b322-b76656e36ed8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c11252f1-935d-4251-a134-c779af3dcadd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcd9ce3a-67f7-4387-b322-b76656e36ed8",
                    "LayerId": "315f84db-2fd8-4ddb-aa37-a604397ea5d2"
                }
            ]
        },
        {
            "id": "4e143540-3757-4442-95e4-921941dfffe6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b2747e7-c114-4346-99c0-3b3116f5d68b",
            "compositeImage": {
                "id": "a45469ea-3154-4edb-b261-83f68096ba19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e143540-3757-4442-95e4-921941dfffe6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "394e88c7-43ff-4a80-846a-77b19f2d7a05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e143540-3757-4442-95e4-921941dfffe6",
                    "LayerId": "315f84db-2fd8-4ddb-aa37-a604397ea5d2"
                }
            ]
        },
        {
            "id": "016a92b4-f9f5-478a-a82c-ac79c3287b8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b2747e7-c114-4346-99c0-3b3116f5d68b",
            "compositeImage": {
                "id": "536fb360-64bd-4188-a820-bbd7e6c7999d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "016a92b4-f9f5-478a-a82c-ac79c3287b8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f391f630-15b0-4fbd-8d48-3f0b6c35b323",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "016a92b4-f9f5-478a-a82c-ac79c3287b8d",
                    "LayerId": "315f84db-2fd8-4ddb-aa37-a604397ea5d2"
                }
            ]
        },
        {
            "id": "2a976179-3d24-4ed6-986a-dd38ec57e2b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b2747e7-c114-4346-99c0-3b3116f5d68b",
            "compositeImage": {
                "id": "181c868f-33a5-466f-bb79-6f02a080b0f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a976179-3d24-4ed6-986a-dd38ec57e2b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63384a5c-4018-4545-8e67-f9ca2347d278",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a976179-3d24-4ed6-986a-dd38ec57e2b1",
                    "LayerId": "315f84db-2fd8-4ddb-aa37-a604397ea5d2"
                }
            ]
        },
        {
            "id": "c020941e-e2d8-48dc-acef-3775c5af77fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b2747e7-c114-4346-99c0-3b3116f5d68b",
            "compositeImage": {
                "id": "eb080afb-8e06-4ce6-b3f1-984d4b2d47ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c020941e-e2d8-48dc-acef-3775c5af77fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d74be011-c111-427d-9be8-023004632cb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c020941e-e2d8-48dc-acef-3775c5af77fc",
                    "LayerId": "315f84db-2fd8-4ddb-aa37-a604397ea5d2"
                }
            ]
        },
        {
            "id": "0f7e9369-50c5-4d3a-9ad0-52c474d7ad7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b2747e7-c114-4346-99c0-3b3116f5d68b",
            "compositeImage": {
                "id": "0f55fb0e-80ff-4cae-aa1c-9fde66e0e9b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f7e9369-50c5-4d3a-9ad0-52c474d7ad7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69ed2241-22ba-4b2e-8adf-52da526d7572",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f7e9369-50c5-4d3a-9ad0-52c474d7ad7d",
                    "LayerId": "315f84db-2fd8-4ddb-aa37-a604397ea5d2"
                }
            ]
        },
        {
            "id": "f22a43f4-5e8c-44ab-85ad-06260635a839",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2b2747e7-c114-4346-99c0-3b3116f5d68b",
            "compositeImage": {
                "id": "8dc416ad-a0d3-4def-b060-0500cf2782d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f22a43f4-5e8c-44ab-85ad-06260635a839",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5208491-2837-4764-9568-25e054cf489a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f22a43f4-5e8c-44ab-85ad-06260635a839",
                    "LayerId": "315f84db-2fd8-4ddb-aa37-a604397ea5d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 39,
    "layers": [
        {
            "id": "315f84db-2fd8-4ddb-aa37-a604397ea5d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2b2747e7-c114-4346-99c0-3b3116f5d68b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 19
}