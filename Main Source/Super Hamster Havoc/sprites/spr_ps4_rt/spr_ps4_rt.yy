{
    "id": "6e64a4ed-05f3-4375-9539-b3d2d9ae447b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ps4_rt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 15,
    "bbox_right": 88,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7a76866-a18a-4410-919a-4272efd55f8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e64a4ed-05f3-4375-9539-b3d2d9ae447b",
            "compositeImage": {
                "id": "00f8a1cf-6d18-497c-8680-8b709fc966a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7a76866-a18a-4410-919a-4272efd55f8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fceb414-f55b-4c7e-b77d-2f923454a743",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7a76866-a18a-4410-919a-4272efd55f8d",
                    "LayerId": "d3995186-fdf7-4240-bd3c-c3b6a3299d83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "d3995186-fdf7-4240-bd3c-c3b6a3299d83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e64a4ed-05f3-4375-9539-b3d2d9ae447b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}