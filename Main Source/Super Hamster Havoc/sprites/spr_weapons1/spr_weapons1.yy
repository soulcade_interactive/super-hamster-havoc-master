{
    "id": "3297f626-4997-40c2-98b0-f09c94d06295",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_weapons1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e0bba7e-b2a6-461f-af43-c1ad8a3285b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3297f626-4997-40c2-98b0-f09c94d06295",
            "compositeImage": {
                "id": "39821346-947c-4c72-8153-eb75dc395e22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e0bba7e-b2a6-461f-af43-c1ad8a3285b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1ed9065-5d41-41eb-aa73-461310be9950",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e0bba7e-b2a6-461f-af43-c1ad8a3285b5",
                    "LayerId": "8ef477c0-f3e0-44b0-a2be-bff20813f2e0"
                }
            ]
        },
        {
            "id": "787225b5-456e-4c97-aa22-945b37cd6f27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3297f626-4997-40c2-98b0-f09c94d06295",
            "compositeImage": {
                "id": "b9c9f1fd-9700-450e-b8f3-dd6895b28d75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "787225b5-456e-4c97-aa22-945b37cd6f27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18040e87-6da6-404c-9e4b-e66c09c2ebd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "787225b5-456e-4c97-aa22-945b37cd6f27",
                    "LayerId": "8ef477c0-f3e0-44b0-a2be-bff20813f2e0"
                }
            ]
        },
        {
            "id": "c9fce9a4-3679-475d-a196-e520e93561bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3297f626-4997-40c2-98b0-f09c94d06295",
            "compositeImage": {
                "id": "03a8e63c-5206-458b-9eed-7c80495eb1e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9fce9a4-3679-475d-a196-e520e93561bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "146869d8-b968-4374-a984-9337979d2ab6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9fce9a4-3679-475d-a196-e520e93561bc",
                    "LayerId": "8ef477c0-f3e0-44b0-a2be-bff20813f2e0"
                }
            ]
        },
        {
            "id": "10460edc-3cfc-4942-8e40-979cb568e768",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3297f626-4997-40c2-98b0-f09c94d06295",
            "compositeImage": {
                "id": "cfaee369-0970-43e2-927d-16f5ef45f962",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10460edc-3cfc-4942-8e40-979cb568e768",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3bf2833-dc29-4fb4-a1ed-3ff60ec50059",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10460edc-3cfc-4942-8e40-979cb568e768",
                    "LayerId": "8ef477c0-f3e0-44b0-a2be-bff20813f2e0"
                }
            ]
        },
        {
            "id": "b0ffe300-c641-4546-9566-1c4eb352712b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3297f626-4997-40c2-98b0-f09c94d06295",
            "compositeImage": {
                "id": "52852b95-74be-434f-abd1-6547eaced6b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0ffe300-c641-4546-9566-1c4eb352712b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0c48867-0394-4031-91ea-406f803aef71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0ffe300-c641-4546-9566-1c4eb352712b",
                    "LayerId": "8ef477c0-f3e0-44b0-a2be-bff20813f2e0"
                }
            ]
        },
        {
            "id": "3c28474e-c102-4956-8a20-22ee0d93fdd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3297f626-4997-40c2-98b0-f09c94d06295",
            "compositeImage": {
                "id": "d07c86cc-ed8b-40d6-b139-82e5399cc2ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c28474e-c102-4956-8a20-22ee0d93fdd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bb9fd31-41e9-42d3-b8bb-b4b61695f97e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c28474e-c102-4956-8a20-22ee0d93fdd8",
                    "LayerId": "8ef477c0-f3e0-44b0-a2be-bff20813f2e0"
                }
            ]
        },
        {
            "id": "d58aabb3-549a-4652-91a4-cd89057fc692",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3297f626-4997-40c2-98b0-f09c94d06295",
            "compositeImage": {
                "id": "f9060623-fb9d-45a9-a958-62a981586117",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d58aabb3-549a-4652-91a4-cd89057fc692",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d85e7730-9f3b-48bb-960b-3a1899e20008",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d58aabb3-549a-4652-91a4-cd89057fc692",
                    "LayerId": "8ef477c0-f3e0-44b0-a2be-bff20813f2e0"
                }
            ]
        },
        {
            "id": "5dd733af-1a05-455c-a73d-67821396ccaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3297f626-4997-40c2-98b0-f09c94d06295",
            "compositeImage": {
                "id": "a42de723-9af5-4b1c-a5fc-f60a78077967",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dd733af-1a05-455c-a73d-67821396ccaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be12621b-4c2c-4934-af98-601623b8b17b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dd733af-1a05-455c-a73d-67821396ccaa",
                    "LayerId": "8ef477c0-f3e0-44b0-a2be-bff20813f2e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8ef477c0-f3e0-44b0-a2be-bff20813f2e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3297f626-4997-40c2-98b0-f09c94d06295",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}