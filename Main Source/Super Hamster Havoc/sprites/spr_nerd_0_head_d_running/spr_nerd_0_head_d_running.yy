{
    "id": "8dee49b4-77dd-41c8-8d40-84a50a1b14f5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nerd_0_head_d_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 18,
    "bbox_right": 45,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "06cc0813-ad16-4b55-a9a2-ba9dad57c076",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dee49b4-77dd-41c8-8d40-84a50a1b14f5",
            "compositeImage": {
                "id": "530e1fe8-6017-46ef-816a-4a921d83e15b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06cc0813-ad16-4b55-a9a2-ba9dad57c076",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f2e7ed4-bae5-46ff-bc1c-304e9ebdf6d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06cc0813-ad16-4b55-a9a2-ba9dad57c076",
                    "LayerId": "ee2593a2-e024-47c2-98c4-82f7c4132bb4"
                }
            ]
        },
        {
            "id": "f69cdfe1-d23d-4f11-9a57-dd66adff1335",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dee49b4-77dd-41c8-8d40-84a50a1b14f5",
            "compositeImage": {
                "id": "4f84f36d-9d7d-4698-9dd9-46829a225f84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f69cdfe1-d23d-4f11-9a57-dd66adff1335",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26133ceb-615e-4688-b094-02c3d4606281",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f69cdfe1-d23d-4f11-9a57-dd66adff1335",
                    "LayerId": "ee2593a2-e024-47c2-98c4-82f7c4132bb4"
                }
            ]
        },
        {
            "id": "49f6a2cb-e071-4cfb-afd2-d2ac594a913a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dee49b4-77dd-41c8-8d40-84a50a1b14f5",
            "compositeImage": {
                "id": "a2d48052-94dd-40d0-b00c-9c843e0f3960",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49f6a2cb-e071-4cfb-afd2-d2ac594a913a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e580f538-f853-4f99-b0e8-beebf4b23d07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49f6a2cb-e071-4cfb-afd2-d2ac594a913a",
                    "LayerId": "ee2593a2-e024-47c2-98c4-82f7c4132bb4"
                }
            ]
        },
        {
            "id": "394254ba-2824-475d-a8f0-0f2d9ad34a6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dee49b4-77dd-41c8-8d40-84a50a1b14f5",
            "compositeImage": {
                "id": "3be43fe4-b821-4446-a03d-6eb9edd9f268",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "394254ba-2824-475d-a8f0-0f2d9ad34a6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41e1232e-aa38-45b9-bc25-b30462cbe0f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "394254ba-2824-475d-a8f0-0f2d9ad34a6d",
                    "LayerId": "ee2593a2-e024-47c2-98c4-82f7c4132bb4"
                }
            ]
        },
        {
            "id": "1f99c83d-a92b-444f-9620-4fae57958833",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dee49b4-77dd-41c8-8d40-84a50a1b14f5",
            "compositeImage": {
                "id": "e82f5df4-9c2e-47af-a38d-1aee68f5e7fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f99c83d-a92b-444f-9620-4fae57958833",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d04c54b-0397-42fd-a651-789e2cf35120",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f99c83d-a92b-444f-9620-4fae57958833",
                    "LayerId": "ee2593a2-e024-47c2-98c4-82f7c4132bb4"
                }
            ]
        },
        {
            "id": "dfa17d98-1555-4be8-8cf3-03990d9fc324",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dee49b4-77dd-41c8-8d40-84a50a1b14f5",
            "compositeImage": {
                "id": "09873b94-c2c4-48d7-b4ad-789849849d96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfa17d98-1555-4be8-8cf3-03990d9fc324",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79eb801a-9374-4a92-abb8-f8669696908f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfa17d98-1555-4be8-8cf3-03990d9fc324",
                    "LayerId": "ee2593a2-e024-47c2-98c4-82f7c4132bb4"
                }
            ]
        },
        {
            "id": "20b54f52-d443-47a7-813c-dd502adbc255",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dee49b4-77dd-41c8-8d40-84a50a1b14f5",
            "compositeImage": {
                "id": "faafcaaf-48cb-4112-abc7-8905fe712e38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20b54f52-d443-47a7-813c-dd502adbc255",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4149667b-ff18-4482-bf8b-d23e6328f341",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20b54f52-d443-47a7-813c-dd502adbc255",
                    "LayerId": "ee2593a2-e024-47c2-98c4-82f7c4132bb4"
                }
            ]
        },
        {
            "id": "8092af3f-91cf-4717-85ac-d2eb4ddf9a71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dee49b4-77dd-41c8-8d40-84a50a1b14f5",
            "compositeImage": {
                "id": "56d70e78-f666-4898-ad2a-b150dda2bd40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8092af3f-91cf-4717-85ac-d2eb4ddf9a71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97dcabc0-79f3-4109-b785-4dcd75330cab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8092af3f-91cf-4717-85ac-d2eb4ddf9a71",
                    "LayerId": "ee2593a2-e024-47c2-98c4-82f7c4132bb4"
                }
            ]
        },
        {
            "id": "8c71e0e0-03c5-4e81-a7f9-67660ab45cd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dee49b4-77dd-41c8-8d40-84a50a1b14f5",
            "compositeImage": {
                "id": "fd9e8636-9fae-4a87-b18f-8e18da512a20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c71e0e0-03c5-4e81-a7f9-67660ab45cd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b21362e1-2ad7-4e69-b503-6324e45c3b78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c71e0e0-03c5-4e81-a7f9-67660ab45cd8",
                    "LayerId": "ee2593a2-e024-47c2-98c4-82f7c4132bb4"
                }
            ]
        },
        {
            "id": "1a68cc4e-1921-48a9-a2b3-5f77f5b79938",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8dee49b4-77dd-41c8-8d40-84a50a1b14f5",
            "compositeImage": {
                "id": "9e5db011-92a9-4128-b1f1-769ee28383bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a68cc4e-1921-48a9-a2b3-5f77f5b79938",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9a9bcd2-a594-4de3-b338-cf8ad054e773",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a68cc4e-1921-48a9-a2b3-5f77f5b79938",
                    "LayerId": "ee2593a2-e024-47c2-98c4-82f7c4132bb4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ee2593a2-e024-47c2-98c4-82f7c4132bb4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8dee49b4-77dd-41c8-8d40-84a50a1b14f5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}