{
    "id": "09b7f421-e82d-4640-a4d1-6754632f196a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buff_0_legs_l_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 17,
    "bbox_right": 42,
    "bbox_top": 43,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3f69d6ce-e71c-4fbd-8529-659eefe4be06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09b7f421-e82d-4640-a4d1-6754632f196a",
            "compositeImage": {
                "id": "9b71f1dc-835a-41ca-9874-a75b286a892b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f69d6ce-e71c-4fbd-8529-659eefe4be06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9687edcb-f82b-4d27-a8be-f15d3510f0e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f69d6ce-e71c-4fbd-8529-659eefe4be06",
                    "LayerId": "b92c5f36-655e-4087-ba5e-79c16ddb5b29"
                }
            ]
        },
        {
            "id": "5456cda6-fb67-4e6c-91c8-308a494d7f8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09b7f421-e82d-4640-a4d1-6754632f196a",
            "compositeImage": {
                "id": "3faf448b-9a24-4e22-b357-9a23e8b120dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5456cda6-fb67-4e6c-91c8-308a494d7f8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94982626-41d2-4118-8fca-e1d25303ed4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5456cda6-fb67-4e6c-91c8-308a494d7f8e",
                    "LayerId": "b92c5f36-655e-4087-ba5e-79c16ddb5b29"
                }
            ]
        },
        {
            "id": "8da4cc05-e564-4fa0-a4ed-6483ecd65085",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09b7f421-e82d-4640-a4d1-6754632f196a",
            "compositeImage": {
                "id": "e2a9739d-d109-4811-a4a7-b2c81b8f4487",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8da4cc05-e564-4fa0-a4ed-6483ecd65085",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d93e577-d835-41c4-beb1-9bda6b47f280",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8da4cc05-e564-4fa0-a4ed-6483ecd65085",
                    "LayerId": "b92c5f36-655e-4087-ba5e-79c16ddb5b29"
                }
            ]
        },
        {
            "id": "39803248-2374-4e7a-b661-2fac88923fdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09b7f421-e82d-4640-a4d1-6754632f196a",
            "compositeImage": {
                "id": "a0a75922-6146-4363-9930-b28838746aea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39803248-2374-4e7a-b661-2fac88923fdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae7832da-f276-41f1-a9f3-24abc546eccb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39803248-2374-4e7a-b661-2fac88923fdb",
                    "LayerId": "b92c5f36-655e-4087-ba5e-79c16ddb5b29"
                }
            ]
        },
        {
            "id": "f5344066-770a-4295-a795-efe7fb7c0a72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09b7f421-e82d-4640-a4d1-6754632f196a",
            "compositeImage": {
                "id": "f1a50198-722e-4300-8e32-c32ac3d04abc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5344066-770a-4295-a795-efe7fb7c0a72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99af28b4-7364-4672-aa49-0534bbcd0b8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5344066-770a-4295-a795-efe7fb7c0a72",
                    "LayerId": "b92c5f36-655e-4087-ba5e-79c16ddb5b29"
                }
            ]
        },
        {
            "id": "1ed55f09-c799-4255-a552-307fa4a44bff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09b7f421-e82d-4640-a4d1-6754632f196a",
            "compositeImage": {
                "id": "9770c294-e9d6-4d79-99ce-b89607136f20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ed55f09-c799-4255-a552-307fa4a44bff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "400350a5-4872-4b27-aa42-c02f39542363",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ed55f09-c799-4255-a552-307fa4a44bff",
                    "LayerId": "b92c5f36-655e-4087-ba5e-79c16ddb5b29"
                }
            ]
        },
        {
            "id": "0a8f3550-90a9-419f-9982-f039613cca51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09b7f421-e82d-4640-a4d1-6754632f196a",
            "compositeImage": {
                "id": "2c34af31-2454-4659-a69a-8239a529b3a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a8f3550-90a9-419f-9982-f039613cca51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "effaa864-1a41-41a0-b9ee-e8d56cf7ab38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a8f3550-90a9-419f-9982-f039613cca51",
                    "LayerId": "b92c5f36-655e-4087-ba5e-79c16ddb5b29"
                }
            ]
        },
        {
            "id": "ddfb3f7d-389d-4714-9a56-6cf4ecdff308",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09b7f421-e82d-4640-a4d1-6754632f196a",
            "compositeImage": {
                "id": "61c39769-1f66-4028-95f9-69ced206e20f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddfb3f7d-389d-4714-9a56-6cf4ecdff308",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fec601a-a902-47b4-a29b-badc68f2bd88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddfb3f7d-389d-4714-9a56-6cf4ecdff308",
                    "LayerId": "b92c5f36-655e-4087-ba5e-79c16ddb5b29"
                }
            ]
        },
        {
            "id": "8cb30bb2-c91c-41a7-b918-3ade9bb2c2d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09b7f421-e82d-4640-a4d1-6754632f196a",
            "compositeImage": {
                "id": "2389f90c-0ca3-4dfc-8e8f-5bfa3f1756f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cb30bb2-c91c-41a7-b918-3ade9bb2c2d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8879d34b-5254-4668-a2d9-9c13cc6b76be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cb30bb2-c91c-41a7-b918-3ade9bb2c2d9",
                    "LayerId": "b92c5f36-655e-4087-ba5e-79c16ddb5b29"
                }
            ]
        },
        {
            "id": "720f2e4d-526b-4307-b4a3-0d0842ce9a8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09b7f421-e82d-4640-a4d1-6754632f196a",
            "compositeImage": {
                "id": "f7bb0820-7c08-4ecb-9c4c-135b9aaff179",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "720f2e4d-526b-4307-b4a3-0d0842ce9a8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "811c7280-10ff-4d24-91c1-ace7a28fbc05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "720f2e4d-526b-4307-b4a3-0d0842ce9a8c",
                    "LayerId": "b92c5f36-655e-4087-ba5e-79c16ddb5b29"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b92c5f36-655e-4087-ba5e-79c16ddb5b29",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09b7f421-e82d-4640-a4d1-6754632f196a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 25,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}