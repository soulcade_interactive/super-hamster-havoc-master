{
    "id": "a16aad48-bf54-4d18-9167-498e7402ddd9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hamster_corspe_head_brown_and_gray",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ad0d7aa-4b16-4c8e-b013-8c33ab074f79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a16aad48-bf54-4d18-9167-498e7402ddd9",
            "compositeImage": {
                "id": "840e10c6-22ec-4944-8b40-4efebf7d0784",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ad0d7aa-4b16-4c8e-b013-8c33ab074f79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9287dd03-8277-4a8a-a9a3-e4493bfe23d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ad0d7aa-4b16-4c8e-b013-8c33ab074f79",
                    "LayerId": "96d6df7e-8798-49e3-95e6-6ac6827d31f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "96d6df7e-8798-49e3-95e6-6ac6827d31f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a16aad48-bf54-4d18-9167-498e7402ddd9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 15,
    "yorig": 12
}