{
    "id": "aca017ad-6465-48d7-bb85-191e9281f0fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guy_1_legs_u_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 25,
    "bbox_right": 38,
    "bbox_top": 56,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b6b2eb5d-dfe9-46cf-92a5-d917c6d636ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca017ad-6465-48d7-bb85-191e9281f0fa",
            "compositeImage": {
                "id": "e7b332e9-8a6f-4bc9-af14-1248e30472ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6b2eb5d-dfe9-46cf-92a5-d917c6d636ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "834878ae-45f6-41f7-a749-f2469b53858d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6b2eb5d-dfe9-46cf-92a5-d917c6d636ae",
                    "LayerId": "f55cfdaf-4038-4892-b94f-bb43f90c3cd7"
                }
            ]
        },
        {
            "id": "e71d76aa-1e3d-490c-a42a-0e05951fc913",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca017ad-6465-48d7-bb85-191e9281f0fa",
            "compositeImage": {
                "id": "5953afbd-31be-4a07-8b14-40d16d09b7d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e71d76aa-1e3d-490c-a42a-0e05951fc913",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f265363-9af4-4833-880a-ee18d6e8afa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e71d76aa-1e3d-490c-a42a-0e05951fc913",
                    "LayerId": "f55cfdaf-4038-4892-b94f-bb43f90c3cd7"
                }
            ]
        },
        {
            "id": "d8e90c3a-6bad-4580-bfba-6ac97d0e2e59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca017ad-6465-48d7-bb85-191e9281f0fa",
            "compositeImage": {
                "id": "738ab1e2-a112-4bf7-b277-dd19c764a35b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8e90c3a-6bad-4580-bfba-6ac97d0e2e59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fe4d6ce-7e8c-4a85-8524-2556fa2b7f80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8e90c3a-6bad-4580-bfba-6ac97d0e2e59",
                    "LayerId": "f55cfdaf-4038-4892-b94f-bb43f90c3cd7"
                }
            ]
        },
        {
            "id": "a7549874-b1af-42d6-bdf7-3bfe786ea158",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca017ad-6465-48d7-bb85-191e9281f0fa",
            "compositeImage": {
                "id": "0963320f-8106-4acc-aee6-9c0982e05a92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7549874-b1af-42d6-bdf7-3bfe786ea158",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb00d60c-871e-4873-8c48-a4ee3f1ca53e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7549874-b1af-42d6-bdf7-3bfe786ea158",
                    "LayerId": "f55cfdaf-4038-4892-b94f-bb43f90c3cd7"
                }
            ]
        },
        {
            "id": "cb7ab510-b87a-443d-bf05-562183b77063",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca017ad-6465-48d7-bb85-191e9281f0fa",
            "compositeImage": {
                "id": "93a7bd9e-545e-45be-a947-b7e311ae880e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb7ab510-b87a-443d-bf05-562183b77063",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d8680f2-9acd-458d-b66a-4180e814bb3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb7ab510-b87a-443d-bf05-562183b77063",
                    "LayerId": "f55cfdaf-4038-4892-b94f-bb43f90c3cd7"
                }
            ]
        },
        {
            "id": "9f179a69-560e-4fa0-ba22-fd40fbddc62a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca017ad-6465-48d7-bb85-191e9281f0fa",
            "compositeImage": {
                "id": "9eccb911-9946-4eeb-b3a0-98e1b843680f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f179a69-560e-4fa0-ba22-fd40fbddc62a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3016bdb4-6c49-4a42-9300-2284104ada0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f179a69-560e-4fa0-ba22-fd40fbddc62a",
                    "LayerId": "f55cfdaf-4038-4892-b94f-bb43f90c3cd7"
                }
            ]
        },
        {
            "id": "cd68df7e-9995-4d0f-b6dc-3df46086d169",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca017ad-6465-48d7-bb85-191e9281f0fa",
            "compositeImage": {
                "id": "0e9497d6-4e59-440d-b771-b9125a411d71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd68df7e-9995-4d0f-b6dc-3df46086d169",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90b0d751-0ee2-48fa-9b0a-044a63cd9803",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd68df7e-9995-4d0f-b6dc-3df46086d169",
                    "LayerId": "f55cfdaf-4038-4892-b94f-bb43f90c3cd7"
                }
            ]
        },
        {
            "id": "e7bf4321-a86f-4910-bd34-9fefb436408c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca017ad-6465-48d7-bb85-191e9281f0fa",
            "compositeImage": {
                "id": "e13f4d1b-c6cd-4146-8200-e6d8d2221929",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7bf4321-a86f-4910-bd34-9fefb436408c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c37b696b-000c-4906-bc46-0812ddc1d0a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7bf4321-a86f-4910-bd34-9fefb436408c",
                    "LayerId": "f55cfdaf-4038-4892-b94f-bb43f90c3cd7"
                }
            ]
        },
        {
            "id": "9f43d7ad-fc1c-4554-bd61-bcf24c036e5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca017ad-6465-48d7-bb85-191e9281f0fa",
            "compositeImage": {
                "id": "7556e8e1-9da8-4ea4-ada7-b3faf140c818",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f43d7ad-fc1c-4554-bd61-bcf24c036e5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95c9a11c-bee8-49bd-8cea-efe4d7ad26b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f43d7ad-fc1c-4554-bd61-bcf24c036e5a",
                    "LayerId": "f55cfdaf-4038-4892-b94f-bb43f90c3cd7"
                }
            ]
        },
        {
            "id": "56320b2b-17a4-491d-97a4-a251fd850fce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aca017ad-6465-48d7-bb85-191e9281f0fa",
            "compositeImage": {
                "id": "15677de1-c279-4e7a-b3b2-57f065f6e66a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56320b2b-17a4-491d-97a4-a251fd850fce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5555ee8f-5a79-400f-8d5a-6a6d310b4fee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56320b2b-17a4-491d-97a4-a251fd850fce",
                    "LayerId": "f55cfdaf-4038-4892-b94f-bb43f90c3cd7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f55cfdaf-4038-4892-b94f-bb43f90c3cd7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aca017ad-6465-48d7-bb85-191e9281f0fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}