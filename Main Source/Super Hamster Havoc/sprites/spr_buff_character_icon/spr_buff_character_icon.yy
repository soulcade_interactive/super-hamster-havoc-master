{
    "id": "77001419-a456-4d0c-b4a5-fd04c3ba0b96",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_buff_character_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0e6f5bc-d088-4be0-8767-2640ae68992e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77001419-a456-4d0c-b4a5-fd04c3ba0b96",
            "compositeImage": {
                "id": "42c10b88-551d-413d-b5de-b5d86e8d70e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0e6f5bc-d088-4be0-8767-2640ae68992e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad1532bf-caef-4c87-8949-38a622809b63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0e6f5bc-d088-4be0-8767-2640ae68992e",
                    "LayerId": "58114112-75a8-42be-b9bf-05a78c443116"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "58114112-75a8-42be-b9bf-05a78c443116",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77001419-a456-4d0c-b4a5-fd04c3ba0b96",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}