{
    "id": "a76c0ec6-1f2a-48c5-aea0-5cc49bc63722",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nerd_0_legs_u_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 25,
    "bbox_right": 38,
    "bbox_top": 56,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "02bbfb5f-69eb-4b4e-bf15-e5d596e90e74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a76c0ec6-1f2a-48c5-aea0-5cc49bc63722",
            "compositeImage": {
                "id": "afaad50a-211a-478c-87c7-b3af930ddf48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02bbfb5f-69eb-4b4e-bf15-e5d596e90e74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe040fe6-dd8c-4d13-8ed8-27339b1efc0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02bbfb5f-69eb-4b4e-bf15-e5d596e90e74",
                    "LayerId": "a1f2a24e-2dda-455e-8415-d5ef20617f86"
                }
            ]
        },
        {
            "id": "76a51037-35fa-4fd5-8dcc-1aa728055546",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a76c0ec6-1f2a-48c5-aea0-5cc49bc63722",
            "compositeImage": {
                "id": "0a79eb38-d677-4b00-ae18-13d38dfa7eed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76a51037-35fa-4fd5-8dcc-1aa728055546",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ea53dc3-6a84-47ae-af53-6e668a201c17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76a51037-35fa-4fd5-8dcc-1aa728055546",
                    "LayerId": "a1f2a24e-2dda-455e-8415-d5ef20617f86"
                }
            ]
        },
        {
            "id": "5b52128e-ae82-44c8-9703-0a8affd07724",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a76c0ec6-1f2a-48c5-aea0-5cc49bc63722",
            "compositeImage": {
                "id": "0e88e5a3-6503-4c4d-8504-c33dde4a7211",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b52128e-ae82-44c8-9703-0a8affd07724",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d281b5b0-c261-4032-ada7-81efa7def9d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b52128e-ae82-44c8-9703-0a8affd07724",
                    "LayerId": "a1f2a24e-2dda-455e-8415-d5ef20617f86"
                }
            ]
        },
        {
            "id": "8a59c637-66ac-42dd-9adc-d2bfb9a73510",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a76c0ec6-1f2a-48c5-aea0-5cc49bc63722",
            "compositeImage": {
                "id": "6eee9116-a99d-4833-90a3-d7d228584667",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a59c637-66ac-42dd-9adc-d2bfb9a73510",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a487b051-f9db-40b2-85ea-80661e7a0462",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a59c637-66ac-42dd-9adc-d2bfb9a73510",
                    "LayerId": "a1f2a24e-2dda-455e-8415-d5ef20617f86"
                }
            ]
        },
        {
            "id": "adf6c95a-ed21-44f0-b289-be1e4ce4fbdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a76c0ec6-1f2a-48c5-aea0-5cc49bc63722",
            "compositeImage": {
                "id": "1cd9272b-c475-40c1-bb7e-90c2c27f8b30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adf6c95a-ed21-44f0-b289-be1e4ce4fbdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3763cad9-53c7-4df8-89ad-2b2c12cd98a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adf6c95a-ed21-44f0-b289-be1e4ce4fbdd",
                    "LayerId": "a1f2a24e-2dda-455e-8415-d5ef20617f86"
                }
            ]
        },
        {
            "id": "f8026c30-2782-4b20-a6b4-d02df793b038",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a76c0ec6-1f2a-48c5-aea0-5cc49bc63722",
            "compositeImage": {
                "id": "d4bab3b7-58df-4a27-b672-0ec42bc5821b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8026c30-2782-4b20-a6b4-d02df793b038",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "038022a3-0713-49d4-9be3-364424fc14e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8026c30-2782-4b20-a6b4-d02df793b038",
                    "LayerId": "a1f2a24e-2dda-455e-8415-d5ef20617f86"
                }
            ]
        },
        {
            "id": "1db39252-16d8-415c-9dc0-7f876fd91f4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a76c0ec6-1f2a-48c5-aea0-5cc49bc63722",
            "compositeImage": {
                "id": "3d9b2f65-f6bd-40a3-a587-a4c7ad54cad7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1db39252-16d8-415c-9dc0-7f876fd91f4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77b6d9d8-3045-4897-af47-04a2710fda0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1db39252-16d8-415c-9dc0-7f876fd91f4b",
                    "LayerId": "a1f2a24e-2dda-455e-8415-d5ef20617f86"
                }
            ]
        },
        {
            "id": "012cc535-f0e3-4418-ad3d-25c4d4f359fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a76c0ec6-1f2a-48c5-aea0-5cc49bc63722",
            "compositeImage": {
                "id": "d9951eab-e5d8-4fea-b0a6-9faf48381db0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "012cc535-f0e3-4418-ad3d-25c4d4f359fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "586d4e9a-7936-4e72-b192-16caa27d482d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "012cc535-f0e3-4418-ad3d-25c4d4f359fa",
                    "LayerId": "a1f2a24e-2dda-455e-8415-d5ef20617f86"
                }
            ]
        },
        {
            "id": "dea7175b-7eb0-4aff-8bb4-b2c116435bb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a76c0ec6-1f2a-48c5-aea0-5cc49bc63722",
            "compositeImage": {
                "id": "3c30a5c4-8a24-47fc-99eb-f10a8338dff4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dea7175b-7eb0-4aff-8bb4-b2c116435bb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63dec6ee-ea36-4ed3-bd16-45fadc243899",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dea7175b-7eb0-4aff-8bb4-b2c116435bb9",
                    "LayerId": "a1f2a24e-2dda-455e-8415-d5ef20617f86"
                }
            ]
        },
        {
            "id": "aa82ceb0-3762-453c-8e20-42527225fbcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a76c0ec6-1f2a-48c5-aea0-5cc49bc63722",
            "compositeImage": {
                "id": "2705bdf5-1ed4-47ea-9610-bf469cc6c0e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa82ceb0-3762-453c-8e20-42527225fbcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "589d9421-92cc-49b6-9273-e55b45337927",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa82ceb0-3762-453c-8e20-42527225fbcd",
                    "LayerId": "a1f2a24e-2dda-455e-8415-d5ef20617f86"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a1f2a24e-2dda-455e-8415-d5ef20617f86",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a76c0ec6-1f2a-48c5-aea0-5cc49bc63722",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}