{
    "id": "888e7ab2-eec3-4e17-b539-5b28fd33e349",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_nerd_0_torso_u_running",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 23,
    "bbox_right": 40,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2d8cde7b-5518-46a9-be0f-b4f89548c797",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "888e7ab2-eec3-4e17-b539-5b28fd33e349",
            "compositeImage": {
                "id": "1507ce3d-c3a0-4106-b782-ffba34b6ae4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d8cde7b-5518-46a9-be0f-b4f89548c797",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ade9eab-2d35-4f3a-82f5-e92bf94eb692",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d8cde7b-5518-46a9-be0f-b4f89548c797",
                    "LayerId": "ab975a19-db9a-41ca-8cc4-aa93ac90ee14"
                }
            ]
        },
        {
            "id": "5478d66a-b280-46ff-ba40-f962eacc83ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "888e7ab2-eec3-4e17-b539-5b28fd33e349",
            "compositeImage": {
                "id": "7f2a60c8-b876-48cf-b922-d034c6f4fb02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5478d66a-b280-46ff-ba40-f962eacc83ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fcc5399-be00-47b4-a415-b4cf486586f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5478d66a-b280-46ff-ba40-f962eacc83ed",
                    "LayerId": "ab975a19-db9a-41ca-8cc4-aa93ac90ee14"
                }
            ]
        },
        {
            "id": "98b8afe0-bbf3-4166-ae53-3822d07e7940",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "888e7ab2-eec3-4e17-b539-5b28fd33e349",
            "compositeImage": {
                "id": "87b1e39b-fd42-4d4c-91b6-4c4d3287e26b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98b8afe0-bbf3-4166-ae53-3822d07e7940",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bc053ef-456d-4455-a274-faa6f5dc03cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98b8afe0-bbf3-4166-ae53-3822d07e7940",
                    "LayerId": "ab975a19-db9a-41ca-8cc4-aa93ac90ee14"
                }
            ]
        },
        {
            "id": "32358770-67b8-48fe-879e-615da2cc3493",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "888e7ab2-eec3-4e17-b539-5b28fd33e349",
            "compositeImage": {
                "id": "19a14b36-df68-4513-90db-1d82f86218ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32358770-67b8-48fe-879e-615da2cc3493",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9afa6619-75a9-41b1-b5bb-d33684d3814d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32358770-67b8-48fe-879e-615da2cc3493",
                    "LayerId": "ab975a19-db9a-41ca-8cc4-aa93ac90ee14"
                }
            ]
        },
        {
            "id": "a2f36c3a-426e-46d0-b8d9-fd9b162eedcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "888e7ab2-eec3-4e17-b539-5b28fd33e349",
            "compositeImage": {
                "id": "8994617b-3bf8-4c73-8d67-56cb9d90737b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2f36c3a-426e-46d0-b8d9-fd9b162eedcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "923292c5-12b5-4c92-a462-02bbc0ebc990",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2f36c3a-426e-46d0-b8d9-fd9b162eedcc",
                    "LayerId": "ab975a19-db9a-41ca-8cc4-aa93ac90ee14"
                }
            ]
        },
        {
            "id": "aef9abe2-1338-426a-9323-cc34d066aad5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "888e7ab2-eec3-4e17-b539-5b28fd33e349",
            "compositeImage": {
                "id": "7de95702-62d1-462e-8680-bc658764dcff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aef9abe2-1338-426a-9323-cc34d066aad5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88db67a9-b1b1-44e6-87ac-d9f175927dca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aef9abe2-1338-426a-9323-cc34d066aad5",
                    "LayerId": "ab975a19-db9a-41ca-8cc4-aa93ac90ee14"
                }
            ]
        },
        {
            "id": "1ed1a0e4-7110-4c2b-bbe5-e031d61a3ad9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "888e7ab2-eec3-4e17-b539-5b28fd33e349",
            "compositeImage": {
                "id": "93ae733b-e833-48f1-be9c-5c49b129df67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ed1a0e4-7110-4c2b-bbe5-e031d61a3ad9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e84a0198-b35a-4b4f-ad45-6e5041732bb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ed1a0e4-7110-4c2b-bbe5-e031d61a3ad9",
                    "LayerId": "ab975a19-db9a-41ca-8cc4-aa93ac90ee14"
                }
            ]
        },
        {
            "id": "757f5bd7-6b01-4731-8dca-0b4d23fc02a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "888e7ab2-eec3-4e17-b539-5b28fd33e349",
            "compositeImage": {
                "id": "61304ac3-6642-4757-b439-3c2d60b32024",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "757f5bd7-6b01-4731-8dca-0b4d23fc02a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a74a220-e008-4a3c-a91f-8112973f97d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "757f5bd7-6b01-4731-8dca-0b4d23fc02a8",
                    "LayerId": "ab975a19-db9a-41ca-8cc4-aa93ac90ee14"
                }
            ]
        },
        {
            "id": "44b04642-a0fa-4f31-8c50-8439ded8e492",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "888e7ab2-eec3-4e17-b539-5b28fd33e349",
            "compositeImage": {
                "id": "b5c63f5e-da77-48eb-a4b3-bb1713c0a2f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44b04642-a0fa-4f31-8c50-8439ded8e492",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3eb2ef2-1f3d-4d2c-b73b-4dd23b53aef0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44b04642-a0fa-4f31-8c50-8439ded8e492",
                    "LayerId": "ab975a19-db9a-41ca-8cc4-aa93ac90ee14"
                }
            ]
        },
        {
            "id": "ce9731fe-0765-4960-bc59-f5641553e3ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "888e7ab2-eec3-4e17-b539-5b28fd33e349",
            "compositeImage": {
                "id": "168c577b-3cc2-48a1-a9c8-0728b431de22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce9731fe-0765-4960-bc59-f5641553e3ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "678aba4e-c67f-4bd0-bceb-7784d8b01640",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce9731fe-0765-4960-bc59-f5641553e3ba",
                    "LayerId": "ab975a19-db9a-41ca-8cc4-aa93ac90ee14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ab975a19-db9a-41ca-8cc4-aa93ac90ee14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "888e7ab2-eec3-4e17-b539-5b28fd33e349",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 33,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}