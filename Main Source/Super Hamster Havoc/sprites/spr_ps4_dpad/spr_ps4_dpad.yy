{
    "id": "71a0c6d1-6125-44c0-a8fb-5cf19574437c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ps4_dpad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 3,
    "bbox_right": 96,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7bae495f-22e5-4c6e-a100-d3c50dc74ba5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71a0c6d1-6125-44c0-a8fb-5cf19574437c",
            "compositeImage": {
                "id": "4514bc23-396f-497b-a9cf-048aac49612b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bae495f-22e5-4c6e-a100-d3c50dc74ba5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d630abb2-a403-4c7b-b648-51b5c35414b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bae495f-22e5-4c6e-a100-d3c50dc74ba5",
                    "LayerId": "a9f55309-7050-438c-9bc9-5dbe923a1290"
                }
            ]
        },
        {
            "id": "33db47ff-7a2b-488f-9fa2-90eb65e0f849",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71a0c6d1-6125-44c0-a8fb-5cf19574437c",
            "compositeImage": {
                "id": "65396671-19f9-48e1-bde2-9fe59ae01381",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33db47ff-7a2b-488f-9fa2-90eb65e0f849",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f60a0962-8ea1-4844-8b85-c7cd66ea51c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33db47ff-7a2b-488f-9fa2-90eb65e0f849",
                    "LayerId": "a9f55309-7050-438c-9bc9-5dbe923a1290"
                }
            ]
        },
        {
            "id": "1416e4c1-589d-4839-9bba-d8fbfbbecb16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71a0c6d1-6125-44c0-a8fb-5cf19574437c",
            "compositeImage": {
                "id": "788635e2-a551-4f09-8db0-f8c5fadf00ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1416e4c1-589d-4839-9bba-d8fbfbbecb16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f47285af-253d-4caf-8ccd-94979a383a18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1416e4c1-589d-4839-9bba-d8fbfbbecb16",
                    "LayerId": "a9f55309-7050-438c-9bc9-5dbe923a1290"
                }
            ]
        },
        {
            "id": "de119959-d720-43c3-93a1-1c7ab98bfe4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71a0c6d1-6125-44c0-a8fb-5cf19574437c",
            "compositeImage": {
                "id": "03e4278e-191e-4172-b866-faf66435a086",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de119959-d720-43c3-93a1-1c7ab98bfe4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae5240b8-9f75-4f5e-99d6-f298c42ad796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de119959-d720-43c3-93a1-1c7ab98bfe4e",
                    "LayerId": "a9f55309-7050-438c-9bc9-5dbe923a1290"
                }
            ]
        },
        {
            "id": "ec693546-8810-409f-8d58-895f1c51c3dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71a0c6d1-6125-44c0-a8fb-5cf19574437c",
            "compositeImage": {
                "id": "e529054c-0268-4223-9df8-724a68b8be51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec693546-8810-409f-8d58-895f1c51c3dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "716eacb3-b8da-4c70-bee3-37695d4bb748",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec693546-8810-409f-8d58-895f1c51c3dd",
                    "LayerId": "a9f55309-7050-438c-9bc9-5dbe923a1290"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "a9f55309-7050-438c-9bc9-5dbe923a1290",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71a0c6d1-6125-44c0-a8fb-5cf19574437c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 50
}