{
    "id": "e365b08d-3809-4767-9cf8-10b8ac3e5c83",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "Eight_bit_chillout_Shader",
    "IncludedResources": [
        "Sprites\\sprRoad",
        "Shaders\\fx8bitChilloutGrayscale",
        "Shaders\\fx8bitChilloutColor",
        "Objects\\objOverlay",
        "Objects\\objImage",
        "Rooms\\roomStart"
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": -1,
    "date": "2018-20-15 12:07:44",
    "description": "",
    "extensionName": "",
    "files": [
        
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": null,
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "com.psichix.psl8bitchillout",
    "productID": "",
    "sourcedir": "",
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": null,
    "tvosdelegatename": null,
    "tvosmaccompilerflags": null,
    "tvosmaclinkerflags": null,
    "tvosplistinject": null,
    "version": "1.0.0"
}