{
    "id": "b175fb09-7aa2-420b-84bb-7961d6a39196",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "Soulcade_Menu_Engine",
    "IncludedResources": [
        "Sprites\\spr_select",
        "Scripts\\readme",
        "Scripts\\scr_title_menu",
        "Fonts\\fnt_small",
        "Fonts\\fnt_large",
        "Objects\\obj_title_menu",
        "Rooms\\rm_main_menu"
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": -1,
    "date": "2018-05-07 05:07:58",
    "description": "",
    "extensionName": "",
    "files": [
        
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosdelegatename": null,
    "iosplistinject": "",
    "license": "Free to use, also for commercial games.",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "com.soulcadeinteractive.menu",
    "productID": "F3D00DAD3DDB83EFFDD568E8093FC7AA",
    "sourcedir": "",
    "tvosProps": false,
    "tvosSystemFrameworkEntries": [
        
    ],
    "tvosThirdPartyFrameworkEntries": [
        
    ],
    "tvosclassname": null,
    "tvosdelegatename": null,
    "tvosmaccompilerflags": null,
    "tvosmaclinkerflags": null,
    "tvosplistinject": null,
    "version": "1.0.0"
}